#ifndef _PARCEL_MODEL_OPTIMIZATION_2_H_
#define _PARCEL_MODEL_OPTIMIZATION_2_H_

#pragma once

#include "ModelData.h"

struct OptimSpectrum2
{
	CString SampleName;
	CString TransName;
	int Num;

	CString Name;
	double Ref;

	bool Use;

	OptimSpectrum2(CString sam, CString trans, int num, double ref, bool use, int indT = 0);
	void Copy(OptimSpectrum2& data);
	bool IsSample(CString sam, CString trans);
};

typedef vector<OptimSpectrum2> VOptSpec2;
typedef vector<OptimSpectrum2>::iterator ItOptSpec2;

struct BadOutsSpectrum2
{
	CString SampleName;
	CString TransName;
	int Num;
};

typedef vector<BadOutsSpectrum2> VBOSp2;
typedef vector<BadOutsSpectrum2>::iterator ItBOSp2;

struct OptimPreprocSet
{
	bool pA;
	bool pB;
	bool pM;
	bool pD;
	bool pC;
	bool pN;
	bool p1;
	bool p2;
	bool pNo;

	int MaxLen;

	bool UseBest;
	int NumBest;

	VStr Adds;

	OptimPreprocSet():pA(false),pB(false),pM(false),pD(false),pC(false),pN(false),
				  p1(false),p2(false),pNo(true),MaxLen(3),UseBest(false),NumBest(1)
	{};
	void Copy(OptimPreprocSet& data);
	CString GetAddsAsStr();
	void SetAddsFromStr(CString str);
};

struct OptimCritSet
{
	bool bSEC;
	bool bSECV;
	bool bSECSECV;
	bool bSEV;

	bool bR2sec;
	bool bR2secv;
	bool bF;
	bool bErr;
	bool bSDV;
	bool bR2sev;

	bool bK1;
	bool bK2;
	bool bK3;
	bool bK4;
	bool bK5;
	bool bK6;
	bool bK7;

	OptimCritSet():bSEC(true),bSECV(true),bSECSECV(false),bSEV(false),bR2sec(true),bR2secv(false),bF(false),bErr(false),bSDV(false),
		bR2sev(false),bK1(false),bK2(false),bK3(false),bK4(false),bK5(false),bK6(false),bK7(false)
	{};
	void Copy(OptimCritSet& data);
	void Empty();
	void SetK(int crit);
};

struct OutsSet
{
	bool chMah;
	bool chSEC;
	bool chSECV;
	bool chBad;
	bool chBound;

	double limMah;
	double limSEC;
	double limSECV;
	double limBound;

	OutsSet():chMah(false),chSEC(false),chSECV(false),chBad(false),chBound(false),limMah(3.),limSEC(1.),limSECV(1.),limBound(3.)
	{};
	void Copy(OutsSet& data);
};

struct OptimStepSettings
{
	int ModelType;			// ��� ������
	int SpectrumType;		// ��� �������

	int NCompMean;			// ������� �������� ����� ������� ���������
	int NCompPlus;			// ����� �� ����� ������� ���������
	int NCompStep;			// ��� �� ����� ������� ���������

	double HiperMean;		// ������� �������� ������� ���������
	double HiperPlus;		// ����� �� ������� ���������
	double HiperStep;		// ��� �� ������� ���������

	int NHarmMean;			// ������� �������� ����� ��������
	int NHarmPlus;			// ����� �� ����� ��������
	int NHarmStep;			// ��� �� ����� ��������
	int NHarmType;			// ��� hqo-������ (�������������)

	double KfnlMean;		// ������� �������� ������������ ������������
	double KfnlPlus;		// ����� �� ������������ ������������
	double KfnlStep;		// ��� �� ������������ ������������
	int KfnlType;			// ��� hqo-������ (����������)

	int LeftMean;			// ������� �������� ����� ������� ������������� ���������
	int LeftPlus;			// ���� �� ����� ������� ������������� ���������
	int LeftStep;			// ��� �� ����� ������� ������������� ���������

	int RightMean;			// ������� �������� ������ ������� ������������� ���������
	int RightPlus;			// ���� �� ������ ������� ������������� ���������
	int RightStep;			// ��� �� ������ ������� ������������� ���������

	bool IsOperation;		// ������� ��������������� ��������
	int Operation;			// ��� ��������������� ��������

	int MainCrit;			// �������� ��� ��������������� ��������

	int SortCrit;			// �������� ��� ����������
	int MaxModels;			// ������������ ����� �������, ��������� � ������� ����������� (�� ������� ��������)

	VOptSpec2 Calib;		// ������ �������� ��� �����������
	VOptSpec2 Valid;		// ������ �������� ��� ���������
	SInt ExcPoints;			// ������ ����������� ������������ �����

	OptimPreprocSet	Preps;	// ��������� �������������
	OptimCritSet Crits;		// �������� �����������
	OutsSet Outs;			// ��������� ������� �� �������

	void Copy(OptimStepSettings& data);
	void GetExcPoints(SInt& points);
	void GetCalibValid(VOptSpec2& calib, VOptSpec2& valid);
	int GetNCalibUse();
	int GetNValidUse();
	OptimSpectrum2* GetCalib(CString name, CString transname, int num);
	void ExcAllBadSpectra(VBOSp2* badsp);
};

typedef vector<OptimStepSettings> VOptStep;
typedef vector<OptimStepSettings>::iterator ItOptStep;

struct OptimModel
{
	int Num;

	int ModelType;
	int SpectrumType;
	int RangeLeft;
	int RangeRight;
	int NComp;
	double Hiper;
	int NHarm;
	double Kfnl;
	CString Preproc;

	SInt ExcPoints;

	MapIntDbl CritValues;

	OptimModel();

	void SetCrits(VInt* crits);
	void Copy(OptimModel& data);
	void GetExcPoints(SInt& points);
	bool SetValue(int crit, double val);
	double GetValue(int crit);
	int GetNExcPoints();
	void SetRange(int left, int right, int low, int upper);
};

typedef vector<OptimModel> VOptMod;
typedef vector<OptimModel>::iterator ItOptMod;

struct OptimResults
{
	int MaxModels;

	VInt Crits;
	MIntDbl Sorts;
	VOptMod OptModels;

	int NCalcModels;

	VModSam SamplesForCalib;
	VModSam SamplesForValid;

	OptimResults(OptimCritSet* critset, int max);

	int GetPosByNum(int num);
	bool DeleteByNum(int num);
	bool AddOptimModel(OptimModel& optmod);
	void EndAdding();
	void Sort(int crit);
	int GetNCrits();
	int GetNModels();
	bool IsHQO();
	bool IsPrepUse(int prep, int maxmod);
	void ClearResults();
	int FindCritInd(int crit);
};

typedef vector<OptimResults> VOptRes;
typedef vector<OptimResults>::iterator ItOptRes;

#endif
