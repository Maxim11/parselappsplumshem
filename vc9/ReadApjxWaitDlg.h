#ifndef _READ_APJX_WAIT_DLG_H_
#define _READ_APJX__WAIT_DLG_H_

#pragma once

#include "WaitingDlg.h"
#include "DeviceData.h"


///////////////////////////////////////////////////////////////////////////////////////////////////
// ������ ��������� �������� ��������� apjx-�����

class CReadApjxWaitDlg : public CWaitingDlg
{
	DECLARE_DYNAMIC(CReadApjxWaitDlg)

public:

	CReadApjxWaitDlg(CDeviceData* dev, apjdb::PRApjProject* apjx, CString fname, CWnd* pParent = NULL);	// �����������
	virtual ~CReadApjxWaitDlg();																		// ����������

	enum { IDD = CWaitingDlg::IDD };	// ������������� �������

protected:

	apjdb::PRApjProject* Apjx;
	CDeviceData* pDevice;
	CString FileName;

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();						// ������������� �������

	virtual void OK();				// ��������� �������� ����� �� �������
	virtual void Stop();			// ��������� ����������� ����� �� �������
	virtual void Work();			// ������ �������� �������

	DECLARE_MESSAGE_MAP()
};

#endif
