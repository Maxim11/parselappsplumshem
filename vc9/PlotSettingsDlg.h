#ifndef _PLOT_SETTINGS_DLG_H_
#define _PLOT_SETTINGS_DLG_H_

#pragma once

#include "Resource.h"
#include "ShowSpectra.h"
#include "ParcelListCtrl.h"


///////////////////////////////////////////////////////////////////////////////////////////////////
// ������ ��������� ����������� �������

const int C_PLOT_SET_MODE_MODEL		= 0;
const int C_PLOT_SET_MODE_SAMPLE	= 1;

class CPlotSettingsDlg : public CDialog
{
	DECLARE_DYNAMIC(CPlotSettingsDlg)

public:

	CPlotSettingsDlg(CShowSpectra* data, int mode, CWnd* pParent = NULL);		// �����������
	virtual ~CPlotSettingsDlg();									// ����������

	enum { IDD = IDD_PLOT_SETTINGS };	// ������������� �������

	CParcelListCtrl m_listAll;		// �������, ���������� ������ ���� ��������
	CListBox m_listSel;				// �������, ���������� ������ ���� ��������, ��������� ��� �����������
	CComboBox m_Type;

protected:

	int Mode;

	CShowSpectra* OldData;
	CShowSpectra* Data;

	int m_vType;

	bool IsBtnClk;

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();						// ������������� �������

	virtual void OnOK()
	{};
	virtual void OnCancel();				// ��������� ������� "�������� ���� � ����� �� �������"

	afx_msg void OnAccept();				// ��������� ������� "������� ������ � ����� �� �������"
	afx_msg void OnChangeType();
	afx_msg void OnAdd();
	afx_msg void OnAddAll();
	afx_msg void OnRemove();
	afx_msg void OnRemoveAll();
	afx_msg void OnHelp();				// ��������� ������� "������� �������"

	void FillListAll(int newsel = -1);
	void FillListSel(int newsel = -1);

	int Retype(int type, bool dir = false);

	bool IsChanged();

public:

	bool IsSpectrumSel(int ind);

protected:

	DECLARE_MESSAGE_MAP()
};

#endif
