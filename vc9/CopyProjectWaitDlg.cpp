#include "stdafx.h"

#include "CopyProjectWaitDlg.h"
#include "BranchInfo.h"

IMPLEMENT_DYNAMIC(CCopyProjectWaitDlg, CWaitingDlg)

CCopyProjectWaitDlg::CCopyProjectWaitDlg(CDeviceData* dev, CWnd* pParent) : CWaitingDlg(pParent)
{
	DevData = dev;
	Comment = ParcelLoadString(STRID_COPY_PROJECT_WAIT_COMMENT);
}

CCopyProjectWaitDlg::~CCopyProjectWaitDlg()
{
}

void CCopyProjectWaitDlg::DoDataExchange(CDataExchange* pDX)
{
	CWaitingDlg::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CCopyProjectWaitDlg, CWaitingDlg)
END_MESSAGE_MAP()


BOOL CCopyProjectWaitDlg::OnInitDialog()
{
	CWaitingDlg::OnInitDialog();

	return true;
}

void CCopyProjectWaitDlg::OK()
{
	CWaitingDlg::OK();
}

void CCopyProjectWaitDlg::Stop()
{
	CWaitingDlg::Stop();
}

void CCopyProjectWaitDlg::Work()
{
	ParcelWait(true);
	
	bool res = DevData->AcceptCopyPrj();

	ParcelWait(false);

	if(res)
		OK();
	else
		Stop();
}
