#include "stdafx.h"

#include "WaitingDlg.h"
#include "wm.h"


IMPLEMENT_DYNAMIC(CWaitingDlg, CDialog)

CWaitingDlg::CWaitingDlg(CWnd* pParent) : CDialog(CWaitingDlg::IDD, pParent)
{
	Comment = cEmpty;
}

CWaitingDlg::~CWaitingDlg()
{
}

void CWaitingDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CWaitingDlg, CDialog)

	ON_WM_TIMER()

END_MESSAGE_MAP()


BOOL CWaitingDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	SetWindowText(ParcelLoadString(STRID_WAITING_DEFHDR));

	GetDlgItem(IDC_STATIC_1)->SetWindowText(Comment);

	SetTimer(PARCEL_WM_PROGRESS_TIMER, 100, 0);

	return true;
}

void CWaitingDlg::OK()
{
	CDialog::OnOK();
}

void CWaitingDlg::Stop()
{
	CDialog::OnCancel();
}

void CWaitingDlg::Work()
{
	OK();
}

void CWaitingDlg::OnTimer(UINT Id)
{
	KillTimer(Id);

	Work();
}
