#include "stdafx.h"

#include "ConvertAcceptDlg.h"
#include "MainFrm.h"


IMPLEMENT_DYNAMIC(CConvertAcceptDlg, CDialog)

CConvertAcceptDlg::CConvertAcceptDlg(CLoadOldDatas* pload, CWnd* pParent) : CDialog(CConvertAcceptDlg::IDD, pParent)
{
	pLoad = pload;

	m_vUnit = ParcelLoadString(STRID_CONVERT_ACCEPT_DEFUNIT);
}

CConvertAcceptDlg::~CConvertAcceptDlg()
{
}

void CConvertAcceptDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_ERROR_LIST, m_list);
	DDX_Control(pDX, IDC_USE, m_NoNum);
	DDX_Control(pDX, IDC_CHECK_WAVE, m_CheckWave);
	DDX_Control(pDX, IDC_INPUT_UNIT, m_Unit);

	cParcelDDX(pDX, IDC_INPUT_UNIT, m_vUnit);
	DDV_MaxChars(pDX, m_vUnit, 20);
}


BEGIN_MESSAGE_MAP(CConvertAcceptDlg, CDialog)

	ON_BN_CLICKED(IDC_ACCEPT, OnAccept)
	ON_BN_CLICKED(IDHELP, OnHelp)

END_MESSAGE_MAP()


BOOL CConvertAcceptDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	CString s;
	int i;

	s = ParcelLoadString(STRID_CONVERT_ACCEPT_COLNUM);
	m_list.InsertColumn(0, s, LVCFMT_LEFT, 0, 0);
	s = ParcelLoadString(STRID_CONVERT_ACCEPT_COLCODE);
	m_list.InsertColumn(1, s, LVCFMT_LEFT, 0, 1);
	s = ParcelLoadString(STRID_CONVERT_ACCEPT_COLERR);
	m_list.InsertColumn(2, s, LVCFMT_LEFT, 0, 2);
	for(i=0; i<3; i++)
		m_list.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	m_list.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

	FillList();

	SetWindowText(ParcelLoadString(STRID_CONVERT_ACCEPT_HDR));

	GetDlgItem(IDC_ACCEPT)->SetWindowText(ParcelLoadString(STRID_CONVERT_ACCEPT_ACCEPT));
	GetDlgItem(IDCANCEL)->SetWindowText(ParcelLoadString(STRID_CONVERT_ACCEPT_DECLINE));
	GetDlgItem(IDHELP)->SetWindowText(ParcelLoadString(STRID_CONVERT_ACCEPT_HELP));
	GetDlgItem(IDC_USE)->SetWindowText(ParcelLoadString(STRID_CONVERT_ACCEPT_WITHOUT));
	GetDlgItem(IDC_CHECK_WAVE)->SetWindowText(ParcelLoadString(STRID_CONVERT_ACCEPT_BADWAVE));
	GetDlgItem(IDC_STATIC_2)->SetWindowText(ParcelLoadString(STRID_CONVERT_ACCEPT_REPLACE));

	GetDlgItem(IDHELP)->EnableWindow(pFrame->IsExistHelp());

	return true;
}

void CConvertAcceptDlg::OnAccept()
{
	if(!UpdateData())
		return;

	if(pLoad->Checks.EmptyUnit != C_CONVERT_WITHOUT_NOT)
	{
		m_vUnit.Trim();
		if(m_vUnit.IsEmpty())
		{
			ParcelError(ParcelLoadString(STRID_CONVERT_ACCEPT_ERROR_EMPTYUNIT));
			return;
		}
		pLoad->Checks.ReplaceUnit = m_vUnit;
	}

	if(m_NoNum.GetCheck())  pLoad->Checks.WithoutNumber = C_CONVERT_WITHOUT_ACCEPT;
	else  pLoad->Checks.WithoutNumber = C_CONVERT_WITHOUT_DECLINE;

	if(m_CheckWave.GetCheck())  pLoad->Checks.BadWave = C_CONVERT_BADWAVE_ACCEPT;
	else  pLoad->Checks.BadWave = C_CONVERT_BADWAVE_DECLINE;

	CDialog::OnOK();
}

void CConvertAcceptDlg::OnCancel()
{
	CDialog::OnCancel();
}

void CConvertAcceptDlg::FillList()
{
	m_list.DeleteAllItems();

	CString s;
	int i, N = int(pLoad->Checks.Errors.size());

	for(i=0; i<N; i++)
	{
		s.Format(cFmt1, i+1);
		m_list.InsertItem(i, s);
		s.Format(cFmt02, pLoad->Checks.ErrNums[i]);
		m_list.SetItemText(i, 1, s);
		s = pLoad->Checks.Errors[i];
		m_list.SetItemText(i, 2, s);
	}

	for(i=0; i<3; i++)
		m_list.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);

	m_NoNum.SetCheck(pLoad->Checks.WithoutNumber == C_CONVERT_WITHOUT_ACCEPT);
	m_CheckWave.SetCheck(pLoad->Checks.BadWave == C_CONVERT_BADWAVE_ACCEPT);
	GetDlgItem(IDC_USE)->EnableWindow(pLoad->Checks.WithoutNumber != C_CONVERT_WITHOUT_NOT);
	GetDlgItem(IDC_CHECK_WAVE)->EnableWindow(pLoad->Checks.BadWave != C_CONVERT_BADWAVE_NOT);

	GetDlgItem(IDC_INPUT_UNIT)->EnableWindow(pLoad->Checks.EmptyUnit != C_CONVERT_WITHOUT_NOT);
	GetDlgItem(IDC_STATIC_2)->EnableWindow(pLoad->Checks.EmptyUnit != C_CONVERT_WITHOUT_NOT);

	s = (N > 0) ? ParcelLoadString(STRID_CONVERT_ACCEPT_RESBAD) : ParcelLoadString(STRID_CONVERT_ACCEPT_RESOK);
	GetDlgItem(IDC_STATIC_1)->SetWindowText(s);
}

void CConvertAcceptDlg::OnHelp()
{
	int IdHelp = DLGID_PROJECT_CONVERT_ACCEPT;

	pFrame->ShowHelp(IdHelp);
}
