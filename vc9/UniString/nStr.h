/* base template class */

#ifndef __NSTR_H_
#define __NSTR_H_

#pragma once

#include "nstrNLangSupport.h"
#include "singleton.h"
#include "a2w.h"

namespace nstr
{
	// � ������� ������ ����� �����:

	const int C_MAX_STR_NUM	= 1202;

	LUM_DECLARE_STRID( STRID_COUNT,	C_MAX_STR_NUM)

	enum ELang
	{
		LANG__RUSSIAN = 0
		, LANG__ENGLISH
		, LANG__GERMAN
		, LANG__POLISH
		, LANG__CZECH
		, LANG__FRENCH
		, LANG__ITALIAN
		, LANG__PORTUGUESE
		//-------------
		, LANG__COUNT
		, LANG__FIRST = 0
	};

	typedef LangStrManager<ELang, LANG__COUNT, nstr::STRID_COUNT > NStrManager;

	static bool InitNStrSupport(const std::wstring& strInitialDir)
	{
		std::wstring strLangNames[ LANG__COUNT ];

		// �������� �����, ����� ����� ��������� �� ����������
		for( UINT nLang = LANG__FIRST; nLang < LANG__COUNT; nLang++ )
			strLangNames[ nLang ] = _T("");

		// ����� ���������( ��� �������� "ts" )
		strLangNames[ LANG__RUSSIAN		]	= _T("Russian");
		strLangNames[ LANG__ENGLISH		]	= _T("English");
		strLangNames[ LANG__GERMAN		]	= _T("German");	
		strLangNames[ LANG__POLISH		]	= _T("Polish");
		strLangNames[ LANG__CZECH		]	= _T("Czech");
		strLangNames[ LANG__FRENCH		]	= _T("French");
		strLangNames[ LANG__ITALIAN		]	= _T("Italian");
		strLangNames[ LANG__PORTUGUESE	]	= _T("Portuguese");

		for(UINT nLang = LANG__FIRST; nLang < LANG__COUNT; ++nLang) {
			Singleton<NStrManager>::Instance()->LoadStringsForLanguage(nLang, strInitialDir.c_str(), strLangNames[ nLang ].c_str());
		}

		Singleton<NStrManager>::Instance()->SetCurrentLanguage(LANG__RUSSIAN);

		return true;
	}
}

#endif