////////////////////////////////////////////////////////////////////////////
//	Author      : Ignatiev Gena 2005
//	Email       : gena.ignatev@paloma.spbu.ru
//	Module      : nstrNLangSupport.h
//	Description : ��������� ����������� ��������� ������ (������ �����)
//              : ��� ������ ���������� � ������������ ���� nstr
//              : -------------= ������ � ���� �����: =----------------
//              : NStrStorage    - ����������� �� ������/������ �/�� ����(�)
//              :                  *.lng ���� ������-����� ��� ����������
//              : StrResource    - ������� ���� ����� ��� ����������� �����
//              : LangStrManager - ��������� ���������� ��������� (StrResource)
//              :                  ��� ���� ������ ����������
////////////////////////////////////////////////////////////////////////////
#ifndef _LUMEX_NATIONAL_LANGUAGES_SUPPORT_DEFINED_H__
#define _LUMEX_NATIONAL_LANGUAGES_SUPPORT_DEFINED_H__





#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


#include <vector>
#include <list>
#include <map>
#include <string>


#include "A2W.h"

typedef DWORD STRID;

namespace nstr
{


#define LUM_DECLARE_STRID( name, id )	const STRID name = (id);
										


/**********************************************************************
*                          -= NStrStorage =-
*	Description : ������������� �� ������/������ ���������� *.lng ����� 
**********************************************************************/
template < typename StrT >
class NStrStorage
{
	typedef typename StrT::value_type				CharType;
	typedef std::map< STRID, StrT >					NStrTable;
	typedef typename NStrTable::const_iterator		NStrConstIter;

	enum{ eHEADERSIZE = 512, eVERSION = 1000 };
public:

	// .ctor
	NStrStorage() : m_locale( "" ), m_lastid( 0 ){}

	// 
	void Put( DWORD id, const StrT& str )
	{
		m_strings[ id ] = str;		
		if( m_lastid < id )
			m_lastid = id;
	}

	//
	StrT const& Get( DWORD id )const	
	{
		static const StrT sNull = StrT();

		NStrConstIter istr = m_strings.find( id );
		if( istr != m_strings.end() ) 
			return istr->second;

		return sNull;
	}

	void SetLocaleName( const char* sloc )		{ m_locale = sloc; }
	std::string const&	GetLocaleName()const	{ return m_locale; }
	
	DWORD Count()const							{ return (DWORD)m_strings.size(); }
	DWORD LastID()const							{ return m_lastid; }
	bool IsEmpty()const							{ return m_strings.empty(); }

	bool Save( LPCTSTR file )const;
	bool Load( LPCTSTR file );



private:

	bool WriteNStr( CFile& storage, NStrConstIter& iter )const;
	bool ReadNStr( CFile& storage, DWORD* dwPos );

	void Clear()
	{
		m_strings.clear();
		m_locale = "";
		m_lastid = 0;
	}

private:
	NStrTable	m_strings;
	std::string m_locale;
	DWORD		m_lastid;
};
//-------------------------------------------------------------------------
//������������������ ������ � ����:
//	1) ��������� (������ 512 ����), ������� ��������:
//		1.1) ��������: "-=SpectroLumEx national strings storage=-"
//		1.2) ���������� � ���������� �����:  "< size: 000000 >"
//		1.3) ���������� � ������ �����: "< language: %s >";
//	2) ��������� �������� ������ (����� = eLOCALE_LENGTH )
//	3) ����� ������ �������� ��������� ����� (������ ����: sizeof(DWORD) )
//	4) ���������� ���� ����� � ��������� (������ ����: sizeof(DWORD) )
//	5) �������� ������������� ������ NSTRID (������ ����: sizeof(DWORD) )
//	6) ����� ����������� ������ - ������ � ���������:
//		6.1) ������������� ������ (������ ����: sizeof(DWORD) )
//		6.2) ������ ������ � ������ (������ ����: sizeof(DWORD) )
//		6.3) ���������� ������
template < typename StrT >
bool NStrStorage<StrT>::Save( LPCTSTR file )const
{
	CFile	storage;
	char	header[ eHEADERSIZE ];
	DWORD	size = Count();

	const char* sHEADER	= "-=SpectroLumEx national strings storage=-< size: %06d >< language: %s >";


	memset( header, 0, eHEADERSIZE );

	if( !storage.Open( file, CFile::modeCreate | CFile::modeWrite ) )
		return false;

	sprintf( header, sHEADER, size, m_locale.c_str() );

	// ����� ��������� 
	storage.Write( header, eHEADERSIZE );

	// ������:
	const DWORD version = eVERSION;
	storage.Write( &version, sizeof(DWORD) );

	// ���������� ���� ����� � ���������
	storage.Write( &size, sizeof(DWORD) );

	// �������� ������������� ������ STRID
	storage.Write( &m_lastid, sizeof(DWORD) );

	// ������:
	for( NStrConstIter	iter = m_strings.begin(); iter != m_strings.end(); ++iter )
		if( !WriteNStr( storage, iter ) ) 
			return false;

	storage.Close();

	return true;
}
//-------------------------------------------------------------------------
template < typename StrT >
bool NStrStorage<StrT>::Load( LPCTSTR file )
{
	Clear();

	// ��������� ������ (��������� � 512 ���� ����������)
	CFile storage;
	if( !storage.Open( file, CFile::modeRead | CFile::shareDenyNone ) )
		return false;

	DWORD dwPos = 0;
	DWORD dwRead, dwValue, dwSize, dwLast, dwLength;

	dwLength = static_cast<DWORD>(storage.GetLength());

	// ������� � ������ ��������� ���������� � ������:
	char buffer[ eHEADERSIZE ];
	dwRead = storage.Read( buffer, eHEADERSIZE );
	if( dwRead != eHEADERSIZE ) return false;
	buffer[ eHEADERSIZE-1 ] = 0;
	std::string header = buffer;

	typedef std::string::size_type	StrPos;
	StrPos bgn = header.find( "language: " );
	if( bgn != header.npos ) 
	{
		bgn += strlen("language: ");
		StrPos end = header.find(' ', bgn+1);
		if( end != header.npos )
			m_locale = header.substr( bgn, end-bgn );
	}

	dwPos = eHEADERSIZE;

	storage.Seek( dwPos, CFile::begin );
	dwRead = storage.Read( &dwValue, sizeof(DWORD) );

	if( dwValue != eVERSION ) {
		return false;
	}

	// ���������� ���� ����� � ���������
	dwPos += sizeof(DWORD);
	storage.Seek( dwPos, CFile::begin );
	dwRead = storage.Read( &dwSize, sizeof(DWORD) );

	// �������� ������������� ������ STRID
	dwPos += sizeof(DWORD);
	storage.Seek( dwPos, CFile::begin );
	dwRead = storage.Read( &dwLast, sizeof(DWORD) );

	// ������:
	dwPos += sizeof(DWORD);
	storage.Seek( dwPos, CFile::begin );

	while( dwPos < dwLength )
	{
		if( !ReadNStr( storage, &dwPos ) )
		{
			Clear();
			return false;
		}
	}

	// ���������:
	return ( dwLast == m_lastid && dwSize == Count() );
}
//-------------------------------------------------------------------------
template < typename StrT >
bool NStrStorage<StrT>::WriteNStr( CFile& storage, NStrConstIter& iter )const
{
	// ������������������ ������:
	//	1) STRID
	//	2) LENGTH = length of string in bytes = str.length() * sizeof( CharType )
	//	2) BUFFER[ LENGTH ] - ���������� ������
	DWORD			length	= static_cast<DWORD>(iter->second.length()) * sizeof( CharType );
	const CharType*	pstr	= iter->second.c_str();
	const BYTE*		pbuffer = static_cast<const BYTE*>(pstr);

	storage.Write( &(iter->first),	sizeof( DWORD ) );
	storage.Write( &length,			sizeof( DWORD ) );
	storage.Write( pbuffer,			length );

	return ( storage.m_hFile != CFile::hFileNull );
}
//-------------------------------------------------------------------------
template < typename StrT >
bool NStrStorage<StrT>::ReadNStr( CFile& storage, DWORD* pPos )
{
	// ������������������ ������:
	//	1) STRID
	//	2) LENGTH = length of string in bytes = str.length() * sizeof( CharType )
	//	2) BUFFER[ LENGTH ] - ���������� ������
	DWORD strID, length, dwRead;
	StrT wstr;

	//	1) STRID
	dwRead = storage.Read( &strID,	sizeof( DWORD ) );
	if( dwRead != sizeof( DWORD ) ) 
		return false;

	*pPos += sizeof(DWORD);
	storage.Seek( *pPos, CFile::begin );

	//	2) LENGTH 
	dwRead = storage.Read( &length,	sizeof( DWORD ) );
	if( dwRead != sizeof( DWORD ) ) 
		return false;

	*pPos += sizeof(DWORD);
	storage.Seek( *pPos, CFile::begin );

	// ���������� ������:
	CharType* buff = new CharType[ length / sizeof(CharType) + 1 ];

	//wstr.resize( length / sizeof(CharType) );

	dwRead = storage.Read( reinterpret_cast<BYTE*>(buff), length );
	if( dwRead != length ) 
	{
		delete[] buff;
		return false;
	}

	buff[ length / sizeof(CharType) ] = CharType();
	wstr = buff;
	delete[] buff;

	Put( strID, wstr );

	*pPos += length;

	return ( storage.m_hFile != CFile::hFileNull );
}
//-------------------------------------------------------------------------





/**********************************************************************
*                          -= StrResource =-
*	Description : �������������� ��������� �������� �� �������
**********************************************************************/
template < UINT nNStrCount >
class StrResource
{
public:

	enum{ eNStrCount = nNStrCount };

	// .ctor
	StrResource()
		: m_language		( _T("")		)
		, m_on_bad_index	( _T("_[?]_")	)
		, m_locale_name		( ""			)
		, m_empty			( true			)
	{}

	// .dtor
	~StrResource()						{ if( !m_empty ) Free(); }

	const char*	GetLocaleName()const	{ return m_locale_name.c_str(); }
	LPCTSTR		GetLanguageName()const	{ return m_language.c_str(); }
	bool		IsEmpty()const			{ return m_empty; }
	STRID		Size()const				{ return (STRID)m_strtable.size(); }

	bool		Load( LPCTSTR lpszPath );
	void		Free();

	LPCTSTR	GetText( STRID id )const	
	{
		return ( id < STRID(m_strtable.size()) ) ? m_strtable[ id ].c_str() : m_on_bad_index.c_str();
	}

private:

	void ReplaceTags( a2w::tstring& str ); 


private:	
	typedef std::vector< a2w::tstring >	StrTable;
	
	StrTable					m_strtable;			
	a2w::tstring				m_language;
	a2w::tstring				m_on_bad_index;			// ������ ������
	std::string					m_locale_name;
	bool						m_empty;
};
//-------------------------------------------------------------------------
template < UINT nNStrCount >
void StrResource<nNStrCount>::Free()
{
	m_strtable.clear();
	m_locale_name.clear();
	m_empty = true;
}
//-------------------------------------------------------------------------
template < UINT nNStrCount >
bool StrResource<nNStrCount>::Load( LPCTSTR lpszPath )
{
	Free();

	NStrStorage<std::wstring>	storage;

	if( !storage.Load( lpszPath ) || storage.LastID()+1 != eNStrCount )
	{
		Free();
		return false;
	}

	m_strtable.resize( eNStrCount );

	for( DWORD id = 0; id < eNStrCount; ++id )
	{
		m_strtable[ id ] = a2w::to_tstring( storage.Get(id), storage.GetLocaleName().c_str() );
		ReplaceTags( m_strtable[ id ] );
	}

	m_empty = m_strtable.empty();
	return !m_empty;
}
//-------------------------------------------------------------------------
template < UINT nNStrCount >
void StrResource<nNStrCount>::ReplaceTags( a2w::tstring& str )
{
	static const a2w::tstring	strNEWLINE = _T("<br>");
	static const a2w::tstring	strTMPLBGN = _T("<ins>");
	static const a2w::tstring	strTMPLEND = _T("</ins>");
	static const a2w::tstring	strCOMMBGN = _T("<!--");	// ������ �����������
	static const a2w::tstring	strCOMMEND = _T("-->");		// ����� �����������

	typedef a2w::tstring::size_type		StrPos;		

	// �������� ������� ������:
	StrPos pos = 0;
	while( str.npos != (pos = str.find( strNEWLINE, pos )) )
	{
		str.replace( pos, strNEWLINE.size(), _T("\n") );
	}

	// ������� ���� ��������� ����������� <ins>%s</ins>
	pos = 0;
	while( str.npos != (pos = str.find( strTMPLBGN, pos )) )
	{
		str.erase( pos, strTMPLBGN.size() ) ;
	}

	pos = 0;
	while( str.npos != (pos = str.find( strTMPLEND, pos )) )
	{
		str.erase( pos, strTMPLEND.size() ) ;
	}

	// ������� �����������:
	StrPos bgn, end;
	bgn = str.find( strCOMMBGN );
	if( bgn != str.npos )
	{
		end = str.find( strCOMMEND, bgn );
		if( end != str.npos )
		{
			end += strCOMMEND.length();
			str.erase( bgn, (end-bgn) );
		}
	}
}
//-------------------------------------------------------------------------



/**********************************************************************
*                         -= LangStrManager =-
*	Description : ��������� ������������ ����� ��� ������ ������
*
*   Tmpl Params : LangE      - ������������ (enum) ���� ������
*                 nLangCount - ���������� ���� ������
*                 nNStrCount - ���������� ����� � ��������� �������
*
*	Interface	:
*					bool IsOK( UINT	nLang )const;	
*
*					void SetLanguageInterface( LangE lang );
*
*					LangE GetLanguageInterface()const;
*
*					bool IsCurrentLanguageUI( LangE lang );
*
*					bool LoadStringsForLanguage	( UINT		nLang
*												, LPCTSTR	lpszLangDir
*												, LPCTSTR	lpszResFileName );
*
*					LPCTSTR GetText( UINT nLang, STRID nStrId )const;
*
*					LPCTSTR GetTextUI( STRID nStrId )const;
*
*					StrTable const* GetStrTable( UINT nLang )const;
*
*					void SetGlobalLocale( UINT nLang );
*
*					void RestorePrevLocale();
**********************************************************************/
template < typename LangE, UINT nLangCount, UINT nNStrCount >
class LangStrManager
{

public:
	enum{ LANG_COUNT = nLangCount };	// ���������� ��������� ������ (���������� ��������)
	enum{ NSTR_COUNT = nNStrCount };	// ���������� ������.����� ��� ������� ������

	typedef StrResource<nNStrCount>		NStrResource;

	// .ctor
	LangStrManager( ) 
		: /*m_langUI( 0 )
		,*/ m_locale_switcher( NULL ){}

	// .ctor
	LangStrManager( LangE defaultLang ) 
		: m_langUI( defaultLang )
		, m_locale_switcher( NULL ){}

	// .dtor
	~LangStrManager() { RestorePrevLocale(); }

	// ������� ����
	bool	IsCurrentLanguage( LangE lang )const	{ return (lang == m_langUI); }
	void	SetCurrentLanguage( LangE lang )		{ m_langUI = lang; }
	LangE	GetCurrentLanguage()const				{ return m_langUI; }

	// ������� ����� ��� ����� nLang ���������� � �� �����
	bool IsOK( UINT	nLang )const
	{
		ASSERT( nLang < LANG_COUNT );
		return nLang < LANG_COUNT && !m_languages[ nLang ].IsEmpty();
	}

	// ��������� ������� ������ ��� ����� nLang �� ���������� lpszLangDir (��z ����� ��� ����������)
	bool LoadStringsForLanguage	( UINT		nLang
								, LPCTSTR	lpszLangDir
								, LPCTSTR	lpszResFileName )
	{
		ASSERT( nLang < LANG_COUNT );
		ASSERT( NULL != lpszLangDir );
		ASSERT( NULL != lpszResFileName );
	
		CString strPath = MakeLangPath(lpszLangDir, _T("ts"), lpszResFileName );
		bool bIsValid = m_languages[ nLang ].Load( strPath );
		if( !bIsValid ) m_languages[ nLang ].Free();
		
		return bIsValid;
	}


	LPCTSTR GetText( UINT nLang, STRID nStrId )const
	{
		static LPCTSTR sNull = _T("");

		ASSERT( nLang < LANG_COUNT );
		ASSERT( !m_languages[ nLang ].IsEmpty() );

		if( nLang >= LANG_COUNT || m_languages[ nLang ].IsEmpty() ) 
			return sNull;
		
		return m_languages[ nLang ].GetText( nStrId );
	}
	
	// ��������� ������ ��� �����
	LPCTSTR GetTextUI( STRID nStrId )const	{ return GetText( m_langUI, nStrId ); }


	NStrResource const* GetStrTable( UINT nLang )const
	{
		ASSERT( nLang < LANG_COUNT );
		return ( nLang >= LANG_COUNT ) ? NULL : &m_languages[ nLang ];
	}

	void SetGlobalLocale( UINT nLang )
	{
		ASSERT( nLang < LANG_COUNT );
		if( nLang < LANG_COUNT )
		{
			RestorePrevLocale();
			// ����������������� ���������� ������
			m_locale_switcher = new a2w::GlobalLocalWrapper( m_languages[ nLang ].GetLocaleName() );
		}
	}

	void RestorePrevLocale()
	{
		if( m_locale_switcher ) 
			delete m_locale_switcher; // ��������������� ������ ���������� ������
		m_locale_switcher = NULL;
	}

private:

	CString MakeLangPath( LPCTSTR lpszDir, LPCTSTR lpszPrefix, LPCTSTR lpszName )
	{
		CString strDir = lpszDir;
		if( strDir.Right(1) != _T("\\") ) 
			strDir += _T("\\");

		CString strPath;
		strPath.Format( _T("%s%s%s.lng"), strDir, lpszPrefix, lpszName );
		return strPath;
	}

private:
		
	LangE						m_langUI;					// ������� ���� ����������
	NStrResource				m_languages[ LANG_COUNT ];
	a2w::GlobalLocalWrapper*	m_locale_switcher;
};
//-------------------------------------------------------------------------




} //namespace nstr

#endif // _LUMEX_NATIONAL_LANGUAGES_SUPPORT_DEFINED_H__



