////////////////////////////////////////////////////////////////////////////
//	Author      : Ignatiev Gena 2005
//	Email       : gena.ignatev@paloma.spbu.ru
//	Module      : A2W.h
//	Description : 
////////////////////////////////////////////////////////////////////////////
#ifndef _CHAR_STRING_TO_UNICODE_STRING_DEFINED_H__
#define _CHAR_STRING_TO_UNICODE_STRING_DEFINED_H__

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include <string>
#include <locale>

namespace a2w
{

#ifdef _UNICODE
	typedef wchar_t					tchar;
	typedef std::wstring			tstring;
	typedef std::wistringstream		tistringstream;
	typedef std::wostringstream		tostringstream;
	typedef std::wofstream			tofstream;
	typedef std::wifstream			tifstream;

	#define tcout					std::wcout
	#define tcin					std::wcin
#else
	typedef char					tchar;
	typedef std::string				tstring;
	typedef std::istringstream		tistringstream;
	typedef std::ostringstream		tostringstream;
	typedef std::ofstream			tofstream;
	typedef std::ifstream			tifstream;

	#define tcout					std::cout
	#define tcin					std::cin
#endif

#ifdef _UNICODE
	#define TtoF				_wtof
	#define TtoI				_wtoi
#else
	#define TtoF				atof
	#define TtoI				atoi
#endif



inline tstring ItoT(int n)
	{ 
		tchar sz[12]; return _itot(n, sz, 10); 
	}

inline tstring FtoT(double d)
	{ 
		char buffer[_CVTBUFSIZE]; _gcvt(d, 12, buffer); 

#ifdef _UNICODE
		tchar wbuffer[_CVTBUFSIZE];
		swprintf( wbuffer, _T("%S"), buffer );
		return tstring(wbuffer);
#else
		return tstring(buffer);
#endif
	}

inline bool EqualCI(const tstring& s1, const tstring& s2) 
	{ 
		return ! _tcsicmp(s1.c_str(), s2.c_str()); 
	}

inline tstring	Upper(const tstring& str)
	{	
		tchar* up = _tcsupr(_tcsdup(str.c_str()));
		tstring s(up);
		::free(up);
		return s; 
	}

inline tstring	Lower(const tstring& str)
	{	
		tchar* up = _tcslwr(_tcsdup(str.c_str()));
		tstring s(up);
		::free(up);
		return s; 
	}




/**********************************************************************
*                       -= GlobalLocalWrapper =-
*	Description : ������ ������� ��� ���������� ������� - �������������
*				  � ������������ � ��������������� �������� � �����������
**********************************************************************/
class GlobalLocalWrapper
{
public:
	//.ctor
	explicit GlobalLocalWrapper( const char* local_name = "" ) // ��� ������: � ���� "����[_����[.���]]"
		: m_old_name( std::locale().name() )
	{
		try
		{
			std::locale::global( std::locale( local_name ) );
		}
		catch(...){ restore(); }
	}

	//.dtor
	~GlobalLocalWrapper(){ restore(); }

	const char* GetPrevLocalName() const { return m_old_name.c_str(); }

private:
	void restore()
	{
		std::locale::global( std::locale( m_old_name.c_str() ) );
	}
private:
	std::string	m_old_name;

}; // GlobalLocalWrapper


/**********************************************************************
*                             -= CnvA2U =-
*	Description : multibyte string -> unicode string
**********************************************************************/
class CnvA2U
{
public:
	//.ctor
	CnvA2U( std::locale const& loc = std::locale() ) 
		: m_str( L"" )
		, m_loc( loc ){}

	explicit CnvA2U	( const char* locale_name )	// ��� ������: � ���� "����[_����[.���]]"
		: m_str( L"" )							// �������� ���������������� ���������� ������ (��� ����������� ��������� const char*)
		, m_loc( !locale_name ? std::locale() : std::locale( locale_name ) ){}

	std::locale const& GetLocale()const			{ return m_loc; }
	std::string GetLocaleName()const			{ return m_loc.name(); }

	void SetLocale( std::locale const& loc )	{ m_loc = loc; }
	void SetLocale( const char* locale_name )	{ m_loc = std::locale( locale_name ); }


	std::wstring to_stdu( std::string const&	in )
	{
		return to_u( in, m_loc );
	}

	std::wstring to_stdu( std::wstring const&	in )
	{
		return in;
	}

	std::wstring to_stdu( std::string const&	in 
						, std::locale const&	loc )
	{
		return to_u( in, loc );
	}

	std::wstring to_stdu( std::wstring const&	in 
						, std::locale const&	/*loc*/ )
	{
		return in;
	}

	std::wstring to_stdu( char const*			in )
	{
		std::string temp( in );
		return to_u( temp, m_loc );
	}

	std::wstring to_stdu( char const*			in 
						, std::locale const&	loc )
	{
		std::string temp( in );
		return to_u( temp, loc );
	}

	std::wstring to_stdu( wchar_t const*		in )
	{
		return std::wstring(in);
	}

	std::wstring to_stdu( wchar_t const*		in 
						, std::locale const&	/*loc*/ )
	{
		return std::wstring(in);
	}

	wchar_t const* to_lpu( std::string const&	in )
	{
		return to_u( in, m_loc ).c_str();
	}

	wchar_t const* to_lpu( std::wstring const&	in )
	{
		return in.c_str();
	}

	wchar_t const* to_lpu( std::string const&	in 
						 , std::locale const&	loc )
	{
		return to_u( in, loc ).c_str();
	}

	wchar_t const* to_lpu( std::wstring const&	in 
						 , std::locale const&	/*loc*/ )
	{
		return in.c_str();
	}

	wchar_t const* to_lpu( char const*			in )
	{
		std::string temp( in );
		return to_u( temp, m_loc ).c_str();
	}

	wchar_t const* to_lpu( char const*			in 
						 , std::locale const&	loc )
	{
		std::string temp( in );
		return to_u( temp, loc ).c_str();
	}

	wchar_t const* to_lpu( wchar_t const*		in )
	{
		return in;
	}

	wchar_t const* to_lpu( wchar_t const*		in 
						 , std::locale const&	/*loc*/ )
	{
		return in;
	}

protected:

	std::wstring const& to_u( std::string const& in, std::locale const& loc )
	{
		m_str.assign( in.size(), wchar_t(0) );

		typedef std::ctype<wchar_t> ctype_t;
		const ctype_t& ct = std::use_facet<ctype_t>( loc );
		ct.widen( in.data(), in.data() + in.size(), &(*m_str.begin()));
		return m_str;
	}

private:
	std::wstring	m_str;	// �������� ���������������� ���������� ������ (��� ����������� ��������� const wchar_t*)
	std::locale		m_loc;
}; // CnvA2U
//-------------------------------------------------------------------------
/**********************************************************************
*                             -= CnvU2A =-
*	Description : unicode string -> multybyte string (shar)
**********************************************************************/
class CnvU2A
{
public:
	//.ctor
	CnvU2A	( std::locale const& loc = std::locale()
			, char	utoken = '?' )	// ����������� ��� ��������������� ������� 
		: m_str( "" )
		, m_loc( loc )
		, m_unknown_token( utoken ){}

	//.ctor
	explicit CnvU2A	( const char* locale_name	// ��� ������: � ���� "����[_����[.���]]"
					, char	utoken = '?' )		// ����������� ��� ��������������� ������� 
		: m_str( "" )							// �������� ���������������� ���������� ������ (��� ����������� ��������� const char*)
		, m_loc( !locale_name ? std::locale() : std::locale( locale_name ) )
		, m_unknown_token( utoken ){}

	std::locale const& GetLocale()const			{ return m_loc; }
	std::string GetLocaleName()const			{ return m_loc.name(); }

	void SetLocale( std::locale const& loc )	{ m_loc = loc; }
	void SetLocale( const char* locale_name )	{ m_loc = std::locale( locale_name ); }

	char GetUnknownChar()const					{ return m_unknown_token; }
	void SetUnknownChar( char utoken )			{ m_unknown_token = utoken; }


	std::string to_stda( std::wstring const&	in )
	{
		return to_a( in, m_loc );
	}

	std::string to_stda	( std::wstring const&	in 
						, std::locale const&	loc )
	{
		return to_a( in, loc );
	}

	std::string to_stda( std::string const&		in )
	{
		return in;
	}

	std::string to_stda	( std::string const&	in 
						, std::locale const&	/*loc*/ )
	{
		return in;
	}

	std::string to_stda( wchar_t const*			in )
	{
		std::wstring temp( in );
		return to_a( temp, m_loc );
	}

	std::string to_stda	( wchar_t const*		in 
						, std::locale const&	loc )
	{
		std::wstring temp( in );
		return to_a( temp, loc );
	}

	std::string to_stda( char const*			in )
	{
		return std::string( in );
	}

	std::string to_stda	( char const*			in 
						, std::locale const&	/*loc*/ )
	{
		return std::string( in );
	}


	char const* to_lpa	( std::wstring const&	in )
	{
		return to_a( in, m_loc ).c_str();
	}

	char const* to_lpa	( std::wstring const&	in 
						, std::locale const&	loc )
	{
		return to_a( in, loc ).c_str();
	}

	char const* to_lpa	( std::string const&	in )
	{
		return in.c_str();
	}

	char const* to_lpa	( std::string const&	in 
						, std::locale const&	/*loc*/ )
	{
		return in.c_str();
	}

	char const* to_lpa	( wchar_t const*		in )
	{
		std::wstring temp( in );
		return to_a( temp, m_loc ).c_str();
	}

	char const* to_lpa	( wchar_t const*		in 
						, std::locale const&	loc )
	{
		std::wstring temp( in );
		return to_a( temp, loc ).c_str();
	}

	char const* to_lpa	( char const*			in )
	{
		return in;
	}

	char const* to_lpa	( char const*			in 
						, std::locale const&	/*loc*/ )
	{
		return in;
	}

protected:

	std::string const& to_a( std::wstring const& in, std::locale const& loc )
	{
		m_str.assign( in.size(), char(0) );

		typedef std::ctype<wchar_t> ctype_t;
		const ctype_t& ct = std::use_facet<ctype_t>( loc );
		ct.narrow( in.data(), in.data() + in.size(), m_unknown_token,  &(*m_str.begin()));
		return m_str;
	}

private:
	std::string	m_str;			// �������� ���������������� ���������� ������ (��� ����������� ��������� const char*)
	std::locale	m_loc;			// ������� ������
	char		m_unknown_token;// ����������� ��� ��������������� �������  
}; // CnvA2U
//-------------------------------------------------------------------------



/**********************************************************************
*                         -= to_multibyte_std =-
*	Description : XXX -> std::string
**********************************************************************/
// <const char*> -> std::string
inline
std::string to_multibyte_std( const char * in, const char* /*locale_name*/ = 0 )	
{ 
	return (0 != in) ? in : std::string("");
}

// <const wchar_t*> -> std::string
inline
std::string to_multibyte_std( const wchar_t * in, const char* locale_name = 0 )	
{ 
	CnvU2A cvt( locale_name );
	return cvt.to_stda( in );
}

// <std::wstring> -> std::string
inline
std::string to_multibyte_std( std::wstring const& in, const char* locale_name = 0 )
{ 
	CnvU2A cvt( locale_name );
	return cvt.to_stda( in );
}

// <std::string> -> std::string
inline
std::string to_multibyte_std( std::string const& in, const char* /*locale_name*/ = 0 )	
{ 
	return in;
}
//-------------------------------------------------------------------------

#if _MSC_VER >= 1000
	/**********************************************************************
	*                         -= to_CString =-
	*	Description : XXX -> const char*
	**********************************************************************/
	// ��� <const char*> -> CString
	inline
	CString to_CString( const char * in, const char* locale_name = 0 )	
	{ 
		CString strTemp;
#ifdef UNICODE
		CnvA2U cvt( locale_name );
		strTemp = cvt.to_lpu( in );
#else
		locale_name;
		strTemp = in;
#endif
		return strTemp;
	}

	// <const wchar_t*> -> CString
	inline
	CString to_CString( const wchar_t * in, const char* locale_name = 0 )	
	{ 
		CString strTemp;
#ifdef UNICODE
		locale_name;
		strTemp = in;
#else
		CnvU2A cvt(locale_name);
		strTemp = cvt.to_lpa( in );		
#endif
		return strTemp;
	}

	// <std::wstring> -> CString
	inline
	CString to_CString( std::wstring const& in, const char* /*locale_name*/ = 0 )	
	{ 
		return to_CString( in.c_str() );
	}

	// <std::string> -> CString
	inline
	CString to_CString( std::string const& in, const char* /*locale_name*/ = 0 )	
	{ 
		return to_CString( in.c_str() );
	}
#endif // _MSC_VER >= 1000

	
/**********************************************************************
*                         -= to_unicode_std =-
*	Description : XXX -> std::wstring
**********************************************************************/
// <const char*> -> std::wstring
inline
std::wstring to_unicode_std( const char * in, const char* locale_name = 0 )
{ 
	CnvA2U cvt( locale_name );
	return cvt.to_stdu( in );
}

// <const wchar_t*> -> std::wstring
inline
std::wstring to_unicode_std( const wchar_t * in, const char* /*locale_name*/ = 0 )
{ 
	return (0 != in) ? in : std::wstring(L"");
}

// <std::wstring> -> std::wstring
inline
std::wstring to_unicode_std( std::wstring const& in, const char* /*locale_name*/ = 0 )
{ 
	return in;
}

// <std::string> -> std::wstring
inline
std::wstring to_unicode_std( std::string const& in, const char* locale_name = 0 )
{ 
	CnvA2U cvt( locale_name );
	return cvt.to_stdu( in );
}
//-------------------------------------------------------------------------


/**********************************************************************
*                         -= to_tstring =-
*	Description : XXX -> to_tstring
**********************************************************************/
// <const char*> -> tstring
inline
tstring to_tstring( const char * in, const char* locale_name = 0 )	
{ 
	tstring str;
#ifdef UNICODE
	CnvA2U cvt( locale_name );
	str = cvt.to_stdu( in );
#else
	locale_name;
	str = in;
#endif
	return str;
}

// <const wchar_t*> -> tstring
inline
tstring to_tstring( const wchar_t * in, const char* locale_name = 0 )
{ 
	tstring str;
#ifdef UNICODE
	locale_name;
	str = in;
#else
	CnvU2A cvt( locale_name );
	str = cvt.to_stda( in );		
#endif
	return str;
}

// <std::wstring> -> tstring
inline
tstring to_tstring( std::wstring const& in, const char* locale_name = 0 )	
{ 
	return to_tstring( in.c_str(), locale_name );
}

// <std::string> -> tstring
inline
tstring to_tstring( std::string const& in, const char* locale_name = 0 )
{ 
	return to_tstring( in.c_str(), locale_name );
}
//-------------------------------------------------------------------------





} //namespace a2w

#endif // _CHAR_STRING_TO_UNICODE_STRING_DEFINED_H__
