/* base template class */

#ifndef __SINGLETON_H_
#define __SINGLETON_H_

namespace nstr
{
	template <typename T>
	class Singleton
	{
	private:
		static T*	pSelf;
		static int	iRefCount;

	protected:

		Singleton() {}
		virtual ~Singleton() throw() 
		{
			delete pSelf;
			pSelf = NULL;
		}

	public:
		static T* Instance();
		void FreeInstance();
	};

	template <typename T>
	T* Singleton<T>::pSelf = NULL;

	template <typename T>
	int Singleton<T>::iRefCount = 0;

	template <typename T>
	T* Singleton<T>::Instance()
	{
		if(!pSelf)
			pSelf = new T();
		    
		iRefCount++;
		return pSelf;
	}

	template <typename T>
	void Singleton<T>::FreeInstance()
	{
		if(--iRefCount == 0)
			delete this;
	}
}

#endif