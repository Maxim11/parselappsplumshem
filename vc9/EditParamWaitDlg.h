#ifndef _EDIT_PARAM_WAIT_DLG_H_
#define _EDIT_PARAM_WAIT_DLG_H_

#pragma once

#include "WaitingDlg.h"
#include "ProjectData.h"

///////////////////////////////////////////////////////////////////////////////////////////////////
// ������ ��������� �������� ���������� �������������� �������

const int C_MODE_PARAM_WAIT_CREATE	= 0;
const int C_MODE_PARAM_WAIT_EDIT	= 1;

class CEditParamWaitDlg : public CWaitingDlg
{
	DECLARE_DYNAMIC(CEditParamWaitDlg)

public:

	CEditParamWaitDlg(CProjectData* prj, bool mode, CParamData* newpar, CParamData* oldpar, CWnd* pParent = NULL);	// �����������
	virtual ~CEditParamWaitDlg();																					// ����������

	enum { IDD = CWaitingDlg::IDD };	// ������������� �������

protected:

	CProjectData* PrjData;	// ��������� �� ������, � ������� ������������ �����������

	CParamData* NewParam;
	CParamData* OldParam;

	int Mode;

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();						// ������������� �������

	virtual void OK();				// ��������� �������� ����� �� �������
	virtual void Stop();			// ��������� ����������� ����� �� �������
	virtual void Work();			// ������ �������� �������

	DECLARE_MESSAGE_MAP()
};

#endif
