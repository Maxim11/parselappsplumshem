#ifndef _PARCEL_SAMPLE_DATA_H_
#define _PARCEL_SAMPLE_DATA_H_

#pragma once

#include "PRDBProject.h"


// ��������� ����������� ������

struct ReferenceData
{
	CString Name;	// ��� ����������
	double Value;	// �������� ����������
	bool bNoValue;	// ������� ���������� ������

	ReferenceData(CString name):Name(name),Value(0.),bNoValue(true)
	{};
	void Copy(ReferenceData& ref);

};

typedef vector<ReferenceData> VRef;
typedef vector<ReferenceData>::iterator ItRef;

// ��������� ������������ ������

struct Spectrum
{
	int Num;			// ����� �������
	bool bUse;			// ����, ������������, ������������ �� ������
	CTime DateTime;		// ����� �����������
	VDbl Data;			// ������������ ������

	Spectrum()
	{};
	~Spectrum()
	{
		Data.clear();
	};
	void Copy(Spectrum& spec);
	CString IsUseStr();

};

typedef vector<Spectrum> VSpc;
typedef vector<Spectrum>::iterator ItSpc;


///////////////////////////////////////////////////////////////////////////////////////////////////
// ����� ��� �������� �������

class CSampleData
{
public:

	CSampleData(HTREEITEM hParent, HTREEITEM hTrans);			// �����������
	~CSampleData();												// ����������

	void Copy(CSampleData& data, int withoutspectra = false);	// ����������� ������

protected:

	HTREEITEM hParentItem;				// ������ ���� � ������, ���������������� ������������� �������
	HTREEITEM hTransItem;				// ������ ���� � ������, ��������������� ������ ��������

	int DevNumber;						// ����� ������������� �������
	CString PrjName;					// ��� ������������� �������

	CString Name;						// ��� �������

	CString Iden2;						// ������������� 2 (���� ���������� ���)
	CString Iden3;						// ������������� 3 (���� ���������� ���)

public:

	VRef RefData;						// ������ ����������� ������

	VSpc SpecData;						// ������ �������� (�������� ������ ��� ��������� � �������
										// � ������������� ��� ������ ����������� (��-�� �������� ������ ������))

public:

	HTREEITEM GetParentHItem();						// �������� ������ ���� � ������, ��������������� �������
	void SetParentHItem(HTREEITEM item);

	HTREEITEM GetTransHItem();						// �������� ������ ���� � ������, ��������������� ������
	void SetHTransItem(HTREEITEM item);

	CString GetName();								// �������� ��� �������
	void SetName(CString name);						// ���������� ��� �������

	CString GetIden2();								// �������� ������������� 2
	void SetIden2(CString str);						// ���������� ������������� 2

	CString GetIden3();								// �������� ������������� 3
	void SetIden3(CString str);						// ���������� ������������� 3

	int GetNReferenceData();						// �������� ����� ����������� ������
	ReferenceData* GetReferenceData(CString name);	// �������� ����������� ������ �� ����� ����������
	ReferenceData* GetReferenceData(int ind);		// �������� ����������� ������ �� ������� � ������
	CString GetReferenceDataStr(CString name);		// �������� ����������� ������ �� ����� ���������� ��� ������
	CString GetReferenceDataStr(int ind);			// �������� ����������� ������ �� ������� � ������ ��� ������
	bool AddReferenceData(CString name);				// �������� ����������� ������
	bool SetReferenceValue(CString name, double value);	// ���������� �������� ������������ �������
	bool SetReferenceValue(int ind, double value);	// ���������� �������� ������������ �������
	bool SetReferenceAsNoValue(CString name);			// ���������� ������� ���������� ������
	bool SetReferenceAsNoValue(int ind);			// ���������� ������� ���������� ������
	bool DeleteReferenceData(CString name);				// ������� ����������� ������ �� ����� ����������
	bool DeleteReferenceData(int ind);					// ������� ����������� ������ �� ������� � ������

	int GetNSpectra();									// �������� ����� ��������
	int GetNSpectraUsed();								// �������� ����� ������������ ��������
	bool IsSpectrumUsed(int num);						// �������� ������� bUse �� ������
	Spectrum* GetSpectrumByNum(int num);				// �������� ������ �� ������
	Spectrum* GetSpectrumByInd(int ind);				// �������� ������ �� ������� � ������
	bool AddSpectrum(Spectrum* Spec);
	bool DeleteSpectrum(int num);
	int GetSpecNumMax();
	bool IsSpecDateExist(CTime date);
	CTime GetLastSpecDate();						// �������� ���� ���������� ��������� �������

	void GetDataForBase(prdb::PRSample& Obj);
	void SetDataFromBase(prdb::PRSample& Obj);

	void GetRefDataForBase(CString name, prdb::PRRefData& Obj);
	void GetSpecDataForBase(prdb::PRSpectrum& Obj);

	bool LoadDatabase();
//	bool LoadSpectraInfo();
	bool LoadSpectraInfo(int nSampleID);
	bool LoadSpectraData(VInt* nums = NULL);
	void FreeSpectraData();
};

struct LoadSampleInfo
{
	CSampleData* pSam;
	VInt Nums;
};

typedef vector<LoadSampleInfo> VLSI;
typedef vector<LoadSampleInfo>::iterator ItLSI;

#endif
