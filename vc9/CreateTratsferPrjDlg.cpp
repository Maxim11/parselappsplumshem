// CreateTratsferPrjDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Parcel.h"
#include "MainFrm.h"
#include "RootData.h"
#include "BranchInfo.h"

#include "ParcelView.h"
#include "CreateTratsferPrjDlg.h"

#include "CreateTransferAlone.h"
#include "CreateTransferAloneV2.h"

#include "CreateModelDlg.h"
#include "SaveModelWaitDlg.h"

// CCreateTratsferPrjDlg dialog

IMPLEMENT_DYNAMIC(CCreateTratsferPrjDlg, CDialog)

CCreateTratsferPrjDlg::CCreateTratsferPrjDlg(HTREEITEM item, CWnd* pParent /*=NULL*/)
	: CDialog(CCreateTratsferPrjDlg::IDD, pParent)
{
	pPrjSlave = GetProjectByHItem(item);
	pDevM = NULL;
	pPrjM = NULL;
	pTransNew = NULL;
}

CCreateTratsferPrjDlg::~CCreateTratsferPrjDlg()
{
}

void CCreateTratsferPrjDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PROMPT_DEVICE, m_promptDevice);
	DDX_Control(pDX, IDC_PROMPT_PROJECT, m_promptProject);
	DDX_Control(pDX, IDC_COMBO_DEVICE, m_comboDevice);
	DDX_Control(pDX, IDC_COMBO_PROJECT, m_comboProject);
//
	DDX_Control(pDX, IDC_PROMPT_MOD1, m_promptModAll);
	DDX_Control(pDX, IDC_LIST_MODELS1, m_listModAll);
	DDX_Control(pDX, IDC_PROMPT_MOD2, m_promptModSel);
	DDX_Control(pDX, IDC_LIST_MODELS2, m_listModSel);
	DDX_Control(pDX, IDC_BUTTON_MOD_ADD, m_butModAdd);
	DDX_Control(pDX, IDC_BUTTON_MOD_ADD_ALL, m_butModAddAll);
	DDX_Control(pDX, IDC_BUTTON_MOD_DEL, m_butModDel);
	DDX_Control(pDX, IDC_BUTTON_MOD_DEL_ALL, m_butModDelAll);
//
	DDX_Control(pDX, IDC_PROMPT_MET1, m_promptMetAll);
	DDX_Control(pDX, IDC_LIST_METHODS1, m_listMetAll);
	DDX_Control(pDX, IDC_PROMPT_MET2, m_promptMetSel);
	DDX_Control(pDX, IDC_LIST_METHODS2, m_listMetSel);
	DDX_Control(pDX, IDC_BUTTON_MET_ADD, m_butMetAdd);
	DDX_Control(pDX, IDC_BUTTON_MET_ADD_ALL, m_butMetAddAll);
	DDX_Control(pDX, IDC_BUTTON_MET_DEL, m_butMetDel);
	DDX_Control(pDX, IDC_BUTTON_MET_DEL_ALL, m_butMetDelAll);
//
	DDX_Control(pDX, IDC_BUTTON_CREATE, m_butCreate);
	DDX_Control(pDX, IDHELP, m_butHelp);
	DDX_Control(pDX, IDCANCEL, m_butClose);
}


BEGIN_MESSAGE_MAP(CCreateTratsferPrjDlg, CDialog)
	ON_CBN_SELCHANGE(IDC_COMBO_DEVICE, &CCreateTratsferPrjDlg::OnCbnSelchangeComboDevice)
	ON_CBN_SELCHANGE(IDC_COMBO_PROJECT, &CCreateTratsferPrjDlg::OnCbnSelchangeComboProject)
	ON_BN_CLICKED(IDC_BUTTON_MOD_ADD, &CCreateTratsferPrjDlg::OnBnClickedButtonModAdd)
	ON_BN_CLICKED(IDC_BUTTON_MOD_ADD_ALL, &CCreateTratsferPrjDlg::OnBnClickedButtonModAddAll)
	ON_BN_CLICKED(IDC_BUTTON_MOD_DEL, &CCreateTratsferPrjDlg::OnBnClickedButtonModDel)
	ON_BN_CLICKED(IDC_BUTTON_MOD_DEL_ALL, &CCreateTratsferPrjDlg::OnBnClickedButtonModDelAll)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_MODELS1, &CCreateTratsferPrjDlg::OnLvnItemchangedListModelsAll)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_MODELS2, &CCreateTratsferPrjDlg::OnLvnItemchangedListModelsSel)
	ON_BN_CLICKED(IDC_BUTTON_MET_ADD, &CCreateTratsferPrjDlg::OnBnClickedButtonMetAdd)
	ON_BN_CLICKED(IDC_BUTTON_MET_ADD_ALL, &CCreateTratsferPrjDlg::OnBnClickedButtonMetAddAll)
	ON_BN_CLICKED(IDC_BUTTON_MET_DEL, &CCreateTratsferPrjDlg::OnBnClickedButtonMetDel)
	ON_BN_CLICKED(IDC_BUTTON_MET_DEL_ALL, &CCreateTratsferPrjDlg::OnBnClickedButtonMetDelAll)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_METHODS1, &CCreateTratsferPrjDlg::OnLvnItemchangedListMethodsAll)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_METHODS2, &CCreateTratsferPrjDlg::OnLvnItemchangedListMethodsSel)
	ON_BN_CLICKED(IDC_BUTTON_CREATE, &CCreateTratsferPrjDlg::OnBnClickedButtonCreate)
	ON_BN_CLICKED(IDHELP, &CCreateTratsferPrjDlg::OnBnClickedHelp)
END_MESSAGE_MAP()


void CCreateTratsferPrjDlg::InitControls()
{
	m_promptDevice.SetWindowText(GETSTRUI(1145));
	m_promptProject.SetWindowText(GETSTRUI(1146));

	m_promptModAll.SetWindowText(GETSTRUI(1147));
	m_promptModSel.SetWindowText(GETSTRUI(1148));

	m_promptMetAll.SetWindowText(GETSTRUI(1160));
	m_promptMetSel.SetWindowText(GETSTRUI(1161));

	m_butModAdd.SetWindowText(GETSTRUI(1152));
	m_butModAddAll.SetWindowText(GETSTRUI(1153));
	m_butModDel.SetWindowText(GETSTRUI(1154));
	m_butModDelAll.SetWindowText(GETSTRUI(1155));

	m_butMetAdd.SetWindowText(GETSTRUI(1152));
	m_butMetAddAll.SetWindowText(GETSTRUI(1153));
	m_butMetDel.SetWindowText(GETSTRUI(1154));
	m_butMetDelAll.SetWindowText(GETSTRUI(1155));

	m_butCreate.SetWindowText(GETSTRUI(1156));
	m_butHelp.SetWindowText(GETSTRUI(1157));
	m_butClose.SetWindowText(GETSTRUI(1158));

	CRect rect;
	m_listModAll.GetClientRect(&rect);
	m_listModAll.InsertColumn(0, GETSTRUI(1149), LVCFMT_CENTER, 22);
	m_listModAll.InsertColumn(1, GETSTRUI(1150), LVCFMT_LEFT, rect.Width() - 22);
	m_listModAll.SetExtendedStyle(LVS_EX_FLATSB | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

	m_listModSel.GetClientRect(&rect);
	m_listModSel.InsertColumn(0, GETSTRUI(1149), LVCFMT_CENTER, 22);
	m_listModSel.InsertColumn(1, GETSTRUI(1150), LVCFMT_LEFT, rect.Width() - 22);
	m_listModSel.SetExtendedStyle(LVS_EX_FLATSB | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

	m_listMetAll.GetClientRect(&rect);
	m_listMetAll.InsertColumn(0, GETSTRUI(1149), LVCFMT_CENTER, 22);
	m_listMetAll.InsertColumn(1, GETSTRUI(1151), LVCFMT_LEFT, rect.Width() - 22);
	m_listMetAll.SetExtendedStyle(LVS_EX_FLATSB | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

	m_listMetSel.GetClientRect(&rect);
	m_listMetSel.InsertColumn(0, GETSTRUI(1149), LVCFMT_CENTER, 22);
	m_listMetSel.InsertColumn(1, GETSTRUI(1151), LVCFMT_LEFT, rect.Width() - 22);
	m_listMetSel.SetExtendedStyle(LVS_EX_FLATSB | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

}

// CCreateTratsferPrjDlg message handlers

BOOL CCreateTratsferPrjDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	SetWindowText(GETSTRUI(1144));
	InitControls();

	if(!FillComboDevice()) EndDialog(IDCANCEL);

	FillComboProject();


	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}


// device list
int CCreateTratsferPrjDlg::FillComboDevice()
{
	pDevM = 0;
	iDevM = -1;

	// creating valid device list
	vDevNames.clear();
	ItDev itD;
	for(itD = pRoot->Devices.begin(); itD != pRoot->Devices.end(); itD++)
	{
		if(itD->GetHItem() == pPrjSlave->GetParentHItem())
		{
			strDevSlave.Format(L"%08d", itD->GetNumber());
			continue; // skipping the slave project
		}

		if(itD->IsReadyForTrans(pPrjSlave))
		{
			vDevNames.push_back(itD->GetName());
		}
	}

	// filling device combo
	m_comboDevice.ResetContent();
	int nDev = vDevNames.size();
	if(!nDev)
	{
		CString s = GETSTRUI(1162);
		AfxMessageBox(s, MB_ICONEXCLAMATION);
		return 0;
	}

	for(int i=0; i<nDev; i++)
	{
		m_comboDevice.AddString(vDevNames[i]);
	}

	iDevM = 0;
	m_comboDevice.SetCurSel(iDevM);
	pDevM = pRoot->GetDevice(vDevNames[iDevM]);

	return nDev;
}

// project list for selected device
int CCreateTratsferPrjDlg::FillComboProject()
{
	pPrjM = 0;
	iPrjM = -1;
	vPrjNames.clear();
	m_comboProject.ResetContent();

	if(!pDevM) return 0;


	ItPrj itP;
	for(itP = pDevM->Projects.begin(); itP != pDevM->Projects.end(); itP++)
	{
		if(itP->IsReadyForTrans(pPrjSlave))
		{
			vPrjNames.push_back(itP->GetName());
		}
	}


	int nPrj = vPrjNames.size();
	if(!nPrj)
	{
		CString s = GETSTRUI(1163);
		AfxMessageBox(s, MB_ICONEXCLAMATION);
	}
	else
	{
		for(int i=0; i<nPrj; i++)
		{
			m_comboProject.AddString(vPrjNames[i]);
		}

		iPrjM = 0;
		m_comboProject.SetCurSel(iPrjM);
		pPrjM = pDevM->GetProject(vPrjNames[iPrjM]);
	}

	FillListModAll();

	return nPrj;
}

int CCreateTratsferPrjDlg::FillListModAll()
{
	vModNamesAll.clear();
	m_listModAll.DeleteAllItems();
	int nModAll = 0;

	if(pPrjM != NULL)
	{
		ItMod itM;
		for(itM = pPrjM->Models.begin(); itM != pPrjM->Models.end(); itM++)
		{
//			if(itM->IsReadyForTrans())
			if(itM->IsReadyForTransPrj())
			{
				vModNamesAll.push_back(itM->GetName());
			}
		}


		nModAll = vModNamesAll.size();
		for(int i=0; i<nModAll; i++)
		{
			CString str;
			str.Format(_T("%d"), i+1);
			m_listModAll.InsertItem(i, str);
			str.Format(_T("%s"), vModNamesAll[i]);
			m_listModAll.SetItemText(i, 1, str);
		}
	}

//	MMod = (MPrj != NULL && m_vModel > 0) ? MPrj->GetModel(MModNames[m_vModel - 1]) : NULL;

//	clear slave model list
	vModNamesSel.clear();
	FillListModSel();

//	clear both model lists
	FillListMetAll(true);

	return nModAll;
}

int CCreateTratsferPrjDlg::FillListModSel()
{
	m_listModSel.DeleteAllItems();
	int nModSel = vModNamesSel.size();

	for(int i=0; i<nModSel; i++)
	{
		CString str;
		str.Format(_T("%d"), i+1);
		m_listModSel.InsertItem(i, str);
		str.Format(_T("%s"), vModNamesSel[i]);
		m_listModSel.SetItemText(i, 1, str);
	}
	return nModSel;
}

// ��������� ����� ������, �������� ������ ��������� �������
void CCreateTratsferPrjDlg::OnModAdd()
{
	FillListMetAll(false);
}

// ������ �������, �������� ������ ��������� � ��������� �������
void CCreateTratsferPrjDlg::OnModDel()
{
	FillListMetAll(false);

	VStr vtmp; 
	ItStr itS;
	for(itS = vMetNamesSel.begin(); itS != vMetNamesSel.end(); itS++)
	{
		CMethodData *pMet = pPrjM->GetMethod(*itS);
		bool bUsed = false;
		int nModSel = vModNamesSel.size();
		for(int i=0; i<nModSel; i++)
		{
			if((*pMet).IsQntModelUsed(vModNamesSel[i]))
			{
				bUsed = true;
				break;
			}
		}
		if(bUsed) 
		{
			vtmp.push_back(*itS);
		}
	}
	vMetNamesSel = vtmp;
	FillListMetSel();
}

int CCreateTratsferPrjDlg::FillListMetAll(bool bClearSel)
{
	vMetNamesAll.clear();
	m_listMetAll.DeleteAllItems();
	int nMetAll = 0;

	if(pPrjM != NULL)
	{
		ItMtd itM;
		for(itM = pPrjM->Methods.begin(); itM != pPrjM->Methods.end(); itM++)
		{
			bool bUsed = false;
			int nModSel = vModNamesSel.size();
			for(int i=0; i<nModSel; i++)
			{
				if((*itM).IsQntModelUsed(vModNamesSel[i]))
				{
					bUsed = true;
					break;
				}
			}
			
			if(bUsed) 
			{
				vMetNamesAll.push_back(itM->GetName());
			}
		}


		nMetAll = vMetNamesAll.size();
		for(int i=0; i<nMetAll; i++)
		{
			CString str;
			str.Format(_T("%d"), i+1);
			m_listMetAll.InsertItem(i, str);
			str.Format(_T("%s"), vMetNamesAll[i]);
			m_listMetAll.SetItemText(i, 1, str);
		}
	}

//	clear slave method list
	if(bClearSel)
	{
		vMetNamesSel.clear();
		FillListMetSel();
	}

	return nMetAll;
}

int CCreateTratsferPrjDlg::FillListMetSel()
{
	m_listMetSel.DeleteAllItems();
	int nMetSel = vMetNamesSel.size();

	for(int i=0; i<nMetSel; i++)
	{
		CString str;
		str.Format(_T("%d"), i+1);
		m_listMetSel.InsertItem(i, str);
		str.Format(_T("%s"), vMetNamesSel[i]);
		m_listMetSel.SetItemText(i, 1, str);
	}
	return nMetSel;
}


void CCreateTratsferPrjDlg::OnCbnSelchangeComboDevice()
{
	int iSel = m_comboDevice.GetCurSel();
	if(iSel == iDevM || iSel == -1)
	{
		return;
	}

	iDevM = iSel;
	pDevM = pRoot->GetDevice(vDevNames[iDevM]);

	FillComboProject();
}

void CCreateTratsferPrjDlg::OnCbnSelchangeComboProject()
{
	int iSel = m_comboProject.GetCurSel();
	if(iSel == iPrjM || iSel == -1)
	{
		return;
	}

	iPrjM = iSel;
	pPrjM = pDevM->GetProject(vPrjNames[iPrjM]);

	FillListModAll();
}

void CCreateTratsferPrjDlg::OnBnClickedButtonModAdd()
{
	int idx = -1; 
	int nsel = m_listModAll.GetSelectedCount();

	if(nsel <= 0) return;

	int nModAll = vModNamesAll.size();
	for(int i=0; i<nsel; i++)
	{
		idx = m_listModAll.GetNextItem(idx, LVNI_SELECTED);
		if(idx < 0) break;
		if(idx >= nModAll) break;

		CString strName = vModNamesAll[idx];

		bool bFound = false;
		for(int j=0; j<vModNamesSel.size(); j++)
		{
			if(vModNamesSel[j].Compare(strName) == 0) 
			{
				bFound = true;
				break;
			}
		}

		if(!bFound)
		{
			vModNamesSel.push_back(strName);
		}
	}

	FillListModSel();
	OnModAdd();
}

void CCreateTratsferPrjDlg::OnBnClickedButtonModAddAll()
{
	vModNamesSel.clear();
	int nModAll = vModNamesAll.size();
	for(int i=0; i<nModAll; i++)
	{
		vModNamesSel.push_back(vModNamesAll[i]);
	}
	FillListModSel();
	OnModAdd();
}

void CCreateTratsferPrjDlg::OnBnClickedButtonModDel()
{
	int nsel = m_listModSel.GetSelectedCount();
	if(nsel <= 0) return;

	int idx = m_listModSel.GetNextItem(-1, LVNI_SELECTED);

	int i1 = idx;
	int i2 = i1+1;

	for(int i=0; i<nsel; i++)
	{
		idx = m_listModSel.GetNextItem(idx, LVNI_SELECTED);
		if(idx < 0) break;
		i2++;
	}
	vModNamesSel.erase(vModNamesSel.begin()+i1, vModNamesSel.begin()+i2);
	FillListModSel();
	OnModDel();
}

void CCreateTratsferPrjDlg::OnBnClickedButtonModDelAll()
{
	vModNamesSel.clear();
	FillListModSel();
	OnModDel();
}


void CCreateTratsferPrjDlg::OnLvnItemchangedListModelsAll(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;
}

void CCreateTratsferPrjDlg::OnLvnItemchangedListModelsSel(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;
}

void CCreateTratsferPrjDlg::OnBnClickedButtonMetAdd()
{
	int idx = -1; 
	int nsel = m_listMetAll.GetSelectedCount();

	if(nsel <= 0) return;

	int nMetAll = vMetNamesAll.size();
	for(int i=0; i<nsel; i++)
	{
		idx = m_listMetAll.GetNextItem(idx, LVNI_SELECTED);
		if(idx < 0) break;
		if(idx >= nMetAll) break;

		CString strName = vMetNamesAll[idx];

		bool bFound = false;
		for(int j=0; j<vMetNamesSel.size(); j++)
		{
			if(vMetNamesSel[j].Compare(strName) == 0) 
			{
				bFound = true;
				break;
			}
		}

		if(!bFound)
		{
			vMetNamesSel.push_back(strName);
		}
	}

	FillListMetSel();
}

void CCreateTratsferPrjDlg::OnBnClickedButtonMetAddAll()
{
	vMetNamesSel.clear();
	int nMetAll = vMetNamesAll.size();
	for(int i=0; i<nMetAll; i++)
	{
		vMetNamesSel.push_back(vMetNamesAll[i]);
	}
	FillListMetSel();
}

void CCreateTratsferPrjDlg::OnBnClickedButtonMetDel()
{
	int nsel = m_listMetSel.GetSelectedCount();
	if(nsel <= 0) return;

	int idx = m_listMetSel.GetNextItem(-1, LVNI_SELECTED);

	int i1 = idx;
	int i2 = i1+1;

	for(int i=0; i<nsel; i++)
	{
		idx = m_listMetSel.GetNextItem(idx, LVNI_SELECTED);
		if(idx < 0) break;
		i2++;
	}
	vMetNamesSel.erase(vMetNamesSel.begin()+i1, vMetNamesSel.begin()+i2);
	FillListMetSel();
}

void CCreateTratsferPrjDlg::OnBnClickedButtonMetDelAll()
{
	vMetNamesSel.clear();
	FillListMetSel();
}

void CCreateTratsferPrjDlg::OnLvnItemchangedListMethodsAll(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;
}

void CCreateTratsferPrjDlg::OnLvnItemchangedListMethodsSel(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;
}
bool CCreateTratsferPrjDlg::ValidateSelectedModels()
{
	int nModSel = vModNamesSel.size();
	for(int i=0; i<nModSel; i++)
	{
		if(NULL != pPrjSlave->GetModel(vModNamesSel[i]))
		{
			CString str;
			str.Format(GETSTRUI(1165), strDevSlave, vModNamesSel[i]);
			AfxMessageBox(str);
			return false;
		}
	}
	return true;
}

bool CCreateTratsferPrjDlg::ValidateSelectedMethods()
{
	int nModSel = vModNamesSel.size();
	int nMetSel = vMetNamesSel.size();
	for(int i=0; i<nMetSel; i++)
	{
		if(NULL != pPrjSlave->GetMethod(vMetNamesSel[i]))
		{
			CString str;
			str.Format(GETSTRUI(1166), strDevSlave, vMetNamesSel[i]);
			AfxMessageBox(str);
			return false;
		}

		CMethodData *pMet = pPrjM->GetMethod(vMetNamesSel[i]);
		VStr names;
		int n = pMet->GetQntModelNames(names);
		for(int k=0; k<n; k++)
		{
			bool bFound=false; 
			for(int j=0; j<nModSel; j++)
			{
				if(!names[k].Compare(vModNamesSel[j]))
				{
					bFound = true;
					break;
				}
			}
			if(!bFound)
			{
				CString str;
				str.Format(GETSTRUI(1174), names[k], vMetNamesSel[i]); 
				AfxMessageBox(str);
				return false;
			}
		}
	}
	return true;
}

void CCreateTratsferPrjDlg::OnBnClickedButtonCreate()
{
	if(!ValidateSelectedModels()) return;
	if(!ValidateSelectedMethods()) return;

	if(!TransferSamples())
	{
		AfxMessageBox(GETSTRUI(1171));
		return;
	}

	if(!TransferSamplesV2())
	{
		AfxMessageBox(GETSTRUI(1171));
		return;
	}

	if(!TransferModels())
	{
		AfxMessageBox(GETSTRUI(1172));
		return;
	}

	if(!TransferMethods())
	{
		AfxMessageBox(GETSTRUI(1173));
		return;
	}

	EndDialog(IDOK);
}


// ������� ������� ������
bool CCreateTratsferPrjDlg::TransferSamples()
{
	if(!pDevM) return false;
	if(!pPrjM) return false;

//	check for existing transfer
	CString strName;
	strName.Format(L"%08d", pDevM->GetNumber());

	bool bNewTrans = true;
	pTransNew = pPrjSlave->GetTransfer(strName);
	if(pTransNew)
	{
		CString str;
		str.Format(GETSTRUI(1170), strDevSlave, strName); 
		AfxMessageBox(str);
		bNewTrans = false;
	}

//	create new
	CTransferData NewTrans(pPrjSlave->GetHItem());
	CCreateTransferAlone cta(&NewTrans, pDevM, pPrjM);
	if(cta.Create(strName))
	{
		if(bNewTrans)
		{
			pTransNew = pPrjSlave->GetLastTransfer();
			pView->AddTransfer(pTransNew);
		}
		return true;
	}

	return false;
}

// ������� �������� �������
bool CCreateTratsferPrjDlg::TransferSamplesV2()
{
	if(!pDevM) return false;
	if(!pPrjM) return false;

	bool bRet = true;

	int nTrans = pPrjM->GetNTransfers();

	for(int i=0; i<nTrans; i++)
	{
		CTransferData *pTransM = pPrjM->GetTransfer(i);
		if(pTransM == pPrjM->GetTransOwn()) continue;

		//	check for existing transfer
		CString strName = pTransM->GetName();

		bool bNewTrans = true;
		CTransferData *pTrans = pPrjSlave->GetTransfer(strName);
		if(pTrans)
		{
			CString str;
			str.Format(GETSTRUI(1170), strDevSlave, strName); 
			AfxMessageBox(str);
			bNewTrans = false;
		}

	//	create new
		CTransferData NewTrans(pPrjSlave->GetHItem());
		CCreateTransferAloneV2 ctaV2(&NewTrans, pDevM, pPrjM, i);
		if(ctaV2.Create())
		{
			if(bNewTrans)
			{
				pTrans = pPrjSlave->GetLastTransfer();
				pView->AddTransfer(pTrans);
			}
		}
		else
		{
			bRet = false;
			break;
		}
	}

	return bRet;
}

bool CCreateTratsferPrjDlg::TransferModels()
{
	int nModSel = vModNamesSel.size();
	for(int i=0; i<nModSel; i++)
	{
		CModelData* pOldModel = pPrjM->GetModel(vModNamesSel[i]);
		CModelData* pNewModel = TransferSingleModel(pOldModel);
		if(pNewModel == NULL) return false;
		pView->AddNewModel(pNewModel);
	}
	return true;
}

CModelData* CCreateTratsferPrjDlg::TransferSingleModel(CModelData* pOldMod)
{
	CString NewTransName = pTransNew->GetName();

	CModelData NewMod(pPrjSlave->GetHItem());
	NewMod.Copy(*pOldMod, true, NewTransName);
	NewMod.SetParentHItem(pPrjSlave->GetHItem());

//	NewMod.SetTransName2(strNewTrans);

	NewMod.SetDate();

	CSaveModelWaitDlg dlg2(pPrjSlave, &NewMod, NULL);
	if(dlg2.DoModal() != IDOK)
	{
		return NULL;
	}
	return pPrjSlave->GetLastModel();
}

bool CCreateTratsferPrjDlg::TransferMethods()
{
	int nMetSel = vMetNamesSel.size();
	for(int i=0; i<nMetSel; i++)
	{
		CMethodData* pOldMethod = pPrjM->GetMethod(vMetNamesSel[i]);
		CMethodData* pNewMethod = TransferSingleMethod(pOldMethod);
		if(pNewMethod == NULL) return false;
		pView->InsertNewMethod(pNewMethod);
	}
	return true;
}

CMethodData* CCreateTratsferPrjDlg::TransferSingleMethod(CMethodData* pOldMet)
{
	CMethodData NewMet(pPrjSlave->GetHItem());
	NewMet.Copy(*pOldMet);
	NewMet.SetParentHItem(pPrjSlave->GetHItem());
	NewMet.SetDate();
	return pPrjSlave->AddNewMethod(&NewMet);
}


void CCreateTratsferPrjDlg::OnBnClickedHelp()
{
	int IdHelp = DLGID_PROJECT_TRANSFER;

	pFrame->ShowHelp(IdHelp);
}

void CCreateTratsferPrjDlg::OnOK()
{
//	CDialog::OnOK();
}

void CCreateTratsferPrjDlg::OnCancel()
{
	CDialog::OnCancel();
}
