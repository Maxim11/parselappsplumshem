#ifndef _CREATE_SAMPLE_DLG_H_
#define _CREATE_SAMPLE_DLG_H_

#pragma once

#include "Resource.h"
#include "SampleData.h"

// ��������� ������ ������ �������

const int SAMDLG_MODE_CREATE = 0;		// ����� �������� ������ �������
const int SAMDLG_MODE_CHANGE = 1;		// ����� �������������� �������
const int SAMDLG_MODE_SHOW = 2;			// ����� ��������� �������

///////////////////////////////////////////////////////////////////////////////////////////////////
// ������ ��������/�������������� �������

class CCreateSampleDlg : public CDialog
{
	DECLARE_DYNAMIC(CCreateSampleDlg)

public:

	CCreateSampleDlg(CSampleData* data, int mode = SAMDLG_MODE_CREATE, CWnd* pParent = NULL);	// �����������
	virtual ~CCreateSampleDlg();																// ����������

	enum { IDD = IDD_CREATE_SAMPLE };	// ������������� �������

	CListCtrl m_listRef;		// ������ ����������� ������
	CListCtrl m_listSpec;		// ������ ��������
	CEdit m_Name;				// ���� ��� �������������� "����� �������"
	CEdit m_Iden2;				// ���� ��� �������������� "�������������� 2 �������"
	CEdit m_Iden3;				// ���� ��� �������������� "�������������� 3 �������"
	CEdit m_Value;				// ���� ��� �������������� "�������� ������������ �������"
	CButton	m_NoValue;			// ��������� "�������� ���������� ������"

protected:

	CSampleData* SamData;		// ��������� �� ������������� �������
	CProjectData* ParentPrj;	// ��������� �� ������, ������������ ��� �������

	ItRef CurRef;				// ��������� �� ������� ����������� ������
	ItRef SelRef;
	ItSpc CurSpec;				// ��������� �� ������� ������

	bool IsEditRef;

	int Mode;					// ����� ������

	int NewSpecNum;				// ����� ��� ������ �������

	CString m_vName;			// ��������� �������� "����� ����������"
	CString m_vIden2;			// ��������� �������� "�������������� 2 �������"
	CString m_vIden3;			// ��������� �������� "�������������� 3 �������"
	double m_vValue;			// ��������� �������� "�������� ������������ �������"
	BOOL m_vNoValue;			// ������������� �������� "�������� ���������� ������"

	bool IsBtnClk;

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();						// ������������� �������

	virtual void OnOK();
	virtual void OnCancel();			// ��������� ������� "�������� ���� � ����� �� �������"

	afx_msg void OnAccept();			// ��������� ������� "������� ������ � ����� �� �������"
	afx_msg void OnRefDataListSelChange(NMHDR* /*pNMHDR*/, LRESULT *pResult); // ��������� ������� "�������� �������� � ������ ���. ������"
	afx_msg void OnSpectraListSelChange(NMHDR* /*pNMHDR*/, LRESULT *pResult); // ��������� ������� "�������� �������� � ������ ��������"
	afx_msg void OnChangeRefData();		// ��������� ������� "�������� ����������� ������"
	afx_msg void OnStartRefData();
	afx_msg void OnRemove();			// ��������� ������� "������� ���������� ������"
	afx_msg void OnExclude();			// ��������� ������� "���������/�������� ������"
	afx_msg void OnShowPlot();			// ��������� ������� "�������� ������ ����������� �������"
	afx_msg void OnShowText();			// ��������� ������� "�������� ��������sq ������ � ��������� ����"
	afx_msg void OnHelp();				// ��������� ������� "������� �������"

	void FillListRef();					// ���������� ������ ����������� ������
	void FillListSpec();				// ���������� ������ ��������
	void FillFields();					// ���������� ����� ����� ������

	int CalcNewSpecNum();				// ��������� ����� ������ �������

	bool IsChanged();

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
};

#endif
