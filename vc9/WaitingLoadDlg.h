#pragma once
//#include "afxwin.h"


// CWaitingLoadDlg dialog

class CWaitingLoadDlg : public CDialog
{
	DECLARE_DYNAMIC(CWaitingLoadDlg)

public:
	CWaitingLoadDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CWaitingLoadDlg();

	bool m_bDone;
	void Done() {m_bDone = true;}

// Dialog Data
	enum { IDD = IDD_WAITING1 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
protected:
	virtual void OnCancel();
	virtual void OnOK();
public:
//	CStatic m_logo;
	CBitmap m_bmp;
	CSize m_size;
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
};
