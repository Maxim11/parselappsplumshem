#pragma once 

#ifndef _LUMEX_MEMORY_DC_DEFINES_H__
#define _LUMEX_MEMORY_DC_DEFINES_H__


//------------------------------------------------------------------------------
/** 
 * \class CMemDC
 * \brief This class implements a memory Device Context
 *
 */
namespace CodeProject {
	class CMemDC : public CDC
	{

	public:

		/// .ctor
		CMemDC(CDC* pDC, const CRect& rcBounds)
			: CDC()
			, m_bitmap(new CBitmap())
			, m_oldBitmap(NULL)
			, m_pDC(pDC)
			, m_rcBounds(rcBounds)
		{
			CreateCompatibleDC(pDC);
			m_bitmap->CreateCompatibleBitmap(pDC, rcBounds.Width(), rcBounds.Height());
			m_oldBitmap = SelectObject(m_bitmap);
		}

		/// .dtor
		~CMemDC()
		{
			m_pDC->BitBlt(m_rcBounds.left, m_rcBounds.top, m_rcBounds.Width(), m_rcBounds.Height()
				, this, m_rcBounds.left, m_rcBounds.top, SRCCOPY);
			SelectObject(m_oldBitmap);
			delete m_bitmap;
		}

		/// ������
		CMemDC* operator->()
		{
			return this;
		}

	private:
		CBitmap*	m_bitmap;
		CBitmap*	m_oldBitmap;
		CDC*		m_pDC;
		CRect		m_rcBounds;

	};
}
//------------------------------------------------------------------------------

#endif // _LUMEX_MEMORY_DC_DEFINES_H__
