#ifndef _IMPORT_SPECTRA_WAIT_DLG_H_
#define _IMPORT_SPECTRA_WAIT_DLG_H_

#pragma once

#include "WaitingDlg.h"
#include "DeviceData.h"


///////////////////////////////////////////////////////////////////////////////////////////////////
// ������ ��������� �������� ���������� �������������� �������

class CImportSpectraWaitDlg : public CWaitingDlg
{
	DECLARE_DYNAMIC(CImportSpectraWaitDlg)

public:

	CImportSpectraWaitDlg(CDeviceData* dev, CString prjname, apjdb::PRApjProject* apjx,
		VImpSam* isam, bool rep, CWnd* pParent = NULL);									// �����������
	virtual ~CImportSpectraWaitDlg();													// ����������

	enum { IDD = CWaitingDlg::IDD };	// ������������� �������

protected:

	CDeviceData* DevData;	// ��������� �� ������, � ������� ������������ �����������
	CString PrjName;
	apjdb::PRApjProject* Apjx;
	VImpSam* ISamples;
	bool repRef;

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();						// ������������� �������

	virtual void OK();				// ��������� �������� ����� �� �������
	virtual void Stop();			// ��������� ����������� ����� �� �������
	virtual void Work();			// ������ �������� �������

	DECLARE_MESSAGE_MAP()
};

#endif
