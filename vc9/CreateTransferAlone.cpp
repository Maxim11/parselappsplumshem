#include "stdafx.h"

#include "RootData.h"
#include "BranchInfo.h"
#include "CreateTransferAlone.h"
#include "ShowSpectraDlg.h"
#include "LoadSpectraPBDlg.h"
#include "MainFrm.h"

#include <math.h>


//static CProjectData *pPrj;

CCreateTransferAlone::CCreateTransferAlone(CTransferData* trans, CDeviceData* pDev, CProjectData* pPrj)
{
	CalcInfo.TransData = trans;
	PrjData = GetProjectByHItem(trans->GetParentHItem());

	MDev = pDev;
	MPrj = pPrj;
}

bool CCreateTransferAlone::Create(CString strName)
{
	if(!AcceptData(strName)) return false;

	if(CalcInfo.bNoCorr && !CalcInfo.LoadSpectraData())	return false;

	bool bOK = CalcInfo.MakeCorrection(cEmpty);

	CalcInfo.FreeSpectraData();

	return bOK;
}

bool CCreateTransferAlone::AcceptData(CString strName)
{
//	CTransferData *trans = PrjData->GetTransfer(strName);
//	if(trans != NULL) return false; 

	CalcInfo.TransData->SetName(strName);
	CalcInfo.TransData->SetMasterDevType(MDev->GetType());
	CalcInfo.TransData->SetMasterDevNumber(MDev->GetNumber());

	CalcInfo.bNoCorr = true;

	CalcInfo.TransData->SetLowLimSpec(max(PrjData->GetLowLimSpecRange(), MPrj->GetLowLimSpecRange()));
	CalcInfo.TransData->SetUpperLimSpec(min(PrjData->GetUpperLimSpecRange(), MPrj->GetUpperLimSpecRange()));

	CalcInfo.PrjM = MPrj;
	CalcInfo.ModM = NULL;
	CalcInfo.iTransM = 0;

	CalcInfo.MSamNames.clear();
	int nSam = MPrj->GetNSamples();
	for(int i=0; i<nSam; i++)
	{
		CalcInfo.MSamNames.push_back(MPrj->GetSample(i)->GetName());
	}

	CalcInfo.MParNames.clear();
	int nPar = MPrj->GetNParams();
	for(int i=0; i<nPar; i++)
	{
		CalcInfo.MParNames.push_back(MPrj->GetParam(i)->GetName());
	}

	CalcInfo.SamplesCorr.clear();

	CalcInfo.Preprocs.clear();
	TransPrepInfo NewPrep;
	VInt empPrep;
	NewPrep.Preproc = empPrep;
	CalcInfo.Preprocs.push_back(NewPrep);

	return true;
}


