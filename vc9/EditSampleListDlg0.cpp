#include "stdafx.h"

#include "BranchInfo.h"
#include "EditSampleListDlg.h"
#include "CreateSampleDlg.h"
#include "LoadSpectraPBDlg.h"
#include "MainFrm.h"


static CEditSampleListDlg* pEditSam = NULL;

static int GetSampleColor(int item, int subitem, void* /*pData*/)
{
	if(pEditSam == NULL)
		return 0;

	COLORREF color = (subitem == 0 && pEditSam->IsSampleUse(item)) ? cColorPass : CLR_DEFAULT;

	pEditSam->m_list.CurTxtColor = color;

	return 1;
}

IMPLEMENT_DYNAMIC(CEditSampleListDlg, CDialog)

CEditSampleListDlg::CEditSampleListDlg(CProjectData* PrjData, LSam* samplesdata, HTREEITEM item, CWnd* pParent)
	: CDialog(CEditSampleListDlg::IDD, pParent)
{
	pEditSam = this;

	ParentPrj = PrjData;
	Trans = ParentPrj->GetTransfer(item);
	SamplesList = samplesdata;
}

CEditSampleListDlg::~CEditSampleListDlg()
{
}

void CEditSampleListDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_SAMPLES_LIST, m_list);
}

BEGIN_MESSAGE_MAP(CEditSampleListDlg, CDialog)

	ON_BN_CLICKED(IDHELP, OnHelp)
	ON_BN_CLICKED(IDC_ADD, OnAdd)
	ON_BN_CLICKED(IDC_EDIT, OnEdit)
	ON_BN_CLICKED(IDC_REMOVE, OnRemove)

	ON_NOTIFY(LVN_ITEMCHANGED, IDC_SAMPLES_LIST, OnSamplesListSelChange)
	ON_NOTIFY(HDN_ITEMCLICK, 0, OnSamplesListColumnClick)

END_MESSAGE_MAP()

BOOL CEditSampleListDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	SetWindowText(ParcelLoadString(STRID_EDIT_SAMPLELIST_HDR));

	CString s;

	s = ParcelLoadString(STRID_EDIT_SAMPLELIST_COLSAM);
	m_list.InsertColumn(0, s, LVCFMT_LEFT, 0, 0);
	s = ParcelLoadString(STRID_EDIT_SAMPLELIST_COLIDEN2);
	m_list.InsertColumn(1, s, LVCFMT_RIGHT, 0, 1);
	s = ParcelLoadString(STRID_EDIT_SAMPLELIST_COLIDEN3);
	m_list.InsertColumn(2, s, LVCFMT_RIGHT, 0, 2);
	s = ParcelLoadString(STRID_EDIT_SAMPLELIST_COLNSPEC);
	m_list.InsertColumn(3, s, LVCFMT_RIGHT, 0, 3);
	s = ParcelLoadString(STRID_MEAS_DATE);
	m_list.InsertColumn(4, s, LVCFMT_RIGHT, 0, 4);
	m_nTimeCol = 4;
	m_nParStart = 5;
	int i, N = ParentPrj->GetNParams();
	for(i=0; i<N; i++)
	{
		CParamData *Par = ParentPrj->GetParam(i);
		CString s;
		s.Format(cFmtS_S, Par->GetName(), Par->GetUnit());
		m_list.InsertColumn(i + m_nParStart, s, LVCFMT_RIGHT, 0, i + m_nParStart);
	}
	for(i=0; i<N+m_nParStart; i++)
		m_list.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	m_list.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);
	m_list.SetIconList();

	m_list.SetUserCallback(LCF_CB_SI_TXTCOLOR, GetSampleColor);
	m_list.EnableUserCallback(LCF_CB_SI_TXTCOLOR, true);

	FillList(0);

	GetDlgItem(IDCANCEL)->SetWindowText(ParcelLoadString(STRID_EDIT_SAMPLELIST_EXIT));
	GetDlgItem(IDHELP)->SetWindowText(ParcelLoadString(STRID_EDIT_SAMPLELIST_HELP));
	GetDlgItem(IDC_ADD)->SetWindowText(ParcelLoadString(STRID_EDIT_SAMPLELIST_ADD));
	GetDlgItem(IDC_EDIT)->SetWindowText(ParcelLoadString(STRID_EDIT_SAMPLELIST_EDIT));
	GetDlgItem(IDC_REMOVE)->SetWindowText(ParcelLoadString(STRID_EDIT_SAMPLELIST_REMOVE));

	GetDlgItem(IDC_ADD)->EnableWindow(Trans->GetType() == C_TRANS_TYPE_OWN);

	GetDlgItem(IDHELP)->EnableWindow(pFrame->IsExistHelp());

	return true;
}

void CEditSampleListDlg::OnAdd()
{
	if(Trans->GetType() != C_TRANS_TYPE_OWN)
		return;

	CSampleData NewSam(ParentPrj->GetHItem(), ParentPrj->GetTransOwnHItem());

	CCreateSampleDlg dlg(&NewSam, SAMDLG_MODE_CREATE);
	if(dlg.DoModal() != IDOK)
		return;
	if(!ParentPrj->AddSample(&NewSam))
		return;

	FillList(0);
}

void CEditSampleListDlg::OnEdit()
{
	int sel = m_list.GetNextItem(-1, LVNI_SELECTED);

	CSampleData* OldSam = Trans->GetSample(sel);
	if(OldSam == NULL)
		return;

	VLSI PSam;
	LoadSampleInfo NewLSI;
	NewLSI.pSam = OldSam;
	ItSpc it;
	for(it=OldSam->SpecData.begin(); it!=OldSam->SpecData.end(); ++it)
		NewLSI.Nums.push_back(it->Num);

	PSam.push_back(NewLSI);

	CLoadSpectraPBDlg dlg2(&PSam, this);
	if(dlg2.DoModal() != IDOK)
	{
		OldSam->FreeSpectraData();
		return;
	}

	CSampleData NewSam(ParentPrj->GetHItem(), Trans->GetHItem());
	NewSam.Copy(*OldSam);

	int mode = (Trans->GetType() == C_TRANS_TYPE_OWN) ? SAMDLG_MODE_CHANGE : SAMDLG_MODE_SHOW;
	CCreateSampleDlg dlg(&NewSam, mode);
	if(dlg.DoModal() != IDOK)
	{
		OldSam->FreeSpectraData();
		return;
	}

	if(Trans->GetType() == C_TRANS_TYPE_OWN)
	{
		if(!ParentPrj->ChangeSample(OldSam, &NewSam))
		{
			OldSam->FreeSpectraData();
			return;
		}
	}

	OldSam->FreeSpectraData();

	FillList(sel, false);
}

void CEditSampleListDlg::OnRemove()
{
	if(Trans->GetType() != C_TRANS_TYPE_OWN)
		return;

	int Ind = -1, nsel = m_list.GetSelectedCount();
	if(nsel <= 0)
		return;

	bool keeped = false;
	VStr Sel;
	for(int i=0; i<nsel; i++)
	{
		Ind = m_list.GetNextItem(Ind, LVNI_SELECTED);
		if(Ind < 0 || Ind >= int(SamplesList->size()))
			break;

		if(ParentPrj->GetSampleKeepStatus(Ind) != C_KEEP_FREE)
		{
			keeped = true;
			continue;
		}

		Sel.push_back(Trans->GetSample(Ind)->GetName());
	}

	CString s = (keeped) ? ParcelLoadString(STRID_EDIT_SAMPLELIST_ASK_DEL_PART)
		: ParcelLoadString(STRID_EDIT_SAMPLELIST_ASK_DEL);

	if(!ParcelConfirm(s, false))
		return;

	ItStr it2;
	for(it2=Sel.begin(); it2!=Sel.end(); ++it2)
		ParentPrj->DeleteSample(*it2);

	FillList(0);
}

void CEditSampleListDlg::OnSamplesListSelChange(NMHDR* /*pNMHDR*/, LRESULT *pResult)
{
	int Ind = -1, nsel = m_list.GetSelectedCount();
	if(nsel <= 0)
	{
		GetDlgItem(IDC_EDIT)->EnableWindow(false);
		GetDlgItem(IDC_REMOVE)->EnableWindow(false);
		return;
	}

	GetDlgItem(IDC_EDIT)->EnableWindow(nsel == 1);
	if(Trans->GetType() != C_TRANS_TYPE_OWN)
		GetDlgItem(IDC_REMOVE)->EnableWindow(Trans->GetType() == C_TRANS_TYPE_OWN);
	else
	{
		bool free = false;
		for(int i=0; i<nsel; i++)
		{
			Ind = m_list.GetNextItem(Ind, LVNI_SELECTED);
			if(Ind < 0 || Ind >= int(SamplesList->size()))
				break;

			if(ParentPrj->GetSampleKeepStatus(Ind) == C_KEEP_FREE)
			{
				free = true;
				break;
			}
		}
		GetDlgItem(IDC_REMOVE)->EnableWindow(free);
	}

	*pResult = 0;
}

void CEditSampleListDlg::OnSamplesListColumnClick(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NMLISTVIEW *pLV = (NMLISTVIEW *) pNMHDR;

	int item = pLV->iItem;

	CString name = ParentPrj->GetSamSortName();
	bool bdir = ParentPrj->GetSamSortDir();
	int ntype = 0;
	int idxpar = 0;

	bool findsort = false;
	if(item == 0)
	{
		if(name.IsEmpty())
		{
			bdir = !bdir;
		}
		ntype = C_SORT_SAMPLE;
		findsort = true;
	}
	else if(item == m_nTimeCol)	//	time
	{
		if(name == cSortTime)
		{
			bdir = !bdir;
		}
		ntype = C_SORT_TIME;
		findsort = true;
	}
	else if(item >= m_nParStart)
	{
		idxpar = item - m_nParStart;
		CString par = ParentPrj->GetParam(idxpar)->GetName();
		if(!par.Compare(name))
		{
			bdir = !bdir;
		}
		ntype = C_SORT_PARAM;
		findsort = true;
	}

	if(findsort)
	{
		GetSamSel();
		if(ParentPrj->SortSamples(ntype, bdir, idxpar))
		{
			FillList(0, false);
		}
	}

	*pResult = 0;
}

int CEditSampleListDlg::GetSamSel()
{
	SamSel.clear();
	int Ind = -1, nsel = m_list.GetSelectedCount();
	if(nsel <= 0)
		return 0;

	for(int i=0; i<nsel; i++)
	{
		Ind = m_list.GetNextItem(Ind, LVNI_SELECTED);
		if(Ind < 0) 
		{
			break;
		}
		SamSel.push_back(m_list.GetItemText(Ind, 0));
	}
	return nsel;
}


void CEditSampleListDlg::FillList(int sel, bool bFromScratch)
{
	CString s;
	ItSam it;
	int i, j;

	int nsel = m_list.GetSelectedCount();
	bool emp = (SamplesList->size() <= 0);

	GetDlgItem(IDC_EDIT)->EnableWindow(!emp && nsel == 1);
	GetDlgItem(IDC_REMOVE)->EnableWindow(!emp);

	if(bFromScratch) m_list.DeleteAllItems();

	for(i=0, it=SamplesList->begin(); it!=SamplesList->end(); ++it, i++)
	{
		s = it->GetName();
		if(bFromScratch)
		{
			m_list.InsertItem(i, s);
		}
		else
		{
			m_list.SetItemText(i, 0, s);
	
			m_list.SetItemState(i, 0, LVIS_SELECTED);
			int ns = SamSel.size();
			for(int k=0; k<ns; k++)
			{
				if(s == SamSel[k])
				{
					m_list.SetItemState(i, LVIS_SELECTED, LVIS_SELECTED);
					break;
				}
			}
		}

		m_list.SetItemText(i, 1, it->GetIden2());
		m_list.SetItemText(i, 2, it->GetIden3());
		
		s.Format(cFmt1_1, it->GetNUsesSpectrum(), it->GetNSpectrum());
		m_list.SetItemText(i, 3, s);
		
		if(it->GetNSpectrum())
		{
			CTime t = it->GetLastSpecDate();
			s = t.Format(cFmtDate2);
		}
		else
		{
			s = L"--";
		}
		m_list.SetItemText(i, m_nTimeCol, s);
		
		for(j=0; j<ParentPrj->GetNParams(); j++)
		{
			s = it->GetReferenceDataStr(j);
			m_list.SetItemText(i, j + m_nParStart, s);
		}
	}

	SetSortIcons();
	for(i=0; i<ParentPrj->GetNParams()+m_nParStart; i++)
		m_list.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);

	if(sel < 0 || sel >= int(SamplesList->size()))
		sel = 0;

	if(bFromScratch)
	{
		m_list.SetItemState(sel, LVIS_SELECTED, LVIS_SELECTED);
	}

    s.Format(ParcelLoadString(STRID_EDIT_SAMPLELIST_SAMPLES), int(SamplesList->size()),	GetNUsesSpectrum());
	GetDlgItem(IDC_STATIC_1)->SetWindowText(s);

	m_list.SetFocus();
}

int CEditSampleListDlg::GetNUsesSpectrum()
{
	int nUses = 0;
	ItSam it;
	for(it=SamplesList->begin(); it!=SamplesList->end(); ++it)
		nUses += it->GetNUsesSpectrum();

	return nUses;
}

void CEditSampleListDlg::SetSortIcons()
{
	int iCol = -1;
	bool dir = true;

	CString name = ParentPrj->GetSamSortName();
	dir = ParentPrj->GetSamSortDir();

	if(name.IsEmpty())
	{
		iCol = 0;
	}
	else if(name == cSortTime)
	{
		iCol = m_nTimeCol;
	}
	else
	{
		ItPar it;
		int i;
		for(i=0, it=ParentPrj->Params.begin(); it!=ParentPrj->Params.end(); ++it, i++)
		{
			if(!name.Compare(it->GetName()))
			{
				iCol = m_nParStart + i;
				break;
			}
		}
	}

	m_list.ShowSortIcons(iCol, dir);
}

bool CEditSampleListDlg::IsSampleUse(int ind)
{
	return (ParentPrj->GetSampleKeepStatus(ind) == C_KEEP_NOEDIT);
}

void CEditSampleListDlg::OnHelp()
{
	int IdHelp = DLGID_SAMPLE_EDIT_LIST;

	pFrame->ShowHelp(IdHelp);
}
