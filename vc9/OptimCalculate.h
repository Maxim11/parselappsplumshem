#ifndef _PARCEL_OPTIM_CALCULATE_H_
#define _PARCEL_OPTIM_CALCULATE_H_

#pragma once

#include "ModelData.h"
#include "ProjectData.h"
#include "ModelOptimization2.h"
#include "SpLumChem.h"


// ��������� ������ ������

const int C_OPTIM_CALC_ALL_MODELS_2		= 0;
const int C_OPTIM_TEST_EXC_POINTS_2		= 1;
const int C_OPTIM_TEST_ANALIZE_OUTS_2	= 2;

class COptimCalculate
{
public:

	COptimCalculate();
	~COptimCalculate();

protected:

	CProjectData* ParentPrj;
	CModelData* StartModel;
	CTransferData* Trans;

	CString ParamName;

	OptimStepSettings* StepData;
	OptimResults* Results;

	int Mode;

public:

	int NAllPoints;
	VDbl AllSpecPoints;
	SInt AllExcPoints;

	VModSam SamplesForCalib;
	VModSam SamplesForValid;

	SInt LeftSet;
	SInt RightSet;
	SInt NCompSet;
	SDbl HiperSet;
	SInt NHarmSet;
	SDbl KfnlSet;
	VStr AllPreprocs;

	VBOSp2 BadSpectra;

	bool IsBreak;

public:

	void InitCalc(CModelData* mod);
	int SetInfo(OptimStepSettings* step, OptimResults* res, VStr* preps, int mode = C_OPTIM_CALC_ALL_MODELS_2);

	ModelSample* GetSampleForCalib(CString name, CString transname);
	ModelSample* GetSampleForValid(CString name, CString transname);

	void GetSamplesCalibValid(VModSam& calib, VModSam& valid);

	int GetModelType();
	int GetSpectrumType();

	int GetModelTransType();

	int GetNAllSpecPoints();
	void GetLowUpperPoints(int left, int right, int& low, int& upper);
	int GetNUsePoints();
	bool SetPoints();
	void GetExcPoints(SInt& points);

	void GetCrits(VInt &crits);
	int GetMainCrit();

	void CalcMinMaxConc(double& min, double& max);

	void SetCalibValidIncludes();
	void ExcAllBadSpectra();

	void MakeSets();
	int CalcNumModels();
	int GetNCalcModels();

	void CalculateAllModels(bool byTmpl, int istep, int nstep);

	bool AddModelResult(OptimModel& mod);

	void ClearResults();

	void FillSampleProxyCalib(vector<CSampleProxy>& Samples);
	void FillSampleProxyValid(vector<CSampleProxy>& Samples);
	void FillSampleProxyCalibOwn(vector<CSampleProxy>& Samples, bool IsPre = false);
	void FillSampleProxyValidOwn(vector<CSampleProxy>& Samples, bool IsPre = false);
	void FillSampleProxyCalibTrans(vector<CSampleProxy>& Samples);
	void FillSampleProxyValidTrans(vector<CSampleProxy>& Samples);
	void ExtractFromSampleProxyOwn(vector<CSampleProxy>& SamplesOwn, vector<CSampleProxy>& Samples);
	void FillFreqScale(CFreqScale& FreqScale);
	void FillFreqScaleTrans(CFreqScale& FreqScale);
	void FillSpecPreproc(CSpecPreproc& prePar, SInt& ExcPoints);
	CString GetOwnPreprocAsStr(CString prep);
	void FillSpecPreprocData(CSpecPreprocData_Parcel& preData);
	void FillQntOutput(CQntOutput& qntOut);

	int GetStartSpecInd();

};

#endif
