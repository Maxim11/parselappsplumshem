#include "stdafx.h"

#include "BranchInfo.h"
#include "ModelData.h"
#include "ModelCalculate.h"
#include "LoadSpectraPBDlg.h"
#include "wm.h"


void ModelParam::Copy(ModelParam& modparam)
{
	ParamName = modparam.ParamName;
	LimMSD = modparam.LimMSD;
	LowValue = modparam.LowValue;
	UpperValue = modparam.UpperValue;
	CorrB = modparam.CorrB;
	CorrA = modparam.CorrA;
}

ModelSample::ModelSample(CSampleData* SamData)
{
	SampleName = SamData->GetName();
	CTransferData *Trans = GetTransferByHItem(SamData->GetTransHItem());
	TransName = (Trans != NULL) ? Trans->GetName() : cOwn;
	TransType = (Trans != NULL) ? Trans->GetType() : C_TRANS_TYPE_OWN;
	tLastSpecDate = SamData->GetLastSpecDate();


	for(int i=0; i<SamData->GetNSpectra(); i++)
	{
		Spectrum *Spec = SamData->GetSpectrumByInd(i);
		if(Spec->bUse)
		{
			PIntBool Use(Spec->Num, true);
			Spectra.insert(Use);
		}
	}
}

void ModelSample::Copy(ModelSample* modsample)
{
	SampleName = modsample->SampleName;
	Spectra.clear();
	copy(modsample->Spectra.begin(), modsample->Spectra.end(), inserter(Spectra, Spectra.begin()));
}

void ModelSample::SetAll(bool bUse)
{
	ItIntBool it;
	for(it=Spectra.begin(); it!=Spectra.end(); ++it)
		it->second = bUse;
}

void ModelSample::Rebuild(CSampleData* SamData)
{
	MapIntBool NewSpectra;
	for(int i=0; i<SamData->GetNSpectra(); i++)
	{
		Spectrum *Spec = SamData->GetSpectrumByInd(i);
		if(Spec->bUse)
		{
			PIntBool Use(Spec->Num, false);
			NewSpectra.insert(Use);
		}
	}

	ItIntBool it;
	for(it=Spectra.begin(); it!=Spectra.end(); ++it)
	{
		if(it->second)
			NewSpectra[it->first] = true;
	}

	Spectra.clear();
	copy(NewSpectra.begin(), NewSpectra.end(), inserter(Spectra, Spectra.begin()));

	NewSpectra.clear();
}

int ModelSample::GetNSpectra()
{
	return int(Spectra.size());
}

int ModelSample::GetNSpectraUsed()
{
	int nUses = 0;
	ItIntBool it;
	for(it=Spectra.begin(); it!=Spectra.end(); ++it)
		if(it->second)
			nUses++;

	return nUses;
}

CString ModelSample::GetUsesSpectrumStr()
{
	CString s = cEmpty, s1;
	ItIntBool it;
	for(it=Spectra.begin(); it!=Spectra.end(); ++it)
		if(it->second)
		{
			if(!s.IsEmpty())  s += _T(", ");
			s1.Format(cFmt1, it->first);
			s += s1;
		}

	return s;
}

CString ModelSample::GetUnUsesSpectrumStr()
{
	CString s = cEmpty, s1;
	ItIntBool it;
	for(it=Spectra.begin(); it!=Spectra.end(); ++it)
		if(!it->second)
		{
			if(!s.IsEmpty())  s += _T(", ");
			s1.Format(cFmt1, it->first);
			s += s1;
		}

	return s;
}

bool ModelSample::IsSpectrum(int num)
{
	ItIntBool it;
	for(it=Spectra.begin(); it!=Spectra.end(); ++it)
		if(it->first == num)
			return true;

	return false;
}

bool ModelSample::IsSpectrumUsed(int num)
{
	ItIntBool it;
	for(it=Spectra.begin(); it!=Spectra.end(); ++it)
		if(it->first == num && it->second)
			return true;

	return false;
}

bool ModelSample::IsOwn()
{
	return TransType == C_TRANS_TYPE_OWN;
}

bool ModelSample::IsTransN()
{
	return TransType == C_TRANS_TYPE_NORMAL;
}

CModelData::CModelData(HTREEITEM hParent)
{
	Name = ParcelLoadString(STRID_DEFNAME_MOD);
	SetDate();
	TransName = cOwn;

	hItem = NULL;
	hParentItem = hParent;

	CProjectData *Prj = GetProjectByHItem(hParentItem);

	AnalysisType = C_ANALYSIS_QNT;
	ModelType = C_MOD_MODEL_PLS;
	SpectrumType = C_MOD_SPECTRUM_ABS;

	SamSortName = cEmpty;
	SamSortDir = true;

	NCompQlt = 3;
	NCompQnt = 3;
	DimHiper = 0.0001;
	NHarm = 3;
	Kfnl = 0.;
	MaxMah = 3.;
	MaxMSD = 1.;
	UseSECV = true;
	LowSpecRange = Prj->GetLowLimSpecRange();
	UpperSpecRange = Prj->GetUpperLimSpecRange();

	GetSpecPoints();

	Preproc.clear();
	Preproc.push_back(C_PREPROC_B);
	Preproc.push_back(C_PREPROC_N);
	Preproc.push_back(C_PREPROC_M);
	Preproc.push_back(C_PREPROC_0);
	Preproc.push_back(C_PREPROC_0);
	Preproc.push_back(C_PREPROC_0);
	Preproc.push_back(C_PREPROC_0);
}

CModelData::~CModelData()
{
	ModelResult.Clear();

	Preproc.clear();
	ModelParams.clear();
	SamplesForCalib.clear();
	SamplesForValid.clear();
	SpecPoints.clear();
	ExcPoints.clear();
}

void CModelData::Copy(CModelData& data, bool bReplaceOwn, CString NewTransName, int NewTransType)
{
	int i;

	hItem = data.GetHItem();
	hParentItem = data.GetParentHItem();

	Name = data.GetName();
	Date = data.GetDate();
	TransName = data.GetTransName();

	TransNames.clear();
	copy(data.TransNames.begin(), data.TransNames.end(), inserter(TransNames, TransNames.begin()));
	if(bReplaceOwn)
	{
		if(TransName == cOwn) TransName = NewTransName; 
		bool bAdd = true; 
		int n = TransNames.size();
		for(int i=0; i<n; i++)
		{
			if(TransNames[i] == NewTransName)
			{
				bAdd = false;
				break;
			}
		}
		if(bAdd) TransNames.push_back(NewTransName);
	}

	AnalysisType = data.GetAnalysisType();
	ModelType = data.GetModelType();
	SpectrumType = data.GetSpectrumType();

	NCompQlt = data.GetNCompQlt();
	NCompQnt = data.GetNCompQnt();
	DimHiper = data.GetDimHiper();
	NHarm = data.GetNHarm();
	Kfnl = data.GetKfnl();
	MaxMah = data.GetMaxMah();
	MaxMSD = data.GetMaxMSD();
	UseSECV = data.IsUseSECV();
	LowSpecRange = data.GetLowSpecRange();
	UpperSpecRange = data.GetUpperSpecRange();

	Preproc.clear();
	for(i=0; i<data.GetNPreproc(); i++)
	{
		Preproc.push_back(data.GetPreprocInd(i));
	}

	SamplesForCalib.clear();
	for(i=0; i<data.GetNSamplesForCalib(); i++)
	{
		SamplesForCalib.push_back(*(data.GetSampleForCalib(i)));
	}
	if(bReplaceOwn)
	{
		for(i=0; i<GetNSamplesForCalib(); i++)
		{
			if(GetSampleForCalib(i)->IsOwn())
			{
				GetSampleForCalib(i)->TransName = NewTransName;
				GetSampleForCalib(i)->TransType = NewTransType;
			}
		}
	}

	SamplesForValid.clear();
	for(i=0; i<data.GetNSamplesForValid(); i++)
	{
		SamplesForValid.push_back(*(data.GetSampleForValid(i)));
	}
	if(bReplaceOwn)
	{
		for(i=0; i<GetNSamplesForValid(); i++)
		{
			if(GetSampleForValid(i)->IsOwn())
			{
				GetSampleForValid(i)->TransName = NewTransName;
				GetSampleForValid(i)->TransType = NewTransType;
			}
		}
	}

	GetSpecPoints();

	data.GetExcPoints(ExcPoints);

	ModelParams.clear();
	for(i=0; i<data.GetNModelParams(); i++)
	{
		ModelParam *OldModParam = data.GetModelParam(i);
		ModelParam NewModParam(OldModParam->ParamName);
		NewModParam.Copy(*OldModParam);
		ModelParams.push_back(NewModParam);
	}

	SamSortName = data.GetSamSortName();
	SamSortDir = data.GetSamSortDir();

	ModelResult.Copy(data.ModelResult, bReplaceOwn, NewTransName);
//	ModelResult.Copy(data.ModelResult);
	FillModInfo();
}

HTREEITEM CModelData::GetHItem()
{
	return hItem;
}

void CModelData::SetHItem(HTREEITEM item)
{
	hItem = item;
}

HTREEITEM CModelData::GetParentHItem()
{
	return hParentItem;
}

void CModelData::SetParentHItem(HTREEITEM item)
{
	hParentItem = item;
}

CString CModelData::GetName()
{
	return Name;
}

void CModelData::SetName(CString name)
{
	Name = name;
}

CTime CModelData::GetDate()
{
	return Date;
}

void CModelData::SetDate()
{
	Date = CTime::GetCurrentTime();
}

int CModelData::GetNTransNames()
{
	return int(TransNames.size());
}

CString CModelData::GetTransName()
{
	return TransName;
}

int CModelData::GetTransInd()
{
	CProjectData *Prj = GetProjectByHItem(hParentItem);
	if(Prj == NULL)
		return -1;

	ItTrans it;
	int i;
	for(it=Prj->Transfers.begin(), i=0; it!=Prj->Transfers.end(); ++it, i++)
		if(!TransName.Compare(it->GetName()))
			return i;

	return -1;
}

void CModelData::SetTransName(CString name)
{
	CProjectData *Prj = GetProjectByHItem(hParentItem);
	if(Prj == NULL)
		return;

	CString oldname = TransName;

	CTransferData *Trans = Prj->GetTransfer(name);
	if(Trans == NULL)
	{
		TransName = cOwn;
		ConformTrans(oldname);
		return;
	}

	TransName = name;
	ConformTrans(oldname);
}

void CModelData::SetTransName2(CString NewTransName)
{
	CProjectData *Prj = GetProjectByHItem(hParentItem);
	if(Prj == NULL)
		return;

	CString oldname = TransName;

	// check if the transfer exists
	if(Prj->GetTransfer(NewTransName) == NULL)
	{
		return;
	}

	if(TransName == cOwn)
	{
		TransName = NewTransName;
		TransNames.push_back(TransName);
	}

	ItModSam it;
	for(it=SamplesForCalib.begin(); it!=SamplesForCalib.end(); ++it)
	{
		if(!it->TransName || it->TransName == cOwn)
		{
			it->TransName = NewTransName;
			it->TransType = C_TRANS_TYPE_NORMAL;
		}
	}

	for(it=SamplesForValid.begin(); it!=SamplesForValid.end(); ++it)
	{
		if(!it->TransName || it->TransName == cOwn) 
		{
			it->TransName = NewTransName;
			it->TransType = C_TRANS_TYPE_NORMAL;
		}
	}
}

void CModelData::SetTransNameByInd(int ind)
{
	CProjectData *Prj = GetProjectByHItem(hParentItem);
	if(Prj == NULL)
		return;

	CString oldname = TransName;

	CTransferData *Trans = Prj->GetTransfer(ind);
	if(Trans == NULL)
	{
		TransName = cOwn;
		ConformTrans(oldname);
		return;
	}

	TransName = Trans->GetName();
	ConformTrans(oldname);
}

void CModelData::ConformTrans(CString oldname)
{
	if(TransName.Compare(cOwn))
		SpectrumType = C_MOD_SPECTRUM_ABS;

	if(!oldname.Compare(TransName))
		return;

	VModSam NewModSam;
	ItModSam it;
	for(it=SamplesForCalib.begin(); it!=SamplesForCalib.end(); ++it)
	{
		if(it->IsOwn())
		{
			NewModSam.push_back(*it);
		}
	}

	SamplesForCalib.clear();
	copy(NewModSam.begin(), NewModSam.end(), inserter(SamplesForCalib, SamplesForCalib.begin()));

	NewModSam.clear();
	for(it=SamplesForValid.begin(); it!=SamplesForValid.end(); ++it)
	{
		if(it->IsOwn())
		{
			NewModSam.push_back(*it);
		}
	}

	SamplesForValid.clear();
	copy(NewModSam.begin(), NewModSam.end(), inserter(SamplesForValid, SamplesForValid.begin()));

	CProjectData *Prj = GetProjectByHItem(hParentItem);
	CTransferData *Trans = Prj->GetTransfer(TransName);
	VInt Preps;
	Trans->GetAllPreproc(Preps);
	int i, NTP = Trans->GetNPreproc();
	bool reset = false;
	for(i=0; i<NTP; i++)
	{
		if(Preproc[i] != Preps[i])
		{
			reset = true;
			break;
		}
	}

	if(reset)
	{
		SetAllPreproc(&Preps);
	}

	if(LowSpecRange < Trans->GetLowLimSpec())
	{
		LowSpecRange = Trans->GetLowLimSpec();
	}

	if(UpperSpecRange > Trans->GetUpperLimSpec())
	{
		UpperSpecRange = Trans->GetUpperLimSpec();
	}

	GetSpecPoints();
}

int CModelData::GetNCompQlt()
{
	return NCompQlt;
}

void CModelData::SetNCompQlt(int num)
{
	NCompQlt = num;
}

int CModelData::GetNCompQnt()
{
	return NCompQnt;
}

void CModelData::SetNCompQnt(int num)
{
	NCompQnt = num;
}

double CModelData::GetDimHiper()
{
	return DimHiper;
}

void CModelData::SetDimHiper(double dim)
{
	DimHiper = dim;
}

int CModelData::GetNHarm()
{
	return NHarm;
}

void CModelData::SetNHarm(int num)
{
	NHarm = num;
}

double CModelData::GetKfnl()
{
	return Kfnl;
}

void CModelData::SetKfnl(double kf)
{
	Kfnl = kf;
}

double CModelData::GetMaxMah()
{
	return MaxMah;
}

void CModelData::SetMaxMah(double max)
{
	MaxMah = max;
}

double CModelData::GetMaxMSD()
{
	return MaxMSD;
}

void CModelData::SetMaxMSD(double max)
{
	MaxMSD = max;
}

bool CModelData::IsUseSECV()
{
	return UseSECV;
}

CString CModelData::GetUseSECVStr()
{
	return (UseSECV) ? ParcelLoadString(STRID_FIELD_YES) : ParcelLoadString(STRID_FIELD_NO);
}

void CModelData::SetUseSECV(bool use)
{
	UseSECV = use;
}

int CModelData::GetLowSpecRange()
{
	return LowSpecRange;
}

void CModelData::SetLowSpecRange(int lim)
{
	LowSpecRange = lim;
}

int CModelData::GetUpperSpecRange()
{
	return UpperSpecRange;
}

void CModelData::SetUpperSpecRange(int lim)
{
	UpperSpecRange = lim;
}

int CModelData::GetAnalysisType()
{
	return AnalysisType;
}

bool CModelData::SetAnalysisType(int ind)
{
	if(ind < C_ANALYSIS_QNT || ind > C_ANALYSIS_QLT)
		return false;

	AnalysisType = ind;

	return true;
}

int CModelData::GetModelType()
{
	return ModelType;
}

int CModelData::GetModelHSOType()
{
	int ModelHSOType = C_MOD_MODEL_HSO_LPM;

	bool bNL = (Kfnl > 1.e-9);
	bool bHm = (NHarm > 0);

	if(!bNL && !bHm)  ModelHSOType = C_MOD_MODEL_HSO_LPM;
	if(bNL && !bHm)  ModelHSOType = C_MOD_MODEL_HSO_NLPM;
	if(!bNL && bHm)  ModelHSOType = C_MOD_MODEL_HSO_LHM;
	if(bNL && bHm)  ModelHSOType = C_MOD_MODEL_HSO_NLHM;

	return ModelHSOType;
}

bool CModelData::SetModelType(int ind)
{
	if(ind < C_MOD_MODEL_PLS || ind > C_MOD_MODEL_HSO)
		return false;

	ModelType = ind;

	return true;
}

int CModelData::GetSpectrumType()
{
	return SpectrumType;
}

bool CModelData::SetSpectrumType(int ind)
{
	if(ind < C_MOD_SPECTRUM_TRA || ind > C_MOD_SPECTRUM_ABS)
		return false;

	SpectrumType = ind;

	return true;
}

CString CModelData::GetSamSortName()
{
	return SamSortName;
}

void CModelData::SetSamSortName(CString name)
{
	SamSortName = name;
}

bool CModelData::GetSamSortDir()
{
	return SamSortDir;
}

void CModelData::SetSamSortDir(bool flag)
{
	SamSortDir = flag;
}

int CModelData::GetNPreproc()
{
	return int(Preproc.size());
}

int CModelData::GetPreprocInd(int num)
{
	if(num < 0 || num >= GetNPreproc())
		return C_PREPROC_0;

	return Preproc[num];
}

CString CModelData::GetAllPreprocAsStr()
{
	return cGetPreprocAsStr(&Preproc);
}

CString CModelData::GetOwnPreprocAsStr()
{
	VInt OwnPreproc;
	int N = GetNTransPreproc();
	copy(Preproc.begin() + N, Preproc.end(), inserter(OwnPreproc, OwnPreproc.begin()));

	return cGetPreprocAsStr(&OwnPreproc);
}

CString CModelData::GetTransPreprocAsStr()
{
	CTransferData *Trans = GetProjectByHItem(hParentItem)->GetTransfer(TransName);
	if(!TransName.Compare(cOwn) || Trans == NULL)
		return cEmpty;

	return Trans->GetAllPreprocAsStr();
}

void CModelData::GetAllPreproc(VInt& vect)
{
	vect.clear();
	copy(Preproc.begin(), Preproc.end(), inserter(vect, vect.begin()));
}

int CModelData::GetPreprocNum(int ind)
{
	for(int i=0; i<GetNPreproc(); i++)
	{
		if(GetPreprocInd(i) == ind)
		{
			return ind;
		}
	}
	return -1;
}

void CModelData::SetAllPreproc(VInt* vect)
{
	Preproc.clear();
	copy(vect->begin(), vect->end(), inserter(Preproc, Preproc.begin()));
}

void CModelData::SetAllPreproc(CString prepstr)
{
	Preproc.clear();
	cGetPreprocFromStr(prepstr, Preproc, 7);
}

int CModelData::GetNTransPreproc()
{
	CTransferData *Trans = GetProjectByHItem(hParentItem)->GetTransfer(TransName);
	if(!TransName.Compare(cOwn) || Trans == NULL)
		return 0;

	return Trans->GetNPreproc();
}

int CModelData::GetNModelParams()
{
	return int(ModelParams.size());
}

ModelParam* CModelData::GetModelParam(int ind)
{
	if(ind < 0 || ind >= int(ModelParams.size()))
		return NULL;

	int i;
	ItModPar it;
	for(i=0, it=ModelParams.begin(); i<ind; i++)
	{
		++it;
	}

	return &(*it);
}

ModelParam* CModelData::GetModelParam(CString name)
{
	ItModPar it;
	for(it=ModelParams.begin(); it!=ModelParams.end(); ++it)
	{
		if(!(*it).ParamName.CompareNoCase(name))
		{
			return &(*it);
		}
	}

	return NULL;
}

bool CModelData::IsModelParamExist(CString name)
{
	ItModPar it;
	for(it=ModelParams.begin(); it!=ModelParams.end(); ++it)
	{
		if(!(*it).ParamName.CompareNoCase(name))
		{
			return true;
		}
	}

	return false;
}

bool CModelData::AddModelParam(ModelParam* modpar)
{
	ItModPar it;
	for(it=ModelParams.begin(); it!=ModelParams.end(); ++it)
	{
		if(!(*it).ParamName.CompareNoCase(modpar->ParamName))
		{
			return false;
		}
	}

	ModelParams.push_back(*modpar);

	return true;
}

bool CModelData::AddModelParam(CString name)
{
	ItModPar it;
	for(it=ModelParams.begin(); it!=ModelParams.end(); ++it)
	{
		if(!(*it).ParamName.CompareNoCase(name))
		{
			return false;
		}
	}

	ModelParam NewModelParam(name);

	ModelParams.push_back(NewModelParam);

	return true;
}

bool CModelData::DeleteModelParam(CString name)
{
	ItModPar it;
	for(it=ModelParams.begin(); it!=ModelParams.end(); ++it)
	{
		if(!(*it).ParamName.CompareNoCase(name))
		{
			ModelParams.erase(it);
			return true;
		}
	}

	return false;
}

void CModelData::DeleteAllModelParams()
{
	ModelParams.clear();
}

int CModelData::GetNSamplesRest()
{
	return int(SamplesRest.size());
}

ModelSample* CModelData::GetSampleRest(int ind)
{
	if(ind < 0 || ind >= int(SamplesRest.size()))
		return NULL;

	return &SamplesRest[ind];
}

ModelSample* CModelData::GetSampleRest(CString name, CString transname)
{
	int i;
	ItModSam it;
	for(i=0, it=SamplesRest.begin(); it!=SamplesRest.end(); ++it, i++)
	{
		if(!it->SampleName.Compare(name) && !it->TransName.Compare(transname))
			break;
	}

	return GetSampleRest(i);
}

bool CModelData::AddSampleRest(ModelSample *modsample)
{
	if(GetSampleRest(modsample->SampleName, modsample->TransName) != NULL)
		return false;

	CTransferData* AddTrans = GetProjectByHItem(GetParentHItem())->GetTransfer(modsample->TransName);
	ModelSample NewModSam(AddTrans->GetSample(modsample->SampleName));

	NewModSam.Copy(modsample);
	SamplesRest.push_back(NewModSam);

	return true;
}

void CModelData::DeleteAllSamplesRest()
{
	SamplesRest.clear();
}

void CModelData::CreateSamplesRest()
{
	CProjectData *ParentPrj = GetProjectByHItem(hParentItem);
	VModSam NamesSmpl;
	int i;
	for(i=0; i<ParentPrj->GetNSamples(); i++)
	{
		CSampleData* Sam = ParentPrj->GetSample(i);
		ItModPar itP;
		for(itP=ModelParams.begin(); itP!=ModelParams.end(); ++itP)
			if(Sam->GetReferenceData(itP->ParamName)->bNoValue)
				break;
		if(itP == ModelParams.end())
		{
			ModelSample MSam(Sam);
			NamesSmpl.push_back(MSam);
		}
	}
	ItStr itT;
	for(itT=TransNames.begin(); itT!=TransNames.end(); ++itT)
	{
		CTransferData *pTr = ParentPrj->GetTransfer(*itT);
		for(i=0; i<pTr->GetNSamples(); i++)
		{
			CSampleData* Sam = pTr->GetSample(i);
			ItModPar itP;
			for(itP=ModelParams.begin(); itP!=ModelParams.end(); ++itP)
				if(Sam->GetReferenceData(itP->ParamName)->bNoValue)
					break;
			if(itP == ModelParams.end())
			{
				ModelSample MSam(Sam);
				NamesSmpl.push_back(MSam);
			}
		}
	}

	ItModSam it;
	ItIntBool itSp;
	for(it=NamesSmpl.begin(); it!=NamesSmpl.end(); ++it)
	{
		int N = it->GetNSpectra();
		ModelSample *MSamC = GetSampleForCalib(it->SampleName, it->TransName);
		ModelSample *MSamV = GetSampleForValid(it->SampleName, it->TransName);
		if(MSamC != NULL && MSamV != NULL)
			for(itSp=it->Spectra.begin(); itSp!=it->Spectra.end(); ++itSp)
				itSp->second = (MSamC->Spectra[itSp->first] || MSamV->Spectra[itSp->first]);
		else if(MSamC != NULL)
			for(itSp=it->Spectra.begin(); itSp!=it->Spectra.end(); ++itSp)
				itSp->second = MSamC->Spectra[itSp->first];
		else if(MSamV != NULL)
			for(itSp=it->Spectra.begin(); itSp!=it->Spectra.end(); ++itSp)
				itSp->second = MSamV->Spectra[itSp->first];
		else
			it->SetAll(false);

		if(!it->GetUnUsesSpectrumStr().IsEmpty())
			AddSampleRest(&(*it));
	}

	NamesSmpl.clear();
}

int CModelData::GetNSamplesForCalib()
{
	return int(SamplesForCalib.size());
}

int CModelData::GetNSpectraForCalib()
{
	int sum = 0;
	ItModSam it;
	for(it=SamplesForCalib.begin(); it!=SamplesForCalib.end(); ++it)
		sum += it->GetNSpectraUsed();

	return sum;
}

ModelSample* CModelData::GetSampleForCalib(int ind)
{
	if(ind < 0 || ind >= int(SamplesForCalib.size()))
		return NULL;

	return &SamplesForCalib[ind];
}

ModelSample* CModelData::GetSampleForCalib(CString name, CString transname)
{
	int i;
	ItModSam it;
	for(i=0, it=SamplesForCalib.begin(); it!=SamplesForCalib.end(); ++it, i++)
		if(!it->SampleName.Compare(name) && !it->TransName.Compare(transname))
			break;

	return GetSampleForCalib(i);
}

bool CModelData::AddSampleForCalib(ModelSample *modsample)
{
	if(GetSampleForCalib(modsample->SampleName, modsample->TransName) != NULL)
		return false;
	if(modsample->GetNSpectraUsed() <= 0)
		return false;

	CTransferData* AddTrans = GetProjectByHItem(GetParentHItem())->GetTransfer(modsample->TransName);
	ModelSample NewModSam(AddTrans->GetSample(modsample->SampleName));

	NewModSam.Copy(modsample);
	SamplesForCalib.push_back(NewModSam);

	return true;
}

bool CModelData::DeleteSampleForCalib(CString name, CString transname)
{
	ItModSam it;
	for(it=SamplesForCalib.begin(); it!=SamplesForCalib.end(); ++it)
		if(!it->SampleName.Compare(name) && !it->TransName.Compare(transname))
		{
			SamplesForCalib.erase(it);
			return true;
		}

	return false;
}

bool CModelData::DeleteSampleForCalib(int ind)
{
	if(ind < 0 || ind >= GetNSamplesForCalib())
		return false;

	ItModSam it = SamplesForCalib.begin() + ind;
	SamplesForCalib.erase(it);

	return true;
}

void CModelData::DeleteAllSamplesForCalib()
{
	SamplesForCalib.clear();
}

int CModelData::GetNSamplesForCalibOwn()
{
	int N = 0;
	ItModSam it;
	for(it=SamplesForCalib.begin(); it!=SamplesForCalib.end(); ++it)
		if(it->IsOwn())
			N++;

	return N;
}

int CModelData::GetNSamplesForCalibTrans()
{
	int N = 0;
	ItModSam it;
	for(it=SamplesForCalib.begin(); it!=SamplesForCalib.end(); ++it)
		if(!it->TransName.Compare(TransName))
			N++;

	return N;
}

ModelSample* CModelData::GetSampleForCalibOwn(int ind)
{
	int i;
	ItModSam it;
	for(i = 0, it=SamplesForCalib.begin(); it!=SamplesForCalib.end(); ++it)
	{
		if(it->IsOwn())
		{
			if(i == ind)
				return &(*it);
			i++;
		}
	}

	return NULL;
}

ModelSample* CModelData::GetSampleForCalibTrans(int ind)
{
	int i;
	ItModSam it;
	for(i = 0, it=SamplesForCalib.begin(); it!=SamplesForCalib.end(); ++it)
	{
		if(!it->TransName.Compare(TransName))
		{
			if(i == ind)
				return &(*it);
			i++;
		}
	}

	return NULL;
}

int CModelData::GetNSamplesForValid()
{
	return int(SamplesForValid.size());
}

int CModelData::GetNSpectraForValid()
{
	int sum = 0;
	ItModSam it;
	for(it=SamplesForValid.begin(); it!=SamplesForValid.end(); ++it)
		sum += it->GetNSpectraUsed();

	return sum;
}

ModelSample* CModelData::GetSampleForValid(int ind)
{
	if(ind < 0 || ind >= int(SamplesForValid.size()))
		return NULL;

	return &SamplesForValid[ind];
}

ModelSample* CModelData::GetSampleForValid(CString name, CString transname)
{
	int i;
	ItModSam it;
	for(i=0, it=SamplesForValid.begin(); it!=SamplesForValid.end(); ++it, i++)
		if(!it->SampleName.Compare(name) && !it->TransName.Compare(transname))
			break;

	return GetSampleForValid(i);
}

bool CModelData::AddSampleForValid(ModelSample *modsample)
{
	if(GetSampleForValid(modsample->SampleName, modsample->TransName) != NULL)
		return false;

	CTransferData* AddTrans = GetProjectByHItem(GetParentHItem())->GetTransfer(modsample->TransName);
	ModelSample NewModSam(AddTrans->GetSample(modsample->SampleName));

	NewModSam.Copy(modsample);
	SamplesForValid.push_back(NewModSam);

	return true;
}

bool CModelData::DeleteSampleForValid(CString name, CString transname)
{
	ItModSam it;
	for(it=SamplesForValid.begin(); it!=SamplesForValid.end(); ++it)
		if(!it->SampleName.Compare(name) && !it->TransName.Compare(transname))
		{
			SamplesForValid.erase(it);
			return true;
		}

	return false;
}

bool CModelData::DeleteSampleForValid(int ind)
{
	if(ind < 0 || ind >= GetNSamplesForValid())
		return false;

	ItModSam it = SamplesForValid.begin() + ind;
	SamplesForValid.erase(it);

	return true;
}

void CModelData::DeleteAllSamplesForValid()
{
	SamplesForValid.clear();
}

int CModelData::GetNSamplesForValidOwn()
{
	int N = 0;
	ItModSam it;
	for(it=SamplesForValid.begin(); it!=SamplesForValid.end(); ++it)
		if(it->IsOwn())
			N++;

	return N;
}

int CModelData::GetNSamplesForValidTrans()
{
	int N = 0;
	ItModSam it;
	for(it=SamplesForValid.begin(); it!=SamplesForValid.end(); ++it)
		if(!it->TransName.Compare(TransName))
			N++;

	return N;
}

ModelSample* CModelData::GetSampleForValidOwn(int ind)
{
	int i;
	ItModSam it;
	for(i = 0, it=SamplesForValid.begin(); it!=SamplesForValid.end(); ++it)
	{
		if(it->IsOwn())
		{
			if(i == ind)
				return &(*it);
			i++;
		}
	}

	return NULL;
}

ModelSample* CModelData::GetSampleForValidTrans(int ind)
{
	int i;
	ItModSam it;
	for(i = 0, it=SamplesForValid.begin(); it!=SamplesForValid.end(); ++it)
	{
		if(!it->TransName.Compare(TransName))
		{
			if(i == ind)
				return &(*it);
			i++;
		}
	}

	return NULL;
}

void CModelData::GetCalibValid(VModSam& calib, VModSam& valid)
{
	calib.clear();
	copy(SamplesForCalib.begin(), SamplesForCalib.end(), inserter(calib, calib.begin()));

	valid.clear();
	copy(SamplesForValid.begin(), SamplesForValid.end(), inserter(valid, valid.begin()));
}

void CModelData::SetCalibValid(VModSam& calib, VModSam& valid)
{
	SamplesForCalib.clear();
	copy(calib.begin(), calib.end(), inserter(SamplesForCalib, SamplesForCalib.begin()));

	SamplesForValid.clear();
	copy(valid.begin(), valid.end(), inserter(SamplesForValid, SamplesForValid.begin()));
}

int CModelData::GetNSpecPoints()
{
	return int(SpecPoints.size());
}

double CModelData::GetSpecPoint(int indSP)
{
	if(indSP < 0 || indSP >= GetNSpecPoints())
		return 0.;

	return SpecPoints[indSP];
}

int CModelData::GetNExcPoints()
{
	return int(ExcPoints.size());
}

int CModelData::GetExcPoint(int indEP)
{
	if(indEP < 0 || indEP >= GetNExcPoints())
		return -1;

	int i=0;
	ItSInt it;
	for(i=0, it=ExcPoints.begin(); i<indEP; i++)
		++it;

	return (*it);
}

double CModelData::GetExcPointMean(int indEP)
{
	if(indEP < 0 || indEP >= GetNExcPoints())
		return 0.;

	CProjectData *Prj = GetProjectByHItem(hParentItem);

	return Prj->GetSpecPoint(GetExcPoint(indEP));
}

void CModelData::GetExcPoints(SInt& points)
{
	points.clear();
	copy(ExcPoints.begin(), ExcPoints.end(), inserter(points, points.begin()));
}

void CModelData::SetExcPoints(SInt& points)
{
	CProjectData *Prj = GetProjectByHItem(hParentItem);
	int indS = Prj->GetSpecPointInd(GetSpecPoint(0));
	int indF = Prj->GetSpecPointInd(GetSpecPoint(GetNSpecPoints() - 1));

	ExcPoints.clear();
	ItSInt it;
	for(it=points.begin(); it!=points.end(); ++it)
	{
		if((*it) < indS)  continue;
		if((*it) > indF)  break;

		ExcPoints.insert(*it);
	}
}

void CModelData::DeleteAllExcPoints()
{
	ExcPoints.clear();
}

bool CModelData::IsSampleAvailable(CSampleData* data)
{
	for(int i=0; i<GetNModelParams(); i++)
		if(data->GetReferenceData(GetModelParam(i)->ParamName)->bNoValue)
			return false;

	return true;
}

void CModelData::GetSpecPoints()
{
	CProjectData *Prj = GetProjectByHItem(hParentItem);

	Prj->ExtractSpecPoints(LowSpecRange, UpperSpecRange, SpecPoints);
}

int CModelData::CalcLeftSpecPointInd(int left)
{
	CProjectData *Prj = GetProjectByHItem(hParentItem);
	CDeviceData *Dev = GetDeviceByHItem(Prj->GetParentHItem());
	bool IsImit = (Dev->GetType() == C_DEV_TYPE_IMIT);
	double Step, MinFreq;

	if(IsImit)
	{
		Step = 1.;
		MinFreq = left;
	}
	else
	{
		Step = Prj->GetSubCanalResolutionMean();
		Step /= (16384 * C_LASER_WAVE);
		MinFreq = int(left / Step) * Step;
		if(left - MinFreq > Step / 2.)
			MinFreq += Step;
	}

	int res = Prj->GetSpecPointInd(MinFreq);

	return res;
}

void CModelData::CalcModParamLimits()
{
	CProjectData *Prj = GetProjectByHItem(hParentItem);

	ItModPar it;
	for(it=ModelParams.begin(); it!=ModelParams.end(); ++it)
	{
		double MinValue = 1.e9, MaxValue = 0.;
		ItModSam it2;
		for(it2=SamplesForCalib.begin(); it2!=SamplesForCalib.end(); ++it2)
		{
			CTransferData *Trans = Prj->GetTransfer(it2->TransName);
			CSampleData *Sam = Trans->GetSample(it2->SampleName);
			if(Sam == NULL)
				break;
			ReferenceData *Ref = Sam->GetReferenceData(it->ParamName);
			if(Ref == NULL || Ref->bNoValue)
				continue;

			if(MinValue > Ref->Value)  MinValue = Ref->Value;
			if(MaxValue < Ref->Value)  MaxValue = Ref->Value;
		}

		if(it->UpperValue < 1.e9 - 1) // ��� ���� ���������
		{
			if(it->LowValue < MinValue)  it->LowValue = MinValue;
			if(it->LowValue > MaxValue)  it->LowValue = MaxValue;
			if(it->UpperValue > MaxValue)  it->UpperValue = MaxValue;
			if(it->UpperValue < MinValue)  it->UpperValue = MinValue;
		}
		else	// ������ ������
		{
			it->LowValue = MinValue;
			it->UpperValue = MaxValue;
		}
	}
}

int CModelData::GetNFactor()
{
	int K = 0;

	if(AnalysisType == C_ANALYSIS_QLT)
		K = NCompQlt;
	else if(ModelType != C_MOD_MODEL_HSO)
		K = NCompQnt;
	else
	{
		int iHSO = GetModelHSOType();
		switch(iHSO)
		{
		case C_MOD_MODEL_HSO_LPM:
			K = GetNSpecPoints() - GetNExcPoints();
			break;
		case C_MOD_MODEL_HSO_LHM:
			K = 2 * NHarm + 1;
			break;
		case C_MOD_MODEL_HSO_NLPM:
			K = 2 * (GetNSpecPoints() - GetNExcPoints());
			break;
		case C_MOD_MODEL_HSO_NLHM:
			K = 2 * (2 * NHarm + 1);
			break;
		}
	}

	return K;
}

CString CModelData::GetSampleTransName(ModelSample *pMSam)
{
	CString s = pMSam->SampleName, fmt = cEmpty;

	int i, N = int(TransNames.size());
	for(i=0; i<N; i++)
	{
		fmt += '*';
		if(!pMSam->TransName.Compare(TransNames[i]))
		{
			fmt += _T("%s");
			break;
		}
	}
	if(i < N)
		s.Format(fmt, pMSam->SampleName);

	return s;
}

bool CModelData::IsReadyForTrans()
{
	if(AnalysisType == C_ANALYSIS_QNT && !TransName.Compare(cOwn))
		return true;

	return false;
}

bool CModelData::IsReadyForTransPrj()
{
	if(AnalysisType == C_ANALYSIS_QNT)
		return true;

	return false;
}

bool CModelData::IsExistCalibValid()
{
	ItModSam itC, itV;
	for(itC=SamplesForCalib.begin(); itC!=SamplesForCalib.end(); ++itC)
		for(itV=SamplesForValid.begin(); itV!=SamplesForValid.end(); ++itV)
			if(!itC->SampleName.Compare(itV->SampleName) && !itC->TransName.Compare(itV->TransName))
				return true;

	return false;
}

bool CModelData::CalculateModel(bool exp)
{
	if(!LoadSpectraData(true))
		return false;

	ParcelWait(true);

	CModelCalculate Calc(this);
	bool res = Calc.CalculateModel(exp);

	ParcelWait(false);

	FillModInfo();

	return res;
}

void CModelData::FillModInfo()
{
	SModInfo NewStr;
	Info.clear();

	bool IsQnt = (GetAnalysisType() == C_ANALYSIS_QNT);
	bool IsHSO = (IsQnt && GetModelType() == C_MOD_MODEL_HSO);

	NewStr.SetName = ParcelLoadString(STRID_VIEW_MOD_ANALYSISTYPE);
	NewStr.Value = cGetAnalysisTypeName(GetAnalysisType());
	Info.push_back(NewStr);

	NewStr.SetName = ParcelLoadString(STRID_VIEW_MOD_LOWSPEC);
	NewStr.Value.Format(cFmt1, GetLowSpecRange());
	Info.push_back(NewStr);

	NewStr.SetName = ParcelLoadString(STRID_VIEW_MOD_UPPERSPEC);
	NewStr.Value.Format(cFmt1, GetUpperSpecRange());
	Info.push_back(NewStr);

	NewStr.SetName = ParcelLoadString(STRID_VIEW_MOD_SPECTRUMTYPE);
	NewStr.Value = cGetSpectrumTypeName(GetSpectrumType());
	Info.push_back(NewStr);

	if(GetTransName().Compare(cOwn))
	{
		NewStr.SetName = ParcelLoadString(STRID_VIEW_MOD_TRANSNAME);
		CString st = cEmpty;
		ItStr itT;
		for(itT=TransNames.begin(); itT!=TransNames.end(); ++itT)
		{
			if(itT!=TransNames.begin())
				st += _T(", ");
			st += (*itT);
		}
		NewStr.Value = st;
		Info.push_back(NewStr);
	}

	NewStr.SetName = ParcelLoadString(STRID_VIEW_MOD_PREPROC);
	NewStr.Value = GetAllPreprocAsStr();
	if(NewStr.Value.IsEmpty())
		NewStr.Value = ParcelLoadString(STRID_VIEW_MOD_PREPROC_NO);
	Info.push_back(NewStr);

	NewStr.SetName = ParcelLoadString(STRID_VIEW_MOD_MODELTYPE);
	if(IsQnt)
		NewStr.Value = cGetModelTypeNameShort(GetModelType());
	else
		NewStr.Value = cGetModelTypeNameShort(C_MOD_MODEL_PCA);
	Info.push_back(NewStr);

	NewStr.SetName = ParcelLoadString(STRID_VIEW_MOD_NCOMP);
	int NComp = (IsQnt) ? GetNCompQnt() : GetNCompQlt();
	NewStr.Value.Format(cFmt1, NComp);
	Info.push_back(NewStr);

	if(IsHSO)
	{
		NewStr.SetName = ParcelLoadString(STRID_VIEW_MOD_DIMHIPER);
		NewStr.Value.Format(cFmt64, GetDimHiper());
		Info.push_back(NewStr);

		NewStr.SetName = ParcelLoadString(STRID_VIEW_MOD_NHARM);
		NewStr.Value.Format(cFmt1, GetNHarm());
		Info.push_back(NewStr);

		NewStr.SetName = ParcelLoadString(STRID_VIEW_MOD_KFNL);
		NewStr.Value.Format(cFmt31, GetKfnl());
		Info.push_back(NewStr);
	}

	NewStr.SetName = ParcelLoadString(STRID_VIEW_MOD_MAXMAH);
	NewStr.Value.Format(cFmt31, GetMaxMah());
	Info.push_back(NewStr);

	if(!IsQnt)
	{
		NewStr.SetName = ParcelLoadString(STRID_VIEW_MOD_MAXMSD);
		NewStr.Value.Format(cFmt31, GetMaxMSD());
		Info.push_back(NewStr);
	}
}

int CModelData::GetNModInfo()
{
	return int(Info.size());
}

SModInfo* CModelData::GetModInfo(int ind)
{
	if(ind < 0 || ind >= GetNModInfo())
		return NULL;

	return &Info[ind];
}

bool CModelData::GetSpectraInfo(VLSI& PSam, bool bVal, bool bAll)
{
	PSam.clear();

	CProjectData *Prj = GetProjectByHItem(hParentItem);

	ItModSam it;
	for(it=SamplesForCalib.begin(); it!=SamplesForCalib.end(); ++it)
	{
		CTransferData *Trans = Prj->GetTransfer(it->TransName);
		if(Trans == NULL)  return false;
		CSampleData* Sam = Trans->GetSample(it->SampleName);
		if(Sam == NULL)  return false;

		LoadSampleInfo NewLSI;
		NewLSI.pSam = Sam;

		ItIntBool it2;
		for(it2=it->Spectra.begin(); it2!=it->Spectra.end(); ++it2)
			if(bAll || it2->second)
				NewLSI.Nums.push_back(it2->first);

		PSam.push_back(NewLSI);
	}
	if(bVal)
	{
		for(it=SamplesForValid.begin(); it!=SamplesForValid.end(); ++it)
		{
			CTransferData *Trans = Prj->GetTransfer(it->TransName);
			if(Trans == NULL)  return false;
			CSampleData* Sam = Trans->GetSample(it->SampleName);
			if(Sam == NULL)  return false;

			ItLSI it3;
			for(it3=PSam.begin(); it3!=PSam.end(); ++it3)
				if(Sam == it3->pSam)
					break;

			if(it3!=PSam.end())
			{
				ItIntBool it2;
				for(it2=it->Spectra.begin(); it2!=it->Spectra.end(); ++it2)
					if(bAll || it2->second)
					{
						ItInt it4 = find(it3->Nums.begin(), it3->Nums.end(), it2->first);
						if(it4 != it3->Nums.end())
							continue;

						it3->Nums.push_back(it2->first);
					}
			}
			else
			{
				LoadSampleInfo NewLSI;
				NewLSI.pSam = Sam;

				ItIntBool it2;
				for(it2=it->Spectra.begin(); it2!=it->Spectra.end(); ++it2)
					if(bAll || it2->second)
						NewLSI.Nums.push_back(it2->first);

				PSam.push_back(NewLSI);
			}
		}
	}

	return true;
}

bool CModelData::LoadSpectraData(bool bVal, bool bAll)
{
	VLSI PSam;

	GetSpectraInfo(PSam, bVal, bAll);

	CLoadSpectraPBDlg dlg(&PSam, NULL);
	if(dlg.DoModal() != IDOK)
	{
		FreeSpectraData();
		return false;
	}

	return true;
}

void CModelData::FreeSpectraData()
{
	CProjectData *Prj = GetProjectByHItem(hParentItem);

	ItModSam it;
	for(it=SamplesForCalib.begin(); it!=SamplesForCalib.end(); ++it)
	{
		CTransferData *Trans = Prj->GetTransfer(it->TransName);
		if(Trans == NULL)  continue;
		CSampleData* Sam = Trans->GetSample(it->SampleName);
		if(Sam == NULL)  continue;

		Sam->FreeSpectraData();
	}
	for(it=SamplesForValid.begin(); it!=SamplesForValid.end(); ++it)
	{
		CTransferData *Trans = Prj->GetTransfer(it->TransName);
		if(Trans == NULL)  continue;
		CSampleData* Sam = Trans->GetSample(it->SampleName);
		if(Sam == NULL)  continue;

		Sam->FreeSpectraData();
	}
}

void CModelData::GetDataForBase(prdb::PRModel& Obj)
{
	Obj.ModelName = Name;
	Obj.TransName = TransName;
	Obj.DateCreated = Date;
	Obj.AnalisisType = AnalysisType;
	Obj.ModelType = ModelType;
	Obj.NumComponentQNT = NCompQnt;
	Obj.NumComponentQLT = NCompQlt;
	Obj.HypercubeVolume = DimHiper;
	Obj.HarmonyCount = NHarm;
	Obj.Nonlinearity = Kfnl;
	Obj.PredH = MaxMah;
	Obj.PredCKO = MaxMSD;
	Obj.UseSECV = UseSECV;
	Obj.SpectrumType = SpectrumType;
	Obj.SpRangeRealDown = LowSpecRange;
	Obj.SpRangeRealUp = UpperSpecRange;
	Obj.Preproc = cGetPreprocAsStr(&Preproc);

	Obj.Spectra.clear();
	ItModSam it1;
	for(it1=SamplesForCalib.begin(); it1!=SamplesForCalib.end(); ++it1)
	{
		ItIntBool it2;
		for(it2=it1->Spectra.begin(); it2!=it1->Spectra.end(); ++it2)
		{
			if(it2->second == false)
				continue;

			prdb::PRModel::PRMDSpectrum NewMSp;
			NewMSp.SampleName = it1->SampleName;
			NewMSp.TransName = it1->TransName;
			NewMSp.SpNum = it2->first;
			NewMSp.IsCalib = true;
			NewMSp.IsValid = false;

			Obj.Spectra.push_back(NewMSp);
		}
	}

	for(it1=SamplesForValid.begin(); it1!=SamplesForValid.end(); ++it1)
	{
		ItIntBool it2;
		for(it2=it1->Spectra.begin(); it2!=it1->Spectra.end(); ++it2)
		{
			if(it2->second == false)
				continue;

			vector<prdb::PRModel::PRMDSpectrum>::iterator it3;
			for(it3=Obj.Spectra.begin(); it3!=Obj.Spectra.end(); ++it3)
				if(it3->SpNum == it2->first && !it3->SampleName.Compare(it1->SampleName) &&
				   !it3->TransName.Compare(it1->TransName))
				    break;

			if(it3 == Obj.Spectra.end())
			{
				prdb::PRModel::PRMDSpectrum NewMSp;
				NewMSp.SampleName = it1->SampleName;
				NewMSp.TransName = it1->TransName;
				NewMSp.SpNum = it2->first;
				NewMSp.IsCalib = false;
				NewMSp.IsValid = true;

				Obj.Spectra.push_back(NewMSp);
			}
			else
				it3->IsValid = true;
		}
	}

	Obj.SpDataExclude.clear();
	copy(ExcPoints.begin(), ExcPoints.end(), inserter(Obj.SpDataExclude, Obj.SpDataExclude.begin()));

	Obj.Indcies.clear();
	ItModPar it;
	for(it=ModelParams.begin(); it!=ModelParams.end(); ++it)
	{
		prdb::PRModel::PRMDParam NewMP;
		NewMP.IndexName = it->ParamName;
		NewMP.PredCKOIX = it->LimMSD;
		NewMP.CalibDown = it->LowValue;
		NewMP.CalibUp = it->UpperValue;
		NewMP.CorrB = it->CorrB;
		NewMP.CorrA = it->CorrA;
		Obj.Indcies.push_back(NewMP);
	}
}

void CModelData::SetDataFromBase(prdb::PRModel& Obj)
{
	Name = Obj.ModelName;
	TransName = Obj.TransName;
	Date = Obj.DateCreated;
	AnalysisType = Obj.AnalisisType;
	ModelType = Obj.ModelType;
	NCompQnt = Obj.NumComponentQNT;
	NCompQlt = Obj.NumComponentQLT;
	DimHiper = Obj.HypercubeVolume;
	NHarm = Obj.HarmonyCount;
	Kfnl = Obj.Nonlinearity;
	MaxMah = Obj.PredH;
	MaxMSD = Obj.PredCKO;
	UseSECV = Obj.UseSECV;
	SpectrumType = Obj.SpectrumType;
	LowSpecRange = Obj.SpRangeRealDown;
	UpperSpecRange = Obj.SpRangeRealUp;
	cGetPreprocFromStr(Obj.Preproc, Preproc, 7);

	CProjectData *ParentPrj = GetProjectByHItem(GetParentHItem());

	SamplesForCalib.clear();
	SamplesForValid.clear();
	TransNames.clear();
	vector<prdb::PRModel::PRMDSpectrum>::iterator it3;
	int iSC = 0, iSV = 0;
	for(it3=Obj.Spectra.begin(); it3!=Obj.Spectra.end(); ++it3)
	{
		CString SName = it3->SampleName, TName = it3->TransName;
		CTransferData *Trans = ParentPrj->GetTransfer(TName);

		if(it3->IsCalib)
		{
			ModelSample* MSam = GetSampleForCalib(SName, TName);
			if(MSam == NULL)
			{
				CSampleData *Sam;
				if(TName.Compare(cOwn))
				{
					Sam = Trans->GetSample(SName);
					if(find(TransNames.begin(), TransNames.end(), TName) == TransNames.end())
						TransNames.push_back(TName);
				}
				else
					Sam = ParentPrj->GetSample(SName);
				SamplesForCalib.push_back(Sam);
				SamplesForCalib[iSC].SetAll(false);
				SamplesForCalib[iSC].Spectra[it3->SpNum] = true;
				iSC++;
			}
			else
				MSam->Spectra[it3->SpNum] = true;
		}
		if(it3->IsValid)
		{
			ModelSample* MSam = GetSampleForValid(SName, TName);
			if(MSam == NULL)
			{
				CSampleData *Sam;
				if(TName.Compare(cOwn))
				{
					Sam = Trans->GetSample(SName);
					if(find(TransNames.begin(), TransNames.end(), TName) == TransNames.end())
						TransNames.push_back(TName);
				}
				else
					Sam = ParentPrj->GetSample(SName);
				SamplesForValid.push_back(Sam);
				SamplesForValid[iSV].SetAll(false);
				SamplesForValid[iSV].Spectra[it3->SpNum] = true;
				iSV++;
			}
			else
				MSam->Spectra[it3->SpNum] = true;
		}
	}

	GetSpecPoints();

	ExcPoints.clear();
	copy(Obj.SpDataExclude.begin(), Obj.SpDataExclude.end(), inserter(ExcPoints, ExcPoints.begin()));

	ModelParams.clear();
	int i, N = int(Obj.Indcies.size());
	for(i=0; i<N; i++)
	{
		ModelParam NewMP(Obj.Indcies[i].IndexName);
		NewMP.LimMSD = Obj.Indcies[i].PredCKOIX;
		NewMP.LowValue = Obj.Indcies[i].CalibDown;
		NewMP.UpperValue = Obj.Indcies[i].CalibUp;
		NewMP.CorrB = Obj.Indcies[i].CorrB;
		NewMP.CorrA = Obj.Indcies[i].CorrA;
		ModelParams.push_back(NewMP);
	}

	FillModInfo();
}
