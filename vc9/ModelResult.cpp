#include "stdafx.h"

#include "BranchInfo.h"
#include "ModelResult.h"
#include "SpLumChem.h"

#include <math.h>

static int SortProp;
static bool SortDir;
static int SortInd;
static double SortMaxMah;

bool PSpCalibSort(SpectrumResult& SpCRes1, SpectrumResult& SpCRes2)
{
	double Value1 = 0., Value2 = 0.;
	switch(SortProp)
	{
	case C_MODRES_SORT_CALIB_REF:
		Value1 = SpCRes1.SpecParamData[SortInd].Reference;
		Value2 = SpCRes2.SpecParamData[SortInd].Reference;
		break;
	case C_MODRES_SORT_CALIB_SEC_PRED:
		Value1 = SpCRes1.SpecParamData[SortInd].SEC_Prediction;
		Value2 = SpCRes2.SpecParamData[SortInd].SEC_Prediction;
		break;
	case C_MODRES_SORT_CALIB_SEC_DIFF:
		Value1 = SpCRes1.SpecParamData[SortInd].SEC_RefPred;
		Value2 = SpCRes2.SpecParamData[SortInd].SEC_RefPred;
		break;
	case C_MODRES_SORT_CALIB_SEC_PROC:
		Value1 = SpCRes1.SpecParamData[SortInd].SEC_Proc;
		Value2 = SpCRes2.SpecParamData[SortInd].SEC_Proc;
		break;
	case C_MODRES_SORT_CALIB_SECV_PRED:
		Value1 = SpCRes1.SpecParamData[SortInd].SECV_Prediction;
		Value2 = SpCRes2.SpecParamData[SortInd].SECV_Prediction;
		break;
	case C_MODRES_SORT_CALIB_SECV_DIFF:
		Value1 = SpCRes1.SpecParamData[SortInd].SECV_RefPred;
		Value2 = SpCRes2.SpecParamData[SortInd].SECV_RefPred;
		break;
	case C_MODRES_SORT_CALIB_SECV_PROC:
		Value1 = SpCRes1.SpecParamData[SortInd].SECV_Proc;
		Value2 = SpCRes2.SpecParamData[SortInd].SECV_Proc;
		break;
	case C_MODRES_SORT_CALIB_SECV_SEC:
		Value1 = SpCRes1.SpecParamData[SortInd].SECV_SEC;
		Value2 = SpCRes2.SpecParamData[SortInd].SECV_SEC;
		break;
	case C_MODRES_SORT_CALIB_OUT_MD:
		Value1 = SpCRes1.Out_Mah;
		Value2 = SpCRes2.Out_Mah;
		break;
	case C_MODRES_SORT_CALIB_OUT_EXCEED:
		Value1 = SortMaxMah - SpCRes1.Out_Mah;
		Value2 = SortMaxMah - SpCRes2.Out_Mah;
		break;
	case C_MODRES_SORT_CALIB_BAD_PRED:
		Value1 = SpCRes1.SpecParamData[SortInd].Bad_Prediction;
		Value2 = SpCRes2.SpecParamData[SortInd].Bad_Prediction;
		break;
	case C_MODRES_SORT_CALIB_BAD_DIFF:
		Value1 = SpCRes1.SpecParamData[SortInd].Bad_RefPred;
		Value2 = SpCRes2.SpecParamData[SortInd].Bad_RefPred;
		break;
	case C_MODRES_SORT_CALIB_BAD_PROC:
		Value1 = SpCRes1.SpecParamData[SortInd].Bad_Proc;
		Value2 = SpCRes2.SpecParamData[SortInd].Bad_Proc;
		break;
	case C_MODRES_SORT_CALIB_BAD_REST:
		Value1 = SpCRes1.SpecParamData[SortInd].Bad_Remainder;
		Value2 = SpCRes2.SpecParamData[SortInd].Bad_Remainder;
		break;
	}

	if(SortProp >= C_MODRES_SORT_CALIB_GK_0)
	{
		Value1 = SpCRes1.Scores_GK[SortProp - C_MODRES_SORT_CALIB_GK_0];
		Value2 = SpCRes2.Scores_GK[SortProp - C_MODRES_SORT_CALIB_GK_0];
	}

	bool res = (fabs(Value1 - Value2) < 1.e-9) ? (SpCRes1.Name.Compare(SpCRes2.Name) < 0) : (Value1 < Value2);
    if(!SortDir)  res = !res;

	return res;
}

bool PSpValidSort(SpectrumValResult& SpVRes1, SpectrumValResult& SpVRes2)
{
	double Value1 = 0., Value2 = 0.;
	switch(SortProp)
	{
	case C_MODRES_SORT_VALID_REF:
		Value1 = SpVRes1.SpecValParamData[SortInd].Reference;
		Value2 = SpVRes2.SpecValParamData[SortInd].Reference;
		break;
	case C_MODRES_SORT_VALID_PRED:
		Value1 = SpVRes1.SpecValParamData[SortInd].Corrected;
		Value2 = SpVRes2.SpecValParamData[SortInd].Corrected;
		break;
	case C_MODRES_SORT_VALID_DIFF:
		Value1 = SpVRes1.SpecValParamData[SortInd].RefPred;
		Value2 = SpVRes2.SpecValParamData[SortInd].RefPred;
		break;
	case C_MODRES_SORT_VALID_PROC:
		Value1 = SpVRes1.SpecValParamData[SortInd].Proc;
		Value2 = SpVRes2.SpecValParamData[SortInd].Proc;
		break;
	case C_MODRES_SORT_VALID_DELTA:
		Value1 = SpVRes1.SpecValParamData[SortInd].Delta;
		Value2 = SpVRes2.SpecValParamData[SortInd].Delta;
		break;
	case C_MODRES_SORT_VALID_MD:
		Value1 = SpVRes1.Out_Mah;
		Value2 = SpVRes2.Out_Mah;
		break;
	case C_MODRES_SORT_VALID_EXCEED:
		Value1 = SortMaxMah - SpVRes1.Out_Mah;
		Value2 = SortMaxMah - SpVRes2.Out_Mah;
		break;
	}

	bool res = (fabs(Value1 - Value2) < 1.e-9) ? (SpVRes1.Name.Compare(SpVRes2.Name) < 0) : (Value1 < Value2);
    if(!SortDir)  res = !res;

	return res;
}

void GraphInfo::CalcParams()
{
	xmin = xmax = ymin = ymax = 0.;
	a = 0.;  b = 1.;

	if(mode == C_GRAPH_MODE_HISTO)
	{
		CalcHistoParams();
		return;
	}

	if(N < 1)
		return;

	int i;
	double sum = 0., sum2 = 0.;
	for(i=0; i<N; i++)
	{
		if(mode == C_GRAPH_MODE_SEC_V)
		{
			sum += x[i];
			sum2 += x[i] * x[i];
		}

		if(i == 0)
		{
			xmin = xmax = x[i];
			ymin = ymax = y[i];
		}
		else
		{
			if(x[i] < xmin)  xmin = x[i];
			if(x[i] > xmax)  xmax = x[i];
			if(y[i] < ymin)  ymin = y[i];
			if(y[i] > ymax)  ymax = y[i];
		}
	}

	if(mode == C_GRAPH_MODE_SEC_V)
	{
		double sumA = 0., sumB = 0., Det = N * sum2 - sum * sum;
		if(fabs(Det) > 1.e-9)
		{
			for(i=0; i<N; i++)
			{
				sumB += y[i] * (N * x[i] - sum);
				sumA += y[i] * (sum2 - x[i] * sum);
			}
			b = sumB / Det;
			a = sumA / Det;
		}
		else
			b = a = 0.;
	}
}

void GraphInfo::CalcHistoParams()
{
	ymin = ymax = 0.;

	if(N <= 1)
		return;

	int i;
	for(i=0; i<N; i++)
	{
		if(i == 0)
			ymin = ymax = y[i];
		else
		{
			if(y[i] < ymin)  ymin = y[i];
			if(y[i] > ymax)  ymax = y[i];
		}
	}
}

int GraphInfo::GetIndByName(CString name)
{
	int i;
	ItStr it;
	for(i=0, it=Names.begin(); it!=Names.end(); ++it, i++)
		if(!(*it).Compare(name))
			return i;

	return -1;
}

CString GraphInfo::GetNameByInd(int ind)
{
	if(ind < 0 || ind >= int(Names.size()))
        return cEmpty;

	return Names[ind];
}

void ExportResult::Clear()
{
	ExcPoints.clear();
	MeanSpecTrans.clear();
	MeanSpecMSCTrans.clear();
	MeanSpecStdTrans.clear();
	MeanSpec.clear();
	MeanSpecMSC.clear();
	MeanSpecStd.clear();
	MeanSpecHQO.clear();
	MeanSpecMSCHQO.clear();
	MeanSpecStdHQO.clear();
	ParRes.clear();
	Factors.clear();
	Scores.clear();
}

void ExportQltResult::Clear()
{
	ExcPoints.clear();
	MeanSpecTrans.clear();
	MeanSpecMSCTrans.clear();
	MeanSpecStdTrans.clear();
	MeanSpec.clear();
	MeanSpecMSC.clear();
	MeanSpecStd.clear();
	Factors.clear();
}

ExportParamResult* ExportResult::GetExpParRes(CString name)
{
	ItExpPRes it;
	for(it=ParRes.begin(); it!=ParRes.end(); ++it)
		if(!it->ParamName.Compare(name))
			return &(*it);

	return NULL;
}

CModelResult::CModelResult()
{
	IsAvr = false;
	NVal = 0;

	SortCalibProp = C_MODRES_SORT_CALIB_NAME;
	SortCalibDir = true;
	SortValidProp = C_MODRES_SORT_VALID_NAME;
	SortValidDir = true;
}

CModelResult::~CModelResult()
{
	Clear();
}

void CModelResult::Copy(CModelResult& data, bool bReplaceOwn, CString NewTransName)
{
	Clear();
	copy(data.TransNames.begin(), data.TransNames.end(), inserter(TransNames, TransNames.begin()));
	if(bReplaceOwn)
	{
		TransNames.push_back(NewTransName);
	}

	copy(data.PSResult.begin(), data.PSResult.end(), inserter(PSResult, PSResult.begin()));

	copy(data.CalibResult.begin(), data.CalibResult.end(), inserter(CalibResult, CalibResult.begin()));
	if(bReplaceOwn)
	{
		int ns = CalibResult.size();
		for(int i=0; i<ns; i++)
		{
			if(CalibResult[i].TransName == cOwn)
			{
				CalibResult[i].TransName = NewTransName;
			}
		}
	}

	copy(data.ValidResult.begin(), data.ValidResult.end(), inserter(ValidResult, ValidResult.begin()));
	if(bReplaceOwn)
	{
		int ns = ValidResult.size();
		for(int i=0; i<ns; i++)
		{
			if(ValidResult[i].TransName == cOwn)
			{
				ValidResult[i].TransName = NewTransName;
			}
		}
	}

	copy(data.LoadRes.begin(), data.LoadRes.end(), inserter(LoadRes, LoadRes.begin()));
	SetAverage(data.IsAverage());

	NVal = data.GetNVal();
}

void CModelResult::Clear()
{
	TransNames.clear();
	PSResult.clear();
	CalibResult.clear();
	ValidResult.clear();
	LoadRes.clear();

	ExpResult.Clear();
	ExpQltResult.Clear();
}

int CModelResult::GetNGK()
{
	SpectrumResult* pSpR = GetCalibResult(0);
	if(pSpR == NULL)
		return 0;

	return int(pSpR->Scores_GK.size());
}

int CModelResult::GetNVal()
{
	return NVal;
}

void CModelResult::SetNVal(int nval)
{
	NVal = nval;
}

int CModelResult::GetNPSResult()
{
	return int(PSResult.size());
}

ParamSumResult* CModelResult::GetPSResult(CString name)
{
	ItParSumRes it;
	for(it=PSResult.begin(); it!=PSResult.end(); ++it)
		if(!it->ParamName.Compare(name))
			return &(*it);

	return NULL;
}

ParamSumResult* CModelResult::GetPSResult(int ind)
{
	if(ind < 0 || ind >= GetNPSResult())
		return NULL;

	return &PSResult[ind];
}

ParamSumResult* CModelResult::AddPSResult(CString name)
{
	ParamSumResult *Old = GetPSResult(name);
	if(Old != NULL)	// ��� ����������
		return Old;

	ParamSumResult NewPSR(name);
	PSResult.push_back(NewPSR);

	return GetPSResult(name);
}

int CModelResult::GetNCalibResult()
{
	return int(CalibResult.size());
}

SpectrumResult* CModelResult::GetCalibResult(CString name)
{
	ItSpecRes it;
	for(it=CalibResult.begin(); it!=CalibResult.end(); ++it)
		if(!it->Name.Compare(name))
			return &(*it);

	return NULL;
}

SpectrumResult* CModelResult::GetCalibResult(int ind)
{
	if(ind < 0 || ind >= int(CalibResult.size()))
		return NULL;

	return &CalibResult[ind];
}

int CModelResult::GetCalibIndByName(CString name)
{
	int i;
	ItSpecRes it;
	for(i=0, it=CalibResult.begin(); it!=CalibResult.end(); ++it, i++)
		if(!it->Name.Compare(name))
			return i;

	return -1;
}

CString CModelResult::GetCalibNameByInd(int ind)
{
	SpectrumResult* Sp = GetCalibResult(ind);
	if(Sp != NULL)
		return Sp->Name;

	return cEmpty;
}

SpectrumResult* CModelResult::AddCalibResult(CString samname, CString transname, int num, bool IsAv)
{
	CString SpName = GetSampleTransName(samname, transname);
	if(!IsAv)
	{
		CString fmt = _T("%s-%02d");
		CString tmp = SpName;
		SpName.Format(fmt, tmp, num); 
	}

	ItSpecRes it;
	for(it=CalibResult.begin(); it!=CalibResult.end(); ++it)
		if(!it->Name.Compare(SpName))
			return &(*it);

	SpectrumResult NewCalibRes(SpName, num);
	NewCalibRes.SamName = samname;
	NewCalibRes.TransName = transname;

	CalibResult.push_back(NewCalibRes);

	return GetCalibResult(SpName);
}

int CModelResult::GetNValidResult()
{
	return int(ValidResult.size());
}

SpectrumValResult* CModelResult::GetValidResult(CString name)
{
	ItSpecValRes it;
	for(it=ValidResult.begin(); it!=ValidResult.end(); ++it)
		if(!it->Name.Compare(name))
			return &(*it);

	return NULL;
}

SpectrumValResult* CModelResult::GetValidResult(int ind)
{
	if(ind < 0 || ind >= int(ValidResult.size()))
		return NULL;

	return &ValidResult[ind];
}

int CModelResult::GetValidIndByName(CString name)
{
	int i;
	ItSpecValRes it;
	for(i=0, it=ValidResult.begin(); it!=ValidResult.end(); ++it, i++)
		if(!it->Name.Compare(name))
			return i;

	return -1;
}

CString CModelResult::GetValidNameByInd(int ind)
{
	SpectrumValResult* Sp = GetValidResult(ind);
	if(Sp != NULL)
		return Sp->Name;

	return cEmpty;
}

SpectrumValResult* CModelResult::AddValidResult(CString samname, CString transname, int num)
{
	CString SpName = GetSampleTransName(samname, transname);

	CString fmt = _T("%s-%02d");
	CString tmp = SpName;
	SpName.Format(fmt, tmp, num); 

	ItSpecValRes it;
	for(it=ValidResult.begin(); it!=ValidResult.end(); ++it)
		if(!it->Name.Compare(SpName))
			return &(*it);

	SpectrumValResult NewValidRes(SpName, num);
	NewValidRes.SamName = samname;
	NewValidRes.TransName = transname;

	ValidResult.push_back(NewValidRes);

	return GetValidResult(SpName);
}

int CModelResult::GetNLoadingResult()
{
	return int(LoadRes.size());
}

LoadingResult* CModelResult::GetLoadingResult(int spec)
{
	ItLoadRes it;
	for(it=LoadRes.begin(); it!=LoadRes.end(); ++it)
		if(spec == it->SpecPoint)
			return &(*it);

	return NULL;
}

LoadingResult* CModelResult::GetLoadingResultByInd(int ind)
{
	if(ind < 0 || ind >= int(LoadRes.size()))
		return NULL;

	return &LoadRes[ind];
}

LoadingResult* CModelResult::AddLoadingResult(int spec)
{
	LoadingResult *LRes = GetLoadingResult(spec);
	if(LRes != NULL)
		return LRes;

	LoadingResult NewLoadRes(spec);
	LoadRes.push_back(NewLoadRes);

	return GetLoadingResult(spec);
}

void CModelResult::CalcCorrections(VDbl& corrA, VDbl& corrB, int nFactor)
{
	int i;
	MDbl mRef, mPred;
	VDbl vSEV, vSDV, vR2sev, vErr;

	ItParSumRes itP;
	for(i=0, itP=PSResult.begin(); itP!=PSResult.end(); ++itP, i++)
	{
		VDbl vRef, vPred;
		ItSpecValRes itSp;
		for(itSp=ValidResult.begin(); itSp!=ValidResult.end(); ++itSp)
		{
			itSp->SpecValParamData[i].Corrected = corrB[i] * itSp->SpecValParamData[i].Prediction + corrA[i];
			vRef.push_back(itSp->SpecValParamData[i].Reference);
			vPred.push_back(itSp->SpecValParamData[i].Corrected);
		}

		mRef.push_back(vRef);
		mPred.push_back(vPred);

		vSEV.push_back(0.);
		vSDV.push_back(0.);
		vR2sev.push_back(0.);
		vErr.push_back(0.);
	}

	int err = CalculateSEVparam(&mRef, &mPred, nFactor, &vSEV, &vSDV, &vR2sev, &vErr);
	if(err != -1)
	{
		for(i=0, itP=PSResult.begin(); itP!=PSResult.end(); ++itP, i++)
		{
			itP->SEV = vSEV[i];
			itP->SDV = vSDV[i];
			itP->R2sev = vR2sev[i];
			itP->e = vErr[i];
		}
	}
}

bool CModelResult::IsAverage()
{
	return IsAvr;
}

void CModelResult::SetAverage(bool avr)
{
	IsAvr = avr;
}

void CModelResult::CreateGraphSEC(int iParam)
{
	GraphSEC.mode = C_GRAPH_MODE_SEC_V;
	GraphSEC.sel = true;

	GraphSEC.Names.clear();
	GraphSEC.x.clear();
	GraphSEC.y.clear();

	int i;
	GraphSEC.N = GetNCalibResult();
	for(i=0; i<GraphSEC.N; i++)
	{
		SpectrumResult* pSpR = GetCalibResult(i);
		SpectrumParamResult *pSpPR = &pSpR->SpecParamData[iParam];

		GraphSEC.Names.push_back(pSpR->Name);
		GraphSEC.x.push_back(pSpPR->Reference);
		GraphSEC.y.push_back(pSpPR->SEC_Prediction);
	}

	GraphSEC.CalcParams();
}

void CModelResult::CreateGraphSECV(int iParam)
{
	GraphSECV.mode = C_GRAPH_MODE_SEC_V;
	GraphSECV.sel = true;

	GraphSECV.Names.clear();
	GraphSECV.x.clear();
	GraphSECV.y.clear();

	int i;
	GraphSECV.N = GetNCalibResult();
	for(i=0; i<GraphSECV.N; i++)
	{
		SpectrumResult* pSpR = GetCalibResult(i);
		SpectrumParamResult *pSpPR = &pSpR->SpecParamData[iParam];

		GraphSECV.Names.push_back(pSpR->Name);
		GraphSECV.x.push_back(pSpPR->Reference);
		GraphSECV.y.push_back(pSpPR->SECV_Prediction);
	}

	if(GraphSECV.N > 0)
		GraphSECV.CalcParams();
	else
	{
		GraphSECV.xmin = GraphSEC.xmin;
		GraphSECV.xmax = GraphSEC.xmax;
		GraphSECV.ymin = GraphSEC.ymin;
		GraphSECV.ymax = GraphSEC.ymax;
	}

}

void CModelResult::CreateGraphSEV(int iParam)
{
	GraphSEV.mode = C_GRAPH_MODE_SEC_V;
	GraphSEV.sel = true;

	GraphSEV.Names.clear();
	GraphSEV.x.clear();
	GraphSEV.y.clear();

	int i;
	GraphSEV.N = GetNValidResult();
	for(i=0; i<GraphSEV.N; i++)
	{
		SpectrumValResult* pSpVR = GetValidResult(i);
		SpectrumValParamResult *pSpVPR = &pSpVR->SpecValParamData[iParam];

		GraphSEV.Names.push_back(pSpVR->Name);
		GraphSEV.x.push_back(pSpVPR->Reference);
		GraphSEV.y.push_back(pSpVPR->Corrected);
	}

	if(GraphSEV.N > 0)
		GraphSEV.CalcParams();
	else
	{
		GraphSEV.xmin = GraphSEC.xmin;
		GraphSEV.xmax = GraphSEC.xmax;
		GraphSEV.ymin = GraphSEC.ymin;
		GraphSEV.ymax = GraphSEC.ymax;
		GraphSEV.a = 0.;
		GraphSEV.b = 1.;
	}
}

void CModelResult::CreateHistoCalib()
{
	HistoCalib.mode = C_GRAPH_MODE_HISTO;
	HistoCalib.sel = true;

	HistoCalib.Names.clear();
	HistoCalib.x.clear();
	HistoCalib.y.clear();

	int i;
	HistoCalib.N = GetNCalibResult() + 1;
	for(i=0; i<HistoCalib.N; i++)
	{
		int ind = (i == GetNCalibResult()) ? i - 1 : i;
		SpectrumResult* pSpR = GetCalibResult(ind);

		HistoCalib.Names.push_back(pSpR->Name);
		HistoCalib.x.push_back(double(i));
		HistoCalib.y.push_back(pSpR->Out_Mah);
	}

	HistoCalib.CalcParams();
}

void CModelResult::CreateHistoValid()
{
	HistoValid.mode = C_GRAPH_MODE_HISTO;
	HistoValid.sel = true;

	HistoValid.Names.clear();
	HistoValid.x.clear();
	HistoValid.y.clear();

	int i;
	HistoValid.N = GetNValidResult() + 1;
	if(HistoValid.N <= 1)
		return;

	for(i=0; i<HistoValid.N; i++)
	{
		int ind = (i == GetNValidResult()) ? i - 1 : i;
		SpectrumValResult* pSpVR = GetValidResult(ind);

		HistoValid.Names.push_back(pSpVR->Name);
		HistoValid.x.push_back(double(i));
		HistoValid.y.push_back(pSpVR->Out_Mah);
	}

	HistoValid.CalcParams();
}

bool CModelResult::CreateHistoSamCalib(int iParam)
{
	HistoSamCalib.mode = C_GRAPH_MODE_HISTO;
	HistoSamCalib.sel = true;

	HistoSamCalib.Names.clear();
	HistoSamCalib.x.clear();
	HistoSamCalib.y.clear();

	VStr Names;
	VDbl Refs;
	double min = 1.e9, max = -1.e9, Ref;
	int i, N = GetNCalibResult();
	for(i=0; i<N; i++)
	{
		SpectrumResult* pSpR = GetCalibResult(i);
		if(find(Names.begin(), Names.end(), pSpR->SamName) != Names.end())
			continue;

		Names.push_back(pSpR->SamName);
		SpectrumParamResult *pSpPR = &pSpR->SpecParamData[iParam];
		Ref = pSpPR->Reference;
		Refs.push_back(Ref);
		if(min > Ref)  min = Ref;
		if(max < Ref)  max = Ref;
	}

	double NN = double(Refs.size());
	int NPart = int(1 + 3.322 * log10(NN));
	if(NPart < 1)
		return false;

	double sh = (max - min) / NPart;
	HistoSamCalib.N = NPart + 1;
	for(i=0; i<NPart + 1; i++)
	{
		HistoSamCalib.x.push_back(min + i * sh);
		HistoSamCalib.y.push_back(0.);
	}
	
	double Nmax = 0.;
	ItDbl it;
	for(it=Refs.begin(); it!=Refs.end(); ++it)
	{
		int ind = (sh > 1.e-9) ? int(((*it) - min) / sh) : 0;
		if(ind < 0 || ind >= NPart)  ind = NPart - 1;

		HistoSamCalib.y[ind] += 1.;
		if(Nmax < HistoSamCalib.y[ind])
			Nmax += 1.;
	}
	HistoSamCalib.y[NPart] = HistoSamCalib.y[NPart - 1];

	HistoSamCalib.CalcParams();
	HistoSamCalib.xmin = min;
	HistoSamCalib.xmax = max;
	HistoSamCalib.ymin = 0;
	HistoSamCalib.ymax = Nmax += 1.;
	return true;
}

bool CModelResult::CreateHistoSamValid(int iParam)
{
	HistoSamValid.mode = C_GRAPH_MODE_HISTO;
	HistoSamValid.sel = true;

	HistoSamValid.Names.clear();
	HistoSamValid.x.clear();
	HistoSamValid.y.clear();

	VStr Names;
	VDbl Refs;
	double min = 1.e9, max = -1.e9, Ref;
	int i, N = GetNValidResult();
	for(i=0; i<N; i++)
	{
		SpectrumValResult* pSpVR = GetValidResult(i);
		if(find(Names.begin(), Names.end(), pSpVR->SamName) != Names.end())
			continue;

		Names.push_back(pSpVR->SamName);
		SpectrumValParamResult *pSpVPR = &pSpVR->SpecValParamData[iParam];
		Ref = pSpVPR->Reference;
		Refs.push_back(Ref);
		if(min > Ref)  min = Ref;
		if(max < Ref)  max = Ref;
	}

	double NN = double(Refs.size());
	int NPart = int(1 + 3.322 * log10(NN));
	if(NPart < 1)
		return false;

	double sh = (max - min) / NPart;
	HistoSamValid.N = NPart + 1;
	for(i=0; i<NPart + 1; i++)
	{
		HistoSamValid.x.push_back(min + i * sh);
		HistoSamValid.y.push_back(0.);
	}
	
	double Nmax = 0.;
	ItDbl it;
	for(it=Refs.begin(); it!=Refs.end(); ++it)
	{
		int ind = (sh > 1.e-9) ? int(((*it) - min) / sh) : 0;
		if(ind < 0 || ind >= NPart)  ind = NPart - 1;

		HistoSamValid.y[ind] += 1.;
		if(Nmax < HistoSamValid.y[ind])
			Nmax += 1.;
	}
	HistoSamValid.y[NPart] = HistoSamValid.y[NPart - 1];

	HistoSamValid.CalcParams();
	HistoSamValid.xmin = min;
	HistoSamValid.xmax = max;
	HistoSamValid.ymin = 0;
	HistoSamValid.ymax = Nmax += 1.;

	return true;
}

void CModelResult::CreateHistoBad(int iParam)
{
	HistoBad.mode = C_GRAPH_MODE_HISTO;
	HistoBad.sel = true;

	HistoBad.Names.clear();
	HistoBad.x.clear();
	HistoBad.y.clear();

	if(iParam < 0 || iParam >= GetNPSResult())
		return;

	int i;
	HistoBad.N = GetNCalibResult() + 1;
	for(i=0; i<HistoBad.N; i++)
	{
		int ind = (i == GetNCalibResult()) ? i - 1 : i;
		SpectrumResult* pSpR = GetCalibResult(ind);
		SpectrumParamResult *pSpPR = &pSpR->SpecParamData[iParam];

		HistoBad.Names.push_back(pSpR->Name);
		HistoBad.x.push_back(double(i));
		HistoBad.y.push_back(pSpPR->Bad_Remainder);
	}

	HistoBad.CalcParams();
}

void CModelResult::CreateGraphScores(int gkX, int gkY)
{
	GraphScores.mode = C_GRAPH_MODE_PNTSET;
	GraphScores.sel = true;

	GraphScores.Names.clear();
	GraphScores.x.clear();
	GraphScores.y.clear();

	if(gkX < 0 || gkY < 0 || gkX >= GetNGK() || gkY >= GetNGK())
		return;

	int i;
	GraphScores.N = GetNCalibResult();
	for(i=0; i<GraphScores.N; i++)
	{
		SpectrumResult* pSpR = GetCalibResult(i);

		GraphScores.Names.push_back(pSpR->Name);
		GraphScores.x.push_back(pSpR->Scores_GK[gkX]);
		GraphScores.y.push_back(pSpR->Scores_GK[gkY]);
	}

	GraphScores.CalcParams();
}

void CModelResult::CreateGraphsSpecLoad(HTREEITEM hItem)
{
	GraphsSpecLoad.clear();
	MasksSpecLoad.clear();

	CProjectData* Prj = GetProjectByHItem(hItem);
	if(Prj == NULL)
		return;

	int i, j, N = GetNLoadingResult(), Ngk = GetNGK();

	for(j=0; j<Ngk; j++)
	{
		GraphInfo Graph;

		Graph.mode = C_GRAPH_MODE_CURVE;
		Graph.sel = true;

		Graph.N = N;
		for(int ii=0; ii<Graph.N; ii++)
		{
			LoadingResult* pLR = GetLoadingResultByInd(ii);

			double spec = Prj->GetSpecPoint(pLR->SpecPoint);
			CString s;
			s.Format(cFmt86, spec);

			Graph.Names.push_back(s);
			Graph.x.push_back(spec);
			Graph.y.push_back(pLR->Spec_GK[j]);
		}

		Graph.CalcParams();

		GraphsSpecLoad.push_back(Graph);
	}

	int old = -1;
	for(i=0; i<N; i++)
	{
		LoadingResult* pLR = GetLoadingResultByInd(i);
		if(i > 0 && pLR->SpecPoint - old > 1)
		{
			double spec1 = Prj->GetSpecPoint(old);
			double spec2 = Prj->GetSpecPoint(pLR->SpecPoint);
			PDblDbl mask(spec1, spec2);
			MasksSpecLoad.push_back(mask);
		}

		old = pLR->SpecPoint;
	}
}

void CModelResult::CreateGraphChemLoad(int gkX, int gkY)
{
	GraphChemLoad.mode = C_GRAPH_MODE_PNTSET;
	GraphChemLoad.sel = true;

	GraphChemLoad.Names.clear();
	GraphChemLoad.x.clear();
	GraphChemLoad.y.clear();

	if(gkX < 0 || gkY < 0 || gkX >= GetNGK() || gkY >= GetNGK())
		return;

	int i;
	GraphChemLoad.N = GetNPSResult();
	for(i=0; i<GraphChemLoad.N; i++)
	{
		ParamSumResult *pPSR = GetPSResult(i);

		GraphChemLoad.Names.push_back(pPSR->ParamName);
		GraphChemLoad.x.push_back(pPSR->Chem_GK[gkX]);
		GraphChemLoad.y.push_back(pPSR->Chem_GK[gkY]);
	}

	GraphChemLoad.CalcParams();
}

GraphInfo* CModelResult::GetGraphSEC()
{
	return &GraphSEC;
}

GraphInfo* CModelResult::GetGraphSECV()
{
	return &GraphSECV;
}

GraphInfo* CModelResult::GetGraphSEV()
{
	return &GraphSEV;
}

GraphInfo* CModelResult::GetHistoCalib()
{
	return &HistoCalib;
}

GraphInfo* CModelResult::GetHistoValid()
{
	return &HistoValid;
}

GraphInfo* CModelResult::GetHistoSamCalib()
{
	return &HistoSamCalib;
}

GraphInfo* CModelResult::GetHistoSamValid()
{
	return &HistoSamValid;
}

GraphInfo* CModelResult::GetHistoBad()
{
	return &HistoBad;
}

GraphInfo* CModelResult::GetGraphScores()
{
	return &GraphScores;
}

GraphInfo* CModelResult::GetGraphSpecLoad(int gk)
{
	if(gk < 0 || gk >= int(GraphsSpecLoad.size()))
		return NULL;

	return &(GraphsSpecLoad[gk]);
}

VDblDbl* CModelResult::GetAllMasksSpecLoad()
{
	return &MasksSpecLoad;
}

GraphInfo* CModelResult::GetGraphChemLoad()
{
	return &GraphChemLoad;
}

CString CModelResult::GetSampleTransName(CString samname, CString transname)
{
	CString s = samname, fmt = cEmpty;

	int i, N = int(TransNames.size());
	for(i=0; i<N; i++)
	{
		fmt += '*';
		if(!transname.Compare(TransNames[i]))
		{
			fmt += _T("%s");
			break;
		}
	}
	if(i < N)
		s.Format(fmt, samname);

	return s;
}

void CModelResult::SortCalib(int prop, bool dir, int ind, double maxmah)
{
	SortCalibProp = SortProp = prop;
	SortCalibDir = SortDir = dir;
	SortInd = ind;
	SortMaxMah = maxmah;

	sort(CalibResult.begin(), CalibResult.end(), PSpCalibSort);
}

void CModelResult::SortValid(int prop, bool dir, int ind, double maxmah)
{
	SortValidProp = SortProp = prop;
	SortValidDir = SortDir = dir;
	SortInd = ind;
	SortMaxMah = maxmah;

	sort(ValidResult.begin(), ValidResult.end(), PSpValidSort);
}
