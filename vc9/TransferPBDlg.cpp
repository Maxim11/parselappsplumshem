#include "stdafx.h"

#include "TransferPBDlg.h"
#include "BranchInfo.h"

IMPLEMENT_DYNAMIC(CTransferPBDlg, CProgressBarDlg)

bool SortDistSpec(SampleCorrDistance& SCDist1, SampleCorrDistance& SCDist2)
{
	return SCDist1.dist < SCDist2.dist;
}

CTransferPBDlg::CTransferPBDlg(CTransferCalculate* trans, CWnd* pParent) : CProgressBarDlg(pParent)
{
	TransCalc = trans;
	NAll = 0;
	CurPrep = 0;
	ipb = 0;
}

CTransferPBDlg::~CTransferPBDlg()
{
}

void CTransferPBDlg::DoDataExchange(CDataExchange* pDX)
{
	CProgressBarDlg::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CTransferPBDlg, CProgressBarDlg)
END_MESSAGE_MAP()


BOOL CTransferPBDlg::OnInitDialog()
{
	CProgressBarDlg::OnInitDialog();

	CString s;
	NAll = int(TransCalc->Preprocs.size());

	SetHeader(ParcelLoadString(STRID_TRANSFER_PB_HDR));
	SetComment(ParcelLoadString(STRID_TRANSFER_PB_COMMENT));
	SetRangeStep(0, NAll, 1);
	SetStatus(ParcelLoadString(STRID_TRANSFER_PB_STATUS));

	return true;
}

void CTransferPBDlg::OK()
{
	CProgressBarDlg::OK();
}

void CTransferPBDlg::Stop()
{
	CString s;
	s.Format(ParcelLoadString(STRID_TRANSFER_PB_ERROR_STOP), CurPrep, NAll);
	if(!ParcelConfirm(s, true))
	{
		TransCalc->IsBreak = true;
		TransCalc->Preprocs.clear();
	}

	CProgressBarDlg::Stop();
}

void CTransferPBDlg::Work()
{
	CurPrep = 0;
	ipb = 0;
	passtime = 0;

	CalculateCorrection();
}

void CTransferPBDlg::CalculateCorrection()
{
	ParcelProcess(true);

	__time64_t ctime, stime;
	_time64(&stime);

	TransCalc->GetSpecPoints();

	TransCalc->FillSampleProxy(TransCalc->SamplesMA, TransCalc->SamplesMC, TransCalc->SamplesSC);
	TransCalc->FillFreqScale(TransCalc->FreqScale);

	if(TransCalc->pSpCorr)
		TransCalc->DeleteCorrection();

	TransCalc->pSpCorr = SpectraCorrection_Create(&TransCalc->SamplesMA, &TransCalc->SamplesMC, &TransCalc->SamplesSC, &TransCalc->FreqScale);
	if(TransCalc->pSpCorr == NULL)
	{
		ParcelProcess(false);
		Stop();
	}

//	ipb++;
	SetPosition(ipb);

	_time64(&ctime);
	int elapsed = passtime + int(ctime - stime);
	int estimated = (ipb > 0) ? int(elapsed * NAll / ipb) : 0;

	SetElapsed(elapsed);
	SetEstimated(estimated);
	UpdateAllWindows();

	CString s;
	bool bStop = false, bPause = false;
	int i;
	for(i=CurPrep; i<NAll; i++)
	{
		bStop = CheckStopPressed();
		if(bStop)
			break;
		bPause = CheckPausePressed();
		if(bPause)
		{
			_time64(&ctime);
			passtime += int(ctime - stime);
			break;
		}

		TransPrepInfo *Info = &TransCalc->Preprocs[i];
		CurPrep++;

		USES_CONVERSION;

		string s(T2A(cGetPreprocAsStr(&(Info->Preproc))));

		SpectraCorrection_Correct(TransCalc->pSpCorr, s);

		double min, max, mean;
		VDbl dist;
		dist.resize(TransCalc->SamplesSC.size());
		SpectraCorrection_GetDistances(TransCalc->pSpCorr, &dist, &min, &max, &mean);

		Info->DistMin = min;
		Info->DistMax = max;
		Info->DistMean = mean;

		ItDbl it2;
		int j;
		Info->DistSpec.clear();
		for(j=0, it2=dist.begin(); it2!=dist.end(); ++it2, j++)
		{
			SampleCorrDistance SCDist;
			SCDist.SCName = TransCalc->SamplesCorr[j].SampleName;
			SCDist.dist = (*it2);
			Info->DistSpec.push_back(SCDist);
		}
		sort(Info->DistSpec.begin(), Info->DistSpec.end(), SortDistSpec);

		Info->IsCalc = true;

		ipb++;
		SetPosition(ipb);

		_time64(&ctime);
		int elapsed = passtime + int(ctime - stime);
		int estimated = (ipb > 0) ? int(elapsed * NAll / ipb) : 0;

		SetElapsed(elapsed);
		SetEstimated(estimated);
		UpdateAllWindows();
	}

	ParcelProcess(false);

	if(bPause)
		return;
	if(bStop)
		Stop();
	else
		OK();
}

void CTransferPBDlg::ResumeWork()
{
	CalculateCorrection();
}
