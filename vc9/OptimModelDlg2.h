#ifndef _PARCEL_OPTIM_MODEL_DLG_2_H_
#define _PARCEL_OPTIM_MODEL_DLG_2_H_

#pragma once

#include "OptimCalculate.h"
#include "ParcelListCtrl.h"
#include "Resource.h"


///////////////////////////////////////////////////////////////////////////////////////////////////
// ������ ����������� �������������� ������.

// �������� �������

const int C_OPT_PAGE_OUTS	= 0;	// �������� ���������� �������� (������ �� �������)
const int C_OPT_PAGE_SPEC	= 1;	// �������� ����� ����������� ���������� �������
const int C_OPT_PAGE_PREP	= 2;	// �������� ����� �������������
const int C_OPT_PAGE_SETS	= 3;	// �������� ����� ����������� ���������� ������
const int C_OPT_PAGE_CRIT	= 4;	// �������� ��������� ��������� ������� ������

// ��������� ������ ������ �������

const int C_OPT_MODE_SINGLE		= 0;
const int C_OPT_MODE_BYTMPL		= 1;
const int C_OPT_MODE_TMPLCREATE = 2;
const int C_OPT_MODE_TMPLEDIT	= 3;

// ����� ������� ����������

const int C_OPTIM_SMPL_CALIB	= 0;		// ���������� �������������� �������
const int C_OPTIM_SMPL_VALID	= 1;		// ���������� ������������� �������

// ��������� ���� ���������� ��������

const int C_OPTIM_SORT_SPEC_NAME_2	= 0;
const int C_OPTIM_SORT_SPEC_REF_2	= 1;

// ������ �������� �� �������

const int C_OPTIM_BYTMPL_ERR		= -1;
const int C_OPTIM_BYTMPL_NOT		= 0;
const int C_OPTIM_BYTMPL_YES		= 1;


class COptimModelDlg2 : public CDialog
{
	DECLARE_DYNAMIC(COptimModelDlg2)

public:

	COptimModelDlg2(CModelData* Mod, Template* tmpl, int mode, CWnd* pParent = NULL);		// �����������
	virtual ~COptimModelDlg2();																// ����������

	enum { IDD = IDD_OPTIM_MODEL };							// ������������� �������

	CTabCtrl m_Tab;				// �������� �� ��������
	CParcelListCtrl m_listResult;	// �������, ���������� ������ ������������ �������
	CParcelListCtrl m_listAll;		// �������, ���������� ������ ���� ������������ �����
	CListCtrl m_listExc;		// �������, ���������� ������ ����������� ������������ �����
	CListCtrl m_listSpectra;	// �������, ���������� ������ ��������
	CComboBox m_ModType;		// ������ ��������� ����� ������
	CComboBox m_SpType;			// ������ ��������� ����� �������
	CComboBox m_Test;			// ������ ��������� ��������������� ��������
	CComboBox m_MainCrit;		// ������ ��������� ��������� �����������
	CComboBox m_Sort;			// ������ ��������� ����� ���������� �� ���������
	CComboBox m_CalibValid;		// ������ ��������� ������� ��������
	CComboBox m_SortSpec;		// ������ ��������� ����� ���������� ��������
	CComboBox m_NHarmType;		// ������ ��������� ����� hqo-������ (�������������)
	CComboBox m_KfnlType;		// ������ ��������� ����� hqo-������ (����������)
	CEdit m_NCompMean;			// ���� ��� �������������� "�������� �� ������������ �������� ������� ���������"
	CEdit m_NCompPlus;			// ���� ��� �������������� "������ �� �������� �� ������������ �������� ������� ���������"
	CEdit m_NCompStep;			// ���� ��� �������������� "���� �������� ������� ���������"
	CEdit m_HiperMean;			// ���� ��� �������������� "�������� �� ������������ �������� ������� ���������"
	CEdit m_HiperPlus;			// ���� ��� �������������� "������ �� �������� �� ������������ �������� ������� ���������"
	CEdit m_HiperStep;			// ���� ��� �������������� "���� �������� �� �������� ������� ���������"
	CEdit m_NHarmMean;			// ���� ��� �������������� "�������� �� ������������ �������� ����� ��������"
	CEdit m_NHarmPlus;			// ���� ��� �������������� "������ �� �������� �� ������������ �������� ����� ��������"
	CEdit m_NHarmStep;			// ���� ��� �������������� "���� �������� �� �������� ����� ��������"
	CEdit m_KfnlMean;			// ���� ��� �������������� "�������� �� ������������ �������� ������������ ������������"
	CEdit m_KfnlPlus;			// ���� ��� �������������� "������ �� �������� �� ������������ �������� ������������ ������������"
	CEdit m_KfnlStep;			// ���� ��� �������������� "���� �������� �� �������� ������������ ������������"
	CEdit m_LeftMean;			// ���� ��� �������������� "�������� �� ������������ �������� ����� ������� ��. ����."
	CEdit m_LeftPlus;			// ���� ��� �������������� "������ �� �������� �� ������������ �������� ����� ������� ��. ����."
	CEdit m_LeftStep;			// ���� ��� �������������� "���� �������� ����� ������� ��. ����."
	CEdit m_RightMean;			// ���� ��� �������������� "�������� �� ������������ �������� ����� ������� ��. ����."
	CEdit m_RightPlus;			// ���� ��� �������������� "������ �� �������� �� ������������ �������� ����� ������� ��. ����."
	CEdit m_RightStep;			// ���� ��� �������������� "���� �������� ����� ������� ��. ����."
	CEdit m_AddPrep;			// ���� ��� �������������� "�������������� �������������"
	CEdit m_MaxLetter;			// ���� ��� �������������� "������������� ����� �������������"
	CEdit m_MaxBetter;			// ���� ��� �������������� "����� ��������� �������"
	CEdit m_MaxMod;				// ���� ��� �������������� "����� ���������� �������"
	CEdit m_Mah;				// ���� ��� �������������� "����������� �������� �� ������������"
	CEdit m_SEC;				// ���� ��� �������������� "����������� �������� �� SEC"
	CEdit m_SECV;				// ���� ��� �������������� "����������� �������� �� SECV"
	CEdit m_Bound;				// ���� ��� �������������� "����������� �������� ��� ������������� ��������"
	CButton m_IsMah;			// ��������� �������� "��������� ������� �� ������������"
	CButton m_IsSEC;			// ��������� �������� "��������� ������� �� SEC"
	CButton m_IsSECV;			// ��������� �������� "��������� ������� �� SECV"
	CButton m_IsBad;			// ��������� �������� "��������� ������� �� ������� ���. �������"
	CButton m_IsBound;			// ��������� �������� "�� ��������� ������� ����� ������"
	CButton m_OperationOnOff;	// ��������� "�������� ��������� ��������������� ��������"
	CButton m_CritSEC;			// ��������� "�������� ������������� �������� SEC"
	CButton m_CritSECV;			// ��������� "�������� ������������� �������� SECV"
	CButton m_CritSECSECV;		// ��������� "�������� ������������� �������� SEC-SECV"
	CButton m_CritSEV;			// ��������� "�������� ������������� �������� SEC"
	CButton m_CritR2sec;		// ��������� "�������� ������������� �������� R2sec"
	CButton m_CritR2secv;		// ��������� "�������� ������������� �������� R2secv"
	CButton m_CritF;			// ��������� "�������� ������������� �������� F"
	CButton m_CritErr;			// ��������� "�������� ������������� �������� Err"
	CButton m_CritSDV;			// ��������� "�������� ������������� �������� SDV"
	CButton m_CritR2sev;		// ��������� "�������� ������������� �������� R2sev"
	CButton m_CritK1;			// ��������� "�������� ������������� �������� K1"
	CButton m_CritK2;			// ��������� "�������� ������������� �������� K2"
	CButton m_CritK3;			// ��������� "�������� ������������� �������� K3"
	CButton m_CritK4;			// ��������� "�������� ������������� �������� K4"
	CButton m_CritK5;			// ��������� "�������� ������������� �������� K5"
	CButton m_CritK6;			// ��������� "�������� ������������� �������� K6"
	CButton m_CritK7;			// ��������� "�������� ������������� �������� K7"
	CButton	m_PrepA;			// ��������� "�������� ������������� ������������� A"
	CButton	m_PrepB;			// ��������� "�������� ������������� ������������� B"
	CButton	m_PrepM;			// ��������� "�������� ������������� ������������� M"
	CButton	m_PrepD;			// ��������� "�������� ������������� ������������� D"
	CButton	m_PrepC;			// ��������� "�������� ������������� ������������� C"
	CButton	m_PrepN;			// ��������� "�������� ������������� ������������� N"
	CButton	m_Prep1;			// ��������� "�������� ������������� ������������� 1"
	CButton	m_Prep2;			// ��������� "�������� ������������� ������������� 2"
	CButton	m_PrepNo;			// ��������� "�������� ������������� ������ �������������"
	CButton	m_UseBest;			// ��������� "�������� ������������� ������ �������"
	CSpinButtonCtrl m_SpinLetter;	// ����������/���������� "������������� ����� �������������"
	CSpinButtonCtrl m_SpinLetter2;	// ����������/���������� "����� ��������� �������"

protected:

	int Mode;					// ����� ������ �������

	CProjectData *ParentPrj;	// ��������� �� ������, ������������ ��� ������
	CModelData *StartModel;		// ��������� �� ������, ��������� ��� �����������
	Template* CurTmpl;			// ��������� �� ������������� ������ �����������

	CString PrePrep;			// ������������� ����� ������ �������������

	bool isTmpl;				// ������� �������� ��� �������������� �������
	bool byTmpl;				// ������� �������� ���� �� �������

	int m_vTab;					// ����� �������� ��������
	int m_vModType;				// ������ ���������� "���� ������"
	int m_vSpType;				// ������ ���������� "���� �������"
	int m_vTest;				// ������ ��������� "��������������� ��������"
	int m_vMainCrit;			// ������ ���������� "�������� �����������"
	int m_vSort;				// ������ ���������� "���� ���������� �� ���������"
	int m_vCalibValid;			// ������ ���������� "������ ��������"
	int m_vSortSpec;			// ������ ���������� "���� ���������� ��������"
	int m_vNHarmType;			// ������ ���������� ���� hqo-������ (�������������)
	int m_vKfnlType;			// ������ ���������� ���� hqo-������ (����������)
	int m_vNCompMean;			// ��������� �������� "���������� �������� ����� ������� ���������"
	int m_vNCompPlus;			// ��������� �������� "���������� �������� ����� ���������"
	int m_vNCompStep;			// ��������� �������� "��� �������� ����� ������� ���������"
	double m_vHiperMean;		// ��������� �������� "���������� �������� ������� ���������"
	double m_vHiperPlus;		// ��������� �������� "���������� �������� ������� ���������"
	double m_vHiperStep;		// ��������� �������� "��� �������� ������� ���������"
	int m_vNHarmMean;			// ��������� �������� "���������� �������� ����� ��������"
	int m_vNHarmPlus;			// ��������� �������� "���������� �������� ����� ��������"
	int m_vNHarmStep;			// ��������� �������� "��� �������� ����� ��������"
	double m_vKfnlMean;			// ��������� �������� "���������� �������� ������������ ������������"
	double m_vKfnlPlus;			// ��������� �������� "���������� �������� ������������ ������������"
	double m_vKfnlStep;			// ��������� �������� "��� �������� ������������ ������������"
	int m_vLeftMean;			// ��������� �������� "�������� �� ������������ �������� ����� ������� ��. ����."
	int m_vLeftPlus;			// ��������� �������� "������ �� �������� �� ������������ �������� ����� ������� ��. ����."
	int m_vLeftStep;			// ��������� �������� "���� �������� ����� ������� ��. ����."
	int m_vRightMean;			// ��������� �������� "�������� �� ������������ �������� ������ ������� ��. ����."
	int m_vRightPlus;			// ��������� �������� "������ �� �������� �� ������������ �������� ������ ������� ��. ����."
	int m_vRightStep;			// ��������� �������� "���� �������� ������ ������� ��. ����."
	CString m_vAddPrep;			// ��������� �������� "�������������� �������������"
	int m_vMaxLetter;			// ��������� �������� "������������� ����� �������������"
	int m_vMaxBetter;			// ��������� �������� "����� ��������� �������"
	int m_vMaxMod;				// ��������� �������� "����� ���������� �������"
	double m_vMah;				// ��������� �������� "����������� �������� �� ������������"
	double m_vSEC;				// ��������� �������� "����������� �������� �� SEC"
	double m_vSECV;				// ��������� �������� "����������� �������� �� SECV"
	double m_vBound;			// ��������� �������� "����������� �������� ��� ������������� ��������"
	BOOL m_vIsMah;				// ������������� �������� "��������� ������� �� ������������"
	BOOL m_vIsSEC;				// ������������� �������� "��������� ������� �� SEC"
	BOOL m_vIsSECV;				// ������������� �������� "��������� ������� �� SECV"
	BOOL m_vIsBad;				// ������������� �������� "��������� ������� �� ������� ���. �������"
	BOOL m_vIsBound;			// ������������� �������� "�� ��������� ������� ����� ������"
	BOOL m_vOperationOnOff;		// ������������� �������� "��������� ��������������� ��������"
	BOOL m_vCritSEC;			// ������������� �������� "�������� ������������� �������� SEC"
	BOOL m_vCritSECV;			// ������������� �������� "�������� ������������� �������� SECV"
	BOOL m_vCritSECSECV;		// ������������� �������� "�������� ������������� �������� SECSECV"
	BOOL m_vCritSEV;			// ������������� �������� "�������� ������������� �������� SEC"
	BOOL m_vCritR2sec;			// ������������� �������� "�������� ������������� �������� R2sec"
	BOOL m_vCritR2secv;			// ������������� �������� "�������� ������������� �������� R2secv"
	BOOL m_vCritF;				// ������������� �������� "�������� ������������� �������� F"
	BOOL m_vCritErr;			// ������������� �������� "�������� ������������� �������� Err"
	BOOL m_vCritSDV;			// ������������� �������� "�������� ������������� �������� SDV"
	BOOL m_vCritR2sev;			// ������������� �������� "�������� ������������� �������� R2sev"
	BOOL m_vCritK1;				// ������������� �������� "�������� ������������� �������� K1"
	BOOL m_vCritK2;				// ������������� �������� "�������� ������������� �������� K2"
	BOOL m_vCritK3;				// ������������� �������� "�������� ������������� �������� K3"
	BOOL m_vCritK4;				// ������������� �������� "�������� ������������� �������� K4"
	BOOL m_vCritK5;				// ������������� �������� "�������� ������������� �������� K5"
	BOOL m_vCritK6;				// ������������� �������� "�������� ������������� �������� K6"
	BOOL m_vCritK7;				// ������������� �������� "�������� ������������� �������� K7"
	BOOL m_vPrepA;				// ������������� �������� "�������� ������������� ������������� A"
	BOOL m_vPrepB;				// ������������� �������� "�������� ������������� ������������� B"
	BOOL m_vPrepM;				// ������������� �������� "�������� ������������� ������������� M"
	BOOL m_vPrepD;				// ������������� �������� "�������� ������������� ������������� D"
	BOOL m_vPrepC;				// ������������� �������� "�������� ������������� ������������� C"
	BOOL m_vPrepN;				// ������������� �������� "�������� ������������� ������������� N"
	BOOL m_vPrep1;				// ������������� �������� "�������� ������������� ������������� 1"
	BOOL m_vPrep2;				// ������������� �������� "�������� ������������� ������������� 2"
	BOOL m_vPrepNo;				// ������������� �������� "�������� ������������� ������ �������������"
	BOOL m_vUseBest;			// ������������� �������� "�������� ������������� ������ �������"

	VOptStep OptimSteps2;		// ������ ������ �����������
	VOptRes OptimRes;			// ������ ����������� �������
	int nSteps;					// ����� ����������� ������ �����������
	int iStep;					// ����� �������� ����� �����������

	COptimCalculate Calc;

	VStr TransesNames;

	VOptSpec2 OptimCalib;		// ������ �������� ��� �����������
	VOptSpec2 OptimValid;		// ������ �������� ��� ���������

	int NAllPoints;				// ����� ����� ������������ �����
	VDbl PointsAll;				// ������ ������ ������������ �����
	SInt PointsExc;				// ������ ����������� ������������ �����

	bool IsAddPreprocValid;		// ������� ������������ ����� �������������� �������������
	int PreprocMaxLen;			// ���������� ����� ������������� �� ���������
	VInt PreprocAval;			// ������ ����������� �������������
	MInt PreprocList;			// ������ ��������� �������������
	MInt PreprocListAdd;		// ������ �������������� �������������

	VInt AvalCrits;				// ������ ��������� ��� ��������������� �������� ���������

	int MinRange;				// ���������� ���������� ������� ������������� ���������
	int MaxRange;				// ����������� ���������� ������� ������������� ���������

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();						// ������������� �������
	virtual void OnOK()
	{};
	virtual void OnCancel();

	void OnAccept();					// ������� ������ � ����� �� �������
	afx_msg void OnAcceptResult();		// ��������� ������� "������� ������ � ����� � ���� ����������� �������"
	afx_msg void OnBack();				// ��������� ������� "������� �� ���������� ���� �����������"
	afx_msg void OnForward();			// ��������� ������� "������� �� ��������� ���� �����������"
	afx_msg void OnCalculate();			// ��������� ������� "��������� ������ � ������� �� ��������� ���� �����������"
	afx_msg void OnSetTarget();			// ��������� ������� "������� ���������� ������ �� ������ ��� ���������� �����"
	afx_msg void OnExclude();			// ��������� ������� "��������� (��������) ������"
	afx_msg void OnSpecListSelChange(NMHDR* /*pNMHDR*/, LRESULT *pResult); // ��������� ������� "�������� �������� � ������ ��������"
	afx_msg void OnResultListSelChange(NMHDR* /*pNMHDR*/, LRESULT *pResult); // ��������� ������� "�������� �������� � ������� �����������"
	afx_msg void OnResultListColumnClick(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnTabSelchange(NMHDR* pNMHDR, LRESULT* pResult); // ��������� ������� "������� �� ������ ��������"
	afx_msg void OnModelTypeChange();	// ��������� ������� "�������� ��� ������"
	afx_msg void OnSpTypeChange();		// ��������� ������� "�������� ��� �������"
	afx_msg void OnTestChange();		// ��������� ������� "�������� ��������������� ��������"
	afx_msg void OnMainCritChange();	// ��������� ������� "�������� �������� �����������"
	afx_msg void OnOperationOnOff();	// ��������� ������� "���������� ������� ��������� ��������������� ��������"
	afx_msg void OnSECOnOff();
	afx_msg void OnSECVOnOff();
	afx_msg void OnSECSECVOnOff();
	afx_msg void OnSEVOnOff();
	afx_msg void OnOutsOnOff();
	afx_msg void OnUseBestOnOff();		// ��������� ������� "���������� ������� ������������� ������ �������"
	afx_msg void OnNHarmTypeChange();
	afx_msg void OnKfnlTypeChange();
	afx_msg void OnAdd();				// ��������� ������� "��������� ������������ �����"
	afx_msg void OnAddAll();			// ��������� ������� "��������� ��� ������������ �����"
	afx_msg void OnRemove();			// ��������� ������� "������� ������������ ����� �� ������ �����������"
	afx_msg void OnRemoveAll();			// ��������� ������� "������� ��� ������������ ����� �� ������ �����������"
	afx_msg	void OnMaxLetterChange();	// ��������� ������� "�������� ����������� ����� �������������"
	afx_msg	void OnMaxBetterChange();		// ��������� ������� "�������� ����� ��������� �������"
	afx_msg	void OnAddPrepChange();		// ��������� ������� "�������� �������������� �������������"
	afx_msg	void OnCreatePreprocList(); // ��������� ������� "�������� ������ �������������"
	afx_msg	void OnPreprocWithout();	// ��������� ������� "���������� ������� ������������� ������ �������������"
	afx_msg void OnSortModels();		// ��������� ������� "�������� ��� ���������� �� ���������"
	afx_msg void OnChangeCalibValid();	// ��������� ������� "�������� ������ ��������"
	afx_msg void OnSortSpectra();		// ��������� ������� "�������� ��� ���������� ��������"
	afx_msg void OnChangeLeftMean();
	afx_msg void OnChangeLeftPlus();
	afx_msg void OnChangeRightMean();
	afx_msg void OnChangeRightPlus();
	afx_msg void OnShowSpectra();
	afx_msg void OnHelp();				// ��������� ������� "������� �������"

	void OnSpinLetterDeltaPos(NMHDR *pNMHDR, LRESULT *pResult); // ��������� ������� "���������/��������� ����������� ����� �������������"
	void OnSpinLetter2DeltaPos(NMHDR *pNMHDR, LRESULT *pResult);// ��������� ������� "���������/��������� ����� ��������� �������"

	void CreateSpectra();
	void CreateSpecPoints();
	void SetAvalCrits();
	void CreateStartStep();
	void CreateByTemplate();

	bool CheckStepNum(int istep);
	bool SetStep(int istep, bool needUpd = true);
	bool CheckOptimSettings();							// ��������� ��������� ������
	bool AcceptStep();
	bool AcceptToModel(CModelData* ModData);
	bool AcceptToTemplate(Template* Tmpl);

	void CreateSortList();
	int GetSortCrit();
	bool SetSortCrit(int crit);
	bool IsCritAval(int crit);				// �������� �� �������� ��� ��������������� ��������
	int GetMainCrit();
	bool SetMainCrit(int crit);

	void GetCalibValid(VOptSpec2& calib, VOptSpec2& valid);
	void GetExcPoints(SInt& points);

	void SortSpectra();

	int GetNPointsShow();
	int GetNExcPointsShow();
	int GetNCalibUse();
	int GetNValidUse();
	int GetNPreprocs();

	void FillSpecLists();
	void FillListAll(int newsel = -1);				// ��������� ������ ������������ �����
	void FillListExc(int newsel = -1);				// ��������� ������ ����������� �����
	void FillTableRes(bool bRemakeColumns, bool bDeleteItems);	// ���������� ������� �����������
	void FillTableSpectra();						// ��������� ������� ��������

	void SetSortIcons();

	void CreatePreprocListAll(bool needUpd = true);
	void CreatePreprocList(VInt* Old);						// ������� ������ �������������
	void CreatePreprocListAdd(CString str);					// ������� ������ �������������� �������������
	bool IsPreprocAddAvailable(VInt* prepline, int prep);	// ����������, ������� �� �������������

	void SetBetterPreps();
	bool SetBestModel(int ind = 0);

	void ClearCritFields();
	void SetCritFields(int crit);
	void UnsetCritFields(int crit);

	void ShowFields(int act = -1);			// ������� �� ����� ��������, ��������������� �������� ��������
	void EnableFields();					// ������� �� ����� ��������, ��������������� �������� ��������

public:

	bool IsPointExc(int ind);
	int GetTransNum(ModelSample *pMSam);
	int GetTransNum(OptimSpectrum2* OptSp);

protected:

	DECLARE_MESSAGE_MAP()
};

#endif
