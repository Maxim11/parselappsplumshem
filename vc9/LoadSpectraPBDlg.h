#ifndef _LOAD_SPECTRA_PB_DLG_H_
#define _LOAD_SPECTRA_PB_DLG_H_

#pragma once

#include "ProgressBarDlg.h"
#include "SampleData.h"


///////////////////////////////////////////////////////////////////////////////////////////////////
// ������ ��������� �������� �������� ������������ ������ ��� ������

class CLoadSpectraPBDlg : public CProgressBarDlg
{
	DECLARE_DYNAMIC(CLoadSpectraPBDlg)

public:

	CLoadSpectraPBDlg(VLSI* psam, CWnd* pParent = NULL);	// �����������
	virtual ~CLoadSpectraPBDlg();							// ����������

	enum { IDD = CProgressBarDlg::IDD };	// ������������� �������

protected:

	VLSI* PSam;				// ��������� �� ������ �������� � �������� ��������, ��� ������� �������� ������������ ������
	int NAll;				// ����� ����� ����������� ��������
	int CurSmpl;			// ���������� ����� �������� �������
	int ipb;				// ����� ����������� ��������
	int passtime;			// �����, ��������� � ������ ������

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();						// ������������� �������

	virtual void OK();				// ��������� �������� ����� �� �������
	virtual void Stop();			// ��������� ����������� ����� �� �������
	virtual void Work();			// ������ �������� �������
	virtual void ResumeWork();		// ����������� ������� ����� �����

	void LoadSpectra();				// �������� ������������ ������

	DECLARE_MESSAGE_MAP()
};

#endif
