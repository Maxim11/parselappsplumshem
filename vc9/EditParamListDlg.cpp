#include "stdafx.h"

#include "BranchInfo.h"
#include "EditParamListDlg.h"
#include "CreateParamDlg.h"
#include "EditParamWaitDlg.h"
#include "MainFrm.h"


IMPLEMENT_DYNAMIC(CEditParamListDlg, CDialog)

CEditParamListDlg::CEditParamListDlg(CProjectData* PrjData, LPar* LParamsData, CWnd* pParent)
	: CDialog(CEditParamListDlg::IDD, pParent)
{
	ParentPrj = PrjData;

	ParamsList = LParamsData;
}

CEditParamListDlg::~CEditParamListDlg()
{
}

void CEditParamListDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_PARAMS_LIST, m_list);
}

BEGIN_MESSAGE_MAP(CEditParamListDlg, CDialog)

	ON_BN_CLICKED(IDHELP, OnHelp)
	ON_BN_CLICKED(IDC_ADD, OnAdd)
	ON_BN_CLICKED(IDC_EDIT, OnEdit)
	ON_BN_CLICKED(IDC_REMOVE, OnRemove)

	ON_NOTIFY(LVN_ITEMCHANGED, IDC_PARAMS_LIST, OnParamsListSelChange)

END_MESSAGE_MAP()

BOOL CEditParamListDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	SetWindowText(ParcelLoadString(STRID_EDIT_PARAMLIST_HDR));

	CString s;
	int i;

	s = ParcelLoadString(STRID_EDIT_PARAMLIST_COLNAME);
	m_list.InsertColumn(0, s, LVCFMT_LEFT, 0, 0);
	s = ParcelLoadString(STRID_EDIT_PARAMLIST_COLSTDMOIS);
	m_list.InsertColumn(1, s, LVCFMT_RIGHT, 0, 1);
	s = ParcelLoadString(STRID_EDIT_PARAMLIST_COLUNIT);
	m_list.InsertColumn(2, s, LVCFMT_RIGHT, 0, 2);
	s = ParcelLoadString(STRID_EDIT_PARAMLIST_COLFORMAT);
	m_list.InsertColumn(3, s, LVCFMT_RIGHT, 0, 3);
	for(i=0; i<4; i++)
		m_list.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	m_list.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

	FillList(0);

	GetDlgItem(IDCANCEL)->SetWindowText(ParcelLoadString(STRID_EDIT_PARAMLIST_EXIT));
	GetDlgItem(IDHELP)->SetWindowText(ParcelLoadString(STRID_EDIT_PARAMLIST_HELP));
	GetDlgItem(IDC_ADD)->SetWindowText(ParcelLoadString(STRID_EDIT_PARAMLIST_ADD));
	GetDlgItem(IDC_EDIT)->SetWindowText(ParcelLoadString(STRID_EDIT_PARAMLIST_EDIT));
	GetDlgItem(IDC_REMOVE)->SetWindowText(ParcelLoadString(STRID_EDIT_PARAMLIST_REMOVE));
	GetDlgItem(IDC_STATIC_4)->SetWindowText(ParcelLoadString(STRID_EDIT_PARAMLIST_PARAMS));

	GetDlgItem(IDHELP)->EnableWindow(pFrame->IsExistHelp());

	return true;
}

void CEditParamListDlg::OnAdd()
{
	CParamData NewPar(ParentPrj->GetHItem());

	CCreateParamDlg dlg(&NewPar, PARDLG_MODE_CREATE);
	if(dlg.DoModal() != IDOK)
		return;

	CEditParamWaitDlg dlg2(ParentPrj, C_MODE_PARAM_WAIT_CREATE, &NewPar, NULL, this);
	if(dlg2.DoModal() != IDOK)
		return;

	FillList(0);
}

void CEditParamListDlg::OnEdit()
{
	int sel = m_list.GetNextItem(-1, LVNI_SELECTED);

	CParamData* OldPar = ParentPrj->GetParam(sel);
	if(OldPar == NULL)
		return;

	CParamData NewPar(ParentPrj->GetHItem());
	NewPar.Copy(*OldPar);

	CCreateParamDlg dlg(&NewPar, PARDLG_MODE_CHANGE);
	if(dlg.DoModal() != IDOK)
		return;

	CEditParamWaitDlg dlg2(ParentPrj, C_MODE_PARAM_WAIT_EDIT, &NewPar, OldPar, this);
	if(dlg2.DoModal() != IDOK)
		return;

	FillList(sel);
}

void CEditParamListDlg::OnRemove()
{
	if(!ParcelConfirm(ParcelLoadString(STRID_EDIT_PARAMLIST_ERROR_DELREF), false))
		return;

	int sel = m_list.GetNextItem(-1, LVNI_SELECTED);
	if(ParentPrj->GetParamKeepStatus(sel) != C_KEEP_FREE)
		return;

	if(!ParentPrj->DeleteParam(sel))
		return;

	FillList(0);
}

void CEditParamListDlg::OnParamsListSelChange(NMHDR* /*pNMHDR*/, LRESULT *pResult)
{
	int sel = m_list.GetNextItem(-1, LVNI_SELECTED);
	if(sel < 0 || sel >= int(ParamsList->size()))
		return;

	GetDlgItem(IDC_EDIT)->EnableWindow(!ParentPrj->IsParamMethodExist(sel));
	GetDlgItem(IDC_REMOVE)->EnableWindow(ParentPrj->GetParamKeepStatus(sel) == C_KEEP_FREE);

	*pResult = 0;
}

void CEditParamListDlg::FillList(int sel)
{
	m_list.DeleteAllItems();
	ItPar it;
	int i;
	for(i=0, it=ParamsList->begin(); it!=ParamsList->end(); ++it, i++)
	{
		m_list.InsertItem(i, it->GetName());
		CString s;
		s = it->GetStdMoistureStr();
		m_list.SetItemText(i, 1, s);
		m_list.SetItemText(i, 2, it->GetUnit());
		s.Format(cFmt1, it->GetIFormat());
		m_list.SetItemText(i, 3, s);
	}
	for(i=0; i<4; i++)
		m_list.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);

	if(sel < 0 || sel >= int(ParamsList->size()))
		sel = 0;

	m_list.SetItemState(sel, LVIS_SELECTED, LVIS_SELECTED);

	if(int(ParamsList->size()) <= 0)
	{
		GetDlgItem(IDC_EDIT)->EnableWindow(false);
		GetDlgItem(IDC_REMOVE)->EnableWindow(false);
	}
	else
	{
		GetDlgItem(IDC_EDIT)->EnableWindow(!ParentPrj->IsParamMethodExist(sel));
		GetDlgItem(IDC_REMOVE)->EnableWindow(ParentPrj->GetParamKeepStatus(sel) == C_KEEP_FREE);
	}
}

void CEditParamListDlg::OnHelp()
{
	int IdHelp = DLGID_PARAM_EDIT_LIST;

	pFrame->ShowHelp(IdHelp);
}
