#include "stdafx.h"

#include "RootData.h"
#include "BranchInfo.h"
#include "CreateTransferDlg.h"
#include "ShowSpectraDlg.h"
#include "LoadSpectraPBDlg.h"
#include "MainFrm.h"

#include <math.h>


static CProjectData *pPrj;
static CString SortName;
static bool SortDir;

static bool iscancel = false;

bool PMSNameUp(CString& SName1, CString& SName2)
{
	CSampleData* Sam1 = pPrj->GetSample(SName1);
	CSampleData* Sam2 = pPrj->GetSample(SName2);

	if(Sam1 == NULL || Sam2 == NULL)
	{
		return (SName1.Compare(SName2) < 0);
	}

	if(SortName == cSortTime)
	{
		CTime t1 = Sam1 ? Sam1->GetLastSpecDate() : 0;
		CTime t2 = Sam2 ? Sam2->GetLastSpecDate() : 0;
		if(t1 == t2)
		{
			return (Sam1->GetName().Compare(Sam2->GetName()) < 0);
		}
		return t1 < t2;
	}


	ReferenceData *Ref1 = Sam1->GetReferenceData(SortName);
	ReferenceData *Ref2 = Sam2->GetReferenceData(SortName);

	if(Ref1 == NULL || Ref2 == NULL)
        return (SName1.Compare(SName2) < 0);

	if(Ref1->bNoValue && !Ref2->bNoValue)
		return true;
	if(Ref2->bNoValue)
		return false;

	return Ref1->Value < Ref2->Value;
}

bool PMSNameDown(CString& SName1, CString& SName2)
{
	CSampleData* Sam1 = pPrj->GetSample(SName1);
	CSampleData* Sam2 = pPrj->GetSample(SName2);

	if(Sam1 == NULL || Sam2 == NULL)
		return (SName1.Compare(SName2) < 0);

	if(SortName == cSortTime)
	{
		CTime t1 = Sam1 ? Sam1->GetLastSpecDate() : 0;
		CTime t2 = Sam2 ? Sam2->GetLastSpecDate() : 0;
		if(t1 == t2)
		{
			return (Sam1->GetName().Compare(Sam2->GetName()) > 0);
		}
		return t1 > t2;
	}


	ReferenceData *Ref1 = Sam1->GetReferenceData(SortName);
	ReferenceData *Ref2 = Sam2->GetReferenceData(SortName);

	if(Ref1 == NULL || Ref2 == NULL)
        return (SName1.Compare(SName2) > 0);

	if(Ref1->bNoValue)
		return false;
	if(Ref2->bNoValue)
		return true;

	return Ref1->Value > Ref2->Value;
}

bool PCSNameUp(ModelSample& ModSam1, ModelSample& ModSam2)
{
	return PMSNameUp(ModSam1.SampleName, ModSam2.SampleName);
}

bool PCSNameDown(ModelSample& ModSam1, ModelSample& ModSam2)
{
	return PMSNameDown(ModSam1.SampleName, ModSam2.SampleName);
}

bool SortPrepSpecAver(TransPrepInfo& PrepInfo1, TransPrepInfo& PrepInfo2)
{
	if(PrepInfo2.DistMean < 0.)
		return true;
	if(PrepInfo1.DistMean < 0.)
		return false;

	return PrepInfo1.DistMean < PrepInfo2.DistMean;
}

bool SortPrepSpecDiff(TransPrepInfo& PrepInfo1, TransPrepInfo& PrepInfo2)
{
	if(PrepInfo2.DistMean < 0.)
		return true;
	if(PrepInfo1.DistMean < 0.)
		return false;

	return (PrepInfo1.DistMax - PrepInfo1.DistMin) < (PrepInfo2.DistMax - PrepInfo2.DistMin);
}

IMPLEMENT_DYNAMIC(CCreateTransferDlg, CDialog)

CCreateTransferDlg::CCreateTransferDlg(CTransferData* trans, CWnd* pParent) : CDialog(CCreateTransferDlg::IDD, pParent)
{
	CalcInfo.TransData = trans;
	PrjData = GetProjectByHItem(trans->GetParentHItem());

	m_vTab = C_TRANSDLG_PAGE_SMPL;

	MDev = NULL;
	MPrj = NULL;
	MMod = NULL;
	pPrj = MPrj;
	nDev = 0;
	nPrj = 0;
	nMod = 0;
	m_vName = ParcelLoadString(STRID_SPEC_TRANS_SAMP_DEFNAME);
	m_vDevice = 0;
	m_vProject = 0;
	m_vModel = 0;
	m_vNoCorr = false;
//	indSort = 0;
	SortDir = 0;
	SortName = cEmpty;

	m_vSort2 = C_SORT_PREP_AVER;
	m_vPrepA = m_vPrepB = m_vPrepM = m_vPrepD = m_vPrepC = m_vPrepN = m_vPrep1 = m_vPrep2 = false;
	m_vPrepNo = true;
	m_vAddPrep = cEmpty;
	m_vMaxLetter = 3;
	MinLimit = CalcInfo.TransData->GetLowLimSpec();
	MaxLimit = CalcInfo.TransData->GetUpperLimSpec();
	m_vLowLim = MinLimit;
	m_vUpperLim = MaxLimit;
	curprep = cEmpty;
	IsAddPreprocValid = true;
}

CCreateTransferDlg::~CCreateTransferDlg()
{
}

void CCreateTransferDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_TAB_1, m_Tab);
	DDX_Control(pDX, IDC_SPEC_TRANS_SAMP_TABLEMASTER, m_listMaster);
	DDX_Control(pDX, IDC_SPEC_TRANS_SAMP_TABLEWORKER, m_listSlave);
	DDX_Control(pDX, IDC_SPECTRUM_LIST, m_listSpec);
	DDX_Control(pDX, IDC_INPUT_NAME, m_Name);
	DDX_Control(pDX, IDC_SELECT_DEVICE, m_Device);
	DDX_Control(pDX, IDC_SELECT_PROJECT, m_Project);
	DDX_Control(pDX, IDC_SELECT_MODEL, m_Model);
	DDX_Control(pDX, IDC_NO_CONV, m_NoCorr);
	DDX_Control(pDX, IDC_SPEC_TRANS_CORR_TABLEPREP, m_listPrep);
	DDX_Control(pDX, IDC_SPEC_TRANS_CORR_TABLEDIST, m_listDist);
	DDX_Control(pDX, IDC_PREP_A, m_PrepA);
	DDX_Control(pDX, IDC_PREP_B, m_PrepB);
	DDX_Control(pDX, IDC_PREP_M, m_PrepM);
	DDX_Control(pDX, IDC_PREP_D, m_PrepD);
	DDX_Control(pDX, IDC_PREP_C, m_PrepC);
	DDX_Control(pDX, IDC_PREP_N, m_PrepN);
	DDX_Control(pDX, IDC_PREP_1, m_Prep1);
	DDX_Control(pDX, IDC_PREP_2, m_Prep2);
	DDX_Control(pDX, IDC_PREP_WITHOUT, m_PrepNo);
	DDX_Control(pDX, IDC_INPUT_PREP, m_AddPrep);
	DDX_Control(pDX, IDC_NUM_LETTER, m_MaxLetter);
	DDX_Control(pDX, IDC_LOWLIM_SPEC, m_LowLim);
	DDX_Control(pDX, IDC_UPPERLIM_SPEC, m_UpperLim);
	DDX_Control(pDX, IDC_SORT_2, m_Sort2);
	DDX_Control(pDX, IDC_SPIN_LETTER, m_SpinLetter);

	DDX_CBIndex(pDX, IDC_SELECT_DEVICE, m_vDevice);
	DDX_CBIndex(pDX, IDC_SELECT_PROJECT, m_vProject);
	DDX_CBIndex(pDX, IDC_SELECT_MODEL, m_vModel);
	DDX_CBIndex(pDX, IDC_SORT_2, m_vSort2);

	cParcelDDX(pDX, IDC_INPUT_NAME, m_vName);
	DDV_MaxChars(pDX, m_vName, 50);
	cParcelDDX(pDX, IDC_INPUT_PREP, m_vAddPrep);
	DDV_MaxChars(pDX, m_vAddPrep, 100);
	cParcelDDX(pDX, IDC_NUM_LETTER, m_vMaxLetter);
	cParcelDDV(pDX, ParcelLoadString(STRID_SPEC_TRANS_CORR_DDV_MAXLETTER), m_vMaxLetter, 1, 7);
	cParcelDDX(pDX, IDC_LOWLIM_SPEC, m_vLowLim);
	cParcelDDV(pDX, ParcelLoadString(STRID_SPEC_TRANS_CORR_DDV_LOWSPEC), m_vLowLim, MinLimit, MaxLimit);
	cParcelDDX(pDX, IDC_UPPERLIM_SPEC, m_vUpperLim);
	cParcelDDV(pDX, ParcelLoadString(STRID_SPEC_TRANS_CORR_DDV_UPPERSPEC), m_vUpperLim, MinLimit, MaxLimit);

	DDX_Check(pDX, IDC_NO_CONV, m_vNoCorr);
	DDX_Check(pDX, IDC_PREP_A, m_vPrepA);
	DDX_Check(pDX, IDC_PREP_B, m_vPrepB);
	DDX_Check(pDX, IDC_PREP_M, m_vPrepM);
	DDX_Check(pDX, IDC_PREP_D, m_vPrepD);
	DDX_Check(pDX, IDC_PREP_C, m_vPrepC);
	DDX_Check(pDX, IDC_PREP_N, m_vPrepN);
	DDX_Check(pDX, IDC_PREP_1, m_vPrep1);
	DDX_Check(pDX, IDC_PREP_2, m_vPrep2);
	DDX_Check(pDX, IDC_PREP_WITHOUT, m_vPrepNo);
}

BEGIN_MESSAGE_MAP(CCreateTransferDlg, CDialog)

	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB_1, OnTabSelchange)
	ON_NOTIFY(HDN_ITEMCLICK, 0, OnAnyTableColumnClick)

	ON_CBN_SELCHANGE(IDC_SELECT_DEVICE, OnSelDevice)
	ON_CBN_SELCHANGE(IDC_SELECT_PROJECT, OnSelProject)
	ON_CBN_SELCHANGE(IDC_SELECT_MODEL, OnSelModel)
	ON_CBN_SELCHANGE(IDC_SORT_2, OnSort2)

	ON_NOTIFY(LVN_ITEMCHANGED, IDC_SPEC_TRANS_SAMP_TABLEWORKER, OnSlaveListSelChange)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_SPECTRUM_LIST, OnSpecListSelChange)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_LETTER, OnSpinLetterDeltaPos)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_SPEC_TRANS_CORR_TABLEPREP, OnPrepListSelChange)

	ON_BN_CLICKED(IDHELP, OnHelp)
	ON_BN_CLICKED(IDC_SELECT, OnSelect)
	ON_BN_CLICKED(IDC_SCORES, OnRemoveM)
	ON_BN_CLICKED(IDC_REMOVE, OnRemove)
	ON_BN_CLICKED(IDC_ADD, OnAdd)
	ON_BN_CLICKED(IDC_EXCLUDE, OnExclude)
	ON_BN_CLICKED(IDC_PLOT_2, OnPlotMaster)
	ON_BN_CLICKED(IDC_PLOT, OnPlotSlave)
	ON_BN_CLICKED(IDC_NO_CONV, OnSetEnablingNoConv)
	ON_BN_CLICKED(IDC_CREATE, OnMake)
	ON_BN_CLICKED(IDC_CALCULATE, OnCorrect)
	ON_BN_CLICKED(IDC_PREP_A, OnCreatePreprocList)
	ON_BN_CLICKED(IDC_PREP_B, OnCreatePreprocList)
	ON_BN_CLICKED(IDC_PREP_M, OnCreatePreprocList)
	ON_BN_CLICKED(IDC_PREP_D, OnCreatePreprocList)
	ON_BN_CLICKED(IDC_PREP_C, OnCreatePreprocList)
	ON_BN_CLICKED(IDC_PREP_N, OnCreatePreprocList)
	ON_BN_CLICKED(IDC_PREP_1, OnCreatePreprocList)
	ON_BN_CLICKED(IDC_PREP_2, OnCreatePreprocList)
	ON_BN_CLICKED(IDC_PREP_WITHOUT, OnCreatePreprocList)

	ON_EN_CHANGE(IDC_NUM_LETTER, OnMaxLetterChange)
	ON_EN_CHANGE(IDC_INPUT_PREP, OnAddPrepChange)

	ON_EN_KILLFOCUS(IDC_LOWLIM_SPEC, OnLowLimChange)
	ON_EN_KILLFOCUS(IDC_UPPERLIM_SPEC, OnUpperLimChange)

END_MESSAGE_MAP()

BOOL CCreateTransferDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	iscancel = false;

	USES_CONVERSION;

	CString s;
	int i;

	m_NoCorr.ShowWindow(false);

	TC_ITEM TabCtrlItem;
	TabCtrlItem.mask = TCIF_TEXT;
//	TabCtrlItem.pszText = A2W(T2A(ParcelLoadString(STRID_SPEC_TRANS_HDR_SAMP)));
	TabCtrlItem.pszText = (LPWSTR)GETSTRUI(STRID_SPEC_TRANS_HDR_SAMP); 
	m_Tab.InsertItem(C_TRANSDLG_PAGE_SMPL, &TabCtrlItem);
//	TabCtrlItem.pszText = A2W(T2A(ParcelLoadString(STRID_SPEC_TRANS_HDR_CORR)));
	TabCtrlItem.pszText = (LPWSTR)GETSTRUI(STRID_SPEC_TRANS_HDR_CORR);
	m_Tab.InsertItem(C_TRANSDLG_PAGE_CORR, &TabCtrlItem);

	s = ParcelLoadString(STRID_SPEC_TRANS_SAMP_COL_NUM);
	m_listMaster.InsertColumn(0, s, LVCFMT_CENTER, 0, 0);
	s = ParcelLoadString(STRID_SPEC_TRANS_SAMP_COL_SAMPLE);
	m_listMaster.InsertColumn(1, s, LVCFMT_LEFT, 0, 1);
	s = ParcelLoadString(STRID_SPEC_TRANS_SAMP_COL_NSPEC_M);
	m_listMaster.InsertColumn(2, s, LVCFMT_CENTER, 0, 2);
	s = ParcelLoadString(STRID_MEAS_DATE);
	m_listMaster.InsertColumn(3, s, LVCFMT_RIGHT, 0, 3);
	m_nTimeCol = 3;
	m_nParStart = 4;
	for(i=0; i<m_nParStart; i++)
	{
		m_listMaster.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	}
	m_listMaster.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);
	m_listMaster.SetIconList();

	s = ParcelLoadString(STRID_SPEC_TRANS_SAMP_COL_NUM);
	m_listSlave.InsertColumn(0, s, LVCFMT_CENTER, 0, 0);
	s = ParcelLoadString(STRID_SPEC_TRANS_SAMP_COL_SAMPLE);
	m_listSlave.InsertColumn(1, s, LVCFMT_LEFT, 0, 1);
	s = ParcelLoadString(STRID_SPEC_TRANS_SAMP_COL_NSPEC_S);
	m_listSlave.InsertColumn(2, s, LVCFMT_CENTER, 0, 2);
	for(i=0; i<3; i++)
	{
		m_listSlave.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	}
	m_listSlave.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);
	m_listSlave.SetIconList();

	s = ParcelLoadString(STRID_SPEC_TRANS_SAMP_COL_SPNUM);
	m_listSpec.InsertColumn(0, s, LVCFMT_LEFT, 0, 0);
	s = ParcelLoadString(STRID_SPEC_TRANS_SAMP_COL_SPUSE);
	m_listSpec.InsertColumn(1, s, LVCFMT_RIGHT, 0, 1);
	s = ParcelLoadString(STRID_SPEC_TRANS_SAMP_COL_SPDATE);
	m_listSpec.InsertColumn(2, s, LVCFMT_RIGHT, 0, 2);
	for(i=0; i<3; i++)
		m_listSpec.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	m_listSpec.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

	s = ParcelLoadString(STRID_SPEC_TRANS_CORR_COL_PREP);
	m_listPrep.InsertColumn(0, s, LVCFMT_LEFT, 0, 0);
	s = ParcelLoadString(STRID_SPEC_TRANS_CORR_COL_AVER);
	m_listPrep.InsertColumn(1, s, LVCFMT_CENTER, 0, 1);
	s = ParcelLoadString(STRID_SPEC_TRANS_CORR_COL_DIFF);
	m_listPrep.InsertColumn(2, s, LVCFMT_CENTER, 0, 2);
	for(i=0; i<3; i++)
		m_listPrep.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	m_listPrep.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);
	m_listPrep.SetIconList();

	s = ParcelLoadString(STRID_SPEC_TRANS_CORR_COL_NUM);
	m_listDist.InsertColumn(0, s, LVCFMT_CENTER, 0, 0);
	s = ParcelLoadString(STRID_SPEC_TRANS_CORR_COL_SAMPLE);
	m_listDist.InsertColumn(1, s, LVCFMT_LEFT, 0, 1);
	s = ParcelLoadString(STRID_SPEC_TRANS_CORR_COL_DEK);
	m_listDist.InsertColumn(2, s, LVCFMT_CENTER, 0, 2);
	for(i=0; i<3; i++)
		m_listDist.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	m_listDist.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

	m_Sort2.ResetContent();
	s.Format(ParcelLoadString(STRID_SPEC_TRANS_CORR_SORT_AVER));
	m_Sort2.AddString(s);
	s.Format(ParcelLoadString(STRID_SPEC_TRANS_CORR_SORT_DIFF));
	m_Sort2.AddString(s);
	m_Sort2.SetCurSel(m_vSort2);

	FillListDevice();

	FillTablePrep(true);

	CWnd* pWindow = GetDlgItem(IDC_NUM_LETTER);
	if(pWindow != 0)
	{
		m_SpinLetter.SetBuddy(pWindow);
	    m_SpinLetter.SetRange(1, 7);
		m_SpinLetter.SetPos(m_vMaxLetter);
	}

	CString Hdr;
	Hdr.Format(ParcelLoadString(STRID_SPEC_TRANS_HDR));
	SetWindowText(Hdr);

	GetDlgItem(IDCANCEL)->SetWindowText(ParcelLoadString(STRID_SPEC_TRANS_EXIT));
	GetDlgItem(IDHELP)->SetWindowText(ParcelLoadString(STRID_SPEC_TRANS_HELP));
	GetDlgItem(IDC_STATIC_4)->SetWindowText(ParcelLoadString(STRID_SPEC_TRANS_SAMP_MASTER));
	GetDlgItem(IDC_STATIC_5)->SetWindowText(ParcelLoadString(STRID_SPEC_TRANS_SAMP_PROJECT));
	GetDlgItem(IDC_STATIC_6)->SetWindowText(ParcelLoadString(STRID_SPEC_TRANS_SAMP_MODEL));
	GetDlgItem(IDC_STATIC_8)->SetWindowText(ParcelLoadString(STRID_SPEC_TRANS_SAMP_SLNAME));
	GetDlgItem(IDC_SELECT)->SetWindowText(ParcelLoadString(STRID_SPEC_TRANS_SAMP_CHOOSE));
	GetDlgItem(IDC_SCORES)->SetWindowText(ParcelLoadString(STRID_SPEC_TRANS_SAMP_REMOVE_M));
	GetDlgItem(IDC_REMOVE)->SetWindowText(ParcelLoadString(STRID_SPEC_TRANS_SAMP_REMOVE));
	GetDlgItem(IDC_ADD)->SetWindowText(ParcelLoadString(STRID_SPEC_TRANS_SAMP_ADD));
	GetDlgItem(IDC_EXCLUDE)->SetWindowText(ParcelLoadString(STRID_SPEC_TRANS_SAMP_EXCLUDE));
	GetDlgItem(IDC_PLOT)->SetWindowText(ParcelLoadString(STRID_SPEC_TRANS_SAMP_PLOT_M));
	GetDlgItem(IDC_PLOT_2)->SetWindowText(ParcelLoadString(STRID_SPEC_TRANS_SAMP_PLOT_S));
	GetDlgItem(IDC_NO_CONV)->SetWindowText(ParcelLoadString(STRID_SPEC_TRANS_SAMP_NOCONV));

	GetDlgItem(IDC_PREP_A)->SetWindowText(ParcelLoadString(STRID_SPEC_TRANS_CORR_PREPROC_A));
	GetDlgItem(IDC_PREP_B)->SetWindowText(ParcelLoadString(STRID_SPEC_TRANS_CORR_PREPROC_B));
	GetDlgItem(IDC_PREP_M)->SetWindowText(ParcelLoadString(STRID_SPEC_TRANS_CORR_PREPROC_M));
	GetDlgItem(IDC_PREP_D)->SetWindowText(ParcelLoadString(STRID_SPEC_TRANS_CORR_PREPROC_D));
	GetDlgItem(IDC_PREP_C)->SetWindowText(ParcelLoadString(STRID_SPEC_TRANS_CORR_PREPROC_C));
	GetDlgItem(IDC_PREP_N)->SetWindowText(ParcelLoadString(STRID_SPEC_TRANS_CORR_PREPROC_N));
	GetDlgItem(IDC_PREP_1)->SetWindowText(ParcelLoadString(STRID_SPEC_TRANS_CORR_PREPROC_1));
	GetDlgItem(IDC_PREP_2)->SetWindowText(ParcelLoadString(STRID_SPEC_TRANS_CORR_PREPROC_2));
	GetDlgItem(IDC_PREP_WITHOUT)->SetWindowText(ParcelLoadString(STRID_SPEC_TRANS_CORR_PREPROC_NO));
	GetDlgItem(IDC_STATIC_9)->SetWindowText(ParcelLoadString(STRID_SPEC_TRANS_CORR_PREPLIST));
	GetDlgItem(IDC_STATIC_10)->SetWindowText(ParcelLoadString(STRID_SPEC_TRANS_CORR_SPECRANGE));
	GetDlgItem(IDC_STATIC_11)->SetWindowText(ParcelLoadString(STRID_SPEC_TRANS_CORR_RESULT));
	GetDlgItem(IDC_STATIC_12)->SetWindowText(ParcelLoadString(STRID_SPEC_TRANS_CORR_SPECLOW));
	GetDlgItem(IDC_STATIC_13)->SetWindowText(ParcelLoadString(STRID_SPEC_TRANS_CORR_SPECUPPER));
	GetDlgItem(IDC_STATIC_14)->SetWindowText(ParcelLoadString(STRID_SPEC_TRANS_CORR_SORTBY));
	GetDlgItem(IDC_STATIC_15)->SetWindowText(ParcelLoadString(STRID_SPEC_TRANS_CORR_ADD));
	GetDlgItem(IDC_STATIC_16)->SetWindowText(ParcelLoadString(STRID_SPEC_TRANS_CORR_MAX));
	GetDlgItem(IDC_STATIC_19)->SetWindowText(ParcelLoadString(STRID_SPEC_TRANS_CORR_PREPTABLE));
	GetDlgItem(IDC_CALCULATE)->SetWindowText(ParcelLoadString(STRID_SPEC_TRANS_CORR_CALCULATE));
	GetDlgItem(IDC_CREATE)->SetWindowText(ParcelLoadString(STRID_SPEC_TRANS_CORR_CREATE));

	GetDlgItem(IDC_CALCULATE)->EnableWindow(false);

	ShowFields(C_TRANSDLG_PAGE_SMPL);

	GetDlgItem(IDHELP)->EnableWindow(pFrame->IsExistHelp());

	return true;
}

void CCreateTransferDlg::OnAccept()
{
	CDialog::OnOK();
}

void CCreateTransferDlg::OnCancel()
{
	iscancel = true;

	CDialog::OnCancel();
}

void CCreateTransferDlg::OnSelect()
{
	int Ind = -1, nsel = m_listMaster.GetSelectedCount(), N = int(MSamNames.size());
	if(nsel <= 0)
		return;

	for(int i=0; i<nsel; i++)
	{
		Ind = m_listMaster.GetNextItem(Ind, LVNI_SELECTED);
		if(Ind < 0 || Ind >= N)
			break;

		CString Name = MSamNames[Ind];
		CSampleData* Sam = PrjData->GetSample(Name);
		if(Sam == NULL)
		{
			CString s;
			s.Format(ParcelLoadString(STRID_SPEC_TRANS_SAMP_ERR_NOSAMPLE), Name);
			ParcelError(s);
			break;
		}
		ItModSam it;
		for(it=SamNames.begin(); it!=SamNames.end(); ++it)
		{
			if(!it->SampleName.Compare(Name))
			{
				it->SetAll(true);
				break;
			}
		}
		if(it == SamNames.end())
		{
			ModelSample NewSamName(Sam);
			SamNames.push_back(NewSamName);
		}
	}

	SortCorrSamples();
	FillTableSpec();
	CreateCorrFields();
}

void CCreateTransferDlg::OnRemoveM()
{
	int Ind = -1, nsel = m_listMaster.GetSelectedCount(), N = int(MSamNames.size());
	if(nsel <= 0)
		return;

	for(int i=0; i<nsel; i++)
	{
		Ind = m_listMaster.GetNextItem(Ind, LVNI_SELECTED);
		if(Ind < 0 || Ind >= N)
			break;

		CString Name = MSamNames[Ind - i];
		ItModSam it;
		for(it=SamNames.begin(); it!=SamNames.end(); ++it)
		{
			if(!it->SampleName.Compare(Name))
			{
				SamNames.erase(it);
				break;
			}
		}

		ItStr itM = MSamNames.begin() + (Ind - i);
		MSamNames.erase(itM);
	}

//	FillTableMaster(true);
	FillTableMaster(false);
	SortCorrSamples();
	FillTableSpec();
	CreateCorrFields();
}

void CCreateTransferDlg::OnRemove()
{
	int i, Ind = -1, nsel = m_listSlave.GetSelectedCount(), N = int(SamNames.size());
	if(nsel <= 0)
		return;

	for(i=0; i<nsel; i++)
	{
		Ind = m_listSlave.GetNextItem(Ind, LVNI_SELECTED);
		if(Ind < 0 || Ind >= N)
			break;

		ItModSam it = SamNames.begin() + (Ind - i);
		SamNames.erase(it);
	}

	int sel = Ind - i + 1;
	if(sel < 0)  sel = 0;
	if(sel >= int(SamNames.size()))  sel = int(SamNames.size()) - 1;
	FillTableSlave(sel);

	CreateCorrFields();
}

void CCreateTransferDlg::OnAdd()
{
}

void CCreateTransferDlg::OnExclude()
{
	int sel = m_listSlave.GetNextItem(-1, LVNI_SELECTED);
	if(sel < 0 || sel >= int(SamNames.size()))
		return;

	ModelSample* CorrSam = &SamNames[sel];

	int sel1 = m_listSpec.GetNextItem(-1, LVNI_SELECTED);
	if(sel1 < 0 || sel1 >= int(CorrSam->Spectra.size()))
		return;

	ItIntBool CurSpec = CorrSam->Spectra.begin();
	for(int i=0; i<sel1; i++)
		++CurSpec;

	//	if "insert", check for number of the master device spectra
	if(!CurSpec->second)
	{
		if(MMod != NULL)
		{
			ModelSample *pModSamp = MMod->GetSampleForCalib(CorrSam->SampleName, cOwn);
			if(!pModSamp)
			{
				CString str;
				str.Format(GETSTRUI(1176), CorrSam->SampleName, MMod->GetName());
				AfxMessageBox(str);
				return;
			}
			if(pModSamp->GetNSpectraUsed() < (CorrSam->GetNSpectraUsed()+1))
			{
				CString str;
				str.Format(GETSTRUI(1178), pModSamp->GetNSpectraUsed());
				AfxMessageBox(str);
				return;
			}
		}
		else
		{
			CSampleData *pPrjSamp = MPrj->GetSample(CorrSam->SampleName);
			if(!pPrjSamp)
			{
				CString str;
				str.Format(GETSTRUI(1177), CorrSam->SampleName, MPrj->GetName());
				AfxMessageBox(str);
				return;
			}
			if(pPrjSamp->GetNSpectraUsed() < (CorrSam->GetNSpectraUsed()+1))
			{
				CString str;
				str.Format(GETSTRUI(1178), pPrjSamp->GetNSpectraUsed());
				AfxMessageBox(str);
				return;
			}
		}
	}

	//	proceed 
	CurSpec->second = !CurSpec->second;

	if(CorrSam->GetNSpectraUsed() <= 0)
	{
		ItModSam it = SamNames.begin() + sel;
		SamNames.erase(it);

		if(sel < 0)  sel = 0;
		if(sel >= int(SamNames.size()))  sel = int(SamNames.size()) - 1;
		FillTableSlave(sel);
		CreateCorrFields();
		GetDlgItem(IDC_SPEC_TRANS_SAMP_TABLEWORKER)->SetFocus();

		return;
	}

	CString s = (CurSpec->second) ? ParcelLoadString(STRID_SPEC_TRANS_SAMP_EXCLUDE) :
		ParcelLoadString(STRID_SPEC_TRANS_SAMP_INCLUDE);
	GetDlgItem(IDC_EXCLUDE)->SetWindowText(s);

	FillTableSlave(sel);

	s = (CurSpec->second) ? ParcelLoadString(STRID_SPEC_TRANS_SAMP_YES) : ParcelLoadString(STRID_SPEC_TRANS_SAMP_NO);
	m_listSpec.SetItemText(sel1, 1, s);

	CreateCorrFields();

	GetDlgItem(IDC_SPECTRUM_LIST)->SetFocus();
}

void CCreateTransferDlg::OnPlotMaster()
{
	if(int(MSamNames.size()) <= 0)
		return;

	CTransferData* pTrans = MPrj->GetTransOwn();
	if(pTrans == NULL)
		return;
	VLSI PSam;
	ItStr it;
	ItSpc it4;
	for(it=MSamNames.begin(); it!=MSamNames.end(); ++it)
	{
		CSampleData* Sam = pTrans->GetSample(*it);
		if(Sam == NULL)
			return;


		LoadSampleInfo NewLSI;
		NewLSI.pSam = Sam;

		for(it4=Sam->SpecData.begin(); it4!=Sam->SpecData.end(); ++it4)
		{
			NewLSI.Nums.push_back(it4->Num);
		}

		PSam.push_back(NewLSI);
	}

	CLoadSpectraPBDlg dlg2(&PSam, this);
	if(dlg2.DoModal() != IDOK)
	{
		for(it=MSamNames.begin(); it!=MSamNames.end(); ++it)
		{
			CSampleData* Sam = pTrans->GetSample(*it);
			if(Sam == NULL)
				return;

			Sam->FreeSpectraData();
		}

		return;
	}

	CShowSpectra ShowSp(&pTrans->SpecPoints);

	int i;
	for(i=0, it=MSamNames.begin(); it!=MSamNames.end(); ++it, i++)
	{
		int shift = MPrj->GetSpecPointInd(pTrans->SpecPoints[0]);

		CSampleData* Sam = pTrans->GetSample(*it);
		if(Sam == NULL)
			return;

		ModelSample* ModSam = MMod ? MMod->GetSampleForCalib(*it, cOwn) : NULL;

		int type = C_SPECTRA_TYPE_SIMPLE;

		Sam->LoadSpectraData();

		bool issel = (m_listMaster.GetItemState(i, LVIS_SELECTED) == LVIS_SELECTED);

		for(it4=Sam->SpecData.begin(); it4!=Sam->SpecData.end(); ++it4)
		{
			bool bUseSpec = it4->bUse;
			if(ModSam)
			{
				bUseSpec = ModSam->IsSpectrumUsed(it4->Num);
			}

			if(bUseSpec)
			{
				SpectraShowSettings* NewSS = ShowSp.AddSpectraShow((*it), it4->Num, 0, shift, issel, type);
				if(NewSS == NULL)
					continue;

				copy(it4->Data.begin(), it4->Data.end(), inserter(NewSS->SpecData, NewSS->SpecData.begin()));
			}
		}
	}

	CShowSpectraDlg dlg(&ShowSp, C_PLOT_SET_MODE_SAMPLE, this);
	dlg.DoModal();

	for(it=MSamNames.begin(); it!=MSamNames.end(); ++it)
	{
		CSampleData* Sam = pTrans->GetSample(*it);
		if(Sam == NULL)
			return;

		Sam->FreeSpectraData();
	}
}

void CCreateTransferDlg::OnPlotSlave()
{
	if(int(SamNames.size()) <= 0)
		return;

	CTransferData* pTrans = PrjData->GetTransOwn();
	if(pTrans == NULL)
		return;

	VLSI PSam;
	ItModSam it;
	for(it=SamNames.begin(); it!=SamNames.end(); ++it)
	{
		CSampleData* Sam = pTrans->GetSample(it->SampleName);
		if(Sam == NULL)
			return;

		LoadSampleInfo NewLSI;
		NewLSI.pSam = Sam;

		ItIntBool it4;
		for(it4=it->Spectra.begin(); it4!=it->Spectra.end(); ++it4)
			NewLSI.Nums.push_back(it4->first);

		PSam.push_back(NewLSI);
	}

	CLoadSpectraPBDlg dlg2(&PSam, this);
	if(dlg2.DoModal() != IDOK)
	{
		for(it=SamNames.begin(); it!=SamNames.end(); ++it)
		{
			CSampleData* Sam = pTrans->GetSample(it->SampleName);
			if(Sam == NULL)
				return;

			Sam->FreeSpectraData();
		}

		return;
	}

	CShowSpectra ShowSp(&pTrans->SpecPoints);

	int i;
	for(i=0, it=SamNames.begin(); it!=SamNames.end(); ++it, i++)
	{
		int shift = PrjData->GetSpecPointInd(pTrans->SpecPoints[0]);

		CSampleData* Sam = pTrans->GetSample(it->SampleName);
		if(Sam == NULL)
			return;

		int type = C_SPECTRA_TYPE_SIMPLE;

		Sam->LoadSpectraData();

		bool issel = (m_listSlave.GetItemState(i, LVIS_SELECTED) == LVIS_SELECTED);

		ItSpc it3;
		for(it3=Sam->SpecData.begin(); it3!=Sam->SpecData.end(); ++it3)
			if(it3->bUse)
			{
				SpectraShowSettings* NewSS = ShowSp.AddSpectraShow(it->SampleName, it3->Num, 0, shift, issel, type);
				if(NewSS == NULL)
					continue;

				copy(it3->Data.begin(), it3->Data.end(), inserter(NewSS->SpecData, NewSS->SpecData.begin()));
			}
	}

	CShowSpectraDlg dlg(&ShowSp, C_PLOT_SET_MODE_SAMPLE, this);
	dlg.DoModal();

	for(it=SamNames.begin(); it!=SamNames.end(); ++it)
	{
		CSampleData* Sam = pTrans->GetSample(it->SampleName);
		if(Sam == NULL)
			return;

		Sam->FreeSpectraData();
	}
}

void CCreateTransferDlg::OnSelDevice()
{
	int sel = m_Device.GetCurSel();
	if(sel == m_vDevice || sel < 0 || sel >= int(MDevNames.size()))
		return;

	m_vDevice = sel;
	MDev = (nDev > 0) ? pRoot->GetDevice(MDevNames[m_vDevice]) : NULL;

	FillListProject();
}

void CCreateTransferDlg::OnSelProject()
{
	int sel = m_Project.GetCurSel();
	if(sel == m_vProject)
		return;

	m_vProject = sel;
	MPrj = (MDev != NULL && nPrj > 0) ? MDev->GetProject(MPrjNames[m_vProject]) : NULL;
	pPrj = MPrj;

	FillListModel();
}

void CCreateTransferDlg::OnSelModel()
{
	int sel = m_Model.GetCurSel();
	if(sel == m_vModel)
		return;

	m_vModel = sel;
	MMod = (MPrj != NULL && m_vModel > 0) ? MPrj->GetModel(MModNames[m_vModel - 1]) : NULL;

	GetMSamPar();
//	FillListSort();
	FindSamples();
}

void CCreateTransferDlg::OnSlaveListSelChange(NMHDR* /*pNMHDR*/, LRESULT *pResult)
{
	FillTableSpec();

	*pResult = 0;
}

void CCreateTransferDlg::OnSpecListSelChange(NMHDR* /*pNMHDR*/, LRESULT *pResult)
{
	int sel = m_listSlave.GetNextItem(-1, LVNI_SELECTED);
	if(sel < 0 || sel >= int(SamNames.size()))
		return;

	ModelSample* CorrSam = &SamNames[sel];

	int sel1 = m_listSpec.GetNextItem(-1, LVNI_SELECTED);
	if(sel1 < 0 || sel1 >= int(CorrSam->Spectra.size()))
		return;

	ItIntBool CurSpec = CorrSam->Spectra.begin();
	for(int i=0; i<sel1; i++)
		++CurSpec;

	EnableFields();

	*pResult = 0;
}

void CCreateTransferDlg::OnSetEnablingNoConv()
{
	m_vNoCorr = m_NoCorr.GetCheck();

	FindSamples();
}

void CCreateTransferDlg::OnSortSamples()
{
	if(!UpdateData())
		return;

	if(SortDir)
		sort(MSamNames.begin(), MSamNames.end(), PMSNameUp);
	else
		sort(MSamNames.begin(), MSamNames.end(), PMSNameDown);

	FillTableMaster(true);

	SortCorrSamples();
}

void CCreateTransferDlg::OnAnyTableColumnClick(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NMLISTVIEW *pLV = (NMLISTVIEW *) pNMHDR;

	int item = pLV->iItem;

	bool findsort = false;
	switch(m_vTab)
	{
	case C_TRANSDLG_PAGE_SMPL:
		if(item == 1)
		{
			if(SortName.IsEmpty())
				SortDir = !SortDir;
			SortName = cEmpty;
			findsort = true;
		}
		else if(item == m_nTimeCol)
		{
			if(SortName == cSortTime)
			{
				SortDir = !SortDir;
			}
			SortName = cSortTime;
			findsort = true;
		}
		else if(item >= m_nParStart)
		{
			int ipar = item - m_nParStart;
			if(!MParNames[ipar].Compare(SortName))
			{
				SortDir = !SortDir;
			}
			SortName = MParNames[ipar];
			findsort = true;
		}
		if(findsort)
		{
			GetSamSel();
			if(SortDir)
			{
				sort(MSamNames.begin(), MSamNames.end(), PMSNameUp);
			}
			else
			{
				sort(MSamNames.begin(), MSamNames.end(), PMSNameDown);
			}
			FillTableMaster(false);
			SortCorrSamples();
		}
		break;
	case C_TRANSDLG_PAGE_CORR:
		if(item == 1)
		{
			m_vSort2 = C_SORT_PREP_AVER;
			findsort = true;
		}
		if(item == 2)
		{
			m_vSort2 = C_SORT_PREP_DIFF;
			findsort = true;
		}
		if(findsort)
		{
			m_Sort2.SetCurSel(m_vSort2);
			FillTablePrep(false);
		}
		break;
	}

	*pResult = 0;
}

void CCreateTransferDlg::SortCorrSamples()
{
	if(SortDir)
	{
		sort(SamNames.begin(), SamNames.end(), PCSNameUp);
	}
	else
	{
		sort(SamNames.begin(), SamNames.end(), PCSNameDown);
	}

	FillTableSlave(0);
	CreateCorrFields();
}

void CCreateTransferDlg::FillListDevice()
{
	int i;
	CString s;

	MDevNames.clear();
	ItDev itD;
	for(itD=pRoot->Devices.begin(); itD!=pRoot->Devices.end(); ++itD)
	{
		if(itD->GetHItem() == PrjData->GetParentHItem())
			continue;

		if(itD->IsReadyForTrans(PrjData))
			MDevNames.push_back(itD->GetName());
	}

	m_Device.ResetContent();

	nDev = int(MDevNames.size());
	for(i=0; i<nDev; i++)
		m_Device.AddString(MDevNames[i]);
	if(nDev == 0)
	{
		s = ParcelLoadString(STRID_SPEC_TRANS_SAMP_DASH);
		m_Device.AddString(s);
	}

	m_vDevice = 0;
	m_Device.SetCurSel(m_vDevice);
	MDev = (nDev > 0) ? pRoot->GetDevice(MDevNames[m_vDevice]) : 0;

	FillListProject();
}

void CCreateTransferDlg::FillListProject()
{
	int i;
	CString s;

	MPrjNames.clear();

	if(MDev != NULL)
	{
		ItPrj itP;
		for(itP=MDev->Projects.begin(); itP!=MDev->Projects.end(); ++itP)
		{
			if(itP->IsReadyForTrans(PrjData))
				MPrjNames.push_back(itP->GetName());
		}
	}

	m_Project.ResetContent();

	nPrj = int(MPrjNames.size());
	for(i=0; i<nPrj; i++)
		m_Project.AddString(MPrjNames[i]);
	if(nPrj == 0)
	{
		s = ParcelLoadString(STRID_SPEC_TRANS_SAMP_DASH);
		m_Project.AddString(s);
	}

	m_vProject = 0;
	m_Project.SetCurSel(m_vProject);
	MPrj = (MDev != NULL && nPrj > 0) ? MDev->GetProject(MPrjNames[m_vProject]) : NULL;
	pPrj = MPrj;

	FillListModel();
}

void CCreateTransferDlg::FillListModel()
{
	int i;
	CString s;

	MModNames.clear();

	if(MPrj != NULL)
	{
		ItMod itM;
		for(itM=MPrj->Models.begin(); itM!=MPrj->Models.end(); ++itM)
		{
			if(itM->IsReadyForTrans())
				MModNames.push_back(itM->GetName());
		}
	}

	m_Model.ResetContent();
	m_Model.AddString(ParcelLoadString(STRID_SPEC_TRANS_SAMP_DASH));
	nMod = int(MModNames.size());
	for(i=0; i<nMod; i++)
		m_Model.AddString(MModNames[i]);

	m_vModel = 0;
	m_Model.SetCurSel(m_vModel);
	MMod = (MPrj != NULL && m_vModel > 0) ? MPrj->GetModel(MModNames[m_vModel - 1]) : NULL;

	GetMSamPar();
//	FillListSort();
	FindSamples();
}
/*
void CCreateTransferDlg::FillListSort()
{
	indSort = GetSortInd(MPrj->GetSamSortName(), MPrj->GetSamSortDir());
}
*/
int CCreateTransferDlg::GetSamSel()
{
	SamSel.clear();
	int Ind = -1, nsel = m_listMaster.GetSelectedCount();
	if(nsel <= 0)
		return 0;

	for(int i=0; i<nsel; i++)
	{
		Ind = m_listMaster.GetNextItem(Ind, LVNI_SELECTED);
		if(Ind < 0) 
		{
			break;
		}
		SamSel.push_back(m_listMaster.GetItemText(Ind, 1));
	}
	return nsel;
}

void CCreateTransferDlg::FillTableMaster(bool bFromScratch)
{
	int i, j;
	CString s;

	if(bFromScratch)
	{
		m_listMaster.DeleteAllItems();

		int nColumnCount = m_listMaster.GetHeaderCtrl()->GetItemCount();
		if(nColumnCount < m_nParStart)
		{
			s = ParcelLoadString(STRID_SPEC_TRANS_SAMP_COL_NUM);
			m_listMaster.InsertColumn(0, s, LVCFMT_CENTER, 0, 0);
			s = ParcelLoadString(STRID_SPEC_TRANS_SAMP_COL_SAMPLE);
			m_listMaster.InsertColumn(1, s, LVCFMT_LEFT, 0, 1);
			s = ParcelLoadString(STRID_SPEC_TRANS_SAMP_COL_NSPEC_M);
			m_listMaster.InsertColumn(2, s, LVCFMT_CENTER, 0, 2);
			s = ParcelLoadString(STRID_MEAS_DATE);
			m_listMaster.InsertColumn(3, s, LVCFMT_RIGHT, 0, 3);
			m_nTimeCol = 3;
			m_nParStart = 4;
		}
		for(i=m_nParStart; i<nColumnCount; i++)
		{
			m_listMaster.DeleteColumn(m_nParStart);
		}

		ItStr itP;
		i=0;
		for(itP=MParNames.begin(); itP!=MParNames.end(); ++itP)
		{
			m_listMaster.InsertColumn(i + m_nParStart, (*itP), LVCFMT_RIGHT, 0, i + m_nParStart);
			i++;
		}
	}
//	m_listMaster.DeleteAllItems();

	int NS = int(MSamNames.size()), NSp = 0;
	int NP = int(MParNames.size());

	ItStr it;
	for(i=0, it=MSamNames.begin(); it!=MSamNames.end(); ++it, i++)
	{
		CSampleData* Sam = MPrj->GetSample(*it);
		if(Sam == NULL)
			continue;

		s.Format(cFmt1, i+1);
		if(bFromScratch)
		{
			m_listMaster.InsertItem(i, s);
		}
		else
		{
			m_listMaster.SetItemText(i, 0, s);
		}

		s = Sam->GetName();
		m_listMaster.SetItemText(i, 1, s);
		if(!bFromScratch)
		{
			m_listMaster.SetItemState(i, 1, LVIS_SELECTED);
			int ns = SamSel.size();
			for(int k=0; k<ns; k++)
			{
				if(s == SamSel[k])
				{
					m_listMaster.SetItemState(i, LVIS_SELECTED, LVIS_SELECTED);
					break;
				}
			}
		}

		int ns = Sam->GetNSpectraUsed();
		ModelSample* ModSam = MMod ? MMod->GetSampleForCalib(*it, cOwn) : NULL;
		if(ModSam)
		{
			int ns1 = ModSam->GetNSpectraUsed(); 
			s.Format(cFmt1_1, ns1, ns);
			NSp += ns1;
		}
		else
		{
			s.Format(cFmt1, ns);
			NSp += ns;
		}
		m_listMaster.SetItemText(i, 2, s);

		if(Sam->GetNSpectra())
		{
			CTime t = Sam->GetLastSpecDate();
			s = t.Format(cFmtDate2);
		}
		else
		{
			s = L"--";
		}
		m_listMaster.SetItemText(i, m_nTimeCol, s);

		ItStr itP;
		for(j=0, itP=MParNames.begin(); itP!=MParNames.end(); ++itP, j++)
		{
			s = Sam->GetReferenceDataStr(*itP);
			m_listMaster.SetItemText(i, j + m_nParStart, s);
		}
	}

	MasterSetSortIcons();
	for(i=0; i<NP+3; i++)
	{
		m_listMaster.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	}

	if(bFromScratch)
	{
		m_listMaster.SetItemState(0, LVIS_SELECTED, LVIS_SELECTED);
	}

	CString s1 = L"<";
	s1 += (MMod != NULL) ? MMod->GetName() : MPrj->GetName();
	if(s1.GetLength() > 26)
	{
		s1.Truncate(26);
		s1 += L"...";
	}
	s1 += L">";
	s.Format(GETSTRUI(MMod ? 372 : 1175), s1, NS, NSp);
	GetDlgItem(IDC_STATIC_1)->SetWindowText(s);
}

void CCreateTransferDlg::FillTableSlave(int sel)
{
	m_listSlave.DeleteAllItems();
	ClearTableSpec();

	if(m_vNoCorr)
		return;

	int NS = int(SamNames.size()), NSp = 0;

	int i;
	CString s;
	ItModSam it;
	for(i=0, it=SamNames.begin(); it!=SamNames.end(); ++it, i++)
	{
		s.Format(cFmt1, i+1);
		m_listSlave.InsertItem(i, s);
		m_listSlave.SetItemText(i, 1, it->SampleName);
		s.Format(cFmt1_1, it->GetNSpectraUsed(), it->GetNSpectra());
		m_listSlave.SetItemText(i, 2, s);

		NSp += it->GetNSpectraUsed();
	}

	SlaveSetSortIcons();
	for(i=0; i<3; i++)
		m_listSlave.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);

	if(sel < 0 || sel >= int(SamNames.size()))
		sel = 0;
	m_listSlave.SetItemState(sel, LVIS_SELECTED, LVIS_SELECTED);

	s.Format(ParcelLoadString(STRID_SPEC_TRANS_SAMP_SLREGR), NS, NSp);
	GetDlgItem(IDC_STATIC_2)->SetWindowText(s);

	if(i > 0)
		FillTableSpec();
	else
		ClearTableSpec();
}

void CCreateTransferDlg::ClearTableSpec()
{
	m_listSpec.DeleteAllItems();

	CString s;
	s.Format(ParcelLoadString(STRID_SPEC_TRANS_SAMP_SPECTRA), cEmpty);
	GetDlgItem(IDC_STATIC_3)->SetWindowText(s);
}

void CCreateTransferDlg::FillTableSpec()
{
	ClearTableSpec();

	int sel = m_listSlave.GetNextItem(-1, LVNI_SELECTED);
	if(sel < 0 || sel >= int(SamNames.size()))
		return;

	ModelSample* CorrSam = &SamNames[sel];
	CSampleData* Sam = PrjData->GetSample(CorrSam->SampleName);
	if(Sam == NULL)
		return;

	CString s;

	s.Format(ParcelLoadString(STRID_SPEC_TRANS_SAMP_SPECTRA), Sam->GetName());
	GetDlgItem(IDC_STATIC_3)->SetWindowText(s);

	ItIntBool it;
	int i;
	for(i=0, it=CorrSam->Spectra.begin(); it!=CorrSam->Spectra.end(); ++it, i++)
	{
		Spectrum *Spec = Sam->GetSpectrumByNum(it->first);

		s.Format(cFmt02, Spec->Num);
		m_listSpec.InsertItem(i, s);

		s = (it->second) ? ParcelLoadString(STRID_SPEC_TRANS_SAMP_YES) : ParcelLoadString(STRID_SPEC_TRANS_SAMP_NO);
		m_listSpec.SetItemText(i, 1, s);
		s = Spec->DateTime.Format(cFmtDate);
		m_listSpec.SetItemText(i, 2, s);
	}

	if(i > 0)
	{
		m_listSpec.SetItemState(0, LVIS_SELECTED, LVIS_SELECTED);

		s = cEmpty;
		bool bEn = true;
		if(CorrSam->Spectra.begin()->second)
		{
			s = ParcelLoadString(STRID_SPEC_TRANS_SAMP_EXCLUDE);
			bEn = true;
		}
		else
		{
			s = ParcelLoadString(STRID_SPEC_TRANS_SAMP_INCLUDE);
			bEn = (CorrSam->GetNSpectraUsed() < MPrj->GetSample(CorrSam->SampleName)->GetNSpectraUsed());
		}
		GetDlgItem(IDC_EXCLUDE)->SetWindowText(s);
		GetDlgItem(IDC_EXCLUDE)->EnableWindow(bEn);
	}
}

void CCreateTransferDlg::GetMSamPar()
{
	MSamNames.clear();
	MParNames.clear();

	if(MPrj == NULL)
		return;

	int i;
	if(MMod != NULL)
	{
		int nSam = MMod->GetNSamplesForCalib();
		for(i=0; i<nSam; i++)
			MSamNames.push_back(MMod->GetSampleForCalib(i)->SampleName);
		int nPar = MMod->GetNModelParams();
		for(i=0; i<nPar; i++)
			MParNames.push_back(MMod->GetModelParam(i)->ParamName);
	}
	else
	{
		int nSam = MPrj->GetNSamples();
		for(i=0; i<nSam; i++)
			MSamNames.push_back(MPrj->GetSample(i)->GetName());
		int nPar = MPrj->GetNParams();
		for(i=0; i<nPar; i++)
			MParNames.push_back(MPrj->GetParam(i)->GetName());
	}

//	indSort = GetSortInd(MPrj->GetSamSortName(), MPrj->GetSamSortDir());
	OnSortSamples();
}

void CCreateTransferDlg::FindSamples()
{
	SamNames.clear();
	m_listSlave.DeleteAllItems();
	ClearTableSpec();

	if(m_vNoCorr)
	{
		FillTableSlave(0);
		CreateCorrFields();
		return;
	}

	CTransferData* Trans = PrjData->GetTransOwn();

	ItSam it;
	for(it=Trans->Samples.begin(); it!=Trans->Samples.end(); ++it)
	{
		CString Name = it->GetName();
		if(MPrj->GetSample(Name) != NULL)
		{
			ModelSample* pModClbSamp = NULL;
			if(MMod != NULL)
			{
				pModClbSamp = MMod->GetSampleForCalib(Name, cOwn);
				if(pModClbSamp == NULL) continue;
			}

			if(find(MSamNames.begin(), MSamNames.end(), Name) == MSamNames.end())
				continue;

			ModelSample NewSamName(&(*it));
			NewSamName.SetAll(false);
			CSampleData *Sam = MPrj->GetSample(NewSamName.SampleName);
			ItIntBool it2;
			int i;
			for(i=0, it2=NewSamName.Spectra.begin(); it2!=NewSamName.Spectra.end() && i<Sam->GetNSpectraUsed(); ++it2, i++)
			{
				if(!pModClbSamp)
				{
					it2->second = true;
				}
				else
				{
					it2->second = pModClbSamp->Spectra[it2->first];
				}
			}
			SamNames.push_back(NewSamName);
		}
	}

	FillTableSlave(0);
	CreateCorrFields();
}

void CCreateTransferDlg::OnSort2()
{
	if(!UpdateData())
		return;

	FillTablePrep(false);
}

void CCreateTransferDlg::OnMake()
{
	if(!AcceptData())
		return;

	if(!IsAddPreprocValid)
	{
		ParcelError(ParcelLoadString(STRID_SPEC_TRANS_CORR_ERR_ADDPREPS));
		return;
	}
	if(!CalcInfo.LoadSpectraData())
		return;

	bool res = CalcInfo.CalculateCorrection();
	if(res)
		GetDlgItem(IDC_CALCULATE)->EnableWindow(true);

	CalcInfo.FreeSpectraData();

	FillTablePrep(true);
/*
	if(MMod != NULL)
	{
		if(IDOK == AfxMessageBox(L"Transfer the source model also?", MB_ICONEXCLAMATION|MB_YESNO))
		{
		}
	}
*/
}

void CCreateTransferDlg::OnMaxLetterChange()
{
	OnCreatePreprocList();
}

void CCreateTransferDlg::OnAddPrepChange()
{
	if(!UpdateData())
		return;

	CreatePreprocListAdd(m_vAddPrep);
}

void CCreateTransferDlg::OnCreatePreprocList()
{
	if(!UpdateData())
		return;

	PreprocAval.clear();

	if(m_vPrepA)  PreprocAval.push_back(C_PREPROC_A);
	if(m_vPrepB)  PreprocAval.push_back(C_PREPROC_B);
	if(m_vPrepM)  PreprocAval.push_back(C_PREPROC_M);
	if(m_vPrepD)  PreprocAval.push_back(C_PREPROC_D);
	if(m_vPrepC)  PreprocAval.push_back(C_PREPROC_C);
	if(m_vPrepN)  PreprocAval.push_back(C_PREPROC_N);
	if(m_vPrep1)  PreprocAval.push_back(C_PREPROC_1);
	if(m_vPrep2)  PreprocAval.push_back(C_PREPROC_2);

	PreprocMaxLen = min(m_vMaxLetter, int(PreprocAval.size()));

	PreprocList.clear();

	VInt Old;
	Old.clear();
	CreatePreprocList(&Old);
	CreatePreprocListAdd(m_vAddPrep);
}

void CCreateTransferDlg::OnSpinLetterDeltaPos(NMHDR *pNMHDR, LRESULT *pResult)
{
   if(pNMHDR == 0 || pResult == 0) return;

   NMUPDOWN *pNMUpDown = (NMUPDOWN *) pNMHDR;

   pNMUpDown->iDelta *= 1;
}

void CCreateTransferDlg::OnPrepListSelChange(NMHDR* /*pNMHDR*/, LRESULT *pResult)
{
	int sel = m_listPrep.GetNextItem(-1, LVNI_SELECTED);

	FillTableDist(sel);

	*pResult = 0;
}

void CCreateTransferDlg::OnLowLimChange()
{
	if(iscancel)
		return;
	if(GetFocus() == GetDlgItem(IDCANCEL))
	{
		OnCancel();
		return;
	}

	bool warn = (GetFocus() == GetDlgItem(IDC_CALCULATE));

	int old = m_vLowLim;
	if(!UpdateData())
		return;

	if(old != m_vLowLim)
	{
		CalcInfo.Preprocs.clear();
		FillTablePrep(true);
		GetDlgItem(IDC_CALCULATE)->EnableWindow(false);

		CreateCorrFields();

		if(warn && !m_vNoCorr)
			ParcelError(ParcelLoadString(STRID_SPEC_TRANS_CORR_RANGECHANGED));
	}
}

void CCreateTransferDlg::OnUpperLimChange()
{
	if(iscancel)
		return;
	if(GetFocus() == GetDlgItem(IDCANCEL))
	{
		OnCancel();
		return;
	}

	bool warn = (GetFocus() == GetDlgItem(IDC_CALCULATE));

	int old = m_vUpperLim;
	if(!UpdateData())
		return;

	if(old != m_vUpperLim)
	{
		CalcInfo.Preprocs.clear();
		FillTablePrep(true);
		GetDlgItem(IDC_CALCULATE)->EnableWindow(false);

		CreateCorrFields();

		if(warn && !m_vNoCorr)
			ParcelError(ParcelLoadString(STRID_SPEC_TRANS_CORR_RANGECHANGED));
	}
}

void CCreateTransferDlg::FillTablePrep(bool bFromScratch)
{
	if(m_vSort2 == C_SORT_PREP_AVER)
		sort(CalcInfo.Preprocs.begin(), CalcInfo.Preprocs.end(), SortPrepSpecAver);
	if(m_vSort2 == C_SORT_PREP_DIFF)
		sort(CalcInfo.Preprocs.begin(), CalcInfo.Preprocs.end(), SortPrepSpecDiff);

	if(bFromScratch) m_listPrep.DeleteAllItems();

	CString s;
	int i;
	ItTransPrepInfo it;
	for(i=0, it=CalcInfo.Preprocs.begin(); it!=CalcInfo.Preprocs.end(); ++it, i++)
	{
		s = GetPreprocLine(&it->Preproc);
		if(s.IsEmpty())
		{
			s = ParcelLoadString(STRID_SPEC_TRANS_CORR_NOPREP);
		}

		if(bFromScratch)
		{
			m_listPrep.InsertItem(i, s);
		}
		else
		{
			m_listPrep.SetItemText(i, 0, s);
		}

		if(it->DistMean < 0)
		{
			m_listPrep.SetItemText(i, 1, ParcelLoadString(STRID_SPEC_TRANS_CORR_NODATA));
			m_listPrep.SetItemText(i, 2, ParcelLoadString(STRID_SPEC_TRANS_CORR_NODATA));
		}
		else
		{
			s.Format(cFmt86, it->DistMean);
			m_listPrep.SetItemText(i, 1, s);
			s.Format(cFmt86, it->DistMax - it->DistMin);
			m_listPrep.SetItemText(i, 2, s);
		}
	}

	PrepSetSortIcons();
	for(i=0; i<3; i++)
		m_listPrep.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);

	m_listPrep.SetItemState(0, LVIS_SELECTED, LVIS_SELECTED);

	FillTableDist(0);
}

void CCreateTransferDlg::FillTableDist(int sel)
{
	m_listDist.DeleteAllItems();
	GetDlgItem(IDC_STATIC_18)->SetWindowText(cEmpty);

	TransPrepInfo* PrepInfo = CalcInfo.GetPrepInfo(sel);
	if(PrepInfo == NULL || PrepInfo->DistMean < 0)
	{
		curprep = cEmpty;
		GetDlgItem(IDC_CALCULATE)->EnableWindow(false);
		return;
	}

	GetDlgItem(IDC_CALCULATE)->EnableWindow(true);

	CString s;
	int i;
	ItSCDist it;
	for(i=0, it=PrepInfo->DistSpec.begin(); it!=PrepInfo->DistSpec.end(); ++it, i++)
	{
		s.Format(cFmt1, i+1);
		m_listDist.InsertItem(i, s);
		m_listDist.SetItemText(i, 1, it->SCName);
		s.Format(cFmt86, it->dist);
		m_listDist.SetItemText(i, 2, s);
	}

	CString s1 = GetPreprocLine(&PrepInfo->Preproc);
	if(s1.IsEmpty())
		s1 = ParcelLoadString(STRID_SPEC_TRANS_CORR_NOPREP);
	s.Format(ParcelLoadString(STRID_SPEC_TRANS_CORR_PREPROCIS), s1);
	GetDlgItem(IDC_STATIC_18)->SetWindowText(s);

	curprep = cGetPreprocAsStr(&PrepInfo->Preproc);
}

void CCreateTransferDlg::CreatePreprocList(VInt* Old)
{
	ItInt it;
	for(it=PreprocAval.begin(); it!=PreprocAval.end(); ++it)
	{
		if(!IsPreprocAddAvailable(Old, (*it)))
			continue;

		VInt NewP;
		copy(Old->begin(), Old->end(), inserter(NewP, NewP.begin()));
		NewP.push_back((*it));

		PreprocList.push_back(NewP);

		if(PreprocMaxLen > int(NewP.size()))
			CreatePreprocList(&NewP);
	}
}

void CCreateTransferDlg::CreatePreprocListAdd(CString str)
{
	bool IsNo = (m_vPrepNo) ? true : false;

	IsAddPreprocValid = true;

	VInt empPrep;
	PreprocListAdd.clear();
	if(IsNo)
	{
		CreateCorrFields();
		PreprocListAdd.push_back(empPrep);
	}

	int i, N = int(_tcslen(str));
	if(N == 0)
	{
		CreateCorrFields();
		return;
	}

	VStr PrepStr;
	CString s = cEmpty;
	for(i=0; i<N; i++)
	{
		if(str[i] == ';' || str[i] == ',')
		{
			PrepStr.push_back(s);
			s = cEmpty;
			continue;
		}
		else
			s += str[i];
	}
	if(!s.IsEmpty())
		PrepStr.push_back(s);
	else
		IsAddPreprocValid = false;

	ItStr it;
	for(it=PrepStr.begin(); it!=PrepStr.end(); ++it)
	{
		CString tmp = (*it);
		tmp.Trim();

		VInt NewP;
		bool err = false, IsNew = false;
		int n = int(_tcslen(tmp));
		for(i=0; i<n; i++)
		{
			int prep = cGetPreprocIntByChar(tmp[i]);
			if(prep == C_PREPROC_0)
			{
				err = true;
				break;
			}

			if(IsPreprocAddAvailable(&NewP, prep))
			{
				NewP.push_back(prep);
				ItInt itf;
				itf = find(PreprocAval.begin(), PreprocAval.end(), prep);
				if(itf == PreprocAval.end() || PreprocMaxLen < int(NewP.size()))
					IsNew = true;
			}
			else
				err = true;
		}

		if(!err)
		{
			if(IsNew)
			{
				ItIntInt it2 = find(PreprocListAdd.begin(), PreprocListAdd.end(), NewP);
				if(it2 == PreprocListAdd.end())
					PreprocListAdd.push_back(NewP);
				else
					IsAddPreprocValid = false;
			}
		}
		else
			IsAddPreprocValid = false;
	}

	CreateCorrFields();
}

CString CCreateTransferDlg::GetPreprocLine(VInt* prepline)
{
	CString res = cEmpty;
	ItInt it;
	int i;
	for(i=0, it=prepline->begin(); it!=prepline->end(); ++it, i++)
		if((*it) != C_PREPROC_0)
			res += GetPreprocCharByMean((*it));

	return res;
}

TCHAR CCreateTransferDlg::GetPreprocCharByMean(int prep)
{
	if(prep < C_PREPROC_0 || prep > C_PREPROC_2)
		return ' ';

	TCHAR res = cPreprocs[prep];

	return res;
}

bool CCreateTransferDlg::IsPreprocAddAvailable(VInt* prepline, int prep)
{
	if(prep <= C_PREPROC_0 || prep > C_PREPROC_2)
		return false;

	int n = int(prepline->size());

	if(prep == C_PREPROC_A && n > 0)
		return false;

	ItInt it;
	for(it=prepline->begin(); it!=prepline->end(); ++it)
	{
		if(prep == (*it) || (prep == C_PREPROC_1 && (*it) == C_PREPROC_2) ||
		   (prep == C_PREPROC_2 && (*it) == C_PREPROC_1))
			return false;
	}
	if(n > 0 && prep == C_PREPROC_D && (*prepline)[n-1] == C_PREPROC_C)
		return false;
	if(n > 0 && prep == C_PREPROC_C && (*prepline)[n-1] == C_PREPROC_M)
		return false;
	if(n > 1 && prep == C_PREPROC_C && (*prepline)[n-1] == C_PREPROC_D && (*prepline)[n-2] == C_PREPROC_M)
		return false;

	return true;
}

bool CCreateTransferDlg::CheckData()
{
	if(!UpdateData())
		return false;

	CString s;

	CTransferData *trans = PrjData->GetTransfer(m_vName);
	if(trans != NULL)
	{
		s.Format(ParcelLoadString(STRID_SPEC_TRANS_SAMP_ERR_EXIST), m_vName);
		ParcelError(s);
		return false;
	}

	if(!m_vNoCorr)
	{
		if(int(SamNames.size()) <= 0)
		{
			s = ParcelLoadString(STRID_SPEC_TRANS_SAMP_ERR_NOSAM);
			ParcelError(s);
			return false;
		}

		if(int(PreprocList.size() + PreprocListAdd.size()) <= 0)
		{
			s = ParcelLoadString(STRID_SPEC_TRANS_SAMP_ERR_NOPREP);
			ParcelError(s);
			return false;
		}
	}

	if(m_vLowLim >= m_vUpperLim)
	{
		s = ParcelLoadString(STRID_SPEC_TRANS_CORR_ERR_RANGE);
		ParcelError(s);
		return false;
	}

	return true;
}

bool CCreateTransferDlg::AcceptData()
{
	if(!CheckData())
		return false;

	CalcInfo.TransData->SetName(m_vName);
	CalcInfo.TransData->SetMasterDevType(MDev->GetType());
	CalcInfo.TransData->SetMasterDevNumber(MDev->GetNumber());

	CalcInfo.bNoCorr = (m_vNoCorr) ? true : false;

	CalcInfo.TransData->SetLowLimSpec(max(PrjData->GetLowLimSpecRange(), MPrj->GetLowLimSpecRange()));
	CalcInfo.TransData->SetUpperLimSpec(min(PrjData->GetUpperLimSpecRange(), MPrj->GetUpperLimSpecRange()));

	CalcInfo.PrjM = MPrj;
	CalcInfo.ModM = MMod;
	CalcInfo.iTransM = 0;

	CalcInfo.MSamNames.clear();
	copy(MSamNames.begin(), MSamNames.end(), inserter(CalcInfo.MSamNames, CalcInfo.MSamNames.begin()));
	CalcInfo.MParNames.clear();
	copy(MParNames.begin(), MParNames.end(), inserter(CalcInfo.MParNames, CalcInfo.MParNames.begin()));

	CalcInfo.SamplesCorr.clear();
	ItModSam it;
	for(it=SamNames.begin(); it!=SamNames.end(); ++it)
	{
		CSampleData* Sam = PrjData->GetTransOwn()->GetSample(it->SampleName);
		ModelSample NewSamCorr(Sam);

		ItIntBool it2;
		for(it2=it->Spectra.begin(); it2!=it->Spectra.end(); ++it2)
			NewSamCorr.Spectra[it2->first] = it2->second;

		CalcInfo.SamplesCorr.push_back(NewSamCorr);
	}

	CalcInfo.TransData->SetLowLimSpec(m_vLowLim);
	CalcInfo.TransData->SetUpperLimSpec(m_vUpperLim);

	CalcInfo.Preprocs.clear();

	int i, N = int(CalcInfo.SamplesCorr.size());
	ItIntInt it3;
	for(it3=PreprocListAdd.begin(); it3!=PreprocListAdd.end(); ++it3)
	{
		TransPrepInfo NewPrep;

		copy((*it3).begin(), (*it3).end(), inserter(NewPrep.Preproc, NewPrep.Preproc.begin()));
		for(i=0; i<N; i++)
		{
			SampleCorrDistance SCDist;
			SCDist.SCName = CalcInfo.SamplesCorr[i].SampleName;
			SCDist.dist = 0.;
			NewPrep.DistSpec.push_back(SCDist);
		}

		CalcInfo.Preprocs.push_back(NewPrep);
	}
	for(it3=PreprocList.begin(); it3!=PreprocList.end(); ++it3)
	{
		TransPrepInfo NewPrep;

		copy((*it3).begin(), (*it3).end(), inserter(NewPrep.Preproc, NewPrep.Preproc.begin()));
		for(i=0; i<N; i++)
		{
			SampleCorrDistance SCDist;
			SCDist.SCName = CalcInfo.SamplesCorr[i].SampleName;
			SCDist.dist = 0.;
			NewPrep.DistSpec.push_back(SCDist);
		}
		CalcInfo.Preprocs.push_back(NewPrep);
	}

	return true;
}

void CCreateTransferDlg::CreateCorrFields()
{
	CalcInfo.Preprocs.clear();
	FillTablePrep(true);
	FillTableDist(-1);

	m_Name.GetWindowText(m_vName);

//	UpdateData(0);

	EnableFields();
}

void CCreateTransferDlg::OnTabSelchange(NMHDR* /*pNMHDR*/, LRESULT* pResult)
{
	m_vTab = m_Tab.GetCurSel();

	ShowFields();

	*pResult = 0;
}

void CCreateTransferDlg::MasterSetSortIcons()
{
	int iCol = -1;
	bool dir = SortDir;

	if(SortName.IsEmpty())
	{
		iCol = 1;
	}
	else if(SortName == cSortTime)
	{
		iCol = m_nTimeCol;
	}
	else
	{
		ItStr itP;
		int i;
		for(i=0, itP=MParNames.begin(); itP!=MParNames.end(); ++itP, i++)
		{
			if(!(*itP).Compare(SortName))
			{
				iCol = m_nParStart + i;
				break;
			}
		}
	}

	m_listMaster.ShowSortIcons(iCol, dir);
}

void CCreateTransferDlg::SlaveSetSortIcons()
{
	int iCol = -1;
	bool dir = SortDir;

	if(SortName.IsEmpty())
		iCol = 1;

	m_listSlave.ShowSortIcons(iCol, dir);
}

void CCreateTransferDlg::PrepSetSortIcons()
{
	int iCol = -1;
	bool dir = true;

	if(CalcInfo.Preprocs.size() > 0)
		iCol = 1 + m_vSort2;

	m_listPrep.ShowSortIcons(iCol, dir);
}

void CCreateTransferDlg::ShowFields(int page/* = -1*/)
{
	if(page >= C_TRANSDLG_PAGE_SMPL && page <= C_TRANSDLG_PAGE_CORR)
		m_vTab = page;

	bool smpl = (m_vTab == C_TRANSDLG_PAGE_SMPL);
	bool corr = (m_vTab == C_TRANSDLG_PAGE_CORR);

	int show_smpl = (smpl) ? SW_SHOW : SW_HIDE;
	int show_corr = (corr) ? SW_SHOW : SW_HIDE;

	GetDlgItem(IDC_INPUT_NAME)->ShowWindow(show_smpl);

//	GetDlgItem(IDC_NO_CONV)->ShowWindow(show_smpl);
	GetDlgItem(IDC_NO_CONV)->ShowWindow(0);

	GetDlgItem(IDC_SELECT_DEVICE)->ShowWindow(show_smpl);
	GetDlgItem(IDC_SELECT_PROJECT)->ShowWindow(show_smpl);
	GetDlgItem(IDC_SELECT_MODEL)->ShowWindow(show_smpl);
	GetDlgItem(IDC_SPEC_TRANS_SAMP_TABLEMASTER)->ShowWindow(show_smpl);
	GetDlgItem(IDC_SPEC_TRANS_SAMP_TABLEWORKER)->ShowWindow(show_smpl);
	GetDlgItem(IDC_SPECTRUM_LIST)->ShowWindow(show_smpl);
	GetDlgItem(IDC_STATIC_1)->ShowWindow(show_smpl);
	GetDlgItem(IDC_STATIC_2)->ShowWindow(show_smpl);
	GetDlgItem(IDC_STATIC_3)->ShowWindow(show_smpl);
	GetDlgItem(IDC_STATIC_4)->ShowWindow(show_smpl);
	GetDlgItem(IDC_STATIC_5)->ShowWindow(show_smpl);
	GetDlgItem(IDC_STATIC_6)->ShowWindow(show_smpl);
	GetDlgItem(IDC_STATIC_8)->ShowWindow(show_smpl);
	GetDlgItem(IDC_SELECT)->ShowWindow(show_smpl);
	GetDlgItem(IDC_SCORES)->ShowWindow(show_smpl);
	GetDlgItem(IDC_REMOVE)->ShowWindow(show_smpl);
	GetDlgItem(IDC_ADD)->ShowWindow(show_smpl);
	GetDlgItem(IDC_EXCLUDE)->ShowWindow(show_smpl);
	GetDlgItem(IDC_PLOT)->ShowWindow(show_smpl);
	GetDlgItem(IDC_PLOT_2)->ShowWindow(show_smpl);

	GetDlgItem(IDC_PREP_A)->ShowWindow(show_corr);
	GetDlgItem(IDC_PREP_B)->ShowWindow(show_corr);
	GetDlgItem(IDC_PREP_M)->ShowWindow(show_corr);
	GetDlgItem(IDC_PREP_D)->ShowWindow(show_corr);
	GetDlgItem(IDC_PREP_C)->ShowWindow(show_corr);
	GetDlgItem(IDC_PREP_N)->ShowWindow(show_corr);
	GetDlgItem(IDC_PREP_1)->ShowWindow(show_corr);
	GetDlgItem(IDC_PREP_2)->ShowWindow(show_corr);
	GetDlgItem(IDC_PREP_WITHOUT)->ShowWindow(show_corr);
	GetDlgItem(IDC_STATIC_9)->ShowWindow(show_corr);
	GetDlgItem(IDC_STATIC_10)->ShowWindow(show_corr);
	GetDlgItem(IDC_STATIC_11)->ShowWindow(show_corr);
	GetDlgItem(IDC_STATIC_12)->ShowWindow(show_corr);
	GetDlgItem(IDC_STATIC_13)->ShowWindow(show_corr);
	GetDlgItem(IDC_STATIC_14)->ShowWindow(show_corr);
	GetDlgItem(IDC_STATIC_15)->ShowWindow(show_corr);
	GetDlgItem(IDC_STATIC_16)->ShowWindow(show_corr);
	GetDlgItem(IDC_STATIC_17)->ShowWindow(show_corr);
	GetDlgItem(IDC_STATIC_18)->ShowWindow(show_corr);
	GetDlgItem(IDC_STATIC_19)->ShowWindow(show_corr);
	GetDlgItem(IDC_CALCULATE)->ShowWindow(show_corr);
	GetDlgItem(IDC_CREATE)->ShowWindow(show_corr);
	GetDlgItem(IDC_CALCULATE)->ShowWindow(show_corr);
	GetDlgItem(IDC_SPEC_TRANS_CORR_TABLEPREP)->ShowWindow(show_corr);
	GetDlgItem(IDC_SPEC_TRANS_CORR_TABLEDIST)->ShowWindow(show_corr);
	GetDlgItem(IDC_SPIN_LETTER)->ShowWindow(show_corr);
	GetDlgItem(IDC_SORT_2)->ShowWindow(show_corr);
	GetDlgItem(IDC_PREP_WITHOUT)->ShowWindow(show_corr);
	GetDlgItem(IDC_INPUT_PREP)->ShowWindow(show_corr);
	GetDlgItem(IDC_NUM_LETTER)->ShowWindow(show_corr);
	GetDlgItem(IDC_LOWLIM_SPEC)->ShowWindow(show_corr);
	GetDlgItem(IDC_UPPERLIM_SPEC)->ShowWindow(show_corr);

	EnableFields();
}

void CCreateTransferDlg::EnableFields()
{
	GetDlgItem(IDC_ADD)->EnableWindow(false);
	GetDlgItem(IDC_SCORES)->EnableWindow(int(MSamNames.size()) > 0);
	GetDlgItem(IDC_SELECT)->EnableWindow(!m_vNoCorr && int(MSamNames.size()) > 0);
	GetDlgItem(IDC_REMOVE)->EnableWindow(!m_vNoCorr && int(SamNames.size()) > 0);
	GetDlgItem(IDC_EXCLUDE)->EnableWindow(!m_vNoCorr && int(SamNames.size()) > 0);
	GetDlgItem(IDC_STATIC_2)->EnableWindow(!m_vNoCorr);
	GetDlgItem(IDC_STATIC_3)->EnableWindow(!m_vNoCorr);

	GetDlgItem(IDC_PLOT_2)->EnableWindow(MPrj != NULL);
	GetDlgItem(IDC_PLOT)->EnableWindow(!m_vNoCorr && int(SamNames.size()) > 0);

	ItIntBool CurSpec;
	ModelSample* CorrSam = NULL;
	int sel = m_listSlave.GetNextItem(-1, LVNI_SELECTED);
	bool bSpec = false;
	if(sel >= 0 && sel < int(SamNames.size()))
	{
		CorrSam = &SamNames[sel];
		int sel1 = m_listSpec.GetNextItem(-1, LVNI_SELECTED);
		if(sel1 >= 0 && sel1 < int(CorrSam->Spectra.size()))
		{
			bSpec = true;
			CurSpec = CorrSam->Spectra.begin();
			for(int i=0; i<sel1; i++)
				++CurSpec;
		}
	}

	CString s = (!bSpec || CurSpec->second) ? ParcelLoadString(STRID_SPEC_TRANS_SAMP_EXCLUDE) : ParcelLoadString(STRID_SPEC_TRANS_SAMP_INCLUDE);
	bool bEn = (!bSpec) ? false : (CurSpec->second) ? true :
		(CorrSam->GetNSpectraUsed() < MPrj->GetSample(CorrSam->SampleName)->GetNSpectraUsed());

	GetDlgItem(IDC_EXCLUDE)->SetWindowText(s);
	GetDlgItem(IDC_EXCLUDE)->EnableWindow(bEn);

	s.Format(ParcelLoadString(STRID_SPEC_TRANS_CORR_COUNT), int(PreprocList.size() + PreprocListAdd.size()));
	GetDlgItem(IDC_STATIC_17)->SetWindowText(s);

	bool ch = (!m_vNoCorr);

	int sel2 = m_listPrep.GetNextItem(-1, LVNI_SELECTED);
	bool calc = (!ch || (sel2 >= 0 && sel2 < int(CalcInfo.Preprocs.size())));

	GetDlgItem(IDC_CALCULATE)->EnableWindow(calc);
	GetDlgItem(IDC_STATIC_2)->EnableWindow(ch);
	GetDlgItem(IDC_STATIC_3)->EnableWindow(ch);
	GetDlgItem(IDC_STATIC_9)->EnableWindow(ch);
	GetDlgItem(IDC_PREP_A)->EnableWindow(ch);
	GetDlgItem(IDC_PREP_B)->EnableWindow(ch);
	GetDlgItem(IDC_PREP_M)->EnableWindow(ch);
	GetDlgItem(IDC_PREP_D)->EnableWindow(ch);
	GetDlgItem(IDC_PREP_C)->EnableWindow(ch);
	GetDlgItem(IDC_PREP_N)->EnableWindow(ch);
	GetDlgItem(IDC_PREP_1)->EnableWindow(ch);
	GetDlgItem(IDC_PREP_2)->EnableWindow(ch);
	GetDlgItem(IDC_PREP_WITHOUT)->EnableWindow(ch);
	GetDlgItem(IDC_CREATE)->EnableWindow(ch);
	GetDlgItem(IDC_SORT_2)->EnableWindow(ch);
	GetDlgItem(IDC_NUM_LETTER)->EnableWindow(ch);
	GetDlgItem(IDC_INPUT_PREP)->EnableWindow(ch);
	GetDlgItem(IDC_STATIC_11)->EnableWindow(ch);
	GetDlgItem(IDC_STATIC_14)->EnableWindow(ch);
	GetDlgItem(IDC_STATIC_15)->EnableWindow(ch);
	GetDlgItem(IDC_STATIC_16)->EnableWindow(ch);
	GetDlgItem(IDC_STATIC_17)->EnableWindow(ch);
	GetDlgItem(IDC_STATIC_19)->EnableWindow(ch);

//	RedrawWindow();
}

void CCreateTransferDlg::OnHelp()
{
	int IdHelp = DLGID_SAMPLE_CREATE_SET;

	pFrame->ShowHelp(IdHelp);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void CCreateTransferDlg::OnCorrect()
{
//	OnCorrectAlone();
//	return;
///////////////////////////
	if(!AcceptData())
		return;

	m_vName.Trim();
	if(m_vName.IsEmpty())
	{
		ParcelError(ParcelLoadString(STRID_SPEC_TRANS_SAMP_ERR_EMPTY));
		m_vTab = C_TRANSDLG_PAGE_SMPL;
		m_Tab.SetCurSel(m_vTab);
		ShowFields();

		GetDlgItem(IDC_INPUT_NAME)->SetFocus();
		HWND hWndLastControl;
		GetDlgItem(IDC_INPUT_NAME, &hWndLastControl);
		::SendMessage(hWndLastControl, EM_SETSEL, 0, -1);
		return;
	}

	if(CalcInfo.Preprocs.size() <= 0)
	{
		ParcelError(ParcelLoadString(STRID_SPEC_TRANS_SAMP_ERR_NOPREP));
		return;
	}

	if(CalcInfo.bNoCorr && !CalcInfo.LoadSpectraData())
		return;

	bool res = CalcInfo.MakeCorrection(curprep);

	CalcInfo.FreeSpectraData();

	if(res)
		OnAccept();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void CCreateTransferDlg::OnCorrectAlone()
{
	if(!AcceptDataAlone()) return;

	if(CalcInfo.bNoCorr && !CalcInfo.LoadSpectraData())	return;

	bool res = CalcInfo.MakeCorrection(cEmpty);

	CalcInfo.FreeSpectraData();

	if(res) CDialog::OnOK();
}

bool CCreateTransferDlg::AcceptDataAlone()
{
	CString strName;
	strName.Format(L"%08d", MDev->GetNumber());

	CTransferData *trans = PrjData->GetTransfer(strName);
	if(trans != NULL) return false; 

	CalcInfo.TransData->SetName(strName);
	CalcInfo.TransData->SetMasterDevType(MDev->GetType());
	CalcInfo.TransData->SetMasterDevNumber(MDev->GetNumber());

	CalcInfo.bNoCorr = true;

	CalcInfo.TransData->SetLowLimSpec(max(PrjData->GetLowLimSpecRange(), MPrj->GetLowLimSpecRange()));
	CalcInfo.TransData->SetUpperLimSpec(min(PrjData->GetUpperLimSpecRange(), MPrj->GetUpperLimSpecRange()));

	CalcInfo.PrjM = MPrj;
	CalcInfo.ModM = NULL;
	CalcInfo.iTransM = NULL;

	CalcInfo.MSamNames.clear();
	copy(MSamNames.begin(), MSamNames.end(), inserter(CalcInfo.MSamNames, CalcInfo.MSamNames.begin()));
	CalcInfo.MParNames.clear();
	copy(MParNames.begin(), MParNames.end(), inserter(CalcInfo.MParNames, CalcInfo.MParNames.begin()));

	CalcInfo.SamplesCorr.clear();

	CalcInfo.Preprocs.clear();
	TransPrepInfo NewPrep;
	VInt empPrep;
	NewPrep.Preproc = empPrep;
	CalcInfo.Preprocs.push_back(NewPrep);

	return true;
}

