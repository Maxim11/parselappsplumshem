#ifndef _TEMPLATE_NAME_DLG_H_
#define _TEMPLATE_NAME_DLG_H_

#pragma once

#include "Resource.h"
#include "OptimTemplate.h"


///////////////////////////////////////////////////////////////////////////////////////////////////
// ������ ����� ����� ���������� ������� �����������

// ��������� ������ ������ �������

const int C_TMPL_MODE_CREATE	= 0;
const int C_TMPL_MODE_EDIT		= 1;

class CTemplateNameDlg : public CDialog
{
	DECLARE_DYNAMIC(CTemplateNameDlg)

public:

	CTemplateNameDlg(Template* data, int mode, CWnd* pParent = NULL);		// �����������
	virtual ~CTemplateNameDlg();											// ����������

	enum { IDD = IDD_TEMPLATE_NAME };	// ������������� �������

	CEdit m_Name;			// ���� ��� ����� "����� �������"
	CEdit m_Note;			// ���� ��� ����� "����������� � �������"

protected:

	int Mode;				// ����� ������ �������

	Template *Tmpl;			// ��������� �� ������������� ������

	CString m_vName;		// ��������� �������� "����� �������"
	CString m_vNote;		// ��������� �������� "����������� � �������"

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();						// ������������� �������

	virtual void OnOK()
	{};
	virtual void OnCancel();		// ��������� ������� "�������� ���� ������ � ����� �� �������"

	afx_msg void OnAccept();		// ��������� ������� "������� ������ � ����� �� �������"
	afx_msg void OnHelp();				// ��������� ������� "������� �������"

	DECLARE_MESSAGE_MAP()
};

#endif
