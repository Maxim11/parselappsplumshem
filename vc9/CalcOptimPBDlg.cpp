#include "stdafx.h"

#include "CalcOptimPBDlg.h"
#include "wm.h"

static void *pCalibQnt = NULL;

IMPLEMENT_DYNAMIC(CCalcOptimPBDlg, CProgressBarDlg)

CCalcOptimPBDlg::CCalcOptimPBDlg(COptimCalculate* optim, bool tmpl, int istep, int nstep, int mode, CWnd* pParent)
	: CProgressBarDlg(pParent)
{
	Mode = mode;
	CalcData = optim;
	byTmpl = tmpl;
	iStep = istep;
	nStep = nstep;

	NAll = 0;
	CurModel = 0;
	ipb = 0;
	Ymin = Ymax = 0;
	brAv = brSp = brSpecL = brSpecR = brPrep = brComp = brHarm = brHiper = brKfnl = 0;
	NPoints = 0;
	Kopt = 0.;
	Iopt = -1;

	prePar.nSpType = 0;
	prePar.strCompName.clear();
	prePar.strPreprocPar.clear();
	prePar.strPreprocParTrans.clear();
	prePar.vFreqExcluded.clear();
}

CCalcOptimPBDlg::~CCalcOptimPBDlg()
{
}

void CCalcOptimPBDlg::DoDataExchange(CDataExchange* pDX)
{
	CProgressBarDlg::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CCalcOptimPBDlg, CProgressBarDlg)

	ON_WM_TIMER()

END_MESSAGE_MAP()


BOOL CCalcOptimPBDlg::OnInitDialog()
{
	CProgressBarDlg::OnInitDialog();

	CString s;

	NAll = CalcData->CalcNumModels();
    switch(Mode)
	{
	case C_OPTIM_CALC_ALL_MODELS_2:
		break;
	case C_OPTIM_TEST_EXC_POINTS_2:
		NPoints = CalcData->GetNUsePoints();
		break;
	case C_OPTIM_TEST_ANALIZE_OUTS_2:
		GetDlgItem(IDC_PAUSE)->EnableWindow(false);
		break;
	}
	SetHeader(ParcelLoadString(STRID_CALC_OPTIM_PB_HDR));
	if(byTmpl)
		s.Format(ParcelLoadString(STRID_CALC_OPTIM_PB_COMMENT_TMPL), iStep, nStep);
	else
		s = ParcelLoadString(STRID_CALC_OPTIM_PB_COMMENT);
	SetComment(s);
	SetRangeStep(0, NAll, 1);
	SetStatus(ParcelLoadString(STRID_CALC_OPTIM_PB_STATUS));

	VInt crits;
	CalcData->GetCrits(crits);
	OptMod.SetCrits(&crits);

	CalcData->GetExcPoints(OptMod.ExcPoints);

	return true;
}

void CCalcOptimPBDlg::OK()
{
	int nCalc = CalcData->GetNCalcModels();
	if(NAll > nCalc && nCalc > 0 && !byTmpl)
	{
		CString s;
		s.Format(ParcelLoadString(STRID_CALC_OPTIM_PB_WARN_NALL), nCalc, NAll);
		ParcelInformation(s);
	}

	CProgressBarDlg::OK();
}

void CCalcOptimPBDlg::Stop()
{
	CalcData->IsBreak = true;

	CString s;
	s.Format(ParcelLoadString(STRID_CALC_OPTIM_PB_ERROR_STOP), CurModel, NAll, iStep);
	if(!ParcelConfirm(s, true))
		CalcData->ClearResults();

	CProgressBarDlg::Stop();
}

void CCalcOptimPBDlg::Work()
{
	ipb = 0;
	passtime = 0;

	switch(Mode)
	{
	case C_OPTIM_CALC_ALL_MODELS_2:
		CurModel = 0;
		brAv = brSp = brSpecL = brSpecR = brPrep = brComp = brHarm = brHiper = brKfnl = 0;
		CalculateAllModels(true);
		return;
	case C_OPTIM_TEST_EXC_POINTS_2:
		CurPoint = -1;
		TestOnExcPoints(true);
		return;
	case C_OPTIM_TEST_ANALIZE_OUTS_2:
		TestOnAnalizeOuts();
		return;
	}
}

void CCalcOptimPBDlg::CalculateAllModels(bool first)
{
	ParcelProcess(true);

	__time64_t ctime, stime;
	_time64(&stime);

	CString s;
	bool bStop = false, bPause = false, bResumed = !first;
	int err;

	bool hqo = (CalcData->GetModelType() == C_MOD_MODEL_HSO);

	if(first)
	{
		CalcData->FillSampleProxyCalib(SamplesC);
		CalcData->FillFreqScale(FreqScale);
		CalcData->FillQntOutput(qntOut);
		CalcData->FillSpecPreproc(prePar, OptMod.ExcPoints);

		CalcData->CalcMinMaxConc(Ymin, Ymax);
	}

	if(pCalibQnt != NULL)
	{
		IRCalibrate_Destroy(pCalibQnt);
		pCalibQnt = NULL;
	}

	pCalibQnt = IRCalibrate_Create(&SamplesC, &FreqScale, &prePar, &qntOut);
	if(pCalibQnt == NULL)
	{
		IRCalibrate_Destroy(pCalibQnt);
		pCalibQnt = NULL;
		ParcelProcess(false);
		Stop();
		return;
	}

	if(qntOut.bSEV)
	{
		CalcData->FillSampleProxyValid(SamplesV);

		err = IRCalibrate_LoadSamples4SEV(pCalibQnt, &SamplesV);
		if(err != 0)
		{
			IRCalibrate_Destroy(pCalibQnt);
			pCalibQnt = NULL;
			ParcelProcess(false);
			Stop();
			return;
		}
	}

	int ModelType = -1, MT = CalcData->GetModelType();
	switch(MT)
	{
	case C_MOD_MODEL_PLS: ModelType = MODEL_PLS;  break;
	case C_MOD_MODEL_PCR: ModelType = MODEL_PCR;  break;
	case C_MOD_MODEL_HSO: ModelType = MODEL_HSO;  break;
	}
	if(ModelType < 0)
	{
		IRCalibrate_Destroy(pCalibQnt);
		pCalibQnt = NULL;
		ParcelProcess(false);
		Stop();
		return;
	}

	err = IRCalibrate_SetModelType(pCalibQnt, ModelType);
	if(err != 0)
	{
		IRCalibrate_Destroy(pCalibQnt);
		pCalibQnt = NULL;
		ParcelProcess(false);
		Stop();
		return;
	}
	OptMod.ModelType = CalcData->GetModelType();

	int indAv, indSp, indSpecL, indSpecR, indPrep, indComp, indHarm, indHiper, indKfnl;

	int iAv;
	bool bAv;
	for(indAv=0, iAv=0; iAv<=1; iAv++, indAv++)
	{
		if(indAv < brAv)
			continue;

		bAv = (iAv == 0);

		err = IRCalibrate_SetAverage(pCalibQnt, bAv);
		if(err != 0)
			continue;

		int SpectrumType = CalcData->GetSpectrumType();

		int iSp;
		int sSp = (SpectrumType == C_MOD_SPECTRUM_TRA || SpectrumType == C_MOD_SPECTRUM_BOTH) ?
			C_MOD_SPECTRUM_TRA : C_MOD_SPECTRUM_ABS;
		int fSp = (SpectrumType == C_MOD_SPECTRUM_ABS || SpectrumType == C_MOD_SPECTRUM_BOTH) ?
			C_MOD_SPECTRUM_ABS : C_MOD_SPECTRUM_TRA;
		if(CalcData->GetModelTransType() == C_TRANS_TYPE_NORMAL)
			sSp = fSp = C_MOD_SPECTRUM_TRA;
		for(indSp=0, iSp=sSp; iSp<=fSp; iSp++, indSp++)
		{
			if(indSp < brSp)
				continue;

			err = IRCalibrate_SetSpType(pCalibQnt, iSp);
			if(err != 0)
				continue;
			OptMod.SpectrumType = (CalcData->GetModelTransType() == C_TRANS_TYPE_NORMAL) ? C_MOD_SPECTRUM_ABS : iSp;

			ItSInt itRL, itRR;
			for(indSpecL=0, itRL=CalcData->LeftSet.begin(); itRL!=CalcData->LeftSet.end(); ++itRL, indSpecL++)
			{
				if(indSpecL < brSpecL)
					continue;

				int RangeLeft = (*itRL);
				for(indSpecR=0, itRR=CalcData->RightSet.begin(); itRR!=CalcData->RightSet.end(); ++itRR, indSpecR++)
				{
					if(indSpecR < brSpecR)
						continue;

					int RangeRight = (*itRR);
					if(RangeLeft >= RangeRight)
						continue;

					err = IRCalibrate_SetFreqRange(pCalibQnt, RangeLeft, RangeRight);
					if(err != 0)
						continue;

					int low, upper;
					CalcData->GetLowUpperPoints(RangeLeft, RangeRight, low, upper);

//					OptMod.SetRange(RangeLeft, RangeRight, low, upper); // ���� ����� ��-�� ����. �����!

					ItSInt itG;
					for(indHarm=0, itG=CalcData->NHarmSet.begin(); itG!=CalcData->NHarmSet.end(); ++itG, indHarm++)
					{
						if(hqo)
						{
							if(indHarm < brHarm)
								continue;

							int NHarm = (*itG);
							if(NHarm == 1 || NHarm == 2)
								continue;

							err = IRCalibrate_SetHarmonics(pCalibQnt, NHarm);
							if(err != 0)
								continue;
							OptMod.NHarm = NHarm;
						}

						ItStr itPrep;
						for(indPrep=0, itPrep=CalcData->AllPreprocs.begin(); itPrep!=CalcData->AllPreprocs.end(); ++itPrep, indPrep++)
						{
							if(indPrep < brPrep)
								continue;

							CString Prep = (*itPrep);
							if(Prep.IsEmpty())
							{
								if(bAv)
									continue;
							}
							else
								if(Prep[0] == cPreprocs[1])
								{
									if(!bAv)
										continue;
									else
										Prep.Delete(0);
								}
								else
									if(bAv)
										continue;

							USES_CONVERSION;

							string s(T2A(Prep));
							err = IRCalibrate_SetPreproc(pCalibQnt, s);
							if(err != 0)
								continue;
							OptMod.Preproc = (*itPrep);

							ItSInt itComp;
							for(indComp=0, itComp=CalcData->NCompSet.begin(); itComp!=CalcData->NCompSet.end(); ++itComp, indComp++)
							{
								int NComp = (*itComp);
								if(!hqo)
								{
									if(indComp < brComp)
										continue;

									err = IRCalibrate_SetFactors(pCalibQnt, NComp);
									if(err != 0)
										continue;
								}
								OptMod.NComp = NComp;

								ItSDbl itK;
								for(indKfnl=0, itK=CalcData->KfnlSet.begin(); itK!=CalcData->KfnlSet.end(); ++itK, indKfnl++)
								{
									if(hqo)
									{
										if(indKfnl < brKfnl)
											continue;

										double Kfnl = (*itK);
										err = IRCalibrate_SetCnl(pCalibQnt, Kfnl);
										if(err != 0)
											continue;
										OptMod.Kfnl = Kfnl;
									}

									ItSDbl itH;
									for(indHiper=0, itH=CalcData->HiperSet.begin(); itH!=CalcData->HiperSet.end(); ++itH, indHiper++)
									{
										if(hqo)
										{
											if(indHiper < brHiper)
												continue;

											double Hiper = (*itH);
											err = IRCalibrate_SetShc(pCalibQnt, Hiper);
											if(err != 0)
												continue;
											OptMod.Hiper = Hiper;
										}

										bStop = CheckStopPressed();
										if(bStop)
											break;

										bool bBreak = false;

										if(!bResumed)
										{
											if((CurModel+1) % 10 == 0)
											{
												IsPause = true;
												SetTimer(PARCEL_WM_PAUSE_TIMER, 5, 0);
												bBreak = true;
											}
										}
										else
											bResumed = false;

										bPause = bBreak || CheckPausePressed();
										if(bPause)
										{
											brAv = indAv;
											brSp = indSp;
											brSpecL = indSpecL;
											brSpecR = indSpecR;
											brPrep = indPrep;
											brComp = indComp;
											brHarm = indHarm;
											brHiper = indHiper;
											brKfnl = indKfnl;
											_time64(&ctime);
											passtime += int(ctime - stime);
											break;
										}
										else
										{
											brAv = brSp = brSpecL = brSpecR = brPrep = 0;
											brComp = brHarm = brHiper = brKfnl = 0;
										}


										err = IRCalibrate_GetResults(pCalibQnt, &qntOut);

										ipb++;
										SetPosition(ipb);

										_time64(&ctime);
										int elapsed = passtime + int(ctime - stime);
										int estimated = (ipb > 0) ? int(elapsed * NAll / ipb) : 0;

										SetElapsed(elapsed);
										SetEstimated(estimated);
										UpdateAllWindows();

										if(err != 0)
											continue;

										OptimModel NewOptMod;
										NewOptMod.Copy(OptMod);
										NewOptMod.SetRange(RangeLeft, RangeRight, low, upper);

										NewOptMod.Num = CurModel;

										ItIntDbl itCr;
										for(itCr=NewOptMod.CritValues.begin(); itCr!=NewOptMod.CritValues.end(); ++itCr)
										{
											switch(itCr->first)
											{
												case C_CRIT_SEC:  itCr->second = qntOut.vSEC[0];  break;
												case C_CRIT_R2SEC:  itCr->second = qntOut.vR2StatSEC[0];  break;
												case C_CRIT_SECV:  itCr->second = qntOut.vSECV[0];  break;
												case C_CRIT_R2SECV:  itCr->second = qntOut.vR2StatSECV[0];  break;
												case C_CRIT_SECSECV:  itCr->second = qntOut.vSECV[0]-qntOut.vSEC[0];  break;
												case C_CRIT_F:  itCr->second = qntOut.vFStatSECV[0];  break;
												case C_CRIT_SEV:  itCr->second = qntOut.vSEV[0];  break;
												case C_CRIT_ERR:  itCr->second = qntOut.vErr[0];  break;
												case C_CRIT_SDV:  itCr->second = qntOut.vSDV[0];  break;
												case C_CRIT_R2SEV:  itCr->second = qntOut.vR2StatSEV[0];  break;
												case C_CRIT_K1:
												case C_CRIT_K2:
												case C_CRIT_K3:
												case C_CRIT_K4:
												case C_CRIT_K5:
												case C_CRIT_K6:
												case C_CRIT_K7:
													itCr->second = cCalcCritK(itCr->first, qntOut.vSEC[0], qntOut.vSECV[0],
														qntOut.vSEV[0], Ymin, Ymax);
													break;
											}
										}

										CalcData->AddModelResult(NewOptMod);
										CurModel++;
/*
										if(CurModel % 10 == 0)
										{
											IsPause = true;
											SetTimer(PARCEL_WM_PAUSE_TIMER, 50, 0);
										}
*/
									}
									if(bStop || bPause)  break;
								}
								if(bStop || bPause)  break;
							}
							if(bStop || bPause)  break;
						}
						if(bStop || bPause)  break;
					}
					if(bStop || bPause)  break;
				}
				if(bStop || bPause)  break;
			}
			if(bStop || bPause)  break;
		}
		if(bStop || bPause)  break;
	}

	if(pCalibQnt != NULL)
	{
		IRCalibrate_Destroy(pCalibQnt);
		pCalibQnt = NULL;
	}

	ParcelProcess(false);

	if(bPause)
		return;
	if(bStop)
		Stop();
	else
		OK();
}

void CCalcOptimPBDlg::TestOnExcPoints(bool first)
{
	ParcelProcess(true);

	__time64_t ctime, stime;
	_time64(&stime);

	bool bStop = false, bPause = false;
	int err;

	bool hqo = (CalcData->GetModelType() == C_MOD_MODEL_HSO);

	int indS = CalcData->GetStartSpecInd();

	if(first)
	{
		CalcData->FillSampleProxyCalib(SamplesC);
		CalcData->FillFreqScale(FreqScale);
		CalcData->FillQntOutput(qntOut);

		CalcData->CalcMinMaxConc(Ymin, Ymax);

		OptMod.ModelType = CalcData->GetModelType();
		OptMod.SpectrumType = CalcData->GetSpectrumType();
		if(OptMod.SpectrumType == C_MOD_SPECTRUM_BOTH)
			OptMod.SpectrumType = C_MOD_SPECTRUM_ABS;

		int low, upper;
		int left = *(CalcData->LeftSet.begin());
		int right = *(CalcData->RightSet.begin());
		CalcData->GetLowUpperPoints(left, right, low, upper);
		OptMod.SetRange(left, right, low, upper);

		OptMod.Preproc = *(CalcData->AllPreprocs.begin());
		OptMod.NComp = *(CalcData->NCompSet.begin());
		if(OptMod.ModelType == C_MOD_MODEL_HSO)
		{
			OptMod.Hiper = *(CalcData->HiperSet.begin());
			OptMod.NHarm = *(CalcData->NHarmSet.begin());
			OptMod.Kfnl = *(CalcData->KfnlSet.begin());
		}
	}

	ItSInt itf;
	int ir = 0, NN = CalcData->GetNAllSpecPoints();
	for(int ii=CurPoint; ii<2*NN; ii++)
	{
		CurPoint = ii;

		if(ii >= NN)
			ir = ii - NN;
		else
			ir = ii;

		if(ii == NN)
		{
			if(Iopt < 0)
				break;
			else
				OptMod.ExcPoints.insert(Iopt + indS);
		}

		if(ir >= 0)
		{
			itf = find(OptMod.ExcPoints.begin(), OptMod.ExcPoints.end(), ir + indS);
			if(itf != OptMod.ExcPoints.end())
				continue;

			OptMod.ExcPoints.insert(ir + indS);
		}

		CalcData->FillSpecPreproc(prePar, OptMod.ExcPoints);

		if(pCalibQnt != NULL)
		{
			IRCalibrate_Destroy(pCalibQnt);
			pCalibQnt = NULL;
		}

		pCalibQnt = IRCalibrate_Create(&SamplesC, &FreqScale, &prePar, &qntOut);
		if(pCalibQnt == NULL)
			continue;

		if(qntOut.bSEV)
		{
			CalcData->FillSampleProxyValid(SamplesV);

			err = IRCalibrate_LoadSamples4SEV(pCalibQnt, &SamplesV);
			if(err != 0)
				continue;
		}

		int ModelType = -1, MT = OptMod.ModelType;
		switch(MT)
		{
		case C_MOD_MODEL_PLS: ModelType = MODEL_PLS;  break;
		case C_MOD_MODEL_PCR: ModelType = MODEL_PCR;  break;
		case C_MOD_MODEL_HSO: ModelType = MODEL_HSO;  break;
		}
		if(ModelType < 0)
			continue;

		err = IRCalibrate_SetModelType(pCalibQnt, ModelType);
		if(err != 0)
			continue;

		bool bAv = (OptMod.Preproc[0] == cPreprocs[1]);
		err = IRCalibrate_SetAverage(pCalibQnt, bAv);
		if(err != 0)
			continue;

		int SpType = (CalcData->GetModelTransType() != C_TRANS_TYPE_NORMAL) ? OptMod.SpectrumType : C_MOD_SPECTRUM_TRA;
		err = IRCalibrate_SetSpType(pCalibQnt, SpType);
		if(err != 0)
			continue;

		err = IRCalibrate_SetFreqRange(pCalibQnt, OptMod.RangeLeft, OptMod.RangeRight);
		if(err != 0)
			continue;

		if(hqo)
		{
			err = IRCalibrate_SetHarmonics(pCalibQnt, OptMod.NHarm);
			if(err != 0)
				continue;
		}

		CString Prep = OptMod.Preproc;
		if(!Prep.IsEmpty() && Prep[0] == cPreprocs[1])
			Prep.Delete(0);

		USES_CONVERSION;

		string s(T2A(Prep));
		err = IRCalibrate_SetPreproc(pCalibQnt, s);
		if(err != 0)
			continue;

		if(!hqo)
		{
			err = IRCalibrate_SetFactors(pCalibQnt, OptMod.NComp);
			if(err != 0)
				continue;
		}

		if(hqo)
		{
			err = IRCalibrate_SetCnl(pCalibQnt, OptMod.Kfnl);
			if(err != 0)
				continue;

			err = IRCalibrate_SetShc(pCalibQnt, OptMod.Hiper);
			if(err != 0)
				continue;
		}

		bStop = CheckStopPressed();
		if(bStop)
			break;
		bPause = CheckPausePressed();
		if(bPause)
		{
			_time64(&ctime);
			passtime += int(ctime - stime);
			break;
		}

		err = IRCalibrate_GetResults(pCalibQnt, &qntOut);

		ipb++;
		SetPosition(ipb);

		_time64(&ctime);
		int elapsed = passtime + int(ctime - stime);
		int estimated = (ipb > 0) ? int(elapsed * NAll / ipb) : 0;

		SetElapsed(elapsed);
		SetEstimated(estimated);
		UpdateAllWindows();

		if(err != 0)
			continue;

		OptimModel NewOptMod;
		NewOptMod.Copy(OptMod);

		NewOptMod.Num = CurModel;

		ItIntDbl itCr;
		for(itCr=NewOptMod.CritValues.begin(); itCr!=NewOptMod.CritValues.end(); ++itCr)
		{
			switch(itCr->first)
			{
				case C_CRIT_SEC:  itCr->second = qntOut.vSEC[0];  break;
				case C_CRIT_R2SEC:  itCr->second = qntOut.vR2StatSEC[0];  break;
				case C_CRIT_SECV:  itCr->second = qntOut.vSECV[0];  break;
				case C_CRIT_R2SECV:  itCr->second = qntOut.vR2StatSECV[0];  break;
				case C_CRIT_SECSECV:  itCr->second = qntOut.vSECV[0]-qntOut.vSEC[0];  break;
				case C_CRIT_F:  itCr->second = qntOut.vFStatSECV[0];  break;
				case C_CRIT_SEV:  itCr->second = qntOut.vSEV[0];  break;
				case C_CRIT_ERR:  itCr->second = qntOut.vErr[0];  break;
				case C_CRIT_SDV:  itCr->second = qntOut.vSDV[0];  break;
				case C_CRIT_R2SEV:  itCr->second = qntOut.vR2StatSEV[0];  break;
				case C_CRIT_K1:
				case C_CRIT_K2:
				case C_CRIT_K3:
				case C_CRIT_K4:
				case C_CRIT_K5:
				case C_CRIT_K6:
				case C_CRIT_K7:
					itCr->second = cCalcCritK(itCr->first, qntOut.vSEC[0], qntOut.vSECV[0],
						qntOut.vSEV[0], Ymin, Ymax);
					break;
			}
		}

		CalcData->AddModelResult(NewOptMod);
		CurModel++;

		double mean = NewOptMod.GetValue(CalcData->GetMainCrit());
		if(ir < 0)
			Kopt = mean;
		else
		{
			bool good = false;
			if(cCompareByCrit(CalcData->GetMainCrit(), NewOptMod.GetValue(CalcData->GetMainCrit()), Kopt))
			{
				good = true;
				Kopt = mean;
				if(ii <= NN)
					Iopt = ir;
			}
			if(ii < NN || !good)
			{
				itf = find(OptMod.ExcPoints.begin(), OptMod.ExcPoints.end(), ir + indS);
				if(itf != OptMod.ExcPoints.end())
					OptMod.ExcPoints.erase(itf);
			}
		}

		if(bStop || bPause)
			break;
	}

	if(pCalibQnt != NULL)
	{
		IRCalibrate_Destroy(pCalibQnt);
		pCalibQnt = NULL;
	}

	ParcelProcess(false);

	if(bPause)
		return;
	if(bStop)
		Stop();
	else
		OK();
}

void CCalcOptimPBDlg::TestOnAnalizeOuts()
{
	ParcelProcess(true);

	bool bStop = false;
	int err;

	bool hqo = (CalcData->GetModelType() == C_MOD_MODEL_HSO);

	CalcData->FillSampleProxyCalib(SamplesC);
	CalcData->FillFreqScale(FreqScale);
	CalcData->FillQntOutput(qntOut);

	CalcData->CalcMinMaxConc(Ymin, Ymax);

	OptMod.ModelType = CalcData->GetModelType();
	OptMod.SpectrumType = CalcData->GetSpectrumType();
	if(OptMod.SpectrumType == C_MOD_SPECTRUM_BOTH)
		OptMod.SpectrumType = C_MOD_SPECTRUM_ABS;

	int low, upper;
	int left = *(CalcData->LeftSet.begin());
	int right = *(CalcData->RightSet.begin());
	CalcData->GetLowUpperPoints(left, right, low, upper);
	OptMod.SetRange(left, right, low, upper);

	OptMod.Preproc = *(CalcData->AllPreprocs.begin());
	OptMod.NComp = *(CalcData->NCompSet.begin());
	if(OptMod.ModelType == C_MOD_MODEL_HSO)
	{
		OptMod.Hiper = *(CalcData->HiperSet.begin());
		OptMod.NHarm = *(CalcData->NHarmSet.begin());
		OptMod.Kfnl = *(CalcData->KfnlSet.begin());
	}

	CalcData->FillSpecPreproc(prePar, OptMod.ExcPoints);

	if(pCalibQnt != NULL)
	{
		IRCalibrate_Destroy(pCalibQnt);
		pCalibQnt = NULL;
	}

	pCalibQnt = IRCalibrate_Create(&SamplesC, &FreqScale, &prePar, &qntOut);
	if(pCalibQnt == NULL)
	{
		ParcelProcess(false);
		Stop();
		return;
	}

	if(qntOut.bSEV)
	{
		CalcData->FillSampleProxyValid(SamplesV);

		err = IRCalibrate_LoadSamples4SEV(pCalibQnt, &SamplesV);
		if(err != 0)
		{
			ParcelProcess(false);
			Stop();
			return;
		}
	}

	int ModelType = -1, MT = OptMod.ModelType;
	switch(MT)
	{
	case C_MOD_MODEL_PLS: ModelType = MODEL_PLS;  break;
	case C_MOD_MODEL_PCR: ModelType = MODEL_PCR;  break;
	case C_MOD_MODEL_HSO: ModelType = MODEL_HSO;  break;
	}
	if(ModelType < 0)
	{
		ParcelProcess(false);
		Stop();
		return;
	}

	err = IRCalibrate_SetModelType(pCalibQnt, ModelType);
	if(err != 0)
	{
		ParcelProcess(false);
		Stop();
		return;
	}

	bool bAv = (OptMod.Preproc[0] == cPreprocs[1]);
	err = IRCalibrate_SetAverage(pCalibQnt, bAv);
	if(err != 0)
	{
		ParcelProcess(false);
		Stop();
		return;
	}


	int SpType = (CalcData->GetModelTransType() != C_TRANS_TYPE_NORMAL) ? OptMod.SpectrumType : C_MOD_SPECTRUM_TRA;
	err = IRCalibrate_SetSpType(pCalibQnt, SpType);
	if(err != 0)
	{
		ParcelProcess(false);
		Stop();
		return;
	}

	err = IRCalibrate_SetFreqRange(pCalibQnt, OptMod.RangeLeft, OptMod.RangeRight);
	if(err != 0)
	{
		ParcelProcess(false);
		Stop();
		return;
	}

	if(hqo)
	{
		err = IRCalibrate_SetHarmonics(pCalibQnt, OptMod.NHarm);
		if(err != 0)
		{
			ParcelProcess(false);
			Stop();
			return;
		}
	}

	CString Prep = OptMod.Preproc;
	if(!Prep.IsEmpty() && Prep[0] == cPreprocs[1])
		Prep.Delete(0);

	USES_CONVERSION;

	string s(T2A(Prep));
	err = IRCalibrate_SetPreproc(pCalibQnt, s);
	if(err != 0)
	{
		ParcelProcess(false);
		Stop();
		return;
	}

	if(!hqo)
	{
		err = IRCalibrate_SetFactors(pCalibQnt, OptMod.NComp);
		if(err != 0)
		{
			ParcelProcess(false);
			Stop();
			return;
		}
	}

	if(hqo)
	{
		err = IRCalibrate_SetCnl(pCalibQnt, OptMod.Kfnl);
		if(err != 0)
		{
			ParcelProcess(false);
			Stop();
			return;
		}

		err = IRCalibrate_SetShc(pCalibQnt, OptMod.Hiper);
		if(err != 0)
		{
			ParcelProcess(false);
			Stop();
			return;
		}
	}

	bStop = CheckStopPressed();
	if(bStop)
	{
		ParcelProcess(false);
		Stop();
		return;
	}

	err = IRCalibrate_GetResults(pCalibQnt, &qntOut);

	if(err != 0)
	{
		ParcelProcess(false);
		Stop();
		return;
	}

	OptimModel NewOptMod;
	NewOptMod.Copy(OptMod);

	NewOptMod.Num = CurModel;

	ItIntDbl itCr;
	for(itCr=NewOptMod.CritValues.begin(); itCr!=NewOptMod.CritValues.end(); ++itCr)
	{
		switch(itCr->first)
		{
			case C_CRIT_SEC:  itCr->second = qntOut.vSEC[0];  break;
			case C_CRIT_R2SEC:  itCr->second = qntOut.vR2StatSEC[0];  break;
			case C_CRIT_SECV:  itCr->second = qntOut.vSECV[0];  break;
			case C_CRIT_R2SECV:  itCr->second = qntOut.vR2StatSECV[0];  break;
			case C_CRIT_SECSECV:  itCr->second = qntOut.vSECV[0]-qntOut.vSEC[0];  break;
			case C_CRIT_F:  itCr->second = qntOut.vFStatSECV[0];  break;
			case C_CRIT_SEV:  itCr->second = qntOut.vSEV[0];  break;
			case C_CRIT_ERR:  itCr->second = qntOut.vErr[0];  break;
			case C_CRIT_SDV:  itCr->second = qntOut.vSDV[0];  break;
			case C_CRIT_R2SEV:  itCr->second = qntOut.vR2StatSEV[0];  break;
			case C_CRIT_K1:
			case C_CRIT_K2:
			case C_CRIT_K3:
			case C_CRIT_K4:
			case C_CRIT_K5:
			case C_CRIT_K6:
			case C_CRIT_K7:
				itCr->second = cCalcCritK(itCr->first, qntOut.vSEC[0], qntOut.vSECV[0],
					qntOut.vSEV[0], Ymin, Ymax);
				break;
		}
	}

	CalcData->AddModelResult(NewOptMod);
	CurModel++;

	if(bStop)
	{
		ParcelProcess(false);
		Stop();
		return;
	}

	if(pCalibQnt != NULL)
	{
		IRCalibrate_Destroy(pCalibQnt);
		pCalibQnt = NULL;
	}

	ParcelProcess(false);

	if(bStop)
		Stop();
	else
		OK();
}

void CCalcOptimPBDlg::ResumeWork()
{
	switch(Mode)
	{
	case C_OPTIM_CALC_ALL_MODELS_2:
		CalculateAllModels(false);
		return;
	case C_OPTIM_TEST_EXC_POINTS_2:
		TestOnExcPoints(false);
		return;
	case C_OPTIM_TEST_ANALIZE_OUTS_2:
		TestOnAnalizeOuts();
		return;
	}
}
