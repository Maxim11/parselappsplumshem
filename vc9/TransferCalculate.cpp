#include "stdafx.h"

#include "TransferCalculate.h"
#include "TransferWaitDlg.h"
#include "TransferPBDlg.h"
#include "LoadSpectraPBDlg.h"
#include "BranchInfo.h"


CTransferCalculate::CTransferCalculate()
{
	pSpCorr = NULL;
	pPPD = NULL;
	bNoCorr = false;
	IsBreak = false;
	iTransM = 0;
}

CTransferCalculate::~CTransferCalculate()
{
	DeleteCorrection();
}

bool CTransferCalculate::CalculateCorrection()
{
	IsBreak = false;

	CTransferPBDlg dlg(this);
	dlg.DoModal();

	int i, N = int(Preprocs.size());
	for(i=N-1; i>=0; i--)
		if(!Preprocs[i].IsCalc)
		{
			ItTransPrepInfo it = Preprocs.begin() + i;
			Preprocs.erase(it);
		}

	if(!IsBreak && int(Preprocs.size()) <= 0)
		ParcelError(ParcelLoadString(STRID_ERROR_TRANS_CALC_CORR));

	return true;
}

bool CTransferCalculate::MakeCorrection(CString prepstr)
{
	if(bNoCorr)
	{
		TransData->SetType(C_TRANS_TYPE_NOTCORR);
		TransData->SetAllPreproc(cEmpty);

		CTransferWaitDlg dlg2(this, C_MODE_TRANSFER_WITHOUT);
		if(dlg2.DoModal() == IDOK)
			return true;

		return false;
	}

	TransData->SetType(C_TRANS_TYPE_NORMAL);
	TransData->SetAllPreproc(prepstr);

	if(!pSpCorr)
	{
		ParcelError(ParcelLoadString(STRID_ERROR_TRANS_CALC_NOKOEF));
		return false;
	}

	ParcelWait(true);

	SamplesSA.clear();
	copy(SamplesMA.begin(), SamplesMA.end(), inserter(SamplesSA, SamplesSA.begin()));

	USES_CONVERSION;
	string s(T2A(prepstr));

	int res = SpectraCorrection_Correct(pSpCorr, s);
	if(res)
	{
		CString errstr = GetErrorStr(res);
		ParcelError(errstr);
		ParcelWait(false);
		return false;
	}

	SpectraCorrection_GetCorrectedSpectra(pSpCorr, &SamplesSA);

	CSpecPreprocData_Parcel PPD;

	PPD.meanSpectrum.resize(FreqScale.nNumberOfFreq);
	PPD.meanSpectrumMSC.resize(FreqScale.nNumberOfFreq);
	PPD.meanStdSpec.resize(FreqScale.nNumberOfFreq);

	SpectraCorrection_GetPreprocData(pSpCorr, &PPD);

	pPPD = &PPD;

	CTransferWaitDlg dlg2(this, C_MODE_TRANSFER_NORMAL);
	if(dlg2.DoModal() == IDOK)
	{
		ParcelWait(false);
		return true;
	}

	ParcelWait(false);

	return false;
}

bool CTransferCalculate::AcceptTransfer()
{
	TransData->GetSpecPoints();

	if(!pPPD || SamplesSA.size() == 0)
		return false;

	CProjectData* Prj = GetProjectByHItem(TransData->GetParentHItem());

	if(!Prj->AddTransfer(TransData))
		return false;

	CTransferData* pTransData = Prj->GetTransfer(TransData->GetName());

	ItSamProxy it;
	ItStr it3;
	for(it=SamplesSA.begin(), it3=MSamNames.begin(); it!=SamplesSA.end(); ++it, ++it3)
	{
		CString SamName = (*it3);
		CSampleData* MSam = iTransM ? PrjM->GetTransfer(iTransM)->GetSample(SamName) : PrjM->GetSample(SamName);
//		ModelSample* MSamClb = ModM ? ModM->GetSampleForCalib(SamName) : NULL;

		CSampleData NewSample(pTransData->GetParentHItem(), pTransData->GetHItem());

		NewSample.SetName(SamName);
		NewSample.SetIden2(MSam->GetIden2());
		NewSample.SetIden3(MSam->GetIden3());

		ItStr it1;
		for(it1=MParNames.begin(); it1!=MParNames.end(); ++it1)
		{
			CString ParName = (*it1);
			if(Prj->GetParam(ParName) == NULL)
			{
				CParamData NewParam(Prj->GetHItem());
				NewParam.SetName(ParName);
				Prj->AddParam(&NewParam);
			}

			NewSample.AddReferenceData(ParName);
			ReferenceData* MRef = MSam->GetReferenceData(ParName);

			if(MRef == NULL || MRef->bNoValue)
				NewSample.SetReferenceAsNoValue(ParName);
			else
				NewSample.SetReferenceValue(ParName, MRef->Value);
		}

		ItDblDbl it2;
		int num;
		for(it2=it->mSpectra.begin(), num=1; it2!=it->mSpectra.end(); ++it2, num++)
		{
			Spectrum NewSpec;
			NewSpec.bUse = true;
//			NewSpec.DateTime = CTime::GetCurrentTime();
			NewSpec.DateTime = MSam->GetSpectrumByNum(num)->DateTime;
			NewSpec.Num = num;

			copy((*it2).begin(), (*it2).end(), inserter(NewSpec.Data, NewSpec.Data.begin()));

			NewSample.SpecData.push_back(NewSpec);
		}

		if(!pTransData->AddSample(&NewSample))
			return false;

		pTransData->GetSample(SamName)->FreeSpectraData();
	}

	CSampleData NewArtSample(pTransData->GetParentHItem(), pTransData->GetHItem());

	NewArtSample.SetName(cArtSam);
	NewArtSample.SetIden2(cEmpty);
	NewArtSample.SetIden3(cEmpty);

	ItStr it1;
	for(it1=MParNames.begin(); it1!=MParNames.end(); ++it1)
	{
		CString ParName = (*it1);
		if(Prj->GetParam(ParName) == NULL)
		{
			CParamData NewParam(Prj->GetHItem());
			NewParam.SetName(ParName);
			Prj->AddParam(&NewParam);
		}

		NewArtSample.AddReferenceData(ParName);
		NewArtSample.SetReferenceAsNoValue(ParName);
	}

	if(cIsPrepInStr(TransData->GetAllPreprocAsStr(), C_PREPROC_M))
	{
		Spectrum NewSpecMean;
		NewSpecMean.bUse = true;
		NewSpecMean.DateTime = CTime::GetCurrentTime();
		NewSpecMean.Num = C_TRANS_SPECNUM_MEAN;
		copy(pPPD->meanSpectrum.begin(), pPPD->meanSpectrum.end(), inserter(NewSpecMean.Data, NewSpecMean.Data.begin()));
		NewArtSample.SpecData.push_back(NewSpecMean);
	}

	if(cIsPrepInStr(TransData->GetAllPreprocAsStr(), C_PREPROC_C))
	{
		Spectrum NewSpecMSC;
		NewSpecMSC.bUse = true;
		NewSpecMSC.DateTime = CTime::GetCurrentTime();
		NewSpecMSC.Num = C_TRANS_SPECNUM_MEANMSC;
		copy(pPPD->meanSpectrumMSC.begin(), pPPD->meanSpectrumMSC.end(), inserter(NewSpecMSC.Data, NewSpecMSC.Data.begin()));
		NewArtSample.SpecData.push_back(NewSpecMSC);
	}

	if(cIsPrepInStr(TransData->GetAllPreprocAsStr(), C_PREPROC_D))
	{
		Spectrum NewSpecStd;
		NewSpecStd.bUse = true;
		NewSpecStd.DateTime = CTime::GetCurrentTime();
		NewSpecStd.Num = C_TRANS_SPECNUM_MEANSTD;
		copy(pPPD->meanStdSpec.begin(), pPPD->meanStdSpec.end(), inserter(NewSpecStd.Data, NewSpecStd.Data.begin()));
		NewArtSample.SpecData.push_back(NewSpecStd);
	}

	if(!pTransData->AddSample(&NewArtSample))
		return false;

	pTransData->GetArtSample()->FreeSpectraData();

	DeleteCorrection();

	return true;
}

int CastParam(CParamData* pParamM, CParamData* pParamS)
{
	if(pParamM->GetName() != pParamS->GetName()) return -1;

	int nRet = 0;
	// use std moisture
	if(pParamM->IsUseStdMoisture() != pParamS->IsUseStdMoisture())
	{
		CString str;
		str.Format(GETSTRUI(1167), pParamM->GetName());
		if(IDYES == AfxMessageBox(str, MB_YESNO|MB_ICONEXCLAMATION))
		{
			pParamS->SetUseStdMoisture(pParamM->IsUseStdMoisture());
			nRet = 1;
		}
	}

	// moisture
	if(pParamS->IsUseStdMoisture() && (pParamM->GetStdMoisture() != pParamS->GetStdMoisture()))
	{
		CString str;
		str.Format(GETSTRUI(1168), pParamM->GetName());
		if(IDYES == AfxMessageBox(str, MB_YESNO|MB_ICONEXCLAMATION))
		{
			pParamS->SetStdMoisture(pParamM->GetStdMoisture());
			nRet = 1;
		}
	}

	// units
	if(pParamM->GetUnit() != pParamS->GetUnit())
	{
		CString str;
		str.Format(GETSTRUI(1169), pParamM->GetName());
		if(IDYES == AfxMessageBox(str, MB_YESNO|MB_ICONEXCLAMATION))
		{
			pParamS->SetUnit(pParamM->GetUnit());
			nRet = 1;
		}
	}

	// units
	if(pParamM->GetIFormat() > pParamS->GetIFormat())
	{
		pParamS->SetFormat(pParamM->GetIFormat());
		nRet = 1;
	}
	return nRet;
}


bool CTransferCalculate::AcceptWithout()
{
	TransData->GetSpecPoints();
	GetSpecPoints();

	CProjectData* Prj = GetProjectByHItem(TransData->GetParentHItem());
	int i, Nsp = int(SpecPoints.size());
	int minfreqM = PrjM->GetSpecPointInd(SpecPoints[0]);	
	int maxfreqM = PrjM->GetSpecPointInd(SpecPoints[Nsp - 1]);

	CTransferData* pTransData = Prj->GetTransfer(TransData->GetName());
	if(pTransData == NULL)
	{
		if(!Prj->AddTransfer(TransData))
		{
			return false;
		}
		else
		{
			pTransData = Prj->GetTransfer(TransData->GetName());
		}
	}

	ItStr it1;
	for(it1=MParNames.begin(); it1!=MParNames.end(); ++it1)
	{
		CString ParName = (*it1);
		if(Prj->GetParam(ParName) == NULL)
		{
			CParamData NewParam(Prj->GetHItem());
			NewParam.Copy(*(PrjM->GetParam(ParName)));
			NewParam.hParentItem = Prj->GetHItem();
			Prj->AddParam(&NewParam);
		}
		else
		{
			CParamData NewParam(Prj->GetHItem());
			NewParam.Copy(*(Prj->GetParam(ParName)));
			if(1 == CastParam(PrjM->GetParam(ParName), &NewParam))
			{
				Prj->ChangeParam(Prj->GetParam(ParName), &NewParam);
			}
		}
	}

	int j;
	ItStr it2;
	for(j=0, it2=MSamNames.begin(); it2!=MSamNames.end(); ++it2)
	{
		CString SamName = (*it2);
		if(pTransData->GetSample(SamName) != NULL) continue;

		CSampleData* MSam = iTransM ? PrjM->GetTransfer(iTransM)->GetSample(SamName) : PrjM->GetSample(SamName);
		ModelSample* MSamClb = ModM ? ModM->GetSampleForCalib(SamName, cOwn) : NULL;

		CSampleData NewSample(pTransData->GetParentHItem(), pTransData->GetHItem());

		NewSample.SetName(SamName);
		NewSample.SetIden2(MSam->GetIden2());
		NewSample.SetIden3(MSam->GetIden3());

		ItStr it1;
		for(it1=MParNames.begin(); it1!=MParNames.end(); ++it1)
		{
			CString ParName = (*it1);

			NewSample.AddReferenceData(ParName);
			ReferenceData* MRef = MSam->GetReferenceData(ParName);
			if(MRef == NULL || MRef->bNoValue)
				NewSample.SetReferenceAsNoValue(ParName);
			else
				NewSample.SetReferenceValue(ParName, MRef->Value);
		}

		ItSpc it;
		for(it=MSam->SpecData.begin(); it!=MSam->SpecData.end(); ++it)
		{
			if(!it->bUse)
				continue;
			if(MSamClb && !MSamClb->IsSpectrumUsed(it->Num)) continue;

			Spectrum NewSpec;
			NewSpec.bUse = true;
//			NewSpec.DateTime = CTime::GetCurrentTime();
			NewSpec.DateTime = it->DateTime;
			NewSpec.Num = it->Num;

			for(i=minfreqM; i<=maxfreqM; i++)
				NewSpec.Data.push_back(it->Data[i]);

			NewSample.SpecData.push_back(NewSpec);
		}

		if(!pTransData->AddSample(&NewSample))
			return false;

		pTransData->GetSample(SamName)->FreeSpectraData();
	}


	//	art sample
	if(pTransData->GetArtSample())
	{
		pTransData->DeleteArtSample();	
	}

	CSampleData NewArtSample(pTransData->GetParentHItem(), pTransData->GetHItem());

	NewArtSample.SetName(cArtSam);
	NewArtSample.SetIden2(cEmpty);
	NewArtSample.SetIden3(cEmpty);

	for(it1=MParNames.begin(); it1!=MParNames.end(); ++it1)
	{
		CString ParName = (*it1);
		NewArtSample.AddReferenceData(ParName);
		NewArtSample.SetReferenceAsNoValue(ParName);
	}

	if(!pTransData->AddSample(&NewArtSample))
		return false;

	pTransData->GetArtSample()->FreeSpectraData();

	DeleteCorrection();

	return true;
}

void CTransferCalculate::DeleteCorrection()
{
	if(pSpCorr)
	{
		SpectraCorrection_Destroy(pSpCorr);
		pSpCorr = NULL;
		pPPD = NULL;
	}
}

void CTransferCalculate::FillSampleProxy(VSamProxy& SamplesMA, VSamProxy& SamplesMC, VSamProxy& SamplesSC)
{
	int l, Nsp = int(SpecPoints.size());
	CProjectData* Prj = GetProjectByHItem(TransData->GetParentHItem());

	int minfreqM = PrjM->GetSpecPointInd(SpecPoints[0]);	
	int maxfreqM = PrjM->GetSpecPointInd(SpecPoints[Nsp - 1]);
	int minfreqS = Prj->GetSpecPointInd(SpecPoints[0]);	
	int maxfreqS = Prj->GetSpecPointInd(SpecPoints[Nsp - 1]);

	SamplesMA.clear();
	SamplesMC.clear();
	SamplesSC.clear();

	ItStr it1;
	for(it1=MSamNames.begin(); it1!=MSamNames.end(); ++it1)
	{
		CSampleProxy OneSample;

		CSampleData* Sam = iTransM ? PrjM->GetTransfer(iTransM)->GetSample(*it1) : PrjM->GetSample(*it1);
		ModelSample* MSamClb = ModM ? ModM->GetSampleForCalib(*it1, cOwn) : NULL;

		ItSpc it;
		for(it=Sam->SpecData.begin(); it!=Sam->SpecData.end(); ++it)
		{
			if(!it->bUse)
				continue;
			if(MSamClb && !MSamClb->IsSpectrumUsed(it->Num)) continue;

			VDbl SpD;
			for(l=minfreqM; l<=maxfreqM; l++)
				SpD.push_back(it->Data[l]);

			OneSample.mSpectra.push_back(SpD);
		}

		SamplesMA.push_back(OneSample);

		ItModSam it2;
		for(it2=SamplesCorr.begin(); it2!=SamplesCorr.end(); ++it2)
		{
			CString SCorrName = it2->SampleName;
			if(SCorrName.Compare(Sam->GetName()))
				continue;

			int k;
			CSampleProxy OneSampleM, OneSampleS;
			CSampleData* SamS = Prj->GetSample(SCorrName);
			ItIntBool it3;
			for(k=0, it3=it2->Spectra.begin(); it3!=it2->Spectra.end(); ++it3)
			{
				if(!it3->second)
					continue;

				Spectrum* Spec = SamS->GetSpectrumByNum(it3->first);
				if(Spec == NULL)
					continue;

				VDbl SpD;
				for(l=minfreqS; l<=maxfreqS; l++)
					SpD.push_back(Spec->Data[l]);

				OneSampleS.mSpectra.push_back(SpD);
				OneSampleM.mSpectra.push_back(OneSample.mSpectra[k]);

				k++;
			}

			SamplesSC.push_back(OneSampleS);
			SamplesMC.push_back(OneSampleM);

			break;
		}
	}
}

void CTransferCalculate::FillFreqScale(CFreqScale& FreqScale)
{
	FreqScale.nNumberOfFreq = int(SpecPoints.size());
	if(FreqScale.nNumberOfFreq > 0)
	{
		FreqScale.fStartFreq = SpecPoints[0];
		if(FreqScale.nNumberOfFreq > 1)
			FreqScale.fStepFreq = SpecPoints[1] - SpecPoints[0];
	}
}

void CTransferCalculate::FillSpecPreproc(vector<string>& prePar)
{
	USES_CONVERSION;

	prePar.clear();

	ItTransPrepInfo it;
	for(it=Preprocs.begin(); it!=Preprocs.end(); ++it)
	{
        string s(T2A(cGetPreprocAsStr(&(it->Preproc))));
		prePar.push_back(s);
	}
}

bool CTransferCalculate::LoadSpectraData()
{
	VLSI PSam;

	ItStr it1;
	for(it1=MSamNames.begin(); it1!=MSamNames.end(); ++it1)
	{
		CSampleData* Sam = iTransM ? PrjM->GetTransfer(iTransM)->GetSample(*it1) : PrjM->GetSample(*it1);
		if(Sam == NULL)
			return false;

		ModelSample* MSamClb = ModM ? ModM->GetSampleForCalib(*it1, cOwn) : NULL;

		LoadSampleInfo NewLSI;
		NewLSI.pSam = Sam;

		ItSpc it4;
		for(it4=Sam->SpecData.begin(); it4!=Sam->SpecData.end(); ++it4)
		{
			if(MSamClb && !MSamClb->Spectra[it4->Num]) continue;
			NewLSI.Nums.push_back(it4->Num);
		}

		PSam.push_back(NewLSI);
	}

	CProjectData* Prj = GetProjectByHItem(TransData->GetParentHItem());

	ItModSam it;
	for(it=SamplesCorr.begin(); it!=SamplesCorr.end(); ++it)
	{
		CSampleData* Sam = Prj->GetSample(it->SampleName);
		if(Sam == NULL)  return false;

		LoadSampleInfo NewLSI;
		NewLSI.pSam = Sam;

		ItIntBool it2;
		for(it2=it->Spectra.begin(); it2!=it->Spectra.end(); ++it2)
			if(it2->second)
				NewLSI.Nums.push_back(it2->first);

		PSam.push_back(NewLSI);
	}

	CLoadSpectraPBDlg dlg(&PSam, NULL);
	if(dlg.DoModal() != IDOK)
	{
		FreeSpectraData();
		return false;
	}

	return true;
}

void CTransferCalculate::FreeSpectraData()
{
	ItStr it;
	for(it=MSamNames.begin(); it!=MSamNames.end(); ++it)
	{
		if(iTransM)
		{
			PrjM->GetTransfer(iTransM)->GetSample(*it)->FreeSpectraData();
		}
		else
		{
			PrjM->GetSample(*it)->FreeSpectraData();
		}
	}

	CProjectData* Prj = GetProjectByHItem(TransData->GetParentHItem());

	ItModSam it2;
	for(it2=SamplesCorr.begin(); it2!=SamplesCorr.end(); ++it2)
		Prj->GetSample(it2->SampleName)->FreeSpectraData();
}

TransPrepInfo* CTransferCalculate::GetPrepInfo(int ind)
{
	if(ind < 0 || ind >= int(Preprocs.size()))
		return NULL;

	return &Preprocs[ind];
}

void CTransferCalculate::GetSpecPoints()
{
	PrjM->ExtractSpecPoints(TransData->GetLowLimSpec(), TransData->GetUpperLimSpec(), SpecPoints);
}

CString CTransferCalculate::GetErrorStr(int ecode)
{
	CString err;

	err.Format(ParcelLoadString(STRID_ERROR_TRANS_CALC), ecode); // ��������!

	return err;
}
