#include "stdafx.h"

#include "ChooseGKDlg.h"
#include "MainFrm.h"

IMPLEMENT_DYNAMIC(CChooseGKDlg, CDialog)

CChooseGKDlg::CChooseGKDlg(CModelResult* data, CWnd* pParent)
	: CDialog(CChooseGKDlg::IDD, pParent)
{
	Data = data;

	IsBtnClk = false;
}

CChooseGKDlg::~CChooseGKDlg()
{
}

void CChooseGKDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_LIST_ALL, m_listAll);
	DDX_Control(pDX, IDC_LIST_SEL, m_listSel);
}

BEGIN_MESSAGE_MAP(CChooseGKDlg, CDialog)

	ON_BN_CLICKED(IDC_ACCEPT, OnAccept)
	ON_BN_CLICKED(IDHELP, OnHelp)
	ON_BN_CLICKED(IDC_ADD, OnAdd)
	ON_BN_CLICKED(IDC_ADD_ALL, OnAddAll)
	ON_BN_CLICKED(IDC_REMOVE, OnRemove)
	ON_BN_CLICKED(IDC_REMOVE_ALL, OnRemoveAll)

END_MESSAGE_MAP()

BOOL CChooseGKDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	CString s;

	Sel.clear();
	for(int i=0; i<Data->GetNGK(); i++)
	{
		GraphInfo* pGI = Data->GetGraphSpecLoad(i);
		if(pGI == NULL)
			continue;

		if(pGI->sel)
			Sel.insert(i);
	}

	FillListAll();

	CString Hdr = ParcelLoadString(STRID_CHOOSE_GK_HDR);
	SetWindowText(Hdr);

	GetDlgItem(IDC_ACCEPT)->SetWindowText(ParcelLoadString(STRID_CHOOSE_GK_ACCEPT));
	GetDlgItem(IDCANCEL)->SetWindowText(ParcelLoadString(STRID_CHOOSE_GK_DECLINE));
	GetDlgItem(IDHELP)->SetWindowText(ParcelLoadString(STRID_CHOOSE_GK_HELP));
	GetDlgItem(IDC_ADD)->SetWindowText(ParcelLoadString(STRID_CHOOSE_GK_ADD));
	GetDlgItem(IDC_ADD_ALL)->SetWindowText(ParcelLoadString(STRID_CHOOSE_GK_ADDALL));
	GetDlgItem(IDC_REMOVE)->SetWindowText(ParcelLoadString(STRID_CHOOSE_GK_REMOVE));
	GetDlgItem(IDC_REMOVE_ALL)->SetWindowText(ParcelLoadString(STRID_CHOOSE_GK_REMOVEALL));
	GetDlgItem(IDC_STATIC_1)->SetWindowText(ParcelLoadString(STRID_CHOOSE_GK_CHOOSE));

	GetDlgItem(IDHELP)->EnableWindow(pFrame->IsExistHelp());

	return true;
}

void CChooseGKDlg::OnAccept()
{
	if(!UpdateData())
		return;

	for(int i=0; i<Data->GetNGK(); i++)
	{
		GraphInfo* pGI = Data->GetGraphSpecLoad(i);
		if(pGI == NULL)
			continue;

		ItSInt it = find(Sel.begin(), Sel.end(), i);
		if(it != Sel.end())
			pGI->sel = true;
		else
			pGI->sel = false;
	}

	CDialog::OnOK();
}

void CChooseGKDlg::OnCancel()
{
	if(IsChanged())
		if(!ParcelConfirm(ParcelLoadString(STRID_CHOOSE_GK_ASK_DOCANCEL), false))
			return;

	CDialog::OnCancel();
}

void CChooseGKDlg::OnAdd()
{
	int nSel = m_listAll.GetSelCount();
	CArray<int, int> Sels;
	Sels.SetSize(nSel);
	m_listAll.GetSelItems(nSel, Sels.GetData());

	for(int i=0; i<nSel; i++)
	{
		int ind = Sels[i];
		if(ind < 0 || ind >= Data->GetNGK())
			continue;

		Sel.insert(ind);
	}

	FillListSel(int(Sel.size() - 1));

	UpdateData(0);

	IsBtnClk = true;
}

void CChooseGKDlg::OnAddAll()
{
	Sel.clear();
	for(int i=0; i<Data->GetNGK(); i++)
		Sel.insert(i);

	int ind = m_listAll.GetCurSel();
	if(ind < 0 || ind >= Data->GetNGK())
		ind = int(Sel.size() - 1);

	FillListSel(ind);

	IsBtnClk = true;
}

void CChooseGKDlg::OnRemove()
{

	int nSel = m_listSel.GetSelCount();
	CArray<int, int> Sels;
	Sels.SetSize(nSel);
	m_listSel.GetSelItems(nSel, Sels.GetData());

	SInt NewSel;
	ItSInt it;
	int i, j;
	for(j=0, i=0, it=Sel.begin(); it!=Sel.end(); ++it, j++)
	{
		if(i >= nSel || Sels[i] != j)
			NewSel.insert(*it);
		else
			i++;
	}

	Sel.clear();
	copy(NewSel.begin(), NewSel.end(), inserter(Sel, Sel.begin()));

	FillListSel(0);

	UpdateData(0);

	IsBtnClk = true;
}

void CChooseGKDlg::OnRemoveAll()
{
	Sel.clear();

	FillListSel();

	IsBtnClk = true;
}

void CChooseGKDlg::FillListAll(int newsel)
{
	m_listAll.ResetContent();

	for(int i=0; i<Data->GetNGK(); i++)
	{
		CString s;
		s.Format(ParcelLoadString(STRID_CHOOSE_GK_NAMEGK), i + 1);
		m_listAll.AddString(s);
	}
	int sel = (newsel >= 0 && newsel < Data->GetNGK()) ? newsel : 0;
	m_listAll.SetCurSel(sel);

	CString s;
	s.Format(ParcelLoadString(STRID_CHOOSE_GK_NUMGKALL), Data->GetNGK());
	GetDlgItem(IDC_STATIC_2)->SetWindowText(s);

	FillListSel();
}

void CChooseGKDlg::FillListSel(int newsel)
{
	m_listSel.ResetContent();

	ItSInt it;
	for(it=Sel.begin(); it!=Sel.end(); ++it)
	{
		CString s;
		s.Format(ParcelLoadString(STRID_CHOOSE_GK_NAMEGK), (*it) + 1);
		m_listSel.AddString(s);
	}
	int sel = (newsel >= 0 && newsel < int(Sel.size())) ? newsel : 0;
	m_listSel.SetCurSel(sel);

	CString s;
	s.Format(ParcelLoadString(STRID_CHOOSE_GK_NUMGKSEL), int(Sel.size()));
	GetDlgItem(IDC_STATIC_3)->SetWindowText(s);

	UpdateData(0);
}

bool CChooseGKDlg::IsChanged()
{
	return IsBtnClk;
}

void CChooseGKDlg::OnHelp()
{
	int IdHelp = DLGID_MODEL_RESULT_CHOOSE_GK;

	pFrame->ShowHelp(IdHelp);
}
