#ifndef _CALC_OPTIM_PB_DLG_H_
#define _CALC_OPTIM_PB_DLG_H_

#pragma once

#include "ProgressBarDlg.h"
#include "OptimCalculate.h"


///////////////////////////////////////////////////////////////////////////////////////////////////
// ������ ��������� �������� ������� ������� ��� �����������

class CCalcOptimPBDlg : public CProgressBarDlg
{
	DECLARE_DYNAMIC(CCalcOptimPBDlg)

public:

	CCalcOptimPBDlg(COptimCalculate* optim, bool tmpl, int istep, int nstep,
		int mode = C_OPTIM_CALC_ALL_MODELS_2, CWnd* pParent = NULL); // �����������
	virtual ~CCalcOptimPBDlg();																				// ����������

	enum { IDD = CProgressBarDlg::IDD };	// ������������� �������

protected:

	int Mode;									// ����� ������
	bool byTmpl;
	int iStep;
	int nStep;

	COptimCalculate* CalcData;					// ��������� �� ���� ����������� ������

	int NAll;									// ����� ����� �������������� �������
	int CurModel;								// ����� ��������� ������������ ������
	int ipb;									// ���������� ����� ������� ������
	int passtime;								// �����, ��������� � ������ ������

	double Ymin, Ymax;							// ����������� � ������������ �������� ���. ������
	vector<CSampleProxy> SamplesC, SamplesV;	// ������� ������ �� �������� ��� ����������� � ���������
	CFreqScale FreqScale;						// ��������� ����� ��������
	CSpecPreproc prePar;						// �������������
	CQntOutput qntOut;							// ��������� � ���������� ������� ������
	OptimModel OptMod;

	int brAv, brSp, brSpecL, brSpecR, brPrep, brComp, brHarm, brHiper, brKfnl;	// ���������� ������ �������� ���������� ��������� ����������� ������

	int NPoints;								// ����� ����� ���������� ������������ �����
	int CurPoint;								// ����� ��������� �������������� ������������ �����
	double Kopt;								// ������� ��������� �������� ��������
	int Iopt;									// ����������� ������ �����

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();						// ������������� �������

	virtual void OK();						// ��������� �������� ����� �� �������
	virtual void Stop();					// ��������� ����������� ����� �� �������
	virtual void Work();					// ������ �������� �������
	virtual void ResumeWork();				// ����������� ������� ����� �����

	void CalculateAllModels(bool first);	// ������ �������
	void TestOnExcPoints(bool first);		// ���� �� ���������� �����
	void TestOnAnalizeOuts();				// ������ �� �������

	DECLARE_MESSAGE_MAP()
};

#endif
