#include "stdafx.h"

#include "BranchInfo.h"
#include "CreateModelDlg.h"
#include "ExcludeSpectraDlg.h"
#include "OptimModelDlg2.h"
#include "ModelResultDlg.h"
#include "ModelQltResultDlg.h"
#include "TemplateManagerDlg.h"
#include "ShowSpectraDlg.h"
#include "LoadSpectraPBDlg.h"
#include "SamplesHistoDlg.h"
#include "MainFrm.h"


static CProjectData *ParentPrj;
static CTransferData *pTrans;
static CString SortName = cEmpty;
static bool SortDir = true;

static bool iscancel = false;

static CCreateModelDlg* pModCreate = NULL;

bool PSortModSamUp(ModelSample& ModSam1, ModelSample& ModSam2)
{
	if(SortName == cSortTime)
	{
		CTime t1 = ModSam1.tLastSpecDate;
		CTime t2 = ModSam2.tLastSpecDate;
		if(t1 == t2)
		{
			return (ModSam1.SampleName.Compare(ModSam2.SampleName) < 0);
		}
		return t1 > t2;
	}

	ReferenceData *Ref1 = ModSam1.pRefSort;
	ReferenceData *Ref2 = ModSam2.pRefSort;
	if((SortName == cEmpty) || (Ref1 == NULL || Ref2 == NULL))
	{
		int TInd1 =ModSam1.iTransSort;
		int TInd2 =ModSam2.iTransSort;

		if(TInd1 >= 0 && TInd2 == -1)
			return false;

		if(TInd1 == -1 && TInd2 >= 0)
			return true;

		if(TInd1 < TInd2)
			return true;

		if(TInd2 < TInd1)
			return false;

        return (ModSam1.SampleName.Compare(ModSam2.SampleName) < 0);
	}
	else
	{
		if(Ref1->bNoValue && !Ref2->bNoValue)
			return true;

		if(Ref2->bNoValue)
			return false;

		return Ref1->Value < Ref2->Value;
	}
}

bool PSortModSamUpOld(ModelSample& ModSam1, ModelSample& ModSam2)
{
	CTransferData *pTr1 = ParentPrj->GetTransfer(ModSam1.TransName);
	CTransferData *pTr2 = ParentPrj->GetTransfer(ModSam2.TransName);

	if(SortName == cSortTime)
	{
		CSampleData *Sam1 = pTr1->GetSample(ModSam1.SampleName);
		CSampleData *Sam2 = pTr2->GetSample(ModSam2.SampleName);
		CTime t1 = Sam1->GetLastSpecDate();
		CTime t2 = Sam2->GetLastSpecDate();
		if(t1 == t2)
		{
			return (Sam1->GetName().Compare(Sam2->GetName()) < 0);
		}
		return t1 < t2;
	}

	ReferenceData *Ref1 = pTr1->GetSample(ModSam1.SampleName)->GetReferenceData(SortName);
	ReferenceData *Ref2 = pTr2->GetSample(ModSam2.SampleName)->GetReferenceData(SortName);
	if(Ref1 == NULL || Ref2 == NULL)
	{
		int TInd1 = pModCreate->GetTransNum(&ModSam1);
		int TInd2 = pModCreate->GetTransNum(&ModSam2);

		if(TInd1 >= 0 && TInd2 == -1)
			return false;

		if(TInd1 == -1 && TInd2 >= 0)
			return true;

		if(TInd1 < TInd2)
			return true;

		if(TInd2 < TInd1)
			return false;

        return (ModSam1.SampleName.Compare(ModSam2.SampleName) < 0);
	}
	else
	{
		if(Ref1->bNoValue && !Ref2->bNoValue)
			return true;

		if(Ref2->bNoValue)
			return false;

		return Ref1->Value < Ref2->Value;
	}
}

bool PSortModSamDown(ModelSample& ModSam1, ModelSample& ModSam2)
{
	if(SortName == cSortTime)
	{
		CTime t1 = ModSam1.tLastSpecDate;
		CTime t2 = ModSam2.tLastSpecDate;
		if(t1 == t2)
		{
			return (ModSam1.SampleName.Compare(ModSam2.SampleName) > 0);
		}
		return t1 < t2;
	}

	ReferenceData *Ref1 = ModSam1.pRefSort;
	ReferenceData *Ref2 = ModSam2.pRefSort;
	if(SortName == cEmpty || Ref1 == NULL || Ref2 == NULL)
	{
		int TInd1 =ModSam1.iTransSort;
		int TInd2 =ModSam2.iTransSort;

		if(TInd1 >= 0 && TInd2 == -1)
			return false;

		if(TInd1 == -1 && TInd2 >= 0)
			return true;

		if(TInd1 < TInd2)
			return true;

		if(TInd2 < TInd1)
			return false;

        return (ModSam1.SampleName.Compare(ModSam2.SampleName) > 0);
	}
	else
	{
		if(Ref1->bNoValue && !Ref2->bNoValue)
			return false;

		if(Ref2->bNoValue)
			return true;

		return Ref1->Value > Ref2->Value;
	}
}

bool PSortModSamDownOld(ModelSample& ModSam1, ModelSample& ModSam2)
{
	CTransferData *pTr1 = ParentPrj->GetTransfer(ModSam1.TransName);
	CTransferData *pTr2 = ParentPrj->GetTransfer(ModSam2.TransName);
	CSampleData *Sam1 = pTr1->GetSample(ModSam1.SampleName);
	CSampleData *Sam2 = pTr2->GetSample(ModSam2.SampleName);
	ReferenceData *Ref1 = pTr1->GetSample(ModSam1.SampleName)->GetReferenceData(SortName);
	ReferenceData *Ref2 = pTr2->GetSample(ModSam2.SampleName)->GetReferenceData(SortName);
	int TInd1 = pModCreate->GetTransNum(&ModSam1);
	int TInd2 = pModCreate->GetTransNum(&ModSam2);

	if(SortName == cSortTime)
	{
		CTime t1 = Sam1->GetLastSpecDate();
		CTime t2 = Sam2->GetLastSpecDate();
		if(t1 == t2)
		{
			return (Sam1->GetName().Compare(Sam2->GetName()) > 0);
		}
		return t1 > t2;
	}

	if(Ref1 == NULL || Ref2 == NULL)
	{
		if(TInd1 >= 0 && TInd2 == -1)
			return false;
		if(TInd1 == -1 && TInd2 >= 0)
			return true;
		if(TInd1 < TInd2)
			return true;
		if(TInd2 < TInd1)
			return false;

        return (ModSam1.SampleName.Compare(ModSam2.SampleName) > 0);
	}

	if(Ref1->bNoValue)
		return false;
	if(Ref2->bNoValue)
		return true;

	return Ref1->Value > Ref2->Value;
}

static int GetSpecPntColor(int item, int /*subitem*/, void* /*pData*/)
{
	if(pModCreate == NULL)
		return 0;

	COLORREF color = (pModCreate->IsPointExc(item)) ? cColorPass : CLR_DEFAULT;

	pModCreate->m_listAll.CurTxtColor = color;

	return 1;
}

static int GetSampleColor(int item, int /*subitem*/, void* /*pData*/)
{
	if(pModCreate == NULL)
		return 0;

	COLORREF color = (pModCreate->IsSampleUse(item)) ? cColorPass : CLR_DEFAULT;

	pModCreate->m_listSmpl.CurTxtColor = color;

	return 1;
}

static int GetParamColor(int item, int /*subitem*/, void* /*pData*/)
{
	if(pModCreate == NULL)
		return 0;

	COLORREF color = (pModCreate->IsParamUse(item)) ? cColorPass : CLR_DEFAULT;

	pModCreate->m_listParamAll.CurTxtColor = color;

	return 1;
}

static int GetParamBgColor(int /*item*/, int /*subitem*/, void* /*pData*/)
{
	if(pModCreate == NULL)
		return 0;

	COLORREF color = (pModCreate->IsAnTypeQlt()) ? GetSysColor(COLOR_3DFACE) : CLR_DEFAULT;

	pModCreate->m_listParamAll.CurBGColor = color;

	return 1;
}

static int GetTransColor(int item, int /*subitem*/, void* /*pData*/)
{
	if(pModCreate == NULL)
		return 0;

	COLORREF color = (pModCreate->IsTransNotVal(item)) ? cColorPass : CLR_DEFAULT;

	pModCreate->m_listTransAll.CurTxtColor = color;

	return 1;
}

IMPLEMENT_DYNAMIC(CCreateModelDlg, CDialog)

CCreateModelDlg::CCreateModelDlg(CModelData* Mod, int mode, CWnd* pParent)
	: CDialog(CCreateModelDlg::IDD, pParent)
{
	pModCreate = this;

	ModData = Mod;
	Mode = mode;

	m_vProcValid = 10;

	InitData();
}

CCreateModelDlg::~CCreateModelDlg()
{
}

void CCreateModelDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	RecalcMaxComp();
	CalcMinMaxCalibValue();

	DDX_Control(pDX, IDC_TAB_1, m_Tab);

	DDX_Control(pDX, IDC_INPUT_NAME, m_Name);
	DDX_Control(pDX, IDC_ANALYSIS_TYPE, m_AnType);
	DDX_Control(pDX, IDC_PRJPARAMS_LIST, m_listParamAll);
	DDX_Control(pDX, IDC_PRJPARAMSSEL_LIST, m_listParamSel);
	DDX_Control(pDX, IDC_MODTRANS_LIST, m_listTransAll);
	DDX_Control(pDX, IDC_MODTRANSSEL_LIST, m_listTransSel);
	DDX_CBIndex(pDX, IDC_ANALYSIS_TYPE, m_vAnType);
	cParcelDDX(pDX, IDC_INPUT_NAME, m_vName);
	DDV_MaxChars(pDX, m_vName, 50);

	DDX_Control(pDX, IDC_SAMPLES_LIST, m_listSmpl);
	DDX_Control(pDX, IDC_SAMPLESSEL_LIST, m_listCalib);
	DDX_Control(pDX, IDC_SAMPLESSEL_LIST_2, m_listValid);
	DDX_Control(pDX, IDC_TEMPLATE_SEL, m_Template);
	DDX_Control(pDX, IDC_USE_TEMPLATE, m_UseTemplate);
	DDX_CBIndex(pDX, IDC_TEMPLATE_SEL, m_vTemplate);
	DDX_Check(pDX, IDC_USE_TEMPLATE, m_vUseTemplate);
	DDX_Control(pDX, IDC_PROCENT, m_ProcValid);

	DDX_Control(pDX, IDC_LOWLIM_SPEC, m_LowSpec);
	DDX_Control(pDX, IDC_UPPERLIM_SPEC, m_UpperSpec);
	DDX_Control(pDX, IDC_SPECTRUM_TYPE, m_SpType);
	DDX_Control(pDX, IDC_PREPROC_1, m_Preproc0);
	DDX_Control(pDX, IDC_PREPROC_2, m_Preproc1);
	DDX_Control(pDX, IDC_PREPROC_3, m_Preproc2);
	DDX_Control(pDX, IDC_PREPROC_4, m_Preproc3);
	DDX_Control(pDX, IDC_PREPROC_5, m_Preproc4);
	DDX_Control(pDX, IDC_PREPROC_6, m_Preproc5);
	DDX_Control(pDX, IDC_PREPROC_7, m_Preproc6);
	DDX_Control(pDX, IDC_CALIBPARAMS_LIST, m_listParamC);
	DDX_Control(pDX, IDC_MAXMSD_CALIB, m_MSD);
	DDX_Control(pDX, IDC_LOWLIM_CALIB, m_LowCalib);
	DDX_Control(pDX, IDC_UPPERLIM_CALIB, m_UpperCalib);
	DDX_Control(pDX, IDC_CORR_CALIB_A, m_CorrA);
	DDX_Control(pDX, IDC_CORR_CALIB_B, m_CorrB);
	DDX_CBIndex(pDX, IDC_SPECTRUM_TYPE, m_vSpType);

	cParcelDDX(pDX, IDC_PROCENT, m_vProcValid);
	cParcelDDV(pDX, ParcelLoadString(STRID_CREATE_MODEL_SMP_DDV_PROCVALID), m_vProcValid, 0, 100);

	cParcelDDX(pDX, IDC_LOWLIM_SPEC, m_vLowSpec);
	cParcelDDV(pDX, ParcelLoadString(STRID_CREATE_MODEL_DLG_DDV_LOWSPEC), m_vLowSpec, LimSpecRangeMin, LimSpecRangeMax);
	cParcelDDX(pDX, IDC_UPPERLIM_SPEC, m_vUpperSpec);
	cParcelDDV(pDX, ParcelLoadString(STRID_CREATE_MODEL_DLG_DDV_UPPERSPEC), m_vUpperSpec, LimSpecRangeMin, LimSpecRangeMax);
	cParcelDDX(pDX, IDC_MAXMSD_CALIB, m_vMSD);
	cParcelDDV(pDX, ParcelLoadString(STRID_CREATE_MODEL_DLG_DDV_MAXMSDCALIB), m_vMSD, 0., 1.e9);
	cParcelDDX(pDX, IDC_LOWLIM_CALIB, m_vLowCalib);

	CString sl = cFmt31, su = cFmt31;
	if(CurParam != Params.end())
	{
		ParentPrj = GetProjectByHItem(ModData->GetParentHItem());
		CParamData* pPar = ParentPrj->GetParam(CurParam->ParamName);
		if(pPar != NULL)
		{
			sl.Format(ParcelLoadString(STRID_CREATE_MODEL_DLG_DDV_LOWCALIB), pPar->GetFormat(), pPar->GetFormat());
			su.Format(ParcelLoadString(STRID_CREATE_MODEL_DLG_DDV_UPPERCALIB), pPar->GetFormat(), pPar->GetFormat());
		}
	}
	cParcelDDV(pDX, sl, m_vLowCalib, MinValue, MaxValue);
	cParcelDDX(pDX, IDC_UPPERLIM_CALIB, m_vUpperCalib);
	cParcelDDV(pDX, su, m_vUpperCalib, MinValue, MaxValue);
	cParcelDDX(pDX, IDC_CORR_CALIB_A, m_vCorrA);
	cParcelDDV(pDX, ParcelLoadString(STRID_CREATE_MODEL_DLG_DDV_CORR_A), m_vCorrA, -1.e9, 1.e9);
	cParcelDDX(pDX, IDC_CORR_CALIB_B, m_vCorrB);
	cParcelDDV(pDX, ParcelLoadString(STRID_CREATE_MODEL_DLG_DDV_CORR_B), m_vCorrB, -1.e9, 1.e9);

	DDX_Control(pDX, IDC_MODEL_TYPE, m_ModType);
	DDX_Control(pDX, IDC_MODEL_QLT_TYPE, m_ModQltType);
	DDX_Control(pDX, IDC_MODEL_HSO_TYPE, m_HSOType);
	DDX_Control(pDX, IDC_NCOMP, m_NComp);
	DDX_Control(pDX, IDC_DIMHIPER, m_DimHiper);
	DDX_Control(pDX, IDC_NHARM, m_NHarm);
	DDX_Control(pDX, IDC_KFNL, m_Kfnl);
	DDX_Control(pDX, IDC_MAXMAH, m_MaxMah);
	DDX_Control(pDX, IDC_MAXMAH_2, m_MaxMah2);
	DDX_Control(pDX, IDC_MAXMSD, m_MaxMSD);
	DDX_Control(pDX, IDC_SPECPOINTS_ALL_LIST, m_listAll);
	DDX_Control(pDX, IDC_SPECPOINTS_EXC_LIST, m_listExc);
	DDX_Control(pDX, IDC_USE_SECV, m_UseSECV);
	DDX_CBIndex(pDX, IDC_MODEL_TYPE, m_vModType);
	DDX_CBIndex(pDX, IDC_MODEL_HSO_TYPE, m_vHSOType);

	int nharm_min = (m_vHSOType == C_MOD_MODEL_HSO_LPM || m_vHSOType == C_MOD_MODEL_HSO_NLPM) ? 0 : 3;
	double kfnl_min = (m_vHSOType == C_MOD_MODEL_HSO_NLPM || m_vHSOType == C_MOD_MODEL_HSO_NLHM) ? 0.1 : 0.;

	/*cParcelDDX*/DDX_Text(pDX, IDC_DIMHIPER, m_vDimHiper);
	cParcelDDX(pDX, IDC_DIMHIPER, m_vDimHiper);
	if(m_vModType == C_MOD_MODEL_HSO)
		cParcelDDV(pDX, ParcelLoadString(STRID_CREATE_MODEL_DLG_DDV_HIPER), m_vDimHiper, 0.0001, 1.e9);
	cParcelDDX(pDX, IDC_NHARM, m_vNHarm);
	if(m_vModType == C_MOD_MODEL_HSO)
		cParcelDDV(pDX, ParcelLoadString(STRID_CREATE_MODEL_DLG_DDV_NHARM), m_vNHarm, nharm_min, 60);
	cParcelDDX(pDX, IDC_KFNL, m_vKfnl);
	if(m_vModType == C_MOD_MODEL_HSO)
		cParcelDDV(pDX, ParcelLoadString(STRID_CREATE_MODEL_DLG_DDV_KFNL), m_vKfnl, kfnl_min, 1.);
	else
	{
		if(m_vDimHiper < 0.0001 || m_vDimHiper > 1.e9)  m_vDimHiper = 0.1;
		if(m_vNHarm < nharm_min || m_vNHarm > 60)
			m_vNHarm = (m_vHSOType == C_MOD_MODEL_HSO_LPM || m_vHSOType == C_MOD_MODEL_HSO_NLPM) ? 0 : 3;
		if(m_vKfnl < kfnl_min || m_vKfnl > 1.)
			m_vKfnl = (m_vHSOType == C_MOD_MODEL_HSO_NLPM || m_vHSOType == C_MOD_MODEL_HSO_NLHM) ? 0.1 : 0.;
	}

	cParcelDDX(pDX, IDC_NCOMP, m_vNComp);
	cParcelDDX(pDX, IDC_MAXMAH_2, m_vMaxMah2);
	cParcelDDV(pDX, ParcelLoadString(STRID_CREATE_MODEL_DLG_DDV_MAXMAH), m_vMaxMah2, 0.1, 1.e9);
	cParcelDDX(pDX, IDC_MAXMAH, m_vMaxMah);
	cParcelDDV(pDX, ParcelLoadString(STRID_CREATE_MODEL_DLG_DDV_MAXMAH), m_vMaxMah, 0.1, 1.e9);
	cParcelDDX(pDX, IDC_MAXMSD, m_vMaxMSD);
	cParcelDDV(pDX, ParcelLoadString(STRID_CREATE_MODEL_DLG_DDV_MAXMSDQLT), m_vMaxMSD, 0.001, 1000.);
	DDX_Check(pDX, IDC_USE_SECV, m_vUseSECV);
}

BEGIN_MESSAGE_MAP(CCreateModelDlg, CDialog)

	ON_BN_CLICKED(IDHELP, OnHelp)

	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB_1, OnTabSelchange)

	ON_CBN_SELCHANGE(IDC_ANALYSIS_TYPE, OnAnTypeChange)
	ON_BN_CLICKED(IDC_ADD, OnParamAdd)
	ON_BN_CLICKED(IDC_ADD_ALL, OnParamAddAll)
	ON_BN_CLICKED(IDC_REMOVE, OnParamRemove)
	ON_BN_CLICKED(IDC_REMOVE_ALL, OnParamRemoveAll)
	ON_BN_CLICKED(IDC_ADD_5, OnTransAdd)
	ON_BN_CLICKED(IDC_REMOVE_5, OnTransRemove)
	ON_BN_CLICKED(IDC_REMOVE_ALL_5, OnTransRemoveAll)

	ON_NOTIFY(HDN_ITEMCLICK, 0, OnSamplesListsColumnClick)
	ON_CBN_SELCHANGE(IDC_TEMPLATE_SEL, OnTemplateChange)
	ON_BN_CLICKED(IDC_ADD_3, OnAddCalib)
	ON_BN_CLICKED(IDC_ADD_ALL_3, OnAddAllCalib)
	ON_BN_CLICKED(IDC_REMOVE_3, OnRemoveCalib)
	ON_BN_CLICKED(IDC_REMOVE_ALL_3, OnRemoveAllCalib)
	ON_BN_CLICKED(IDC_SHOWSPEC_3, OnShowSpecCalib)
	ON_BN_CLICKED(IDC_PLOT_3, OnPlotCalib)
	ON_BN_CLICKED(IDC_ADD_2, OnAddValid)
	ON_BN_CLICKED(IDC_ADD_ALL_2, OnAddAllValid)
	ON_BN_CLICKED(IDC_REMOVE_2, OnRemoveValid)
	ON_BN_CLICKED(IDC_REMOVE_ALL_2, OnRemoveAllValid)
	ON_BN_CLICKED(IDC_SHOWSPEC_2, OnShowSpecValid)
	ON_BN_CLICKED(IDC_PLOT_2, OnPlotValid)
	ON_BN_CLICKED(IDC_USE_TEMPLATE, OnUseTemplate)
	ON_BN_CLICKED(IDC_TEMPLATE, OnTemplate)
	ON_BN_CLICKED(IDC_OPTIM, OnOptim)
	ON_BN_CLICKED(IDC_PLOT, OnShowSpectra)
	ON_BN_CLICKED(IDC_AUTOCHOOSE, OnAutoChoose)

	ON_NOTIFY(LVN_ITEMCHANGED, IDC_CALIBPARAMS_LIST, OnParamsCListSelChange)
	ON_CBN_SELCHANGE(IDC_PREPROC_1, OnPreproc0)
	ON_CBN_SELCHANGE(IDC_PREPROC_2, OnPreproc1)
	ON_CBN_SELCHANGE(IDC_PREPROC_3, OnPreproc2)
	ON_CBN_SELCHANGE(IDC_PREPROC_4, OnPreproc3)
	ON_CBN_SELCHANGE(IDC_PREPROC_5, OnPreproc4)
	ON_CBN_SELCHANGE(IDC_PREPROC_6, OnPreproc5)
	ON_CBN_SELCHANGE(IDC_PREPROC_7, OnPreproc6)
	ON_EN_KILLFOCUS(IDC_LOWLIM_SPEC, OnChangeLowSpecRange)
	ON_EN_KILLFOCUS(IDC_UPPERLIM_SPEC, OnChangeUpperSpecRange)
	ON_EN_KILLFOCUS(IDC_MAXMSD_CALIB, OnMaxMSDCalibChange)
	ON_EN_KILLFOCUS(IDC_LOWLIM_CALIB, OnLowLimCalibChange)
	ON_EN_KILLFOCUS(IDC_UPPERLIM_CALIB, OnUpperLimCalibChange)
	ON_EN_KILLFOCUS(IDC_CORR_CALIB_B, OnCorrCalibBChange)
	ON_EN_KILLFOCUS(IDC_CORR_CALIB_A, OnCorrCalibAChange)

	ON_CBN_SELCHANGE(IDC_MODEL_TYPE, OnModelTypeChange)
	ON_CBN_SELCHANGE(IDC_MODEL_HSO_TYPE, OnModelHSOTypeChange)
	ON_BN_CLICKED(IDC_ADD_4, OnAddExc)
	ON_BN_CLICKED(IDC_ADD_ALL_4, OnAddAllExc)
	ON_BN_CLICKED(IDC_REMOVE_4, OnRemoveExc)
	ON_BN_CLICKED(IDC_REMOVE_ALL_4, OnRemoveAllExc)
	ON_BN_CLICKED(IDC_CALCULATE, OnCalculate)

END_MESSAGE_MAP()

BOOL CCreateModelDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	iscancel = false;

	USES_CONVERSION;

	TC_ITEM TabCtrlItem;
	TabCtrlItem.mask = TCIF_TEXT;
	TabCtrlItem.pszText = (LPWSTR)GETSTRUI(STRID_CREATE_MODEL_PRM_HDR);
	m_Tab.InsertItem(0, &TabCtrlItem);
	TabCtrlItem.pszText = (LPWSTR)GETSTRUI(STRID_CREATE_MODEL_SMP_HDR);
	m_Tab.InsertItem(1, &TabCtrlItem);
	TabCtrlItem.pszText = (LPWSTR)GETSTRUI(STRID_CREATE_MODEL_PRE_HDR);
	m_Tab.InsertItem(2, &TabCtrlItem);
	TabCtrlItem.pszText = (LPWSTR)GETSTRUI(STRID_CREATE_MODEL_SET_HDR);
	m_Tab.InsertItem(3, &TabCtrlItem);

	FillAllPages();

	m_listSmpl.SetIconList();
	m_listCalib.SetIconList();
	m_listValid.SetIconList();

	m_listSmpl.SetUserCallback(LCF_CB_SI_TXTCOLOR, GetSampleColor);
	m_listSmpl.EnableUserCallback(LCF_CB_SI_TXTCOLOR, true);

	m_listParamAll.SetUserCallback(LCF_CB_SI_TXTCOLOR, GetParamColor);
	m_listParamAll.EnableUserCallback(LCF_CB_SI_TXTCOLOR, true);
	m_listParamAll.SetUserCallback(LCF_CB_SI_BGCOLOR, GetParamBgColor);
	m_listParamAll.EnableUserCallback(LCF_CB_SI_BGCOLOR, true);

	m_listTransAll.SetUserCallback(LCF_CB_SI_TXTCOLOR, GetTransColor);
	m_listTransAll.EnableUserCallback(LCF_CB_SI_TXTCOLOR, true);

	m_listAll.SetUserCallback(LCF_CB_SI_TXTCOLOR, GetSpecPntColor);
	m_listAll.EnableUserCallback(LCF_CB_SI_TXTCOLOR, true);

	CString Hdr;
	switch(Mode)
	{
	case C_MODDLG_MODE_CREATE:
		Hdr = ParcelLoadString(STRID_CREATE_MODEL_DLG_HDR_CREATE);
		break;
	case C_MODDLG_MODE_CHANGE:
		Hdr = ParcelLoadString(STRID_CREATE_MODEL_DLG_HDR_CHANGE);
		break;
	case C_MODDLG_MODE_COPY:
		Hdr = ParcelLoadString(STRID_CREATE_MODEL_DLG_HDR_COPY);
		break;
	}
	SetWindowText(Hdr);

	ShowFields(C_MODDLG_PAGE_PRM);

	GetDlgItem(IDCANCEL)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_DLG_DECLINE));
	GetDlgItem(IDHELP)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_DLG_HELP));

	GetDlgItem(IDC_ADD)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_PRM_ADD));
	GetDlgItem(IDC_ADD_ALL)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_PRM_ADDALL));
	GetDlgItem(IDC_REMOVE)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_PRM_REMOVE));
	GetDlgItem(IDC_REMOVE_ALL)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_PRM_REMOVEALL));
	GetDlgItem(IDC_ADD_5)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_PRM_TRANS_ADD));
	GetDlgItem(IDC_REMOVE_5)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_PRM_TRANS_REMOVE));
	GetDlgItem(IDC_REMOVE_ALL_5)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_PRM_TRANS_REMOVEALL));
	GetDlgItem(IDC_STATIC_1)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_PRM_MODNAME));
	GetDlgItem(IDC_STATIC_2)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_PRM_ALLPARAMS));
	GetDlgItem(IDC_STATIC_3)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_PRM_SELPARAMS));
	GetDlgItem(IDC_STATIC_4)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_PRM_ANTYPE));
	GetDlgItem(IDC_STATIC_5)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_PRM_ANPARAMS));
	GetDlgItem(IDC_STATIC_50)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_PRM_TRANSES));
	GetDlgItem(IDC_STATIC_51)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_PRM_ALLTRANS));
	GetDlgItem(IDC_STATIC_52)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_PRM_SELTRANS));

	GetDlgItem(IDC_ADD_2)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_SMP_ADD));
	GetDlgItem(IDC_ADD_ALL_2)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_SMP_ADDALL));
	GetDlgItem(IDC_REMOVE_2)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_SMP_REMOVE));
	GetDlgItem(IDC_REMOVE_ALL_2)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_SMP_REMOVEALL));
	GetDlgItem(IDC_ADD_3)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_SMP_ADD));
	GetDlgItem(IDC_ADD_ALL_3)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_SMP_ADDALL));
	GetDlgItem(IDC_REMOVE_3)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_SMP_REMOVE));
	GetDlgItem(IDC_REMOVE_ALL_3)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_SMP_REMOVEALL));
	GetDlgItem(IDC_SHOWSPEC_3)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_SMP_SPECTRA));
	GetDlgItem(IDC_PLOT_3)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_SMP_HISTOGRAM));
	GetDlgItem(IDC_SHOWSPEC_2)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_SMP_SPECTRA));
	GetDlgItem(IDC_PLOT_2)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_SMP_HISTOGRAM));
	GetDlgItem(IDC_OPTIM)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_SMP_OPTIM));
	GetDlgItem(IDC_TEMPLATE)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_SMP_TMPLMANAGER));
	GetDlgItem(IDC_USE_TEMPLATE)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_SMP_TEMPLATES));
	GetDlgItem(IDC_STATIC_10)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_SMP_OPTIMMODEL));
	GetDlgItem(IDC_PLOT)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_SMP_PLOT));
	GetDlgItem(IDC_STATIC_53)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_SMP_PROCVALID));
	GetDlgItem(IDC_AUTOCHOOSE)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_SMP_AUTOCHOOSE));

	GetDlgItem(IDC_STATIC_12)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_PRE_SPECPARAM));
	GetDlgItem(IDC_STATIC_19)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_PRE_LIMLOW));
	GetDlgItem(IDC_STATIC_20)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_PRE_LIMUPPER));
	GetDlgItem(IDC_STATIC_13)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_PRE_SPECTYPE));
	GetDlgItem(IDC_STATIC_21)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_PRE_PREPROC));
	GetDlgItem(IDC_STR_PREP_1)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_PRE_PREP_1));
	GetDlgItem(IDC_STR_PREP_2)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_PRE_PREP_2));
	GetDlgItem(IDC_STR_PREP_3)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_PRE_PREP_3));
	GetDlgItem(IDC_STR_PREP_4)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_PRE_PREP_4));
	GetDlgItem(IDC_STR_PREP_5)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_PRE_PREP_5));
	GetDlgItem(IDC_STR_PREP_6)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_PRE_PREP_6));
	GetDlgItem(IDC_STR_PREP_7)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_PRE_PREP_7));
	GetDlgItem(IDC_STATIC_38)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_PRE_EXCFROMSPEC));
	GetDlgItem(IDC_ADD_4)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_PRE_EXCLUDE));
	GetDlgItem(IDC_ADD_ALL_4)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_PRE_ADDALL));
	GetDlgItem(IDC_REMOVE_4)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_PRE_INCLUDE));
	GetDlgItem(IDC_REMOVE_ALL_4)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_PRE_REMOVEALL));

	GetDlgItem(IDC_STATIC_14)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_SET_PARAMPROP));
	GetDlgItem(IDC_STATIC_15)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_SET_MSD));
	GetDlgItem(IDC_STATIC_16)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_SET_LOW));
	GetDlgItem(IDC_STATIC_17)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_SET_UPPER));
	GetDlgItem(IDC_STATIC_23)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_SET_CORRB));
	GetDlgItem(IDC_STATIC_25)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_SET_CORRA));
	GetDlgItem(IDC_STATIC_29)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_SET_MODTYPE));
	GetDlgItem(IDC_STATIC_34)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_SET_HSOTYPE));
	GetDlgItem(IDC_STATIC_30)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_SET_NCOMP));
	GetDlgItem(IDC_STATIC_35)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_SET_DIMHIPER));
	GetDlgItem(IDC_STATIC_36)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_SET_NHARM));
	GetDlgItem(IDC_STATIC_37)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_SET_KFNL));
	GetDlgItem(IDC_STATIC_31)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_SET_MAXMAH));
	GetDlgItem(IDC_STATIC_32)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_SET_MAXMSD));
	GetDlgItem(IDC_STATIC_28)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_SET_ALLPARAM));
	GetDlgItem(IDC_STATIC_33)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_SET_HSOPARAM));
	GetDlgItem(IDC_USE_SECV)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_SET_NOSECV));
	GetDlgItem(IDC_CALCULATE)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_SET_CALCULATE));
	GetDlgItem(IDC_STATIC_47)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_SET_QLTPROP));
	GetDlgItem(IDC_STATIC_48)->SetWindowText(ParcelLoadString(STRID_CREATE_MODEL_SET_MAXMAH));

	GetDlgItem(IDHELP)->EnableWindow(pFrame->IsExistHelp());

	return true;
}

void CCreateModelDlg::OnAccept()
{
	ModData->FreeSpectraData();

	CDialog::OnOK();
}

void CCreateModelDlg::OnCancel()
{
	iscancel = true;

	ModData->FreeSpectraData();

	CDialog::OnCancel();
}

void CCreateModelDlg::OnTabSelchange(NMHDR* /*pNMHDR*/, LRESULT* pResult)
{
	int old1 = m_vLowSpec;
	int old2 = m_vUpperSpec;

	if(!UpdateData())
	{
		m_Tab.SetCurSel(m_vTab);
		return;
	}

	if(m_vLowSpec != old1 || m_vUpperSpec != old2)
	{
		if(m_vUpperSpec < m_vLowSpec)
			m_vUpperSpec = m_vLowSpec;

		RecreateSpecPoints();

		UpdateData(0);
	}

	m_vTab = m_Tab.GetCurSel();

	ShowFields();

	*pResult = 0;
}

void CCreateModelDlg::OnAnTypeChange()
{
	int old = m_vAnType;
	if(!UpdateData())
		return;

	if(old == m_vAnType)
		return;

	if(m_vAnType == C_ANALYSIS_QLT)
		OnParamRemoveAll();
	else
		RecreateSmplLists();

	UpdateData(0);

	EnableFields();
}

void CCreateModelDlg::OnParamAdd()
{
	if(!UpdateData())
		return;

	int Ind = -1, nsel = m_listParamAll.GetSelectedCount();
	if(nsel <= 0)
		return;

	for(int i=0; i<nsel; i++)
	{
		Ind = m_listParamAll.GetNextItem(Ind, LVNI_SELECTED);
		if(Ind < 0 || Ind >= NAllParams)
			break;

		CString Name = ParentPrj->GetParam(Ind)->GetName();

		ItModPar it;
		for(it=Params.begin(); it!=Params.end(); ++it)
			if(!(*it).ParamName.Compare(Name))
				break;

		if(it == Params.end())
			Params.push_back(Name);
	}

	FillListParamSel(int(Params.size() - 1));

	RecreateSmplLists();

	EnableFields();
}

void CCreateModelDlg::OnParamAddAll()
{
	if(!UpdateData())
		return;

	Params.clear();
	for(int i=0; i<NAllParams; i++)
		Params.push_back(ParentPrj->GetParam(i)->GetName());

	FillListParamSel(0);
	RecreateSmplLists();

	EnableFields();
}

void CCreateModelDlg::OnParamRemove()
{
	if(!UpdateData())
		return;

	int NAllSel = int(Params.size());
	int *selbuf = new int[NAllSel];
	int nsel = m_listParamSel.GetSelItems(NAllSel, selbuf);
	if(nsel < 0)
	{
		delete[] selbuf;
		return;
	}

	for(int i=0; i<nsel; i++)
	{
		ItModPar it = Params.begin() + (selbuf[i] - i);
		Params.erase(it);
	}

	int ind = (selbuf[0] >= int(Params.size())) ? int(Params.size()) : selbuf[0];
	delete[] selbuf;

	FillListParamSel(ind);
	RecreateSmplLists();

	EnableFields();
}

void CCreateModelDlg::OnParamRemoveAll()
{
	if(!UpdateData())
		return;

	Params.clear();
	FillListParamSel(-1);
	RecreateSmplLists();

	EnableFields();
}

void CCreateModelDlg::OnTransAdd()
{
	if(!UpdateData())
		return;

	int NS = int(TransesSel.size());
	int N = int(TransesAll.size());
	int Ind = m_listTransAll.GetNextItem(-1, LVNI_SELECTED);
	if(Ind < 0 || Ind >= N || IsTransNotVal(Ind))
		return;

	CString Name = TransesAll[Ind];
	TransesSel.push_back(Name);

	if(NS == 0)
	{
		SetBasicTrans(Name);

		if(ParentPrj->GetTransfer(Name)->GetPreprocInd(0) != C_PREPROC_0)
		{
			ParentPrj->GetTransfer(Name)->GetAllPreproc(m_vPreproc);
		}
/*
		if(m_vPreproc[0] == C_PREPROC_0)
		{
			m_vPreproc[0] = C_PREPROC_B;
			m_vPreproc[1] = C_PREPROC_N;
			m_vPreproc[2] = C_PREPROC_M;
		}
*/
		m_Preproc0.SetCurSel(m_vPreproc[0]);
		FillListsPreproc(0);
	}
	else
	{
		FillListTransSel(int(TransesSel.size() - 1));
		RecreateSmplLists(false);
	}

	EnableFields();
}

void CCreateModelDlg::OnTransRemove()
{
	if(!UpdateData())
		return;

	int NAllSel = int(TransesSel.size());
	int *selbuf = new int[NAllSel];
	int nsel = m_listTransSel.GetSelItems(NAllSel, selbuf);
	if(nsel < 0)
	{
		delete[] selbuf;
		return;
	}

	bool change = false;
	for(int i=0; i<nsel; i++)
	{
		ItStr it = TransesSel.begin() + (selbuf[i] - i);
		if(it == TransesSel.begin())
			change = true;

		TransesSel.erase(it);
	}

	int ind = (selbuf[0] >= int(TransesSel.size())) ? int(TransesSel.size()) : selbuf[0];
	delete[] selbuf;

	if(change)
	{
		if(TransesSel.size() > 0)
			SetBasicTrans(TransesSel[0]);
		else
			SetBasicTrans(cOwn);

		FillListsPreproc(0);
	}
	else
	{
		FillListTransSel(ind);
		RecreateSmplLists(false);
	}

	EnableFields();
}

void CCreateModelDlg::OnTransRemoveAll()
{
	if(!UpdateData())
		return;

	TransesSel.clear();

	SetBasicTrans(cOwn);
	FillListsPreproc(0);
	
	EnableFields();
}

int CCreateModelDlg::GetSamSelAll()
{
	SamSelAll.clear();
	int Ind = -1, nsel = m_listSmpl.GetSelectedCount();
	if(nsel <= 0)
		return 0;

	for(int i=0; i<nsel; i++)
	{
		Ind = m_listSmpl.GetNextItem(Ind, LVNI_SELECTED);
		if(Ind < 0) 
		{
			break;
		}
		SamSelAll.push_back(m_listSmpl.GetItemText(Ind, 0));
	}
	return nsel;
}

int CCreateModelDlg::GetSamSelClb()
{
	SamSelClb.clear();
	int Ind = -1, nsel = m_listCalib.GetSelectedCount();
	if(nsel <= 0)
		return 0;

	for(int i=0; i<nsel; i++)
	{
		Ind = m_listCalib.GetNextItem(Ind, LVNI_SELECTED);
		if(Ind < 0)
		{
			break;
		}
		SamSelClb.push_back(m_listCalib.GetItemText(Ind, 0));
	}
	return nsel;
}

int CCreateModelDlg::GetSamSelVal()
{
	SamSelVal.clear();
	int Ind = -1, nsel = m_listValid.GetSelectedCount();
	if(nsel <= 0)
		return 0;

	for(int i=0; i<nsel; i++)
	{
		Ind = m_listValid.GetNextItem(Ind, LVNI_SELECTED);
		if(Ind < 0)
		{
			break;
		}
		SamSelVal.push_back(m_listValid.GetItemText(Ind, 0));
	}
	return nsel;
}

void CCreateModelDlg::OnAddCalib()
{
	if(!UpdateData())
		return;

	int Ind = -1, nsel = m_listSmpl.GetSelectedCount(), N = int(NamesSmpl.size());
	if(nsel <= 0)
		return;

	for(int i=0; i<nsel; i++)
	{
		Ind = m_listSmpl.GetNextItem(Ind, LVNI_SELECTED);
		if(Ind < 0 || Ind >= N)
			break;

		ItModSam it, itA = NamesSmpl.begin() + Ind;
		for(it=NamesCalib.begin(); it!=NamesCalib.end(); ++it)
			if(!it->SampleName.Compare(itA->SampleName) && !it->TransName.Compare(itA->TransName))
			{
				it->SetAll(true);
				break;
			}

		if(it == NamesCalib.end())
			NamesCalib.push_back(*itA);
	}

	FillListCalib(int(NamesCalib.size()) - 1);
	RefillParamCList();

	EnableFields();
}

void CCreateModelDlg::OnAddAllCalib()
{
	if(!UpdateData())
		return;

	ItModSam it, itA;
	for(itA=NamesSmpl.begin(); itA!=NamesSmpl.end(); ++itA)
	{
		for(it=NamesCalib.begin(); it!=NamesCalib.end(); ++it)
			if(!it->SampleName.Compare(itA->SampleName) && !it->TransName.Compare(itA->TransName))
			{
				it->SetAll(true);
				break;
			}

		if(it == NamesCalib.end())
			NamesCalib.push_back(*itA);
	}

	SortNamesCalib();
	FillListCalib(int(NamesCalib.size()) - 1);
	RefillParamCList();

	EnableFields();
}

void CCreateModelDlg::OnRemoveCalib()
{
	if(!UpdateData())
		return;

	int Ind = -1, nsel = m_listCalib.GetSelectedCount(), N = int(NamesCalib.size());
	if(nsel <= 0)
		return;

	for(int i=0; i<nsel; i++)
	{
		Ind = m_listCalib.GetNextItem(Ind, LVNI_SELECTED);
		if(Ind < 0 || Ind >= N)
			break;

		ItModSam it = NamesCalib.begin() + (Ind - i);
		NamesCalib.erase(it);
	}

	SortNamesCalib();
	FillListCalib(0);
	RefillParamCList();

	EnableFields();
}

void CCreateModelDlg::OnRemoveAllCalib()
{
	if(!UpdateData())
		return;

	NamesCalib.clear();

	FillListCalib(-1);
	RefillParamCList();

	EnableFields();
}

void CCreateModelDlg::OnShowSpecCalib()
{
	int N = int(NamesCalib.size());
	int Ind = m_listCalib.GetNextItem(-1, LVNI_SELECTED);
	if(Ind < 0 || Ind >= N)
		return;

	ModelSample *MSam = &NamesCalib[Ind];
	CTransferData *pTr = ParentPrj->GetTransfer(MSam->TransName);
	ModelSample ModSam(pTr->GetSample(MSam->SampleName));
	ModSam.Copy(MSam);

	CExcludeSpectraDlg Dlg(ParentPrj, &ModSam, GetTransNum(MSam) + 1);
	if(Dlg.DoModal() != IDOK)
		return;

	NamesCalib[Ind].Copy(&ModSam);

	ItModSam it = NamesCalib.begin() + Ind;
	if(it->GetNSpectraUsed() <= 0)
		NamesCalib.erase(it);

	FillListCalib(Ind);
	RefillParamCList();
}

void CCreateModelDlg::OnPlotCalib()
{
	VPSam SamH;

	ItModSam it;
	for(it=NamesCalib.begin(); it!=NamesCalib.end(); ++it)
	{
		CTransferData* pTr = ParentPrj->GetTransfer(it->TransName);
		if(!pTr)
			continue;
		CSampleData *Sam = pTr->GetSample(it->SampleName);
		if(Sam != NULL)
			SamH.push_back(Sam);
	}

	CSamplesHistoDlg dlg(&SamH, &Params, ParentPrj, C_SAMPLESH_CALIB, this);
	dlg.DoModal();
}

void CCreateModelDlg::OnAddValid()
{
	if(!UpdateData())
		return;

	int Ind = -1, nsel = m_listSmpl.GetSelectedCount(), N = int(NamesSmpl.size());
	if(nsel <= 0)
		return;

	for(int i=0; i<nsel; i++)
	{
		Ind = m_listSmpl.GetNextItem(Ind, LVNI_SELECTED);
		if(Ind < 0 || Ind >= N)
			break;

		ItModSam it, itA = NamesSmpl.begin() + Ind;
		for(it=NamesValid.begin(); it!=NamesValid.end(); ++it)
			if(!it->SampleName.Compare(itA->SampleName) && !it->TransName.Compare(itA->TransName))
			{
				it->SetAll(true);
				break;
			}

		if(it == NamesValid.end())
			NamesValid.push_back(*itA);
	}

	FillListValid(int(NamesValid.size()) - 1);

	EnableFields();
}

void CCreateModelDlg::OnAddAllValid()
{
	if(!UpdateData())
		return;

	ItModSam it, itA;
	for(itA=NamesSmpl.begin(); itA!=NamesSmpl.end(); ++itA)
	{
		for(it=NamesValid.begin(); it!=NamesValid.end(); ++it)
			if(!it->SampleName.Compare(itA->SampleName) && !it->TransName.Compare(itA->TransName))
			{
				it->SetAll(true);
				break;
			}

		if(it == NamesValid.end())
			NamesValid.push_back(*itA);
	}

	SortNamesValid();
	FillListValid(int(NamesValid.size()) - 1);

	EnableFields();
}

void CCreateModelDlg::OnRemoveValid()
{
	if(!UpdateData())
		return;

	int Ind = -1, nsel = m_listValid.GetSelectedCount(), N = int(NamesValid.size());
	if(nsel <= 0)
		return;

	for(int i=0; i<nsel; i++)
	{
		Ind = m_listValid.GetNextItem(Ind, LVNI_SELECTED);
		if(Ind < 0 || Ind >= N)
			break;

		ItModSam it = NamesValid.begin() + (Ind - i);
		NamesValid.erase(it);
	}

	SortNamesValid();
	FillListValid(0);

	EnableFields();
}

void CCreateModelDlg::OnRemoveAllValid()
{
	if(!UpdateData())
		return;

	NamesValid.clear();

	FillListValid(-1);

	EnableFields();
}

void CCreateModelDlg::OnShowSpecValid()
{
	int N = int(NamesValid.size());
	int Ind = m_listValid.GetNextItem(-1, LVNI_SELECTED);
	if(Ind < 0 || Ind >= N)
		return;

	ModelSample *MSam = &NamesValid[Ind];
	CTransferData *Trans = ParentPrj->GetTransfer(MSam->TransName);
	ModelSample ModSam(Trans->GetSample(MSam->SampleName));
	ModSam.Copy(MSam);

	CExcludeSpectraDlg Dlg(ParentPrj, &ModSam, GetTransNum(MSam) + 1);
	if(Dlg.DoModal() != IDOK)
		return;

	NamesValid[Ind].Copy(&ModSam);

	ItModSam it = NamesValid.begin() + Ind;
	if(it->GetNSpectraUsed() <= 0)
		NamesValid.erase(it);

	FillListValid(Ind);
}

void CCreateModelDlg::OnPlotValid()
{
	VPSam SamH;

	ItModSam it;
	for(it=NamesValid.begin(); it!=NamesValid.end(); ++it)
	{
		CTransferData* pTr = ParentPrj->GetTransfer(it->TransName);
		if(!pTr)
			continue;
		CSampleData *Sam = pTr->GetSample(it->SampleName);
		if(Sam != NULL)
			SamH.push_back(Sam);
	}

	CSamplesHistoDlg dlg(&SamH, &Params, ParentPrj, C_SAMPLESH_VALID, this);
	dlg.DoModal();
}

void CCreateModelDlg::OnSamplesListsColumnClick(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NMLISTVIEW *pLV = (NMLISTVIEW *) pNMHDR;

	int item = pLV->iItem;

	bool findsort = false;
	if(item == 0)
	{
		if(SortName.IsEmpty())
			SortDir = !SortDir;
		SortName = cEmpty;
		findsort = true;
	}
	else if(item == m_nListTimeCol)
	{
		if(SortName == cSortTime)
		{
			SortDir = !SortDir;
		}
		SortName = cSortTime;
		findsort = true;
	}
	else if(item >= m_nListParStart)
	{
		int ipar = item - m_nListParStart;
		if(ipar < int(Params.size()))
		{
			CString par = Params[ipar].ParamName;
			if(!SortName.Compare(par))
				SortDir = !SortDir;
			SortName = par;
			findsort = true;
		}
	}

	if(findsort)
	{
		GetSamSelAll();
		SortNamesSmpl();
		FillListSmpl(0, false);

		GetSamSelClb();
		SortNamesCalib();
		FillListCalib(0, false);

		GetSamSelVal();
		SortNamesValid();
		FillListValid(0, false);
	}

	*pResult = 0;
}

void CCreateModelDlg::OnOptim()
{
	if(!AcceptData())
		return;

	if(ModData->IsExistCalibValid())
	{
		if(!ParcelConfirm(ParcelLoadString(STRID_CREATE_MODEL_SMP_UNECALIBVALID), false))
			return;
	}

	bool chk1 = (int(Params.size()) == 1);
	if(!chk1)
	{
		ParcelError(ParcelLoadString(STRID_CREATE_MODEL_DLG_ERROR_OPT_2PAR));
		return;
	}

	Template* Tmpl = NULL;
	if(m_vUseTemplate)
	{
		Tmpl = GetOptimTemplates()->GetTemplate(m_vTemplate);
		if(Tmpl == NULL)
			return;

		bool chk2 = (int(NamesValid.size()) > 0);
		bool chk4 = Tmpl->IsUseSEV() && Tmpl->CheckPrjSettings(ParentPrj);
		bool chk5 = (!chk2 && chk4);
		bool chk6 = (chk1 && !chk5);
		if(!chk6)
		{
			ParcelError(ParcelLoadString(STRID_CREATE_MODEL_DLG_ERROR_OPT_SEV));
			return;
		}
	}

	if(!ModData->LoadSpectraData(true, true))
		return;

	int mode = (m_vUseTemplate) ? C_OPT_MODE_BYTMPL : C_OPT_MODE_SINGLE;

	COptimModelDlg2 OptDlg(ModData, Tmpl, mode);
	int res = (OptDlg.DoModal() == IDOK);

	if(res)
	{
		ModData->FreeSpectraData();
		OnAccept();
	}
}

void CCreateModelDlg::OnTemplate()
{
	if(!AcceptData())
		return;

	if(int(Params.size()) > 1)
	{
		ParcelError(ParcelLoadString(STRID_CREATE_MODEL_DLG_ERROR_OPT_2PAR));
		return;
	}

	CTemplateManagerDlg TmplDlg(ModData);
	TmplDlg.DoModal();

	FillTemplates();

	EnableFields();

	UpdateData(0);
}

void CCreateModelDlg::OnUseTemplate()
{
	m_vUseTemplate = m_UseTemplate.GetCheck();

	EnableFields();
}

void CCreateModelDlg::OnTemplateChange()
{
	m_vTemplate = m_Template.GetCurSel();

	EnableFields();
}

void CCreateModelDlg::OnAutoChoose()
{
	if(!UpdateData() &&	Params.size() <= 0)
		return;

	int N = int(NamesSmpl.size());
	int Nv = int(N * m_vProcValid / 100. + 0.5);
	int Nc = N - Nv;
	float rest = 0, proc = (N > 6 && Nv > 2) ? float(Nv - 2) / float(N - 6) : 0.f;

	NamesCalib.clear();
	NamesValid.clear();
	CString OldSort = SortName;
	SortName = Params[0].ParamName;
	VModSam SamCopy;
	copy(NamesSmpl.begin(), NamesSmpl.end(), inserter(SamCopy, SamCopy.begin()));
	sort(SamCopy.begin(), SamCopy.end(), PSortModSamUp);

	int i;
	for(i=0; i<N; i++)
	{
		bool isV = false;
		SamCopy[i].SetAll(true);
		if(i == 0)  isV = (Nc < 1);
		else if(i == N - 2)  isV = (Nc < 2);
		else if(i == 1)  isV = (Nc < 3);
		else if(i == N - 1)  isV = (Nc < 4);
		else if(i == 2)  isV = (Nv > 0);
		else if(i == N - 3)  isV = (Nv > 1);
		else
		{
			isV = (int(rest) < int(rest + proc));
			rest += proc;
		}

		if(isV)  NamesValid.push_back(SamCopy[i]);
		else  NamesCalib.push_back(SamCopy[i]);
	}
	SamCopy.clear();
	SortName = OldSort;

	SortNamesCalib();
	SortNamesValid();
	FillListCalib(0);
	FillListValid(0);
	RefillParamCList();

	EnableFields();
}

void CCreateModelDlg::OnShowSpectra()
{
	CTransferData* pTrans1 = ParentPrj->GetTransOwn();
	if(pTrans1 == NULL)
		return;

	VLSI PSam;
	ItModSam it;
	for(it=NamesSmpl.begin(); it!=NamesSmpl.end(); ++it)
	{
		if(it->IsTransN())
			continue;

		CTransferData* pTrans2 = ParentPrj->GetTransfer(it->TransName);
		if(pTrans2 == NULL)
			continue;

		CSampleData* Sam = pTrans2->GetSample(it->SampleName);
		if(Sam == NULL)
			return;

		LoadSampleInfo NewLSI;
		NewLSI.pSam = Sam;

		ItIntBool it4;
		for(it4=it->Spectra.begin(); it4!=it->Spectra.end(); ++it4)
			NewLSI.Nums.push_back(it4->first);

		PSam.push_back(NewLSI);
	}

	CLoadSpectraPBDlg dlg2(&PSam, this);
	if(dlg2.DoModal() != IDOK)
	{
		for(it=NamesSmpl.begin(); it!=NamesSmpl.end(); ++it)
		{
			CSampleData* Sam = pTrans1->GetSample(it->SampleName);
			if(Sam == NULL)
				return;

			Sam->FreeSpectraData();
		}

		return;
	}

	CShowSpectra ShowSp(&pTrans1->SpecPoints);

	int i;
	for(i=0, it=NamesSmpl.begin(); it!=NamesSmpl.end(); ++it, i++)
	{
		if(it->IsTransN())
			continue;

		CTransferData* pTrans2 = ParentPrj->GetTransfer(it->TransName);
		int shift = ParentPrj->GetSpecPointInd(pTrans2->SpecPoints[0]);

		CSampleData* Sam = pTrans2->GetSample(it->SampleName);
		if(Sam == NULL)
			return;

		bool isCalib = false, isValid = false;
		ItModSam it2;
		for(it2=NamesCalib.begin(); it2!=NamesCalib.end(); ++it2)
			if(!it->SampleName.Compare(it2->SampleName) && !it->TransName.Compare(it2->TransName))
			{
				isCalib = true;
				break;
			}
		for(it2=NamesValid.begin(); it2!=NamesValid.end(); ++it2)
			if(!it->SampleName.Compare(it2->SampleName) && !it->TransName.Compare(it2->TransName))
			{
				isValid = true;
				break;
			}

		int indT = GetTransNum(&(*it));

		int type = C_SPECTRA_TYPE_SIMPLE;
		if(isCalib && isValid)  type = C_SPECTRA_TYPE_MODEL;
		if(isCalib && !isValid)  type = C_SPECTRA_TYPE_CALIB;
		if(!isCalib && isValid)  type = C_SPECTRA_TYPE_VALID;
		if(!isCalib && !isValid)  type = C_SPECTRA_TYPE_SIMPLE;

		Sam->LoadSpectraData();

		bool issel = (m_listSmpl.GetItemState(i, LVIS_SELECTED) == LVIS_SELECTED);

		ItSpc it3;
		for(it3=Sam->SpecData.begin(); it3!=Sam->SpecData.end(); ++it3)
			if(it3->bUse)
			{
				SpectraShowSettings* NewSS = ShowSp.AddSpectraShow(it->SampleName, it3->Num, indT + 1, shift, issel, type);
				if(NewSS == NULL)
					continue;

				copy(it3->Data.begin(), it3->Data.end(), inserter(NewSS->SpecData, NewSS->SpecData.begin()));
			}
	}

	CShowSpectraDlg dlg(&ShowSp, C_PLOT_SET_MODE_MODEL, this);
	dlg.DoModal();

	for(it=NamesSmpl.begin(); it!=NamesSmpl.end(); ++it)
	{
		CSampleData* Sam = pTrans1->GetSample(it->SampleName);
		if(Sam == NULL)
			return;

		Sam->FreeSpectraData();
	}
}

void CCreateModelDlg::OnChangeLowSpecRange()
{
	if(iscancel)
		return;
	if(GetFocus() == GetDlgItem(IDCANCEL))
	{
		OnCancel();
		return;
	}

	int old = m_vLowSpec;

	if(!UpdateData())
		return;

	if(m_vLowSpec == old)
		return;

	if(m_vUpperSpec < m_vLowSpec)
	{
		ParcelError(ParcelLoadString(STRID_CREATE_MODEL_DLG_ERROR_SPECRANGE));
		m_vUpperSpec = m_vLowSpec;
	}

	RecreateSpecPoints();

	UpdateData(0);
}

void CCreateModelDlg::OnChangeUpperSpecRange()
{
	if(iscancel)
		return;
	if(GetFocus() == GetDlgItem(IDCANCEL))
	{
		OnCancel();
		return;
	}

	int old = m_vUpperSpec;

	if(!UpdateData())
		return;

	if(m_vUpperSpec == old)
		return;

	if(m_vUpperSpec < m_vLowSpec)
	{
		ParcelError(ParcelLoadString(STRID_CREATE_MODEL_DLG_ERROR_SPECRANGE));
		m_vLowSpec = m_vUpperSpec;
	}

	RecreateSpecPoints();

	UpdateData(0);
}

void CCreateModelDlg::OnMaxMSDCalibChange()
{
	if(iscancel)
		return;
	if(GetFocus() == GetDlgItem(IDCANCEL))
	{
		OnCancel();
		return;
	}

	ChangeCalibSettings();
}

void CCreateModelDlg::OnLowLimCalibChange()
{
	if(iscancel)
		return;
	if(GetFocus() == GetDlgItem(IDCANCEL))
	{
		OnCancel();
		return;
	}

	ChangeCalibSettings();
}

void CCreateModelDlg::OnUpperLimCalibChange()
{
	if(iscancel)
		return;
	if(GetFocus() == GetDlgItem(IDCANCEL))
	{
		OnCancel();
		return;
	}

	ChangeCalibSettings();
}

void CCreateModelDlg::OnCorrCalibBChange()
{
	if(iscancel)
		return;
	if(GetFocus() == GetDlgItem(IDCANCEL))
	{
		OnCancel();
		return;
	}

	ChangeCalibSettings();
}

void CCreateModelDlg::OnCorrCalibAChange()
{
	if(iscancel)
		return;
	if(GetFocus() == GetDlgItem(IDCANCEL))
	{
		OnCancel();
		return;
	}

	ChangeCalibSettings();
}

void CCreateModelDlg::OnPreproc0()
{
	int sel = m_Preproc0.GetCurSel();
	OnPreproc(0, sel);
}

void CCreateModelDlg::OnPreproc1()
{
	int sel = m_Preproc1.GetCurSel();
	OnPreproc(1, sel);
}

void CCreateModelDlg::OnPreproc2()
{
	int sel = m_Preproc2.GetCurSel();
	OnPreproc(2, sel);
}

void CCreateModelDlg::OnPreproc3()
{
	int sel = m_Preproc3.GetCurSel();
	OnPreproc(3, sel);
}

void CCreateModelDlg::OnPreproc4()
{
	int sel = m_Preproc4.GetCurSel();
	OnPreproc(4, sel);
}

void CCreateModelDlg::OnPreproc5()
{
	int sel = m_Preproc5.GetCurSel();
	OnPreproc(5, sel);
}

void CCreateModelDlg::OnPreproc6()
{
	int sel = m_Preproc6.GetCurSel();
	OnPreproc(6, sel);
}

void CCreateModelDlg::ChangeCalibSettings()
{
	if(CurParam == Params.end())
		return;

	if(!UpdateData())
		return;

	if(m_vLowCalib > m_vUpperCalib)
	{
		ParcelError(ParcelLoadString(STRID_CREATE_MODEL_DLG_ERROR_REFRANGE));
		return;
	}

	CurParam->LimMSD = m_vMSD;
	CurParam->LowValue = m_vLowCalib;
	CurParam->UpperValue = m_vUpperCalib;
	CurParam->CorrB = m_vCorrB;
	CurParam->CorrA = m_vCorrA;

	int i, n = int(CurParam - Params.begin());
	if(n < 0 || n >= int(Params.size()))
		return;

	CString s, fmt = ParentPrj->GetParam(CurParam->ParamName)->GetFormat();

	s.Format(cFmt53, CurParam->LimMSD);
	m_listParamC.SetItemText(n, 1, s);
	s.Format(fmt, CurParam->LowValue);
	m_listParamC.SetItemText(n, 2, s);
	s.Format(fmt, CurParam->UpperValue);
	m_listParamC.SetItemText(n, 3, s);
	s.Format(cFmt64, CurParam->CorrA);
	m_listParamC.SetItemText(n, 4, s);
	s.Format(cFmt64, CurParam->CorrB);
	m_listParamC.SetItemText(n, 5, s);

	for(i=0; i<6; i++)
		m_listParamC.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
}

void CCreateModelDlg::OnParamsCListSelChange(NMHDR* /*pNMHDR*/, LRESULT *pResult)
{
	int sel = m_listParamC.GetNextItem(-1, LVNI_SELECTED);
	if(sel < 0 || sel >= int(Params.size()))
		return;

	CurParam = Params.begin() + sel;

	FillParamCFields();

	*pResult = 0;
}

void CCreateModelDlg::OnModelTypeChange()
{
	if(!UpdateData())
		return;

	m_vModType = m_ModType.GetCurSel();
	FillHSOFields();

	UpdateData(0);
	EnableFields();
}

void CCreateModelDlg::OnModelHSOTypeChange()
{
	m_vHSOType = m_HSOType.GetCurSel();
	FillHSOFields();

	UpdateData(0);
	EnableFields();
}

void CCreateModelDlg::OnAddExc()
{
	if(!UpdateData())
		return;

	int Ind = -1, nsel = m_listAll.GetSelectedCount();
	if(nsel <= 0)  return;

	int shift = ParentPrj->GetSpecPointInd(PointsAll[0]);

	for(int i=0; i<nsel; i++)
	{
		Ind = m_listAll.GetNextItem(Ind, LVNI_SELECTED);
		if(Ind < 0 || Ind >= NAllPoints)
			return;

		PointsExc.insert(Ind + shift);
	}

	FillListExc(0);

	EnableFields();
}

void CCreateModelDlg::OnAddAllExc()
{
	if(!UpdateData())
		return;

	int shift = ParentPrj->GetSpecPointInd(PointsAll[0]);

	for(int i=0; i<NAllPoints; i++)
		PointsExc.insert(i + shift);

	FillListExc(0);

	EnableFields();
}

void CCreateModelDlg::OnRemoveExc()
{
	if(!UpdateData())
		return;

	int i, j, Ind = -1, nsel = m_listExc.GetSelectedCount();
	if(nsel <= 0)  return;

	int indS = ParentPrj->GetSpecPointInd(PointsAll[0]);
	int indF = ParentPrj->GetSpecPointInd(PointsAll[NAllPoints-1]);

	int N = int(PointsExc.size());
	VInt sel, mean;
	for(i=0; i<nsel; i++)
	{
		Ind = m_listExc.GetNextItem(Ind, LVNI_SELECTED);
		if(Ind < 0 || Ind >= N)
			return;

		sel.push_back(Ind);
	}

	ItSInt it;
	for(i=0, j=0, it=PointsExc.begin(); j<nsel && it!=PointsExc.end(); ++it)
	{
		if((*it) < indS)  continue;
		if((*it) > indF)  break;

		if(sel[j] == i)
		{
			mean.push_back(*it);
			j++;
		}
		i++;
	}

	ItInt it2;
	for(it2=mean.begin(); it2<mean.end(); ++it2)
		PointsExc.erase(*it2);

	FillListExc(0);

	EnableFields();
}

void CCreateModelDlg::OnRemoveAllExc()
{
	if(!UpdateData())
		return;

	int indS = ParentPrj->GetSpecPointInd(PointsAll[0]);
	int indF = ParentPrj->GetSpecPointInd(PointsAll[NAllPoints-1]);

	VInt mean;
	ItSInt it;
	for(it=PointsExc.begin(); it!=PointsExc.end(); ++it)
	{
		if((*it) < indS)  continue;
		if((*it) > indF)  break;

		mean.push_back(*it);
	}

	ItInt it2;
	for(it2=mean.begin(); it2<mean.end(); ++it2)
		PointsExc.erase(*it2);

	FillListExc(-1);

	EnableFields();
}

void CCreateModelDlg::OnCalculate()
{
	if(!AcceptData())
		return;

	if(ModData->IsExistCalibValid())
	{
		if(!ParcelConfirm(ParcelLoadString(STRID_CREATE_MODEL_SMP_UNECALIBVALID), false))
			return;
	}
	if(ModData->CalculateModel())
	{
		int res;
		if(ModData->GetAnalysisType() == C_ANALYSIS_QNT)
		{
			CModelResultDlg ModResDlg(ModData, C_MODRESDLG_MODE_EDIT);
			res = (ModResDlg.DoModal() == IDOK);
		}
		else
		{
			CModelQltResultDlg ModQltResDlg(ModData, C_MODQLTRESDLG_MODE_EDIT);
			res = (ModQltResDlg.DoModal() == IDOK);
		}

		if(res)
			OnAccept();
	}
	else
		ModData->FreeSpectraData();
}

void CCreateModelDlg::FillListParamSel(int newsel)
{
	m_listParamSel.ResetContent();
	ItModPar it;
	for(it=Params.begin(); it!=Params.end(); ++it)
		m_listParamSel.AddString((*it).ParamName);

	if(Params.size() == 0)
		CurParam = Params.end();
	else
	{
		int sel = (newsel >= 0 && Params.size() > 0) ? newsel : 0;
		m_listParamSel.SetSel(sel);
		CurParam = Params.begin() + sel;
	}

	m_listParamAll.RedrawWindow();
}

void CCreateModelDlg::GetTransValList()
{
	TransesAll.clear();
	int i;
	for(i=0; i<ParentPrj->GetNTransfers(); i++)
	{
		CTransferData *pTr = ParentPrj->GetTransfer(i);
		if(pTr != NULL && pTr->GetType() != C_TRANS_TYPE_OWN)
			TransesAll.push_back(pTr->GetName());
	}

	VStr Tmp;
	ItStr itT;
	for(itT=TransesSel.begin(); itT!=TransesSel.end(); ++itT)
	{
		if(find(TransesAll.begin(), TransesAll.end(), (*itT)) != TransesAll.end())
			Tmp.push_back(*itT);
	}
	if(pTrans->GetName().Compare(cOwn) && find(Tmp.begin(), Tmp.end(), pTrans->GetName()) == Tmp.end())
		Tmp.insert(Tmp.begin(), pTrans->GetName());

	TransesSel.clear();
	copy(Tmp.begin(), Tmp.end(), inserter(TransesSel, TransesSel.begin()));
}

CString CCreateModelDlg::GetSampleTransName(ModelSample *pMSam)
{
	CString s = pMSam->SampleName, fmt = cEmpty;

	int i, N = int(TransesSel.size());
	for(i=0; i<N; i++)
	{
		fmt += '*';
		if(!pMSam->TransName.Compare(TransesSel[i]))
		{
			fmt += _T("%s");
			break;
		}
	}
	if(i < N)
		s.Format(fmt, pMSam->SampleName);

	return s;
}

void CCreateModelDlg::FillListTransAll(int newsel)
{
	m_listTransAll.DeleteAllItems();
	CString s;
	int i, N = int(TransesAll.size());
	for(i=0; i<N; i++)
	{
		CTransferData* pTrans1 = ParentPrj->GetTransfer(TransesAll[i]);
		m_listTransAll.InsertItem(i, TransesAll[i]);
		s = (pTrans1->GetType() == C_TRANS_TYPE_NORMAL) ? ParcelLoadString(STRID_CREATE_MODEL_PRM_WITHCORR)
			: ParcelLoadString(STRID_CREATE_MODEL_PRM_WITHOUTCORR);
		m_listTransAll.SetItemText(i, 1, s);
		s = pTrans1->GetAllPreprocAsStr();
		m_listTransAll.SetItemText(i, 2, s);
		s.Format(_T("%1d - %1d"), pTrans1->GetLowLimSpec(), pTrans1->GetUpperLimSpec());
		m_listTransAll.SetItemText(i, 3, s);
	}
	
	int sel = (newsel >= 0 && newsel < int(TransesAll.size())) ? newsel : 0;
	m_listTransAll.SetItemState(sel, LVIS_SELECTED, LVIS_SELECTED);
}

void CCreateModelDlg::FillListTransSel(int newsel)
{
	m_listTransSel.ResetContent();
	ItStr it;
	for(it=TransesSel.begin(); it!=TransesSel.end(); ++it)
		m_listTransSel.AddString(*it);

	int sel = (newsel >= 0 && newsel < int(TransesSel.size())) ? newsel : 0;
	m_listTransSel.SetSel(sel);

	m_listTransAll.RedrawWindow();
}

void CCreateModelDlg::SetBasicTrans(CString Name)
{
	pTrans = ParentPrj->GetTransfer(Name);

	if(pTrans->GetType() == C_TRANS_TYPE_NORMAL)
		m_vSpType = C_MOD_SPECTRUM_ABS;

	FillListTransSel(0);
	RecreateSmplLists();
	RecreateSpecPoints();
}

void CCreateModelDlg::RecreateSmplLists(bool resetpar)
{
	int i;
	CString s;
	m_nListTimeCol = 2;
	m_nListParStart = 3;

	if(resetpar)
	{
		int NP = int(Params.size());
		bool find = false;
		ItModPar itP;
		for(itP=Params.begin(); itP!=Params.end(); ++itP)
		{
			CString ParName = itP->ParamName;
			if(!ParName.Compare(SortName))
				find = true;
		}
		if(!find)
			SortName = cEmpty;

		m_listSmpl.DeleteAllItems();
		m_listCalib.DeleteAllItems();
		m_listValid.DeleteAllItems();

		int nColumnCount = m_listSmpl.GetHeaderCtrl()->GetItemCount();
		if(nColumnCount < m_nListParStart)
		{
			s = ParcelLoadString(STRID_CREATE_MODEL_SMP_COLSAM);
			m_listSmpl.InsertColumn(0, s, LVCFMT_LEFT, 0, 0);
			m_listCalib.InsertColumn(0, s, LVCFMT_LEFT, 0, 0);
			m_listValid.InsertColumn(0, s, LVCFMT_LEFT, 0, 0);

			s = ParcelLoadString(STRID_CREATE_MODEL_SMP_COLNSPEC);
			m_listSmpl.InsertColumn(1, s, LVCFMT_RIGHT, 0, 1);
			s = ParcelLoadString(STRID_CREATE_MODEL_SMP_COLNSPECOF);
			m_listCalib.InsertColumn(1, s, LVCFMT_RIGHT, 0, 1);
			m_listValid.InsertColumn(1, s, LVCFMT_RIGHT, 0, 1);

			s = ParcelLoadString(STRID_MEAS_DATE);
			m_listSmpl.InsertColumn(m_nListTimeCol, s, LVCFMT_LEFT, 0, 0);
			m_listCalib.InsertColumn(m_nListTimeCol, s, LVCFMT_LEFT, 0, 0);
			m_listValid.InsertColumn(m_nListTimeCol, s, LVCFMT_LEFT, 0, 0);
		}
		for(i=m_nListParStart; i<nColumnCount; i++)
		{
			m_listSmpl.DeleteColumn(m_nListParStart);
			m_listCalib.DeleteColumn(m_nListParStart);
			m_listValid.DeleteColumn(m_nListParStart);
		}


		for(itP=Params.begin(); itP!=Params.end(); ++itP)
		{
			s = itP->ParamName;
			m_listSmpl.InsertColumn(i + m_nListParStart, s, LVCFMT_RIGHT, 0, i + 2);
			m_listCalib.InsertColumn(i + m_nListParStart, s, LVCFMT_RIGHT, 0, i + 2);
			m_listValid.InsertColumn(i + m_nListParStart, s, LVCFMT_RIGHT, 0, i + 2);
		}
		m_listSmpl.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);
		m_listCalib.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);
		m_listValid.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

		for(i=0; i<NP+m_nListParStart; i++)
			m_listSmpl.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
		for(i=0; i<NP+m_nListParStart; i++)
			m_listCalib.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
		for(i=0; i<NP+m_nListParStart; i++)
			m_listValid.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	}

	CreateSmplAll();
	RecreateSmplCalibValid();

	FillListSmpl(0);
	FillListCalib(0);
	FillListValid(0);

	RefillParamCList(resetpar);
}

void CCreateModelDlg::CreateSmplAll()
{
	int i;

	if(m_vAnType == C_ANALYSIS_QNT && int(Params.size()) == 0)
	{
		NamesSmpl.clear();
		NamesCalib.clear();
		NamesValid.clear();
		return;
	}

	NamesSmpl.clear();
	for(i=0; i<ParentPrj->GetNSamples(); i++)
	{
		CSampleData* Sam = ParentPrj->GetSample(i);
		if(IsSampleAvailable(Sam))
		{
			ModelSample MSam(Sam);
			NamesSmpl.push_back(MSam);
		}
	}
	ItStr itT;
	for(itT=TransesSel.begin(); itT!=TransesSel.end(); ++itT)
	{
		CTransferData *pTr = ParentPrj->GetTransfer(*itT);
		for(i=0; i<pTr->GetNSamples(); i++)
		{
			CSampleData* Sam = pTr->GetSample(i);
			if(IsSampleAvailable(Sam))
			{
				ModelSample MSam(Sam);
				NamesSmpl.push_back(MSam);
			}
		}
	}
}

void CCreateModelDlg::RecreateSmplCalibValid()
{
	VModSam NamesCopy;
	ItModSam it;
	CString Name;

	for(it=NamesCalib.begin(); it!=NamesCalib.end(); ++it)
	{
		Name = it->TransName;
		CTransferData* pTr = ParentPrj->GetTransfer(Name);
		if(pTr != NULL && IsSampleAvailable(pTr->GetSample(it->SampleName)))
		{
			if(Name.Compare(cOwn) && find(TransesSel.begin(), TransesSel.end(), Name) == TransesSel.end())
				continue;
			NamesCopy.push_back(*(it));
		}
	}
	NamesCalib.clear();
	copy(NamesCopy.begin(), NamesCopy.end(), inserter(NamesCalib, NamesCalib.begin()));

	NamesCopy.clear();
	for(it=NamesValid.begin(); it!=NamesValid.end(); ++it)
	{
		Name = it->TransName;
		CTransferData* pTr = ParentPrj->GetTransfer(Name);
		if(pTr != NULL && IsSampleAvailable(pTr->GetSample(it->SampleName)))
		{
			if(Name.Compare(cOwn) && find(TransesSel.begin(), TransesSel.end(), Name) == TransesSel.end())
				continue;
			NamesCopy.push_back(*(it));
		}
	}
	NamesValid.clear();
	copy(NamesCopy.begin(), NamesCopy.end(), inserter(NamesValid, NamesValid.begin()));
}

void CCreateModelDlg::FillListSmpl(int newsel, bool bFromScratch)
{
	int i, j;
	CString s;

	SortNamesSmpl();

	int Na = int(NamesSmpl.size());

	if(bFromScratch) m_listSmpl.DeleteAllItems();
	if(Na > 0)
	{
		CString s;
		ItModSam itA;
		for(i=0, itA=NamesSmpl.begin(); itA!=NamesSmpl.end(); ++itA, i++)
		{
			CTransferData* pTr = ParentPrj->GetTransfer(itA->TransName);
			CSampleData *Sam = pTr->GetSample(itA->SampleName);
			s = GetSampleTransName(&(*itA));

			if(bFromScratch)
			{
				m_listSmpl.InsertItem(i, s);
			}
			else
			{
				m_listSmpl.SetItemText(i, 0, s);
	
				m_listSmpl.SetItemState(i, 0, LVIS_SELECTED);
				int ns = SamSelAll.size();
				for(int k=0; k<ns; k++)
				{
					if(s == SamSelAll[k])
					{
						m_listSmpl.SetItemState(i, LVIS_SELECTED, LVIS_SELECTED);
						break;
					}
				}
			}
			s.Format(cFmt1, Sam->GetNSpectraUsed());
			m_listSmpl.SetItemText(i, 1, s);

			CTime t = Sam->GetLastSpecDate();
			s = t.Format(cFmtDate2);
			m_listSmpl.SetItemText(i, m_nListTimeCol, s);

			ItModPar itP;
			for(j=0, itP=Params.begin(); itP!=Params.end(); ++itP, j++)
			{
				s = Sam->GetReferenceDataStr(itP->ParamName);
				m_listSmpl.SetItemText(i, j + m_nListParStart, s);
			}
		}

		if(bFromScratch)
		{
			int sel = (newsel >= 0 && Na > 0) ? newsel : 0;
			m_listSmpl.SetItemState(sel, LVIS_SELECTED, LVIS_SELECTED);
		}
	}

	SmplSetSortIcons();
	int NP = int(Params.size());
	for(i=0; i<NP+m_nListParStart; i++)
	{
		m_listSmpl.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	}

	s.Format(ParcelLoadString(STRID_CREATE_MODEL_SMP_ALLSAMPLES), int(NamesSmpl.size()), GetNUsesSpectrumAll());
	GetDlgItem(IDC_STATIC_7)->SetWindowText(s);
}

void CCreateModelDlg::FillListCalib(int newsel, bool bFromScratch)
{
	SortNamesCalib();

	int Na = int(NamesCalib.size());

	if(bFromScratch) m_listCalib.DeleteAllItems();
	int i, j;
	CString s;
	ItModSam it;
	for(i=0, it=NamesCalib.begin(); it!=NamesCalib.end(); ++it, i++)
	{
		CTransferData* pTr = ParentPrj->GetTransfer(it->TransName);
		CSampleData *Sam = pTr->GetSample(it->SampleName);
		s = GetSampleTransName(&(*it));

		if(bFromScratch)
		{
			m_listCalib.InsertItem(i, s);
		}
		else
		{
			m_listCalib.SetItemText(i, 0, s);

			m_listCalib.SetItemState(i, 0, LVIS_SELECTED);
			int ns = SamSelClb.size();
			for(int k=0; k<ns; k++)
			{
				if(s == SamSelClb[k])
				{
					m_listCalib.SetItemState(i, LVIS_SELECTED, LVIS_SELECTED);
					break;
				}
			}
		}

		s.Format(cFmt1_1, it->GetNSpectraUsed(), it->GetNSpectra());
		m_listCalib.SetItemText(i, 1, s);

		CTime t = Sam->GetLastSpecDate();
		s = t.Format(cFmtDate2);
		m_listCalib.SetItemText(i, m_nListTimeCol, s);

		ItModPar itP;
		for(j=0, itP=Params.begin(); itP!=Params.end(); ++itP, j++)
		{
			s = Sam->GetReferenceDataStr(itP->ParamName);
			m_listCalib.SetItemText(i, j + m_nListParStart, s);
		}
	}

	CalibSetSortIcons();
	int NP = int(Params.size());
	for(i=0; i<NP+m_nListParStart; i++)
	{
		m_listCalib.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	}

	if(Na > 0)
	{
		if(bFromScratch)
		{
			int sel = (newsel >= 0 && Na > 0) ? newsel : 0;
			m_listCalib.SetItemState(sel, LVIS_SELECTED, LVIS_SELECTED);
		}
	}

	s.Format(ParcelLoadString(STRID_CREATE_MODEL_SMP_CALIBSAMPLES), int(Na), GetNUsesSpectrumCalib());
	GetDlgItem(IDC_STATIC_8)->SetWindowText(s);

	m_listSmpl.RedrawWindow();
}

void CCreateModelDlg::FillListValid(int newsel, bool bFromScratch)
{
	SortNamesValid();

	int Na = int(NamesValid.size());

	if(bFromScratch) m_listValid.DeleteAllItems();
	int i, j;
	CString s;
	ItModSam it;
	for(i=0, it=NamesValid.begin(); it!=NamesValid.end(); ++it, i++)
	{
		CTransferData* pTr = ParentPrj->GetTransfer(it->TransName);
		CSampleData *Sam = pTr->GetSample(it->SampleName);
		s = GetSampleTransName(&(*it));

		if(bFromScratch)
		{
			m_listValid.InsertItem(i, s);
		}
		else
		{
			m_listValid.SetItemText(i, 0, s);

			m_listValid.SetItemState(i, 0, LVIS_SELECTED);
			int ns = SamSelVal.size();
			for(int k=0; k<ns; k++)
			{
				if(s == SamSelVal[k])
				{
					m_listValid.SetItemState(i, LVIS_SELECTED, LVIS_SELECTED);
					break;
				}
			}
		}

		s.Format(cFmt1_1, it->GetNSpectraUsed(), it->GetNSpectra());
		m_listValid.SetItemText(i, 1, s);

		CTime t = Sam->GetLastSpecDate();
		s = t.Format(cFmtDate2);
		m_listValid.SetItemText(i, m_nListTimeCol, s);

		ItModPar itP;
		for(j=0, itP=Params.begin(); itP!=Params.end(); ++itP, j++)
		{
			s = Sam->GetReferenceDataStr(itP->ParamName);
			m_listValid.SetItemText(i, j + m_nListParStart, s);
		}
	}

	ValidSetSortIcons();
	int NP = int(Params.size());
	for(i=0; i<NP+m_nListParStart; i++)
	{
		m_listValid.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	}

	if(Na > 0)
	{
		if(bFromScratch)
		{
			int sel = (newsel >= 0 && Na > 0) ? newsel : 0;
			m_listValid.SetItemState(sel, LVIS_SELECTED, LVIS_SELECTED);
		}
	}

	s.Format(ParcelLoadString(STRID_CREATE_MODEL_SMP_VALIDSAMPLES), int(Na), GetNUsesSpectrumValid());
	GetDlgItem(IDC_STATIC_9)->SetWindowText(s);

	m_listSmpl.RedrawWindow();
}

void CCreateModelDlg::FillTemplates()
{
	m_Template.ResetContent();

	COptimTemplate* Templates = GetOptimTemplates();
	int i, N = Templates->GetNTemplates();
	CString s;

	if(N > 0)
	{
		for(i=0; i<N; i++)
		{
			Template* pTmpl = Templates->GetTemplate(i);
			if(pTmpl == NULL)
				break;

			m_Template.AddString(pTmpl->Name);
		}
	}
	else
	{
		m_Template.AddString(ParcelLoadString(STRID_CREATE_MODEL_SMP_NO));
		m_vUseTemplate = false;
		m_UseTemplate.SetCheck(false);
	}
	m_Template.SetCurSel(0);

}

void CCreateModelDlg::SmplSetSortIcons()
{
	int iCol = -1;
	bool dir = SortDir;

	if(int(NamesSmpl.size()) > 0)
	{
		if(SortName.IsEmpty())
		{
			iCol = 0;
		}
		else if(SortName == cSortTime)
		{
			iCol = m_nListTimeCol;
		}
		else
		{
			ItModPar it;
			int i;
			for(i=0, it=Params.begin(); it!=Params.end(); ++it, i++)
			{
				if(!SortName.Compare(it->ParamName))
				{
					iCol = m_nListParStart + i;
					break;
				}
			}
		}
	}

	m_listSmpl.ShowSortIcons(iCol, dir);
}

void CCreateModelDlg::CalibSetSortIcons()
{
	int iCol = -1;
	bool dir = SortDir;

	if(int(NamesCalib.size()) > 0)
	{
		if(SortName.IsEmpty())
		{
			iCol = 0;
		}
		else if(SortName == cSortTime)
		{
			iCol = m_nListTimeCol;
		}
		else
		{
			ItModPar it;
			int i;
			for(i=0, it=Params.begin(); it!=Params.end(); ++it, i++)
			{
				if(!SortName.Compare(it->ParamName))
				{
					iCol = m_nListParStart + i;
					break;
				}
			}
		}
	}

	m_listCalib.ShowSortIcons(iCol, dir);
}

void CCreateModelDlg::ValidSetSortIcons()
{
	int iCol = -1;
	bool dir = SortDir;

	if(int(NamesValid.size()) > 0)
	{
		if(SortName.IsEmpty())
		{
			iCol = 0;
		}
		else if(SortName == cSortTime)
		{
			iCol = m_nListTimeCol;
		}
		else
		{
			ItModPar it;
			int i;
			for(i=0, it=Params.begin(); it!=Params.end(); ++it, i++)
			{
				if(!SortName.Compare(it->ParamName))
				{
					iCol = m_nListParStart + i;
					break;
				}
			}
		}
	}

	m_listValid.ShowSortIcons(iCol, dir);
}

void CCreateModelDlg::FillListsPreproc(int icb)
{
	int NTP = pTrans->GetNPreproc();

	CComboBox* pCb = NULL;
	switch(icb)
	{
	case 0:
		GetDlgItem(IDC_STR_PREP_1)->EnableWindow(NTP <= 0);
		GetDlgItem(IDC_PREPROC_1)->EnableWindow(NTP <= 0);
		FillListsPreproc(1);
		return;
	case 1:  pCb = &m_Preproc1;  break;
	case 2:  pCb = &m_Preproc2;  break;
	case 3:  pCb = &m_Preproc3;  break;
	case 4:  pCb = &m_Preproc4;  break;
	case 5:  pCb = &m_Preproc5;  break;
	case 6:  pCb = &m_Preproc6;  break;
	default:  return;
	}

	pCb->ResetContent();
	pCb->AddString(cGetPreprocName(C_PREPROC_0));

	if(m_vPreproc[icb-1] == C_PREPROC_0)
	{
		m_vPreproc[icb] = C_PREPROC_0;
		GetDlgItem(IDC_STR_PREP_1 + icb)->EnableWindow(false);
		GetDlgItem(IDC_PREPROC_1 + icb)->EnableWindow(false);
	}
	else
	{
		GetDlgItem(IDC_STR_PREP_1 + icb)->EnableWindow(NTP <= icb);
		GetDlgItem(IDC_PREPROC_1 + icb)->EnableWindow(NTP <= icb);
	}

	for(int i=C_PREPROC_M; i<=C_PREPROC_2; i++)
	{
		CString s = cGetPreprocName(i);
		int ind = FindPreproc(i);
		if(ind < icb)
			continue;
		if(i == C_PREPROC_D && icb > 0 && m_vPreproc[icb-1] == C_PREPROC_C)
			continue;
		if(i == C_PREPROC_C && icb > 0 && m_vPreproc[icb-1] == C_PREPROC_M)
			continue;
		if(i == C_PREPROC_C && icb > 1 && m_vPreproc[icb-2] == C_PREPROC_M && m_vPreproc[icb-1] == C_PREPROC_D)
			continue;
		if(i == C_PREPROC_1 && FindPreproc(C_PREPROC_2) < icb)
			continue;
		if(i == C_PREPROC_2 && FindPreproc(C_PREPROC_1) < icb)
			continue;

		pCb->AddString(s);
	}

	pCb->SetCurSel(GetSelByVal(icb, m_vPreproc[icb]));

	FillListsPreproc(icb+1);
}

void CCreateModelDlg::RefillParamCList(bool reset)
{
	int i;
	CString s;

	LimSpecRangeMin = pTrans->GetLowLimSpec();
	LimSpecRangeMax = pTrans->GetUpperLimSpec();

	if(m_vLowSpec < LimSpecRangeMin)  m_vLowSpec = LimSpecRangeMin;
	if(m_vLowSpec > LimSpecRangeMax)  m_vLowSpec = LimSpecRangeMax;
	if(m_vUpperSpec < LimSpecRangeMin)  m_vUpperSpec = LimSpecRangeMin;
	if(m_vUpperSpec > LimSpecRangeMax)  m_vUpperSpec = LimSpecRangeMax;

	m_listParamC.DeleteAllItems();
	ItModPar it;
	for(i=0, it=Params.begin(); it!=Params.end(); ++it, i++)
	{
		CString fmt = ParentPrj->GetParam(it->ParamName)->GetFormat();

		CurParam = it;
		CalcMinMaxCalibValue();

		if(!reset)
		{
			if(it->LowValue < MinValue)  it->LowValue = MinValue;
			if(it->LowValue > MaxValue)  it->LowValue = MaxValue;
			if(it->UpperValue < MinValue)  it->UpperValue = MinValue;
			if(it->UpperValue > MaxValue)  it->UpperValue = MaxValue;
		}
		else
		{
			it->LowValue = MinValue;
			it->UpperValue = MaxValue;
		}

		m_listParamC.InsertItem(i, it->ParamName);

		s.Format(cFmt53, it->LimMSD);
		m_listParamC.SetItemText(i, 1, s);
		s.Format(fmt, it->LowValue);
		m_listParamC.SetItemText(i, 2, s);
		s.Format(fmt, it->UpperValue);
		m_listParamC.SetItemText(i, 3, s);
		s.Format(cFmt64, it->CorrA);
		m_listParamC.SetItemText(i, 4, s);
		s.Format(cFmt64, it->CorrB);
		m_listParamC.SetItemText(i, 5, s);
	}
	for(i=0; i<6; i++)
		m_listParamC.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);

	if(int(Params.size() > 0))
	{
		m_listParamC.SetItemState(0, LVIS_SELECTED, LVIS_SELECTED);
		CurParam = Params.begin();
	}

	FillParamCFields();
}

void CCreateModelDlg::RecalcMaxComp()
{
	bool IsAvr = (m_vPreproc[0] == C_PREPROC_A);
	int n = (IsAvr) ? GetNSamplesForCalib() - 3 : GetNSpectraForCalib() - GetMaxSpectraInNamesCalib() - 2;
	NMaxComp = min(n, GetNUseSpecPoints());
}

void CCreateModelDlg::FillParamCFields()
{
	if(CurParam != Params.end())
	{
		m_vMSD = CurParam->LimMSD;
		m_vLowCalib = CurParam->LowValue;
		m_vUpperCalib = CurParam->UpperValue;
		m_vCorrA = CurParam->CorrA;
		m_vCorrB = CurParam->CorrB;

		CString s = ParentPrj->GetParam(CurParam->ParamName)->GetUnit();
		GetDlgItem(IDC_STATIC_27)->SetWindowText(s);
		GetDlgItem(IDC_STATIC_22)->SetWindowText(s);
		GetDlgItem(IDC_STATIC_26)->SetWindowText(s);
	}
	else
	{
		m_vMSD = m_vLowCalib = m_vUpperCalib = m_vCorrA = 0.;
		m_vCorrB = 1.;

		GetDlgItem(IDC_STATIC_27)->SetWindowText(cEmpty);
		GetDlgItem(IDC_STATIC_22)->SetWindowText(cEmpty);
		GetDlgItem(IDC_STATIC_26)->SetWindowText(cEmpty);
	}

	UpdateData(0);

	GetDlgItem(IDC_PRJPARAMS_LIST)->SetFocus();
}

void CCreateModelDlg::RecreateSpecPoints()
{
	GetSpecPoints();
	FillSpecAllList();
	FillListExc(0);

	UpdateData(0);
}

void CCreateModelDlg::GetSpecPoints()
{
	LimSpecRangeMin = pTrans->GetLowLimSpec();
	LimSpecRangeMax = pTrans->GetUpperLimSpec();

	if(m_vLowSpec < LimSpecRangeMin)  m_vLowSpec = LimSpecRangeMin;
	if(m_vLowSpec > LimSpecRangeMax)  m_vLowSpec = LimSpecRangeMax;
	if(m_vUpperSpec < LimSpecRangeMin)  m_vUpperSpec = LimSpecRangeMin;
	if(m_vUpperSpec > LimSpecRangeMax)  m_vUpperSpec = LimSpecRangeMax;

	ParentPrj->ExtractSpecPoints(m_vLowSpec, m_vUpperSpec, PointsAll);

	NAllPoints = int(PointsAll.size());
}

void CCreateModelDlg::FillSpecAllList()
{
	CString s;
	int i;

	m_listAll.DeleteAllItems();
	for(i=0; i<NAllPoints; i++)
	{
		int ind = ParentPrj->GetSpecPointInd(PointsAll[i]);
		s.Format(cFmt1, ind + 1);
		m_listAll.InsertItem(i, s);
		s.Format(cFmt86, PointsAll[i]);
		m_listAll.SetItemText(i, 1, s);
	}
	for(i=0; i<2; i++)
		m_listAll.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);

	if(NAllPoints > 0)
		m_listAll.SetItemState(0, LVIS_SELECTED, LVIS_SELECTED);

	s.Format(ParcelLoadString(STRID_CREATE_MODEL_PRE_ALLPOINTS), NAllPoints);
	GetDlgItem(IDC_STATIC_39)->SetWindowText(s);
}

void CCreateModelDlg::FillListExc(int newsel)
{
	int indS = ParentPrj->GetSpecPointInd(PointsAll[0]);
	int indF = ParentPrj->GetSpecPointInd(PointsAll[NAllPoints-1]);

	m_listExc.DeleteAllItems();
	ItSInt it;
	int i;
	for(i=0, it=PointsExc.begin(); it!=PointsExc.end(); ++it)
	{
		if((*it) < indS)  continue;
		if((*it) > indF)  break;

		CString s;

		s.Format(cFmt1, (*it) + 1);
		m_listExc.InsertItem(i, s);
		s.Format(cFmt86, PointsAll[(*it) - indS]);
		m_listExc.SetItemText(i, 1, s);

		i++;
	}
	for(i=0; i<2; i++)
		m_listExc.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);

	int N = GetNRealExcPoints();
	if(N > 0)
	{
		int isel = (newsel < 0 || newsel >= N) ? 0 : newsel;
		m_listExc.SetItemState(isel, LVIS_SELECTED, LVIS_SELECTED);
	}

	CString s;
	s.Format(ParcelLoadString(STRID_CREATE_MODEL_PRE_EXCPOINTS), N);
	GetDlgItem(IDC_STATIC_40)->SetWindowText(s);

	m_listAll.RedrawWindow();
}

void CCreateModelDlg::FillHSOFields()
{
	if(m_vModType != C_MOD_MODEL_HSO)
		return;

	FillListExc();

	if(m_vHSOType == C_MOD_MODEL_HSO_LPM || m_vHSOType == C_MOD_MODEL_HSO_NLPM)
		m_vNHarm = 0;
	else if(m_vNHarm == 0)
		m_vNHarm = 3;

	if(m_vHSOType == C_MOD_MODEL_HSO_LPM || m_vHSOType == C_MOD_MODEL_HSO_LHM)
		m_vKfnl = 0.;
	else if(m_vKfnl < 1.e-9)
		m_vKfnl = 0.1;
}

bool CCreateModelDlg::IsSampleAvailable(CSampleData* data)
{
	if(data == NULL || data->GetNSpectraUsed() == 0)
		return false;

	if(m_vAnType == C_ANALYSIS_QLT)
		return true;

	int i;
	ItModPar itP;
	for(i=0, itP=Params.begin(); itP!=Params.end(); ++itP, i++)
		if(data->GetReferenceData(itP->ParamName)->bNoValue)
			return false;

	return true;
}

int CCreateModelDlg::GetNUsesSpectrumAll()
{
	int nUses = 0;

	ItModSam it;
	for(it=NamesSmpl.begin(); it!=NamesSmpl.end(); ++it)
		nUses += it->GetNSpectraUsed();

	return nUses;
}

int CCreateModelDlg::GetNUsesSpectrumCalib()
{
	int nUses = 0;

	ItModSam it;
	for(it=NamesCalib.begin(); it!=NamesCalib.end(); ++it)
		nUses += it->GetNSpectraUsed();

	return nUses;
}

int CCreateModelDlg::GetNUsesSpectrumValid()
{
	int nUses = 0;

	ItModSam it;
	for(it=NamesValid.begin(); it!=NamesValid.end(); ++it)
		nUses += it->GetNSpectraUsed();

	return nUses;
}

int CCreateModelDlg::GetNSamplesForCalib()
{
	return int(NamesCalib.size());
}

int CCreateModelDlg::GetNSpectraForCalib()
{
	int sum = 0;
	ItModSam it;
	for(it=NamesCalib.begin(); it!=NamesCalib.end(); ++it)
	{
		ItIntBool it2;
		for(it2=it->Spectra.begin(); it2!=it->Spectra.end(); ++it2)
			if(it2->second)
				sum++;
	}

	return sum;
}

int CCreateModelDlg::GetMaxSpectraInNamesCalib()
{
	int N = 0;
	ItModSam it;
	for(it=NamesCalib.begin(); it!=NamesCalib.end(); ++it)
		if(it->GetNSpectraUsed() > N)
			N = it->GetNSpectraUsed();

	return N;
}

void CCreateModelDlg::PrepareSort(ModelSample& ModSam1)
{
	ModSam1.pRefSort = NULL;

	if(SortName == cSortTime)
	{
		return;
	}
	else if(SortName != cEmpty)
	{
		CTransferData *pTr1 = ParentPrj->GetTransfer(ModSam1.TransName);
		ModSam1.pRefSort = pTr1->GetSample(ModSam1.SampleName)->GetReferenceData(SortName);
	}
	ModSam1.iTransSort = pModCreate->GetTransNum(&ModSam1);
}



void CCreateModelDlg::SortNamesSmpl()
{
double tll1 = GetLLTimerSec();
	ItModSam it;
	for(it=NamesSmpl.begin(); it!=NamesSmpl.end(); ++it)
	{
		PrepareSort(*it);
	}
double dtll = GetLLTimerSec()-tll1;
TRACE1("Prepare: %f\n",dtll);

tll1 = GetLLTimerSec();
	if(SortDir)
		sort(NamesSmpl.begin(), NamesSmpl.end(), PSortModSamUp);
	else
		sort(NamesSmpl.begin(), NamesSmpl.end(), PSortModSamDown);
dtll = GetLLTimerSec()-tll1;
TRACE1("Sort: %f\n",dtll);
}

void CCreateModelDlg::SortNamesCalib()
{
	ItModSam it;
	for(it=NamesCalib.begin(); it!=NamesCalib.end(); ++it)
	{
		PrepareSort(*it);
	}
double tll1 = GetLLTimerSec();
	if(SortDir)
		sort(NamesCalib.begin(), NamesCalib.end(), PSortModSamUp);
	else
		sort(NamesCalib.begin(), NamesCalib.end(), PSortModSamDown);
double dtll = GetLLTimerSec()-tll1;
TRACE1("%f\n",dtll);
}

void CCreateModelDlg::SortNamesValid()
{
	ItModSam it;
	for(it=NamesValid.begin(); it!=NamesValid.end(); ++it)
	{
		PrepareSort(*it);
	}
double tll1 = GetLLTimerSec();
	if(SortDir)
		sort(NamesValid.begin(), NamesValid.end(), PSortModSamUp);
	else
		sort(NamesValid.begin(), NamesValid.end(), PSortModSamDown);
double dtll = GetLLTimerSec()-tll1;
TRACE1("%f\n",dtll);
}

void CCreateModelDlg::OnPreproc(int icb, int sel)
{
	int val = GetValBySel(icb, sel);

	int ind = FindPreproc(val);
	if(ind < 7 && ind > icb)
		m_vPreproc[ind] = C_PREPROC_0;
	if(val == C_PREPROC_C && icb < 6 && m_vPreproc[icb+1] == C_PREPROC_D)
		m_vPreproc[icb+1] = C_PREPROC_0;
	if(val == C_PREPROC_M && icb < 6 && m_vPreproc[icb+1] == C_PREPROC_C)
		m_vPreproc[icb+1] = C_PREPROC_0;
	if(val == C_PREPROC_M && icb < 5 && m_vPreproc[icb+1] == C_PREPROC_D && m_vPreproc[icb+2] == C_PREPROC_C)
		m_vPreproc[icb+2] = C_PREPROC_0;
	if(val == C_PREPROC_D && icb < 6 && icb > 0 && m_vPreproc[icb+1] == C_PREPROC_C && m_vPreproc[icb-1] == C_PREPROC_M)
		m_vPreproc[icb+1] = C_PREPROC_0;
	if(val == C_PREPROC_1)
	{
		ind = FindPreproc(C_PREPROC_2);
		if(ind < 7 && ind > icb)
			m_vPreproc[ind] = C_PREPROC_0;
	}
	if(val == C_PREPROC_2)
	{
		ind = FindPreproc(C_PREPROC_1);
		if(ind < 7 && ind > icb)
			m_vPreproc[ind] = C_PREPROC_0;
	}

	m_vPreproc[icb] = val;
	FillListsPreproc(icb+1);
}

int CCreateModelDlg::FindPreproc(int val)
{
	int i;
	ItInt it;
	for(i=0, it=m_vPreproc.begin(); it!=m_vPreproc.end(); ++it, i++)
		if((*it) == val)
			return i;

	return 100;
}

int CCreateModelDlg::GetSelByVal(int icb, int val)
{
	if(icb == 0 || val < C_PREPROC_M)  return 0;

	ItInt it;
	int sel = val - 1;
	bool checkC = (val > C_PREPROC_C);
	for(it=m_vPreproc.begin(); it!=m_vPreproc.begin() + icb; ++it)
	{
		if((*it) >= C_PREPROC_M && (*it) < val)
		{
			sel--;
			if((*it) == C_PREPROC_C)  checkC = false;
		}
	}
	if(checkC && (m_vPreproc[icb-1] == C_PREPROC_M ||
	   (icb > 1 && m_vPreproc[icb-1] == C_PREPROC_D && m_vPreproc[icb-2] == C_PREPROC_M)))
		sel--;

	bool IsD = false;
	for(it=m_vPreproc.begin(); it!=m_vPreproc.begin() + icb; ++it)
		if((*it) == C_PREPROC_D)
			IsD = true;

	if(!IsD && m_vPreproc[icb-1] == C_PREPROC_C && m_vPreproc[icb] != C_PREPROC_M)
		sel--;

	return sel;
}

int CCreateModelDlg::GetValBySel(int icb, int sel)
{
	if(icb == 0 || sel == C_PREPROC_0)  return sel;

	int is = 0;
	for(int i=C_PREPROC_M; i<=C_PREPROC_2; i++)
	{
		CString s = cGetPreprocName(i);
		int ind = FindPreproc(i);
		if(ind < icb)
			continue;
		if(i == C_PREPROC_D && icb > 0 && m_vPreproc[icb-1] == C_PREPROC_C)
			continue;
		if(i == C_PREPROC_C && icb > 0 && m_vPreproc[icb-1] == C_PREPROC_M)
			continue;
		if(i == C_PREPROC_C && icb > 1 && m_vPreproc[icb-2] == C_PREPROC_M && m_vPreproc[icb-1] == C_PREPROC_D)
			continue;
		if(i == C_PREPROC_1 && FindPreproc(C_PREPROC_2) < icb)
			continue;
		if(i == C_PREPROC_2 && FindPreproc(C_PREPROC_1) < icb)
			continue;

		is++;
		if(sel == is)
			return i;
	}

	return C_PREPROC_0;
}

void CCreateModelDlg::CalcMinMaxCalibValue()
{
	MinValue = MaxValue = 0.;
	if(CurParam == Params.end())
		return;

	ItModSam itS;
	for(itS=NamesCalib.begin(); itS!=NamesCalib.end(); ++itS)
	{
		CTransferData *Trans = ParentPrj->GetTransfer(itS->TransName);
		CSampleData *Sam = Trans->GetSample(itS->SampleName);
		if(Sam == NULL)
			break;
		ReferenceData *Ref = Sam->GetReferenceData(CurParam->ParamName);
		if(Ref == NULL || Ref->bNoValue)  continue;

		if(itS == NamesCalib.begin())
			MinValue = MaxValue = Ref->Value;

		if(MinValue > Ref->Value)  MinValue = Ref->Value;
		if(MaxValue < Ref->Value)  MaxValue = Ref->Value;
	}
}

int CCreateModelDlg::GetNUseSpecPoints()
{
	return NAllPoints - GetNRealExcPoints();
}

int CCreateModelDlg::GetNRealExcPoints()
{
	int indS = ParentPrj->GetSpecPointInd(PointsAll[0]);
	int indF = ParentPrj->GetSpecPointInd(PointsAll[NAllPoints-1]);

	ItSInt it;
	int N;
	for(N=0, it=PointsExc.begin(); it!=PointsExc.end(); ++it)
	{
		if((*it) < indS)  continue;
		if((*it) > indF)  break;

		N++;
	}

	return N;
}

int CCreateModelDlg::GetNFactor()
{
	int K = 0;

	if(m_vAnType == C_ANALYSIS_QLT)
		K = m_vNComp;
	else if(m_vModType != C_MOD_MODEL_HSO)
		K = m_vNComp;
	else
	{
		switch(m_vHSOType)
		{
		case C_MOD_MODEL_HSO_LPM:
			K = int(PointsAll.size() - PointsExc.size());
			break;
		case C_MOD_MODEL_HSO_LHM:
			K = 2 * m_vNHarm + 1;
			break;
		case C_MOD_MODEL_HSO_NLPM:
			K = 2 * int(PointsAll.size() - PointsExc.size());
			break;
		case C_MOD_MODEL_HSO_NLHM:
			K = 2 * (2 * m_vNHarm + 1);
			break;
		}
	}

	return K;
}

bool CCreateModelDlg::IsPointExc(int ind)
{
	if(ind >= 0 && ind < int(PointsAll.size()))
	{
		int iPnt = ParentPrj->GetSpecPointInd(PointsAll[ind]);
		ItSInt it = find(PointsExc.begin(), PointsExc.end(), iPnt);
		if(it != PointsExc.end())
			return true;
	}

	return false;
}

bool CCreateModelDlg::IsSampleUse(int ind)
{
	if(ind >= 0 && ind < int(NamesSmpl.size()))
	{
		ModelSample* MSam = &NamesSmpl[ind];

		ItModSam it;
		for(it=NamesCalib.begin(); it!=NamesCalib.end(); ++it)
			if(!it->SampleName.Compare(MSam->SampleName) && !it->TransName.Compare(MSam->TransName))
				return true;

		for(it=NamesValid.begin(); it!=NamesValid.end(); ++it)
			if(!it->SampleName.Compare(MSam->SampleName) && !it->TransName.Compare(MSam->TransName))
				return true;
	}

	return false;
}

int CCreateModelDlg::GetTransNum(ModelSample *pMSam)
{
	ItStr it;
	int i;
	for(i=0, it=TransesSel.begin(); it!=TransesSel.end(); ++it, i++)
	{
		if(!it->Compare(pMSam->TransName))
		{
			return i;
		}
	}

	return -1;
}

bool CCreateModelDlg::IsParamUse(int ind)
{
	if(ind >= 0 && ind < int(NAllParams))
	{
		CString ParName = ParentPrj->GetParam(ind)->GetName();
		ItModPar it;
		for(it=Params.begin(); it!=Params.end(); ++it)
			if(!it->ParamName.Compare(ParName))
				return true;
	}

	return false;
}

bool CCreateModelDlg::IsTransNotVal(int ind)
{
	bool IsTN = (pTrans->GetType() == C_TRANS_TYPE_NORMAL);
	if(ind >= 0 && ind < int(TransesAll.size()))
	{
		CString TransName = TransesAll[ind];
		CTransferData *pTr = ParentPrj->GetTransfer(TransName);
		if(pTr == NULL || pTr->GetType() == C_TRANS_TYPE_OWN)
			return true;

		ItStr it;
		for(it=TransesSel.begin(); it!=TransesSel.end(); ++it)
			if(!(*it).Compare(TransName))
				return true;

		if(pTrans->GetType() == C_TRANS_TYPE_OWN)
			return false;

		if(pTrans->GetType() == C_TRANS_TYPE_NOTCORR && pTr->GetType() == C_TRANS_TYPE_NORMAL)
			return true;

		if(pTr->GetType() == C_TRANS_TYPE_NOTCORR && (!IsTN || (pTrans->GetLowLimSpec() >= pTr->GetLowLimSpec()
		   && pTrans->GetUpperLimSpec() <= pTr->GetUpperLimSpec())))
		    return false;

		if(pTr->GetType() == C_TRANS_TYPE_NORMAL && (!pTrans->GetName().Compare(pTr->GetName()) ||
		   (pTrans->GetLowLimSpec() == pTr->GetLowLimSpec() && pTrans->GetUpperLimSpec() == pTr->GetUpperLimSpec() &&
		   !pTrans->GetAllPreprocAsStr().Compare(pTr->GetAllPreprocAsStr()))))
		    return false;
	}

	return true;
}

bool CCreateModelDlg::IsAnTypeQlt()
{
	return (m_vAnType == C_ANALYSIS_QLT);
}

void CCreateModelDlg::ShowFields(int act/* = -1*/)
{
	if(act >= C_MODDLG_PAGE_PRM && act <= C_MODDLG_PAGE_SET)
		m_vTab = act;

	int showprm = (m_vTab == C_MODDLG_PAGE_PRM) ? SW_SHOW : SW_HIDE;
	int showsmp = (m_vTab == C_MODDLG_PAGE_SMP) ? SW_SHOW : SW_HIDE;
	int showpre = (m_vTab == C_MODDLG_PAGE_PRE) ? SW_SHOW : SW_HIDE;
	int showset = (m_vTab == C_MODDLG_PAGE_SET) ? SW_SHOW : SW_HIDE;

	GetDlgItem(IDC_ADD)->ShowWindow(showprm);
	GetDlgItem(IDC_ADD_ALL)->ShowWindow(showprm);
	GetDlgItem(IDC_REMOVE)->ShowWindow(showprm);
	GetDlgItem(IDC_REMOVE_ALL)->ShowWindow(showprm);
	GetDlgItem(IDC_ADD_5)->ShowWindow(showprm);
	GetDlgItem(IDC_REMOVE_5)->ShowWindow(showprm);
	GetDlgItem(IDC_REMOVE_ALL_5)->ShowWindow(showprm);
	GetDlgItem(IDC_INPUT_NAME)->ShowWindow(showprm);
	GetDlgItem(IDC_ANALYSIS_TYPE)->ShowWindow(showprm);
	GetDlgItem(IDC_PRJPARAMSSEL_LIST)->ShowWindow(showprm);
	GetDlgItem(IDC_PRJPARAMS_LIST)->ShowWindow(showprm);
	GetDlgItem(IDC_MODTRANSSEL_LIST)->ShowWindow(showprm);
	GetDlgItem(IDC_MODTRANS_LIST)->ShowWindow(showprm);
	GetDlgItem(IDC_STATIC_1)->ShowWindow(showprm);
	GetDlgItem(IDC_STATIC_2)->ShowWindow(showprm);
	GetDlgItem(IDC_STATIC_3)->ShowWindow(showprm);
	GetDlgItem(IDC_STATIC_4)->ShowWindow(showprm);
	GetDlgItem(IDC_STATIC_5)->ShowWindow(showprm);
	GetDlgItem(IDC_STATIC_50)->ShowWindow(showprm);
	GetDlgItem(IDC_STATIC_51)->ShowWindow(showprm);
	GetDlgItem(IDC_STATIC_52)->ShowWindow(showprm);

	GetDlgItem(IDC_OPTIM)->ShowWindow(showsmp);
	GetDlgItem(IDC_PLOT)->ShowWindow(showsmp && NamesSmpl.size() > 0);
	GetDlgItem(IDC_AUTOCHOOSE)->ShowWindow(showsmp);
	GetDlgItem(IDC_TEMPLATE)->ShowWindow(showsmp);
	GetDlgItem(IDC_USE_TEMPLATE)->ShowWindow(showsmp);
	GetDlgItem(IDC_TEMPLATE_SEL)->ShowWindow(showsmp);
	GetDlgItem(IDC_SAMPLES_LIST)->ShowWindow(showsmp);
	GetDlgItem(IDC_SAMPLESSEL_LIST)->ShowWindow(showsmp);
	GetDlgItem(IDC_ADD_3)->ShowWindow(showsmp);
	GetDlgItem(IDC_ADD_ALL_3)->ShowWindow(showsmp);
	GetDlgItem(IDC_REMOVE_3)->ShowWindow(showsmp);
	GetDlgItem(IDC_REMOVE_ALL_3)->ShowWindow(showsmp);
	GetDlgItem(IDC_SHOWSPEC_3)->ShowWindow(showsmp);
	GetDlgItem(IDC_PLOT_3)->ShowWindow(showsmp);
	GetDlgItem(IDC_SAMPLESSEL_LIST_2)->ShowWindow(showsmp);
	GetDlgItem(IDC_ADD_2)->ShowWindow(showsmp);
	GetDlgItem(IDC_ADD_ALL_2)->ShowWindow(showsmp);
	GetDlgItem(IDC_REMOVE_2)->ShowWindow(showsmp);
	GetDlgItem(IDC_REMOVE_ALL_2)->ShowWindow(showsmp);
	GetDlgItem(IDC_SHOWSPEC_2)->ShowWindow(showsmp);
	GetDlgItem(IDC_PLOT_2)->ShowWindow(showsmp);
	GetDlgItem(IDC_PROCENT)->ShowWindow(showsmp);
	GetDlgItem(IDC_STATIC_7)->ShowWindow(showsmp);
	GetDlgItem(IDC_STATIC_8)->ShowWindow(showsmp);
	GetDlgItem(IDC_STATIC_9)->ShowWindow(showsmp);
	GetDlgItem(IDC_STATIC_10)->ShowWindow(showsmp);
	GetDlgItem(IDC_STATIC_53)->ShowWindow(showsmp);

	GetDlgItem(IDC_SPECTRUM_TYPE)->ShowWindow(showpre);
	GetDlgItem(IDC_LOWLIM_SPEC)->ShowWindow(showpre);
	GetDlgItem(IDC_UPPERLIM_SPEC)->ShowWindow(showpre);
	GetDlgItem(IDC_PREPROC_1)->ShowWindow(showpre);
	GetDlgItem(IDC_PREPROC_2)->ShowWindow(showpre);
	GetDlgItem(IDC_PREPROC_3)->ShowWindow(showpre);
	GetDlgItem(IDC_PREPROC_4)->ShowWindow(showpre);
	GetDlgItem(IDC_PREPROC_5)->ShowWindow(showpre);
	GetDlgItem(IDC_PREPROC_6)->ShowWindow(showpre);
	GetDlgItem(IDC_PREPROC_7)->ShowWindow(showpre);
	GetDlgItem(IDC_SPECPOINTS_ALL_LIST)->ShowWindow(showpre);
	GetDlgItem(IDC_SPECPOINTS_EXC_LIST)->ShowWindow(showpre);
	GetDlgItem(IDC_ADD_4)->ShowWindow(showpre);
	GetDlgItem(IDC_ADD_ALL_4)->ShowWindow(showpre);
	GetDlgItem(IDC_REMOVE_4)->ShowWindow(showpre);
	GetDlgItem(IDC_REMOVE_ALL_4)->ShowWindow(showpre);
	GetDlgItem(IDC_STATIC_12)->ShowWindow(showpre);
	GetDlgItem(IDC_STATIC_13)->ShowWindow(showpre);
	GetDlgItem(IDC_STATIC_19)->ShowWindow(showpre);
	GetDlgItem(IDC_STATIC_20)->ShowWindow(showpre);
	GetDlgItem(IDC_STATIC_21)->ShowWindow(showpre);
	GetDlgItem(IDC_STATIC_38)->ShowWindow(showpre);
	GetDlgItem(IDC_STATIC_39)->ShowWindow(showpre);
	GetDlgItem(IDC_STATIC_40)->ShowWindow(showpre);
	GetDlgItem(IDC_STR_PREP_1)->ShowWindow(showpre);
	GetDlgItem(IDC_STR_PREP_2)->ShowWindow(showpre);
	GetDlgItem(IDC_STR_PREP_3)->ShowWindow(showpre);
	GetDlgItem(IDC_STR_PREP_4)->ShowWindow(showpre);
	GetDlgItem(IDC_STR_PREP_5)->ShowWindow(showpre);
	GetDlgItem(IDC_STR_PREP_6)->ShowWindow(showpre);
	GetDlgItem(IDC_STR_PREP_7)->ShowWindow(showpre);

	GetDlgItem(IDC_MODEL_TYPE)->ShowWindow(showset && m_vAnType == C_ANALYSIS_QNT);
	GetDlgItem(IDC_MODEL_QLT_TYPE)->ShowWindow(showset && m_vAnType == C_ANALYSIS_QLT);
	GetDlgItem(IDC_NCOMP)->ShowWindow(showset);
	GetDlgItem(IDC_MAXMAH)->ShowWindow(showset && m_vAnType == C_ANALYSIS_QLT);
	GetDlgItem(IDC_MAXMAH_2)->ShowWindow(showset && m_vAnType == C_ANALYSIS_QNT);
	GetDlgItem(IDC_MAXMSD)->ShowWindow(showset);
	GetDlgItem(IDC_MODEL_HSO_TYPE)->ShowWindow(showset);
	GetDlgItem(IDC_DIMHIPER)->ShowWindow(showset);
	GetDlgItem(IDC_NHARM)->ShowWindow(showset);
	GetDlgItem(IDC_KFNL)->ShowWindow(showset);
	GetDlgItem(IDC_USE_SECV)->ShowWindow(showset);
	GetDlgItem(IDC_CALCULATE)->ShowWindow(showset);
	GetDlgItem(IDC_CALIBPARAMS_LIST)->ShowWindow(showset);
	GetDlgItem(IDC_MAXMSD_CALIB)->ShowWindow(showset);
	GetDlgItem(IDC_LOWLIM_CALIB)->ShowWindow(showset);
	GetDlgItem(IDC_UPPERLIM_CALIB)->ShowWindow(showset);
	GetDlgItem(IDC_CORR_CALIB_B)->ShowWindow(showset);
	GetDlgItem(IDC_CORR_CALIB_A)->ShowWindow(showset);
	GetDlgItem(IDC_STATIC_28)->ShowWindow(showset);
	GetDlgItem(IDC_STATIC_29)->ShowWindow(showset);
	GetDlgItem(IDC_STATIC_30)->ShowWindow(showset);
	GetDlgItem(IDC_STATIC_31)->ShowWindow(showset && m_vAnType == C_ANALYSIS_QLT);
	GetDlgItem(IDC_STATIC_32)->ShowWindow(showset);
	GetDlgItem(IDC_STATIC_33)->ShowWindow(showset);
	GetDlgItem(IDC_STATIC_34)->ShowWindow(showset);
	GetDlgItem(IDC_STATIC_35)->ShowWindow(showset);
	GetDlgItem(IDC_STATIC_36)->ShowWindow(showset);
	GetDlgItem(IDC_STATIC_37)->ShowWindow(showset);
	GetDlgItem(IDC_STATIC_14)->ShowWindow(showset);
	GetDlgItem(IDC_STATIC_15)->ShowWindow(showset);
	GetDlgItem(IDC_STATIC_16)->ShowWindow(showset);
	GetDlgItem(IDC_STATIC_17)->ShowWindow(showset);
	GetDlgItem(IDC_STATIC_22)->ShowWindow(showset);
	GetDlgItem(IDC_STATIC_23)->ShowWindow(showset);
	GetDlgItem(IDC_STATIC_25)->ShowWindow(showset);
	GetDlgItem(IDC_STATIC_26)->ShowWindow(showset);
	GetDlgItem(IDC_STATIC_27)->ShowWindow(showset);
	GetDlgItem(IDC_STATIC_47)->ShowWindow(showset);
	GetDlgItem(IDC_STATIC_48)->ShowWindow(showset && m_vAnType == C_ANALYSIS_QNT);

	EnableFields();
}

void CCreateModelDlg::EnableFields()
{
	bool qnt = (m_vAnType == C_ANALYSIS_QNT);
	bool chk1, chk2, chk3, chk4, chk5, chk6, chk7, chk8, chk9;

	switch(m_vTab)
	{
	case C_MODDLG_PAGE_PRM:
		chk1 = (qnt && (NAllParams > 0));
		chk2 = (qnt && (Params.size() > 0));
		chk3 = (TransesAll.size() > 0);
		chk4 = (TransesSel.size() > 0);
		GetDlgItem(IDC_STATIC_2)->EnableWindow(qnt);
		GetDlgItem(IDC_STATIC_3)->EnableWindow(qnt);
		GetDlgItem(IDC_STATIC_5)->EnableWindow(qnt);
		GetDlgItem(IDC_PRJPARAMS_LIST)->EnableWindow(qnt);
		GetDlgItem(IDC_PRJPARAMSSEL_LIST)->EnableWindow(qnt);
		GetDlgItem(IDC_ADD)->EnableWindow(chk1);
		GetDlgItem(IDC_ADD_ALL)->EnableWindow(chk1);
		GetDlgItem(IDC_REMOVE)->EnableWindow(chk2);
		GetDlgItem(IDC_REMOVE_ALL)->EnableWindow(chk2);
		GetDlgItem(IDC_ADD_5)->EnableWindow(chk3);
		GetDlgItem(IDC_REMOVE_5)->EnableWindow(chk4);
		GetDlgItem(IDC_REMOVE_ALL_5)->EnableWindow(chk4);
		break;

	case C_MODDLG_PAGE_SMP:
		chk1 = (NamesSmpl.size() > 0);
		chk2 = (NamesCalib.size() > 0);
		chk3 = (NamesValid.size() > 0);
		chk4 = (m_vAnType == C_ANALYSIS_QNT);
		chk5 = (chk4 && GetOptimTemplates()->GetNTemplates() > 0);
		chk6 = (chk5 && m_vUseTemplate);
		chk7 = (chk4 && NamesCalib.size() > 1);
		chk8 = (chk4 && NamesValid.size() > 1);
		chk9 = (qnt && (Params.size() > 0));
		GetDlgItem(IDC_PLOT_3)->EnableWindow(chk7);
		GetDlgItem(IDC_PLOT_2)->EnableWindow(chk8);
		GetDlgItem(IDC_ADD_3)->EnableWindow(chk1);
		GetDlgItem(IDC_ADD_ALL_3)->EnableWindow(chk1);
		GetDlgItem(IDC_ADD_2)->EnableWindow(chk1);
		GetDlgItem(IDC_ADD_ALL_2)->EnableWindow(chk1);
		GetDlgItem(IDC_REMOVE_3)->EnableWindow(chk2);
		GetDlgItem(IDC_REMOVE_ALL_3)->EnableWindow(chk2);
		GetDlgItem(IDC_SHOWSPEC_3)->EnableWindow(chk2);
		GetDlgItem(IDC_REMOVE_2)->EnableWindow(chk3);
		GetDlgItem(IDC_REMOVE_ALL_2)->EnableWindow(chk3);
		GetDlgItem(IDC_SHOWSPEC_2)->EnableWindow(chk3);
		GetDlgItem(IDC_USE_TEMPLATE)->EnableWindow(chk5);
		GetDlgItem(IDC_TEMPLATE_SEL)->EnableWindow(chk6);
		GetDlgItem(IDC_STATIC_10)->EnableWindow(chk4);
		GetDlgItem(IDC_OPTIM)->EnableWindow(chk4);
		GetDlgItem(IDC_TEMPLATE)->EnableWindow(chk4);
		GetDlgItem(IDC_AUTOCHOOSE)->EnableWindow(chk9);

		break;

	case C_MODDLG_PAGE_PRE:
		chk1 = (pTrans->GetType() != C_TRANS_TYPE_NORMAL);
		chk4 = (NAllPoints > 0);
		chk5 = (PointsExc.size() > 0);
		GetDlgItem(IDC_STATIC_13)->EnableWindow(chk1);
		GetDlgItem(IDC_SPECTRUM_TYPE)->EnableWindow(chk1);
		GetDlgItem(IDC_ADD_4)->EnableWindow(chk4);
		GetDlgItem(IDC_ADD_ALL_4)->EnableWindow(chk4);
		GetDlgItem(IDC_REMOVE_4)->EnableWindow(chk5);
		GetDlgItem(IDC_REMOVE_ALL_4)->EnableWindow(chk5);
		break;

	case C_MODDLG_PAGE_SET:
		chk1 = (qnt && m_vModType == C_MOD_MODEL_HSO);
		chk2 = (chk1 && (m_vHSOType == C_MOD_MODEL_HSO_LHM || m_vHSOType == C_MOD_MODEL_HSO_NLHM));
		chk3 = (chk1 && (m_vHSOType == C_MOD_MODEL_HSO_NLPM || m_vHSOType == C_MOD_MODEL_HSO_NLHM));
		chk6 = (qnt && CurParam != Params.end());
		GetDlgItem(IDC_MODEL_TYPE)->EnableWindow(qnt);
		GetDlgItem(IDC_MODEL_QLT_TYPE)->EnableWindow(!qnt);
		GetDlgItem(IDC_STATIC_32)->EnableWindow(!qnt);
		GetDlgItem(IDC_STATIC_47)->EnableWindow(!qnt);
		GetDlgItem(IDC_MAXMSD)->EnableWindow(!qnt);
		GetDlgItem(IDC_USE_SECV)->EnableWindow(qnt);
		GetDlgItem(IDC_STATIC_33)->EnableWindow(chk1);
		GetDlgItem(IDC_STATIC_34)->EnableWindow(chk1);
		GetDlgItem(IDC_STATIC_35)->EnableWindow(chk1);
		GetDlgItem(IDC_STATIC_36)->EnableWindow(chk2);
		GetDlgItem(IDC_STATIC_37)->EnableWindow(chk3);
		GetDlgItem(IDC_MODEL_HSO_TYPE)->EnableWindow(chk1);
		GetDlgItem(IDC_DIMHIPER)->EnableWindow(chk1);
		GetDlgItem(IDC_NHARM)->EnableWindow(chk2);
		GetDlgItem(IDC_KFNL)->EnableWindow(chk3);
		GetDlgItem(IDC_STATIC_14)->EnableWindow(qnt);
		GetDlgItem(IDC_STATIC_15)->EnableWindow(chk6);
		GetDlgItem(IDC_STATIC_16)->EnableWindow(chk6);
		GetDlgItem(IDC_STATIC_17)->EnableWindow(chk6);
		GetDlgItem(IDC_STATIC_23)->EnableWindow(chk6);
		GetDlgItem(IDC_STATIC_25)->EnableWindow(chk6);
		GetDlgItem(IDC_CALIBPARAMS_LIST)->EnableWindow(qnt);
		GetDlgItem(IDC_MAXMSD_CALIB)->EnableWindow(chk6);
		GetDlgItem(IDC_LOWLIM_CALIB)->EnableWindow(chk6);
		GetDlgItem(IDC_UPPERLIM_CALIB)->EnableWindow(chk6);
		GetDlgItem(IDC_CORR_CALIB_B)->EnableWindow(chk6);
		GetDlgItem(IDC_CORR_CALIB_A)->EnableWindow(chk6);
		break;
	}
}

void CCreateModelDlg::FillAllPages()
{
	int i;
	CString s;

	m_AnType.ResetContent();
	for(i=C_ANALYSIS_QNT; i<=C_ANALYSIS_QLT; i++)
	{
		s = cGetAnalysisTypeName(i);
		m_AnType.AddString(s);
	}
	m_AnType.SetCurSel(m_vAnType);

	m_listParamAll.InsertColumn(0, cEmpty, LVCFMT_LEFT, 0, 0);
	CRect Rect;
	m_listParamAll.GetWindowRect(Rect);
	m_listParamAll.SetColumnWidth(0, Rect.Width()-5);
	m_listParamAll.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

	m_listParamAll.DeleteAllItems();
	if(NAllParams > 0)
	{
		for(i=0; i<NAllParams; i++)
			m_listParamAll.InsertItem(i, ParentPrj->GetParam(i)->GetName());
		m_listParamAll.SetItemState(0, LVIS_SELECTED, LVIS_SELECTED);
	}

	if(CurParam != Params.end())
		FillListParamSel(-1);
	else
		FillListParamSel(int(CurParam - Params.begin()));

	m_listTransAll.InsertColumn(0, ParcelLoadString(STRID_CREATE_MODEL_PRM_TRANS_COLNAME), LVCFMT_LEFT, 0, 0);
	m_listTransAll.InsertColumn(1, ParcelLoadString(STRID_CREATE_MODEL_PRM_TRANS_COLCORR), LVCFMT_LEFT, 0, 1);
	m_listTransAll.InsertColumn(2, ParcelLoadString(STRID_CREATE_MODEL_PRM_TRANS_COLPREP), LVCFMT_LEFT, 0, 2);
	m_listTransAll.InsertColumn(3, ParcelLoadString(STRID_CREATE_MODEL_PRM_TRANS_COLDIAP), LVCFMT_LEFT, 0, 3);
	m_listTransAll.GetWindowRect(Rect);
	m_listTransAll.SetColumnWidth(0, Rect.Width()-215);
	m_listTransAll.SetColumnWidth(1, 60);
	m_listTransAll.SetColumnWidth(2, 60);
	m_listTransAll.SetColumnWidth(3, 90);
	m_listTransAll.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

	FillListTransAll(0);
	FillListTransSel(0);

	FillTemplates();
	m_Template.SetCurSel(m_vTemplate);

	RecreateSmplLists();

	s = ParcelLoadString(STRID_CREATE_MODEL_SET_PARAMNAME);
	m_listParamC.InsertColumn(0, s, LVCFMT_LEFT, 0, 0);
	s = ParcelLoadString(STRID_CREATE_MODEL_SET_PARAMMSD);
	m_listParamC.InsertColumn(1, s, LVCFMT_CENTER, 0, 1);
	s = ParcelLoadString(STRID_CREATE_MODEL_SET_PARAMLOW);
	m_listParamC.InsertColumn(2, s, LVCFMT_CENTER, 0, 2);
	s = ParcelLoadString(STRID_CREATE_MODEL_SET_PARAMUPPER);
	m_listParamC.InsertColumn(3, s, LVCFMT_CENTER, 0, 3);
	s = ParcelLoadString(STRID_CREATE_MODEL_SET_PARAMCORRA);
	m_listParamC.InsertColumn(4, s, LVCFMT_CENTER, 0, 4);
	s = ParcelLoadString(STRID_CREATE_MODEL_SET_PARAMCORRB);
	m_listParamC.InsertColumn(5, s, LVCFMT_CENTER, 0, 5);
	for(i=0; i<6; i++)
		m_listParamC.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	m_listParamC.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

	m_SpType.ResetContent();
	for(i=C_MOD_SPECTRUM_TRA; i<=C_MOD_SPECTRUM_ABS; i++)
	{
		s = cGetSpectrumTypeName(i);
		m_SpType.AddString(s);
	}
	m_SpType.SetCurSel(m_vSpType);

	m_Preproc0.ResetContent();
	for(i=C_PREPROC_0; i<=C_PREPROC_2; i++)
		m_Preproc0.AddString(cGetPreprocName(i));
	m_Preproc0.SetCurSel(m_vPreproc[0]);
	FillListsPreproc(0);

	RefillParamCList(false);

	bool IsQnt = (m_vAnType == C_ANALYSIS_QNT);

	m_ModType.ResetContent();
	for(i=C_MOD_MODEL_PLS; i<=C_MOD_MODEL_HSO; i++)
	{
		s = cGetModelTypeName(i);
		m_ModType.AddString(s);
	}
	m_ModType.SetCurSel(m_vModType);

	m_ModQltType.ResetContent();
	m_ModQltType.AddString(cGetModelTypeName(C_MOD_MODEL_PCA));
	m_ModQltType.SetCurSel(0);

	m_HSOType.ResetContent();
	for(i=C_MOD_MODEL_HSO_LPM; i<=C_MOD_MODEL_HSO_NLHM; i++)
	{
		s = cGetModelHSOTypeName(i);
		m_HSOType.AddString(s);
	}
	m_HSOType.SetCurSel(m_vHSOType);

	if(IsQnt)
	{
		FillHSOFields();
		FillListExc(0);
	}

	s = _T("998");
	m_listAll.InsertColumn(0, s, LVCFMT_RIGHT, 0, 0);
	m_listExc.InsertColumn(0, s, LVCFMT_RIGHT, 0, 0);
	s = _T("999");
	m_listAll.InsertColumn(1, s, LVCFMT_LEFT, 0, 1);
	m_listExc.InsertColumn(1, s, LVCFMT_LEFT, 0, 1);

	for(i=0; i<2; i++)
		m_listAll.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	m_listAll.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

	for(i=0; i<2; i++)
		m_listExc.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	m_listExc.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

	FillSpecAllList();
	FillListExc(0);
}

void CCreateModelDlg::ReinitData()
{
	InitData();
	FillAllPages();
}

void CCreateModelDlg::InitData()
{
	int i;

	ParentPrj = GetProjectByHItem(ModData->GetParentHItem());
	m_vName = ModData->GetName();
	m_vAnType = ModData->GetAnalysisType();
	pTrans = ParentPrj->GetTransfer(ModData->GetTransInd());

	TransesSel.clear();
	copy(ModData->TransNames.begin(), ModData->TransNames.end(), inserter(TransesSel, TransesSel.begin()));

	GetTransValList();

	NAllParams = ParentPrj->GetNParams();

	Params.clear();
	for(i=0; i<ModData->GetNModelParams(); i++)
		Params.push_back(*(ModData->GetModelParam(i)));
	CurParam = (Params.empty()) ? Params.end() : Params.begin();

	if(CurParam == Params.end())
	{
		m_vMSD = 0.;
		m_vLowCalib = 0.;
		m_vUpperCalib = 0.;
		m_vCorrB = 1.;
		m_vCorrA = 0.;
	}
	else
	{
		m_vMSD = CurParam->LimMSD;
		m_vLowCalib = CurParam->LowValue;
		m_vUpperCalib = CurParam->UpperValue;
		m_vCorrB = CurParam->CorrB;
		m_vCorrA = CurParam->CorrA;
	}

	SortName = ModData->GetSamSortName();
	SortDir = ModData->GetSamSortDir();
	m_vTemplate = 0;
	m_vUseTemplate = false;

	CreateSmplAll();
	NamesCalib.clear();
	for(i=0; i<ModData->GetNSamplesForCalib(); i++)
		NamesCalib.push_back(*(ModData->GetSampleForCalib(i)));
	NamesValid.clear();
	for(i=0; i<ModData->GetNSamplesForValid(); i++)
		NamesValid.push_back(*(ModData->GetSampleForValid(i)));

	ModData->GetAllPreproc(m_vPreproc);

	m_vModType = ModData->GetModelType();
	m_vHSOType = ModData->GetModelHSOType();
	m_vNComp = (ModData->GetAnalysisType() == C_ANALYSIS_QNT) ? ModData->GetNCompQnt() : ModData->GetNCompQlt();
	m_vDimHiper = ModData->GetDimHiper();
	m_vNHarm = ModData->GetNHarm();
	m_vKfnl = ModData->GetKfnl();
	m_vMaxMah = m_vMaxMah2 = ModData->GetMaxMah();
	m_vMaxMSD = ModData->GetMaxMSD();
	m_vUseSECV = !ModData->IsUseSECV();

	m_vLowSpec = ModData->GetLowSpecRange();
	m_vUpperSpec = ModData->GetUpperSpecRange();
	m_vSpType = ModData->GetSpectrumType();

	GetSpecPoints();

	ModData->GetExcPoints(PointsExc);

	CalcMinMaxCalibValue();
}

bool CCreateModelDlg::CheckData()
{
	if(!UpdateData())
		return false;

	CString err;

	m_vName.Trim();
	if(m_vName.IsEmpty())
	{
		err = ParcelLoadString(STRID_CREATE_MODEL_DLG_ERROR_EMPTYNAME);
		ParcelError(err);
		return false;
	}
	if(Mode == C_MODDLG_MODE_COPY || Mode == C_MODDLG_MODE_CREATE || ModData->GetName().CompareNoCase(m_vName))
	{
		CModelData *mod = ParentPrj->GetModel(m_vName);
		if(mod != NULL)
		{
			err.Format(ParcelLoadString(STRID_CREATE_MODEL_DLG_ERROR_NAME), m_vName);
			ParcelError(err);
			return false;
		}
	}

	if(m_vLowSpec >= m_vUpperSpec)
	{
		ParcelError(ParcelLoadString(STRID_CREATE_MODEL_DLG_ERROR_SPECRANGE));
		return false;
	}

	int nUsePoints = int(PointsAll.size() - PointsExc.size());
	if(nUsePoints < 4 && cIsPrepInStr(cGetPreprocAsStr(&m_vPreproc), C_PREPROC_B))
	{
		ParcelError(ParcelLoadString(STRID_CREATE_MODEL_DLG_ERROR_PREPB));
		return false;
	}
	if(nUsePoints < 21 && (cIsPrepInStr(cGetPreprocAsStr(&m_vPreproc), C_PREPROC_1) ||
		cIsPrepInStr(cGetPreprocAsStr(&m_vPreproc), C_PREPROC_2)))
	{
		ParcelError(ParcelLoadString(STRID_CREATE_MODEL_DLG_ERROR_PREP12));
		return false;
	}

	if(m_vAnType == C_ANALYSIS_QNT)
	{
		if(Params.size() == 0)
		{
			ParcelError(ParcelLoadString(STRID_CREATE_MODEL_DLG_ERROR_NOPARAM));
			return false;
		}

		if(NamesCalib.size() < 2)
		{
			err = ParcelLoadString(STRID_CREATE_MODEL_DLG_ERROR_2SAM);
			ParcelError(err);
			return false;
		}

		if(GetNUsesSpectrumValid() == 1)
		{
			err = ParcelLoadString(STRID_CREATE_MODEL_DLG_ERROR_VAL2SPEC);
			ParcelError(err);
			return false;
		}

		if(m_vPreproc[0] == C_PREPROC_A && NamesCalib.size() < 4)
		{
			err = ParcelLoadString(STRID_CREATE_MODEL_DLG_ERROR_A);
			ParcelError(err);
			return false;
		}

		if(nUsePoints <= 0)
		{
			err = ParcelLoadString(STRID_CREATE_MODEL_DLG_ERROR_NOPNT);
			ParcelError(err);
			return false;
		}

		if(m_vModType == C_MOD_MODEL_HSO)
		{
			if((m_vHSOType == C_MOD_MODEL_HSO_LHM || m_vHSOType == C_MOD_MODEL_HSO_NLHM) &&
				(m_vNHarm < 3 || m_vNHarm > 60))
			{
				err.Format(ParcelLoadString(STRID_CREATE_MODEL_DLG_ERROR_NHARM), 3, 60);
				ParcelError(err);

				GetDlgItem(IDC_NHARM)->SetFocus();
				HWND hWndLastControl;
				GetDlgItem(IDC_NHARM, &hWndLastControl);
				::SendMessage(hWndLastControl, EM_SETSEL, 0, -1);

				return false;
			}

			if((m_vHSOType == C_MOD_MODEL_HSO_NLPM || m_vHSOType == C_MOD_MODEL_HSO_NLHM) && m_vKfnl < 0.1)
			{
				err.Format(ParcelLoadString(STRID_CREATE_MODEL_DLG_ERROR_KFNL), 0.1, 1.0);
				ParcelError(err);

				GetDlgItem(IDC_KFNL)->SetFocus();
				HWND hWndLastControl;
				GetDlgItem(IDC_KFNL, &hWndLastControl);
				::SendMessage(hWndLastControl, EM_SETSEL, 0, -1);

				return false;
			}

			bool IsAvr = (m_vPreproc[0] == C_PREPROC_A);
			int NF = (!IsAvr) ? GetNSpectraForCalib() - GetMaxSpectraInNamesCalib() : GetNSamplesForCalib() - 1;
			if(NF - GetNFactor() < 2)
			{
				err = ParcelLoadString(STRID_CREATE_MODEL_DLG_ERROR_FREE);
				if(m_vHSOType == C_MOD_MODEL_HSO_LPM || m_vHSOType == C_MOD_MODEL_HSO_NLPM)
					err += ParcelLoadString(STRID_CREATE_MODEL_DLG_ERROR_FREEP);
				else
					err += ParcelLoadString(STRID_CREATE_MODEL_DLG_ERROR_FREEH);
				ParcelError(err);
				return false;
			}
		}
	}
	else
	{
		if(GetNUsesSpectrumCalib() < 2)
		{
			err = ParcelLoadString(STRID_CREATE_MODEL_DLG_ERROR_QLT2SPEC);
			ParcelError(err);
			return false;
		}

		if(m_vPreproc[0] == C_PREPROC_A && NamesCalib.size() < 2)
		{
			err = ParcelLoadString(STRID_CREATE_MODEL_DLG_ERROR_QLT2ASAM);
			ParcelError(err);
			return false;
		}
	}

	if(GetNUsesSpectrumCalib() < GetMaxSpectraInNamesCalib() + 3)
	{
		err = ParcelLoadString(STRID_CREATE_MODEL_DLG_ERROR_SPECCOND);
		ParcelError(err);
		return false;
	}

	if(m_vNComp < 1 || m_vNComp > NMaxComp)
	{
		CString err;
		err.Format(ParcelLoadString(STRID_CREATE_MODEL_DLG_ERROR_NCOMP), 1, NMaxComp);
		ParcelError(err);

		GetDlgItem(IDC_NCOMP)->SetFocus();
		HWND hWndLastControl;
		GetDlgItem(IDC_NCOMP, &hWndLastControl);
		::SendMessage(hWndLastControl, EM_SETSEL, 0, -1);

		return false;
	}

	return true;
}

bool CCreateModelDlg::AcceptData()
{
	if(!CheckData())
		return false;

	ModData->SetName(m_vName);
	ModData->SetAnalysisType(m_vAnType);
	if(pTrans != NULL)
		ModData->SetTransName(pTrans->GetName());
	else
		ModData->SetTransName(cEmpty);

	ModData->TransNames.clear();
	copy(TransesSel.begin(), TransesSel.end(), inserter(ModData->TransNames, ModData->TransNames.begin()));

	ModData->DeleteAllModelParams();
	ItModPar it;
	for(it=Params.begin(); it!=Params.end(); ++it)
		ModData->AddModelParam(&(*it));

	ModData->SetSamSortName(SortName);
	ModData->SetSamSortDir(SortDir);

	ModData->DeleteAllSamplesForCalib();
	ItModSam it2;
	for(it2=NamesCalib.begin(); it2!=NamesCalib.end(); ++it2)
		ModData->AddSampleForCalib(&(*it2));

	ModData->DeleteAllSamplesForValid();
	for(it2=NamesValid.begin(); it2!=NamesValid.end(); ++it2)
		ModData->AddSampleForValid(&(*it2));

	ModData->CalcModParamLimits();

	ModData->SetSpectrumType(m_vSpType);
	ModData->SetAllPreproc(&m_vPreproc);

	if(ModData->GetLowSpecRange() != m_vLowSpec || ModData->GetUpperSpecRange() != m_vUpperSpec)
	{
		ModData->SetLowSpecRange(m_vLowSpec);
		ModData->SetUpperSpecRange(m_vUpperSpec);
		ModData->GetSpecPoints();
	}

	ModData->SetModelType(m_vModType);
	if(ModData->GetAnalysisType() == C_ANALYSIS_QNT)
	{
		ModData->SetNCompQnt(m_vNComp);

		if(m_vModType == C_MOD_MODEL_HSO)
		{
			ModData->SetDimHiper(m_vDimHiper);
			ModData->SetNHarm(m_vNHarm);
			ModData->SetKfnl(m_vKfnl);
		}
		ModData->SetMaxMah(m_vMaxMah2);
	}
	else
	{
		ModData->SetNCompQlt(m_vNComp);
		ModData->SetMaxMSD(m_vMaxMSD);
		ModData->SetMaxMah(m_vMaxMah);
	}

	ModData->SetExcPoints(PointsExc);

	if(m_vUseSECV)  ModData->SetUseSECV(false);
	else  ModData->SetUseSECV(true);

	return true;
}

void CCreateModelDlg::OnHelp()
{
	int IdHelp = DLGID_MAIN;
	switch(Mode)
	{
	case C_MODDLG_MODE_CREATE:  IdHelp = DLGID_MODEL_CREATE;  break;
	case C_MODDLG_MODE_CHANGE:  IdHelp = DLGID_MODEL_CHANGE;  break;
	case C_MODDLG_MODE_COPY:  IdHelp = DLGID_MODEL_COPY;  break;
	}

	pFrame->ShowHelp(IdHelp);
}
