#ifndef _DFX_CACHED_H_
#define _DFX_CACHED_H_


const int NSAMPLE_SPEC_MAX = 256;
const int DFX_MAX_STRLEN   = 4096;

class CDfxCache
{
protected:

	CStringArray m_strings;
	int m_idx;
	int m_cnt;
	CString m_fname;

	int Load(CString fname);

	void Clear();
	void ResetIdx() { m_idx = 0;}
	CString GetNextString();
	int GetNextString(TCHAR* str);

public:

	CDfxCache(CString fname);

	int CheckFile(CString fname);

	TCHAR* GetProfileItem(CString fname, CString section, CString key);
	CString GetProfileString(CString fname, CString section, CString key, TCHAR* sval);
	unsigned GetProfileHex(CString fname, CString section, CString key, unsigned uval);
	int GetProfileInt(CString fname, CString section, CString key, int ival);
	double GetProfileDouble(CString fname, CString section, CString key, double dval);
	int GetProfileIntArray(CString fname, CString section, CString key, int imaxsize, int *pdata);

	void DfxPutProfileItem(CString fname, CString section, CString key, CString item);
	void DfxPutProfileString(CString fname, CString section, CString key, CString sval);
	void DfxPutProfileHex(CString fname, CString section, CString key, unsigned uval);
	void DfxPutProfileInt(CString fname, CString section, CString key, int ival);
	void DfxPutProfileDouble(CString fname, CString section, CString key, double dval);
	void DfxPutProfileIntArray(CString fname, CString section, CString key, int isize, int *pdata);

	int DfxCheckProfileSection(CString chkstr, CString section, int soft=0);
	int DfxCheckProfileKey(CString chkstr, CString key);

	int GetNStrings();
	CString GetString(int idx);

};

#endif