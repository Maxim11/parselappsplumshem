#ifndef _PARCEL_TEMPLATE_MANAGER_DLG_H_
#define _PARCEL_TEMPLATE_MANAGER_DLG_H_

#pragma once

#include "Resource.h"
#include "OptimTemplate.h"

///////////////////////////////////////////////////////////////////////////////////////////////////
// ������ ������ � ��������� �����������

class CTemplateManagerDlg : public CDialog
{
	DECLARE_DYNAMIC(CTemplateManagerDlg)

public:

	CTemplateManagerDlg(CModelData* Mod, CWnd* pParent = NULL);		// �����������
	virtual ~CTemplateManagerDlg();									// ����������

	enum { IDD = IDD_TEMPLATE_MANAGER };	// ������������� �������

	CListCtrl m_list;						// ������ �����������

protected:

	COptimTemplate* Templates;
	CModelData* CurModel;

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();						// ������������� �������

//	afx_msg void OnTemplateSelChange(NMHDR* /*pNMHDR*/, LRESULT *pResult);
	afx_msg void OnLoad();				// ��������� ������� "��������� ���� ������ � ��������� �����������"
	afx_msg void OnCreate();			// ��������� ������� "������� ����� ���� ������ ��� �������� �����������"
	afx_msg void OnAdd();				// ��������� ������� "������� ����� ������"
	afx_msg void OnEdit();				// ��������� ������� "�������� ������"
	afx_msg void OnRemove();			// ��������� ������� "������� ������"
	afx_msg void OnHelp();				// ��������� ������� "������� �������"

	void FillList(int sel = -1);		// ��������� ������ ��������

	DECLARE_MESSAGE_MAP()
};

#endif
