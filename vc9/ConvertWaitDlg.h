#ifndef _CONVERT_WAIT_DLG_H_
#define _CONVERT_WAIT_DLG_H_

#pragma once

#include "WaitingDlg.h"
#include "DeviceData.h"


///////////////////////////////////////////////////////////////////////////////////////////////////
// ������ ��������� �������� ���������� ����������������� �������

class CConvertWaitDlg : public CWaitingDlg
{
	DECLARE_DYNAMIC(CConvertWaitDlg)

public:

	CConvertWaitDlg(CDeviceData* dev, bool sst, CWnd* pParent = NULL);	// �����������
	virtual ~CConvertWaitDlg();									// ����������

	enum { IDD = CWaitingDlg::IDD };	// ������������� �������

protected:

	CDeviceData* DevData;	// ��������� �� ������, � ������� ������������ �����������
	bool IsSST;				// ������� ����������� ������������ �������

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();						// ������������� �������

	virtual void OK();				// ��������� �������� ����� �� �������
	virtual void Stop();			// ��������� ����������� ����� �� �������
	virtual void Work();			// ������ �������� �������

	DECLARE_MESSAGE_MAP()
};

#endif
