#include "stdafx.h"

#include "BranchInfo.h"
#include "EditSampleListDlg.h"
#include "CreateSampleDlg.h"
#include "LoadSpectraPBDlg.h"
#include "MainFrm.h"



#include <string>
#include <sstream>

static bool isFloat( string myString ) 
{
    std::istringstream iss(myString);
    float f;
    iss >> noskipws >> f; // noskipws considers leading whitespace invalid
    // Check the entire string was consumed and if either failbit or badbit is set
    return iss.eof() && !iss.fail(); 
}

static CEditSampleListDlg* pEditSam = NULL;

/*
static int GetSampleColor(int item, int subitem, void* pData)
{
	if(pEditSam == NULL)
		return 0;

	COLORREF color = (subitem == 0 && pEditSam->IsSampleUse(item)) ? cColorPass : CLR_DEFAULT;

	pEditSam->m_list.CurTxtColor = color;

	return 1;
}
*/

// this function is sent to CEditList 
// there it is used to determine which type of ctrl to create 
// when cell is being edited
static int _List1_CellType( int row, int col )
{
	if(pEditSam == NULL) return CEditList::eText;

	if(!pEditSam->IsEditMode()) return CEditList::eText;

//	if(pEditSam->IsSampleUse(row)) return CEditList::eText;

/*
	if ( col == 0) // comments	
	{
		return pEditSam->IsSampleUse(row) ? CEditList::eText : CEditList::eEdit;
	}
	else
*/
	if ( col == 1 || col == 2) // comments	
	{
		return CEditList::eEdit;
	}
	else
	if (col > 4 ) // ref data
	{
		int iRet = pEditSam->IsRefUse(row,col-5) ? CEditList::eText : CEditList::eEdit;
		if(iRet == CEditList::eEdit)
		{
			CString str = pEditSam->m_list.GetItemText(row, col);
			if(str == ParcelLoadString(STRID_FIELD_NODATA))
			{
				pEditSam->m_list.SetItemText(row, col, _T(""));
			}
		}
		return iRet;
	}
	else
	{
		return CEditList::eText;
	}
}

static int _List1_CellCallback ( int row, int col, int code )
{
	if(pEditSam == NULL) return 0;
	if(!pEditSam->IsEditMode()) return CEditList::eText;

	if(code > 0)
	{
		pEditSam->OnEditCell(row, col);
	}
	else
	{
		if (col > 4 ) // ref data
		{
			CString str = pEditSam->m_list.GetItemText(row, col);
			if(str == _T(""))
			{
				pEditSam->m_list.SetItemText(row, col, ParcelLoadString(STRID_FIELD_NODATA));
			}
		}
	}

	return 1;
}


IMPLEMENT_DYNAMIC(CEditSampleListDlg, CDialog)

CEditSampleListDlg::CEditSampleListDlg(CProjectData* PrjData, LSam* samplesdata, HTREEITEM item, CWnd* pParent)
	: CDialog(CEditSampleListDlg::IDD, pParent)
{
	pEditSam = this;

	pParentPrj = PrjData;
	pTrans = pParentPrj->GetTransfer(item);
	pSamplesList = samplesdata;

	m_bOwn = (pTrans->GetType() == C_TRANS_TYPE_OWN);
	m_bTableChanged = false;
	m_bTableEditMode = false;
}

CEditSampleListDlg::~CEditSampleListDlg()
{
}

void CEditSampleListDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_SAMPLES_LIST, m_list);
	DDX_Control(pDX, IDC_BUTTON_EDIT, m_butEditSample);
	DDX_Control(pDX, IDC_BUTTON_REMOVE, m_butRemoveSample);
	DDX_Control(pDX, IDC_BUTTON_ADD, m_butAddSample);

	DDX_Control(pDX, IDC_EDIT_TABLE, m_butEditTable);
	DDX_Control(pDX, IDC_SAVE_TABLE, m_butSaveTable);
}

BEGIN_MESSAGE_MAP(CEditSampleListDlg, CDialog)

	ON_BN_CLICKED(IDHELP, OnHelp)
	ON_BN_CLICKED(IDC_BUTTON_ADD, OnAdd)
	ON_BN_CLICKED(IDC_BUTTON_EDIT, OnEdit)
	ON_BN_CLICKED(IDC_BUTTON_REMOVE, OnRemove)

	ON_NOTIFY(LVN_ITEMCHANGED, IDC_SAMPLES_LIST, OnSamplesListSelChange)
	ON_NOTIFY(HDN_ITEMCLICK, 0, OnSamplesListColumnClick)

	ON_BN_CLICKED(IDC_EDIT_TABLE, &CEditSampleListDlg::OnBnClickedEditTable)
	ON_BN_CLICKED(IDC_SAVE_TABLE, &CEditSampleListDlg::OnBnClickedSaveTable)
	ON_BN_CLICKED(IDCANCEL, &CEditSampleListDlg::OnBnClickedCancel)
END_MESSAGE_MAP()

BOOL CEditSampleListDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	SetWindowText(ParcelLoadString(STRID_EDIT_SAMPLELIST_HDR));

	CString s;

	s = ParcelLoadString(STRID_EDIT_SAMPLELIST_COLSAM);
	m_list.InsertColumn(0, s, LVCFMT_LEFT, 0, 0);
	s = ParcelLoadString(STRID_EDIT_SAMPLELIST_COLIDEN2);
	m_list.InsertColumn(1, s, LVCFMT_RIGHT, 0, 1);
	s = ParcelLoadString(STRID_EDIT_SAMPLELIST_COLIDEN3);
	m_list.InsertColumn(2, s, LVCFMT_RIGHT, 0, 2);
	s = ParcelLoadString(STRID_EDIT_SAMPLELIST_COLNSPEC);
	m_list.InsertColumn(3, s, LVCFMT_RIGHT, 0, 3);
	s = ParcelLoadString(STRID_MEAS_DATE);
	m_list.InsertColumn(4, s, LVCFMT_RIGHT, 0, 4);
	m_nTimeCol = 4;
	m_nParStart = 5;
	int i, N = pParentPrj->GetNParams();
	for(i=0; i<N; i++)
	{
		CParamData *Par = pParentPrj->GetParam(i);
		CString s;
		s.Format(cFmtS_S, Par->GetName(), Par->GetUnit());
		m_list.InsertColumn(i + m_nParStart, s, LVCFMT_RIGHT, 0, i + m_nParStart);
	}

	for(i=0; i<N+m_nParStart; i++)
	{
		m_list.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	}

	m_list.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

	m_list.SetCellType ( (fGetCellType)_List1_CellType );	
	m_list.SetCellCallback ( (fCellCallbackType)_List1_CellCallback );	


	FillList(0);

	GetDlgItem(IDCANCEL)->SetWindowText(ParcelLoadString(STRID_EDIT_SAMPLELIST_EXIT));
	GetDlgItem(IDHELP)->SetWindowText(ParcelLoadString(STRID_EDIT_SAMPLELIST_HELP));
	m_butAddSample.SetWindowText(ParcelLoadString(STRID_EDIT_SAMPLELIST_ADD));
	m_butEditSample.SetWindowText(ParcelLoadString(STRID_EDIT_SAMPLELIST_EDIT));
	m_butRemoveSample.SetWindowText(ParcelLoadString(STRID_EDIT_SAMPLELIST_REMOVE));

	m_butAddSample.EnableWindow(m_bOwn);

	m_butEditTable.SetWindowText(ParcelLoadString(STRID_EDIT_TABLE));
	m_butSaveTable.SetWindowText(ParcelLoadString(STRID_SAVE_CHANGES));
	m_butSaveTable.EnableWindow(0);

	GetDlgItem(IDHELP)->EnableWindow(pFrame->IsExistHelp());

	m_list.SetFocus();

	return false;
}

void CEditSampleListDlg::OnAdd()
{
	if(pTrans->GetType() != C_TRANS_TYPE_OWN)
		return;

	CSampleData NewSam(pParentPrj->GetHItem(), pParentPrj->GetTransOwnHItem());

	CCreateSampleDlg dlg(&NewSam, SAMDLG_MODE_CREATE);
	if(dlg.DoModal() != IDOK)
		return;
	if(!pParentPrj->AddSample(&NewSam))
		return;

	FillList(0);
}

void CEditSampleListDlg::OnEditCell(int iItem, int iSubItem)
{
	const int iFirstRef = 5;
	CString strText = m_list.GetItemText(iItem, iSubItem);

	CString strName = m_list.GetItemText(iItem, 0);
	strName.Remove(_T('*'));

	CSampleData* pSam = NULL;

//	check if the sample is already modified 
	ItSamPtr it;
	for(it=m_SamplesChanged.begin(); it!=m_SamplesChanged.end(); ++it)
	{
		if(!(*it)->GetName().CompareNoCase(strName))
		{
			pSam = *it;
			break;
		}
	}

//	else create and add a new modified sample
	if(!pSam)
	{
		CSampleData *pOldSam = pTrans->GetSample(strName);
		if(!pOldSam) return; //!!!

		pSam = new CSampleData(pParentPrj->GetHItem(), pTrans->GetHItem());
		pSam->Copy(*pOldSam);
		m_SamplesChanged.push_back(pSam);
	}


	if(iSubItem == 1)
	{
		pSam->SetIden2(strText);
	}
	else 
	if(iSubItem == 2)
	{
		pSam->SetIden3(strText);
	}
	else 
	if(iSubItem >= iFirstRef)
	{
		int iRef = iSubItem - iFirstRef;

//		checking for float
		CString strMFC(strText);
		strMFC.Replace(_T(","),_T("."));
		CT2CA pszConvertedAnsiString (strMFC);
		std::string strStd (pszConvertedAnsiString);
		bool bFloatOK = isFloat(strStd);

		if(!bFloatOK) 
		{
			pSam->SetReferenceAsNoValue(iRef);
			m_list.SetItemText(iItem, iSubItem, ParcelLoadString(STRID_FIELD_NODATA));
		}
		else 
		{
			strText.Replace(_T(","),_T("."));
			m_list.SetItemText(iItem, iSubItem, strText);
			double v = _tstof(strText);
			pSam->SetReferenceValue(iRef, v);
		}

/*
		CString strNoData = CString(L" ") + ParcelLoadString(STRID_FIELD_NODATA);
		strText.Trim(strNoData);
		if(strText.GetLength() == 0) 
		{
			pSam->SetReferenceAsNoValue(iRef);
			m_list.SetItemText(iItem, iSubItem, ParcelLoadString(STRID_FIELD_NODATA));
		}
		else 
		{
			if(strText.Replace(_T(","),_T(".")) > 0)
			{
				m_list.SetItemText(iItem, iSubItem, strText);
			}
			double v = _tstof(strText);
			pSam->SetReferenceValue(iRef, v);
		}
*/
	}
	

	m_bTableChanged = true;
	m_butSaveTable.EnableWindow(1);
}

void CEditSampleListDlg::OnEdit()
{
	int sel = m_list.GetNextItem(-1, LVNI_SELECTED);

	CSampleData* pOldSam = pTrans->GetSample(sel);
	if(pOldSam == NULL) return;

	VLSI PSam;
	LoadSampleInfo NewLSI;
	NewLSI.pSam = pOldSam;
	ItSpc it;
	for(it=pOldSam->SpecData.begin(); it!=pOldSam->SpecData.end(); ++it)
	{
		NewLSI.Nums.push_back(it->Num);
	}

	PSam.push_back(NewLSI);

	CLoadSpectraPBDlg dlg2(&PSam, this);
	if(dlg2.DoModal() != IDOK)
	{
		pOldSam->FreeSpectraData();
		return;
	}

	CSampleData NewSam(pParentPrj->GetHItem(), pTrans->GetHItem());
	NewSam.Copy(*pOldSam);

	int mode = (pTrans->GetType() == C_TRANS_TYPE_OWN) ? SAMDLG_MODE_CHANGE : SAMDLG_MODE_SHOW;
	CCreateSampleDlg dlg(&NewSam, mode);
	if(dlg.DoModal() != IDOK)
	{
		pOldSam->FreeSpectraData();
		return;
	}

	if(pTrans->GetType() == C_TRANS_TYPE_OWN)
	{
		if(!pParentPrj->ChangeSample(pOldSam, &NewSam))
		{
			pOldSam->FreeSpectraData();
			return;
		}
	}

	pOldSam->FreeSpectraData();

	FillList(sel, false);
}

void CEditSampleListDlg::OnRemove()
{
	if(pTrans->GetType() != C_TRANS_TYPE_OWN)
		return;

	int Ind = -1, nsel = m_list.GetSelectedCount();
	if(nsel <= 0)
		return;

	bool keeped = false;
	VStr Sel;
	for(int i=0; i<nsel; i++)
	{
		Ind = m_list.GetNextItem(Ind, LVNI_SELECTED);
		if(Ind < 0 || Ind >= int(pSamplesList->size()))
			break;

		if(pParentPrj->GetSampleKeepStatus(Ind) != C_KEEP_FREE)
		{
			keeped = true;
			continue;
		}

		Sel.push_back(pTrans->GetSample(Ind)->GetName());
	}

	CString s = (keeped) ? ParcelLoadString(STRID_EDIT_SAMPLELIST_ASK_DEL_PART)
		: ParcelLoadString(STRID_EDIT_SAMPLELIST_ASK_DEL);

	if(!ParcelConfirm(s, false))
		return;

	ItStr it2;
	for(it2=Sel.begin(); it2!=Sel.end(); ++it2)
	{
		pParentPrj->DeleteSample(*it2);
	}

	FillList(0);
}

void CEditSampleListDlg::OnSamplesListSelChange(NMHDR* /*pNMHDR*/, LRESULT *pResult)
{
	if(m_bTableEditMode) return;

	int Ind = -1, nsel = m_list.GetSelectedCount();
	if(nsel <= 0)
	{
		return;
	}

	m_butEditSample.EnableWindow((nsel == 1) && m_bOwn);

	if(pTrans->GetType() != C_TRANS_TYPE_OWN)
	{
		m_butRemoveSample.EnableWindow(0);
	}
	else
	{
		bool bOk = true;
		for(int i=0; i<nsel; i++)
		{
			Ind = m_list.GetNextItem(Ind, LVNI_SELECTED);
			if(Ind < 0 || Ind >= int(pSamplesList->size()))
			{
				break;
			}

			if(pParentPrj->GetSampleKeepStatus(Ind) != C_KEEP_FREE)
			{
				bOk = false;
				break;
			}
		}
		m_butRemoveSample.EnableWindow(bOk && m_bOwn);
	}

	*pResult = 0;
}

void CEditSampleListDlg::OnSamplesListColumnClick(NMHDR* pNMHDR, LRESULT* pResult) 
{
	if(m_bTableEditMode) return;

	NMLISTVIEW *pLV = (NMLISTVIEW *) pNMHDR;

	int item = pLV->iItem;

	CString name = pParentPrj->GetSamSortName();
	bool bdir = pParentPrj->GetSamSortDir();
	int ntype = 0;
	int idxpar = 0;

	bool findsort = false;
	if(item == 0)
	{
		if(name.IsEmpty())
		{
			bdir = !bdir;
		}
		ntype = C_SORT_SAMPLE;
		findsort = true;
	}
	else if(item == m_nTimeCol)	//	time
	{
		if(name == cSortTime)
		{
			bdir = !bdir;
		}
		ntype = C_SORT_TIME;
		findsort = true;
	}
	else if(item >= m_nParStart)
	{
		idxpar = item - m_nParStart;
		CString par = pParentPrj->GetParam(idxpar)->GetName();
		if(!par.Compare(name))
		{
			bdir = !bdir;
		}
		ntype = C_SORT_PARAM;
		findsort = true;
	}

	if(findsort)
	{
		GetSamSel();
		if(pParentPrj->SortSamples(ntype, bdir, idxpar))
		{
			FillList(0, false);
		}
	}

	*pResult = 0;
}

int CEditSampleListDlg::GetSamSel()
{
	SamSel.clear();
	int Ind = -1, nsel = m_list.GetSelectedCount();
	if(nsel <= 0)
		return 0;

	for(int i=0; i<nsel; i++)
	{
		Ind = m_list.GetNextItem(Ind, LVNI_SELECTED);
		if(Ind < 0) 
		{
			break;
		}
		CString sSample = m_list.GetItemText(Ind,0);
		sSample.Remove(_T('*'));
		SamSel.push_back(sSample);
	}
	return nsel;
}

static COLORREF colDarkGray = RGB(128,128,128);
static COLORREF colGray = RGB(232,232,232);
static COLORREF colCyan = RGB(217,245,245);
static COLORREF colPink = RGB(245,217,217);
static COLORREF colBlue = RGB(217,217,245);
static COLORREF colGreen = RGB(217,245,217);
static COLORREF colDarkGreen = RGB(0,160,0);
static COLORREF colDarkRed = RGB(224,0,0);


void CEditSampleListDlg::FillList(int sel, bool bFromScratch)
{
	COLORREF colEdit = RGB(0,160,0);
	COLORREF colNoEdit = RGB(160,160,160);//RGB(160,0,0);
	CString s;
	ItSam it;
	int i, j;

	int nsel = m_list.GetSelectedCount();
	bool emp = (pSamplesList->size() <= 0);

	if(!m_bTableEditMode)
	{
		m_butEditSample.EnableWindow(!emp && (nsel == 1) && m_bOwn);
		m_butRemoveSample.EnableWindow(!emp && m_bOwn);
	}

	if(m_bTableEditMode)
	{
		m_list.SetColTxtColor(1, colEdit, false);
		m_list.SetColStyle(1, LIS_TXTCOLOR);
		m_list.SetColTxtColor(2, colEdit, false);
		m_list.SetColStyle(2, LIS_TXTCOLOR);
		m_list.SetColTxtColor(3, colNoEdit, false);
		m_list.SetColStyle(3, LIS_TXTCOLOR);
		m_list.SetColTxtColor(4, colNoEdit, false);
		m_list.SetColStyle(4, LIS_TXTCOLOR);
	}
	else
	{
		m_list.SetColStyle(1, LIS_NO_COL_STYLE);
		m_list.SetColStyle(2, LIS_NO_COL_STYLE);
		m_list.SetColStyle(3, LIS_NO_COL_STYLE);
		m_list.SetColStyle(4, LIS_NO_COL_STYLE);
	}

	if(bFromScratch) m_list.DeleteAllItems();

	for(i=0, it=pSamplesList->begin(); it!=pSamplesList->end(); ++it, i++)
	{
		CString sName = it->GetName();
		CString sNameT = m_bOwn ? sName : (CString(L"*")+sName); 
		if(bFromScratch)
		{
			m_list.InsertItem(i, sNameT);
		}
		else
		{
			m_list.SetItemText(i, 0, sNameT);
	
			m_list.SetItemState(i, 0, LVIS_SELECTED);
			int ns = SamSel.size();
			for(int k=0; k<ns; k++)
			{
				if(s == SamSel[k])
				{
					m_list.SetItemState(i, LVIS_SELECTED, LVIS_SELECTED);
					break;
				}
			}

		}

		if(m_bTableEditMode)
		{
			if((pParentPrj->GetTransSampleKeepStatus(pTrans->GetName(), sName) == C_KEEP_NOEDIT))
			{
				COLORREF col = colNoEdit;
				m_list.SetItemTxtColor(i, 0, col, false);
				m_list.SetItemStyle(i, 0, LIS_TXTCOLOR);
			}
		}

		m_list.SetItemText(i, 1, it->GetIden2());
		m_list.SetItemText(i, 2, it->GetIden3());
		
		s.Format(cFmt1_1, it->GetNSpectraUsed(), it->GetNSpectra());
		m_list.SetItemText(i, 3, s);
		
		if(it->GetNSpectra())
		{
			CTime t = it->GetLastSpecDate();
			s = t.Format(cFmtDate2);
		}
		else
		{
			s = L"--";
		}
		m_list.SetItemText(i, m_nTimeCol, s);
		
		for(j=0; j<pParentPrj->GetNParams(); j++)
		{
			s = it->GetReferenceDataStr(j);
			m_list.SetItemText(i, j + m_nParStart, s);
			if(m_bTableEditMode)
			{
				COLORREF col = IsRefUse(sName,j) ? colNoEdit : colEdit;
				m_list.SetItemTxtColor(i, j + m_nParStart, col, false);
				m_list.SetItemStyle(i, j + m_nParStart, LIS_TXTCOLOR);
			}
		}



		m_list.SetRowStyle(i, LIS_NO_ROW_STYLE);
		if(!m_bTableEditMode)
		{
			if((pParentPrj->GetTransSampleKeepStatus(pTrans->GetName(), sName) == C_KEEP_NOEDIT))
			{
				m_list.SetRowBgColor(i, colGray, false);
				m_list.SetRowStyle(i, LIS_BGCOLOR);
//				m_list.SetRowTxtColor(i, colDarkGray, true);
//				m_list.SetRowStyle(i, LIS_TXTCOLOR);
			}
		}

	}

	for(i=0; i<pParentPrj->GetNParams()+m_nParStart; i++)
	{
		m_list.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	}

	if(sel < 0 || sel >= int(pSamplesList->size()))
	{
		sel = 0;
	}

	if(bFromScratch)
	{
		m_list.SetItemState(sel, LVIS_SELECTED, LVIS_SELECTED);
	}

    s.Format(ParcelLoadString(STRID_EDIT_SAMPLELIST_SAMPLES), int(pSamplesList->size()), GetNSpectraUsed());
	GetDlgItem(IDC_STATIC_1)->SetWindowText(s);

//	m_list.SetFocus();
}

int CEditSampleListDlg::GetNSpectraUsed()
{
	int nUses = 0;
	ItSam it;
	for(it=pSamplesList->begin(); it!=pSamplesList->end(); ++it)
	{
		nUses += it->GetNSpectraUsed();
	}

	return nUses;
}

bool CEditSampleListDlg::IsRefUse(int iSam, int iRef)
{
	CString sSample = m_list.GetItemText(iSam,0);
	sSample.Remove(_T('*'));
	CString strTransName = pTrans->GetName();
	CString strRefName = pParentPrj->GetParam(iRef)->GetName();
	return pParentPrj->IsSampleRefKeep(sSample, strTransName, strRefName);
}

bool CEditSampleListDlg::IsRefUse(CString sSample, int iRef)
{
	CString strTransName = pTrans->GetName();
	CString strRefName = pParentPrj->GetParam(iRef)->GetName();
	return pParentPrj->IsSampleRefKeep(sSample, strTransName, strRefName);
}


void CEditSampleListDlg::OnHelp()
{
	int IdHelp = DLGID_SAMPLE_EDIT_LIST;

	pFrame->ShowHelp(IdHelp);
}

void CEditSampleListDlg::OnBnClickedEditTable()
{
	if(!m_bTableEditMode)
	{
		m_bTableEditMode = true;
		m_bTableChanged = false;
		m_SamplesChanged.clear();

		m_butAddSample.EnableWindow(0);
		m_butEditSample.EnableWindow(0);
		m_butRemoveSample.EnableWindow(0);
		m_butEditTable.SetWindowText(ParcelLoadString(STRID_DISCARD_CHANGES));
		m_butSaveTable.EnableWindow(1);
	}
	else
	{
		ItSamPtr it;
		for(it=m_SamplesChanged.begin(); it!=m_SamplesChanged.end(); ++it)
		{
			delete *it;
			*it = NULL;
		}
		m_SamplesChanged.clear();
		m_bTableChanged = false;
		m_bTableEditMode = false;
		m_butEditTable.SetWindowText(ParcelLoadString(STRID_EDIT_TABLE));
		m_butSaveTable.EnableWindow(0);
		m_butRemoveSample.EnableWindow(m_bOwn);
		m_butAddSample.EnableWindow(m_bOwn);
	}
	FillList(0);
	m_list.SetFocus();
}

void CEditSampleListDlg::OnBnClickedSaveTable()
{
	if(!m_bTableEditMode) return;
	if(m_bTableChanged)
	{
		ItSamPtr it;
		for(it=m_SamplesChanged.begin(); it!=m_SamplesChanged.end(); ++it)
		{
			CSampleData* pOldSam = pTrans->GetSample((*it)->GetName());
			if(!pOldSam)
			{
				AfxMessageBox(_T("Can't find old sample"));
				continue;
			}
			pParentPrj->ChangeSampleNoSpectra(pOldSam, *it, pTrans);
			delete *it;
			*it = NULL;
		}
	}
	m_SamplesChanged.clear();
	m_bTableEditMode = false;
	m_bTableChanged = false;
	m_butEditTable.SetWindowText(ParcelLoadString(STRID_EDIT_TABLE));
	m_butSaveTable.EnableWindow(0);
	FillList(0);
	m_butRemoveSample.EnableWindow(m_bOwn);
	m_butAddSample.EnableWindow(m_bOwn);
}

void CEditSampleListDlg::OnBnClickedCancel()
{
	if(m_bTableEditMode && m_bTableChanged)
	{
		if(!ParcelConfirm(ParcelLoadString(STRID_CREATE_SAMPLE_ASK_DOCANCEL), false)) return;
	}
	OnCancel();
}

void CEditSampleListDlg::OnOK()
{
	OnBnClickedSaveTable();
	CDialog::OnOK();
}
