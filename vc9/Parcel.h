// Parcel.h : main header file for the Parcel application
//
#pragma once

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols


// CParcelApp:
// See Parcel.cpp for the implementation of this class
//

class CParcelApp : public CWinApp
{
public:
	CParcelApp();

// Registry
	int DelRegSection(const TCHAR* strSection);
	int DelRegValue(const TCHAR* strSection, const TCHAR* strValue);

	int PutRegInt(const TCHAR* strSection, const TCHAR* strKey, int nValue);
	int GetRegInt(const TCHAR* strSection, const TCHAR* strKey, int *pnValue);
	int PutRegString(const TCHAR* strSection, const TCHAR* strKey, const TCHAR* strValue);
	int GetRegString(const TCHAR* strSection, const TCHAR* strKey, CString &strValue);

	CString GetAppVer(int *pnV1=0, int *pnV2=0, int *pnV3=0);


// Overrides
public:
	virtual BOOL InitInstance();

// Implementation
	afx_msg void OnAppAbout();
	afx_msg void OnAppExit();

	DECLARE_MESSAGE_MAP()
};

extern CParcelApp theApp;