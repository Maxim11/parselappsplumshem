#ifndef _PARCEL_MODEL_QLT_RESULT_DLG_H_
#define _PARCEL_MODEL_QLT_RESULT_DLG_H_

#pragma once

#include "ProjectData.h"
#include "ParcelListCtrl.h"
#include "Resource.h"


///////////////////////////////////////////////////////////////////////////////////////////////////
// ������ ������ ����������� ������� ������������ ������������� ������.

// ��������� ������ ������ �������

const int C_MODQLTRESDLG_MODE_EDIT = 0;		// ����� � ����������� ��������������� ������
const int C_MODQLTRESDLG_MODE_SHOW = 1;		// ����� � ����������� ��������������� ������

// �������� �������

const int C_MODQLTRES_PAGE_OUT		= 0;	// �������� ������ ��������
const int C_MODQLTRES_PAGE_SCORES	= 1;	// �������� ������ scores
const int C_MODQLTRES_PAGE_SPECLOAD	= 2;	// �������� ������ ������������ ��������

// � ����� ���� ����������

const int C_MODQLTRES_SHOW_TABLE	= 0;	// ���������� � ���� �������
const int C_MODQLTRES_SHOW_PLOT		= 1;	// ���������� � ���� �������

// ����� ������� ����������

const int C_MODQLTRES_SMPL_CALIB	= 0;	// ���������� �������������� �������
const int C_MODQLTRES_SMPL_VALID	= 1;	// ���������� ������������� �������

// ������ ������������ ����

const int C_MODQLTRES_MENU_PREV		= 11;
const int C_MODQLTRES_MENU_NEXT		= 12;
const int C_MODQLTRES_MENU_FULL		= 13;
const int C_MODQLTRES_MENU_AUTO		= 14;
const int C_MODQLTRES_MENU_LEFT		= 15;
const int C_MODQLTRES_MENU_RIGHT	= 16;
const int C_MODQLTRES_MENU_LEGEND	= 17;
const int C_MODQLTRES_MENU_GRID		= 18;
const int C_MODQLTRES_MENU_MDL		= 19;

class CModelQltResultDlg : public CDialog
{
	DECLARE_DYNAMIC(CModelQltResultDlg)

public:

	CModelQltResultDlg(CModelData* Mod, int mode = C_MODQLTRESDLG_MODE_EDIT, CWnd* pParent = NULL);	// �����������
	virtual ~CModelQltResultDlg();																	// ����������

	enum { IDD = IDD_MODEL_QLT_RESULT };						// ������������� �������

	CTabCtrl  m_Tab;			// �������� �� ��������
	CParcelListCtrl m_listBig;		// ������� ������ �����������
	CComboBox m_Show;			// ������ ��������� ����� �������������
	CComboBox m_CalibValid;		// ������ ��������� ������� ��������
	CComboBox m_OXsc;
	CComboBox m_OYsc;
	CEdit m_MaxMah;				// ���� ��� �������������� "������������� ���������� ������������"

	CMenu *pMenu1;

public:

	CModelData* ModData;		// ��������� �� �������������� ������
	LMod ModTmp;				// ���������� ������������� ��������

protected:

	CModelData* StartModel;		// ��������� �� ��������� �������������� ������

	int Mode;
	bool IsAvr;

	int m_vTab;					// ����� �������� ��������
	int m_vShow;				// ������ ���������� "���� �������������"
	int m_vCalibValid;			// ������ ���������� "������ ��������"
	int m_vOXsc;
	int m_vOYsc;
	double m_vMaxMah;			// ��������� �������� "������������� ���������� ������������"

	VStr selC;					// ����� ���������� �������������� ��������
	VStr selV;					// ����� ���������� ������������� ��������

	CLumChart *pChart;

	CLumChartDataSet* pHistoCalib;
	CLumChartDataSet* pHistoValid;
	CLumChartDataSet* pHistoMDL;
	CLumChartDataSet* pGraphScores;
	vector<CLumChartDataSet> GraphsSpecLoad;
	vector<POSITION> MasksSLPos;

	CLumChartDataMarker* pMarkHCalib;
	CLumChartDataMarker* pMarkHValid;
	CLumChartDataMarker* pMarkScores;

	POSITION posInfo;
	POSITION posCoord;
	POSITION posLegendOut;
	POSITION posLegendSpec;

	bool show_legend;
	bool show_mdl;

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();						// ������������� �������

	virtual void OnOK()
	{};
	virtual void OnCancel();			// ��������� ������� "�������� ���� � ����� �� �������"

	afx_msg void OnAccept();			// ��������� ������� "������� ������ � ����� �� �������"
	afx_msg void OnTabSelchange(NMHDR* pNMHDR, LRESULT* pResult);			// ��������� ������� "������� �� ������ ��������"
	afx_msg void OnTableBigSelChange(NMHDR* /*pNMHDR*/, LRESULT *pResult);	// ��������� ������� "�������� �������� � ������� �����������"
	afx_msg void OnTableBigColumnClick(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnChangeView();		// ��������� ������� "�������� ��� �������������"
	afx_msg void OnChangeCalibValid();	// ��������� ������� "�������� ������ ��������"
	afx_msg void OnMaxMahChanged();		// ��������� ������� "�������� ������������ ���������� ������������"
	afx_msg void OnExclude();			// ��������� ������� "��������� ���������� �������"
	afx_msg void OnBack();				// ��������� ������� "�������� ��������� ���������"
	afx_msg void OnBackAll();			// ��������� ������� "�������� ��� ���������"
	afx_msg void OnChangeAxeGKsc();
	afx_msg void OnSettings();
	afx_msg void OnSetTemplate();
	afx_msg void OnCreateProtocol();
	afx_msg void OnHelp();				// ��������� ������� "������� �������"

	void OnUserMenuPrev();
	void OnUserMenuNext();
	void OnUserMenuFull();
	void OnUserMenuAuto();
	void OnUserMenuLeft();
	void OnUserMenuRight();
	void OnUserMenuLegend();
	void OnUserMenuGrid();
	void OnUserMenuMDL();

	void ShowPage(int page = -1);		// ������� �� ����� ��������, ��������������� �������� ��������

	void RebuildTableBig();				// ����������� ������� �����������
	void FillTableBig();				// ��������� ������� �����������

	void SortAll();						// ����������� ���������� �������

	void SetSortIcons();

	bool ExcludeSpectra();
	bool ExcludePoints();

	void CreateHistoCalib();
	void CreateHistoValid();
	void CreateHistoMDL();
	void CreateGraphScores();
	void CreateGraphsSpecLoad();
	void CreateLegendOut();
	void CreateLegendSpec();
	void CreateAllGraphs();
	void CreateChart();

	void ColoredHistoCalib();
	void ColoredHistoValid();
	void ColoredGraphScores();
	void ColoredAllGraphs();

	void BuildUserPopupMenu();

	bool IsLegendEnable();
	int GetNGKShow();

public:

	void SelShowGraphs(int graph);
	void MakeGraphPicture(CString FileName, int graph);

	void SelectByRect(CRect* rect, bool isFrame = true);
	void ShowGK(CRect* rect);
	void ShowCoord(CPoint* pnt);
	void RemoveInfo();

	int GetCurTab();
	bool GetCurCalibValid();
	double GetMaxMah();
	bool IsResultBad(int item);

	void SetStatusMenu();

protected:

	DECLARE_MESSAGE_MAP()

};

#endif
