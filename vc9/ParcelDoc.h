#ifndef _PARCEL_DOC_H_
#define _PARCEL_DOC_H_

#pragma once

class CParcelDoc : public CDocument
{
protected: // create from serialization only
	CParcelDoc();
	DECLARE_DYNCREATE(CParcelDoc)

// Attributes
public:

// Operations
public:

// Overrides
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);

// Implementation
public:
	virtual ~CParcelDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
};


#endif