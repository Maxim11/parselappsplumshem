#ifndef _EXCLUDE_SPECTRA_DLG_H_
#define _EXCLUDE_SPECTRA_DLG_H_

#pragma once

#include "Resource.h"
#include "ModelData.h"


///////////////////////////////////////////////////////////////////////////////////////////////////
// ������ ���������� �������� �� ������� �������������� ������

class CExcludeSpectraDlg : public CDialog
{
	DECLARE_DYNAMIC(CExcludeSpectraDlg)

public:

	CExcludeSpectraDlg(CProjectData* PrjData, ModelSample* ModSample, int indT = 0, CWnd* pParent = NULL);	// �����������
	virtual ~CExcludeSpectraDlg();																		// ����������

	enum { IDD = IDD_EXCLUDE_SPECTRA };	// ������������� �������

	CListCtrl m_list;			// ������ ��������

protected:

	CProjectData *ParentPrj;	// ��������� �� ������, ������������ ��� ����������
	ModelSample *ModSam;		// ��������� �� ������� ������

	int indTrans;

	ItIntBool CurSpec;			// ��������� �� ������� ������

	bool IsBtnClk;

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();						// ������������� �������

	virtual void OnOK()
	{};
	virtual void OnCancel();			// ��������� ������� "�������� ���� � ����� �� �������"

	afx_msg void OnAccept();			// ��������� ������� "������� ������ � ����� �� �������"
	afx_msg void OnExclude();			// ��������� ������� "��������� (��������) ������"
	afx_msg void OnShow();				// ��������� ������� "�������� ������"
	afx_msg void OnHelp();				// ��������� ������� "������� �������"

	afx_msg void OnSpecListSelChange(NMHDR* /*pNMHDR*/, LRESULT *pResult); // ��������� ������� "�������� �������� � ������ ��������"

	void FillList();		// ��������� ������� �� ������� ��������

	bool IsChanged();

	DECLARE_MESSAGE_MAP()
};

#endif
