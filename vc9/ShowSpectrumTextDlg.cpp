#include "stdafx.h"

#include "ShowSpectrumTextDlg.h"
#include "MainFrm.h"


IMPLEMENT_DYNAMIC(CShowSpectrumTextDlg, CDialog)

CShowSpectrumTextDlg::CShowSpectrumTextDlg(VDbl* points, VDbl* data, CString name, CWnd* pParent)
	: CDialog(CShowSpectrumTextDlg::IDD, pParent)
{
	copy(points->begin(), points->end(), inserter(SpecPoints, SpecPoints.begin()));
	copy(data->begin(), data->end(), inserter(SpecData, SpecData.begin()));
	SpecName = name;
}

CShowSpectrumTextDlg::~CShowSpectrumTextDlg()
{
}

void CShowSpectrumTextDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_SAMPLES_LIST, m_list);
}

BEGIN_MESSAGE_MAP(CShowSpectrumTextDlg, CDialog)

	ON_BN_CLICKED(IDHELP, OnHelp)

END_MESSAGE_MAP()

BOOL CShowSpectrumTextDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	SetWindowText(SpecName);

	CString s;

	s = ParcelLoadString(STRID_SPECTRUM_TEXT_COLPOINT);
	m_list.InsertColumn(0, s, LVCFMT_LEFT, 0, 0);
	s = ParcelLoadString(STRID_SPECTRUM_TEXT_COLMEAN);
	m_list.InsertColumn(1, s, LVCFMT_RIGHT, 0, 1);
	
	int i;
	ItDbl it1, it2;
	for(i=0, it1=SpecPoints.begin(), it2=SpecData.begin(); it1!=SpecPoints.end(), it2!=SpecData.end(); ++it1, ++it2, i++)
	{
		CString s;
		s.Format(cFmt86, (*it1));
		m_list.InsertItem(i, s);
		s.Format(cFmt86, (*it2));
		m_list.SetItemText(i, 1, s);
	}
	for(i=0; i<2; i++)
		m_list.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	m_list.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

	GetDlgItem(IDCANCEL)->SetWindowText(ParcelLoadString(STRID_BUTTON_EXIT));
	GetDlgItem(IDHELP)->SetWindowText(ParcelLoadString(STRID_BUTTON_HELP));

	GetDlgItem(IDHELP)->EnableWindow(pFrame->IsExistHelp());

	return true;
}

void CShowSpectrumTextDlg::OnHelp()
{
	int IdHelp = DLGID_MAIN;

	pFrame->ShowHelp(IdHelp);
}
