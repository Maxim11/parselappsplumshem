#include "stdafx.h"

#include "Parcel.h"
#include "ParcelReport.h"
#include "ConstTypes.h"
#include "BranchInfo.h"
#include "RootData.h"
#include "FileSystem.h"

#include <math.h>

using namespace filesystem;

CParcelReportQnt::CParcelReportQnt(CModelResultDlg *pmoddlg, int iparam)
{
	pModDlg = pmoddlg;
	pMod = pModDlg->ModData;
	pRes = &pMod->ModelResult;
	pPrj = GetProjectByHItem(pMod->GetParentHItem());
	CurPar = iparam;
	ParFmt = pPrj->GetParam(pMod->GetModelParam(CurPar)->ParamName)->GetFormat();

	pMod->FillModInfo();
	pMod->CreateSamplesRest();
}

CWSTR CParcelReportQnt::GetTextFor(Wuid wuid) const
{
	CString sid = wuid.widget_id;
	CString Txt;

	if(!sid.Compare(_T("calib_samples_spectra")) && wuid.counter == 0)
	{
		Txt.Format(ParcelLoadString(STRID_VIEW_MOD_SAMPLES), pMod->GetNSamplesForCalib(), pMod->GetNSpectraForCalib());
		return Txt;
	}
	else if(!sid.Compare(_T("valid_samples_spectra")) && wuid.counter == 0)
	{
		Txt.Format(ParcelLoadString(STRID_VIEW_MOD_SAMPLESVAL), pMod->GetNSamplesForValid(), pMod->GetNSpectraForValid());
		return Txt;
	}
	else if(!sid.Compare(_T("excluded_points")) && wuid.counter == 0)
	{
		Txt.Format(ParcelLoadString(STRID_VIEW_MOD_EXCLUDED), pMod->GetNExcPoints());
		return Txt;
	}

	return cEmpty;
}

TextListItem const* CParcelReportQnt::GetTextArrayFor(Wuid wuid) const
{
	CString sid = wuid.widget_id;
	CString Txt, Tmp;
	CDeviceData *pDev = GetDeviceByHItem(pPrj->GetParentHItem());
	TextListItem const* NewLT;
	TextListItem* NewLT2;

	if(!sid.Compare(_T("instrument")) && wuid.counter == 0)
	{
		Txt = pDev->GetName();
		NewLT2 = new TextListItem(Txt, NULL);
		NewLT = new TextListItem(cEmpty, NewLT2);
		return NewLT;
	}
	else if(!sid.Compare(_T("report_created")) && wuid.counter == 0)
	{
		CTime Date = CTime::GetCurrentTime();
		Txt = Date.Format(cFmtDate);
		NewLT2 = new TextListItem(Txt, NULL);
		NewLT = new TextListItem(cEmpty, NewLT2);
		return NewLT;
	}
	else if(!sid.Compare(_T("software_version")) && wuid.counter == 0)
	{
		int n1, n2, n3;
		theApp.GetAppVer(&n1, &n2, &n3);
		Tmp.Format(ParcelLoadString(STRID_ABOUT_VERSION), n1, n2, n3);
		Txt = ParcelLoadString(STRID_ABOUT_PARCEL) + _T(". ") + Tmp;
		NewLT2 = new TextListItem(Txt, NULL);
		NewLT = new TextListItem(cEmpty, NewLT2);
		return NewLT;
	}
	else if(!sid.Compare(_T("Model")) && wuid.counter == 0)
	{
		Txt = pMod->GetName();
		NewLT2 = new TextListItem(Txt, NULL);
		NewLT = new TextListItem(cEmpty, NewLT2);
		return NewLT;
	}
	else if(!sid.Compare(_T("Project")) && wuid.counter == 0)
	{
		Txt = pPrj->GetName();
		NewLT2 = new TextListItem(Txt, NULL);
		NewLT = new TextListItem(cEmpty, NewLT2);
		return NewLT;
	}

	NewLT = new TextListItem(cEmpty, NULL);
	return NewLT;
}

CWSTR CParcelReportQnt::GetPicturePathFor(Wuid wuid) const
{
	CString sid = wuid.widget_id;
	CString FileName, Path = pRoot->GetReportPictureFolder(C_TEMPLATE_QNT);

	if(!sid.Compare(_T("histogram_calib")) && wuid.counter == 0)
	{
		FileName = MakePath(Path, _T("report_picture_histogram_calib.png"));
		pModDlg->MakeGraphPicture(FileName, C_PGRAPH_SAMCALIB);
	}
	else if(!sid.Compare(_T("histogram_valid")) && wuid.counter == 0)
	{
		FileName = MakePath(Path, _T("report_picture_histogram_valid.png"));
		pModDlg->MakeGraphPicture(FileName, C_PGRAPH_SAMVALID);
	}
	else if(!sid.Compare(_T("sec_graph")) && wuid.counter == 0)
	{
		FileName = MakePath(Path, _T("report_picture_sec_graph.png"));
		pModDlg->MakeGraphPicture(FileName, C_PGRAPH_SEC);
	}
	else if(!sid.Compare(_T("secv_graph")) && wuid.counter == 0)
	{
		FileName = MakePath(Path, _T("report_picture_secv_graph.png"));
		pModDlg->MakeGraphPicture(FileName, C_PGRAPH_SECV);
	}
	else if(!sid.Compare(_T("sev_graph")) && wuid.counter == 0) 
	{
		FileName = MakePath(Path, _T("report_picture_sev_graph.png"));
		pModDlg->MakeGraphPicture(FileName, C_PGRAPH_SEV);
	}
	else if(!sid.Compare(_T("MD_calib_histogram")) && wuid.counter == 0)
	{
		FileName = MakePath(Path, _T("report_picture_MD_calib_histogram.png"));
		pModDlg->MakeGraphPicture(FileName, C_PGRAPH_MDCALIB);
	}
	else if(!sid.Compare(_T("MD_valid_histogram")) && wuid.counter == 0)
	{
		FileName = MakePath(Path, _T("report_picture_MD_valid_histogram.png"));
		pModDlg->MakeGraphPicture(FileName, C_PGRAPH_MDVALID);
	}
	else if(!sid.Compare(_T("chem_histogram")) && wuid.counter == 0)
	{
		FileName = MakePath(Path, _T("report_picture_chem_histogram.png"));
		pModDlg->MakeGraphPicture(FileName, C_PGRAPH_BAD);
	}
	else if(!sid.Compare(_T("scores_graph")) && wuid.counter == 0)
	{
		FileName = MakePath(Path, _T("report_picture_scores_graph.png"));
		pModDlg->MakeGraphPicture(FileName, C_PGRAPH_SCORES);
	}
	else if(!sid.Compare(_T("loadings_graph")) && wuid.counter == 0)
	{
		FileName = MakePath(Path, _T("report_picture_loadings_graph.png"));
		pModDlg->MakeGraphPicture(FileName, C_PGRAPH_SPLOAD);
	}
	else if(!sid.Compare(_T("ch_loadings_graph")) && wuid.counter == 0)
	{
		FileName = MakePath(Path, _T("report_picture_ch_loadings_graph.png"));
		pModDlg->MakeGraphPicture(FileName, C_PGRAPH_CHLOAD);
	}

	return FileName;
}

TableHint const* CParcelReportQnt::GetTableHintFor(Wuid wuid) const
{
	CString sid = wuid.widget_id;
	CString Txt, Tmp;
	int NP = pMod->GetNModelParams();
	int NSC = pMod->GetNSamplesForCalib();
	int NSpC = pMod->GetNSpectraForCalib();
	int NSV = pMod->GetNSamplesForValid();
	int NSpV = pMod->GetNSpectraForValid();
	int NExc = pMod->GetNExcPoints();
	int NLR = pRes->GetNLoadingResult();
	int NGK = pMod->GetNCompQnt();
	int NSR = pMod->GetNSamplesRest();

	TableHint const* NewTH;

	if(!sid.Compare(_T("model_parameters")) && wuid.counter == 0)
		NewTH = new TableHint(2, pMod->GetNModInfo(), cEmpty, cEmpty);
	else if(!sid.Compare(_T("properties")) && wuid.counter == 0)
		NewTH = new TableHint(6, NP, cEmpty, cEmpty);
	else if(!sid.Compare(_T("calib_samples")) && wuid.counter == 0)
		NewTH = new TableHint(NP + 2, NSC, cEmpty, cEmpty);
	else if(!sid.Compare(_T("valid_samples")) && wuid.counter == 0)
		NewTH = new TableHint(NP + 2, NSV, cEmpty, cEmpty);
	else if(!sid.Compare(_T("excluded")) && wuid.counter == 0)
		NewTH = new TableHint(2, NExc, cEmpty, cEmpty);
	else if(!sid.Compare(_T("sec_table")) && wuid.counter == 0)
		NewTH = new TableHint(6, NSpC, cEmpty, cEmpty);
	else if(!sid.Compare(_T("sec_properties")) && wuid.counter == 0)
		NewTH = new TableHint(4, NP, cEmpty, cEmpty);
	else if(!sid.Compare(_T("secv_table")) && wuid.counter == 0)
		NewTH = new TableHint(7, NSpC, cEmpty, cEmpty);
	else if(!sid.Compare(_T("secv_properties")) && wuid.counter == 0)
		NewTH = new TableHint(5, NP, cEmpty, cEmpty);
	else if(!sid.Compare(_T("sev_table")) && wuid.counter == 0)
		NewTH = new TableHint(7, NSpV, cEmpty, cEmpty);
	else if(!sid.Compare(_T("sev_properties")) && wuid.counter == 0)
		NewTH = new TableHint(7, NP, cEmpty, cEmpty);
	else if(!sid.Compare(_T("MD_calib_table")) && wuid.counter == 0)
		NewTH = new TableHint(4, NSpC, cEmpty, cEmpty);
	else if(!sid.Compare(_T("MD_valid_table")) && wuid.counter == 0)
		NewTH = new TableHint(4, NSpV, cEmpty, cEmpty);
	else if(!sid.Compare(_T("chem_table")) && wuid.counter == 0)
		NewTH = new TableHint(7, NSpC, cEmpty, cEmpty);
	else if(!sid.Compare(_T("scores_table")) && wuid.counter == 0)
		NewTH = new TableHint(NGK + 2, NSpC, cEmpty, cEmpty);
	else if(!sid.Compare(_T("loadings_table")) && wuid.counter == 0)
		NewTH = new TableHint(NGK + 2, NLR, cEmpty, cEmpty);
	else if(!sid.Compare(_T("ch_loadings_table")) && wuid.counter == 0)
		NewTH = new TableHint(NGK + 2, NP, cEmpty, cEmpty);
	else if(!sid.Compare(_T("leftover")) && wuid.counter == 0)
		NewTH = new TableHint(NP + 2, NSR, cEmpty, cEmpty);
	else
		NewTH = new TableHint(1, 1, cEmpty, cEmpty);

	return NewTH;
}

ColumnHint const* CParcelReportQnt::GetTableHeaderHintFor(Wuid wuid, Index col) const
{
	CString sid = wuid.widget_id;
	ColumnHint const* NewCH;
	CString s, fmtGK = ParcelLoadString(STRID_MODEL_RESULT_SCORES_COL_GK);
	int NGK = pMod->GetNCompQnt();

	if(!sid.Compare(_T("model_parameters")) && wuid.counter == 0)
	{
		switch(col)
		{
		case 0:  NewCH = new ColumnHint(hLeft, ParcelLoadString(STRID_VIEW_MOD_COLSET_PARAMETR));  return NewCH;
		case 1:  NewCH = new ColumnHint(hRight, ParcelLoadString(STRID_VIEW_MOD_COLSET_VALUE));  return NewCH;
		}
	}
	else if(!sid.Compare(_T("properties")) && wuid.counter == 0)
	{
		switch(col)
		{
		case 0:  NewCH = new ColumnHint(hLeft, ParcelLoadString(STRID_VIEW_MOD_COLPAR_NAME));  return NewCH;
		case 1:  NewCH = new ColumnHint(hRight, ParcelLoadString(STRID_VIEW_MOD_COLPAR_MSD));  return NewCH;
		case 2:  NewCH = new ColumnHint(hRight, ParcelLoadString(STRID_VIEW_MOD_COLPAR_LOW));  return NewCH;
		case 3:  NewCH = new ColumnHint(hRight, ParcelLoadString(STRID_VIEW_MOD_COLPAR_UPPER));  return NewCH;
		case 4:  NewCH = new ColumnHint(hRight, ParcelLoadString(STRID_VIEW_MOD_COLPAR_CORRA));  return NewCH;
		case 5:  NewCH = new ColumnHint(hRight, ParcelLoadString(STRID_VIEW_MOD_COLPAR_CORRB));  return NewCH;
		}
	}
	else if(!sid.Compare(_T("calib_samples")) && wuid.counter == 0 && int(col) < pMod->GetNModelParams() + 2)
	{
		switch(col)
		{
		case 0:  NewCH = new ColumnHint(hLeft, ParcelLoadString(STRID_CREATE_MODEL_SMP_COLSAM));  return NewCH;
		case 1:  NewCH = new ColumnHint(hRight, ParcelLoadString(STRID_CREATE_MODEL_SMP_COLNSPEC));  return NewCH;
		default:  NewCH = new ColumnHint(hRight, pMod->GetModelParam(col - 2)->ParamName);  return NewCH;
		}
	}
	else if(!sid.Compare(_T("valid_samples")) && wuid.counter == 0 && int(col) < pMod->GetNModelParams() + 2)
	{
		switch(col)
		{
		case 0:  NewCH = new ColumnHint(hLeft, ParcelLoadString(STRID_CREATE_MODEL_SMP_COLSAM));  return NewCH;
		case 1:  NewCH = new ColumnHint(hRight, ParcelLoadString(STRID_CREATE_MODEL_SMP_COLNSPEC));  return NewCH;
		default:  NewCH = new ColumnHint(hRight, pMod->GetModelParam(col - 2)->ParamName);  return NewCH;
		}
	}
	else if(!sid.Compare(_T("excluded")) && wuid.counter == 0)
	{
		switch(col)
		{
		case 0:  NewCH = new ColumnHint(hLeft, cEmpty);  return NewCH;
		case 1:  NewCH = new ColumnHint(hLeft, cEmpty);  return NewCH;
		}
	}
	else if(!sid.Compare(_T("sec_table")) && wuid.counter == 0)
	{
		switch(col)
		{
		case 0:  NewCH = new ColumnHint(hLeft, ParcelLoadString(STRID_MODEL_RESULT_COL_NUM));  return NewCH;
		case 1:  NewCH = new ColumnHint(hLeft, ParcelLoadString(STRID_MODEL_RESULT_SEC_COL_SPEC));  return NewCH;
		case 2:  NewCH = new ColumnHint(hRight, ParcelLoadString(STRID_MODEL_RESULT_SEC_COL_REF));  return NewCH;
		case 3:  NewCH = new ColumnHint(hRight, ParcelLoadString(STRID_MODEL_RESULT_SEC_COL_PRED));  return NewCH;
		case 4:  NewCH = new ColumnHint(hRight, ParcelLoadString(STRID_MODEL_RESULT_SEC_COL_DIFF));  return NewCH;
		case 5:  NewCH = new ColumnHint(hRight, ParcelLoadString(STRID_MODEL_RESULT_SEC_COL_PROC));  return NewCH;
		}
	}
	else if(!sid.Compare(_T("sec_properties")) && wuid.counter == 0)
	{
		switch(col)
		{
		case 0:  NewCH = new ColumnHint(hLeft, ParcelLoadString(STRID_MODEL_RESULT_COL_PARAM));  return NewCH;
		case 1:  NewCH = new ColumnHint(hRight, cGetCritName(C_CRIT_SEC));  return NewCH;
		case 2:  NewCH = new ColumnHint(hRight, cGetCritName(C_CRIT_R2SEC));  return NewCH;
		case 3:  NewCH = new ColumnHint(hRight, ParcelLoadString(STRID_MODEL_RESULT_COLTREND));  return NewCH;
		}
	}
	else if(!sid.Compare(_T("secv_table")) && wuid.counter == 0)
	{
		switch(col)
		{
		case 0:  NewCH = new ColumnHint(hLeft, ParcelLoadString(STRID_MODEL_RESULT_COL_NUM));  return NewCH;
		case 1:  NewCH = new ColumnHint(hLeft, ParcelLoadString(STRID_MODEL_RESULT_SECV_COL_SPEC));  return NewCH;
		case 2:  NewCH = new ColumnHint(hRight, ParcelLoadString(STRID_MODEL_RESULT_SECV_COL_REF));  return NewCH;
		case 3:  NewCH = new ColumnHint(hRight, ParcelLoadString(STRID_MODEL_RESULT_SECV_COL_PRED));  return NewCH;
		case 4:  NewCH = new ColumnHint(hRight, ParcelLoadString(STRID_MODEL_RESULT_SECV_COL_DIFF));  return NewCH;
		case 5:  NewCH = new ColumnHint(hRight, ParcelLoadString(STRID_MODEL_RESULT_SECV_COL_PROC));  return NewCH;
		case 6:  NewCH = new ColumnHint(hRight, ParcelLoadString(STRID_MODEL_RESULT_SECV_COL_SEC));  return NewCH;
		}
	}
	else if(!sid.Compare(_T("secv_properties")) && wuid.counter == 0)
	{
		switch(col)
		{
		case 0:  NewCH = new ColumnHint(hLeft, ParcelLoadString(STRID_MODEL_RESULT_COL_NUM));  return NewCH;
		case 1:  NewCH = new ColumnHint(hRight, cGetCritName(C_CRIT_SECV));  return NewCH;
		case 2:  NewCH = new ColumnHint(hRight, cGetCritName(C_CRIT_R2SECV));  return NewCH;
		case 3:  NewCH = new ColumnHint(hRight, cGetCritName(C_CRIT_F));  return NewCH;
		case 4:  NewCH = new ColumnHint(hRight, ParcelLoadString(STRID_MODEL_RESULT_COLTREND));  return NewCH;
		}
	}
	else if(!sid.Compare(_T("sev_table")) && wuid.counter == 0)
	{
		switch(col)
		{
		case 0:  NewCH = new ColumnHint(hLeft, ParcelLoadString(STRID_MODEL_RESULT_COL_NUM));  return NewCH;
		case 1:  NewCH = new ColumnHint(hLeft, ParcelLoadString(STRID_MODEL_RESULT_SEV_COL_SPEC));  return NewCH;
		case 2:  NewCH = new ColumnHint(hRight, ParcelLoadString(STRID_MODEL_RESULT_SEV_COL_REF));  return NewCH;
		case 3:  NewCH = new ColumnHint(hRight, ParcelLoadString(STRID_MODEL_RESULT_SEV_COL_PRED));  return NewCH;
		case 4:  NewCH = new ColumnHint(hRight, ParcelLoadString(STRID_MODEL_RESULT_SEV_COL_DIFF));  return NewCH;
		case 5:  NewCH = new ColumnHint(hRight, ParcelLoadString(STRID_MODEL_RESULT_SEV_COL_PROC));  return NewCH;
		case 6:  NewCH = new ColumnHint(hRight, ParcelLoadString(STRID_MODEL_RESULT_SEV_COL_DELTA));  return NewCH;
		}
	}
	else if(!sid.Compare(_T("sev_properties")) && wuid.counter == 0)
	{
		switch(col)
		{
		case 0:  NewCH = new ColumnHint(hLeft, cEmpty);  return NewCH;
		case 1:  NewCH = new ColumnHint(hRight, cGetCritName(C_CRIT_SEV));  return NewCH;
		case 2:  NewCH = new ColumnHint(hRight, cGetCritName(C_CRIT_SDV));  return NewCH;
		case 3:  NewCH = new ColumnHint(hRight, cGetCritName(C_CRIT_R2SEV));  return NewCH;
		case 4:  NewCH = new ColumnHint(hRight, cGetCritName(C_CRIT_ERR));  return NewCH;
		case 5:  NewCH = new ColumnHint(hRight, ParcelLoadString(STRID_MODEL_RESULT_SEV_COL_T));  return NewCH;
		case 6:  NewCH = new ColumnHint(hRight, ParcelLoadString(STRID_MODEL_RESULT_COLTREND));  return NewCH;
		}
	}
	else if(!sid.Compare(_T("MD_calib_table")) && wuid.counter == 0)
	{
		switch(col)
		{
		case 0:  NewCH = new ColumnHint(hLeft, ParcelLoadString(STRID_MODEL_RESULT_COL_NUM));  return NewCH;
		case 1:  NewCH = new ColumnHint(hLeft, ParcelLoadString(STRID_MODEL_RESULT_OUT_COL_SPEC));  return NewCH;
		case 2:  NewCH = new ColumnHint(hRight, ParcelLoadString(STRID_MODEL_RESULT_OUT_COL_MAH));  return NewCH;
		case 3:  NewCH = new ColumnHint(hRight, ParcelLoadString(STRID_MODEL_RESULT_OUT_COL_EXCEED));  return NewCH;
		}
	}
	else if(!sid.Compare(_T("MD_valid_table")) && wuid.counter == 0)
	{
		switch(col)
		{
		case 0:  NewCH = new ColumnHint(hLeft, ParcelLoadString(STRID_MODEL_RESULT_COL_NUM));  return NewCH;
		case 1:  NewCH = new ColumnHint(hLeft, ParcelLoadString(STRID_MODEL_RESULT_OUT_COL_SPEC));  return NewCH;
		case 2:  NewCH = new ColumnHint(hRight, ParcelLoadString(STRID_MODEL_RESULT_OUT_COL_MAH));  return NewCH;
		case 3:  NewCH = new ColumnHint(hRight, ParcelLoadString(STRID_MODEL_RESULT_OUT_COL_EXCEED));  return NewCH;
		}
	}
	else if(!sid.Compare(_T("chem_table")) && wuid.counter == 0)
	{
		switch(col)
		{
		case 0:  NewCH = new ColumnHint(hLeft, ParcelLoadString(STRID_MODEL_RESULT_COL_NUM));  return NewCH;
		case 1:  NewCH = new ColumnHint(hLeft, ParcelLoadString(STRID_MODEL_RESULT_BAD_COL_SPEC));  return NewCH;
		case 2:  NewCH = new ColumnHint(hRight, ParcelLoadString(STRID_MODEL_RESULT_BAD_COL_REF));  return NewCH;
		case 3:  NewCH = new ColumnHint(hRight, ParcelLoadString(STRID_MODEL_RESULT_BAD_COL_PRED));  return NewCH;
		case 4:  NewCH = new ColumnHint(hRight, ParcelLoadString(STRID_MODEL_RESULT_BAD_COL_DIFF));  return NewCH;
		case 5:  NewCH = new ColumnHint(hRight, ParcelLoadString(STRID_MODEL_RESULT_BAD_COL_PROC));  return NewCH;
		case 6:  NewCH = new ColumnHint(hRight, ParcelLoadString(STRID_MODEL_RESULT_BAD_COL_REST));  return NewCH;
		}
	}
	else if(!sid.Compare(_T("scores_table")) && wuid.counter == 0 && int(col) < NGK + 2)
	{
		switch(col)
		{
		case 0:  NewCH = new ColumnHint(hLeft, ParcelLoadString(STRID_MODEL_RESULT_COL_NUM));  return NewCH;
		case 1:  NewCH = new ColumnHint(hLeft, ParcelLoadString(STRID_MODEL_RESULT_SCORES_COL_SPEC));  return NewCH;
		default:  s.Format(fmtGK, col - 2 + 1);  NewCH = new ColumnHint(hRight, s);  return NewCH;
		}
	}
	else if(!sid.Compare(_T("loadings_table")) && wuid.counter == 0 && int(col) < NGK + 2)
	{
		switch(col)
		{
		case 0:  NewCH = new ColumnHint(hLeft, ParcelLoadString(STRID_MODEL_RESULT_COL_NUM));  return NewCH;
		case 1:  NewCH = new ColumnHint(hLeft, ParcelLoadString(STRID_MODEL_RESULT_SPECLOAD_COL_WAVENUM));  return NewCH;
		default:  s.Format(fmtGK, col - 2 + 1);  NewCH = new ColumnHint(hRight, s);  return NewCH;
		}
	}
	else if(!sid.Compare(_T("ch_loadings_table")) && wuid.counter == 0 && int(col) < NGK + 2)
	{
		switch(col)
		{
		case 0:  NewCH = new ColumnHint(hLeft, ParcelLoadString(STRID_MODEL_RESULT_COL_NUM));  return NewCH;
		case 1:  NewCH = new ColumnHint(hLeft, ParcelLoadString(STRID_MODEL_RESULT_CHEMLOAD_COL_PARAM));  return NewCH;
		default:  s.Format(fmtGK, col - 2 + 1);  NewCH = new ColumnHint(hRight, s);  return NewCH;
		}
	}
	else if(!sid.Compare(_T("leftover")) && wuid.counter == 0 && int(col) < pMod->GetNModelParams() + 2)
	{
		switch(col)
		{
		case 0:  NewCH = new ColumnHint(hLeft, ParcelLoadString(STRID_CREATE_MODEL_SMP_COLSAM));  return NewCH;
		case 1:  NewCH = new ColumnHint(hRight, ParcelLoadString(STRID_CREATE_MODEL_SMP_COLNSPEC));  return NewCH;
		default:  NewCH = new ColumnHint(hRight, pMod->GetModelParam(col - 2)->ParamName);  return NewCH;
		}
	}
	else
		NewCH = new ColumnHint(hLeft, cEmpty);

	return NewCH;
}

CWSTR CParcelReportQnt::GetTableCellFor(Wuid wuid, Index row, Index col) const
{
	CString sid = wuid.widget_id;
	ParamSumResult *pPSR = pRes->GetPSResult(CurPar);
	int NGK = pMod->GetNCompQnt();
	CString s, fmt;

	if(!sid.Compare(_T("model_parameters")) && wuid.counter == 0 && int(col) < 2 && int(row) < pMod->GetNModInfo())
	{
		switch(col)
		{
		case 0:  return pMod->GetModInfo(row)->SetName;
		case 1:  return pMod->GetModInfo(row)->Value;
		}
	}
	else if(!sid.Compare(_T("properties")) && wuid.counter == 0 && int(col) < 6 && int(row) < pMod->GetNModelParams())
	{
		ModelParam *Dat = pMod->GetModelParam(row);
		switch(col)
		{
		case 0:  return Dat->ParamName;
		case 1:  s.Format(cFmt53, Dat->LimMSD);  return s;
		case 2:  s.Format(ParFmt, Dat->LowValue);  return s;
		case 3:  s.Format(ParFmt, Dat->UpperValue);  return s;
		case 4:  s.Format(ParFmt, Dat->CorrA);  return s;
		case 5:  s.Format(ParFmt, Dat->CorrB);  return s;
		}
	}
	else if(!sid.Compare(_T("calib_samples")) && wuid.counter == 0 && int(col) < pMod->GetNModelParams() + 2 && int(row) < pMod->GetNSamplesForCalib())
	{
		ModelSample *Dat = pMod->GetSampleForCalib(row);
		CTransferData* pTr = pPrj->GetTransfer(Dat->TransName);
		CSampleData *pSam = pTr->GetSample(Dat->SampleName);
		switch(col)
		{
		case 0:  return pMod->GetSampleTransName(Dat);
		case 1:  s.Format(_T("%s (%1d)"), Dat->GetUsesSpectrumStr(), Dat->GetNSpectra());  return s;
		default:  s = pSam->GetReferenceDataStr(pMod->GetModelParam(col - 2)->ParamName);  return s;
		}
	}
	else if(!sid.Compare(_T("valid_samples")) && wuid.counter == 0 && int(col) < pMod->GetNModelParams() + 2 && int(row) < pMod->GetNSamplesForValid())
	{
		ModelSample *Dat = pMod->GetSampleForValid(row);
		CTransferData* pTr = pPrj->GetTransfer(Dat->TransName);
		CSampleData *Sam = pTr->GetSample(Dat->SampleName);
		switch(col)
		{
		case 0:  return pMod->GetSampleTransName(Dat);
		case 1:  s.Format(_T("%s (%1d)"), Dat->GetUsesSpectrumStr(), Dat->GetNSpectra());  return s;
		default:  s = Sam->GetReferenceDataStr(pMod->GetModelParam(col - 2)->ParamName);  return s;
		}
	}
	else if(!sid.Compare(_T("excluded")) && wuid.counter == 0 && int(col) < 2 && int(row) < pMod->GetNExcPoints())
	{
		switch(col)
		{
		case 0:  s.Format(cFmt1, pMod->GetExcPoint(row) + 1);  return s;
		case 1:  s.Format(cFmt86, pMod->GetExcPointMean(row));  return s;
		}
	}
	else if(!sid.Compare(_T("sec_table")) && wuid.counter == 0 && int(col) < 6 && int(row) < pRes->GetNCalibResult())
	{
		SpectrumResult* pSpR = pRes->GetCalibResult(row);
		SpectrumParamResult *pSpPR = &pSpR->SpecParamData[CurPar];
		switch(col)
		{
		case 0:  s.Format(cFmt1, row + 1);  return s;
		case 1:  return pSpR->Name;
		case 2:  s.Format(ParFmt, pSpPR->Reference);  return s;
		case 3:  s.Format(ParFmt, pSpPR->SEC_Prediction);  return s;
		case 4:  s.Format(cFmt31, pSpPR->SEC_RefPred);  return s;
		case 5:
			if(pSpPR->Reference > 1.e-9)  s.Format(cFmt31, pSpPR->SEC_Proc);
			else  s = cEmpty;
			return s;
		}
	}
	else if(!sid.Compare(_T("sec_properties")) && wuid.counter == 0 && int(col) < 4 && int(row) < pMod->GetNModelParams())
	{
		ParamSumResult *pPSR2 = pRes->GetPSResult(row);
		switch(col)
		{
		case 0:  return pPSR2->ParamName;
		case 1:  s.Format(cFmt86, pPSR2->SEC);  return s;
		case 2:  s.Format(cFmt86, pPSR2->R2sec);  return s;
		case 3:
			if(pPSR->tSECb >= 0)  s.Format(ParcelLoadString(STRID_MODEL_RESULT_TRENDPLUS), pPSR2->tSECa, fabs(pPSR2->tSECb));
			else  s.Format(ParcelLoadString(STRID_MODEL_RESULT_TRENDMINUS), pPSR2->tSECa, fabs(pPSR2->tSECb));
			return s;
		}
	}
	else if(!sid.Compare(_T("secv_table")) && wuid.counter == 0 && int(col) < 7 && int(row) < pRes->GetNCalibResult())
	{
		SpectrumResult* pSpR = pRes->GetCalibResult(row);
		SpectrumParamResult *pSpPR = &pSpR->SpecParamData[CurPar];
		switch(col)
		{
		case 0:  s.Format(cFmt1, row + 1);  return s;
		case 1:  return pSpR->Name;
		case 2:  s.Format(ParFmt, pSpPR->Reference);  return s;
		case 3:  s.Format(ParFmt, pSpPR->SECV_Prediction);  return s;
		case 4:  s.Format(ParFmt, pSpPR->SECV_RefPred);  return s;
		case 5:  s.Format(cFmt31, pSpPR->SECV_Proc);  return s;
		case 6:  s.Format(cFmt86, pSpPR->SECV_SEC);  return s;
		}
	}
	else if(!sid.Compare(_T("secv_properties")) && wuid.counter == 0 && int(col) < 5 && int(row) < pMod->GetNModelParams())
	{
		ParamSumResult *pPSR2 = pRes->GetPSResult(row);
		switch(col)
		{
		case 0:  return pPSR2->ParamName;
		case 1:  s.Format(cFmt86, pPSR2->SECV);  return s;
		case 2:  s.Format(cFmt86, pPSR2->R2secv);  return s;
		case 3:  s.Format(cFmt86, pPSR2->F);  return s;
		case 4:
			if(pPSR->tSECb >= 0)  s.Format(ParcelLoadString(STRID_MODEL_RESULT_TRENDPLUS), pPSR2->tSECVa, fabs(pPSR2->tSECVb));
			else  s.Format(ParcelLoadString(STRID_MODEL_RESULT_TRENDMINUS), pPSR2->tSECVa, fabs(pPSR2->tSECVb));
			return s;
		}
	}
	else if(!sid.Compare(_T("sev_table")) && wuid.counter == 0 && int(col) < 7 && int(row) < pRes->GetNValidResult())
	{
		SpectrumValResult* pSpVR = pRes->GetValidResult(row);
		SpectrumValParamResult *pSpVPR = &pSpVR->SpecValParamData[CurPar];
		switch(col)
		{
		case 0:  s.Format(cFmt1, row + 1);  return s;
		case 1:  return pSpVR->Name;
		case 2:  s.Format(ParFmt, pSpVPR->Reference);  return s;
		case 3:  s.Format(ParFmt, pSpVPR->Corrected);  return s;
		case 4:  s.Format(ParFmt, pSpVPR->RefPred);  return s;
		case 5:  s.Format(cFmt31, pSpVPR->Proc);  return s;
		case 6:  s.Format(cFmt42, pSpVPR->Delta);  return s;
		}
	}
	else if(!sid.Compare(_T("sev_properties")) && wuid.counter == 0 && int(col) < 7 && int(row) < pMod->GetNModelParams())
	{
		ParamSumResult *pPSR2 = pRes->GetPSResult(row);
		switch(col)
		{
		case 0:  return pPSR2->ParamName;
		case 1:  s.Format(cFmt86, pPSR2->SEV);  return s;
		case 2:  s.Format(cFmt86, pPSR2->SDV);  return s;
		case 3:
			if(pRes->GetNVal() > 1)  s.Format(cFmt86, pPSR2->R2sev);
			else  s = ParcelLoadString(STRID_MODEL_RESULT_DASH);
			return s;
		case 4:  s.Format(cFmt86, pPSR2->e);  return s;
		case 5:  s.Format(_T("%4.2f (%4.2f)"), pPSR2->t, pPSR2->t0);  return s;
		case 6:
			if(pPSR->tSECb >= 0)  s.Format(ParcelLoadString(STRID_MODEL_RESULT_TRENDPLUS), pPSR2->tSEVa, fabs(pPSR2->tSEVb));
			else  s.Format(ParcelLoadString(STRID_MODEL_RESULT_TRENDMINUS), pPSR2->tSEVa, fabs(pPSR2->tSEVb));
			return s;
		}
	}
	else if(!sid.Compare(_T("MD_calib_table")) && wuid.counter == 0 && int(col) < 4 && int(row) < pRes->GetNCalibResult())
	{
		SpectrumResult* pSpR = pRes->GetCalibResult(row);
		switch(col)
		{
		case 0:  s.Format(cFmt1, row + 1);  return s;
		case 1:  return pSpR->Name;
		case 2:  s.Format(cFmt42, pSpR->Out_Mah);  return s;
		case 3:  s.Format(cFmt42, pMod->GetMaxMah() - pSpR->Out_Mah);  return s;
		}
	}
	else if(!sid.Compare(_T("MD_valid_table")) && wuid.counter == 0 && int(col) < 4 && int(row) < pRes->GetNValidResult())
	{
		SpectrumValResult* pSpVR = pRes->GetValidResult(row);
		switch(col)
		{
		case 0:  s.Format(cFmt1, row + 1);  return s;
		case 1:  return pSpVR->Name;
		case 2:  s.Format(cFmt42, pSpVR->Out_Mah);  return s;
		case 3:  s.Format(cFmt42, pMod->GetMaxMah() - pSpVR->Out_Mah);  return s;
		}
	}
	else if(!sid.Compare(_T("chem_table")) && wuid.counter == 0 && int(col) < 7 && int(row) < pRes->GetNCalibResult())
	{
		SpectrumResult* pSpR = pRes->GetCalibResult(row);
		SpectrumParamResult *pSpPR = &pSpR->SpecParamData[CurPar];
		switch(col)
		{
		case 0:  s.Format(cFmt1, row + 1);  return s;
		case 1:  return pSpR->Name;
		case 2:  s.Format(ParFmt, pSpPR->Reference);  return s;
		case 3:  s.Format(ParFmt, pSpPR->Bad_Prediction);  return s;
		case 4:  s.Format(ParFmt, pSpPR->Bad_RefPred);  return s;
		case 5:
			if(pSpPR->Reference > 1.e-9)  s.Format(cFmt31, pSpPR->Bad_Proc);
			else  s = cEmpty;
			return s;
		case 6:  s.Format(ParFmt, pSpPR->Bad_Remainder);  return s;
		}
	}
	else if(!sid.Compare(_T("scores_table")) && wuid.counter == 0 && int(col) < NGK + 2 && int(row) < pRes->GetNCalibResult())
	{
		SpectrumResult* pSpR = pRes->GetCalibResult(row);
		switch(col)
		{
		case 0:  s.Format(cFmt1, row + 1);  return s;
		case 1:  return pSpR->Name;
		default:  s.Format(cFmt86, pSpR->Scores_GK[col - 2]);  return s;
		}
	}
	else if(!sid.Compare(_T("loadings_table")) && wuid.counter == 0 && int(col) < NGK + 2 && int(row) < pRes->GetNLoadingResult())
	{
		LoadingResult *pLR = pRes->GetLoadingResultByInd(row);
		switch(col)
		{
		case 0:  s.Format(cFmt1, pLR->SpecPoint + 1);  return s;
		case 1:  s.Format(cFmt86, pPrj->GetSpecPoint(pLR->SpecPoint));  return s;
		default:  s.Format(cFmt86, pLR->Spec_GK[col - 2]);  return s;
		}
	}
	else if(!sid.Compare(_T("ch_loadings_table")) && wuid.counter == 0 && int(col) < NGK + 2 && int(row) < pRes->GetNPSResult())
	{
		ParamSumResult *pPSR2 = pRes->GetPSResult(row);
		switch(col)
		{
		case 0:  s.Format(cFmt1, row + 1);  return s;
		case 1:  return pPSR->ParamName;
		default:  s.Format(cFmt86, pPSR->Chem_GK[col - 2]);  return s;
		}
	}
	else if(!sid.Compare(_T("leftover")) && wuid.counter == 0 && int(col) < pMod->GetNModelParams() + 2 && int(row) < pMod->GetNSamplesRest())
	{
		ModelSample *Dat = pMod->GetSampleRest(row);
		CTransferData* pTr = pPrj->GetTransfer(Dat->TransName);
		CSampleData *pSam = pTr->GetSample(Dat->SampleName);
		switch(col)
		{
		case 0:  return pMod->GetSampleTransName(Dat);
		case 1:  s.Format(_T("%s (%1d)"), Dat->GetUnUsesSpectrumStr(), Dat->GetNSpectra());  return s;
		default:  s = pSam->GetReferenceDataStr(pMod->GetModelParam(col - 2)->ParamName);  return s;
		}
	}

	return cEmpty;
}

void CParcelReportQnt::SetReportLanguage(CWSTR language)
{
	return;
}
