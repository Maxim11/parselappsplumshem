#ifndef _CONVERT_READ_WAIT_DLG_H_
#define _CONVERT_READ_WAIT_DLG_H_

#pragma once

#include "WaitingDlg.h"
#include "DeviceData.h"


///////////////////////////////////////////////////////////////////////////////////////////////////
// ������ ��������� �������� ���������� ����������������� �������

class CConvertReadWaitDlg : public CWaitingDlg
{
	DECLARE_DYNAMIC(CConvertReadWaitDlg)

public:

	CConvertReadWaitDlg(CLoadOldDatas* pload, ProjectOldData* papj, CString fname, CWnd* pParent = NULL);	// �����������
	virtual ~CConvertReadWaitDlg();																			// ����������

	enum { IDD = CWaitingDlg::IDD };	// ������������� �������

protected:

	CLoadOldDatas* LoadData;
	ProjectOldData* OldApj;
	CString FileName;

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();						// ������������� �������

	virtual void OK();				// ��������� �������� ����� �� �������
	virtual void Stop();			// ��������� ����������� ����� �� �������
	virtual void Work();			// ������ �������� �������

	DECLARE_MESSAGE_MAP()
};

#endif
