#include "stdafx.h"

#include "BranchInfo.h"
#include "EditTransferDlg.h"
#include "MainFrm.h"


IMPLEMENT_DYNAMIC(CEditTransferDlg, CDialog)

CEditTransferDlg::CEditTransferDlg(CTransferData* data, CWnd* pParent)
	: CDialog(CEditTransferDlg::IDD, pParent)
{

	TransData = data;
	ParentPrj = GetProjectByHItem(TransData->GetParentHItem());

	m_vName = TransData->GetName();
}

CEditTransferDlg::~CEditTransferDlg()
{
}

void CEditTransferDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_INPUT_NAME, m_Name);

	cParcelDDX(pDX, IDC_INPUT_NAME, m_vName);
	DDV_MaxChars(pDX, m_vName, 50);
}


BEGIN_MESSAGE_MAP(CEditTransferDlg, CDialog)

	ON_BN_CLICKED(IDC_ACCEPT, OnAccept)
	ON_BN_CLICKED(IDHELP, OnHelp)

END_MESSAGE_MAP()


BOOL CEditTransferDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	SetWindowText(ParcelLoadString(STRID_EDIT_TRANS_HDR));

	GetDlgItem(IDC_ACCEPT)->SetWindowText(ParcelLoadString(STRID_EDIT_TRANS_ACCEPT));
	GetDlgItem(IDCANCEL)->SetWindowText(ParcelLoadString(STRID_EDIT_TRANS_DECLINE));
	GetDlgItem(IDHELP)->SetWindowText(ParcelLoadString(STRID_EDIT_TRANS_HELP));

	GetDlgItem(IDC_STATIC_1)->SetWindowText(ParcelLoadString(STRID_EDIT_TRANS_NAME));

	GetDlgItem(IDHELP)->EnableWindow(pFrame->IsExistHelp());

	return true;
}

void CEditTransferDlg::OnAccept()
{
	if(!UpdateData())
		return;

	CString s;

	m_vName.Trim();
	if(m_vName.IsEmpty())
	{
		ParcelError(ParcelLoadString(STRID_EDIT_TRANS_ERR_EMPTY));
		return;
	}
	if(TransData->GetName().Compare(m_vName))
	{
		CTransferData *Trans = ParentPrj->GetTransfer(m_vName);
		if(Trans != NULL)
		{
			s.Format(ParcelLoadString(STRID_EDIT_TRANS_ERR_EXIST), m_vName);
			ParcelError(s);
			return;
		}
	}

	TransData->SetName(m_vName);

	CDialog::OnOK();
}

void CEditTransferDlg::OnCancel()
{
	if(IsChanged())
		if(!ParcelConfirm(ParcelLoadString(STRID_EDIT_TRANS_ASK_DOCANCEL), false))
			return;

	CDialog::OnCancel();
}

bool CEditTransferDlg::IsChanged()
{
	CString NewName;
	m_Name.GetWindowText(NewName);

	if(TransData->GetName().Compare(NewName))
		return true;

	return false;
}

void CEditTransferDlg::OnHelp()
{
	int IdHelp = DLGID_SAMPLE_CHANGE_SET;

	pFrame->ShowHelp(IdHelp);
}
