#ifndef _EDIT_PARAM_LIST_DLG_H_
#define _EDIT_PARAM_LIST_DLG_H_

#pragma once

#include "Resource.h"
#include "ParamData.h"


///////////////////////////////////////////////////////////////////////////////////////////////////
// ������ �������������� ������ �����������

class CEditParamListDlg : public CDialog
{
	DECLARE_DYNAMIC(CEditParamListDlg)

public:

	CEditParamListDlg(CProjectData* PrjData, LPar* LParamsData, CWnd* pParent = NULL);	// �����������
	virtual ~CEditParamListDlg();														// ����������

	enum { IDD = IDD_EDIT_PARAM_LIST };	// ������������� �������

	CListCtrl m_list;					// ������ �����������

protected:

	CProjectData *ParentPrj;			// ��������� �� ������, ������������ ��� ����������
	LPar* ParamsList;					// ��������� �� ������ ����������� ��� ��������������

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();						// ������������� �������
/*
	virtual void OnOK();				// ��������� ������� "������� ������ � ����� �� �������"
	virtual void OnCancel();			// ��������� ������� "�������� ���� � ����� �� �������"
*/
	afx_msg void OnAdd();				// ��������� ������� "������� ����� ����������"
	afx_msg void OnEdit();				// ��������� ������� "�������� ����������"
	afx_msg void OnRemove();			// ��������� ������� "������� ����������"
	afx_msg void OnHelp();				// ��������� ������� "������� �������"

	afx_msg void OnParamsListSelChange(NMHDR* /*pNMHDR*/, LRESULT *pResult); // ��������� ������� "�������� �������� � ������ �����������"

	void FillList(int sel);				// ���������� ������ �����������

	DECLARE_MESSAGE_MAP()
};

#endif
