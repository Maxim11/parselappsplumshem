#include "stdafx.h"

#include "BranchInfo.h"
#include "SampleData.h"



void ReferenceData::Copy(ReferenceData& ref)
{
	Name = ref.Name;
	Value = ref.Value;
	bNoValue = ref.bNoValue;
}

void Spectrum::Copy(Spectrum& spec)
{
	Num = spec.Num;
	bUse = spec.bUse;
	DateTime = spec.DateTime;
	Data.clear();
	copy(spec.Data.begin(), spec.Data.end(), inserter(Data, Data.begin()));
}

CString Spectrum::IsUseStr()
{
	CString s = (bUse) ? ParcelLoadString(STRID_FIELD_YES) : ParcelLoadString(STRID_FIELD_NO);
	return s;
}

CSampleData::CSampleData(HTREEITEM hParent, HTREEITEM hTrans)
{
	Name = ParcelLoadString(STRID_DEFNAME_SAM);
	hParentItem = hParent;
	hTransItem = hTrans;

	CProjectData* ParentPrj = GetProjectByHItem(hParentItem);

	PrjName = ParentPrj->GetName();

	DevNumber = GetDeviceByHItem(ParentPrj->GetParentHItem())->GetNumber();

	Iden2 = cEmpty;
	Iden3 = cEmpty;

	int i;
	for(i=0; i<ParentPrj->GetNParams(); i++)
		AddReferenceData(ParentPrj->GetParam(i)->GetName());
}

CSampleData::~CSampleData()
{
	RefData.clear();
	SpecData.clear();
}

void CSampleData::Copy(CSampleData& data, int withoutspectra)
{
	int i;

	hParentItem = data.GetParentHItem();

	Name = data.GetName();

	Iden2 = data.GetIden2();
	Iden3 = data.GetIden3();

	RefData.clear();
	for(i=0; i<data.GetNReferenceData(); i++)
	{
		ReferenceData *OldRef = data.GetReferenceData(i);
		ReferenceData NewRef(OldRef->Name);
		NewRef.Copy(*OldRef);
		RefData.push_back(NewRef);
	}

	SpecData.clear();
	if(!withoutspectra)
	{
		for(i=0; i<data.GetNSpectra(); i++)
		{
			Spectrum *OldSpec = data.GetSpectrumByInd(i);
			Spectrum NewSpec;
			NewSpec.Copy(*OldSpec);
			SpecData.push_back(NewSpec);
		}
	}
}

HTREEITEM CSampleData::GetParentHItem()
{
	return hParentItem;
}

void CSampleData::SetParentHItem(HTREEITEM item)
{
	hParentItem = item;
}

HTREEITEM CSampleData::GetTransHItem()
{
	return hTransItem;
}

void CSampleData::SetHTransItem(HTREEITEM item)
{
	hTransItem = item;
}

CString CSampleData::GetName()
{
	return Name;
}

void CSampleData::SetName(CString name)
{
	Name = name;
}

CString CSampleData::GetIden2()
{
	return Iden2;
}

void CSampleData::SetIden2(CString str)
{
	Iden2 = str;
}

CString CSampleData::GetIden3()
{
	return Iden3;
}

void CSampleData::SetIden3(CString str)
{
	Iden3 = str;
}

int CSampleData::GetNReferenceData()
{
	return int(RefData.size());
}

ReferenceData* CSampleData::GetReferenceData(CString name)
{
	ItRef it;
	for(it=RefData.begin(); it!=RefData.end(); ++it)
		if(!(*it).Name.Compare(name))
			return &(*it);

	return NULL;
}

ReferenceData* CSampleData::GetReferenceData(int ind)
{
	if(ind < 0 || ind >= int(RefData.size()))
		return NULL;

	return &(RefData[ind]);
}

CString CSampleData::GetReferenceDataStr(CString name)
{
	CString s = cEmpty;

	ReferenceData *Ref = GetReferenceData(name);
	if(Ref == NULL)
		return cEmpty;

	if(Ref->bNoValue)
		s = ParcelLoadString(STRID_FIELD_NODATA);
	else
	{
		CProjectData* ParentPrj = GetProjectByHItem(GetParentHItem());
		if(ParentPrj == NULL)
			return cEmpty;

		s.Format(ParentPrj->GetParam(Ref->Name)->GetFormat(), Ref->Value);
	}

	return s;
}

CString CSampleData::GetReferenceDataStr(int ind)
{
	CString s = cEmpty;

	ReferenceData *Ref = GetReferenceData(ind);
	if(Ref == NULL)
		return cEmpty;

	if(Ref->bNoValue)
	{
		s = ParcelLoadString(STRID_FIELD_NODATA);
	}
	else
	{
		CProjectData* ParentPrj = GetProjectByHItem(GetParentHItem());
		if(ParentPrj == NULL)
			return cEmpty;

		s.Format(ParentPrj->GetParam(Ref->Name)->GetFormat(), Ref->Value);
	}

	return s;
}

bool CSampleData::AddReferenceData(CString name)
{
	ReferenceData *Ref = GetReferenceData(name);
	if(Ref != NULL)
		return false;

	ReferenceData Data(name);
	RefData.push_back(Data);

	return true;
}

bool CSampleData::SetReferenceValue(CString name, double value)
{
	ReferenceData *Ref = GetReferenceData(name);
	if(Ref == NULL)
		return false;

	Ref->Value = value;
	Ref->bNoValue = false;

	return true;
}

bool CSampleData::SetReferenceValue(int ind, double value)
{
	ReferenceData *Ref = GetReferenceData(ind);
	if(Ref == NULL)
		return false;

	Ref->Value = value;
	Ref->bNoValue = false;

	return true;
}

bool CSampleData::SetReferenceAsNoValue(CString name)
{
	ReferenceData *Ref = GetReferenceData(name);
	if(Ref == NULL)
		return false;

	Ref->Value = 0.;
	Ref->bNoValue = true;

	return true;
}

bool CSampleData::SetReferenceAsNoValue(int ind)
{
	ReferenceData *Ref = GetReferenceData(ind);
	if(Ref == NULL)
		return false;

	Ref->Value = 0.;
	Ref->bNoValue = true;

	return true;
}

bool CSampleData::DeleteReferenceData(CString name)
{
	ItRef it;
	for(it=RefData.begin(); it!=RefData.end(); ++it)
		if(!(*it).Name.Compare(name))
		{
			RefData.erase(it);
			return true;
		}

	return false;
}

bool CSampleData::DeleteReferenceData(int ind)
{
	if(ind < 0 || ind >= int(RefData.size()))
		return false;

	ItRef it = RefData.begin() + ind;
	RefData.erase(it);

	return true;
}

int CSampleData::GetNSpectra()
{
	return int(SpecData.size());
}

int CSampleData::GetNSpectraUsed()
{
	int nUses = 0;
	ItSpc it;
	for(it=SpecData.begin(); it!=SpecData.end(); ++it)
		if((*it).bUse)
			nUses++;

	return nUses;
}

bool CSampleData::IsSpectrumUsed(int num)
{
	ItSpc it;
	for(it=SpecData.begin(); it!=SpecData.end(); ++it)
		if((*it).Num == num && (*it).bUse)
			return true;

	return false;
}


Spectrum* CSampleData::GetSpectrumByNum(int num)
{
	ItSpc it;
	for(it=SpecData.begin(); it!=SpecData.end(); ++it)
		if((*it).Num == num)
			return &(*it);

	return NULL;
}

Spectrum* CSampleData::GetSpectrumByInd(int ind)
{
	if(ind < 0 || ind >= int(SpecData.size()))
		return NULL;

	int i;
	ItSpc it;
	for(i=0, it=SpecData.begin(); i<ind; i++)
		++it;

	return &(*it);
}

bool CSampleData::AddSpectrum(Spectrum* NewSpec)
{
	CTransferData* Trans = GetTransferByHItem(hTransItem);
	CProjectData* Prj = GetProjectByHItem(hParentItem);
	if(Prj == NULL || Trans == NULL)
		return false;

	if(Prj->GetNSpecPoints() != int(NewSpec->Data.size()))
		return false;

	ItSpc it;
	for(it=SpecData.begin(); it!=SpecData.end(); ++it)
		if((*it).Num == NewSpec->Num)
			return false;

	prdb::PRSpectrum Obj3;
	Obj3.SpecNum = NewSpec->Num;
	Obj3.IsUse = NewSpec->bUse;
	Obj3.DateCreated = NewSpec->DateTime;
	Obj3.Vc.clear();
	copy(NewSpec->Data.begin(), NewSpec->Data.end(), inserter(Obj3.Vc, Obj3.Vc.begin()));

	if(!GetDatabase()->SpectrumAddEx(Prj->GetDevNumber(), Prj->GetName(), Trans->GetName(), Name, &Obj3))
		return false;

	SpecData.push_back((*NewSpec));

	return true;
}

bool CSampleData::DeleteSpectrum(int num)
{
	CTransferData* Trans = GetTransferByHItem(hTransItem);
	CProjectData* Prj = GetProjectByHItem(hParentItem);
	if(Prj == NULL || Trans == NULL)
		return false;

	ItSpc it;
	for(it=SpecData.begin(); it!=SpecData.end(); ++it)
		if((*it).Num == num)
			break;

	if(it == SpecData.end())
		return false;

	if(GetDatabase()->IsSpectrumExist(Prj->GetDevNumber(), Prj->GetName(), Trans->GetName(), Name, it->Num))
		if(!GetDatabase()->SpectrumDelete(Prj->GetDevNumber(), Prj->GetName(), Trans->GetName(), Name, it->Num))
			return false;

	SpecData.erase(it);

	return true;
}

bool CSampleData::IsSpecDateExist(CTime date)
{
	ItSpc it;
	for(it=SpecData.begin(); it!=SpecData.end(); ++it)
		if(it->DateTime == date)
			return true;

	return false;
}

CTime CSampleData::GetLastSpecDate()
{
	CTime t=0;
	ItSpc it;
	for(it=SpecData.begin(); it!=SpecData.end(); ++it)
	{
		if(it->DateTime > t)
		{
			t = it->DateTime;
		}
	}
	return t;
}


int CSampleData::GetSpecNumMax()
{
	int N = 0;
	ItSpc it;
	for(it=SpecData.begin(); it!=SpecData.end(); ++it)
		if(it->Num > N)
			N = it->Num;

	return N;
}

void CSampleData::GetDataForBase(prdb::PRSample& Obj)
{
	Obj.Sample1 = Name;
	Obj.Sample2 = Iden2;
	Obj.Sample3 = Iden3;
}

void CSampleData::SetDataFromBase(prdb::PRSample& Obj)
{
	Name = Obj.Sample1;
	Iden2 = Obj.Sample2;
	Iden3 = Obj.Sample3;
}

void CSampleData::GetRefDataForBase(CString name, prdb::PRRefData& Obj)
{
	ReferenceData* Ref = GetReferenceData(name);

	Obj.IdxName = name;
	Obj.IsDataExists = Ref->bNoValue;
	Obj.RefData = Ref->Value;
}

void CSampleData::GetSpecDataForBase(prdb::PRSpectrum& Obj)
{
	Spectrum* Spec = GetSpectrumByNum(Obj.SpecNum);
	if(Spec == NULL)
		return;

	Obj.IsUse = Spec->bUse;
	Obj.DateCreated = Spec->DateTime;
	Obj.Vc.clear();
	copy(Spec->Data.begin(), Spec->Data.end(), inserter(Obj.Vc, Obj.Vc.begin()));
}

bool CSampleData::LoadDatabase()
{
double tll1 = GetLLTimerSec();

	VRef NewRefData;

	CTransferData* Trans = GetTransferByHItem(hTransItem);
	CString TransName = Trans->GetName();

	int nSampleID;
	if(!GetDatabase()->SampleGetID(DevNumber, PrjName, TransName, Name, &nSampleID))
		return false;
/*
	if(!GetDatabase()->RefDataRead(DevNumber, PrjName, TransName, Name))
		return false;
*/
	if(Name != cArtSam)
	{
		if(!GetDatabase()->RefDataReadBySampleID(nSampleID))
			return false;

		while(!GetDatabase()->IsEOF())
		{
			prdb::PRRefData Obj;
			if(!GetDatabase()->RefDataGet(&Obj))
				break;

			ReferenceData Ref(Obj.IdxName);
			Ref.bNoValue = Obj.IsDataExists;
			Ref.Value = Obj.RefData;
			NewRefData.push_back(Ref);
			GetDatabase()->MoveNext();
		}

		RefData.clear();
		copy(NewRefData.begin(), NewRefData.end(), inserter(RefData, RefData.begin()));
	}

	GetDatabase()->Free();


	LoadSpectraInfo(nSampleID);
double dtll = GetLLTimerSec()-tll1;
//TRACE1("%f\n",dtll);
	return true;
}

bool CSampleData::LoadSpectraInfo(int nSampleID)
{
	CTransferData* Trans = GetTransferByHItem(hTransItem);
	CString TransName = Trans->GetName();

	VSpc NewSpecData;

//double tll1 = GetLLTimerSec();
	VInt vSpecIDs;
//	if(!GetDatabase()->GetSampleSpectraID(DevNumber, PrjName, TransName, Name, vSpecIDs))
	if(!GetDatabase()->GetSampleSpectraID(nSampleID, vSpecIDs))
		return false;
//double dtll = GetLLTimerSec()-tll1;
//TRACE1("%f\n",dtll);


	ItInt it;
	for(it=vSpecIDs.begin(); it!=vSpecIDs.end(); ++it)
	{
		Spectrum NewSpec;
		prdb::PRSpectrumLite dataLite;

		int nID = (*it);
		if(!GetDatabase()->SpectrumGetByID(nID, &dataLite))
			return false;
		NewSpec.Num = dataLite.SpecNum;
		NewSpec.bUse = dataLite.IsUse;
		NewSpec.DateTime = dataLite.DateCreated;

		NewSpecData.push_back(NewSpec);
	}
	GetDatabase()->Free();

	SpecData.clear();
	copy(NewSpecData.begin(), NewSpecData.end(), inserter(SpecData, SpecData.begin()));
	return true;
}

/*
bool CSampleData::LoadSpectraInfo()
{
double tll1 = GetLLTimerSec();

	CTransferData* Trans = GetTransferByHItem(hTransItem);
	CString TransName = Trans->GetName();

	VSpc NewSpecData;

	VInt num;
	if(!GetDatabase()->SpectrumLinkGet(DevNumber, PrjName, TransName, Name, &num))
		return false;

	VInt vSpecIDs;
	if(!GetDatabase()->GetSampleSpectraID(DevNumber, PrjName, TransName, Name, vSpecIDs))
		return false;


	ItInt it;
	for(it=num.begin(); it!=num.end(); ++it)
	{
		Spectrum NewSpec;
		prdb::PRSpectrumLite dataLite;

		dataLite.SpecNum = (*it);
		if(!GetDatabase()->SpectrumGet(DevNumber, PrjName, TransName, Name, &dataLite))
			return false;
		NewSpec.Num = dataLite.SpecNum;
		NewSpec.bUse = dataLite.IsUse;
		NewSpec.DateTime = dataLite.DateCreated;

		NewSpecData.push_back(NewSpec);
	}
	GetDatabase()->Free();

	SpecData.clear();
	copy(NewSpecData.begin(), NewSpecData.end(), inserter(SpecData, SpecData.begin()));

double dtll = GetLLTimerSec()-tll1;
TRACE1("%f\n",dtll);

	return true;
}
*/

bool CSampleData::LoadSpectraData(VInt* nums/* = NULL*/)
{
	CTransferData* Trans = GetTransferByHItem(hTransItem);
	CString TransName = Trans->GetName();

	VInt Nums;
	if(nums == NULL)
	{
		ItSpc it2;
		for(it2=SpecData.begin(); it2!=SpecData.end(); ++it2)
			Nums.push_back(it2->Num);
	}
	else
		copy(nums->begin(), nums->end(), inserter(Nums, Nums.begin()));

	ItInt it;
	for(it=Nums.begin(); it!=Nums.end(); ++it)
	{
		Spectrum* Spec = GetSpectrumByNum(*it);
		if(Spec == NULL)
			return false;
		if(Spec->Data.size() > 0)
			continue;

		prdb::PRSpectrum data;

		data.SpecNum = (*it);
		if(!GetDatabase()->SpectrumGet(DevNumber, PrjName, TransName, Name, &data))
		{
			FreeSpectraData();
			return false;
		}

		copy(data.Vc.begin(), data.Vc.end(), inserter(Spec->Data, Spec->Data.begin()));
	}
	GetDatabase()->Free();

	return true;
}

void CSampleData::FreeSpectraData()
{
	ItSpc it;
	for(it=SpecData.begin(); it!=SpecData.end(); ++it)
		it->Data.clear();
}
