////////////////////////////////////////////////////////////////////////////
//	Author      : Ignatiev Gena 2006
//	Email       : gena.ignatev@paloma.spbu.ru
//	Module      : RegHelper.h
//	Description : 
////////////////////////////////////////////////////////////////////////////
#ifndef _REGISTRY_HELPER_FUNCTION_INCLUDED_H__
#define _REGISTRY_HELPER_FUNCTION_INCLUDED_H__

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include <string>
#include <sstream>
#include <vector>
#include <list>
#include <memory>
#include <iomanip>
#include <limits>

namespace registry
{

#ifdef _UNICODE
	typedef wchar_t					tchar;
	typedef std::wstring			tstring;
	typedef std::wistringstream		tistringstream;
	typedef std::wostringstream		tostringstream;
	typedef std::wstringstream		tstringstream;
#else
	typedef char					tchar;
	typedef std::string				tstring;
	typedef std::istringstream		tistringstream;
	typedef std::ostringstream		tostringstream;
	typedef std::stringstream		tstringstream;
#endif

	typedef unsigned long	dword;


//namespace helper
//{

/**********************************************************************
*                            -= MakeBinary =-
**********************************************************************/
struct BinaryData
{
	std::vector<BYTE>	bytes;
};
//-------------------------------------------------------------------------
std::auto_ptr<BinaryData>	MakeBinary( const LPBYTE ptr, size_t length );
std::auto_ptr<BinaryData>	MakeBinary( const std::string& data );
std::auto_ptr<BinaryData>	MakeBinary( const std::wstring& data );
std::auto_ptr<BinaryData>	MakeBinary( LPCTSTR lpszData );

tstring BinaryToString( const std::vector<BYTE>& bytes );

/**********************************************************************
*                            -= MakeString =-
**********************************************************************/
template < typename T >
tstring MakeString( T v )
{
	tostringstream ss;
	bool is_spec = std::numeric_limits<T>::is_specialized;
	if( is_spec ) 
		ss << std::setprecision( std::numeric_limits<T>::digits );
	ss << v;
	return ss.str();
}

inline
tstring MakeString( tstring data ) { return data; }

inline
tstring MakeString( LPCTSTR lpszData ) { return tstring(lpszData); }
//-------------------------------------------------------------------------



/**********************************************************************
*                            -= MakeMulti =-
**********************************************************************/
struct MultiData
{
	std::vector<tstring> strings;
};
//-------------------------------------------------------------------------
std::auto_ptr<MultiData>	MakeMultiFromBuffer( const BYTE* buf, size_t len );

std::auto_ptr<MultiData>	MakeMulti( const std::list<tstring>& data );
std::auto_ptr<MultiData>	MakeMulti( const std::vector<tstring>& data );

template < typename T >
std::auto_ptr<MultiData>	MakeMulti( T v )
{
	std::auto_ptr<MultiData> p( new MultiData() );
	p->strings.push_back( MakeString(v) );
	return p;
}

template < typename T1, typename T2 >
std::auto_ptr<MultiData>	MakeMulti( T1 v1, T2 v2 )
{
	std::auto_ptr<MultiData> p( new MultiData() );
	p->strings.push_back( MakeString(v1) );
	p->strings.push_back( MakeString(v2) );
	return p;
}

template < typename T1, typename T2, typename T3 >
std::auto_ptr<MultiData>	MakeMulti( T1 v1, T2 v2, T3 v3 )
{
	std::auto_ptr<MultiData> p( new MultiData() );
	p->strings.push_back( MakeString(v1) );
	p->strings.push_back( MakeString(v2) );
	p->strings.push_back( MakeString(v3) );
	return p;
}

template < typename T1, typename T2, typename T3, typename T4 >
std::auto_ptr<MultiData>	MakeMulti( T1 v1, T2 v2, T3 v3, T4 v4 )
{
	std::auto_ptr<MultiData> p( new MultiData() );
	p->strings.push_back( MakeString(v1) );
	p->strings.push_back( MakeString(v2) );
	p->strings.push_back( MakeString(v3) );
	p->strings.push_back( MakeString(v4) );
	return p;
}

template < typename T1, typename T2, typename T3, typename T4, typename T5 >
std::auto_ptr<MultiData>	MakeMulti( T1 v1, T2 v2, T3 v3, T4 v4, T5 v5 )
{
	std::auto_ptr<MultiData> p( new MultiData() );
	p->strings.push_back( MakeString(v1) );
	p->strings.push_back( MakeString(v2) );
	p->strings.push_back( MakeString(v3) );
	p->strings.push_back( MakeString(v4) );
	p->strings.push_back( MakeString(v5) );
	return p;
}

template < typename T1, typename T2, typename T3, typename T4, typename T5
		, typename T6 >
std::auto_ptr<MultiData>	MakeMulti( T1 v1, T2 v2, T3 v3, T4 v4, T5 v5, T6 v6 )
{
	std::auto_ptr<MultiData> p( new MultiData() );
	p->strings.push_back( MakeString(v1) );
	p->strings.push_back( MakeString(v2) );
	p->strings.push_back( MakeString(v3) );
	p->strings.push_back( MakeString(v4) );
	p->strings.push_back( MakeString(v5) );
	p->strings.push_back( MakeString(v6) );
	return p;
}

template < typename T1, typename T2, typename T3, typename T4, typename T5
		, typename T6 , typename T7 >
std::auto_ptr<MultiData>	MakeMulti( T1 v1, T2 v2, T3 v3, T4 v4, T5 v5, T6 v6, T7 v7 )
{
	std::auto_ptr<MultiData> p( new MultiData() );
	p->strings.push_back( MakeString(v1) );
	p->strings.push_back( MakeString(v2) );
	p->strings.push_back( MakeString(v3) );
	p->strings.push_back( MakeString(v4) );
	p->strings.push_back( MakeString(v5) );
	p->strings.push_back( MakeString(v6) );
	p->strings.push_back( MakeString(v7) );
	return p;
}

template < typename T1, typename T2, typename T3, typename T4, typename T5
		, typename T6 , typename T7, typename T8 >
std::auto_ptr<MultiData>	MakeMulti( T1 v1, T2 v2, T3 v3, T4 v4, T5 v5, T6 v6, T7 v7, T8 v8 )
{
	std::auto_ptr<MultiData> p( new MultiData() );
	p->strings.push_back( MakeString(v1) );
	p->strings.push_back( MakeString(v2) );
	p->strings.push_back( MakeString(v3) );
	p->strings.push_back( MakeString(v4) );
	p->strings.push_back( MakeString(v5) );
	p->strings.push_back( MakeString(v6) );
	p->strings.push_back( MakeString(v7) );
	p->strings.push_back( MakeString(v8) );
	return p;
}

template < typename T1, typename T2, typename T3, typename T4, typename T5
		, typename T6 , typename T7, typename T8, typename T9 >
std::auto_ptr<MultiData>	MakeMulti( T1 v1, T2 v2, T3 v3, T4 v4, T5 v5, T6 v6, T7 v7, T8 v8, T9 v9 )
{
	std::auto_ptr<MultiData> p( new MultiData() );
	p->strings.push_back( MakeString(v1) );
	p->strings.push_back( MakeString(v2) );
	p->strings.push_back( MakeString(v3) );
	p->strings.push_back( MakeString(v4) );
	p->strings.push_back( MakeString(v5) );
	p->strings.push_back( MakeString(v6) );
	p->strings.push_back( MakeString(v7) );
	p->strings.push_back( MakeString(v8) );
	p->strings.push_back( MakeString(v9) );
	return p;
}

template < typename T1, typename T2, typename T3, typename T4, typename T5
		, typename T6 , typename T7, typename T8, typename T9, typename T10 >
std::auto_ptr<MultiData>	MakeMulti( T1 v1, T2 v2, T3 v3, T4 v4, T5 v5, T6 v6, T7 v7, T8 v8, T9 v9, T10 v10 )
{
	std::auto_ptr<MultiData> p( new MultiData() );
	p->strings.push_back( MakeString(v1) );
	p->strings.push_back( MakeString(v2) );
	p->strings.push_back( MakeString(v3) );
	p->strings.push_back( MakeString(v4) );
	p->strings.push_back( MakeString(v5) );
	p->strings.push_back( MakeString(v6) );
	p->strings.push_back( MakeString(v7) );
	p->strings.push_back( MakeString(v8) );
	p->strings.push_back( MakeString(v9) );
	p->strings.push_back( MakeString(v10) );
	return p;
}



//-------------------------------------------------------------------------
tstring MultiToString( const MultiData& mul );
tstring MultiToString( const std::vector<tstring>& mul );


/**********************************************************************
*                            -= MakeDWORD =-
**********************************************************************/
inline 
dword	MakeDWORD( char ch ) { return static_cast<dword>(ch); }

inline
dword	MakeDWORD( unsigned char uch ) { return static_cast<dword>(uch); }

inline
dword	MakeDWORD( short sh ) { return static_cast<dword>(sh); }

inline
dword	MakeDWORD( unsigned short ush ) { return static_cast<dword>(ush); }

inline
dword	MakeDWORD( int n ) { return static_cast<dword>(n); }

inline
dword	MakeDWORD( unsigned int un ) { return static_cast<dword>(un); }

inline
dword	MakeDWORD( long l ) { return static_cast<dword>(l); }

inline
dword	MakeDWORD( unsigned long ul ) { return static_cast<dword>(ul); }








//} //namespace helper




}	//namespace registry


#endif // _REGISTRY_HELPER_FUNCTION_INCLUDED_H__
