////////////////////////////////////////////////////////////////////////////
//	Author      : Ignatiev Gena 2006
//	Email       : gena.ignatev@paloma.spbu.ru
//	Module      : RegEntry.h
//	Description : 
////////////////////////////////////////////////////////////////////////////
#ifndef _REGISTRY_ENTRY_INCLUDED_H__
#define _REGISTRY_ENTRY_INCLUDED_H__

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


#include "RegHelper.h"



namespace registry
{



class RegKey;
/**********************************************************************
*                            -= RegEntry =-
*	Description : �������� ��� ����� � �������
**********************************************************************/
class RegEntry
{
	friend class RegKey;

public:

	// .ctor
	explicit RegEntry( const tstring& name );
	virtual ~RegEntry();

	RegEntry( const RegEntry& other );
	RegEntry&	operator=( const RegEntry& other );

	RegEntry&	operator=( dword val );
	RegEntry&	operator=( tstring val );
	RegEntry&	operator=( LPCTSTR val );
	RegEntry&	operator=( const MultiData& val );
	RegEntry&	operator=( const BinaryData& val );
	RegEntry&	operator=( std::auto_ptr<MultiData> pval );
	RegEntry&	operator=( std::auto_ptr<BinaryData> pval );

	tstring		GetName()const { return m_name; }
	dword		GetType()const { return m_type; }
	bool		Write( HKEY hkey )const;
	bool		Delete();

	tstring				ToString()const;
	std::vector<BYTE>	ToBinary()const;

	operator DWORD()const;
	operator LPCTSTR()const;
	operator tstring()const;

	void	SetModified( bool isModified ) { m_isModified = isModified; }
	bool	IsModified()const		{ return m_isModified; }
	bool	IsExist()const			{ return (m_type != REG_NONE); }
	bool	IsString()const			{ return (m_type == REG_SZ); }
	bool	IsMultiString()const	{ return (m_type == REG_MULTI_SZ); }
	bool	IsDword()const			{ return (m_type == REG_DWORD); }
	bool	IsBinary()const			{ return (m_type == REG_BINARY); }

	template < typename TargetT >
		TargetT To()const; // cast function.

protected:

	RegEntry( const tstring& name, RegKey* pkey );

	template < typename TargetT >
		TargetT ToReal()const; // real cast function.

	void SetOwner( RegKey* key ) { m_owner = key; }



private:

	RegKey*			m_owner;
	mutable bool	m_isModified;
	dword			m_type;
	tstring			m_name;
	
	//data (one of them uses according type. Other are used as a cache)
	dword					m_dataD;	// DWORD data
	mutable tstring			m_dataS;	// STRING data
	std::vector<tstring>	m_dataM;	// MULTI_STRING data
	std::vector<BYTE>		m_dataB;	// BINARY data


};
//-------------------------------------------------------------------------

/**********************************************************************
*                            -= ����� ���� =-
**********************************************************************/

template <> inline
bool RegEntry::To<bool>()const
{ 
	if( !IsExist() )			return false;
	else if( IsDword() )		return (0 != m_dataD);
	else if( IsString() )		return (!m_dataS.empty());
	else if( IsMultiString() )	return (!m_dataM.empty());
	else if( IsBinary() )		return (!m_dataB.empty());
	return false;
}
//-------------------------------------------------------------------------
#define ALLOW_CAST__REGENTRY_TO( targetT )	template<> inline\
											targetT RegEntry::To<targetT>()const\
											{ dword v = (DWORD)*const_cast<RegEntry*>(this);\
											return static_cast<targetT>(v); }
			

ALLOW_CAST__REGENTRY_TO( char )
ALLOW_CAST__REGENTRY_TO( unsigned char )
ALLOW_CAST__REGENTRY_TO( short )
ALLOW_CAST__REGENTRY_TO( unsigned short )
ALLOW_CAST__REGENTRY_TO( int )
ALLOW_CAST__REGENTRY_TO( unsigned int )
ALLOW_CAST__REGENTRY_TO( long )
ALLOW_CAST__REGENTRY_TO( unsigned long )

//-------------------------------------------------------------------------




/**********************************************************************
*                            -= ������������ ���� =-
**********************************************************************/
template < typename TargetT > inline
TargetT RegEntry::ToReal()const
{
	if( !IsExist() ) return TargetT();
	else if( IsString() || IsMultiString() )
	{
//		tstring s = (tstring)*const_cast<RegEntry*>(this);
//		tstring s = (*const_cast<RegEntry*>(this)).ToString();
		tstring s = *this;
		tistringstream ss(s);
		TargetT v;
		ss >> std::setprecision( std::numeric_limits<TargetT>::digits10+1 ) >> v;
		return v;
	}
	else if( IsDword() ) return static_cast<TargetT>( m_dataD );
	else if( IsBinary() && m_dataB.size() >= sizeof(TargetT) )
	{
		return *( const TargetT*)( &m_dataB[0] );
	}
	return TargetT();
}

template <> inline
float RegEntry::To<float>()const  { return ToReal<float>(); }

template <> inline
double RegEntry::To<double>()const  { return ToReal<double>(); }

template <> inline
__int64 RegEntry::To<__int64>()const  { return ToReal<__int64>(); }
//-------------------------------------------------------------------------






}	//namespace registry


#endif // _REGISTRY_ENTRY_INCLUDED_H__
