////////////////////////////////////////////////////////////////////////////
//	Author      : Ignatiev Gena 2006
//	Email       : gena.ignatev@paloma.spbu.ru
//	Module      : RegHelper.cpp
//	Description : 
////////////////////////////////////////////////////////////////////////////
#include "../stdafx.h"


#include "RegHelper.h"

using namespace std;

namespace registry
{

	/**********************************************************************
	*                            -= MakeBinary =-
	**********************************************************************/
	std::auto_ptr<BinaryData>	MakeBinary(const LPBYTE ptr, size_t length)
	{
		auto_ptr<BinaryData> p(new BinaryData());
		p->bytes.resize(length);
		std::copy(ptr, ptr + length, p->bytes.begin());
		return p;
	}
	//-------------------------------------------------------------------------
	std::auto_ptr<BinaryData>	MakeBinary(const std::string& data)
	{
		size_t size = data.size() + 1;
		const LPBYTE ptr = (const LPBYTE)data.c_str();
		return MakeBinary(ptr, size);
	}
	//-------------------------------------------------------------------------
	std::auto_ptr<BinaryData>	MakeBinary(const std::wstring& data)
	{
		size_t size = (data.size() + 1) * sizeof(wchar_t);
		const LPBYTE ptr = (const LPBYTE)data.c_str();
		return MakeBinary(ptr, size);
	}
	//-------------------------------------------------------------------------
	std::auto_ptr<BinaryData>	MakeBinary(LPCTSTR lpszData)
	{
		size_t size = (_tcslen(lpszData) + 1) * sizeof(tchar);
		const LPBYTE ptr = (const LPBYTE)lpszData;
		return MakeBinary(ptr, size);
	}
	//-------------------------------------------------------------------------
	tstring BinaryToString(const std::vector<BYTE>& bytes)
	{
		tstring str = _T("");
		tchar buf[64];
		const size_t size = bytes.size();
		for (size_t i = 0; i < size; ++i)
		{
			if (i) str += _T(" ");
			if (0 > _stprintf(buf, _T("0x%02X"), bytes[i])) return _T("");
			str += buf;
		}
		return str;
	}
	//-------------------------------------------------------------------------


	/**********************************************************************
	*                            -= MakeMulti =-
	**********************************************************************/
	std::auto_ptr<MultiData>	MakeMultiFromBuffer(const BYTE* buf, size_t len)
	{
		std::auto_ptr<MultiData> p(new MultiData());

		if (buf && len)
		{
			const tchar* ptr = (const tchar*)buf;
			const tchar* end = ptr + len / sizeof(tchar);
			const tchar* eos = ptr; // end of string

			while (ptr != end)
			{
				while (*ptr == _T('\0') && ptr != end) ++ptr;
				if (ptr == end) break;

				// find eos
				eos = ptr;
				while (*eos && eos != end) ++eos;
				if (eos == end) break;

				p->strings.push_back(tstring(ptr));

				ptr = ++eos;
			}
		}

		return p;
	}
	//-------------------------------------------------------------------------
	std::auto_ptr<MultiData>	MakeMulti(const std::list<tstring>& data)
	{
		std::auto_ptr<MultiData> p(new MultiData());
		p->strings.resize(data.size());
		size_t i = 0;
		for (std::list<tstring>::const_iterator iter = data.begin(); iter != data.end(); ++iter, ++i)
			p->strings[i] = *iter;

		return p;
	}
	//-------------------------------------------------------------------------
	std::auto_ptr<MultiData>	MakeMulti(const std::vector<tstring>& data)
	{
		std::auto_ptr<MultiData> p(new MultiData());
		size_t size = data.size();
		p->strings.resize(size);
		for (size_t i = 0; i < size; ++i)
			p->strings[i] = data[i];

		return p;
	}
	//-------------------------------------------------------------------------
	tstring MultiToString(const MultiData& mul)
	{
		return MultiToString(mul.strings);
	}
	//-------------------------------------------------------------------------
	tstring MultiToString(const std::vector<tstring>& mul)
	{
		tstring str = _T("");
		const size_t size = mul.size();
		for (size_t i = 0; i < size; ++i)
		{
			str += mul[i] + tstring(1, _T('\0'));
		}
		str += _T("\0");
		return str;
	}
	//-------------------------------------------------------------------------

}
	//namespace registry