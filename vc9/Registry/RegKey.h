////////////////////////////////////////////////////////////////////////////
//	Author      : Ignatiev Gena 2006
//	Email       : gena.ignatev@paloma.spbu.ru
//	Module      : RegKey.h
//	Description : 
////////////////////////////////////////////////////////////////////////////
#ifndef _REGISTRY_KEY_INCLUDED_H__
#define _REGISTRY_KEY_INCLUDED_H__

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include <map>
#include "RegEntry.h"


namespace registry
{

/**********************************************************************
*                            -= RegKey =-
*	Description : �������� ��� ����� � �������
**********************************************************************/
class RegKey
{
	friend class RegEntry;
	enum { eMAX_REG_VALUE_SIZE = 2048 };

public:
	typedef std::map< tstring, RegEntry* >		EntryMap;
	typedef EntryMap::iterator					EntryIter;
	typedef EntryMap::const_iterator			EntryConstIter;
	typedef std::pair<EntryIter, bool>			EntryIns;

public:
	explicit	RegKey( bool isCreated = true );
	virtual		~RegKey();

	RegKey( const RegKey& other );
	RegKey& operator=( const RegKey& other );


	RegEntry&	operator[]( const tchar* name );
	RegEntry&	operator[]( const tstring& name );

	bool		Open( const tchar*	keypath
					, HKEY			hRoot = REGROOT
					, DWORD			dwAccess = KEY_QUERY_VALUE | KEY_SET_VALUE );

	bool		IsOpened()const		{ return (m_hkey != NULL); }
	LPCTSTR		GetKeyPath()const	{ return m_path.c_str(); }


	std::auto_ptr<RegKey>	OpenSubKey( const tchar* name, bool isCreated = true, DWORD dwAccess = KEY_QUERY_VALUE | KEY_SET_VALUE );

	void		Close();
	bool		Refresh();	
	void		Write()const;

	//HKEY		GetHKEY()const { return m_hkey; }

	static bool	IsKeyExist( const tchar* keypath, HKEY hRoot = REGROOT );
	bool		IsSubKeyExist( const tchar* subkey )const;	

	void		DeleteMe( bool isRecursively = false );
	static void DeleteKey( const tchar*	keypath, HKEY hRoot = REGROOT, bool isRecursively = false );

	size_t		Count()const		{ return m_entries.size(); }
	bool		IsReadOnly()const	{ return m_isReadOnly; }

	bool		GetSubKeyList( /*OUT*/ std::list<tstring>& subkeys, bool asFullSubkeyPath = false );
	const EntryMap& GetEntryMap()const  { return m_entries; }

protected:

	void		ClearInnerData();

private:
	bool		m_isReadOnly;
	bool		m_isCreated;
	HKEY		m_hkey;
	HKEY		m_hroot;
	tstring		m_path;
	EntryMap	m_entries;

};
//-------------------------------------------------------------------------


}	//namespace registry


#endif // _REGISTRY_KEY_INCLUDED_H__
