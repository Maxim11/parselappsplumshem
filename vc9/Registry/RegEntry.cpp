////////////////////////////////////////////////////////////////////////////
//	Author      : Ignatiev Gena 2006
//	Email       : gena.ignatev@paloma.spbu.ru
//	Module      : RegEntry.cpp
//	Description : 
////////////////////////////////////////////////////////////////////////////
#include "../stdafx.h"


#include "RegEntry.h"
#include "RegKey.h"


using namespace std;

namespace registry
{

/**********************************************************************
*                            -= RegEntry =-
*	Description : �������� ��� ����� � �������
**********************************************************************/
// protected .ctor
RegEntry::RegEntry( const tstring& name )
	: m_owner( NULL )
	, m_isModified( false )
	, m_type( REG_NONE )
	, m_name( name )
	, m_dataD( 0 )
	, m_dataS( _T("") )
{
}
//-------------------------------------------------------------------------
RegEntry::RegEntry( const tstring& name, RegKey* pkey )
	: m_owner( pkey )
	, m_isModified( false )
	, m_type( REG_NONE )
	, m_name( name )
	, m_dataD( 0 )
	, m_dataS( _T("") )
{

}
//-------------------------------------------------------------------------
/*virtual*/
RegEntry::~RegEntry()
{
}
//-------------------------------------------------------------------------
RegEntry::RegEntry( const RegEntry& other )
	: m_owner( other.m_owner )
	, m_isModified( other.m_isModified )
	, m_type( other.m_type )
	, m_name( other.m_name )
	, m_dataD( other.m_dataD )
	, m_dataS( other.m_dataS )
	, m_dataM( other.m_dataM )
	, m_dataB( other.m_dataB )
{
}
//-------------------------------------------------------------------------
RegEntry&	RegEntry::operator=( const RegEntry& other )
{
	if( this != &other )
	{
		m_owner			= other.m_owner;
		m_isModified	= other.m_isModified;
		m_type			= other.m_type;
		m_name			= other.m_name;
		m_dataD			= other.m_dataD;
		m_dataS			= other.m_dataS;
		m_dataM			= other.m_dataM;
		m_dataB			= other.m_dataB;
	}
	return *this;
}
//-------------------------------------------------------------------------
RegEntry&	RegEntry::operator=( dword val )
{
	if( !IsExist() )
	{
		m_type = REG_DWORD;
		m_dataD = val;
	}
	else if( IsDword() )		{ m_dataD = val; }
	else if( IsString() )		{ m_dataS = MakeString( val ); }
	else if( IsMultiString() )	{ m_dataM = MakeMulti( val )->strings; }
	else if( IsBinary() )		{ m_dataB = MakeBinary( (const LPBYTE)&val, sizeof(dword) )->bytes; }

	m_isModified = true;

	return *this;
}
//-------------------------------------------------------------------------
RegEntry&	RegEntry::operator=( tstring val )
{
	if( !IsExist() )
	{
		m_type = REG_SZ;
		m_dataS = val;
	}
	else if( IsDword() )
	{ 
		tstringstream ss(val);
		ss >> m_dataD; 
	}
	else if( IsString() )		{ m_dataS = val; }
	else if( IsMultiString() )	{ m_dataM = MakeMulti( val )->strings; }
	else if( IsBinary() )		{ m_dataB = MakeBinary( val )->bytes; }

	m_isModified = true;

	return *this;
}
//-------------------------------------------------------------------------
RegEntry&	RegEntry::operator=( LPCTSTR val )
{
	tstring v = val;
	return (*this = v);
}
//-------------------------------------------------------------------------
RegEntry&	RegEntry::operator=( const MultiData& val )
{
	// ������ �������������� ��� ������ �� multi
	m_type = REG_MULTI_SZ;
	m_dataM = val.strings;
	m_dataD = 0;
	m_dataS = _T("");
	m_dataB.clear();

	m_isModified = true;

	return *this;
}
//-------------------------------------------------------------------------
RegEntry& RegEntry::operator=( std::auto_ptr<MultiData> pval )
{
	if( pval.get() ) return (*this = *pval );
	return *this;
}
//-------------------------------------------------------------------------
RegEntry& RegEntry::operator=( const BinaryData& val )
{
	// ������ �������������� ��� ������ �� binary
	m_type = REG_BINARY;
	m_dataD = 0;
	m_dataS = _T("");
	m_dataM.clear();
	m_dataB = val.bytes;

	m_isModified = true;

	return *this;
}
//-------------------------------------------------------------------------
RegEntry& RegEntry::operator=( std::auto_ptr<BinaryData> pval )
{
	if( pval.get() ) return (*this = *pval );
	return *this;
}
//-------------------------------------------------------------------------
bool RegEntry::Write( HKEY hkey )const
{
	if( !hkey || !IsExist() ) return false;

	if( !m_isModified ) return true;

	tstring	val;
	const BYTE*	ptr = NULL;
	size_t		len = 0;

	if( IsDword() )
	{
		ptr = (const BYTE*) &m_dataD;
		len = sizeof(dword);
	}
	else if( IsString() )
	{
		ptr = (const BYTE*) m_dataS.c_str();
		len = (m_dataS.length()+1) * sizeof(tchar);
	}
	else if( IsMultiString() )
	{
		val = MultiToString( m_dataM );
		ptr = (const BYTE*) val.c_str();
		len = (val.length()+1) * sizeof(tchar);
	}
	else if( IsBinary() )
	{
		ptr = (const BYTE*)&m_dataB[0];
		len = m_dataB.size();
	}

	if( ERROR_SUCCESS == RegSetValueEx	( hkey
										, m_name.c_str()
										, NULL
										, m_type
										, ptr
										, static_cast<DWORD>(len) ) )
	{
		m_isModified = false;
		return true;
	}

	return false;
}
//-------------------------------------------------------------------------
bool RegEntry::Delete()
{
	if( !IsExist() ) return true;
	if( !m_owner ) return false;

	bool isOk = false;

	if( NULL == m_owner->m_hkey && !m_owner->m_path.empty() )
	{
		HKEY hkey = NULL;
		if( ERROR_SUCCESS != RegOpenKeyEx( m_owner->m_hroot, m_owner->m_path.c_str(), 0, KEY_SET_VALUE, &hkey ) )
			return false;

		isOk = ( ERROR_SUCCESS == RegDeleteValue( hkey, m_name.c_str() ) );
		RegCloseKey( hkey );
	}
	else if( m_owner->m_hkey )
	{
		isOk = ( ERROR_SUCCESS == RegDeleteValue( m_owner->m_hkey, m_name.c_str() ) );
	}
	
	if( isOk )
	{
		m_type = REG_NONE;
		m_dataD = 0;
		m_dataS = _T("");
		m_dataM.clear();
		m_dataB.clear();
	}
	return isOk;
}
//-------------------------------------------------------------------------
tstring RegEntry::ToString()const
{
	tstring ret;
	
	if( IsDword() )
	{
		ret = MakeString( m_dataD );
	}
	else if( IsString() )
	{
		ret = m_dataS;
	}
	else if( IsMultiString() )
	{
		MultiData md;
		md.strings = m_dataM;
		ret = MultiToString(md);
	}
	else if( IsBinary() )
	{
		ret = BinaryToString( m_dataB );
	}

	return ret;
}
//-------------------------------------------------------------------------
std::vector<BYTE> RegEntry::ToBinary()const
{
	 std::vector<BYTE> tmp;

	if( IsDword() )
	{
		tmp.resize( sizeof(dword) );
		const BYTE* ptr = (const BYTE*)&m_dataD;
		std::copy( ptr, ptr+sizeof(dword), tmp.begin() );
	}
	else if( IsString() )
	{
		size_t len = (m_dataS.size()+1) * sizeof(tchar);
		tmp.resize( len );
		const BYTE* ptr = (const BYTE*)m_dataS.c_str();
		std::copy( ptr, ptr+len, tmp.begin() );

	}
	else if( IsMultiString() )
	{
		tstring str = MultiToString( m_dataM );
		size_t len = (str.size()+1) * sizeof(tchar);
		tmp.resize( len );
		const BYTE* ptr = (const BYTE*)str.c_str();
		std::copy( ptr, ptr+len, tmp.begin() );
	}
	else if( IsBinary() )
	{
		tmp = m_dataB;
	}

	return tmp;
}
//-------------------------------------------------------------------------
RegEntry::operator DWORD()const
{
	dword val = 0;
	
	if( IsDword() )
	{ 
		val = m_dataD;
	}
	else if( IsString() )
	{
		tistringstream ss(m_dataS);
		ss >> val;
	}
	else if( IsMultiString() )
	{ 
		if( m_dataM.empty() ) val = 0;
		else
		{
			tistringstream ss( m_dataM[0] );
			ss >> val;
		}
	}
	else if( IsBinary() )
	{
		if( m_dataB.size() < sizeof(dword) ) val = 0;
		else
		{
			val = *(dword*)&m_dataB[0];
		}
	}

	return val;
}
//-------------------------------------------------------------------------
RegEntry::operator LPCTSTR()const
{
	// use m_dataS as cache for other types
	if( IsDword() )				{ m_dataS = MakeString( m_dataD ); }
	else if( IsString() )		{ /* m_dataS = m_dataS :-)*/ }
	else if( IsMultiString() )	{ m_dataS = MultiToString( m_dataM ); }
	else if( IsBinary() )		{ m_dataS = BinaryToString( m_dataB ); }

	return m_dataS.c_str();
}
//-------------------------------------------------------------------------
RegEntry::operator tstring()const
{
	// use m_dataS as cache for other types
	if( IsDword() )				{ m_dataS = MakeString( m_dataD ); }
	else if( IsString() )		{ /* m_dataS = m_dataS :-)*/ }
	else if( IsMultiString() )	{ m_dataS = MultiToString( m_dataM ); }
	else if( IsBinary() )		{ m_dataS = BinaryToString( m_dataB ); }

	return m_dataS;
}
//-------------------------------------------------------------------------



}	//namespace registry

