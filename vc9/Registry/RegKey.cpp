////////////////////////////////////////////////////////////////////////////
//	Author      : Ignatiev Gena 2006
//	Email       : gena.ignatev@paloma.spbu.ru
//	Module      : RegKey.cpp
//	Description : 
////////////////////////////////////////////////////////////////////////////
#include "../stdafx.h"


#include "shlwapi.h"
#include "RegKey.h"

using namespace std;

namespace registry
{

/**********************************************************************
*                          -= RegKey =-
*	Description : 
**********************************************************************/
RegKey::RegKey( bool isCreated /*= true*/)
	: m_isReadOnly( true )
	, m_isCreated( isCreated )
	, m_hkey( NULL )
	, m_hroot( NULL )
	, m_path( _T("") )
{	
}
//-------------------------------------------------------------------------
/*virtual*/
RegKey::~RegKey()
{
	Close();
	for( EntryIter i = m_entries.begin(); i != m_entries.end(); ++i )
		delete i->second;
}
//-------------------------------------------------------------------------
RegKey::RegKey( const RegKey& other )
	: m_isReadOnly( other.m_isReadOnly )
	, m_isCreated( other.m_isCreated )
	, m_hkey( other.m_hkey )
	, m_hroot( other.m_hroot )
	, m_path( other.m_path )
{
	for( EntryConstIter i = other.m_entries.begin(); i != other.m_entries.end(); ++i )
		m_entries[ i->first ] = new RegEntry( *(i->second) );
}
//-------------------------------------------------------------------------
RegKey& RegKey::operator=( const RegKey& other )
{
	if( this != &other )
	{
		ClearInnerData();
		m_isReadOnly	= other.m_isReadOnly;
		m_isCreated		= other.m_isCreated;
		m_hkey			= other.m_hkey;
		m_hroot			= other.m_hroot;
		m_path			= other.m_path;

		for( EntryConstIter i = other.m_entries.begin(); i != other.m_entries.end(); ++i )
			m_entries[ i->first ] = new RegEntry( *(i->second) );
	}

	return *this;
}
//-------------------------------------------------------------------------
RegEntry& RegKey::operator[]( const tchar* name )
{
	tstring n = ( NULL == name ? tstring() : tstring(name) );
	return this->operator[]( n );
}
//-------------------------------------------------------------------------
RegEntry& RegKey::operator[]( const tstring& name ) 
{
	static RegEntry dummy( (tstring()) );

	EntryIter iter = m_entries.find( name );
	if( iter == m_entries.end() )
	{
		EntryIns ins = m_entries.insert( std::make_pair( name, new RegEntry(name, this) ) );
		if( ins.second )
		{
			iter = ins.first;
			iter->second->SetOwner( this );
		}
	}
	
	return (iter != m_entries.end() ? *(iter->second) : dummy ) ;
}
//-------------------------------------------------------------------------
bool RegKey::IsKeyExist( const tchar* keypath, HKEY hRoot /*= HKEY_LOCAL_MACHINE*/ )
{
	RegKey key( false ); // without create
	return key.Open( keypath, hRoot, KEY_QUERY_VALUE );
}
//-------------------------------------------------------------------------
bool RegKey::IsSubKeyExist( const tchar* subkey )const
{
	if( m_hkey == NULL ) return false;

	HKEY hTemp;
	bool bResult = (ERROR_SUCCESS == RegOpenKeyEx( m_hkey, subkey, 0, KEY_QUERY_VALUE, &hTemp ));

	if( bResult ) RegCloseKey( hTemp );
	return bResult;
}
//-------------------------------------------------------------------------
void RegKey::ClearInnerData()
{
	m_path = _T("");
	for( EntryIter i = m_entries.begin(); i != m_entries.end(); ++i )
		delete i->second;

	m_entries.clear();
}
//-------------------------------------------------------------------------
bool RegKey::Open( const tchar*	keypath, HKEY hRoot, DWORD dwAccess )
{
	if( NULL == keypath ) return false;

	Close();
	ClearInnerData();

	m_path	= keypath;
	m_hroot	= hRoot;

	if( m_isCreated )	
		dwAccess |= KEY_CREATE_SUB_KEY;
	else 
		dwAccess &= ~KEY_CREATE_SUB_KEY;

	m_isReadOnly = !(dwAccess &  KEY_SET_VALUE );

	/* Open or create the sub key, and return the result: */
	LONG lResult = m_isCreated	? RegCreateKeyEx( m_hroot, keypath, 0, NULL, REG_OPTION_NON_VOLATILE, dwAccess, NULL, &m_hkey, NULL )
								: RegOpenKeyEx( m_hroot, keypath, 0, dwAccess, &m_hkey );

	if( lResult != ERROR_SUCCESS ) return false;
	if( dwAccess & KEY_QUERY_VALUE ) return Refresh();

	return true;
}
//-------------------------------------------------------------------------
std::auto_ptr<RegKey> RegKey::OpenSubKey( const tchar*	name
										, bool			isCreated /*= true*/
										, DWORD			dwAccess /*= KEY_QUERY_VALUE | KEY_SET_VALUE*/ )
{
	if( !IsOpened() ) return std::auto_ptr<RegKey>( NULL );

	tstring path = m_path + tstring(_T("\\")) + tstring(name);

	std::auto_ptr<RegKey> p( new RegKey( isCreated ) );
	if( p.get() && !p->Open( path.c_str(), m_hroot, dwAccess ) )
	{
		delete p.release();
	}
	return p;
}
//-------------------------------------------------------------------------
// max length:
//     Key name: 255 characters 
//     Value name: 16,383 characters
//                 Windows 2000:  260 ANSI characters or 16,383 Unicode characters.
//                 Windows Me/98/95:  255 characters 
//     Value: Available memory 
bool RegKey::Refresh()
{
	//todo..
	LONG	lResult			= 0L;
	DWORD	dwType			= REG_NONE;
	DWORD	dwNameLen		= _MAX_PATH;
	DWORD	dwValueCount	= 0;
	DWORD	dwBufferSize	= 0;
	TCHAR	cValueName[ _MAX_PATH ];	
	tstring value;
	std::vector<BYTE>	v;


	if( m_hkey == NULL ) return false;

	if( ERROR_SUCCESS != RegQueryInfoKey( m_hkey, NULL, NULL, NULL, NULL, NULL, NULL, &dwValueCount, NULL, NULL, NULL, NULL ) )
		return false;

	std::vector<BYTE> bufData( eMAX_REG_VALUE_SIZE );

	for( DWORD dwIndex = 0; dwIndex < dwValueCount; ++dwIndex )
	{
		dwNameLen		= _MAX_PATH;
		dwBufferSize	= eMAX_REG_VALUE_SIZE;	
		cValueName[0]	= _T('\0');

		lResult = RegEnumValue( m_hkey, dwIndex, cValueName, &dwNameLen, NULL, &dwType, &bufData[0], &dwBufferSize );
		if( ERROR_MORE_DATA == lResult )
		{ 
			// try else:
			bufData.resize( dwBufferSize+1 );
			lResult = RegEnumValue( m_hkey, dwIndex, cValueName, &dwNameLen, NULL, &dwType, &bufData[0], &dwBufferSize );
		}
		if( ERROR_SUCCESS != lResult ) continue;
		if( (dwType != REG_DWORD) && (dwType != REG_SZ) && (dwType != REG_MULTI_SZ) && (dwType != REG_BINARY) )
			continue;

		cValueName[ dwNameLen ] = _T('\0');
		tstring name = cValueName;

		RegEntry* p = new RegEntry( name, this );
		EntryIns ins = m_entries.insert( std::make_pair( name, p ) );
		if( ins.second )
		{
			if( dwType == REG_DWORD )
			{
				*p = (*reinterpret_cast<dword*>( &bufData[0] ) );
			}
			else if( dwType == REG_SZ )
			{
				tstring str = (const tchar*)&bufData[0];
				*p = str;
			}
			else if( dwType == REG_MULTI_SZ )
			{
				std::auto_ptr<MultiData> ptr = MakeMultiFromBuffer( &bufData[0], dwBufferSize );
				if( ptr.get() )
				{
					*p = *ptr;
				}
			}
			else if( dwType == REG_BINARY )
			{
				std::auto_ptr<BinaryData> ptr = MakeBinary( &bufData[0], dwBufferSize );
				if( ptr.get() )
				{
					*p = *ptr;
				}
			}

			p->SetModified( false );
		}
		else delete p;

	}

	return true;
}
//-------------------------------------------------------------------------
void RegKey::DeleteMe( bool isRecursively /*= false*/ )
{
	if( isRecursively )
		SHDeleteKey( m_hroot, m_path.c_str() );
	else 
		RegDeleteKey( m_hroot, m_path.c_str() );

	Close();
}
//-------------------------------------------------------------------------
/*static*/
void RegKey::DeleteKey	( const tchar*	keypath
						, HKEY			hRoot /*= HKEY_LOCAL_MACHINE*/
						, bool			isRecursively /*= false*/ )
{
	RegKey key( false );
	if( keypath && key.Open( keypath, hRoot, KEY_SET_VALUE ) )
	{
		key.DeleteMe( isRecursively );
	}
}
//-------------------------------------------------------------------------
void RegKey::Close() 
{
	if( m_hkey != NULL )
	{
		if( !m_isReadOnly ) Write();

		RegCloseKey( m_hkey );
		m_hkey = NULL; 
	}
}
//-------------------------------------------------------------------------
void RegKey::Write()const
{
	if( m_isReadOnly ) return;
	for( EntryConstIter i = m_entries.begin(); i != m_entries.end(); ++i )
	{
		const RegEntry* p = i->second;
		if( p ) p->Write( m_hkey );
	}
}
//-------------------------------------------------------------------------
bool RegKey::GetSubKeyList( /*OUT*/ std::list<tstring>& subkeys, bool asFullSubkeyPath /*= false*/ )
{
	subkeys.clear();

	FILETIME	ftTemp;
	DWORD		dwKeyLen	= 0;
	HKEY		hKey		= NULL;
	LONG		lResult		= ERROR_SUCCESS;
	tchar		bufkey[ _MAX_PATH ];
	tstring		subkey;

	if( ERROR_SUCCESS != RegOpenKeyEx( m_hroot, m_path.c_str(), 0, KEY_ENUMERATE_SUB_KEYS | KEY_READ, &hKey ) )
		return false;

	for( DWORD dwIndex = 0; lResult == ERROR_SUCCESS; ++dwIndex )
	{
		dwKeyLen = _MAX_PATH;
		lResult	 = RegEnumKeyEx( hKey, dwIndex, bufkey, &dwKeyLen, NULL, NULL, NULL, &ftTemp );
		if( ERROR_NO_MORE_ITEMS == lResult ) break;
		else if( lResult == ERROR_SUCCESS )
		{
			bufkey[dwKeyLen] = _T('\0');
			subkey = bufkey;
			if( asFullSubkeyPath ) subkey = m_path + tstring(_T("\\")) + subkey;
			subkeys.push_back( subkey );
		}
	}
	return true;
}
//-------------------------------------------------------------------------


}	//namespace registry