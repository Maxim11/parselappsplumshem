#ifndef _PARCEL_TRANS_DATA_H_
#define _PARCEL_TRANS_DATA_H_

#pragma once

#include "SampleData.h"
#include "PRDBProject.h"


typedef list<CSampleData> LSam;
typedef list<CSampleData>::iterator ItSam;
typedef list<CSampleData*>::iterator ItSamPtr;

typedef vector<CSampleData> VSam;
typedef vector<CSampleData>::iterator ItVSam;

typedef vector<CSampleData*> VPSam;
typedef vector<CSampleData*>::iterator ItVPSam;

///////////////////////////////////////////////////////////////////////////////////////////////////
// ����� ��� �������� ������ ������������ ��������

struct PlateData
{
	int Num;								// ����� ��������
	CString SamName;						// ��� �������-��������
	double R;								// ����������� ��������� ��������
	double T0;								// ������� ��������
	double T1;								// 1-� ���. ������� ��������
	double T2;								// 2-� ���. ������� ��������
	double T3;								// 3-� ���. ������� ��������
	double T4;								// 4-� ���. ������� ��������

	VInt Spectra;							// ������ ������� ��������

	PlateData(int num);
};

typedef vector<PlateData> VPlate;
typedef vector<PlateData>::iterator ItPlate;

class CTransferData
{
public:

	CTransferData(HTREEITEM hParent);
	~CTransferData();

	void Copy(CTransferData& data);

protected:

	CString Name;			// ��� ������

	int Type;				// ��� ������

	HTREEITEM hItem;		// ������ ���� � ������, ���������������� ������
	HTREEITEM hParentItem;	// ������ ���� � ������, ���������������� ������������� ������� �������

	int MasterDevType;		// ��� ������-�������
	int MasterDevNumber;	// ����� ������-�������
	CTime Date;				// ���� � ����� ��������
	int LowLimSpec;			// ������ ������� ������������� ���������
	int UpperLimSpec;		// ������� ������� ������������� ���������

	CString FrameName;		// ��� �������-������ (��� SST)

	VDbl MeanSpec;			// ������� ������ ��������������� ������
	VDbl MeanSpecMSC;		// ������� ������ ��� ��� ����������������� ���������
	VDbl MeanSpecStd;		// ������������������ ����������

public:

	VInt Preproc;			// ������ �������������

	LSam Samples;			// ������ ��������
	VSam ArtSamples;		// ��������� �������: ��� ������� ��������

	VDbl SpecPoints;		// ������ ������������ ����� (���)

	VPlate Plates;			// ������ ������� (������ ��� ������������ ������)

public:

	HTREEITEM GetHItem();					// �������� ������ ���� � ������, ��������������� ������
	void SetHItem(HTREEITEM item);			// ���������� ������ � ������, ��������������� ������

	HTREEITEM GetParentHItem();				// �������� ������ ���� � ������, ��������������� ������� �������
	void SetParentHItem(HTREEITEM item);

	CString GetName();						// �������� ��� ������
	void SetName(CString name);				// ���������� ��� ������

	int GetType();
	void SetType(int type);

	int GetMasterDevType();
	void SetMasterDevType(int type);
	int GetMasterDevNumber();
	void SetMasterDevNumber(int num);
	CString GetMasterDevName();				// �������� ��� ������-�������

	CTime GetDate();						// �������� ���� � ����� ��������
	void SetDate();							// ���������� ������� ���� � ����� ��� ���� � ����� ��������

	int GetLowLimSpec();					// �������� ������ ������� ������������� ���������
	void SetLowLimSpec(int lim);			// ���������� ������ ������� ������������� ���������

	int GetUpperLimSpec();					// �������� ������� ������� ������������� ���������
	void SetUpperLimSpec(int lim);			// ���������� ������� ������� ������������� ���������

	int GetNPreproc();						// �������� ����� �������������
	int GetPreprocInd(int num);				// �������� ������������� ��� ������ � ���
	CString GetAllPreprocAsStr();			// �������� ��� �������������� � ���� ������
	void GetAllPreproc(VInt& vect);			// �������� ��� ������������� � ���� ������� ��������
	void SetAllPreproc(VInt* preps);		// ���������� ������ �������������
	void SetAllPreproc(CString prepstr);

	PlateData* GetPlate(int num);

	int GetNSamples();						// �������� ����� �������� ��� ������� ������
	int GetNSpectra();						// �������� ����� ��������
	int GetNSpectraUsed();					// �������� ����� ������������ ��������
	CSampleData* GetSample(CString name);	// �������� ������� �� �����
	CSampleData* GetSample(int ind);		// �������� ������� �� ����������� ������ � ������
	CSampleData* GetLastSample();
	bool AddSample(CSampleData* NewSam);	// �������� ������� � ������
	bool DeleteSample(int ind);				// ������� ������� �� ������ �� ������� � ������
	bool DeleteSample(CString name);		// ������� ������� �� ������ �� �����

	CSampleData* GetArtSample();
	bool DeleteArtSample();

	CString GetFrameName();					// �������� ��� �������-������
	void SetFrameName(CString name);		// ���������� ��� �������-������

	int GetNSpecPoints();					// �������� ����� ������������ �����
	double GetSpecPoint(int indSP);			// �������� ������������ �����
	int GetSpecPointInd(double value);		// �������� ������������ ����� ��� ������ � ������
	void GetSpecPoints();					// ���������� ������ ������������ �����

	void GetMeanSpec(VDbl& Mean);
	void SetMeanSpec(VDbl& mean);
	void GetMeanSpecMSC(VDbl& Mean);
	void SetMeanSpecMSC(VDbl& mean);
	void GetMeanSpecStd(VDbl& Mean);
	void SetMeanSpecStd(VDbl& mean);

	void GetDataForBase(prdb::PRTrans& Obj);
	void SetDataFromBase(prdb::PRTrans& Obj);
	bool LoadDatabase();
};

#endif
