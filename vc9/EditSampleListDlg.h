#ifndef _EDIT_SAMPLE_LIST_DLG_H_
#define _EDIT_SAMPLE_LIST_DLG_H_

#pragma once

#include "SampleData.h"
//#include "ParcelListCtrl.h"
#include "EditList.h"
#include "Resource.h"
#include "afxwin.h"


///////////////////////////////////////////////////////////////////////////////////////////////////
// ������ �������������� ������ ��������

class CEditSampleListDlg : public CDialog
{
	DECLARE_DYNAMIC(CEditSampleListDlg)

public:

	CEditSampleListDlg(CProjectData* PrjData, LSam* samplesdata, HTREEITEM item, CWnd* pParent = NULL);	// �����������
	virtual ~CEditSampleListDlg();																		// ����������

	enum { IDD = IDD_EDIT_SAMPLE_LIST };	// ������������� �������

//	CListCtrlStyled m_list;					// ������ ��������
	CEditList m_list;					// ������ ��������

	bool IsEditMode() {return m_bTableEditMode;}

protected:

	CProjectData* pParentPrj;				// ��������� �� ������, ������������ ��� ��������
	CTransferData* pTrans;
	LSam* pSamplesList;						// ��������� �� ������ �������� ��� ��������������
	std::list<CSampleData*> m_SamplesChanged;					// ������ ���������� �������� (� �������)
	int m_nParStart;
	int m_nTimeCol;

	int GetSamSel();
	VStr SamSel;

	bool m_bOwn;
	bool m_bTableEditMode;
	bool m_bTableChanged;				// 


protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();						// ������������� �������
/*
	virtual void OnOK();				// ��������� ������� "������� ������ � ����� �� �������"
	virtual void OnCancel();			// ��������� ������� "�������� ���� � ����� �� �������"
*/
	afx_msg void OnAdd();				// ��������� ������� "������� ����� �������"
	afx_msg void OnEdit();				// ��������� ������� "�������� �������"
	afx_msg void OnRemove();			// ��������� ������� "������� �������"
	afx_msg void OnHelp();				// ��������� ������� "������� �������"

	afx_msg void OnSamplesListSelChange(NMHDR* /*pNMHDR*/, LRESULT *pResult);// ��������� ������� "�������� �������� � ������ ��������"
	afx_msg void OnSamplesListColumnClick(NMHDR* pNMHDR, LRESULT* pResult);

	void FillList(int sel, bool bFromScratch = true);		// ��������� ������ ��������
	int GetNSpectraUsed();							// ������� ������ ����� ���������� ��������



public:

	bool IsNoData(int iSam, int iRef);
	bool IsRefUse(int iSam, int iRef);
	bool IsRefUse(CString sSample, int iRef);
	void OnEditCell(int iItem, int iSubItem);

protected:

	DECLARE_MESSAGE_MAP()
public:
	CButton m_butEditSample;
	CButton m_butRemoveSample;
	CButton m_butAddSample;

	CButton m_butEditTable;
	CButton m_butSaveTable;
	afx_msg void OnBnClickedEditTable();
	afx_msg void OnBnClickedSaveTable();
	afx_msg void OnBnClickedCancel();
protected:
	virtual void OnOK();
};

#endif
