#include "stdafx.h"

#include "BranchInfo.h"
#include "ModelQltResultDlg.h"
#include "ChooseGKDlg.h"
#include "SetTemplateDlg.h"
#include "MainFrm.h"
#include "ParcelReport.h"
#include "RootData.h"
#include "FileSystem.h"
#include "Registry/RegKey.h"

#include <math.h>

using namespace filesystem;


IMPLEMENT_DYNAMIC(CModelQltResultDlg, CDialog)

static CModelQltResultDlg* pModQltRes = NULL;

static GraphInfo* pHICalib = NULL;
static GraphInfo* pHIValid = NULL;
static GraphInfo* pGIScores = NULL;

static double fHistoMDL(double /*x*/)
{
	return pModQltRes->GetMaxMah();
}

static int GetTextColor(int item, int subitem, void* /*pData*/)
{
	if(pModQltRes == NULL)
		return 0;

	int iTab = pModQltRes->GetCurTab();
	COLORREF color = CLR_DEFAULT;
	switch(iTab)
	{
	case C_MODQLTRES_PAGE_OUT:
		if(subitem != 0 && pModQltRes->IsResultBad(item))
			color = cColorErr;
		break;
	}

	pModQltRes->m_listBig.CurTxtColor = color;

	return 1;
}

static int LbClickCallback(UINT /*nFlags*/, CPoint point, void *pData)
{
	int sh = 3;
	if(pModQltRes->GetCurTab() == C_MODQLTRES_PAGE_OUT)
		sh = 1;
	if(pModQltRes->GetCurTab() == C_MODQLTRES_PAGE_SPECLOAD)
		sh = 2;

	CRect rect;
	rect.left = point.x - sh;
	rect.right = point.x + sh;
	rect.top = point.y - sh;
	rect.bottom = point.y + sh;

	if(pModQltRes != NULL)
	{
		pModQltRes->SelectByRect(&rect, false);
		pModQltRes->ShowCoord(&point);
	}

	return 1;
}

static int LbUnClickCallback(UINT /*nFlags*/, CPoint /*point*/, void *pData)
{
	if(pModQltRes != NULL)
	{
		pModQltRes->RemoveInfo();
		if(pData != NULL)
		{
			CRect *pRect = (CRect*)pData;
			pModQltRes->SelectByRect(pRect);
		}
	}

	return 1;
}

static int RbClickCallback(UINT /*nFlags*/, CPoint /*point*/, void *pData)
{
	return 1;
}

static int RbUnClickCallback(UINT /*nFlags*/, CPoint /*point*/, void *pData)
{
	if(pModQltRes != NULL)
		pModQltRes->SetStatusMenu();

	return 1;
}

CModelQltResultDlg::CModelQltResultDlg(CModelData* Mod, int mode, CWnd* pParent)
	: CDialog(CModelQltResultDlg::IDD, pParent)
{
	pModQltRes = this;

	StartModel = Mod;
	Mode = mode;

	ModData = new(CModelData)(StartModel->GetParentHItem());
	ModData->Copy(*StartModel);
	ModTmp.clear();

	m_vTab = C_MODQLTRES_PAGE_OUT;
	m_vShow = C_MODQLTRES_SHOW_TABLE;
	m_vCalibValid = C_MODQLTRES_SMPL_CALIB;
	m_vMaxMah = Mod->GetMaxMah();
	m_vOXsc = 0;
	m_vOYsc = (ModData->GetNCompQlt() > 1) ? 1 : 0;

	IsAvr = ModData->ModelResult.IsAverage();

	pHistoCalib = NULL;
	pHistoValid = NULL;
	pHistoMDL = NULL;
	pGraphScores = NULL;

	pMarkHCalib = NULL;
	pMarkHValid = NULL;
	pMarkScores = NULL;

	posInfo = NULL;
	posCoord = NULL;
	posLegendOut = NULL;
	posLegendSpec = NULL;

	pMenu1 = new CMenu;
	pMenu1->CreatePopupMenu();

	show_legend = false;
	show_mdl = true;
}

CModelQltResultDlg::~CModelQltResultDlg()
{
	if(ModData != NULL)  delete ModData;
	ModTmp.clear();

	if(pHistoCalib != NULL)  delete pHistoCalib;
	if(pHistoValid != NULL)  delete pHistoValid;
	if(pHistoMDL != NULL)  delete pHistoMDL;
	if(pGraphScores != NULL)  delete pGraphScores;
	GraphsSpecLoad.clear();
	MasksSLPos.clear();

	if(pMarkHCalib != NULL)  delete[] pMarkHCalib;
	if(pMarkHValid != NULL)  delete[] pMarkHValid;
	if(pMarkScores != NULL)  delete[] pMarkScores;

	pMenu1->DestroyMenu();
	delete pMenu1;
}

void CModelQltResultDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_TAB_1, m_Tab);
	DDX_Control(pDX, IDC_MODEL_RESULT_TABLEBIG, m_listBig);
	DDX_Control(pDX, IDC_SHOW, m_Show);
	DDX_Control(pDX, IDC_CALIBVALID, m_CalibValid);
	DDX_Control(pDX, IDC_MAXMAH, m_MaxMah);
	DDX_Control(pDX, IDC_AXE_OX, m_OXsc);
	DDX_Control(pDX, IDC_AXE_OY, m_OYsc);

	DDX_CBIndex(pDX, IDC_SHOW, m_vShow);
	DDX_CBIndex(pDX, IDC_CALIBVALID, m_vCalibValid);
	DDX_CBIndex(pDX, IDC_AXE_OX, m_vOXsc);
	DDX_CBIndex(pDX, IDC_AXE_OY, m_vOYsc);

	cParcelDDX(pDX, IDC_MAXMAH, m_vMaxMah);
	cParcelDDV(pDX, ParcelLoadString(STRID_MODEL_QLT_RESULT_DLG_DDV_MAXMAH), m_vMaxMah, 0.1, 1.e9);

}

BEGIN_MESSAGE_MAP(CModelQltResultDlg, CDialog)

	ON_COMMAND(C_MODQLTRES_MENU_PREV, OnUserMenuPrev)
	ON_COMMAND(C_MODQLTRES_MENU_NEXT, OnUserMenuNext)
	ON_COMMAND(C_MODQLTRES_MENU_FULL, OnUserMenuFull)
	ON_COMMAND(C_MODQLTRES_MENU_AUTO, OnUserMenuAuto)
	ON_COMMAND(C_MODQLTRES_MENU_LEFT, OnUserMenuLeft)
	ON_COMMAND(C_MODQLTRES_MENU_RIGHT, OnUserMenuRight)
	ON_COMMAND(C_MODQLTRES_MENU_LEGEND, OnUserMenuLegend)
	ON_COMMAND(C_MODQLTRES_MENU_GRID, OnUserMenuGrid)
	ON_COMMAND(C_MODQLTRES_MENU_MDL, OnUserMenuMDL)

	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB_1, OnTabSelchange)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_MODEL_RESULT_TABLEBIG, OnTableBigSelChange)
	ON_NOTIFY(HDN_ITEMCLICK, 0, OnTableBigColumnClick)

	ON_CBN_SELCHANGE(IDC_SHOW, OnChangeView)
	ON_CBN_SELCHANGE(IDC_CALIBVALID, OnChangeCalibValid)
	ON_CBN_SELCHANGE(IDC_AXE_OX, OnChangeAxeGKsc)
	ON_CBN_SELCHANGE(IDC_AXE_OY, OnChangeAxeGKsc)

	ON_BN_CLICKED(IDC_ACCEPT, OnAccept)
	ON_BN_CLICKED(IDHELP, OnHelp)
	ON_BN_CLICKED(IDC_SET_TEMPLATE, OnSetTemplate)
	ON_BN_CLICKED(IDC_CREATE_PROTOCOL, OnCreateProtocol)
	ON_BN_CLICKED(IDC_EXCLUDE, OnExclude)
	ON_BN_CLICKED(IDC_BACK, OnBack)
	ON_BN_CLICKED(IDC_BACK_ALL, OnBackAll)
	ON_BN_CLICKED(IDC_REFRESH, OnMaxMahChanged)
	ON_BN_CLICKED(IDC_SETTINGS, OnSettings)

END_MESSAGE_MAP()

BOOL CModelQltResultDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	USES_CONVERSION;

	CString s;

	TC_ITEM TabCtrlItem;
	TabCtrlItem.mask = TCIF_TEXT;
	TabCtrlItem.pszText = (LPWSTR)GETSTRUI(STRID_MODEL_QLT_RESULT_OUT_HDR);
	m_Tab.InsertItem(C_MODQLTRES_PAGE_OUT, &TabCtrlItem);
	TabCtrlItem.pszText = (LPWSTR)GETSTRUI(STRID_MODEL_QLT_RESULT_SCORES_HDR);
	m_Tab.InsertItem(C_MODQLTRES_PAGE_SCORES, &TabCtrlItem);
	TabCtrlItem.pszText = (LPWSTR)GETSTRUI(STRID_MODEL_QLT_RESULT_SPECLOAD_HDR);
	m_Tab.InsertItem(C_MODQLTRES_PAGE_SPECLOAD, &TabCtrlItem);

	m_CalibValid.ResetContent();
	s = ParcelLoadString(STRID_MODEL_QLT_RESULT_SAMCALIB);
	m_CalibValid.AddString(s);
	s = ParcelLoadString(STRID_MODEL_QLT_RESULT_SAMVALID);
	m_CalibValid.AddString(s);
	m_CalibValid.SetCurSel(m_vCalibValid);

	m_OXsc.ResetContent();
	m_OYsc.ResetContent();
	CString fmtGK = ParcelLoadString(STRID_MODEL_QLT_RESULT_SPECLOAD_COL_GK);
	int i;
	for(i=0; i<ModData->GetNCompQlt(); i++)
	{
		s.Format(fmtGK, i + 1);
		m_OXsc.AddString(s);
		m_OYsc.AddString(s);
	}
	m_OXsc.SetCurSel(m_vOXsc);
	m_OYsc.SetCurSel(m_vOYsc);

	s = ParcelLoadString(STRID_MODEL_QLT_RESULT_COL_NUM);
	m_listBig.InsertColumn(0, s, LVCFMT_CENTER, 0, 0);
	m_listBig.SetColumnWidth(0, LVSCW_AUTOSIZE_USEHEADER);
	m_listBig.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);
	m_listBig.SetIconList();
	m_listBig.SetUserCallback(LCF_CB_SI_TXTCOLOR, GetTextColor);
	m_listBig.EnableUserCallback(LCF_CB_SI_TXTCOLOR, true);

	CreateChart();

	CString Hdr;
	Hdr.Format(ParcelLoadString(STRID_MODEL_QLT_RESULT_HDR), ModData->GetName());
	SetWindowText(Hdr);

	GetDlgItem(IDC_ACCEPT)->SetWindowText(ParcelLoadString(STRID_MODEL_QLT_RESULT_ACCEPT));
	GetDlgItem(IDHELP)->SetWindowText(ParcelLoadString(STRID_MODEL_QLT_RESULT_HELP));
	GetDlgItem(IDC_SET_TEMPLATE)->SetWindowText(ParcelLoadString(STRID_MODEL_QLT_RESULT_SET_TEMPLATE));
	GetDlgItem(IDC_CREATE_PROTOCOL)->SetWindowText(ParcelLoadString(STRID_MODEL_QLT_RESULT_CREATE_PROTOCOL));
	GetDlgItem(IDC_STATIC_12)->SetWindowText(ParcelLoadString(STRID_MODEL_QLT_RESULT_SHOWAS));
	GetDlgItem(IDC_STATIC_13)->SetWindowText(ParcelLoadString(STRID_MODEL_QLT_RESULT_SHOWSAMPLES));
	GetDlgItem(IDC_STATIC_3)->SetWindowText(ParcelLoadString(STRID_MODEL_QLT_RESULT_OUTSDEFINE));
	GetDlgItem(IDC_EXCLUDE)->SetWindowText(ParcelLoadString(STRID_MODEL_QLT_RESULT_EXCLUDESPEC));
	GetDlgItem(IDC_BACK)->SetWindowText(ParcelLoadString(STRID_MODEL_QLT_RESULT_BACK));
	GetDlgItem(IDC_BACK_ALL)->SetWindowText(ParcelLoadString(STRID_MODEL_QLT_RESULT_BACK_ALL));
	GetDlgItem(IDC_REFRESH)->SetWindowText(ParcelLoadString(STRID_MODEL_QLT_RESULT_REFRESH));
	GetDlgItem(IDC_STATIC_10)->SetWindowText(ParcelLoadString(STRID_MODEL_QLT_RESULT_MAXMAH));
	GetDlgItem(IDC_STATIC_14)->SetWindowText(ParcelLoadString(STRID_MODEL_QLT_RESULT_AXIS));
	GetDlgItem(IDC_STATIC_15)->SetWindowText(ParcelLoadString(STRID_MODEL_QLT_RESULT_AXE_OY));
	GetDlgItem(IDC_STATIC_16)->SetWindowText(ParcelLoadString(STRID_MODEL_QLT_RESULT_AXE_OX));
	GetDlgItem(IDC_SETTINGS)->SetWindowText(ParcelLoadString(STRID_MODEL_QLT_RESULT_SETTINGS));

	switch(Mode)
	{
	case C_MODQLTRESDLG_MODE_EDIT:
		GetDlgItem(IDCANCEL)->SetWindowText(ParcelLoadString(STRID_MODEL_QLT_RESULT_DECLINE));
		GetDlgItem(IDC_SET_TEMPLATE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CREATE_PROTOCOL)->ShowWindow(SW_HIDE);
		break;
	case C_MODQLTRESDLG_MODE_SHOW:
		GetDlgItem(IDCANCEL)->SetWindowText(ParcelLoadString(STRID_MODEL_QLT_RESULT_EXIT));
		GetDlgItem(IDC_ACCEPT)->ShowWindow(SW_HIDE);
	}

	bool bEdit = (Mode == C_MODQLTRESDLG_MODE_EDIT);
	GetDlgItem(IDC_EXCLUDE)->EnableWindow(bEdit);
	GetDlgItem(IDC_BACK)->EnableWindow(bEdit);
	GetDlgItem(IDC_BACK_ALL)->EnableWindow(bEdit);

	ShowPage(C_MODQLTRES_PAGE_OUT);

	GetDlgItem(IDHELP)->EnableWindow(pFrame->IsExistHelp());

	return true;
}

void CModelQltResultDlg::OnAccept()
{
	if(Mode == C_MODQLTRESDLG_MODE_EDIT)
		StartModel->Copy(*ModData);

	CDialog::OnOK();
}

void CModelQltResultDlg::OnCancel()
{
	CDialog::OnCancel();
}

void CModelQltResultDlg::OnTabSelchange(NMHDR* /*pNMHDR*/, LRESULT* pResult)
{
	m_vTab = m_Tab.GetCurSel();

	ShowPage();

	*pResult = 0;
}

void CModelQltResultDlg::OnTableBigSelChange(NMHDR* /*pNMHDR*/, LRESULT *pResult)
{
	int Ind = -1, i, nsel = m_listBig.GetSelectedCount();
	if(nsel <= 0)
		return;

	if(m_vTab == C_MODQLTRES_PAGE_OUT && m_vCalibValid == C_MODQLTRES_SMPL_VALID)
	{
		selV.clear();
		for(i=0; i<nsel; i++)
		{
			Ind = m_listBig.GetNextItem(Ind, LVNI_SELECTED);
			selV.push_back(ModData->ModelResult.GetValidNameByInd(Ind));
		}
	}
	else if(m_vTab != C_MODQLTRES_PAGE_SPECLOAD)
	{
		selC.clear();
		for(i=0; i<nsel; i++)
		{
			Ind = m_listBig.GetNextItem(Ind, LVNI_SELECTED);
			selC.push_back(ModData->ModelResult.GetCalibNameByInd(Ind));
		}
	}
	ColoredAllGraphs();

	*pResult = 0;
}

void CModelQltResultDlg::OnTableBigColumnClick(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NMLISTVIEW *pLV = (NMLISTVIEW *) pNMHDR;

	int item = pLV->iItem;

	bool isValid = (m_vTab == C_MODQLTRES_PAGE_OUT && m_vCalibValid == C_MODQLTRES_SMPL_VALID);

	if(isValid && ModData->ModelResult.GetNValidResult() <= 0)
	{
		*pResult = 0;
		return;
	}

	int prop = (!isValid) ? ModData->ModelResult.SortCalibProp : ModData->ModelResult.SortValidProp;
	bool dir = (!isValid) ? ModData->ModelResult.SortCalibDir : ModData->ModelResult.SortValidDir;
	int gk = 0;

	bool findsort = false;
	switch(m_vTab)
	{
	case C_MODQLTRES_PAGE_OUT:
		if(isValid)
		{
			if(item == 1)
			{
				if(prop == C_MODRES_SORT_VALID_NAME)  dir = !dir;
				else  prop = C_MODRES_SORT_VALID_NAME;
				findsort = true;
			}
			if(item == 2)
			{
				if(prop == C_MODRES_SORT_VALID_MD)  dir = !dir;
				else  prop = C_MODRES_SORT_VALID_MD;
				findsort = true;
			}
			if(item == 3)
			{
				if(prop == C_MODRES_SORT_VALID_EXCEED)  dir = !dir;
				else  prop = C_MODRES_SORT_VALID_EXCEED;
				findsort = true;
			}
		}
		else
		{
			if(item == 1)
			{
				if(prop == C_MODRES_SORT_CALIB_NAME)  dir = !dir;
				else  prop = C_MODRES_SORT_CALIB_NAME;
				findsort = true;
			}
			if(item == 2)
			{
				if(prop == C_MODRES_SORT_CALIB_OUT_MD)  dir = !dir;
				else  prop = C_MODRES_SORT_CALIB_OUT_MD;
				findsort = true;
			}
			if(item == 3)
			{
				if(prop == C_MODRES_SORT_CALIB_OUT_EXCEED)  dir = !dir;
				else  prop = C_MODRES_SORT_CALIB_OUT_EXCEED;
				findsort = true;
			}
		}
		break;
	case C_MODQLTRES_PAGE_SCORES:
		if(item == 1)
		{
			if(prop == C_MODRES_SORT_CALIB_NAME)  dir = !dir;
			else  prop = C_MODRES_SORT_CALIB_NAME;
			findsort = true;
		}
		if(item > 1)
		{
			gk = item - 2;
			if(prop == C_MODRES_SORT_CALIB_GK_0 + gk)  dir = !dir;
			prop = C_MODRES_SORT_CALIB_GK_0 + gk;
			findsort = true;
		}
		break;
	}

	if(findsort)
	{
		if(isValid)
			ModData->ModelResult.SortValid(prop, dir, 0, gk);
		else
			ModData->ModelResult.SortCalib(prop, dir, 0, gk);

		FillTableBig();
	}

	*pResult = 0;
}

void CModelQltResultDlg::OnChangeView()
{
	m_vShow = m_Show.GetCurSel();

	ShowPage();
}

void CModelQltResultDlg::OnChangeCalibValid()
{
	m_vCalibValid = m_CalibValid.GetCurSel();

	ShowPage();
}

void CModelQltResultDlg::OnChangeAxeGKsc()
{
	m_vOXsc = m_OXsc.GetCurSel();
	m_vOYsc = m_OYsc.GetCurSel();

	UpdateData(0);

	CreateGraphScores();

	ShowPage();
}

void CModelQltResultDlg::OnSettings()
{
	CChooseGKDlg dlg(&ModData->ModelResult, this);
	if(dlg.DoModal() != IDOK)
		return;

	CreateLegendSpec();
	pChart->GetUserLegend(posLegendSpec)->Enable(GetNGKShow() > 0 && show_legend);

	ShowPage();
}

void CModelQltResultDlg::OnSetTemplate()
{
	CSetTemplateDlg dlg(C_TEMPLATE_QLT);
	dlg.DoModal();
}

void CModelQltResultDlg::OnCreateProtocol()
{
	CParcelReportQlt theReport(pModQltRes);

	CString TmplPath = MakePath(pRoot->GetTemplateFolder(C_TEMPLATE_QLT), pRoot->GetTemplateName(C_TEMPLATE_QLT));
	CString HtmlPath = pRoot->GetReportPath(C_TEMPLATE_QLT);
	CString PicPath = pRoot->GetReportPictureFolder(C_TEMPLATE_QLT);

	ICreatorApi* pCreator = REPORTGEN_QueryCreatorApi();
	if(NULL != pCreator)
	{
		DeleteAllContentFromDirectory(PicPath);
		if(!pCreator->CreateReport(TmplPath, HtmlPath, static_cast<IAppCallback*>(&theReport)))
		{
			CString strMsg = pCreator->GetErrorInfo(pCreator->GetLastError());
			AfxMessageBox(strMsg);
		}
		REPORTGEN_ReleaseCreatorApi();
	}

	registry::RegKey key(true);
	if(key.Open(_T("SOFTWARE\\Lumex\\LumReportViewer"), REGROOT))
	{
		key[_T("ReportPath")] = pRoot->GetReportPath(C_TEMPLATE_QLT);
		key[_T("AlwaysTop")] = _T("0");
		key[_T("UiLanguage")] = pFrame->GetLanguage();

		key.Close();

		CString strViewer = MakePath(pRoot->GetWorkFolder(), _T("LumReportViewer.exe"));

		STARTUPINFO si;
		PROCESS_INFORMATION pi;

		ZeroMemory(&si, sizeof(STARTUPINFO));
		ZeroMemory(&pi, sizeof(PROCESS_INFORMATION));

		si.cb = sizeof(STARTUPINFO);
		si.dwFlags = STARTF_USESHOWWINDOW;
		si.wShowWindow = SW_SHOWNORMAL;

		LPTSTR lpszViewer = strViewer.GetBuffer(0);
		::CreateProcessW(NULL, lpszViewer, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi);
		::WaitForInputIdle(GetCurrentProcess(), INFINITE);

		if(pi.hProcess)
		{
			DWORD dwExitCode = STILL_ACTIVE;
			while(dwExitCode == STILL_ACTIVE)
			{
				::WaitForSingleObject(pi.hProcess, 1000);
				::GetExitCodeProcess(pi.hProcess, &dwExitCode);
			}           
		}
	}
}

void CModelQltResultDlg::OnMaxMahChanged()
{
	double old = m_vMaxMah;

	if(!UpdateData())
	{
		GetDlgItem(IDC_MAXMAH)->SetFocus();
		return;
	}

	if(fabs(old - m_vMaxMah) > 1.e-9)
	{
		ModData->SetMaxMah(m_vMaxMah);

		ShowPage();
	}
}

void CModelQltResultDlg::OnExclude()
{
	SInt PointsCopy;
	ModData->GetExcPoints(PointsCopy);

	VModSam CalibCopy, ValidCopy;
	ModData->GetCalibValid(CalibCopy, ValidCopy);

	CModelData NewTmpMod(ModData->GetParentHItem());
	NewTmpMod.Copy(*ModData);

	bool res = false;
	switch(m_vTab)
	{
	case C_MODQLTRES_PAGE_OUT:
	case C_MODQLTRES_PAGE_SCORES:
		res = ExcludeSpectra();
		break;
	case C_MODQLTRES_PAGE_SPECLOAD:
		res = ExcludePoints();
		break;
	}

	if(res)
	{
		if(ModData->CalculateModel())
		{
			ModTmp.push_back(NewTmpMod);
			GetDlgItem(IDC_BACK)->EnableWindow(true);
			GetDlgItem(IDC_BACK_ALL)->EnableWindow(true);

			SortAll();
			CreateAllGraphs();
			ShowPage();
		}
		else
		{
			if(m_vTab == C_MODQLTRES_PAGE_SPECLOAD)
				ModData->SetExcPoints(PointsCopy);
			else
				ModData->SetCalibValid(CalibCopy, ValidCopy);
		}

	}
}

void CModelQltResultDlg::OnBack()
{
	if(ModTmp.size() <= 0)
		return;

	ModData->Copy(ModTmp.back());
	ModTmp.pop_back();
	GetDlgItem(IDC_BACK)->EnableWindow(ModTmp.size() > 0);
	GetDlgItem(IDC_BACK_ALL)->EnableWindow(ModTmp.size() > 0);

	m_vMaxMah = ModData->GetMaxMah();

	UpdateData(0);

	if(ModData->CalculateModel())
	{
		SortAll();
		CreateAllGraphs();
		ShowPage();
	}
}

void CModelQltResultDlg::OnBackAll()
{
	ModData->Copy(*StartModel);
	ModTmp.clear();
	GetDlgItem(IDC_BACK)->EnableWindow(false);
	GetDlgItem(IDC_BACK_ALL)->EnableWindow(false);

	m_vMaxMah = ModData->GetMaxMah();

	UpdateData(0);

	if(ModData->CalculateModel())
	{
		SortAll();
		CreateAllGraphs();
		ShowPage();
	}
}

void CModelQltResultDlg::OnUserMenuPrev()
{
	pChart->GetWnd()->PostMessage(WM_COMMAND, LC_POPUP_GO_BACK, 0);
}

void CModelQltResultDlg::OnUserMenuNext()
{
	pChart->GetWnd()->PostMessage(WM_COMMAND, LC_POPUP_GO_FORWARD, 0);
}

void CModelQltResultDlg::OnUserMenuFull()
{
	pChart->GetWnd()->PostMessage(WM_COMMAND, LC_POPUP_FULL_SCREEN, 0);
}

void CModelQltResultDlg::OnUserMenuAuto()
{
	pChart->GetWnd()->PostMessage(WM_COMMAND, LC_POPUP_AUTOY, 0);
}

void CModelQltResultDlg::OnUserMenuLeft()
{
	pChart->GetWnd()->PostMessage(WM_COMMAND, LC_POPUP_XLEFT, 0);
}

void CModelQltResultDlg::OnUserMenuRight()
{
	pChart->GetWnd()->PostMessage(WM_COMMAND, LC_POPUP_XRIGHT, 0);
}

void CModelQltResultDlg::OnUserMenuLegend()
{
	show_legend = !show_legend;
	ShowPage();
}

void CModelQltResultDlg::OnUserMenuGrid()
{
	bool btmp, btmp2;
	pChart->IsGridEnabled(&btmp, &btmp2);
	pChart->ShowGrid(!btmp, !btmp);
}

void CModelQltResultDlg::OnUserMenuMDL()
{
	if(pHistoMDL == NULL)
		return;

	bool btmp;
	pChart->IsGraphEnabled(pHistoMDL->GetPos(), &btmp);
	pChart->ShowGraph(pHistoMDL->GetPos(), !btmp);
	show_mdl = !btmp;

	CreateLegendOut();
	pChart->GetUserLegend(posLegendOut)->Enable(show_mdl && show_legend);

	pChart->RedrawWindow();
}

int CModelQltResultDlg::GetCurTab()
{
	return m_vTab;
}

bool CModelQltResultDlg::GetCurCalibValid()
{
	return (m_vTab == C_MODQLTRES_PAGE_OUT && m_vCalibValid == C_MODQLTRES_SMPL_VALID);
}

double CModelQltResultDlg::GetMaxMah()
{
	return ModData->GetMaxMah();
}

bool CModelQltResultDlg::IsResultBad(int item)
{
	bool isValid = GetCurCalibValid();

	SpectrumResult* pSpR = (!isValid) ? ModData->ModelResult.GetCalibResult(item) : NULL;
	SpectrumValResult* pSpVR = (isValid) ? ModData->ModelResult.GetValidResult(item) : NULL;

	switch(m_vTab)
	{
	case C_MODQLTRES_PAGE_OUT:
		if(!isValid)
		{
			if(pSpR->Out_Mah > m_vMaxMah)
				return true;
		}
		else
		{
			if(pSpVR->Out_Mah > m_vMaxMah)
				return true;
		}
		break;
	}

	return false;
}

void CModelQltResultDlg::ShowPage(int page/* = -1*/)
{
	if(page >= C_MODQLTRES_PAGE_OUT && page <= C_MODQLTRES_PAGE_SPECLOAD)
		m_vTab = page;

	int graph;
	switch(m_vTab)
	{
	case C_MODQLTRES_PAGE_OUT:
		if(m_vCalibValid == C_MODQLTRES_SMPL_CALIB)  graph = C_PGRAPH_MDCALIB;
		else  graph = C_PGRAPH_MDVALID;
		break;
	case C_MODQLTRES_PAGE_SCORES:  graph = C_PGRAPH_SCORES;  break;
	case C_MODQLTRES_PAGE_SPECLOAD:  graph = C_PGRAPH_SPLOAD;  break;
	}

	bool out = (m_vTab == C_MODQLTRES_PAGE_OUT);
	bool scores = (m_vTab == C_MODQLTRES_PAGE_SCORES);
	bool spload = (m_vTab == C_MODQLTRES_PAGE_SPECLOAD);

	int show_edit = SW_SHOW;
	int show_table = (m_vShow == C_MODQLTRES_SHOW_TABLE) ? SW_SHOW : SW_HIDE;
	int show_axes = (scores) ? SW_SHOW : SW_HIDE;
	int show_gk = (spload) ? SW_SHOW : SW_HIDE;

	RebuildTableBig();

	GetDlgItem(IDC_MODEL_RESULT_TABLEBIG)->ShowWindow(show_table);

	pChart->GetWnd()->ShowWindow(!show_table);

	GetDlgItem(IDC_STATIC_3)->ShowWindow(out);
	GetDlgItem(IDC_REFRESH)->ShowWindow(out);
	GetDlgItem(IDC_MAXMAH)->ShowWindow(out);
	GetDlgItem(IDC_STATIC_10)->ShowWindow(out);
	GetDlgItem(IDC_CALIBVALID)->ShowWindow(out);
	GetDlgItem(IDC_STATIC_13)->ShowWindow(out);
	GetDlgItem(IDC_EXCLUDE)->ShowWindow(show_edit);
	GetDlgItem(IDC_BACK)->ShowWindow(show_edit);
	GetDlgItem(IDC_BACK_ALL)->ShowWindow(show_edit);
	GetDlgItem(IDC_SETTINGS)->ShowWindow(show_gk);
	GetDlgItem(IDC_AXE_OX)->ShowWindow(show_axes);
	GetDlgItem(IDC_AXE_OY)->ShowWindow(show_axes);
	GetDlgItem(IDC_STATIC_14)->ShowWindow(show_axes);
	GetDlgItem(IDC_STATIC_15)->ShowWindow(show_axes);
	GetDlgItem(IDC_STATIC_16)->ShowWindow(show_axes);

	CString s;

	s = (spload) ? ParcelLoadString(STRID_MODEL_QLT_RESULT_EXCLUDEPNT)
		: ParcelLoadString(STRID_MODEL_QLT_RESULT_EXCLUDESPEC);
	GetDlgItem(IDC_EXCLUDE)->SetWindowText(s);

	m_Show.ResetContent();
	s = ParcelLoadString(STRID_MODEL_QLT_RESULT_SHOWASTAB);
	m_Show.AddString(s);
	if(out)
		s = ParcelLoadString(STRID_MODEL_QLT_RESULT_SHOWASHIST);
	else
		s = ParcelLoadString(STRID_MODEL_QLT_RESULT_SHOWASPLOT);
	m_Show.AddString(s);
	m_Show.SetCurSel(m_vShow);

	if(!show_table)
		SelShowGraphs(graph);

	RedrawWindow();
}

void CModelQltResultDlg::RebuildTableBig()
{
	m_listBig.DeleteAllItems();

	CString s;
	int i;
	int nColumnCount = m_listBig.GetHeaderCtrl()->GetItemCount();
	for(i=1; i<nColumnCount; i++)
		m_listBig.DeleteColumn(1);

	if(m_vTab == C_MODQLTRES_PAGE_OUT)
	{
		s = ParcelLoadString(STRID_MODEL_QLT_RESULT_OUT_COL_SPEC);
		m_listBig.InsertColumn(1, s, LVCFMT_LEFT, 0, 1);
		s = ParcelLoadString(STRID_MODEL_QLT_RESULT_OUT_COL_MAH);
		m_listBig.InsertColumn(2, s, LVCFMT_RIGHT, 0, 2);
		s = ParcelLoadString(STRID_MODEL_QLT_RESULT_OUT_COL_EXCEED);
		m_listBig.InsertColumn(3, s, LVCFMT_RIGHT, 0, 3);
		for(i=0; i<4; i++)
			m_listBig.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	}
	if(m_vTab == C_MODQLTRES_PAGE_SCORES)
	{
		s = ParcelLoadString(STRID_MODEL_QLT_RESULT_SCORES_COL_SPEC);
		m_listBig.InsertColumn(1, s, LVCFMT_LEFT, 0, 1);

		CString fmt = ParcelLoadString(STRID_MODEL_QLT_RESULT_SCORES_COL_GK);
		int NGK = ModData->GetNCompQlt();
		for(int m=0; m<NGK; m++)
		{
			s.Format(fmt, m + 1);
			m_listBig.InsertColumn(m + 2, s, LVCFMT_RIGHT, 0, 2);
		}
		for(i=0; i<NGK+2; i++)
			m_listBig.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	}
	if(m_vTab == C_MODQLTRES_PAGE_SPECLOAD)
	{
		s = ParcelLoadString(STRID_MODEL_QLT_RESULT_SPECLOAD_COL_WAVENUM);
		m_listBig.InsertColumn(1, s, LVCFMT_LEFT, 0, 1);

		CString fmt = ParcelLoadString(STRID_MODEL_QLT_RESULT_SCORES_COL_GK);
		int NGK = ModData->GetNCompQlt();
		for(int m=0; m<NGK; m++)
		{
			s.Format(fmt, m + 1);
			m_listBig.InsertColumn(m + 2, s, LVCFMT_RIGHT, 0, 2);
		}
		for(i=0; i<NGK+2; i++)
			m_listBig.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	}

	FillTableBig();
}

void CModelQltResultDlg::FillTableBig()
{
	m_listBig.DeleteAllItems();

	CString s;
	int i;

	CProjectData *ParentPrj = GetProjectByHItem(ModData->GetParentHItem());

	int N = 0;
	VInt sel;
	if(m_vTab != C_MODQLTRES_PAGE_SPECLOAD)
	{
		bool isValid = (m_vTab == C_MODQLTRES_PAGE_OUT && m_vCalibValid == C_MODQLTRES_SMPL_VALID);

		int iList = 0;
		if(!isValid)
		{
			ItStr itCp;
			for(itCp=selC.begin(); itCp!=selC.end(); ++itCp)
				sel.push_back(ModData->ModelResult.GetCalibIndByName(*itCp));

			N = ModData->ModelResult.GetNCalibResult();
			for(iList=0; iList<N; iList++)
			{
				SpectrumResult* pSpR = ModData->ModelResult.GetCalibResult(iList);

				s.Format(cFmt1, iList+1);
				m_listBig.InsertItem(iList, s);
				m_listBig.SetItemText(iList, 1, pSpR->Name);

				if(m_vTab == C_MODQLTRES_PAGE_OUT)
				{
					s.Format(cFmt42, pSpR->Out_Mah);
					m_listBig.SetItemText(iList, 2, s);
					s.Format(cFmt42, m_vMaxMah - pSpR->Out_Mah);
					if(pSpR->Out_Mah > m_vMaxMah)
						s += cWarn;
					m_listBig.SetItemText(iList, 3, s);
				}
				if(m_vTab == C_MODQLTRES_PAGE_SCORES)
				{
					int NGK = ModData->GetNCompQlt();
					for(int m=0; m<NGK; m++)
					{
						s.Format(cFmt86, pSpR->Scores_GK[m]);
						m_listBig.SetItemText(iList, m + 2, s);
					}
				}
			}
		}
		else
		{
			ItStr itCp;
			for(itCp=selV.begin(); itCp!=selV.end(); ++itCp)
				sel.push_back(ModData->ModelResult.GetValidIndByName(*itCp));

			N = ModData->ModelResult.GetNValidResult();
			for(iList=0; iList<N; iList++)
			{
				SpectrumValResult* pSpVR = ModData->ModelResult.GetValidResult(iList);

				s.Format(cFmt1, iList+1);
				m_listBig.InsertItem(iList, s);
				m_listBig.SetItemText(iList, 1, pSpVR->Name);

				if(m_vTab == C_MODQLTRES_PAGE_OUT)
				{
					s.Format(cFmt42, pSpVR->Out_Mah);
					m_listBig.SetItemText(iList, 2, s);
					s.Format(cFmt42, m_vMaxMah - pSpVR->Out_Mah);
					if(pSpVR->Out_Mah > m_vMaxMah)
						s += cWarn;
					m_listBig.SetItemText(iList, 3, s);
				}
			}
		}

		if(!IsAvr)
			s.Format(ParcelLoadString(STRID_MODEL_QLT_RESULT_ALLNUM), N);
		else
			s.Format(ParcelLoadString(STRID_MODEL_QLT_RESULT_ALLNUM_AVR), N);
		GetDlgItem(IDC_STATIC_1)->SetWindowText(s);
	}

	if(m_vTab == C_MODQLTRES_PAGE_SPECLOAD)
	{
//		sel.push_back(0);
		N = ModData->ModelResult.GetNLoadingResult();
		for(int ispec=0; ispec<N; ispec++)
		{
			LoadingResult *pLR = ModData->ModelResult.GetLoadingResultByInd(ispec);
			s.Format(cFmt1, pLR->SpecPoint + 1);
			m_listBig.InsertItem(ispec, s);
			s.Format(cFmt86, ParentPrj->GetSpecPoint(pLR->SpecPoint));
			m_listBig.SetItemText(ispec, 1, s);

			int NGK = ModData->GetNCompQlt();
			for(int m=0; m<NGK; m++)
			{
				s.Format(cFmt86, pLR->Spec_GK[m]);
				m_listBig.SetItemText(ispec, m + 2, s);
			}

			s.Format(ParcelLoadString(STRID_MODEL_QLT_RESULT_SPECLOAD_ALLNUM), N);
			GetDlgItem(IDC_STATIC_1)->SetWindowText(s);
		}
	}

	SetSortIcons();
	for(i=0; i<m_listBig.GetHeaderCtrl()->GetItemCount(); i++)
		m_listBig.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);

	ItInt itS;
	for(itS=sel.begin(); itS!=sel.end(); ++itS)
		m_listBig.SetItemState((*itS), LVIS_SELECTED, LVIS_SELECTED);
}

void CModelQltResultDlg::SortAll()
{
	int prop = ModData->ModelResult.SortCalibProp;
	bool dir = ModData->ModelResult.SortCalibDir;

	ModData->ModelResult.SortCalib(prop, dir, 0, m_vMaxMah);

	prop = ModData->ModelResult.SortValidProp;
	dir = ModData->ModelResult.SortValidDir;

	ModData->ModelResult.SortValid(prop, dir, 0, m_vMaxMah);
}

void CModelQltResultDlg::SetSortIcons()
{
	bool isValid = GetCurCalibValid();

	int prop = (!isValid) ? ModData->ModelResult.SortCalibProp : ModData->ModelResult.SortValidProp;
	bool dir = (!isValid) ? ModData->ModelResult.SortCalibDir : ModData->ModelResult.SortValidDir;

	int iCol = -1;
	switch(m_vTab)
	{
	case C_MODQLTRES_PAGE_OUT:
		if(!isValid)
		{
			if(prop == C_MODRES_SORT_CALIB_NAME)  iCol = 1;
			if(prop == C_MODRES_SORT_CALIB_OUT_MD)  iCol = 2;
			if(prop == C_MODRES_SORT_CALIB_OUT_EXCEED)  iCol = 3;
		}
		else
		{
			if(ModData->GetNSamplesForValid() <= 0)
				break;
			if(prop == C_MODRES_SORT_VALID_NAME)  iCol = 1;
			if(prop == C_MODRES_SORT_VALID_MD)  iCol = 2;
			if(prop == C_MODRES_SORT_VALID_EXCEED)  iCol = 3;
		}
		break;
	case C_MODQLTRES_PAGE_SCORES:
		if(prop == C_MODRES_SORT_CALIB_NAME)  iCol = 1;
		if(prop >= C_MODRES_SORT_CALIB_GK_0)  iCol = 2 + (prop - C_MODRES_SORT_CALIB_GK_0);
		break;
	}

	m_listBig.ShowSortIcons(iCol, dir);
}

bool CModelQltResultDlg::ExcludeSpectra()
{
	if(m_vTab == C_MODQLTRES_PAGE_SPECLOAD)
		return false;

	bool isValid = GetCurCalibValid();
	if(!isValid && int(selC.size()) >= ModData->ModelResult.GetNCalibResult())
	{
		CString s = ParcelLoadString(STRID_MODEL_QLT_RESULT_ERROR_NOCALIB);
		ParcelError(s);
		return false;
	}

	VStr* pSel = (isValid) ? &selV : &selC;
	ItStr it;
	for(it=pSel->begin(); it!=pSel->end(); ++it)
	{
		if(!isValid)
		{
			SpectrumResult* pSpR = ModData->ModelResult.GetCalibResult(*it);
			if(pSpR == NULL)
				continue;

			ModelSample* MSam = ModData->GetSampleForCalib(pSpR->SamName, pSpR->TransName);
			if(MSam == NULL)
				continue;

			if(!IsAvr)
				MSam->Spectra[pSpR->SpecNum] = false;

			if(IsAvr || MSam->GetNSpectraUsed() <= 0)
				ModData->DeleteSampleForCalib(pSpR->SamName, pSpR->TransName);
		}
		else
		{
			SpectrumValResult* pSpVR = ModData->ModelResult.GetValidResult(*it);
			if(pSpVR == NULL)
				continue;

			ModelSample* MSam = ModData->GetSampleForValid(pSpVR->SamName, pSpVR->TransName);
			if(MSam == NULL)
				continue;

			if(!IsAvr)
				MSam->Spectra[pSpVR->SpecNum] = false;

			if(IsAvr || MSam->GetNSpectraUsed() <= 0)
				ModData->DeleteSampleForValid(pSpVR->SamName, pSpVR->TransName);
		}
	}

	pSel->clear();

	return true;
}

bool CModelQltResultDlg::ExcludePoints()
{
	if(m_vTab != C_MODQLTRES_PAGE_SPECLOAD)
		return false;

	int N = ModData->ModelResult.GetNLoadingResult();

	int Ind = -1, nsel = m_listBig.GetSelectedCount();
	if(nsel <= 0)
		return false;

	SInt Points;
	ModData->GetExcPoints(Points);
	for(int i=0; i<nsel; i++)
	{
		Ind = m_listBig.GetNextItem(Ind, LVNI_SELECTED);
		if(Ind < 0 || Ind >= N)
			break;

		LoadingResult *pLR = ModData->ModelResult.GetLoadingResultByInd(Ind);
		if(pLR == NULL)
			return false;

		Points.insert(pLR->SpecPoint);
	}
	ModData->SetExcPoints(Points);

	return true;
}

void CModelQltResultDlg::CreateHistoCalib()
{
	if(pHistoCalib != NULL)
	{
		pChart->RemoveGraph(pHistoCalib->GetPos());
		delete pHistoCalib;
		pHistoCalib = NULL;
	}
    if(pMarkHCalib != NULL)
	{
		delete[] pMarkHCalib;
		pMarkHCalib = NULL;
	}

	ModData->ModelResult.CreateHistoCalib();
	pHICalib = ModData->ModelResult.GetHistoCalib();

	pHistoCalib = new CLumChartDataSet(pHICalib->N, 1, 1, pHICalib->y);
	pHistoCalib->SetInterpolation(LCF_CONNECT_HISTO);
	pHistoCalib->SetColor(cColorBlack);
	pHistoCalib->SetMarkerColor(cColorPass);
	pHistoCalib->SetName(ParcelLoadString(STRID_MODEL_QLT_RESULT_GRAPH_HISTOCALIBNAME));
	pMarkHCalib = new CLumChartDataMarker[pHICalib->N];
	for(int i=0; i<pHICalib->N; i++)
	{
		pMarkHCalib[i].nColor = cColorPass;
		pMarkHCalib[i].nSize = 3;
		pMarkHCalib[i].nType = LCF_MARKERS_ROUND;
	}
	pHistoCalib->SetMarkerArray(pMarkHCalib);
	pHistoCalib->EnableUserMarkers(true);
	pChart->AddGraph(pHistoCalib, false);

	ColoredHistoCalib();
}

void CModelQltResultDlg::CreateHistoValid()
{
	if(pHistoValid != NULL)
	{
		pChart->RemoveGraph(pHistoValid->GetPos());
		delete pHistoValid;
		pHistoValid = NULL;
	}
	if(pMarkHValid != NULL)
	{
		delete[] pMarkHValid;
		pMarkHValid = NULL;
	}

	ModData->ModelResult.CreateHistoValid();
	pHIValid = ModData->ModelResult.GetHistoValid();

	if(pHIValid->N <= 1)
		return;

	pHistoValid = new CLumChartDataSet(pHIValid->N, 1, 1, pHIValid->y);
	pHistoValid->SetInterpolation(LCF_CONNECT_HISTO);
	pHistoValid->SetColor(cColorBlack);
	pHistoValid->SetMarkerColor(cColorPass);
	pHistoValid->SetName(ParcelLoadString(STRID_MODEL_QLT_RESULT_GRAPH_HISTOVALIDNAME));
	pMarkHValid = new CLumChartDataMarker[pHIValid->N];
	for(int i=0; i<pHIValid->N; i++)
	{
		pMarkHValid[i].nColor = cColorPass;
		pMarkHValid[i].nSize = 3;
		pMarkHValid[i].nType = LCF_MARKERS_ROUND;
	}
	pHistoValid->SetMarkerArray(pMarkHValid);
	pHistoValid->EnableUserMarkers(true);
	pChart->AddGraph(pHistoValid, false);

	ColoredHistoValid();
}

void CModelQltResultDlg::CreateGraphScores()
{
	if(pGraphScores != NULL)
	{
		pChart->RemoveGraph(pGraphScores->GetPos());
		delete pGraphScores;
		pGraphScores = NULL;
	}
	if(pMarkScores != NULL)
	{
		delete[] pMarkScores;
		pMarkScores = NULL;
	}

	ModData->ModelResult.CreateGraphScores(m_vOXsc, m_vOYsc);
	pGIScores = ModData->ModelResult.GetGraphScores();

	pGraphScores = new CLumChartDataSet(pGIScores->N, pGIScores->x, pGIScores->y);
	pGraphScores->SetInterpolation(LCF_CONNECT_NONE);
	pGraphScores->SetColor(cColorBlack);
	pGraphScores->SetMarkerColor(cColorGraphNorm);
	pGraphScores->SetMarkerType(LCF_MARKERS_ROUND);
	pGraphScores->SetMarkerSize(3);
	pGraphScores->SetName(ParcelLoadString(STRID_MODEL_QLT_RESULT_GRAPH_SCORESNAME));

	pMarkScores = new CLumChartDataMarker[pGIScores->N];
	for(int i=0; i<pGIScores->N; i++)
	{
		pMarkScores[i].nColor = cColorGraphNorm;
		pMarkScores[i].nSize = 3;
		pMarkScores[i].nType = LCF_MARKERS_ROUND;
	}
	pGraphScores->SetMarkerArray(pMarkScores);
	pGraphScores->EnableUserMarkers(true);
	pChart->AddGraph(pGraphScores, false);

	ColoredGraphScores();
}

void CModelQltResultDlg::CreateGraphsSpecLoad()
{
	vector<CLumChartDataSet>::iterator it;
	for(it=GraphsSpecLoad.begin(); it!=GraphsSpecLoad.end(); ++it)
		pChart->RemoveGraph(it->GetPos());
	GraphsSpecLoad.clear();

	vector<POSITION>::iterator it3;
	for(it3=MasksSLPos.begin(); it3!=MasksSLPos.end(); ++it3)
		pChart->RemoveMask(*it3);
	MasksSLPos.clear();

	ModData->ModelResult.CreateGraphsSpecLoad(ModData->GetParentHItem());

	int i;
	for(i=0; i<ModData->GetNCompQlt(); i++)
	{
		GraphInfo* pGI = ModData->ModelResult.GetGraphSpecLoad(i);

		CLumChartDataSet Graph(pGI->N, pGI->x, pGI->y);
		Graph.SetInterpolation(LCF_CONNECT_DIRECT);
		Graph.SetColor(cGetDefColor(i));
		Graph.SetMarkerType(LCF_MARKERS_NONE);

		CString s;
		s.Format(ParcelLoadString(STRID_MODEL_QLT_RESULT_GRAPH_SPLOADNAME), i + 1);
		Graph.SetName(s);

		pChart->AddGraph(&Graph, false);
		GraphsSpecLoad.push_back(Graph);
	}
}

void CModelQltResultDlg::CreateHistoMDL()
{
	if(pHistoMDL != NULL)
	{
		pChart->RemoveGraph(pHistoMDL->GetPos());
		delete pHistoMDL;
		pHistoMDL = NULL;
	}

	double N = double(max(pHICalib->N, pHIValid->N));
	pHistoMDL = new CLumChartDataSet(0., N, fHistoMDL);
	pHistoMDL->SetColor(cColorErr);
	pHistoMDL->SetName(ParcelLoadString(STRID_MODEL_QLT_RESULT_GRAPH_LINEMAXNAME));
	pChart->AddGraph(pHistoMDL, false);
}

void CModelQltResultDlg::CreateLegendOut()
{
	if(posLegendOut != NULL)
	{
		pChart->RemoveUserLegend(posLegendOut);
		posLegendOut = NULL;
	}

	CRect RectGraph;
	GetDlgItem(IDC_MODEL_RESULT_TABLEBIG)->GetWindowRect(RectGraph);

	vector<CLCULString> strings;

	CLCULString NewStringMDL;
	NewStringMDL.SetText(ParcelLoadString(STRID_MODEL_RESULT_GRAPH_LINEMAXNAME));
	NewStringMDL.SetLine(cColorErr, PS_SOLID, 1);
	NewStringMDL.EnableLine(true);

	strings.clear();
	if(show_mdl)  strings.push_back(NewStringMDL);

	CLCUL legendOut;
	legendOut.MoveTo(RectGraph.Width() - 100, 0);
	legendOut.Resize(100, 20);
	legendOut.SetStrings(strings);
	legendOut.SetFillColor(cColorGraphNorm);
	legendOut.SetTextColor(cColorBlack);
	legendOut.Enable(false);
	legendOut.EnableMoveToFit(true);
	legendOut.EnableAutoWidth(true);
	legendOut.EnableAutoHeight(true);
	posLegendOut = pChart->AddUserLegend(legendOut);
}

void CModelQltResultDlg::CreateLegendSpec()
{
	if(posLegendSpec != NULL)
	{
		pChart->RemoveUserLegend(posLegendSpec);
		posLegendSpec = NULL;
	}

	CRect RectGraph;
	GetDlgItem(IDC_MODEL_RESULT_TABLEBIG)->GetWindowRect(RectGraph);

	vector<CLCULString> strings;

	int i;
	for(i=0; i<ModData->GetNCompQlt(); i++)
	{
		GraphInfo* pGI = ModData->ModelResult.GetGraphSpecLoad(i);
		if(!pGI->sel)
			continue;

		CLCULString NewString;
		CString s;
		s.Format(ParcelLoadString(STRID_MODEL_QLT_RESULT_GRAPH_SPLOADNAME), i + 1);
		NewString.SetText(s);
		NewString.SetLine(cGetDefColor(i), PS_SOLID, 1);
		NewString.EnableLine(true);

		strings.push_back(NewString);
	}

	CLCUL legendSpec;
	legendSpec.MoveTo(RectGraph.Width() - 100, 0);
	legendSpec.Resize(100, 20);
	legendSpec.SetStrings(strings);
	legendSpec.SetFillColor(cColorGraphNorm);
	legendSpec.SetTextColor(cColorBlack);
	legendSpec.Enable(false);
	legendSpec.EnableMoveToFit(true);
	legendSpec.EnableAutoWidth(true);
	legendSpec.EnableAutoHeight(true);
	posLegendSpec = pChart->AddUserLegend(legendSpec);
}

void CModelQltResultDlg::CreateAllGraphs()
{
	CreateHistoCalib();
	CreateHistoValid();
	CreateHistoMDL();
	CreateGraphScores();
	CreateGraphsSpecLoad();
	CreateLegendOut();
	CreateLegendSpec();
}

void CModelQltResultDlg::CreateChart()
{
	CRect Rect1, RectGraph;
	GetDlgItem(IDC_MODEL_RESULT_TABLEBIG)->GetWindowRect(RectGraph);

	pChart = new CLumChart(RectGraph, this, LCF_RECT_SCREEN);
	pChart->SetAxesNames(ParcelLoadString(STRID_MODEL_QLT_RESULT_GRAPH_AXISSMPL),
		ParcelLoadString(STRID_MODEL_QLT_RESULT_GRAPH_AXISMDL));

	CreateAllGraphs();

	COLORREF bg = GetSysColor(COLOR_3DFACE);
	pChart->SetBgOutsideColor(bg, cColorBlack);
	pChart->SetMargins(45, 30, 15, 50);
	pChart->SetSelFrameProps(RGB(0, 0, 255), 2);
	pChart->SetHighlightMode(false);

	pChart->SetSolidAxisHorzAt(0.);
	pChart->SetSolidAxisVertAt(0.);

	pChart->ShowLegend(false);
	pChart->EnableUpperLegend(false);
	pChart->EnableCoordLegend(false);

	pChart->SetUserCallback(LCF_CB_LBDOWN, LbClickCallback);
	pChart->SetUserCallback(LCF_CB_LBUP, LbUnClickCallback);
	pChart->SetUserCallback(LCF_CB_RBDOWN, RbClickCallback);
	pChart->SetUserCallback(LCF_CB_RBUP, RbUnClickCallback);
	pChart->EnableUserCallback(LCF_CB_LBDOWN, true);
	pChart->EnableUserCallback(LCF_CB_LBUP, true);
	pChart->EnableUserCallback(LCF_CB_RBDOWN, true);
	pChart->EnableUserCallback(LCF_CB_RBUP, true);

	BuildUserPopupMenu();
	pChart->SetUserPopupMenu(this, pMenu1);
	pChart->EnableUserPopupMenu(true);
}

void CModelQltResultDlg::ColoredHistoCalib()
{
	if(pHistoCalib == NULL || pMarkHCalib == NULL)
		return;

	ItSpecRes it;
	for(it=ModData->ModelResult.CalibResult.begin(); it!=ModData->ModelResult.CalibResult.end(); ++it)
	{
		CString SpName = it->Name;
		SpectrumResult* pSpR = ModData->ModelResult.GetCalibResult(SpName);

		int ind = ModData->ModelResult.GetHistoCalib()->GetIndByName(SpName);
		if(ind >= 0 && ind < ModData->ModelResult.GetHistoCalib()->N)
		{
			if(pSpR->Out_Mah > m_vMaxMah)
				pMarkHCalib[ind].nColor = cColorErr;
			else
				pMarkHCalib[ind].nColor = cColorPass;

			if(find(selC.begin(), selC.end(), SpName) != selC.end())
				pMarkHCalib[ind].nColor = cColorSel;
		}
	}
}

void CModelQltResultDlg::ColoredHistoValid()
{
	if(pHistoValid == NULL || pMarkHValid == NULL)
		return;

	ItSpecValRes it;
	for(it=ModData->ModelResult.ValidResult.begin(); it!=ModData->ModelResult.ValidResult.end(); ++it)
	{
		CString SpName = it->Name;
		SpectrumValResult* pSpVR = ModData->ModelResult.GetValidResult(SpName);

		int ind = ModData->ModelResult.GetHistoValid()->GetIndByName(SpName);
		if(ind >= 0 && ind < ModData->ModelResult.GetHistoValid()->N)
		{
			if(pSpVR->Out_Mah > m_vMaxMah)
				pMarkHValid[ind].nColor = cColorErr;
			else
				pMarkHValid[ind].nColor = cColorPass;

			if(find(selV.begin(), selV.end(), SpName) != selV.end())
				pMarkHValid[ind].nColor = cColorSel;
		}
	}
}

void CModelQltResultDlg::ColoredGraphScores()
{
	GraphInfo* pGI = ModData->ModelResult.GetGraphScores();
	if(pGraphScores == NULL || pMarkScores == NULL || pGI == NULL)
		return;

	ItSpecRes it;
	for(it=ModData->ModelResult.CalibResult.begin(); it!=ModData->ModelResult.CalibResult.end(); ++it)
	{
		CString SpName = it->Name;

		int ind = pGI->GetIndByName(SpName);
		if(ind >= 0 && ind < pGI->N)
		{
			if(find(selC.begin(), selC.end(), SpName) != selC.end())
				pMarkScores[ind].nColor = cColorSel;
			else
				pMarkScores[ind].nColor = cColorGraphNorm;
		}
	}
}

void CModelQltResultDlg::ColoredAllGraphs()
{
	ColoredHistoCalib();
	ColoredHistoValid();
	ColoredGraphScores();
}

void CModelQltResultDlg::SelShowGraphs(int graph)
{
	if(pHistoCalib)
		pChart->ShowGraph(pHistoCalib->GetPos(), graph == C_PGRAPH_MDCALIB);
	if(pHistoValid)
		pChart->ShowGraph(pHistoValid->GetPos(), graph == C_PGRAPH_MDVALID && pHIValid->N > 1);
	pChart->ShowGraph(pHistoMDL->GetPos(), (graph == C_PGRAPH_MDCALIB || graph == C_PGRAPH_MDVALID) && show_mdl);
	if(posLegendOut != NULL)
		pChart->GetUserLegend(posLegendOut)->Enable((graph == C_PGRAPH_MDCALIB || graph == C_PGRAPH_MDVALID)
			&& show_mdl && show_legend);

	pChart->ShowGraph(pGraphScores->GetPos(), graph == C_PGRAPH_SCORES);

	if(graph == C_PGRAPH_SPLOAD)
	{
		VDblDbl* Masks = ModData->ModelResult.GetAllMasksSpecLoad();
		ItVDblDbl it4;
		for(it4=Masks->begin(); it4!=Masks->end(); ++it4)
		{
			POSITION Pos = pChart->AddMask((*it4).first, (*it4).second, false, true, cColorPass);
			MasksSLPos.push_back(Pos);
		}
	}
	else
	{
		vector<POSITION>::iterator it3;
		for(it3=MasksSLPos.begin(); it3!=MasksSLPos.end(); ++it3)
			pChart->RemoveMask(*it3);
		MasksSLPos.clear();
	}
	int j;
	vector<CLumChartDataSet>::iterator it;
	for(j=0, it=GraphsSpecLoad.begin(); it!=GraphsSpecLoad.end(); ++it, j++)
	{
		GraphInfo* pGI = ModData->ModelResult.GetGraphSpecLoad(j);
		pChart->ShowGraph(it->GetPos(), (graph == C_PGRAPH_SPLOAD && pGI->sel));
	}
	vector<POSITION>::iterator it3;
	for(it3=MasksSLPos.begin(); it3!=MasksSLPos.end(); ++it3)
		pChart->ShowGraph((*it3), graph == C_PGRAPH_SPLOAD);
	if(posLegendSpec != NULL)
		pChart->GetUserLegend(posLegendSpec)->Enable(graph == C_PGRAPH_SPLOAD && IsLegendEnable() && show_legend);

	GraphInfo* pGI = NULL;
	switch(graph)
	{
	case C_PGRAPH_MDCALIB:  pGI = ModData->ModelResult.GetHistoCalib();  break;
	case C_PGRAPH_MDVALID:
		if(pHIValid->N > 1)  pGI = ModData->ModelResult.GetHistoCalib();
		break;
	case C_PGRAPH_SCORES:  pGI = ModData->ModelResult.GetGraphScores();  break;
	}
	if(pGI != NULL || graph == C_PGRAPH_SPLOAD)
	{
		double min, max, plus, Ymax, minX, minY, maxX, maxY, plusY;
		vector<CLumChartDataSet>::iterator it;
		switch(graph)
		{
		case C_PGRAPH_SCORES:
			min = min(pGI->xmin, pGI->ymin);
			max = max(pGI->xmax, pGI->ymax);
			plus = (max - min) * 0.05;
			if(fabs(plus) < 1.e-9)  plus = max * 0.1;
			pChart->SetUserViewPort(min - plus, min - plus, max + plus, max + plus);
			break;
		case C_PGRAPH_MDCALIB:
		case C_PGRAPH_MDVALID:
			Ymax = max(GetMaxMah(), pGI->ymax);
			pChart->SetUserViewPort(1, 0, pGI->N, Ymax * 1.1);
			break;
		case C_PGRAPH_SPLOAD:
			minX = 1.e9;  maxX = -1.e9;  minY = 1.e9;  maxY = -1.e9;
			for(j=0, it=GraphsSpecLoad.begin(); it!=GraphsSpecLoad.end(); ++it, j++)
			{
				pGI = ModData->ModelResult.GetGraphSpecLoad(j);
				if(!pGI->sel)
					continue;

				minX = min(minX, pGI->xmin);
				maxX = max(maxX, pGI->xmax);
				minY = min(minY, pGI->ymin);
				maxY = max(maxY, pGI->ymax);
			}
			plusY = (maxY - minY) * 0.05;
			if(fabs(plusY) < 1.e-9)  plusY = maxY * 0.1;

			pChart->SetUserViewPort(minX, minY - plusY, maxX, maxY + plusY);
		}
	}

	CString ox = cEmpty, oy = cEmpty;
	CString fmt = ParcelLoadString(STRID_MODEL_QLT_RESULT_GRAPH_AXISGK);
	switch(graph)
	{
	case C_PGRAPH_MDCALIB:
	case C_PGRAPH_MDVALID:
		ox = ParcelLoadString(STRID_MODEL_QLT_RESULT_GRAPH_AXISSMPL);
		oy = ParcelLoadString(STRID_MODEL_QLT_RESULT_GRAPH_AXISMDL);
		break;
	case C_PGRAPH_SCORES:
		ox.Format(fmt, m_vOXsc + 1);
		oy.Format(fmt, m_vOYsc + 1);
		break;
	case C_PGRAPH_SPLOAD:
		ox = ParcelLoadString(STRID_MODEL_QLT_RESULT_GRAPH_AXISPNT);
		oy = ParcelLoadString(STRID_MODEL_QLT_RESULT_GRAPH_AXISSKIP);
		break;
	}
	pChart->SetAxesNames(ox, oy);

	pChart->EnableSolidAxisHorz(graph == C_PGRAPH_SCORES || graph == C_PGRAPH_SPLOAD);
	pChart->EnableSolidAxisVert(graph == C_PGRAPH_SCORES);
	pChart->EmptyZoomList();
}

void CModelQltResultDlg::MakeGraphPicture(CString FileName, int graph)
{
	int graph_old;
	switch(m_vTab)
	{
	case C_MODQLTRES_PAGE_OUT:
		if(m_vCalibValid == C_MODQLTRES_SMPL_CALIB)  graph_old = C_PGRAPH_MDCALIB;
		else  graph_old = C_PGRAPH_MDVALID;
		break;
	case C_MODQLTRES_PAGE_SCORES:  graph_old = C_PGRAPH_SCORES;  break;
	case C_MODQLTRES_PAGE_SPECLOAD:  graph_old = C_PGRAPH_SPLOAD;  break;
	}

	SelShowGraphs(graph);
	pChart->ExportPicture(FileName);

	SelShowGraphs(graph_old);
}

void CModelQltResultDlg::SelectByRect(CRect* rect, bool isFrame)
{
	if(!isFrame)
	{
		CPoint pnt(int((rect->left + rect->right) / 2), int((rect->top + rect->bottom) / 2));
		int resfind = -1;
		pChart->IsPointInSelRect(pnt, &resfind);
		if(resfind == 1)
			return;
	}

	bool isValid = GetCurCalibValid();
	GraphInfo* pGI = NULL;
	CLumChartDataSet* pGD = NULL;
	VStr* pSel = NULL;

	switch(m_vTab)
	{
	case C_MODQLTRES_PAGE_OUT:
		if(m_vCalibValid == C_MODQLTRES_SMPL_CALIB)
		{
			pGI = ModData->ModelResult.GetHistoCalib();
			pGD = pHistoCalib;
			pSel = &selC;
		}
		else
		{
			pGI = ModData->ModelResult.GetHistoValid();
			pGD = pHistoValid;
			pSel = &selV;
		}
		break;
	case C_MODQLTRES_PAGE_SCORES:
		pGI = ModData->ModelResult.GetGraphScores();
		pGD = pGraphScores;
		pSel = &selC;
		break;
	case C_MODQLTRES_PAGE_SPECLOAD:
		ShowGK(rect);
		return;
	default:
		return;
	}

	if(pGD == NULL || pGI == NULL)
		return;

	int N = pChart->LocateGraphPoints(pGD->GetPos(), (*rect), 0);
	if(N > 0)
	{
		int* pidx = new int[N];
		pChart->LocateGraphPoints(pGD->GetPos(), (*rect), pidx);

		int i;
		bool make_sel = false;
		for(i=0; i<N; i++)
		{
			CString SpName = pGI->GetNameByInd(pidx[i]);
			ItStr itf = find(pSel->begin(), pSel->end(), SpName);
			if(itf == pSel->end())
			{
				make_sel = true;
				break;
			}
		}
		for(i=0; i<N; i++)
		{
			CString SpName = pGI->GetNameByInd(pidx[i]);
			ItStr itf = find(pSel->begin(), pSel->end(), SpName);
			if(itf != pSel->end() && !make_sel)
				pSel->erase(itf);
			if(itf == pSel->end() && make_sel)
				pSel->push_back(SpName);
		}

		if(!isFrame)
		{
			vector<CLCULString> strings;
			for(i=0; i<N; i++)
			{
				CLCULString NewString;
				CString tmpf;

				CString SpName = pGI->GetNameByInd(pidx[i]);

				CString fmt = cEmpty, fmt2 = cEmpty, str = cEmpty;
				SpectrumResult* pSpR = (isValid) ? NULL : ModData->ModelResult.GetCalibResult(SpName);
				SpectrumValResult* pSpVR = (isValid) ? ModData->ModelResult.GetValidResult(SpName) : NULL;
				switch(m_vTab)
				{
				case C_MODQLTRES_PAGE_OUT:
					fmt = cFmt42;
					fmt2.Format(ParcelLoadString(STRID_MODEL_QLT_RESULT_GRAPH_INFO_Y), fmt);
					if(isValid)
						tmpf.Format(fmt2, pSpVR->Out_Mah);
					else
						tmpf.Format(fmt2, pSpR->Out_Mah);
					str = SpName + tmpf;
					break;
				case C_MODQLTRES_PAGE_SCORES:
					fmt = cFmt86;
					fmt2.Format(ParcelLoadString(STRID_MODEL_QLT_RESULT_GRAPH_INFO_XY), fmt, fmt);
					tmpf.Format(fmt2, pSpR->Scores_GK[m_vOYsc], pSpR->Scores_GK[m_vOXsc]);
					str = SpName + tmpf;
					break;
				}

				NewString.SetText(str);
				strings.push_back(NewString);
			}

			CLCUL legend;
			legend.MoveTo(0, 0);
			legend.Resize(100, 20);
			legend.SetStrings(strings);

			legend.SetFillColor(cColorGraphNorm);
			legend.SetTextColor(cColorBlack);

			legend.Enable(true);
			legend.EnableMoveToFit(true);
			legend.EnableAutoWidth(true);
			legend.EnableAutoHeight(true);

			posInfo = pChart->AddUserLegend(legend);
		}

		ColoredAllGraphs();
		pChart->RedrawWindow();

		delete pidx;
	}
}

void CModelQltResultDlg::ShowGK(CRect* rect)
{
	vector<CLCULString> strings;

	int i;
	bool find = false;
	vector<CLumChartDataSet>::iterator it;
	for(i=0, it=GraphsSpecLoad.begin(); it!=GraphsSpecLoad.end(); ++it, i++)
	{
		bool res = false;
		pChart->IsGraphInRect(it->GetPos(), (*rect), &res);
		if(!res)
			continue;

		find = true;

		CString Name;
		Name.Format(ParcelLoadString(STRID_MODEL_QLT_RESULT_GRAPH_SPLOADNAME), i + 1);

		CLCULString NewString;
		NewString.SetText(Name);
		NewString.SetLine(cGetDefColor(i), PS_SOLID, 1);
		NewString.EnableLine(true);

		strings.push_back(NewString);
	}
	if(!find)
		return;

	CLCUL legend;
	legend.MoveTo(0, 0);
	legend.Resize(100, 20);
	legend.SetStrings(strings);

	legend.SetFillColor(cColorGraphNorm);
	legend.SetTextColor(cColorBlack);

	legend.Enable(true);
	legend.EnableMoveToFit(true);
	legend.EnableAutoWidth(true);
	legend.EnableAutoHeight(true);

	posInfo = pChart->AddUserLegend(legend);

	pChart->RedrawWindow();
}

void CModelQltResultDlg::ShowCoord(CPoint* pnt)
{
	if(m_vTab != C_MODQLTRES_PAGE_SPECLOAD)
		return;

	vector<CLCULString> strings;

	CLCULString NewString;

	CString str;
	double X = 0, Y = 0;
	pChart->GetViewPortXY(*pnt, &X, &Y);
	str.Format(ParcelLoadString(STRID_MODEL_QLT_RESULT_GRAPH_INFO_COORD), Y, X);
	NewString.SetText(str);
	strings.push_back(NewString);

	CRect RectGraph;
	GetDlgItem(IDC_MODEL_RESULT_TABLEBIG)->GetWindowRect(RectGraph);

	CLCUL legend;
	legend.MoveTo(0, RectGraph.Height() - 20);
	legend.Resize(100, 20);
	legend.SetStrings(strings);

	legend.SetFillColor(cColorGraphNorm);
	legend.SetTextColor(cColorBlack);

	legend.Enable(true);
	legend.EnableMoveToFit(true);
	legend.EnableAutoWidth(true);
	legend.EnableAutoHeight(true);

	posCoord = pChart->AddUserLegend(legend);
}

void CModelQltResultDlg::RemoveInfo()
{
	if(posInfo != NULL)
	{
		pChart->RemoveUserLegend(posInfo);
		posInfo = NULL;
	}
	if(posCoord != NULL)
	{
		pChart->RemoveUserLegend(posCoord);
		posCoord = NULL;
	}
}

bool CModelQltResultDlg::IsLegendEnable()
{
	bool res = false;
	switch(m_vTab)
	{
	case C_MODQLTRES_PAGE_OUT:
		res = (show_mdl);
		break;
	case C_MODQLTRES_PAGE_SCORES:
		res = false;
		break;
	case C_MODQLTRES_PAGE_SPECLOAD:
		res = (GetNGKShow() > 0);
		break;
	}

	return res;
}

int CModelQltResultDlg::GetNGKShow()
{
	int j, N=0;
	vector<CLumChartDataSet>::iterator it;
	for(j=0, it=GraphsSpecLoad.begin(); it!=GraphsSpecLoad.end(); ++it, j++)
	{
		GraphInfo* pGI = ModData->ModelResult.GetGraphSpecLoad(j);
		if(pGI->sel)  N++;
	}

	return N;
}

void CModelQltResultDlg::BuildUserPopupMenu()
{
	pMenu1->AppendMenu(MF_STRING, C_MODQLTRES_MENU_PREV, ParcelLoadString(STRID_MODEL_QLT_RESULT_GRAPH_MENU_PREV));
	pMenu1->AppendMenu(MF_STRING, C_MODQLTRES_MENU_NEXT, ParcelLoadString(STRID_MODEL_QLT_RESULT_GRAPH_MENU_NEXT));
	pMenu1->AppendMenu(MF_SEPARATOR);
	pMenu1->AppendMenu(MF_STRING, C_MODQLTRES_MENU_FULL, ParcelLoadString(STRID_MODEL_QLT_RESULT_GRAPH_MENU_FULL));
	pMenu1->AppendMenu(MF_STRING, C_MODQLTRES_MENU_AUTO, ParcelLoadString(STRID_MODEL_QLT_RESULT_GRAPH_MENU_AUTO));
	pMenu1->AppendMenu(MF_SEPARATOR);
	pMenu1->AppendMenu(MF_STRING, C_MODQLTRES_MENU_LEFT, ParcelLoadString(STRID_MODEL_QLT_RESULT_GRAPH_MENU_LEFT));
	pMenu1->AppendMenu(MF_STRING, C_MODQLTRES_MENU_RIGHT, ParcelLoadString(STRID_MODEL_QLT_RESULT_GRAPH_MENU_RIGHT));
	pMenu1->AppendMenu(MF_SEPARATOR);
	pMenu1->AppendMenu(MF_STRING, C_MODQLTRES_MENU_LEGEND, ParcelLoadString(STRID_MODEL_QLT_RESULT_GRAPH_MENU_LEGEND));
	pMenu1->AppendMenu(MF_STRING, C_MODQLTRES_MENU_GRID, ParcelLoadString(STRID_MODEL_QLT_RESULT_GRAPH_MENU_GRID));
	pMenu1->AppendMenu(MF_SEPARATOR);
	pMenu1->AppendMenu(MF_STRING, C_MODQLTRES_MENU_MDL, ParcelLoadString(STRID_MODEL_QLT_RESULT_GRAPH_MENU_MDL));
}

void CModelQltResultDlg::SetStatusMenu()
{
	UINT nFlagsE = MF_ENABLED | MF_BYCOMMAND;
	UINT nFlagsD = MF_DISABLED | MF_GRAYED | MF_BYCOMMAND;
	UINT nFlagsC = MF_CHECKED;
	UINT nFlagsU = MF_UNCHECKED;
	UINT nFlags;

	bool btmp = false, btmp2 = false;

	pChart->IsGoBackAvailable(&btmp);
	nFlags = (btmp) ? nFlagsE : nFlagsD;
	pMenu1->EnableMenuItem(C_MODQLTRES_MENU_PREV, nFlags);

	pChart->IsGoForwardAvailable(&btmp);
	nFlags = (btmp) ? nFlagsE : nFlagsD;
	pMenu1->EnableMenuItem(C_MODQLTRES_MENU_NEXT, nFlags);

	btmp2 = IsLegendEnable();
	nFlags = (show_legend && btmp2) ? nFlagsC : nFlagsU;
	pMenu1->CheckMenuItem(C_MODQLTRES_MENU_LEGEND, nFlags);
	nFlags = (btmp2) ? nFlagsE : nFlagsD;
	pMenu1->EnableMenuItem(C_MODQLTRES_MENU_LEGEND, nFlags);

	pChart->IsGridEnabled(&btmp, &btmp2);
	nFlags = (btmp) ? nFlagsC : nFlagsU;
	pMenu1->CheckMenuItem(C_MODQLTRES_MENU_GRID, nFlags);

	btmp2 = (m_vTab == C_MODQLTRES_PAGE_OUT);
	nFlags = (show_mdl) ? nFlagsC : nFlagsU;
	pMenu1->CheckMenuItem(C_MODQLTRES_MENU_MDL, nFlags);
	nFlags = (btmp2) ? nFlagsE : nFlagsD;
	pMenu1->EnableMenuItem(C_MODQLTRES_MENU_MDL, nFlags);
}

void CModelQltResultDlg::OnHelp()
{
	int IdHelp = DLGID_MODEL_RESULT_QLT;

	pFrame->ShowHelp(IdHelp);
}
