#ifndef _CREATE_PROJECT_DLG_H_
#define _CREATE_PROJECT_DLG_H_

#pragma once

#include "Resource.h"
#include "ProjectData.h"

// ��������� ������ ������ �������

const int PRJDLG_MODE_CREATE	 = 0;	// ����� �������� ������ ������� �������
const int PRJDLG_MODE_CHANGE	 = 1;	// ����� ��������� �������� ������� �������
const int PRJDLG_MODE_CREATE_SST = 2;	// ����� �������� ������������ ������� �������
const int PRJDLG_MODE_CHANGE_SST = 3;	// ����� ��������� �������� ������������ ������� �������

///////////////////////////////////////////////////////////////////////////////////////////////////
// ������ ��������/�������������� ������� �������

class CCreateProjectDlg : public CDialog
{
	DECLARE_DYNAMIC(CCreateProjectDlg)

public:

	CCreateProjectDlg(CProjectData* data, int mode = PRJDLG_MODE_CREATE, CWnd* pParent = NULL);	// �����������
	virtual ~CCreateProjectDlg();																// ����������

	enum { IDD = IDD_CREATE_PROJECT };	// ������������� �������

	CEdit m_Name;				// ���� ��� �������������� "����� �������"
	CEdit m_LowSpec;			// ���� ��� �������������� "������ ������� ������������� ���������"
	CEdit m_UpperSpec;			// ���� ��� �������������� "������� ������� ������������� ���������"
	CEdit m_NScanSub;			// ���� ��� �������������� "����� ������ ����������"
	CEdit m_NMeasureSub;		// ���� ��� �������������� "����� ��������� ����������"
	CEdit m_NScanStd;			// ���� ��� �������������� "����� ������ �������"
	CEdit m_BathLength;			// ���� ��� �������������� "����� ������"
	CEdit m_LowTemp;			// ���� ��� �������������� "������� ������� ����������� �������"
	CEdit m_UpperTemp;			// ���� ��� �������������� "�������� ������� ����������� �������"
	CEdit m_MaxDiff;			// ���� ��� �������������� "����������� ������� ����. ������� �� ����. �����. �����"
	CComboBox m_NMeasureStd;	// ������ ��� ������ "����� ��������� �������"
	CComboBox m_ZeroFill;		// ������ ��� ������ "���������� ������"
	CComboBox m_NResolSubj;		// ������ ��� ������ "���������� ����������� ������"
	CComboBox m_NApodizeSubj;	//������ ��� ������ "���������� ����������� ������"
	CComboBox m_NResolRef;		// ������ ��� ������ "���������� �������� ������"
	CComboBox m_NApodizeRef;	// ������ ��� ������ "���������� �������� ������"
	CButton m_UseRef;			// ��������� "�������� ������������� �������� ������"

protected:

	CProjectData *PrjData;		// ��������� �� ������������� ������
	CDeviceData *ParentDev;		// ��������� �� ������, ������������ ��� �������

	int Mode;					// ����� ������

	CString m_vName;			// ��������� �������� "����� �������"
	int m_vLowSpec;				// ��������� �������� "������ ������� ������������� ���������"
	int m_vUpperSpec;			// ��������� �������� "������� ������� ������������� ���������"
	int m_vNScanSub;			// ��������� �������� "����� ������ ����������"
	int m_vNMeasureSub;			// ��������� �������� "����� ��������� ����������"
	int m_vNScanStd;			// ��������� �������� "����� ������ �������"
	double m_vBathLength;		// ��������� �������� "����� ������"
	int m_vLowTemp;				// ��������� �������� "������� ������� ����������� �������"
	int m_vUpperTemp;			// ��������� �������� "�������� ������� ����������� �������"
	int m_vMaxDiff;				// ��������� �������� "����������� ������� ����. ������� �� ����. �����. �����"
	int m_vNMeasureStd;			// ������ ���������� "����� ��������� �������"
	int m_vZeroFill;			// ������ ���������� "���������� ������"
	int m_vNResolSubj;			// ������ ���������� "���������� ����������� ������"
	int m_vNApodizeSubj;		// ������ ��������� "���������� ����������� ������"
	int m_vNResolRef;			// ������ ���������� "���������� �������� ������"
	int m_vNApodizeRef;			// ������ ��������� "���������� �������� ������"
	BOOL m_vUseRef;				// ������������� �������� "�������� ������������� �������� ������"

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();						// ������������� �������

	virtual void OnOK()
	{};
	virtual void OnCancel();					// ��������� ������� "�������� ���� � ����� �� �������"

	afx_msg void OnAccept();					// ��������� ������� "������� ������ � ����� �� �������"
	afx_msg void OnHelp();						// ��������� ������� "������� �������"
	afx_msg void OnCopyFrom();
	afx_msg void OnSubCanalResolChange();		// ��������� ������� "�������� ���������� ����������� ������"
	afx_msg	void OnSetEnablingRefCanal();		// ��������� ������� "���. ��. �������� ������������� �������� ������"

	void SetSettings(bool upd = true);

	bool IsChanged();

	DECLARE_MESSAGE_MAP()
};

#endif
