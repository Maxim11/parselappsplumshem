#include "stdafx.h"

#include "LoadOldDatas.h"
#include "RootData.h"

#include <math.h>

CLoadOldDatas::CLoadOldDatas()
{
}

CLoadOldDatas::~CLoadOldDatas()
{
}

bool CLoadOldDatas::LoadOldProject(CString fname, ProjectOldData &OldPrj)
{
	CDfxCache dfx(fname);

	Checks.Errors.clear();
	Checks.ErrNums.clear();

	CDeviceData* ParentDev = pRoot->GetDeviceByNum(OldPrj.DevNum);
	bool IsImit = (ParentDev->GetType() == C_DEV_TYPE_IMIT);

	Checks.DevNumber = OldPrj.DevNum;
	Checks.ReplaceUnit = _T("%");

	int i;
	CString s;

	CString section = _T("AnalysisProject");

	OldPrj.Name = dfx.GetProfileString(fname, section, _T("strTitle"), _T("NoTitle"));
	OldPrj.Dir = dfx.GetProfileString(fname, section, _T("strSpecDir"), _T("demo"));

	if(ParentDev->GetProject(OldPrj.Name) != NULL)
	{
		s.Format(ParcelLoadString(STRID_ERROR_LOAD_OLD_APJ_NAME), OldPrj.Name);
		ParcelError(s);
		return false;
	}

	int NSamples = dfx.GetProfileInt(fname, section, _T("nNumSamples"), 0);
	OldPrj.SamNames.clear();
	for(i=0; i<NSamples; i++)
	{
		CString field;
		field.Format(_T("Sample%1d"), i+1);

		CString SamName = dfx.GetProfileString(fname, section, field, _T(""));
		OldPrj.SamNames.push_back(SamName);
	}

	section = _T("SpecConf");

	OldPrj.LowSpec = dfx.GetProfileInt(fname, section, _T("nparFmin"), 10000);
	OldPrj.UpperSpec = dfx.GetProfileInt(fname, section, _T("nparFmax"), 20000);
	OldPrj.ResCode = dfx.GetProfileInt(fname, section, _T("nparResCodeSig"), 0);
	OldPrj.Apodize = dfx.GetProfileInt(fname, section, _T("nparApodSig"), 0);
	int tmp = dfx.GetProfileInt(fname, section, _T("nparChanMode"), 0);
	OldPrj.UseRef = (tmp == 0) ? false : true;
	OldPrj.ResCodeRef = dfx.GetProfileInt(fname, section, _T("nparResCodeRef"), 0);
	OldPrj.ApodizeRef = dfx.GetProfileInt(fname, section, _T("nparApodRef"), 0);
	OldPrj.ZeroFilling = dfx.GetProfileInt(fname, section, _T("nparZfillCode"), 0);
	OldPrj.NScanSample = dfx.GetProfileInt(fname, section, _T("nparScansSample"), 1);
	OldPrj.NMeasSample = dfx.GetProfileInt(fname, section, _T("nparSeries"), 1);
	OldPrj.NScanStd = dfx.GetProfileInt(fname, section, _T("nparScansEtalon"), 1);
	OldPrj.NMeasStd = dfx.GetProfileInt(fname, section, _T("nparEtalonMode"), 0);
	OldPrj.BathLength = int(dfx.GetProfileInt(fname, section, _T("nparCellPath"), 0) / 100.);
	OldPrj.MinTemp = dfx.GetProfileInt(fname, section, _T("nparTminSample"), 0);
	OldPrj.MaxTemp = dfx.GetProfileInt(fname, section, _T("nparTmaxSample"), 0);
	OldPrj.DiffTemp = dfx.GetProfileInt(fname, section, _T("nparTdiff"), 0);

	OldPrj.RealRes = ParentDev->GetResolutionFromList(OldPrj.ResCode);	// �����������

	Checks.WaveLen = C_LASER_WAVE;
	Checks.LowSpec = OldPrj.LowSpec;
	Checks.UpperSpec = OldPrj.UpperSpec;
	Checks.Resol = OldPrj.RealRes;
	Checks.Apod = OldPrj.Apodize;
	Checks.Zero = OldPrj.ZeroFilling;

	section = _T("ComponentList");

	Checks.ParNames.clear();
	Checks.EmptyUnit = C_CONVERT_EMPTYUNIT_NOT;
	int NParams = dfx.GetProfileInt(fname, section, _T("ncomp"), 0);
	for(i=0; i<NParams; i++)
	{
		ParamOldData Param;
		CString section;
		section.Format(_T("Component_%1d"), i+1);

		Param.Name = dfx.GetProfileString(fname, section, _T("strName"), _T(""));
		Param.Format = dfx.GetProfileString(fname, section, _T("strFormat"), _T(""));
		Param.Unit = dfx.GetProfileString(fname, section, _T("strUnits"), _T(""));
		Param.HumidRef = dfx.GetProfileDouble(fname, section, _T("fHumidRef"), 0.);

		Param.Name.Trim();
		Param.Format.Trim();
		Param.Unit.Trim();

		if(Param.Unit.IsEmpty())
		{
			Checks.EmptyUnit = C_CONVERT_EMPTYUNIT_EXIST;
			Checks.ErrNums.push_back(C_CONVERT_ERROR_EMPTYUNIT);
			s.Format(cGetConvertError(C_CONVERT_ERROR_EMPTYUNIT), Param.Name);
			Checks.Errors.push_back(s);
		}

		CString Fmt = Param.Format;
		int l = Param.Format.GetLength();
		if(Param.Format[l-1] == 'f')
		{
			CString Fmt = Param.Format;
			Fmt.Delete(0, l-2);
			Fmt.Delete(1);
			Param.IFormat = _tstoi(Fmt);
		}
		else
			Param.IFormat = 0;

		OldPrj.Params.push_back(Param);
		Checks.ParNames.push_back(Param.Name);
	}

	CString path, PathApj = fname;
	int pos = PathApj.ReverseFind('\\');
	int len = PathApj.GetLength();
	PathApj.Delete(pos+1, len-pos-1);
	path = PathApj + _T("SPECTRA\\") + OldPrj.Dir + _T("\\");

	Checks.SamNames.clear();
	Checks.WithoutNumber = C_CONVERT_WITHOUT_NOT;
	Checks.BadWave = C_CONVERT_BADWAVE_NOT;
	ItStr its;
	for(its=OldPrj.SamNames.begin(); its!=OldPrj.SamNames.end(); ++its)
	{
		SampleOldDataFull SamF;
		if(LoadOldSampleFull((*its), path, SamF, IsImit))
		{
			Checks.SamNames.push_back(SamF.OldSam.Name);
			OldPrj.Samples.push_back(SamF);
		}
	}

	WIN32_FIND_DATA FindData;
	CString filemask = PathApj;

	Checks.ModFiles.clear();
	filemask += _T("*.clb");
	HANDLE hFindFile = FindFirstFile(filemask, &FindData);
	if(hFindFile == INVALID_HANDLE_VALUE)
		return true;
	do
	{
		CString File = CString(FindData.cFileName);
		ModelOldData Mod;

		if(LoadOldModel(File, PathApj, OldPrj.Name, Mod, &OldPrj.Samples))
		{
			Mod.FileName = File;
			Checks.ModFiles.push_back(Mod.FileName);
			OldPrj.Models.push_back(Mod);
		}
	}
	while(FindNextFile(hFindFile, &FindData));

	filemask = PathApj + _T("*.mtd");
	hFindFile = FindFirstFile(filemask, &FindData);
	if(hFindFile == INVALID_HANDLE_VALUE)
		return true;
	do
	{
		CString File = CString(FindData.cFileName);
		MethodOldData Mtd;

		if(LoadOldMethod(File, PathApj, OldPrj.Name, Mtd))
			OldPrj.Methods.push_back(Mtd);
	}
	while(FindNextFile(hFindFile, &FindData));

	return true;
}

bool CLoadOldDatas::LoadOldSampleFull(CString name, CString path, SampleOldDataFull &OldSamF, bool IsImit)
{
	CString fname = path + name;
	CString samname = name;
	int len = samname.GetLength();
	samname.Delete(len-4, 4);

	CString s;
	if(!LoadOldSample(fname, OldSamF.OldSam))
	{
		Checks.ErrNums.push_back(C_CONVERT_ERROR_SAMPLE);
		s.Format(cGetConvertError(C_CONVERT_ERROR_SAMPLE), samname);
		Checks.Errors.push_back(s);
		return false;
	}

	OldSamF.RefData.clear();

	CString fnameref = fname;
	fnameref.Replace(_T(".saa"), _T(".ref"));
	if(!LoadOldRef(fnameref, OldSamF.RefData))
	{
		Checks.ErrNums.push_back(C_CONVERT_ERROR_REFDATA);
		s.Format(cGetConvertError(C_CONVERT_ERROR_REFDATA), samname);
		Checks.Errors.push_back(s);
//		return false;
	}

	OldSamF.SpecData.clear();
	for(int i=0; i<OldSamF.OldSam.NSpectra; i++)
	{
		int spnum = OldSamF.OldSam.Spectra[i];
		CString fnamespec = fname;
		CString fin;
		if(spnum < 100)
			fin.Format(_T("-%02d.spa"), spnum);
		else
			fin.Format(_T("-%d.spa"), spnum);
		fnamespec.Replace(_T(".saa"), fin);

		SpectrumOldData OldSpec;
		if(LoadOldSpectrum(fnamespec, OldSpec, IsImit))
		{
			if(Checks.LowSpec != OldSpec.PrjLowSpec)
			{
				Checks.ErrNums.push_back(C_CONVERT_ERROR_LOWLIM);
				s.Format(cGetConvertError(C_CONVERT_ERROR_LOWLIM), fnamespec);
				Checks.Errors.push_back(s);
				continue;
			}
			if(Checks.UpperSpec != OldSpec.PrjUpperSpec)
			{
				Checks.ErrNums.push_back(C_CONVERT_ERROR_UPPERLIM);
				s.Format(cGetConvertError(C_CONVERT_ERROR_UPPERLIM), fnamespec);
				Checks.Errors.push_back(s);
				continue;
			}

			if(!IsImit)
			{
				if(OldSpec.DevNumber == 0)
				{
					Checks.WithoutNumber = C_CONVERT_WITHOUT_DECLINE;
					Checks.ErrNums.push_back(C_CONVERT_ERROR_ZERONUM);
					s.Format(cGetConvertError(C_CONVERT_ERROR_ZERONUM), fnamespec);
					Checks.Errors.push_back(s);
				}
				if(Checks.DevNumber != OldSpec.DevNumber && OldSpec.DevNumber > 0)
				{
					Checks.ErrNums.push_back(C_CONVERT_ERROR_BADNUM);
					s.Format(cGetConvertError(C_CONVERT_ERROR_BADNUM), fnamespec);
					Checks.Errors.push_back(s);
					continue;
				}
				if(fabs(OldSpec.WaveLength - Checks.WaveLen) > 1.e-9)
				{
					Checks.BadWave = C_CONVERT_BADWAVE_DECLINE;
					Checks.ErrNums.push_back(C_CONVERT_ERROR_WAVE);
					s.Format(cGetConvertError(C_CONVERT_ERROR_WAVE), fnamespec);
					Checks.Errors.push_back(s);
	//				continue;
				}
				if(fabs(Checks.Resol - OldSpec.PrjResolution) > 1.e-3)
				{
					Checks.ErrNums.push_back(C_CONVERT_ERROR_RESOL);
					s.Format(cGetConvertError(C_CONVERT_ERROR_RESOL), fnamespec);
					Checks.Errors.push_back(s);
					continue;
				}
				if(Checks.Apod != OldSpec.PrjApodize)
				{
					Checks.ErrNums.push_back(C_CONVERT_ERROR_APODIZE);
					s.Format(cGetConvertError(C_CONVERT_ERROR_APODIZE), fnamespec);
					Checks.Errors.push_back(s);
					continue;
				}
				if(Checks.Zero != OldSpec.PrjZeroFilling)
				{
					Checks.ErrNums.push_back(C_CONVERT_ERROR_ZEROFILL);
					s.Format(cGetConvertError(C_CONVERT_ERROR_ZEROFILL), fnamespec);
					Checks.Errors.push_back(s);
					continue;
				}
			}

			OldSpec.num = spnum;
			OldSamF.SpecData.push_back(OldSpec);
		}
	}

	if(OldSamF.SpecData.size() == 0)
	{
		Checks.ErrNums.push_back(C_CONVERT_ERROR_NOSPEC);
		s.Format(cGetConvertError(C_CONVERT_ERROR_NOSPEC), samname);
		Checks.Errors.push_back(s);
		return false;
	}

	return true;
}

bool CLoadOldDatas::LoadOldSample(CString fname, SampleOldData &OldSam)
{
	if(!PathFileExists(fname))
		return false;

	CDfxCache dfx(fname);

	CString section = _T("Sample");

	OldSam.Name = dfx.GetProfileString(fname, section, _T("strFileTitle"), _T("NoTitle"));
	OldSam.Iden2 = dfx.GetProfileString(fname, section, _T("strTextID"), _T(""));

	int len = OldSam.Iden2.GetLength();
	if(len > 40)  OldSam.Iden2.Delete(40, len - 40);

	OldSam.NSpectra = dfx.GetProfileInt(fname, section, _T("nSpecNum"), 0);
	if(OldSam.NSpectra > 0)
		OldSam.NSpectra = dfx.GetProfileIntArray(fname, section, _T("SpecArray"), OldSam.NSpectra, OldSam.Spectra);

	return true;
}

bool CLoadOldDatas::LoadOldRef(CString fname, VROD &OldRef)
{
	OldRef.clear();

	if(!PathFileExists(fname))
		return false;

	CDfxCache dfx(fname);

	CString s;
	int ncomp = dfx.GetProfileInt(fname, _T("ComponentList"), _T("ncomp"), 0);
	for(int i=0; i<ncomp; i++)
	{
		RefOldData Ref;
		CString section;
		section.Format(_T("Component_%1d"), i+1);

		Ref.Name = dfx.GetProfileString(fname, section, _T("strName"), _T(""));
		Ref.Value = dfx.GetProfileDouble(fname, section, _T("fConc"), -1.);

		if(find(Checks.ParNames.begin(), Checks.ParNames.end(), Ref.Name) == Checks.ParNames.end())
			continue;

		OldRef.push_back(Ref);
	}

	return true;
}

bool CLoadOldDatas::LoadOldSpectrum(CString fname, SpectrumOldData &OldSpec, bool IsImit)
{
	CString s;

	FILE *f = _tfopen(fname, _T("rt"));
	if(!f)
	{
		Checks.ErrNums.push_back(C_CONVERT_ERROR_NOTOPEN);
		s.Format(cGetConvertError(C_CONVERT_ERROR_NOTOPEN), fname);
		Checks.Errors.push_back(s);
		return false;
	}

	bool isbreak = false;
	int nmust = (IsImit) ? 3 : 9;
	int readDSN = 0, readAPT = 0, readZFL = 0, readFEQ = 0, readFMI = 0, readFMA = 0;
	double readRES = 0., readWVL = 0.;
	bool errspec = false;
	CTime readTIM(0);

	TCHAR str[DFX_MAX_STRLEN];
	while(_fgetts(str, DFX_MAX_STRLEN - 4, f))
	{
		if(!IsImit && !_tcsncmp(str, _T("#DSN"), 4))
		{
			readDSN = _tstoi(str + 4);
			if(readDSN != 0 && Checks.DevNumber != readDSN)
			{
				Checks.ErrNums.push_back(C_CONVERT_ERROR_BADNUM);
				s.Format(cGetConvertError(C_CONVERT_ERROR_BADNUM), fname);
				Checks.Errors.push_back(s);
				isbreak = true;
				break;
			}
			continue;
		}
		if(!_tcsncmp(str, _T("#TIM"), 4))
		{
			TCHAR *tstr = str + 4;
			int yr = _tstoi(_tcstok(tstr, _T(" \t\n\r-:")));
			int mo = _tstoi(_tcstok(0, _T(" \t\n\r-:")));
			int dy = _tstoi(_tcstok(0, _T(" \t\n\r-:")));
			int h = _tstoi(_tcstok(0, _T(" \t\n\r-:")));
			int m = _tstoi(_tcstok(0, _T(" \t\n\r-:")));
			int s = _tstoi(_tcstok(0, _T(" \t\n\r-:")));
			readTIM = CTime(yr, mo, dy, h, m, s);
			nmust--;
			continue;
		}
		if(!_tcsncmp(str, _T("#FMI"), 4))
		{
			readFMI = int(_tstof(str + 4));
			nmust--;
			if(Checks.LowSpec != readFMI)
			{
				Checks.ErrNums.push_back(C_CONVERT_ERROR_LOWLIM);
				s.Format(cGetConvertError(C_CONVERT_ERROR_LOWLIM), fname);
				Checks.Errors.push_back(s);
				isbreak = true;
				break;
			}
			continue;
		}
		if(!_tcsncmp(str, _T("#FMA"), 4))
		{
			readFMA = int(_tstof(str + 4));
			nmust--;
			if(Checks.UpperSpec != readFMA)
			{
				Checks.ErrNums.push_back(C_CONVERT_ERROR_UPPERLIM);
				s.Format(cGetConvertError(C_CONVERT_ERROR_UPPERLIM), fname);
				Checks.Errors.push_back(s);
				isbreak = true;
				break;
			}
			continue;
		}
		if(!IsImit && !_tcsncmp(str, _T("#RES"), 4))
		{
			readRES = _tstof(str + 4);
			nmust--;
			if(fabs(Checks.Resol - readRES) > 1.e-3)
			{
				Checks.ErrNums.push_back(C_CONVERT_ERROR_RESOL);
				s.Format(cGetConvertError(C_CONVERT_ERROR_RESOL), fname);
				Checks.Errors.push_back(s);
				isbreak = true;
				break;
			}
			continue;
		}
		if(!IsImit && !_tcsncmp(str, _T("#WVL"), 4))
		{
			readWVL = _tstof(str + 4);
			nmust--;
/*			if(fabs(Checks.WaveLen - readWVL) > 1.e-9)
			{
				Checks.ErrNums.push_back(C_CONVERT_ERROR_WAVE);
				s.Format(cGetConvertError(C_CONVERT_ERROR_WAVE), fname);
				Checks.Errors.push_back(s);
				isbreak = true;
				break;
			}
*/			continue;
		}
		if(!IsImit && !_tcsncmp(str, _T("#APT"), 4))
		{
			readAPT = _tstoi(str + 4);
			nmust--;
			if(Checks.Apod != readAPT)
			{
				Checks.ErrNums.push_back(C_CONVERT_ERROR_APODIZE);
				s.Format(cGetConvertError(C_CONVERT_ERROR_APODIZE), fname);
				Checks.Errors.push_back(s);
				isbreak = true;
				break;
			}
			continue;
		}
		if(!IsImit && !_tcsncmp(str, _T("#ZFL"), 4))
		{
			readZFL = _tstoi(str + 4);
			nmust--;
			if(Checks.Zero != readZFL - 1)
			{
				Checks.ErrNums.push_back(C_CONVERT_ERROR_ZEROFILL);
				s.Format(cGetConvertError(C_CONVERT_ERROR_ZEROFILL), fname);
				Checks.Errors.push_back(s);
				isbreak = true;
				break;
			}
			continue;
		}
		if(!IsImit && !_tcsncmp(str, _T("#FEQ"), 4))
		{
			readFEQ = _tstoi(str + 4);
			nmust--;
			continue;
		}
		if(!_tcsncmp(str, _T("DATA"), 4))
		{
			double Step, MinFreq, MaxFreq;

			if(IsImit)
			{
				Step = 1.;
				MinFreq = readFMI;
				MaxFreq = readFMA;
			}
			else
			{
				Step = readRES / (16384 * C_LASER_WAVE);
				MinFreq = int(readFMI / Step) * Step;
				if(readFMI - MinFreq > Step / 2.)
					MinFreq += Step;
				MaxFreq = int(readFMA / Step) * Step;
				if(readFMA - MaxFreq > Step / 2.)
					MaxFreq += Step;
			}
			int NAllPoints = int((MaxFreq - MinFreq) / Step + 1.1);

			int ir = 0;
			double old = 0.;
			bool freq_sh = false;
			OldSpec.SpecData.clear();
			while(_fgetts(str, DFX_MAX_STRLEN - 4, f))
			{
				double sp = _tstof(_tcstok(str, _T(" \t\n\r")));

				if(!freq_sh && (ir == 0 && fabs(sp - MinFreq) > 1.0e-6))
				{
					freq_sh = true;
					continue;
				}
				if((ir == 0 && fabs(sp - MinFreq) > 1.0e-6) || (ir > 0 && fabs(sp - old - Step) > 1.e-6))
				{
					Checks.ErrNums.push_back(C_CONVERT_ERROR_SPECPNT);
					s.Format(cGetConvertError(C_CONVERT_ERROR_SPECPNT), fname, sp);
					errspec = true;
					Checks.Errors.push_back(s);
					break;
				}
				old = sp;
				ir++;
				double spv = _tstof(_tcstok(0, _T(" \t\n\r")));
				OldSpec.SpecData.push_back(spv);
				if(ir >= NAllPoints)
					break;
			}
			if(ir < NAllPoints)
			{
				OldSpec.SpecData.clear();
				isbreak = true;
				break;
			}
			nmust--;
			break;
		}
	}
	fclose(f);
	if(isbreak)
		return false;

	if(nmust > 0)
	{
		if(!errspec)
		{
			Checks.ErrNums.push_back(C_CONVERT_ERROR_NOTALLDATA);
			s.Format(cGetConvertError(C_CONVERT_ERROR_NOTALLDATA), fname);
			Checks.Errors.push_back(s);
		}
		return false;
	}

	OldSpec.DateTime = readTIM;
	OldSpec.PrjLowSpec = readFMI;
	OldSpec.PrjUpperSpec = readFMA;
	if(!IsImit)
	{
		OldSpec.DevNumber = readDSN;
		OldSpec.PrjResolution = readRES;
		OldSpec.PrjApodize = readAPT;
		OldSpec.PrjZeroFilling = readZFL - 1;
		OldSpec.PrjXTransform = (readFEQ == 0) ? false : true;
		OldSpec.WaveLength = readWVL;
	}

	return true;
}

bool CLoadOldDatas::LoadOldModel(CString name, CString path, CString PrjName, ModelOldData &OldModel, vector<SampleOldDataFull>* Samples)
{
	CString s;
	CString fname = path + name;
	CString modname = name;
	int len = modname.GetLength();
	modname.Delete(len-4, 4);

	CDfxCache dfx(fname);

	CString section = _T("Calibration");

	OldModel.PrjName = dfx.GetProfileString(fname, section, _T("strApjFile"), _T(""));
	if(PrjName.Compare(OldModel.PrjName))
		return false;

	int IsTrans = dfx.GetProfileInt(fname, section, _T("nTransFlag"), 0);
	if(IsTrans != 0)
	{
		Checks.ErrNums.push_back(C_CONVERT_ERROR_MODTRANS);
		s.Format(cGetConvertError(C_CONVERT_ERROR_MODTRANS), modname);
		Checks.Errors.push_back(s);
		return false;
	}

	OldModel.AnType = dfx.GetProfileInt(fname, section, _T("nAnlType"), 0);
	if(OldModel.AnType != 1)
	{
		Checks.ErrNums.push_back(C_CONVERT_ERROR_MODNOTQNT);
		s.Format(cGetConvertError(C_CONVERT_ERROR_MODNOTQNT), modname);
		Checks.Errors.push_back(s);
		return false;
	}

	OldModel.ModelName = dfx.GetProfileString(fname, section, _T("strTitle"), _T(""));
	OldModel.SpecType = dfx.GetProfileInt(fname, section, _T("nAbsmode"), 0);
	OldModel.ModelType = dfx.GetProfileInt(fname, section, _T("nModel"), 0);
	OldModel.LowSpecRange = dfx.GetProfileInt(fname, section, _T("nFminUser"), 0);
	OldModel.UpperSpecRange = dfx.GetProfileInt(fname, section, _T("nFmaxUser"), 0);
	OldModel.IsAvr = dfx.GetProfileInt(fname, section, _T("nAvrMode"), 0);
	OldModel.Preproc = dfx.GetProfileString(fname, section, _T("strPrepSeq"), _T(""));
	OldModel.NComp = dfx.GetProfileInt(fname, section, _T("nPCRcomps"), 0);
	OldModel.MaxMah = dfx.GetProfileInt(fname, section, _T("fQltDmax"), 0);

	OldModel.nParams = dfx.GetProfileInt(fname, _T("ComponentList"), _T("ncomp"), 0);
	if(OldModel.nParams <= 0)
	{
		Checks.ErrNums.push_back(C_CONVERT_ERROR_MODNOPARAMS);
		s.Format(cGetConvertError(C_CONVERT_ERROR_MODNOPARAMS), modname);
		Checks.Errors.push_back(s);
		return false;
	}
	for(int i=0; i<OldModel.nParams; i++)
	{
		ModelParOldData MPar;
		section.Format(_T("Component_%1d"), i+1);

		MPar.MPName = dfx.GetProfileString(fname, section, _T("strName"), _T(""));
		MPar.MPSKO = dfx.GetProfileDouble(fname, section, _T("fStdDevMax"), 3.);
		if(MPar.MPSKO < 0.)  MPar.MPSKO = 3.;
		MPar.MinRange = dfx.GetProfileDouble(fname, section, _T("fRangeMin"), 0.);
		MPar.MaxRange = dfx.GetProfileDouble(fname, section, _T("fRangeMax"), 0.);
		MPar.Corr = dfx.GetProfileDouble(fname, section, _T("fCorr"), 0.);

		if(find(Checks.ParNames.begin(), Checks.ParNames.end(), MPar.MPName) == Checks.ParNames.end())
		{
			Checks.ErrNums.push_back(C_CONVERT_ERROR_PARAM);
			s.Format(cGetConvertError(C_CONVERT_ERROR_PARAM), MPar.MPName, fname);
			Checks.Errors.push_back(s);
			return false;
		}

		OldModel.Params.push_back(MPar);
	}

	OldModel.nCalib = dfx.GetProfileInt(fname, _T("SampArray"), _T("nSamp"), 0);
	if(OldModel.nCalib <= 0)
	{
		Checks.ErrNums.push_back(C_CONVERT_ERROR_MODNOCALIB);
		s.Format(cGetConvertError(C_CONVERT_ERROR_MODNOCALIB), modname);
		Checks.Errors.push_back(s);
		return false;
	}
	for(int i=0; i<OldModel.nCalib; i++)
	{
		ModelSamOldData MSam;
		section.Format(_T("SampArray_%1d"), i+1);

		MSam.Name = dfx.GetProfileString(fname, section, _T("strFileTitle"), _T(""));
		MSam.nSpec = dfx.GetProfileInt(fname, section, _T("nSpecNum"), 0);
		if(MSam.nSpec > 0)
			MSam.nSpec = dfx.GetProfileIntArray(fname, section, _T("SpecArray"), MSam.nSpec, MSam.SpecNums);

		vector<SampleOldDataFull>::iterator itS;
		for(itS=Samples->begin(); itS!=Samples->end(); ++itS)
			if(!itS->OldSam.Name.Compare(MSam.Name))
				break;

		if(itS == Samples->end())
		{
			Checks.ErrNums.push_back(C_CONVERT_ERROR_MODSAMPLE);
			s.Format(cGetConvertError(C_CONVERT_ERROR_MODSAMPLE), MSam.Name, fname);
			Checks.Errors.push_back(s);
			return false;
		}

		ItMPOD itP;
		for(itP=OldModel.Params.begin(); itP!=OldModel.Params.end(); ++itP)
		{
			ItROD itR;
			for(itR=itS->RefData.begin(); itR!=itS->RefData.end(); ++itR)
				if(!itR->Name.Compare(itP->MPName))
					break;

			if(itR == itS->RefData.end() || itR->Value < 0.)
			{
				Checks.ErrNums.push_back(C_CONVERT_ERROR_MODREF);
				s.Format(cGetConvertError(C_CONVERT_ERROR_MODREF), MSam.Name, fname, itR->Name);
				Checks.Errors.push_back(s);
				return false;
			}
		}

		OldModel.Calib.push_back(MSam);
	}

	OldModel.nValid = dfx.GetProfileInt(fname, _T("SampArrayExt"), _T("nSamp"), 0);
	for(int i=0; i<OldModel.nValid; i++)
	{
		ModelSamOldData MSam;
		section.Format(_T("SampArrayExt_%1d"), i+1);

		MSam.Name = dfx.GetProfileString(fname, section, _T("strFileTitle"), _T(""));
		MSam.nSpec = dfx.GetProfileInt(fname, section, _T("nSpecNum"), 0);
		if(MSam.nSpec > 0)
			MSam.nSpec = dfx.GetProfileIntArray(fname, section, _T("SpecArray"), MSam.nSpec, MSam.SpecNums);

		vector<SampleOldDataFull>::iterator itS;
		for(itS=Samples->begin(); itS!=Samples->end(); ++itS)
			if(!itS->OldSam.Name.Compare(MSam.Name))
				break;

		if(itS == Samples->end())
		{
			Checks.ErrNums.push_back(C_CONVERT_ERROR_MODSAMPLE);
			s.Format(cGetConvertError(C_CONVERT_ERROR_MODSAMPLE), MSam.Name, fname);
			Checks.Errors.push_back(s);
			return false;
		}

		ItMPOD itP;
		for(itP=OldModel.Params.begin(); itP!=OldModel.Params.end(); ++itP)
		{
			ItROD itR;
			for(itR=itS->RefData.begin(); itR!=itS->RefData.end(); ++itR)
				if(!itR->Name.Compare(itP->MPName))
					break;

			if(itR == itS->RefData.end() || itR->Value < 0.)
			{
				Checks.ErrNums.push_back(C_CONVERT_ERROR_MODREF);
				s.Format(cGetConvertError(C_CONVERT_ERROR_MODREF), MSam.Name, fname, itR->Name);
				Checks.Errors.push_back(s);
				return false;
			}
		}

		OldModel.Valid.push_back(MSam);
	}

	return true;
}

bool CLoadOldDatas::LoadOldMethod(CString name, CString path, CString PrjName, MethodOldData &OldMethod)
{
	CString s;
	CString fname = path + name;
	CString mtdname = name;
	int len = mtdname.GetLength();
	mtdname.Delete(len-4, 4);

	CDfxCache dfx(fname);

	int i;
	CString section = _T("Method");

	OldMethod.PrjName = dfx.GetProfileString(fname, section, _T("strApjFile"), _T(""));
	if(PrjName.Compare(OldMethod.PrjName))
		return false;

	OldMethod.AnType = dfx.GetProfileInt(fname, section, _T("nAnlType"), 0);
	if(OldMethod.AnType != 1)
	{
		Checks.ErrNums.push_back(C_CONVERT_ERROR_MTDNOTQNT);
		s.Format(cGetConvertError(C_CONVERT_ERROR_MTDNOTQNT), mtdname);
		Checks.Errors.push_back(s);
		return false;
	}

	OldMethod.MethodName = dfx.GetProfileString(fname, section, _T("strTitle"), _T(""));
	OldMethod.IndMois = dfx.GetProfileInt(fname, section, _T("nHumidIdx"), -1);

	OldMethod.nModels = dfx.GetProfileInt(fname, section, _T("nNumClbs"), 0);
	for(i=0; i<OldMethod.nModels; i++)
	{
		CString field;
		field.Format(_T("Calib%1d"), i+1);

		CString modfile = dfx.GetProfileString(fname, section, field, _T(""));

		if(find(Checks.ModFiles.begin(), Checks.ModFiles.end(), modfile) == Checks.ModFiles.end())
		{
			Checks.ErrNums.push_back(C_CONVERT_ERROR_MTDNOTOPEN);
			s.Format(cGetConvertError(C_CONVERT_ERROR_MTDNOTOPEN), modfile, mtdname);
			Checks.Errors.push_back(s);
			return false;
		}

		OldMethod.Models.push_back(modfile);
	}

	OldMethod.nRules = dfx.GetProfileInt(fname, _T("ComponentList"), _T("ncomp"), 0);
	if(OldMethod.nRules <= 0)
	{
		Checks.ErrNums.push_back(C_CONVERT_ERROR_MTDNOPARAMS);
		s.Format(cGetConvertError(C_CONVERT_ERROR_MTDNOPARAMS), mtdname);
		Checks.Errors.push_back(s);
		return false;
	}
	for(i=0; i<OldMethod.nRules; i++)
	{
		RuleOldData Rule;
		section.Format(_T("Component_%1d"), i+1);

		Rule.ParamName = dfx.GetProfileString(fname, section, _T("strName"), _T(""));
		Rule.DefModel = dfx.GetProfileString(fname, section, _T("strDefClb"), _T(""));
		Rule.StdMois = dfx.GetProfileDouble(fname, section, _T("fHumidStd"), 0.);

		if(find(Checks.ParNames.begin(), Checks.ParNames.end(), Rule.ParamName) == Checks.ParNames.end())
		{
			Checks.ErrNums.push_back(C_CONVERT_ERROR_PARAM);
			s.Format(cGetConvertError(C_CONVERT_ERROR_PARAM), Rule.ParamName, fname);
			Checks.Errors.push_back(s);
			return false;
		}

		OldMethod.Rules.push_back(Rule);
	}

	section = _T("SpecConf");

	OldMethod.MinTemp = dfx.GetProfileInt(fname, section, _T("nparTminSample"), -5);
	OldMethod.MaxTemp = dfx.GetProfileInt(fname, section, _T("nparTmaxSample"), 40);
	OldMethod.DiffTemp = dfx.GetProfileInt(fname, section, _T("nparTdiff"), 45);

	return true;
}
