#include "stdafx.h"

#include "ReadApjxWaitDlg.h"
#include "BranchInfo.h"

IMPLEMENT_DYNAMIC(CReadApjxWaitDlg, CWaitingDlg)

CReadApjxWaitDlg::CReadApjxWaitDlg(CDeviceData* dev, apjdb::PRApjProject* apjx, CString fname, CWnd* pParent)
	: CWaitingDlg(pParent)
{
	Apjx = apjx;
	pDevice = dev;
	FileName = fname;

	Comment = ParcelLoadString(STRID_READ_APJX_WAIT_COMMENT);
}

CReadApjxWaitDlg::~CReadApjxWaitDlg()
{
}

void CReadApjxWaitDlg::DoDataExchange(CDataExchange* pDX)
{
	CWaitingDlg::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CReadApjxWaitDlg, CWaitingDlg)
END_MESSAGE_MAP()


BOOL CReadApjxWaitDlg::OnInitDialog()
{
	CWaitingDlg::OnInitDialog();

	return true;
}

void CReadApjxWaitDlg::OK()
{
	CWaitingDlg::OK();
}

void CReadApjxWaitDlg::Stop()
{
	CWaitingDlg::Stop();
}

void CReadApjxWaitDlg::Work()
{
	ParcelWait(true);
	
	bool res = pDevice->ReadApjx(FileName, (*Apjx));

	ParcelWait(false);

	if(res)
		OK();
	else
		Stop();
}
