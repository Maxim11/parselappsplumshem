#include "stdafx.h"

#include "BranchInfo.h"
#include "ImportSpectraDlg.h"
#include "ImportSpectraWaitDlg.h"
#include "MainFrm.h"

#include <math.h>

static CImportSpectraDlg* pSpec = NULL;

static int GetTextColorSam(int item, int /*subitem*/, void* /*pData*/)
{
	if(pSpec == NULL)
		return 0;

	COLORREF color = CLR_DEFAULT;
	if(pSpec->IsSampleDiff(item))
		color = cColorErr;
	if(!pSpec->IsSampleAval(item))
		color = cColorPass;

	pSpec->m_listSam.CurTxtColor = color;

	return 1;
}

static int GetTextColorRef(int item, int /*subitem*/, void* /*pData*/)
{
	if(pSpec == NULL)
		return 0;

	COLORREF color = CLR_DEFAULT;
	if(pSpec->IsCurSampleRefDiff(item))
		color = cColorErr;

	pSpec->m_listRef.CurTxtColor = color;

	return 1;
}

int ImportSample::GetNSpectra()
{
	return int(Spectra.size());
}

int ImportSample::GetNNewSpectra()
{
	int N = 0;

	ItIntBool it;
	for(it=Spectra.begin(); it!=Spectra.end(); ++it)
		if(it->second)
			N++;

	return N;
}

IMPLEMENT_DYNAMIC(CImportSpectraDlg, CDialog)

CImportSpectraDlg::CImportSpectraDlg(CDeviceData* dev, apjdb::PRApjProject* apjx, CProjectData* pprj, CWnd* pParent)
	: CDialog(CImportSpectraDlg::IDD, pParent)
{
	pSpec = this;

	Device = dev;
	pPrj = pprj;
	Apjx = apjx;

	IsSST = apjx->IsSST;

	NSamples = int(Apjx->Samples.size());

	m_vName = (IsSST) ? Device->GetSSTName() : cEmpty;
	m_vDiff = false;
	m_vPrj = 0;
}

CImportSpectraDlg::~CImportSpectraDlg()
{
}

void CImportSpectraDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_SAMPLES_LIST, m_listSam);
	DDX_Control(pDX, IDC_REFDATA_LIST, m_listRef);
	DDX_Control(pDX, IDC_PROJECT_LIST, m_Prj);
	DDX_Control(pDX, IDC_NEW_PROJECT, m_NewPrj);
	DDX_Control(pDX, IDC_MAKE_DIFF, m_Diff);

	DDX_CBIndex(pDX, IDC_PROJECT_LIST, m_vPrj);

	cParcelDDX(pDX, IDC_INPUT_NAME, m_vName);
	DDV_MaxChars(pDX, m_vName, 50);

	DDX_Check(pDX, IDC_NEW_PROJECT, m_vNewPrj);
	DDX_Check(pDX, IDC_MAKE_DIFF, m_vDiff);
}

BEGIN_MESSAGE_MAP(CImportSpectraDlg, CDialog)

	ON_CBN_SELCHANGE(IDC_PROJECT_LIST, OnProjectListChange)
	ON_CBN_SELCHANGE(IDC_MAKE_DIFF, OnMakeDiffChange)

	ON_BN_CLICKED(IDC_ACCEPT, OnAccept)
	ON_BN_CLICKED(IDHELP, OnHelp)
	ON_BN_CLICKED(IDC_ADD, OnAdd)
	ON_BN_CLICKED(IDC_ADD_ALL, OnAddAll)
	ON_BN_CLICKED(IDC_REMOVE, OnRemove)
	ON_BN_CLICKED(IDC_REMOVE_ALL, OnRemoveAll)
	ON_BN_CLICKED(IDC_NEW_PROJECT, OnSetNewPrj)

	ON_NOTIFY(LVN_ITEMCHANGED, IDC_SAMPLES_LIST, OnSamplesListSelChange)

END_MESSAGE_MAP()

BOOL CImportSpectraDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	int i;
	CString s;

	s = (IsSST) ? ParcelLoadString(STRID_IMPORT_SPECTRA_SST_HDR) : ParcelLoadString(STRID_IMPORT_SPECTRA_HDR);
	SetWindowText(ParcelLoadString(STRID_IMPORT_SPECTRA_HDR));

	CreateListPrj();

	if(IsSST)
	{
		GetDlgItem(IDC_NEW_PROJECT)->EnableWindow(false);
		GetDlgItem(IDC_INPUT_NAME)->EnableWindow(false);
	}

	m_Prj.ResetContent();
	ItStr it;
	for(it=PrjNames.begin(); it!=PrjNames.end(); ++it)
		m_Prj.AddString(*it);
	if(int(PrjNames.size()) > 0)
		m_Prj.SetCurSel(m_vPrj);
	else
	{
		m_vNewPrj = true;
		m_NewPrj.SetCheck(m_vNewPrj);
		GetDlgItem(IDC_NEW_PROJECT)->EnableWindow(false);
	}

	s = ParcelLoadString(STRID_IMPORT_SPECTRA_COLSAM);
	m_listSam.InsertColumn(0, s, LVCFMT_LEFT, 0, 0);
	s = ParcelLoadString(STRID_IMPORT_SPECTRA_COLNSPEC);
	m_listSam.InsertColumn(1, s, LVCFMT_RIGHT, 0, 1);
	s = ParcelLoadString(STRID_IMPORT_SPECTRA_COLINCLUDE);
	m_listSam.InsertColumn(2, s, LVCFMT_RIGHT, 0, 2);
	CRect Rect;
	m_listSam.GetWindowRect(Rect);
	int width = int((Rect.Width() - 20) / 3);
	for(i=0; i<3; i++)
		m_listSam.SetColumnWidth(i, width);
	m_listSam.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

	s = ParcelLoadString(STRID_IMPORT_SPECTRA_COLPAR);
	m_listRef.InsertColumn(0, s, LVCFMT_LEFT, 0, 0);
	s = ParcelLoadString(STRID_IMPORT_SPECTRA_COLFILE);
	m_listRef.InsertColumn(1, s, LVCFMT_RIGHT, 0, 1);
	s = ParcelLoadString(STRID_IMPORT_SPECTRA_COLPRJ);
	m_listRef.InsertColumn(2, s, LVCFMT_RIGHT, 0, 2);
	for(i=0; i<3; i++)
		m_listRef.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	m_listRef.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

	m_listSam.SetUserCallback(LCF_CB_SI_TXTCOLOR, GetTextColorSam);
	m_listSam.EnableUserCallback(LCF_CB_SI_TXTCOLOR, true);
	m_listRef.SetUserCallback(LCF_CB_SI_TXTCOLOR, GetTextColorRef);
	m_listRef.EnableUserCallback(LCF_CB_SI_TXTCOLOR, true);

	CreateSamples();

	FillListSam(0);

	GetDlgItem(IDC_ACCEPT)->SetWindowText(ParcelLoadString(STRID_IMPORT_SPECTRA_ACCEPT));
	GetDlgItem(IDCANCEL)->SetWindowText(ParcelLoadString(STRID_IMPORT_SPECTRA_DECLINE));
	GetDlgItem(IDHELP)->SetWindowText(ParcelLoadString(STRID_IMPORT_SPECTRA_HELP));
	GetDlgItem(IDC_ADD)->SetWindowText(ParcelLoadString(STRID_IMPORT_SPECTRA_ADD));
	GetDlgItem(IDC_ADD_ALL)->SetWindowText(ParcelLoadString(STRID_IMPORT_SPECTRA_ADD_ALL));
	GetDlgItem(IDC_REMOVE)->SetWindowText(ParcelLoadString(STRID_IMPORT_SPECTRA_REMOVE));
	GetDlgItem(IDC_REMOVE_ALL)->SetWindowText(ParcelLoadString(STRID_IMPORT_SPECTRA_REMOVE_ALL));
	GetDlgItem(IDC_NEW_PROJECT)->SetWindowText(ParcelLoadString(STRID_IMPORT_SPECTRA_NEWPRJ));
	GetDlgItem(IDC_MAKE_DIFF)->SetWindowText(ParcelLoadString(STRID_IMPORT_SPECTRA_MAKEDIFF));
	GetDlgItem(IDC_STATIC_5)->SetWindowText(ParcelLoadString(STRID_IMPORT_SPECTRA_ISAM));
	GetDlgItem(IDC_STATIC_6)->SetWindowText(ParcelLoadString(STRID_IMPORT_SPECTRA_REFDATAS));

	GetDlgItem(IDHELP)->EnableWindow(pFrame->IsExistHelp());

	return true;
}

void CImportSpectraDlg::OnAccept()
{
	if(!UpdateData())
		return;

	CString s;

	if(m_vNewPrj)
	{
		m_vName.Trim();
		if(m_vName.IsEmpty())
		{
			ParcelError(ParcelLoadString(STRID_IMPORT_SPECTRA_ERROR_EMPTYNAME));
			return;
		}
		if(!IsSST && !m_vName.Compare(Device->GetSSTName()))
		{
			s.Format(ParcelLoadString(STRID_IMPORT_SPECTRA_ERROR_SSTNAME), m_vName);
			ParcelError(s);
			return;
		}
		if(m_vNewPrj && Device->GetProject(m_vName) != NULL)
		{
			if(Device->GetProject(m_vName) != NULL)
			{
				s.Format(ParcelLoadString(STRID_IMPORT_SPECTRA_ERROR_NAME), m_vName);
				ParcelError(s);
				return;
			}
		}
	}

	if(GetNImporting() <= 0)
	{
		ParcelError(ParcelLoadString(STRID_IMPORT_SPECTRA_ERROR_NOIMPORT));
		return;
	}

	CString PrjName = (m_vNewPrj) ? m_vName : PrjNames[m_vPrj];

	bool replace = (m_vDiff) ? true : false;
	CImportSpectraWaitDlg dlg2(Device, PrjName, Apjx, &Samples, replace, this);
	if(dlg2.DoModal() != IDOK)
		return;

	CDialog::OnOK();
}

void CImportSpectraDlg::OnCancel()
{
	CDialog::OnCancel();
}

void CImportSpectraDlg::OnAdd()
{
	CString s = ParcelLoadString(STRID_IMPORT_SPECTRA_YES);
	int Ind = -1, nsel = m_listSam.GetSelectedCount();
	if(nsel <= 0)
		return;

	for(int i=0; i<nsel; i++)
	{
		Ind = m_listSam.GetNextItem(Ind, LVNI_SELECTED);
		if(Ind < 0 || Ind >= NSamples)
			return;

		if(IsSampleAval(Ind))
		{
			Samples[Ind].Include = true;
			m_listSam.SetItemText(Ind, 2, s);
		}
	}

	EnableFields();
}

void CImportSpectraDlg::OnAddAll()
{
	CString s = ParcelLoadString(STRID_IMPORT_SPECTRA_YES);
	int i;
	for(i=0; i<NSamples; i++)
		if(IsSampleAval(i))
		{
			Samples[i].Include = true;
			m_listSam.SetItemText(i, 2, s);
		}

	EnableFields();
}

void CImportSpectraDlg::OnRemove()
{
	CString s = ParcelLoadString(STRID_IMPORT_SPECTRA_NO);
	int Ind = -1, nsel = m_listSam.GetSelectedCount();
	if(nsel <= 0)
		return;

	for(int i=0; i<nsel; i++)
	{
		Ind = m_listSam.GetNextItem(Ind, LVNI_SELECTED);
		if(Ind < 0 || Ind >= NSamples)
			return;

		Samples[Ind].Include = false;
		m_listSam.SetItemText(Ind, 2, s);
	}

	EnableFields();
}

void CImportSpectraDlg::OnRemoveAll()
{
	CString s = ParcelLoadString(STRID_IMPORT_SPECTRA_NO);
	int i;
	for(i=0; i<NSamples; i++)
	{
		Samples[i].Include = false;
		m_listSam.SetItemText(i, 2, s);
	}

	EnableFields();
}

void CImportSpectraDlg::OnSetNewPrj()
{
	if(!UpdateData())
		return;

	CreateSamples();
	FillListSam(0);
	EnableFields();
}

void CImportSpectraDlg::OnProjectListChange()
{
	if(!UpdateData())
		return;

	CreateSamples();
	FillListSam(0);
	EnableFields();
}

void CImportSpectraDlg::OnMakeDiffChange()
{
	if(!UpdateData())
		return;

	EnableFields();
}

void CImportSpectraDlg::OnSamplesListSelChange(NMHDR* /*pNMHDR*/, LRESULT *pResult)
{
	FillListRef();
	EnableFields();

	*pResult = 0;
}

void CImportSpectraDlg::FillListSam(int sel)
{
	m_listSam.DeleteAllItems();

	CString s;
	int i;

	ItImpSam it;
	for(i=0, it=Samples.begin(); it!=Samples.end(); ++it, i++)
	{
		m_listSam.InsertItem(i, it->SamName);
		s.Format(cFmt1_1, it->GetNSpectra(), it->GetNNewSpectra());
		m_listSam.SetItemText(i, 1, s);
		s = (it->Include) ? ParcelLoadString(STRID_IMPORT_SPECTRA_YES) : ParcelLoadString(STRID_IMPORT_SPECTRA_NO);
		m_listSam.SetItemText(i, 2, s);
	}

	if(sel < 0 || sel >= NSamples)
		sel = 0;

	m_listSam.SetItemState(sel, LVIS_SELECTED, LVIS_SELECTED);

	s.Format(ParcelLoadString(STRID_IMPORT_SPECTRA_SAMPLES), NSamples, GetNNewSpectra());
	GetDlgItem(IDC_STATIC_4)->SetWindowText(s);

	FillListRef();
	EnableFields();
}

void CImportSpectraDlg::FillListRef()
{
	m_listRef.DeleteAllItems();

	int Ind = m_listSam.GetNextItem(-1, LVNI_SELECTED);
	if(Ind < 0 || Ind >= NSamples)
		return;

	CString SamName = Samples[Ind].SamName;

	vector<PRApjSample>::iterator itAS;
	for(itAS=Apjx->Samples.begin(); itAS!=Apjx->Samples.end(); ++itAS)
		if(!itAS->SampleName.Compare(SamName))
			break;

	if(itAS == Apjx->Samples.end())
		return;

	CProjectData* CurPrj = (m_vNewPrj) ? NULL : Device->GetProject(PrjNames[m_vPrj]);
	CSampleData* Sam = (CurPrj != NULL) ? CurPrj->GetSample(SamName) : NULL;

	CString s;
	int j;
	ItStr itN;
	for(j=0, itN=ParNames.begin(); itN!=ParNames.end(); ++itN, j++)
	{
		CString ParName = (*itN), ParamUnit, ParamFormat;

		PRApjParam* pParam = GetFileParam(ParName);
		CParamData* Param = (CurPrj == NULL) ? NULL : CurPrj->GetParam(ParName);
		if(Param != NULL)
		{
			ParamUnit = Param->GetUnit();
			ParamFormat = Param->GetFormat();
		}
		else
		{
			ParamUnit = pParam->Unit;
			ParamFormat = pParam->Format;
		}

		vector<PRApjRefData>::iterator itR;
		for(itR=itAS->RefData.begin(); itR!=itAS->RefData.end(); ++itR)
			if(!itR->ParamName.Compare(ParName))
				break;

		bool isRefFile = (itR != itAS->RefData.end() && itR->IsDataExists);
		bool isRefPrj = (Sam != NULL && Param != NULL && !Sam->GetReferenceData(ParName)->bNoValue);

		s.Format(cFmtS_S, ParName, ParamUnit);
		m_listRef.InsertItem(j, s);
		if(!isRefFile)
			s = ParcelLoadString(STRID_IMPORT_SPECTRA_NODATA);
		else
			s.Format(ParamFormat, itR->RefData);
		m_listRef.SetItemText(j, 1, s);

		if(!isRefPrj)
			s = ParcelLoadString(STRID_IMPORT_SPECTRA_NODATA);
		else
			s.Format(ParamFormat, Sam->GetReferenceData(ParName)->Value);

		m_listRef.SetItemText(j, 2, s);
	}

	for(j=0; j<3; j++)
		m_listRef.SetColumnWidth(j, LVSCW_AUTOSIZE_USEHEADER);

	s.Format(ParcelLoadString(STRID_IMPORT_SPECTRA_REFDATA), SamName);
	GetDlgItem(IDC_STATIC_3)->SetWindowText(s);
}

void CImportSpectraDlg::EnableFields()
{
	int i, Ind = -1, nsel = m_listSam.GetSelectedCount();
	bool chk1 = (nsel > 0);
	bool chk2 = IsDiffExist();

	bool yes = false, not = false;
	for(i=0; i<nsel; i++)
	{
		Ind = m_listSam.GetNextItem(Ind, LVNI_SELECTED);
		if(Ind < 0 || Ind >= NSamples)
			return;

		if(Samples[Ind].Include)
			not = true;
		else if(IsSampleAval(Ind))
			yes = true;

		if(yes && not)
			break;
	}

	GetDlgItem(IDC_ADD)->EnableWindow(chk1 && yes);
	GetDlgItem(IDC_REMOVE)->EnableWindow(chk1 && not);
	GetDlgItem(IDC_STATIC_6)->EnableWindow(chk2);
	GetDlgItem(IDC_MAKE_DIFF)->EnableWindow(chk2);

	EnableButtonsAll();

	GetDlgItem(IDC_INPUT_NAME)->ShowWindow(m_vNewPrj);
	GetDlgItem(IDC_PROJECT_LIST)->ShowWindow(!m_vNewPrj);

	CString s = (m_vNewPrj) ? ParcelLoadString(STRID_IMPORT_SPECTRA_PRJNAME)
		: ParcelLoadString(STRID_IMPORT_SPECTRA_SELPRJ);
	GetDlgItem(IDC_STATIC_1)->SetWindowText(s);

	RedrawWindow();
}

void CImportSpectraDlg::EnableButtonsAll()
{
	int i;
	bool yes = false, not = false;
	for(i=0; i<NSamples; i++)
	{
		if(Samples[i].Include)
			not = true;
		else if(IsSampleAval(i))
			yes = true;

		if(yes && not)
			break;
	}

	GetDlgItem(IDC_ADD_ALL)->EnableWindow(yes);
	GetDlgItem(IDC_REMOVE_ALL)->EnableWindow(not);
}

void CImportSpectraDlg::CreateListPrj()
{
	PrjNames.clear();

	ItPrj it;
	for(it=Device->Projects.begin(); it!=Device->Projects.end(); ++it)
	{
		if(it->GetLowLimSpecRange() == Apjx->SpecRangeLow && it->GetUpperLimSpecRange() == Apjx->SpecRangeUpper &&
		   it->GetSubCanalResolution() == Apjx->ResSub && it->GetSubCanalApodization() == Apjx->ApodSub && 
		   it->GetZeroFillingInd() == Apjx->ZeroFill && (IsSST && it->IsSST() || !IsSST && !it->IsSST()))
		    PrjNames.push_back(it->GetName());
	}
}

void CImportSpectraDlg::CreateSamples()
{
	Samples.clear();

	CProjectData* CurPrj = (m_vNewPrj) ? NULL : Device->GetProject(PrjNames[m_vPrj]);

	vector<PRApjSample>::iterator it;
	for(it=Apjx->Samples.begin(); it!=Apjx->Samples.end(); ++it)
	{
		CSampleData* Sam = (CurPrj != NULL) ? CurPrj->GetSample(it->SampleName) : NULL;

		ImportSample ISam;
		ISam.SamName = it->SampleName;

		vector<PRApjSpectrum>::iterator it2;
		for(it2=it->Spectra.begin(); it2!=it->Spectra.end(); ++it2)
		{
			bool isnew = (Sam != NULL) ? !Sam->IsSpecDateExist(it2->DateCreated) : true;
			PIntBool Spec(it2->SpecNum, isnew);
			ISam.Spectra.insert(Spec);
		}

		ISam.Diff = false;
		if(Sam != NULL)
		{
			ItRef it3;
			for(it3=Sam->RefData.begin(); it3!=Sam->RefData.end(); ++it3)
			{
				vector<PRApjRefData>::iterator it4;
				for(it4=it->RefData.begin(); it4!=it->RefData.end(); ++it4)
				{
					if(!it3->Name.Compare(it4->ParamName))
					{
						if(it3->bNoValue && it4->IsDataExists) 
						{
							ISam.Diff = true;
							break;
						}

						if(!it3->bNoValue && it4->IsDataExists) 
						{
							if( fabs(it3->Value - it4->RefData) > 1.e-9)
							{
								ISam.Diff = true;
								break;
							}
						}
					}
				}

				if(it4 != it->RefData.end())
					break;
			}
		}
		ISam.Include = (!ISam.Diff && ISam.GetNNewSpectra() > 0);

		Samples.push_back(ISam);
	}

	ParNames.clear();
	vector<PRApjParam>::iterator itP;
	for(itP=Apjx->Params.begin(); itP!=Apjx->Params.end(); ++itP)
		ParNames.push_back(itP->ParamName);
	if(CurPrj != NULL)
	{
		int j;
		for(j=0; j<CurPrj->GetNParams(); j++)
		{
			CString CurParName = CurPrj->GetParam(j)->GetName();
			if(find(ParNames.begin(), ParNames.end(), CurParName) == ParNames.end())
				ParNames.push_back(CurParName);
		}
	}

}

PRApjParam* CImportSpectraDlg::GetFileParam(CString name)
{
	vector<PRApjParam>::iterator itP;
	for(itP=Apjx->Params.begin(); itP!=Apjx->Params.end(); ++itP)
		if(!itP->ParamName.Compare(name))
			return &(*itP);

	return NULL;
}

int CImportSpectraDlg::GetNImporting()
{
	int N = 0;
	ItImpSam it;
	for(it=Samples.begin(); it!=Samples.end(); ++it)
	{
		if(it->Include)
		{
			N += it->GetNNewSpectra();
			N += it->Diff;
		}
	}

	return N;
}

int CImportSpectraDlg::GetNNewSpectra()
{
	int N = 0;
	ItImpSam it;
	for(it=Samples.begin(); it!=Samples.end(); ++it)
		N += it->GetNNewSpectra();

	return N;
}

bool CImportSpectraDlg::IsDiffExist()
{
	ItImpSam it;
	for(it=Samples.begin(); it!=Samples.end(); ++it)
	{
		if(it->Diff && it->Include)
//		if(it->Diff)
			return true;
	}

	return false;
}

bool CImportSpectraDlg::IsSampleAval(int ind)
{
	if(ind < 0 || ind >= NSamples)
		return false;

//	if(Samples[ind].GetNNewSpectra() <= 0)
//		return false;

	return true;
}

bool CImportSpectraDlg::IsSampleDiff(int ind)
{
	if(ind < 0 || ind >= NSamples)
		return false;

	return Samples[ind].Diff;
}

bool CImportSpectraDlg::IsCurSampleRefDiff(int iref)
{
	int Ind = m_listSam.GetNextItem(-1, LVNI_SELECTED);
	if(Ind < 0 || Ind >= NSamples)
		return false;

	if(iref < 0 || iref >= int(ParNames.size()))
		return false;

	CString SamName = Samples[Ind].SamName;
	CString ParName = ParNames[iref];

	vector<PRApjSample>::iterator itAS;
	for(itAS=Apjx->Samples.begin(); itAS!=Apjx->Samples.end(); ++itAS)
	{
		if(!itAS->SampleName.Compare(SamName))
			break;
	}

	if(itAS == Apjx->Samples.end())
		return false;

	vector<PRApjRefData>::iterator itAR;
	for(itAR=itAS->RefData.begin(); itAR!=itAS->RefData.end(); ++itAR)
	{
		if(!itAR->ParamName.Compare(ParName))
			break;
	}

	if(itAR == itAS->RefData.end())
		return false;

	CProjectData* CurPrj = (m_vNewPrj) ? NULL : Device->GetProject(PrjNames[m_vPrj]);
	CSampleData* Sam = (CurPrj != NULL) ? CurPrj->GetSample(SamName) : NULL;
	ReferenceData* Ref = (Sam != NULL) ? Sam->GetReferenceData(ParName) : NULL;

	if(Ref == NULL)
		return false;

	if(Ref->bNoValue || itAR->IsDataExists)
		return true;

	if(Ref->bNoValue || !itAR->IsDataExists)
		return false;

	return (fabs(itAR->RefData - Ref->Value) > 1.e-9);
}

void CImportSpectraDlg::OnHelp()
{
	int IdHelp = DLGID_PROJECT_IMPORT_SPECTRA;

	pFrame->ShowHelp(IdHelp);
}
