#include "stdafx.h"

#include "PlotSettingsDlg.h"
#include "MainFrm.h"


static CPlotSettingsDlg* pPlotSet = NULL;

static int GetSpectrumColor(int item, int /*subitem*/, void* /*pData*/)
{
	if(pPlotSet == NULL)
		return 0;

	COLORREF color = (pPlotSet->IsSpectrumSel(item)) ? cColorPass : CLR_DEFAULT;

	pPlotSet->m_listAll.CurTxtColor = color;

	return 1;
}


IMPLEMENT_DYNAMIC(CPlotSettingsDlg, CDialog)

CPlotSettingsDlg::CPlotSettingsDlg(CShowSpectra* data, int mode, CWnd* pParent)
	: CDialog(CPlotSettingsDlg::IDD, pParent)
{
	pPlotSet = this;

	OldData = data;
	Mode = mode;

	Data = new CShowSpectra(OldData->GetPoints());
	Data->Copy(*OldData);

	m_vType = Retype(Data->GetType(), true);

	IsBtnClk = false;
}

CPlotSettingsDlg::~CPlotSettingsDlg()
{
	delete Data;
}

void CPlotSettingsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_CALIBVALID, m_Type);
	DDX_Control(pDX, IDC_LIST_ALL, m_listAll);
	DDX_Control(pDX, IDC_LIST_SEL, m_listSel);

	DDX_CBIndex(pDX, IDC_CALIBVALID, m_vType);
}

BEGIN_MESSAGE_MAP(CPlotSettingsDlg, CDialog)

	ON_CBN_SELCHANGE(IDC_CALIBVALID, OnChangeType)

	ON_BN_CLICKED(IDC_ACCEPT, OnAccept)
	ON_BN_CLICKED(IDHELP, OnHelp)
	ON_BN_CLICKED(IDC_ADD, OnAdd)
	ON_BN_CLICKED(IDC_ADD_ALL, OnAddAll)
	ON_BN_CLICKED(IDC_REMOVE, OnRemove)
	ON_BN_CLICKED(IDC_REMOVE_ALL, OnRemoveAll)

END_MESSAGE_MAP()

BOOL CPlotSettingsDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	CString s;

	m_Type.ResetContent();
	s = cGetSpectraTypeName(C_SPECTRA_TYPE_ALL);
	m_Type.AddString(s);
	if(Mode == C_PLOT_SET_MODE_MODEL)
	{
		s = cGetSpectraTypeName(C_SPECTRA_TYPE_CALIB);
		m_Type.AddString(s);
		s = cGetSpectraTypeName(C_SPECTRA_TYPE_VALID);
		m_Type.AddString(s);
	}
	else
	{
		GetDlgItem(IDC_CALIBVALID)->EnableWindow(false);
		GetDlgItem(IDC_STATIC_4)->EnableWindow(false);
	}
	
	m_Type.SetCurSel(m_vType);

	m_listAll.SetUserCallback(LCF_CB_SI_TXTCOLOR, GetSpectrumColor);
	m_listAll.EnableUserCallback(LCF_CB_SI_TXTCOLOR, true);

	FillListAll();

	CString Hdr = ParcelLoadString(STRID_PLOT_SETTINGS_HDR);
	SetWindowText(Hdr);

	GetDlgItem(IDC_ACCEPT)->SetWindowText(ParcelLoadString(STRID_PLOT_SETTINGS_ACCEPT));
	GetDlgItem(IDCANCEL)->SetWindowText(ParcelLoadString(STRID_PLOT_SETTINGS_DECLINE));
	GetDlgItem(IDHELP)->SetWindowText(ParcelLoadString(STRID_PLOT_SETTINGS_HELP));
	GetDlgItem(IDC_ADD)->SetWindowText(ParcelLoadString(STRID_PLOT_SETTINGS_ADD));
	GetDlgItem(IDC_ADD_ALL)->SetWindowText(ParcelLoadString(STRID_PLOT_SETTINGS_ADDALL));
	GetDlgItem(IDC_REMOVE)->SetWindowText(ParcelLoadString(STRID_PLOT_SETTINGS_REMOVE));
	GetDlgItem(IDC_REMOVE_ALL)->SetWindowText(ParcelLoadString(STRID_PLOT_SETTINGS_REMOVEALL));
	GetDlgItem(IDC_STATIC_1)->SetWindowText(ParcelLoadString(STRID_PLOT_SETTINGS_CHOOSE));
	GetDlgItem(IDC_STATIC_4)->SetWindowText(ParcelLoadString(STRID_PLOT_SETTINGS_SHOWSPEC));

	GetDlgItem(IDHELP)->EnableWindow(pFrame->IsExistHelp());

	return true;
}

void CPlotSettingsDlg::OnAccept()
{
	if(!UpdateData())
		return;

	OldData->Copy(*Data);

	CDialog::OnOK();
}

void CPlotSettingsDlg::OnCancel()
{
	if(IsChanged())
		if(!ParcelConfirm(ParcelLoadString(STRID_PLOT_SETTINGS_ASK_DOCANCEL), false))
			return;

	CDialog::OnCancel();
}

void CPlotSettingsDlg::OnChangeType()
{
	m_vType = m_Type.GetCurSel();

	Data->SetType(Retype(m_vType));

	FillListAll();

	IsBtnClk = true;
}

void CPlotSettingsDlg::OnAdd()
{
	if(!UpdateData())
		return;

	int ind = -1, nSel = m_listAll.GetSelectedCount();
	if(nSel <= 0)
		return;

	int i;
	vector<SpectraShowSettings*> tmp;
	for(i=0; i<nSel; i++)
	{
		ind = m_listAll.GetNextItem(ind, LVNI_SELECTED);
		if(ind < 0 || ind >= Data->GetNSpectraType(Retype(m_vType)))
			return;

		SpectraShowSettings* Spec = Data->GetSpectraShow(Retype(m_vType), ind);
		if(Spec != NULL)
			tmp.push_back(Spec);
	}
	for(i=0; i<nSel; i++)
		tmp[i]->IsSel = true;

	tmp.clear();

	FillListSel(Data->GetNSpectraSel() - 1);

	UpdateData(0);

	IsBtnClk = true;
}

void CPlotSettingsDlg::OnAddAll()
{
	if(!UpdateData())
		return;

	Data->SetSelAll(true);

	FillListSel(Data->GetNSpectraSel() - 1);

	UpdateData(0);

	IsBtnClk = true;
}

void CPlotSettingsDlg::OnRemove()
{
	if(!UpdateData())
		return;

	int nSel = m_listSel.GetSelCount();
	CArray<int, int> Sels;
	Sels.SetSize(nSel);
	m_listSel.GetSelItems(nSel, Sels.GetData());

	int i, ind = 0;
	vector<SpectraShowSettings*> tmp;
	for(i=0; i<nSel; i++)
	{
		ind = Sels[i];
		if(ind < 0 || ind >= Data->GetNSpectraType(Retype(m_vType)))
			return;

		SpectraShowSettings* Spec = Data->GetSpectraShowSel(ind);
		if(Spec != NULL)
			tmp.push_back(Spec);
	}
	for(i=0; i<nSel; i++)
		tmp[i]->IsSel = false;

	tmp.clear();

	FillListSel(ind - 1);

	UpdateData(0);

	IsBtnClk = true;
}

void CPlotSettingsDlg::OnRemoveAll()
{
	if(!UpdateData())
		return;

	Data->SetSelAll(false);

	FillListSel();

	UpdateData(0);

	IsBtnClk = true;
}

void CPlotSettingsDlg::FillListAll(int newsel)
{
	m_listAll.DeleteAllItems();

	m_listAll.InsertColumn(0, cEmpty, LVCFMT_LEFT, 0, 0);
	CRect Rect;
	m_listAll.GetWindowRect(Rect);
	m_listAll.SetColumnWidth(0, Rect.Width()-5);
	m_listAll.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

	int i;
	for(i=0; i<Data->GetNSpectraType(Retype(m_vType)); i++)
	{
		SpectraShowSettings* Spec = Data->GetSpectraShow(Retype(m_vType), i);
		if(Spec != NULL)
            m_listAll.InsertItem(i, Spec->Name);
	}
	int sel = (newsel >= 0 && Data->GetNSpectraType(Retype(m_vType)) > 0) ? newsel : Data->GetNSpectraType(Retype(m_vType)) - 1;
	m_listAll.SetItemState(sel, LVIS_SELECTED, LVIS_SELECTED);

	CString s;
	s.Format(ParcelLoadString(STRID_PLOT_SETTINGS_NUMSPECAVAL), Data->GetNSpectraType(Retype(m_vType)));
	GetDlgItem(IDC_STATIC_2)->SetWindowText(s);

	FillListSel();
}

void CPlotSettingsDlg::FillListSel(int newsel)
{
	m_listSel.ResetContent();

	int i;
	for(i=0; i<Data->GetNSpectraSel(); i++)
	{
		SpectraShowSettings* Spec = Data->GetSpectraShowSel(i);
		if(Spec != NULL)
            m_listSel.AddString(Spec->Name);
	}
	int sel = (newsel >= 0 && Data->GetNSpectraSel() > 0) ? newsel : Data->GetNSpectraSel() - 1;
	m_listSel.SetCurSel(sel);

	CString s;
	s.Format(ParcelLoadString(STRID_PLOT_SETTINGS_NUMSPECSEL), Data->GetNSpectraSel());
	GetDlgItem(IDC_STATIC_3)->SetWindowText(s);

	m_listAll.RedrawWindow();
}

int CPlotSettingsDlg::Retype(int type, bool dir)
{
	int res = 0;
	if(dir)
	{
		switch(type)
		{
		case C_SPECTRA_TYPE_ALL:  res = 0;  break;
		case C_SPECTRA_TYPE_CALIB:  res = 1;  break;
		case C_SPECTRA_TYPE_VALID:  res = 2;  break;
		default:  res = 0;
		}
	}
	else
	{
		switch(type)
		{
		case 0:  res = C_SPECTRA_TYPE_ALL;  break;
		case 1:  res = C_SPECTRA_TYPE_CALIB;  break;
		case 2:  res = C_SPECTRA_TYPE_VALID;  break;
		default:  res = C_SPECTRA_TYPE_ALL;
		}
	}

	return res;
}

bool CPlotSettingsDlg::IsChanged()
{
	return IsBtnClk;
}

bool CPlotSettingsDlg::IsSpectrumSel(int ind)
{
	SpectraShowSettings* Spec = Data->GetSpectraShow(Retype(m_vType), ind);

	return (Data->GetSpectraShowSel(Spec->Name) != NULL);
}

void CPlotSettingsDlg::OnHelp()
{
	int IdHelp = DLGID_SHOW_PLOT_SETTINGS;

	pFrame->ShowHelp(IdHelp);
}
