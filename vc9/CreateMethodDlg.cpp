#include "stdafx.h"

#include "BranchInfo.h"
#include "CreateMethodDlg.h"
#include "MainFrm.h"


static bool iscancel = false;
static CCreateMethodDlg* pMtdDlg = NULL;

static int GetParamColor(int item, int /*subitem*/, void* /*pData*/)
{
	if(pMtdDlg == NULL)
		return 0;

	COLORREF color = (pMtdDlg->IsParamSel(item)) ? cColorPass : CLR_DEFAULT;

	pMtdDlg->m_listParAll.CurTxtColor = color;

	return 1;
}

static int GetModelQltColor(int item, int /*subitem*/, void* /*pData*/)
{
	if(pMtdDlg == NULL)
		return 0;

	COLORREF color = (pMtdDlg->IsModelQltSel(item)) ? cColorPass : CLR_DEFAULT;

	pMtdDlg->m_listModQltAll.CurTxtColor = color;

	return 1;
}

static int GetModelQntColor(int item, int /*subitem*/, void* /*pData*/)
{
	if(pMtdDlg == NULL)
		return 0;

	COLORREF color = (pMtdDlg->IsModelQntSel(item)) ? cColorPass : CLR_DEFAULT;

	pMtdDlg->m_listModAll.CurTxtColor = color;

	return 1;
}

IMPLEMENT_DYNAMIC(CCreateMethodDlg, CDialog)

CCreateMethodDlg::CCreateMethodDlg(CMethodData* data, int mode, CWnd* pParent)
	: CDialog(CCreateMethodDlg::IDD, pParent)
{
	pMtdDlg = this;

	MtdData = data;
	Mode = mode;

	ParentPrj = GetProjectByHItem(MtdData->GetParentHItem());
	GrandParentDev = GetDeviceByHItem(ParentPrj->GetParentHItem());

	NAllParams = 0;
	NAllQltModels = 0;

	m_vName = MtdData->GetName();
	m_vType = MtdData->GetAnalysisType();
	m_vLow = MtdData->GetLowTemp();
	m_vUpper = MtdData->GetUpperTemp();
	m_vDiff = MtdData->GetMaxDiffTemp();

	m_vUse = false;
	m_vStdMois = 0;

	m_vMois = 0;
	m_vDefMod = 0;

	CurRule = MtdData->Rules.end();

	IsBtnClk = false;
}

CCreateMethodDlg::~CCreateMethodDlg()
{
}

void CCreateMethodDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_INPUT_NAME, m_Name);
	DDX_Control(pDX, IDC_ANALYSIS_TYPE, m_Type);
	DDX_Control(pDX, IDC_PRJPARAMS_LIST, m_listParAll);
	DDX_Control(pDX, IDC_PRJPARAMSSEL_LIST, m_listParSel);
	DDX_Control(pDX, IDC_MODELS_LIST, m_listModAll);
	DDX_Control(pDX, IDC_MODELSSEL_LIST, m_listModSel);
	DDX_Control(pDX, IDC_QLTMODELS_LIST, m_listModQltAll);
	DDX_Control(pDX, IDC_QLTMODELSSEL_LIST, m_listModQltSel);
	DDX_Control(pDX, IDC_LOWLIM_TEMP, m_Low);
	DDX_Control(pDX, IDC_UPPERLIM_TEMP, m_Upper);
	DDX_Control(pDX, IDC_MAXDIFF_TEMP, m_Diff);
	DDX_Control(pDX, IDC_STANDART_MOISTURE, m_StdMois);
	DDX_Control(pDX, IDC_AS_MOISTURE, m_Mois);
	DDX_Control(pDX, IDC_MAIN_NAME, m_DefMod);
	DDX_Control(pDX, IDC_USE, m_Use);

	DDX_CBIndex(pDX, IDC_ANALYSIS_TYPE, m_vType);
	DDX_CBIndex(pDX, IDC_AS_MOISTURE, m_vMois);
	DDX_CBIndex(pDX, IDC_MAIN_NAME, m_vDefMod);

	cParcelDDX(pDX, IDC_INPUT_NAME, m_vName);
	DDV_MaxChars(pDX, m_vName, 50);

	cParcelDDX(pDX, IDC_LOWLIM_TEMP, m_vLow);
	cParcelDDX(pDX, IDC_UPPERLIM_TEMP, m_vUpper);
	cParcelDDX(pDX, IDC_MAXDIFF_TEMP, m_vDiff);

	DDX_Check(pDX, IDC_USE, m_vUse);
	if(m_vUse)
	{
		cParcelDDX(pDX, IDC_STANDART_MOISTURE, m_vStdMois);
		cParcelDDV(pDX, ParcelLoadString(STRID_CREATE_METHOD_DDV_STDMOIS), m_vStdMois, 0., 100.);
	}

}

BEGIN_MESSAGE_MAP(CCreateMethodDlg, CDialog)

	ON_CBN_SELCHANGE(IDC_ANALYSIS_TYPE, OnChangeAnalysisType)
	ON_CBN_SELCHANGE(IDC_AS_MOISTURE, OnChangeLabelMoisture)
	ON_CBN_SELCHANGE(IDC_MAIN_NAME, OnChangeDefModel)
	ON_CBN_SELCHANGE(IDC_PRJPARAMSSEL_LIST, OnParamsSelListSelChange)

	ON_BN_CLICKED(IDC_ACCEPT, OnAccept)
	ON_BN_CLICKED(IDHELP, OnHelp)
	ON_BN_CLICKED(IDC_USE, OnSetUse)
	ON_BN_CLICKED(IDC_ADD, OnAdd)
	ON_BN_CLICKED(IDC_ADD_ALL, OnAddAll)
	ON_BN_CLICKED(IDC_REMOVE, OnRemove)
	ON_BN_CLICKED(IDC_REMOVE_ALL, OnRemoveAll)
	ON_BN_CLICKED(IDC_ADD_2, OnAdd2)
	ON_BN_CLICKED(IDC_ADD_ALL_2, OnAddAll2)
	ON_BN_CLICKED(IDC_REMOVE_2, OnRemove2)
	ON_BN_CLICKED(IDC_REMOVE_ALL_2, OnRemoveAll2)

	ON_BN_CLICKED(IDC_ADD_4, OnAdd4)
	ON_BN_CLICKED(IDC_ADD_ALL_4, OnAddAll4)
	ON_BN_CLICKED(IDC_REMOVE_4, OnRemove4)
	ON_BN_CLICKED(IDC_REMOVE_ALL_4, OnRemoveAll4)
	ON_BN_CLICKED(IDC_FORMULA_MEAN, OnInsertMEAN)
	ON_BN_CLICKED(IDC_FORMULA_AND, OnInsertAND)
	ON_BN_CLICKED(IDC_FORMULA_OR, OnInsertOR)
	ON_BN_CLICKED(IDC_FORMULA_BRACKETOPEN, OnInsertOPEN)
	ON_BN_CLICKED(IDC_FORMULA_BRACKETCLOSE, OnInsertCLOSE)
	ON_BN_CLICKED(IDC_BACK, OnBack)
	ON_BN_CLICKED(IDC_CLEAR, OnClear)

	ON_EN_KILLFOCUS(IDC_STANDART_MOISTURE, OnStdMoisChanged)

END_MESSAGE_MAP()

BOOL CCreateMethodDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	iscancel = false;

	CString s;
	int i, j;

	m_Type.ResetContent();
	for(i=C_ANALYSIS_QNT; i<=C_ANALYSIS_QLT; i++)
	{
		s = cGetAnalysisTypeName(i);
		m_Type.AddString(s);
	}
	m_Type.SetCurSel(m_vType);

	m_listParAll.InsertColumn(0, cEmpty, LVCFMT_LEFT, 0, 0);
	CRect Rect;
	m_listParAll.GetWindowRect(Rect);
	m_listParAll.SetColumnWidth(0, Rect.Width()-5);
	m_listParAll.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

	int N = ParentPrj->GetNParams();
	NAllParams = 0;
	m_listParAll.DeleteAllItems();
	for(i=0, j=0; i<N; i++)
	{
		s = ParentPrj->GetParam(i)->GetName();
		if(!IsAvailableModelExist(s))
			continue;

		Names.push_back(s);
		NAllParams++;

		m_listParAll.InsertItem(j, s);
		j++;
	}
	if(NAllParams > 0)
		m_listParAll.SetItemState(0, LVIS_SELECTED, LVIS_SELECTED);

	FillListParamSel();

	m_listModQltAll.InsertColumn(0, cEmpty, LVCFMT_LEFT, 0, 0);
	m_listModQltAll.GetWindowRect(Rect);
	m_listModQltAll.SetColumnWidth(0, Rect.Width()-5);
	m_listModQltAll.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

	NAllQltModels = ParentPrj->GetNQltModels();
	m_listModQltAll.DeleteAllItems();
	for(i=0; i<NAllQltModels; i++)
	{
		s = ParentPrj->GetQltModel(i)->GetName();

		QltAllNames.push_back(s);

		m_listModQltAll.InsertItem(i, s);
	}
	if(NAllQltModels > 0)
		m_listModQltAll.SetItemState(0, LVIS_SELECTED, LVIS_SELECTED);
	else
	{
		GetDlgItem(IDC_ADD_4)->EnableWindow(false);
		GetDlgItem(IDC_ADD_ALL_4)->EnableWindow(false);

		m_vType = C_ANALYSIS_QNT;
		m_Type.SetCurSel(m_vType);
		GetDlgItem(IDC_ANALYSIS_TYPE)->EnableWindow(false);
		GetDlgItem(IDC_STATIC_5)->EnableWindow(false);
	}

	QltNames.clear();
	ItIntStr itQ;
	for(itQ=MtdData->ModelQltNames.begin(); itQ!=MtdData->ModelQltNames.end(); ++itQ)
		QltNames.push_back(itQ->second);

	s = ParcelLoadString(STRID_CREATE_METHOD_COLNUM);
	m_listModQltSel.InsertColumn(0, s, LVCFMT_LEFT, 0, 0);
	s = ParcelLoadString(STRID_CREATE_METHOD_COLMODNAME);
	m_listModQltSel.InsertColumn(1, s, LVCFMT_LEFT, 0, 1);
	s = ParcelLoadString(STRID_EDIT_PARAMLIST_COLUNIT);
	for(i=0; i<2; i++)
		m_listModQltSel.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	m_listModQltSel.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

	m_listParAll.SetUserCallback(LCF_CB_SI_TXTCOLOR, GetParamColor);
	m_listParAll.EnableUserCallback(LCF_CB_SI_TXTCOLOR, true);

	m_listModQltAll.SetUserCallback(LCF_CB_SI_TXTCOLOR, GetModelQltColor);
	m_listModQltAll.EnableUserCallback(LCF_CB_SI_TXTCOLOR, true);

	m_listModAll.SetUserCallback(LCF_CB_SI_TXTCOLOR, GetModelQntColor);
	m_listModAll.EnableUserCallback(LCF_CB_SI_TXTCOLOR, true);

	Formula.Copy(MtdData->Formula);

	FillListQltModelSel();

	CString Hdr;
	switch(Mode)
	{
	case MTDDLG_MODE_CREATE:
		Hdr = ParcelLoadString(STRID_CREATE_METHOD_HDR_CREATE);
		break;
	case MTDDLG_MODE_CHANGE:
		Hdr = ParcelLoadString(STRID_CREATE_METHOD_HDR_CHANGE);
		break;
	case MTDDLG_MODE_COPY:
		Hdr = ParcelLoadString(STRID_CREATE_METHOD_HDR_COPY);
		break;
	}
	SetWindowText(Hdr);

	if(MtdData->Rules.size() == 0)
	{
		GetDlgItem(IDC_REMOVE)->EnableWindow(false);
		GetDlgItem(IDC_REMOVE_ALL)->EnableWindow(false);
	}

	if((GrandParentDev->GetType() != C_DEV_TYPE_40) && (GrandParentDev->GetType() != C_DEV_TYPE_12M))
	{
		GetDlgItem(IDC_LOWLIM_TEMP)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_UPPERLIM_TEMP)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_MAXDIFF_TEMP)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STATIC_9)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STATIC_10)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STATIC_11)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STATIC_12)->ShowWindow(SW_HIDE);
	}

	ShowFields();

	GetDlgItem(IDC_ACCEPT)->SetWindowText(ParcelLoadString(STRID_CREATE_METHOD_ACCEPT));
	GetDlgItem(IDCANCEL)->SetWindowText(ParcelLoadString(STRID_CREATE_METHOD_DECLINE));
	GetDlgItem(IDHELP)->SetWindowText(ParcelLoadString(STRID_CREATE_METHOD_HELP));
	GetDlgItem(IDC_ADD)->SetWindowText(ParcelLoadString(STRID_CREATE_METHOD_ADD));
	GetDlgItem(IDC_ADD_ALL)->SetWindowText(ParcelLoadString(STRID_CREATE_METHOD_ADDALL));
	GetDlgItem(IDC_REMOVE)->SetWindowText(ParcelLoadString(STRID_CREATE_METHOD_REMOVE));
	GetDlgItem(IDC_REMOVE_ALL)->SetWindowText(ParcelLoadString(STRID_CREATE_METHOD_REMOVEALL));
	GetDlgItem(IDC_ADD_2)->SetWindowText(ParcelLoadString(STRID_CREATE_METHOD_ADD));
	GetDlgItem(IDC_ADD_ALL_2)->SetWindowText(ParcelLoadString(STRID_CREATE_METHOD_ADDALL));
	GetDlgItem(IDC_REMOVE_2)->SetWindowText(ParcelLoadString(STRID_CREATE_METHOD_REMOVE));
	GetDlgItem(IDC_REMOVE_ALL_2)->SetWindowText(ParcelLoadString(STRID_CREATE_METHOD_REMOVEALL));
	GetDlgItem(IDC_ADD_4)->SetWindowText(ParcelLoadString(STRID_CREATE_METHOD_ADD));
	GetDlgItem(IDC_ADD_ALL_4)->SetWindowText(ParcelLoadString(STRID_CREATE_METHOD_ADDALL));
	GetDlgItem(IDC_REMOVE_4)->SetWindowText(ParcelLoadString(STRID_CREATE_METHOD_REMOVE));
	GetDlgItem(IDC_REMOVE_ALL_4)->SetWindowText(ParcelLoadString(STRID_CREATE_METHOD_REMOVEALL));
	GetDlgItem(IDC_STATIC_1)->SetWindowText(ParcelLoadString(STRID_CREATE_METHOD_MTDNAME));
	GetDlgItem(IDC_STATIC_2)->SetWindowText(ParcelLoadString(STRID_CREATE_METHOD_ALLPARAMS));
	GetDlgItem(IDC_STATIC_3)->SetWindowText(ParcelLoadString(STRID_CREATE_METHOD_SELPARAMS));
	GetDlgItem(IDC_STATIC_4)->SetWindowText(ParcelLoadString(STRID_CREATE_METHOD_QNTPARAMS));
	GetDlgItem(IDC_STATIC_5)->SetWindowText(ParcelLoadString(STRID_CREATE_METHOD_ANTYPE));
	GetDlgItem(IDC_STATIC_7)->SetWindowText(ParcelLoadString(STRID_CREATE_METHOD_ALLMODELS));
	GetDlgItem(IDC_STATIC_8)->SetWindowText(ParcelLoadString(STRID_CREATE_METHOD_SELMODELS));
	GetDlgItem(IDC_STATIC_9)->SetWindowText(ParcelLoadString(STRID_CREATE_METHOD_FROM));
	GetDlgItem(IDC_STATIC_10)->SetWindowText(ParcelLoadString(STRID_CREATE_METHOD_TO));
	GetDlgItem(IDC_STATIC_11)->SetWindowText(ParcelLoadString(STRID_CREATE_METHOD_DIFFTEMP));
	GetDlgItem(IDC_STATIC_12)->SetWindowText(ParcelLoadString(STRID_CREATE_METHOD_TEMPRANGE));
	GetDlgItem(IDC_STATIC_13)->SetWindowText(ParcelLoadString(STRID_CREATE_METHOD_SETASMOIS));
	GetDlgItem(IDC_STATIC_14)->SetWindowText(ParcelLoadString(STRID_CREATE_METHOD_STDMOIS));
	GetDlgItem(IDC_STATIC_15)->SetWindowText(ParcelLoadString(STRID_CREATE_METHOD_DEFMODEL));
	GetDlgItem(IDC_STATIC_16)->SetWindowText(ParcelLoadString(STRID_CREATE_METHOD_QLTMODELS));
	GetDlgItem(IDC_STATIC_17)->SetWindowText(ParcelLoadString(STRID_CREATE_METHOD_ALLMODELS));
	GetDlgItem(IDC_STATIC_18)->SetWindowText(ParcelLoadString(STRID_CREATE_METHOD_SELMODELS));
	GetDlgItem(IDC_STATIC_19)->SetWindowText(ParcelLoadString(STRID_CREATE_METHOD_FORMULA));
	GetDlgItem(IDC_USE)->SetWindowText(ParcelLoadString(STRID_CREATE_METHOD_USE));
	GetDlgItem(IDC_STR_UNIT)->SetWindowText(ParcelLoadString(STRID_CREATE_METHOD_DEFUNIT));
	GetDlgItem(IDC_FORMULA_MEAN)->SetWindowText(ParcelLoadString(STRID_CREATE_METHOD_FORMULA_MEAN));
	GetDlgItem(IDC_FORMULA_AND)->SetWindowText(ParcelLoadString(STRID_CREATE_METHOD_FORMULA_AND));
	GetDlgItem(IDC_FORMULA_OR)->SetWindowText(ParcelLoadString(STRID_CREATE_METHOD_FORMULA_OR));
	GetDlgItem(IDC_FORMULA_BRACKETOPEN)->SetWindowText(ParcelLoadString(STRID_CREATE_METHOD_FORMULA_OPEN));
	GetDlgItem(IDC_FORMULA_BRACKETCLOSE)->SetWindowText(ParcelLoadString(STRID_CREATE_METHOD_FORMULA_CLOSE));
	GetDlgItem(IDC_BACK)->SetWindowText(ParcelLoadString(STRID_CREATE_METHOD_FORMULA_BACK));
	GetDlgItem(IDC_CLEAR)->SetWindowText(ParcelLoadString(STRID_CREATE_METHOD_FORMULA_CLEAR));

	GetDlgItem(IDHELP)->EnableWindow(pFrame->IsExistHelp());

	return true;
}

void CCreateMethodDlg::OnAccept()
{
	if(!UpdateData())
		return;

	bool qnt = (m_vType == C_ANALYSIS_QNT);
	int nQlt = int(QltNames.size());

	CString s;

	m_vName.Trim();
	if(m_vName.IsEmpty())
	{
		ParcelError(ParcelLoadString(STRID_CREATE_METHOD_ERROR_EMPTYNAME));
		return;
	}
	if(!cIsBadSymbolInName(m_vName))
	{
		ParcelError(ParcelLoadString(STRID_CREATE_METHOD_ERROR_BADSYMBOL));
		return;
	}
	if(Mode == MTDDLG_MODE_CREATE || Mode == MTDDLG_MODE_COPY || MtdData->GetName().Compare(m_vName))
	{
		CMethodData *mtd = ParentPrj->GetMethod(m_vName);
		if(mtd != NULL)
		{
			s.Format(ParcelLoadString(STRID_CREATE_METHOD_ERROR_NAME), m_vName);
			ParcelError(s);
			return;
		}
	}

	if(GrandParentDev->GetType() == C_DEV_TYPE_40 || GrandParentDev->GetType() == C_DEV_TYPE_12M)
	{
		int MinTemp = MtdData->GetLowTemp();
		int MaxTemp = MtdData->GetUpperTemp();
		int MaxDiff = MtdData->GetMaxDiffTemp();
		if(m_vLow < MinTemp || m_vLow > MaxTemp)
		{
			s.Format(ParcelLoadString(STRID_CREATE_METHOD_ERROR_LOWTEMP), MinTemp, MaxTemp);
			ParcelError(s);
			return;
		}
		if(m_vUpper < MinTemp || m_vUpper > MaxTemp)
		{
			s.Format(ParcelLoadString(STRID_CREATE_METHOD_ERROR_UPPERTEMP), MinTemp, MaxTemp);
			ParcelError(s);
			return;
		}
		if(m_vDiff < 0 || m_vDiff > MaxDiff)
		{
			s.Format(ParcelLoadString(STRID_CREATE_METHOD_ERROR_UPPERTEMP), 0, MaxDiff);
			ParcelError(s);
			return;
		}
		if(m_vLow > m_vUpper)
		{
			ParcelError(ParcelLoadString(STRID_CREATE_METHOD_ERROR_SAMPLETEMP));
			return;
		}
	}

	if(qnt)
	{
		MtdData->DeleteAllModelQltNames();
		if(int(MtdData->Rules.size()) <= 0)
		{
			s = ParcelLoadString(STRID_CREATE_METHOD_ERROR_NORULES);
			ParcelError(s);
			return;
		}

		ItRul it;
		for(it=MtdData->Rules.begin(); it!=MtdData->Rules.end(); ++it)
		{
			if(int(it->AddModels.size()) <= 0)
			{
				CString s1 = ParcelLoadString(STRID_CREATE_METHOD_ERROR_NOMODELS);
				s.Format(s1, it->Name);
				ParcelError(s);
				return;
			}
		}
	}
	else
	{
		MtdData->Rules.clear();
		if(nQlt <= 0)
		{
			s = ParcelLoadString(STRID_CREATE_METHOD_ERROR_QLT_NOMODELS);
			ParcelError(s);
			return;
		}
		if(Formula.CheckFormula(nQlt) != C_MTD_QLTFORM_ERR_OK)
		{
			s = ParcelLoadString(STRID_CREATE_METHOD_ERROR_QLT_BADFORM);
			ParcelError(s);
			return;
		}

		int ind;
		ItStr itQ;
		MtdData->ModelQltNames.clear();
		for(ind=0, itQ=QltNames.begin(); itQ!=QltNames.end(); ++itQ, ind++)
			MtdData->AddModelQltName(ind, (*itQ));
	}

	MtdData->SetName(m_vName);
	MtdData->SetAnalysisType(m_vType);

	if(qnt)
	{
		if(m_vMois <= 0)
			MtdData->SetMoisParam(cEmpty);
		else
			MtdData->SetMoisParam(MtdData->Rules[m_vMois-1].Name);
	}

	if(GrandParentDev->GetType() == C_DEV_TYPE_40 || GrandParentDev->GetType() == C_DEV_TYPE_12M)
	{
		MtdData->SetLowTemp(m_vLow);
		MtdData->SetUpperTemp(m_vUpper);
		MtdData->SetMaxDiffTemp(m_vDiff);
	}

	MtdData->Formula.Copy(Formula);

	MtdData->SetDate();

	CDialog::OnOK();
}

void CCreateMethodDlg::OnCancel()
{
	if(Mode == MTDDLG_MODE_CHANGE && IsChanged())
	{
		if(!ParcelConfirm(ParcelLoadString(STRID_CREATE_METHOD_ASK_DOCANCEL), false))
			return;
	}

	iscancel = true;

	CDialog::OnCancel();
}

void CCreateMethodDlg::OnParamsSelListSelChange()
{
	int sel = m_listParSel.GetCurSel();
	if(sel < 0 || sel >= int(MtdData->Rules.size()))
		return;

	CurRule = MtdData->Rules.begin() + sel;

	FillListModelAll();
}

void CCreateMethodDlg::OnChangeAnalysisType()
{
	m_vType = m_Type.GetCurSel();

	ShowFields();
	ShowFormula();
}

void CCreateMethodDlg::OnChangeLabelMoisture()
{
	m_vMois = m_Mois.GetCurSel();

	if(m_vMois == 0)
	{
		ItRul it;
		for(it=MtdData->Rules.begin(); it!=MtdData->Rules.begin(); ++it)
			it->UseStdMoisture = false;
	}

	if(m_vMois == 0 || m_listParSel.GetCurSel() == m_vMois - 1)
	{
		if(CurRule != MtdData->Rules.end())
			CurRule->UseStdMoisture = false;

		m_vUse = false;
		GetDlgItem(IDC_STATIC_14)->EnableWindow(false);
		GetDlgItem(IDC_USE)->EnableWindow(false);
	}
	else
	{
		if(CurRule != MtdData->Rules.end())
		{
			m_vUse = CurRule->UseStdMoisture;
			GetDlgItem(IDC_STATIC_14)->EnableWindow(true);
			GetDlgItem(IDC_USE)->EnableWindow(true);
		}
		else
		{
			m_vUse = false;
			GetDlgItem(IDC_STATIC_14)->EnableWindow(false);
			GetDlgItem(IDC_USE)->EnableWindow(false);
		}
	}
	m_Use.SetCheck(m_vUse);
	OnSetUse();
}

void CCreateMethodDlg::OnChangeDefModel()
{
	m_vDefMod = m_DefMod.GetCurSel();

	if(CurRule != MtdData->Rules.end())
		CurRule->MainModel = (m_vDefMod == 0) ? cEmpty : CurRule->AddModels[m_vDefMod-1];
}

void CCreateMethodDlg::OnAdd()
{
	if(!UpdateData())
		return;
/*
	int ind = -1, nSel = m_listParAll.GetSelectedCount();
	if(nSel <= 0)
		return;
*/
	int ind = m_listParAll.GetNextItem(-1, LVNI_SELECTED);
	if(ind < 0 || ind >= int(Names.size()))
		return;

	CString Name = Names[ind];

	ItRul it;
	for(it=MtdData->Rules.begin(); it!=MtdData->Rules.end(); ++it)
		if(!(*it).Name.Compare(Name))
			return;

	AnalyseRule NewRule(Name);

	MtdData->Rules.push_back(NewRule);

	FillListParamSel(int(MtdData->Rules.size() - 1));

	m_listParAll.RedrawWindow();

	UpdateData(0);
}

void CCreateMethodDlg::OnAddAll()
{
	if(!UpdateData())
		return;

	ItStr it2;
	for(it2=Names.begin(); it2!=Names.end(); ++it2)
	{
		bool br = false;
		ItRul it;
		for(it=MtdData->Rules.begin(); it!=MtdData->Rules.end(); ++it)
			if(!(*it).Name.Compare(*it2))
			{
				br = true;
				break;
			}

		if(br)  continue;

		AnalyseRule NewRule(*it2);

		MtdData->Rules.push_back(NewRule);
	}

	int ind = m_listParAll.GetNextItem(-1, LVNI_SELECTED);
	if(ind < 0 || ind >= int(Names.size()))
		ind = int(MtdData->Rules.size() - 1);

	FillListParamSel(ind);
	UpdateData(0);
}

void CCreateMethodDlg::OnRemove()
{
	if(!UpdateData())
		return;
	if(CurRule == MtdData->Rules.end())
		return;

	int ind = int(CurRule - MtdData->Rules.begin());
	MtdData->Rules.erase(CurRule);

	FillListParamSel(ind - 1);
	UpdateData(0);
}

void CCreateMethodDlg::OnRemoveAll()
{
	if(!UpdateData())
		return;

	MtdData->Rules.clear();

	FillListParamSel();
	UpdateData(0);
}

void CCreateMethodDlg::OnAdd2()
{
	if(!UpdateData())
		return;
	if(CurRule == MtdData->Rules.end())
		return;
/*
	int ind = -1, nSel = m_listModAll.GetSelectedCount();
	if(nSel <= 0)
		return;
*/
	int ind = m_listModAll.GetNextItem(-1, LVNI_SELECTED);
	if(ind < 0 || ind >= int(CurModNames.size()))
		return;

	CString Name = CurModNames[ind];
	ItStr it;
	for(it=CurRule->AddModels.begin(); it!=CurRule->AddModels.end(); ++it)
		if(!(*it).Compare(Name))
			return;

	CurRule->AddModels.push_back(Name);

	FillListModelSel(int(CurRule->AddModels.size()) - 1);
	UpdateData(0);
}

void CCreateMethodDlg::OnAddAll2()
{
	if(!UpdateData())
		return;
	if(CurRule == MtdData->Rules.end())
		return;

	CurRule->AddModels.clear();
	copy(CurModNames.begin(), CurModNames.end(), inserter(CurRule->AddModels, CurRule->AddModels.begin()));

	int ind = m_listModAll.GetNextItem(-1, LVNI_SELECTED);
	if(ind < 0 || ind >= int(CurModNames.size()))
		ind = int(CurRule->AddModels.size()) - 1;

	FillListModelSel(ind);
	UpdateData(0);
}

void CCreateMethodDlg::OnRemove2()
{
	if(!UpdateData())
		return;
	if(CurRule == MtdData->Rules.end())
		return;

	int ind = m_listModSel.GetCurSel();
	if(ind < 0 || ind >= int(CurRule->AddModels.size()))
		return;

	ItStr it = CurRule->AddModels.begin() + ind;

	if(!CurRule->MainModel.Compare(*it))
		CurRule->MainModel = cEmpty;

	CurRule->AddModels.erase(it);

	FillListModelSel(ind - 1);
	UpdateData(0);
}

void CCreateMethodDlg::OnRemoveAll2()
{
	if(!UpdateData())
		return;
	if(CurRule == MtdData->Rules.end())
		return;

	CurRule->AddModels.clear();
	CurRule->MainModel = cEmpty;

	FillListModelSel();
	UpdateData(0);
}

void CCreateMethodDlg::OnStdMoisChanged()
{
	if(iscancel)
		return;
	if(GetFocus() == GetDlgItem(IDCANCEL))
	{
		OnCancel();
		return;
	}

	if(!UpdateData())
		return;
	if(CurRule == MtdData->Rules.end())
		return;

	CurRule->Moisture = m_vStdMois;
}

void CCreateMethodDlg::OnSetUse()
{
	int Use = m_Use.GetCheck();

	if(CurRule != MtdData->Rules.end())
		CurRule->UseStdMoisture = (Use) ? true : false;

	GetDlgItem(IDC_STR_UNIT)->EnableWindow(m_Use.GetCheck());
	GetDlgItem(IDC_STANDART_MOISTURE)->EnableWindow(m_Use.GetCheck());

	if(Use)
	{
		CString s;
		s.Format(cFmt31, m_vStdMois);
		GetDlgItem(IDC_STANDART_MOISTURE)->SetWindowText(s);
	}
	else
		GetDlgItem(IDC_STANDART_MOISTURE)->SetWindowText(ParcelLoadString(STRID_CREATE_METHOD_PARAM_NODATA));
}

void CCreateMethodDlg::OnAdd4()
{
	if(!UpdateData())
		return;

/*
	int ind = -1, nSel = m_listModQltAll.GetSelectedCount();
	if(nSel <= 0)
		return;
*/
	int ind = m_listModQltAll.GetNextItem(-1, LVNI_SELECTED);
	if(ind < 0 || ind >= NAllQltModels)
		return;

	CString Name = QltAllNames[ind];

	ItStr it;
	for(it=QltNames.begin(); it!=QltNames.end(); ++it)
		if(!(*it).Compare(Name))
			return;

	QltNames.push_back(Name);

	FillListQltModelSel(int(QltNames.size() - 1));
	UpdateData(0);
}

void CCreateMethodDlg::OnAddAll4()
{
	if(!UpdateData())
		return;

	ItStr it2;
	for(it2=QltAllNames.begin(); it2!=QltAllNames.end(); ++it2)
	{
		bool br = false;
		ItStr it;
		for(it=QltNames.begin(); it!=QltNames.end(); ++it)
			if(!(*it).Compare(*it2))
			{
				br = true;
				break;
			}

		if(br)  continue;

		QltNames.push_back((*it2));
	}

	FillListQltModelSel(int(QltNames.size() - 1));
	UpdateData(0);
}

void CCreateMethodDlg::OnRemove4()
{
	if(!UpdateData())
		return;

	int sel = m_listModQltSel.GetNextItem(-1, LVNI_SELECTED);
	if(sel < 0 || sel >= int(QltNames.size()))
		return;

	ItStr it = QltNames.begin() + sel;

	QltNames.erase(it);

	Formula.Clear();

	FillListQltModelSel(sel - 1);
	UpdateData(0);
}

void CCreateMethodDlg::OnRemoveAll4()
{
	if(!UpdateData())
		return;

	QltNames.clear();

	Formula.Clear();

	FillListQltModelSel();
	UpdateData(0);
}

void CCreateMethodDlg::OnInsertMEAN()
{
	if(!UpdateData())
		return;

	int N = int(QltNames.size());
	int sel = m_listModQltSel.GetNextItem(-1, LVNI_SELECTED);
	if(sel < 0 || sel >= int(QltNames.size()))
		return;

	if(Formula.AddElement(N, C_MTD_QLTFORM_MEAN, sel))
		ShowFormula();
}

void CCreateMethodDlg::OnInsertAND()
{
	if(!UpdateData())
		return;

	int N = int(QltNames.size());
	if(Formula.AddElement(N, C_MTD_QLTFORM_AND))
		ShowFormula();
}

void CCreateMethodDlg::OnInsertOR()
{
	if(!UpdateData())
		return;

	int N = int(QltNames.size());
	if(Formula.AddElement(N, C_MTD_QLTFORM_OR))
		ShowFormula();
}

void CCreateMethodDlg::OnInsertOPEN()
{
	if(!UpdateData())
		return;

	int N = int(QltNames.size());
	if(Formula.AddElement(N, C_MTD_QLTFORM_OPEN))
		ShowFormula();
}

void CCreateMethodDlg::OnInsertCLOSE()
{
	if(!UpdateData())
		return;

	int N = int(QltNames.size());
	if(Formula.AddElement(N, C_MTD_QLTFORM_CLOSE))
		ShowFormula();
}

void CCreateMethodDlg::OnBack()
{
	if(!UpdateData())
		return;

	Formula.DeleteLast();
	ShowFormula();
}

void CCreateMethodDlg::OnClear()
{
	if(!UpdateData())
		return;

	Formula.Clear();
	ShowFormula();
}

void CCreateMethodDlg::ShowFields()
{
	int qlt = (m_vType == C_ANALYSIS_QLT) ? SW_SHOW : SW_HIDE;
	int qnt = (m_vType == C_ANALYSIS_QNT) ? SW_SHOW : SW_HIDE;

	GetDlgItem(IDC_STATIC_4)->ShowWindow(qnt);
	GetDlgItem(IDC_STATIC_2)->ShowWindow(qnt);
	GetDlgItem(IDC_STATIC_3)->ShowWindow(qnt);
	GetDlgItem(IDC_PRJPARAMSSEL_LIST)->ShowWindow(qnt);
	GetDlgItem(IDC_PRJPARAMS_LIST)->ShowWindow(qnt);
	GetDlgItem(IDC_ADD)->ShowWindow(qnt);
	GetDlgItem(IDC_ADD_ALL)->ShowWindow(qnt);
	GetDlgItem(IDC_REMOVE)->ShowWindow(qnt);
	GetDlgItem(IDC_REMOVE_ALL)->ShowWindow(qnt);
	GetDlgItem(IDC_STATIC_13)->ShowWindow(qnt);
	GetDlgItem(IDC_AS_MOISTURE)->ShowWindow(qnt);
	GetDlgItem(IDC_STATIC_6)->ShowWindow(qnt);
	GetDlgItem(IDC_STATIC_7)->ShowWindow(qnt);
	GetDlgItem(IDC_STATIC_8)->ShowWindow(qnt);
	GetDlgItem(IDC_MODELSSEL_LIST)->ShowWindow(qnt);
	GetDlgItem(IDC_MODELS_LIST)->ShowWindow(qnt);
	GetDlgItem(IDC_ADD_2)->ShowWindow(qnt);
	GetDlgItem(IDC_ADD_ALL_2)->ShowWindow(qnt);
	GetDlgItem(IDC_REMOVE_2)->ShowWindow(qnt);
	GetDlgItem(IDC_REMOVE_ALL_2)->ShowWindow(qnt);
	GetDlgItem(IDC_STATIC_15)->ShowWindow(qnt);
	GetDlgItem(IDC_MAIN_NAME)->ShowWindow(qnt);
	GetDlgItem(IDC_STATIC_14)->ShowWindow(qnt);
	GetDlgItem(IDC_USE)->ShowWindow(qnt);
	GetDlgItem(IDC_STANDART_MOISTURE)->ShowWindow(qnt);
	GetDlgItem(IDC_STR_UNIT)->ShowWindow(qnt);

	GetDlgItem(IDC_STATIC_16)->ShowWindow(qlt);
	GetDlgItem(IDC_STATIC_17)->ShowWindow(qlt);
	GetDlgItem(IDC_STATIC_18)->ShowWindow(qlt);
	GetDlgItem(IDC_QLTMODELSSEL_LIST)->ShowWindow(qlt);
	GetDlgItem(IDC_QLTMODELS_LIST)->ShowWindow(qlt);
	GetDlgItem(IDC_ADD_4)->ShowWindow(qlt);
	GetDlgItem(IDC_ADD_ALL_4)->ShowWindow(qlt);
	GetDlgItem(IDC_REMOVE_4)->ShowWindow(qlt);
	GetDlgItem(IDC_REMOVE_ALL_4)->ShowWindow(qlt);
	GetDlgItem(IDC_STATIC_19)->ShowWindow(qlt);
	GetDlgItem(IDC_STATIC_20)->ShowWindow(qlt);
	GetDlgItem(IDC_FORMULA)->ShowWindow(qlt);
	GetDlgItem(IDC_FORMULA_MEAN)->ShowWindow(qlt);
	GetDlgItem(IDC_FORMULA_AND)->ShowWindow(qlt);
	GetDlgItem(IDC_FORMULA_OR)->ShowWindow(qlt);
	GetDlgItem(IDC_FORMULA_BRACKETOPEN)->ShowWindow(qlt);
	GetDlgItem(IDC_FORMULA_BRACKETCLOSE)->ShowWindow(qlt);
	GetDlgItem(IDC_BACK)->ShowWindow(qlt);
	GetDlgItem(IDC_CLEAR)->ShowWindow(qlt);
}

void CCreateMethodDlg::ShowFormula()
{
	if(m_vType != C_ANALYSIS_QLT)
		return;

	int N = int(QltNames.size());

	GetDlgItem(IDC_FORMULA_MEAN)->EnableWindow(N > 0 && Formula.IsNextElementEnable(C_MTD_QLTFORM_MEAN));
	GetDlgItem(IDC_FORMULA_AND)->EnableWindow(N > 0 && Formula.IsNextElementEnable(C_MTD_QLTFORM_AND));
	GetDlgItem(IDC_FORMULA_OR)->EnableWindow(N > 0 && Formula.IsNextElementEnable(C_MTD_QLTFORM_OR));
	GetDlgItem(IDC_FORMULA_BRACKETOPEN)->EnableWindow(N > 0 && Formula.IsNextElementEnable(C_MTD_QLTFORM_OPEN));
	GetDlgItem(IDC_FORMULA_BRACKETCLOSE)->EnableWindow(N > 0 && Formula.IsNextElementEnable(C_MTD_QLTFORM_CLOSE));
	GetDlgItem(IDC_BACK)->EnableWindow(Formula.GetLength() > 0);
	GetDlgItem(IDC_CLEAR)->EnableWindow(Formula.GetLength() > 0);

	GetDlgItem(IDC_STATIC_20)->SetWindowText(Formula.GetCheckFormulaResult(N));
	GetDlgItem(IDC_FORMULA)->SetWindowText(Formula.GetAsString());
}

void CCreateMethodDlg::FillListParamSel(int newsel)
{
	m_listParSel.ResetContent();
	m_Mois.ResetContent();
	m_Mois.AddString(ParcelLoadString(STRID_CREATE_METHOD_MOISNO));

	ItRul it;
	int i, cbsel = 0;
	for(i=1, it=MtdData->Rules.begin(); it!=MtdData->Rules.end(); ++it, i++)
	{
		m_listParSel.AddString((*it).Name);
		m_Mois.AddString((*it).Name);
		if(!MtdData->GetMoisParam().Compare((*it).Name))
			cbsel = i;
	}
	m_vMois = cbsel;
	m_Mois.SetCurSel(m_vMois);

	int sel = (newsel >= 0 && MtdData->Rules.size() > 0) ? newsel : int(MtdData->Rules.size()) - 1;
	m_listParSel.SetCurSel(sel);

	GetDlgItem(IDC_REMOVE)->EnableWindow(sel >= 0);
	GetDlgItem(IDC_REMOVE_ALL)->EnableWindow(sel >= 0);

	CurRule = (sel >= 0 && sel < int(MtdData->Rules.size())) ? MtdData->Rules.begin() + sel : MtdData->Rules.end();

	FillListModelAll();
}

void CCreateMethodDlg::FillListQltModelSel(int newsel)
{
	m_listModQltSel.DeleteAllItems();

	ItStr it;
	int N = int(QltNames.size());
	CString s;
	int i;
	for(i=0, it=QltNames.begin(); it!=QltNames.end(); ++it, i++)
	{
		s.Format(cFmt1, i + 1);
		m_listModQltSel.InsertItem(i, s);
		m_listModQltSel.SetItemText(i, 1, (*it));
	}

	for(i=0; i<2; i++)
		m_listModQltSel.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);

	int sel = (newsel >= 0 && N > 0) ? newsel : 0;
	m_listModQltSel.SetItemState(sel, LVIS_SELECTED, LVIS_SELECTED);

	GetDlgItem(IDC_REMOVE)->EnableWindow(sel >= 0);
	GetDlgItem(IDC_REMOVE_ALL)->EnableWindow(sel >= 0);

	m_listModQltAll.RedrawWindow();

	ShowFormula();
}

void CCreateMethodDlg::FillListModelAll(int newsel)
{
	CurModNames.clear();
	m_listModAll.DeleteAllItems();

	if(CurRule == MtdData->Rules.end())
	{
		FillListModelSel();
		GetDlgItem(IDC_STATIC_6)->SetWindowText(cEmpty);
		OnSetUse();
		return;
	}

	int i, j;
	CString s;

	s.Format(ParcelLoadString(STRID_CREATE_METHOD_PARNAME), CurRule->Name);
	GetDlgItem(IDC_STATIC_6)->SetWindowText(s);

	m_vStdMois = CurRule->Moisture;
	if(m_vMois == 0 || m_listParSel.GetCurSel() == m_vMois - 1)
	{
		m_vUse = false;
		GetDlgItem(IDC_STATIC_14)->EnableWindow(false);
		GetDlgItem(IDC_USE)->EnableWindow(false);
	}
	else
	{
		m_vUse = CurRule->UseStdMoisture;
		GetDlgItem(IDC_STATIC_14)->EnableWindow(true);
		GetDlgItem(IDC_USE)->EnableWindow(true);
	}
	m_Use.SetCheck(m_vUse);
	OnSetUse();

	m_listModAll.InsertColumn(0, cEmpty, LVCFMT_LEFT);
	//m_listModAll.InsertColumn(0, cEmpty, LVCFMT_LEFT, 0, 0);
	//CRect Rect;
	//m_listModAll.GetWindowRect(Rect);
	//m_listModAll.SetColumnWidth(0, Rect.Width()-5);
	m_listModAll.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

	for(i=0, j=0; i<ParentPrj->GetNModels(); i++)
	{
		CModelData* Mod = ParentPrj->GetModel(i);
		if(Mod->GetAnalysisType() == C_ANALYSIS_QNT && Mod->IsModelParamExist(CurRule->Name))
		{
			CurModNames.push_back(Mod->GetName());
			m_listModAll.InsertItem(j, Mod->GetName());
			j++;
		}
	}

	m_listModAll.SetColumnWidth(0, LVSCW_AUTOSIZE);
	CRect Rect;
	m_listModAll.GetWindowRect(Rect);
	int nw = m_listModAll.GetColumnWidth(0);
	if(nw < Rect.Width()-5)
	{
		m_listModAll.SetColumnWidth(0, Rect.Width()-5);
	}

	int sel = (newsel >= 0 && CurModNames.size() > 0) ? newsel : int(CurModNames.size()) - 1;
	m_listModAll.SetItemState(sel, LVIS_SELECTED, LVIS_SELECTED);

	GetDlgItem(IDC_ADD_2)->EnableWindow(sel >= 0);
	GetDlgItem(IDC_ADD_ALL_2)->EnableWindow(sel >= 0);

	FillListModelSel();
}

void CCreateMethodDlg::FillListModelSel(int newsel)
{
	m_listModSel.ResetContent();
	m_DefMod.ResetContent();
	m_DefMod.AddString(ParcelLoadString(STRID_CREATE_METHOD_DEFMODNO));
	m_vDefMod = 0;
	m_DefMod.SetCurSel(m_vDefMod);

	if(CurRule == MtdData->Rules.end())
		return;

	int i, cbsel = 0;
	ItStr it;
	for(i=1, it=CurRule->AddModels.begin(); it!=CurRule->AddModels.end(); ++it, i++)
	{
		m_listModSel.AddString(*it);
		m_DefMod.AddString(*it);
		if(!CurRule->MainModel.Compare(*it))
			cbsel = i;
	}
	m_vDefMod = cbsel;
	m_DefMod.SetCurSel(m_vDefMod);

	int sel = (newsel >= 0 && MtdData->Rules.size() > 0) ? newsel : int(CurRule->AddModels.size()) - 1;
	m_listModSel.SetCurSel(sel);

	GetDlgItem(IDC_REMOVE_2)->EnableWindow(sel >= 0);
	GetDlgItem(IDC_REMOVE_ALL_2)->EnableWindow(sel >= 0);

	m_listModAll.RedrawWindow();
}

bool CCreateMethodDlg::IsAvailableModelExist(CString name)
{
	for(int i=0; i<ParentPrj->GetNModels(); i++)
		if(ParentPrj->GetModel(i)->GetAnalysisType() == C_ANALYSIS_QNT &&
		   ParentPrj->GetModel(i)->IsModelParamExist(name))
			return true;

	return false;
}

bool CCreateMethodDlg::IsChanged()
{
	return true;
}

bool CCreateMethodDlg::IsParamSel(int ind)
{
	CString Name = Names[ind];

	ItRul it;
	for(it=MtdData->Rules.begin(); it!=MtdData->Rules.end(); ++it)
		if(!(*it).Name.Compare(Name))
			return true;

	return false;
}

bool CCreateMethodDlg::IsModelQntSel(int ind)
{
	CString Name = CurModNames[ind];

	ItStr it;
	for(it=CurRule->AddModels.begin(); it!=CurRule->AddModels.end(); ++it)
		if(!(*it).Compare(Name))
			return true;

	return false;
}

bool CCreateMethodDlg::IsModelQltSel(int ind)
{
	CString Name = QltAllNames[ind];

	ItStr it;
	for(it=QltNames.begin(); it!=QltNames.end(); ++it)
		if(!(*it).Compare(Name))
			return true;

	return false;
}

void CCreateMethodDlg::OnHelp()
{
	int IdHelp = DLGID_MAIN;
	switch(Mode)
	{
	case MTDDLG_MODE_CREATE:
		IdHelp = (m_vType == C_ANALYSIS_QLT) ? DLGID_METHOD_CREATE_QLT : DLGID_METHOD_CREATE_QNT;
		break;
	case MTDDLG_MODE_CHANGE:
		IdHelp = (m_vType == C_ANALYSIS_QLT) ? DLGID_METHOD_CHANGE_QLT : DLGID_METHOD_CHANGE_QNT;
		break;
	case MTDDLG_MODE_COPY:
		IdHelp = (m_vType == C_ANALYSIS_QLT) ? DLGID_METHOD_COPY_QLT : DLGID_METHOD_COPY_QNT;
		break;
	}

	pFrame->ShowHelp(IdHelp);
}
