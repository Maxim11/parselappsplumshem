#ifndef _PARCEL_ROOT_DATA_H_
#define _PARCEL_ROOT_DATA_H_

#pragma once

#include "DeviceData.h"
#include "PRDBProject.h"
#include "OptimTemplate.h"


typedef list<CDeviceData> LDev;
typedef list<CDeviceData>::iterator ItDev;

class CRootData
{
public:

	CRootData();
	~CRootData();

protected:

	HTREEITEM hItem;

	CString BaseFileName;
	CString BaseFileDefName;

	CString BaseName;
	CString BaseDefName;

	CString TemplateQntName;
	CString TemplateQltName;

	CString WorkFolder;
	CString BaseFolder;

public:

	CPRDBProject Database;

	COptimTemplate Templates;

	LDev Devices;

	CDeviceData* pCopiedDev;

public:

	HTREEITEM GetHItem();
	void SetHItem(HTREEITEM item);

	CString GetBaseFileName();
	void SetBaseFileName(CString name);
	CString GetBaseName();
	void SetBaseName(CString name);
	CString GetBaseDefFileName();
	CString GetBaseDefName();

	CString GetWorkFolder();
	CString GetBaseFolder();
	void SetBaseFolder(CString name);

	CString GetPalettePath();
	CString GetPaletteName();
	CString GetPaletteFullName();
	CString GetTemplateFolder(int tmpl);
	CString GetTemplateName(int tmpl);
	void SetTemplateName(CString name, int rep);
	CString GetReportPath(int rep);
	CString GetReportPictureFolder(int rep);

	int GetNDevices();
	CDeviceData* GetDevice(HTREEITEM item);
	CDeviceData* GetDevice(CString name);
	CDeviceData* GetDevice(int ind);
	CDeviceData* GetDeviceByNum(int num);
	CDeviceData* GetLastDevice();
	HTREEITEM GetLastDeviceHItem();
	CDeviceData* AddNewDevice();
	bool AddExistingDevice(CString FileName, CDeviceData* CopyDev, CTreeCtrl* pTree, CString PrjName = cEmpty);
	bool ChangeDeviceSettings(HTREEITEM item);
	bool DeleteDevice(HTREEITEM item);
	bool DeleteDevice(int number);

	int GetNProjects();

	bool LoadDatabase(CTreeCtrl* pTree);
	bool CloseDatabase(CTreeCtrl* pTree);
	bool DeleteDatabase(CTreeCtrl* pTree);

	void DeleteBase(CString fname);
	bool CopyBase(CString fname);
	bool CreateSpectraDataDirectory(CString fname);

	void DeleteBaseApjx(CString fname);

};

extern CRootData *pRoot;

#endif
