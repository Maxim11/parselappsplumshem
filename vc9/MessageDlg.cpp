#include "stdafx.h"

#include "MessageDlg.h"


IMPLEMENT_DYNAMIC(CMessageDlg, CDialog)

CMessageDlg::CMessageDlg(int type, CString text, CWnd* pParent) : CDialog(CMessageDlg::IDD, pParent)
{
	Type = type;
	Comment = text;
}

CMessageDlg::~CMessageDlg()
{
}

void CMessageDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CMessageDlg, CDialog)

	ON_BN_CLICKED(IDC_ACCEPT, OnYes)
	ON_BN_CLICKED(IDC_CANCEL, OnNo)
	ON_BN_CLICKED(IDC_CLOSE, OnClose)

END_MESSAGE_MAP()

BOOL CMessageDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	CString s;

	switch(Type)
	{
	case C_MESSAGE_INFO:
		s = ParcelLoadString(STRID_MESSAGE_HDR_INFO);
		GetDlgItem(IDC_ACCEPT)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CANCEL)->ShowWindow(SW_HIDE);
		break;
	case C_MESSAGE_ERROR:
		s = ParcelLoadString(STRID_MESSAGE_HDR_ERROR);
		GetDlgItem(IDC_ACCEPT)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CANCEL)->ShowWindow(SW_HIDE);
		break;
	case C_MESSAGE_CONFIRM_YES:
	case C_MESSAGE_CONFIRM_NO:
		s = ParcelLoadString(STRID_MESSAGE_HDR_CONFIRM);
		GetDlgItem(IDC_CLOSE)->ShowWindow(SW_HIDE);
		break;
	}
	SetWindowText(s);

	GetDlgItem(IDC_ACCEPT)->SetWindowText(ParcelLoadString(STRID_MESSAGE_YES));
	GetDlgItem(IDC_CANCEL)->SetWindowText(ParcelLoadString(STRID_MESSAGE_NO));
	GetDlgItem(IDC_CLOSE)->SetWindowText(ParcelLoadString(STRID_MESSAGE_CLOSE));
	GetDlgItem(IDC_STATIC_1)->SetWindowText(Comment);

	if(Type == C_MESSAGE_CONFIRM_YES)
	{
		GetDlgItem(IDC_ACCEPT)->SetFocus();
		return false;
	}

	return true;
}

void CMessageDlg::OnCancel()
{
	CDialog::OnCancel();
}

void CMessageDlg::OnOK()
{
	if(Type == C_MESSAGE_CONFIRM_YES)
	{
		CDialog::OnOK();
	}
}

void CMessageDlg::OnYes()
{
	CDialog::OnOK();
}

void CMessageDlg::OnNo()
{
	CDialog::OnCancel();
}

void CMessageDlg::OnClose()
{
	CDialog::OnCancel();
}
