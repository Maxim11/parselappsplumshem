#include "stdafx.h"

#include "BranchInfo.h"
#include "OptimModelDlg2.h"
#include "ModelResultDlg.h"
#include "TemplateNameDlg.h"
#include "ShowSpectraDlg.h"
#include "MainFrm.h"

#include <math.h>


static bool noCheck = false;
static int cr_tmpl = C_OPTIM_BYTMPL_NOT;
static int SortSpecBy = C_OPTIM_SORT_SPEC_NAME_2;

static bool iscancel = false;

static COptimModelDlg2* pModOptim = NULL;

bool SortOptimSpectraNamesUp(OptimSpectrum2& OptSp1, OptimSpectrum2& OptSp2)
{
	int TNum1 = pModOptim->GetTransNum(&OptSp1);
	int TNum2 = pModOptim->GetTransNum(&OptSp2);
	if(TNum1 > TNum2)
		return false;
	if(TNum1 < TNum2)
		return true;

    return (OptSp1.Name.Compare(OptSp2.Name) < 0);
}

bool SortOptimSpectraNamesDown(OptimSpectrum2& OptSp1, OptimSpectrum2& OptSp2)
{
	int TNum1 = pModOptim->GetTransNum(&OptSp1);
	int TNum2 = pModOptim->GetTransNum(&OptSp2);
	if(TNum1 > TNum2)
		return false;
	if(TNum1 < TNum2)
		return true;

    return (OptSp1.Name.Compare(OptSp2.Name) > 0);
}

bool SortOptimSpectraUp(OptimSpectrum2& OptSp1, OptimSpectrum2& OptSp2)
{
	if(SortSpecBy == C_OPTIM_SORT_SPEC_NAME_2)
	{
		return SortOptimSpectraNamesUp(OptSp1, OptSp2);
	}
	if(SortSpecBy == C_OPTIM_SORT_SPEC_REF_2)
	{
		if(fabs(OptSp1.Ref - OptSp2.Ref) < 1.e-9)
			return SortOptimSpectraNamesUp(OptSp1, OptSp2);

		return OptSp1.Ref < OptSp2.Ref;
	}

	return false;
}

bool SortOptimSpectraDown(OptimSpectrum2& OptSp1, OptimSpectrum2& OptSp2)
{
	if(SortSpecBy == C_OPTIM_SORT_SPEC_NAME_2)
	{
		return SortOptimSpectraNamesDown(OptSp1, OptSp2);
	}
	if(SortSpecBy == C_OPTIM_SORT_SPEC_REF_2)
	{
		if(fabs(OptSp1.Ref - OptSp2.Ref) < 1.e-9)
			return SortOptimSpectraNamesDown(OptSp1, OptSp2);

		return OptSp1.Ref > OptSp2.Ref;
	}

	return false;
}

static int GetSpecPntColor(int item, int /*subitem*/, void* /*pData*/)
{
	if(pModOptim == NULL)
		return 0;

	COLORREF color = (pModOptim->IsPointExc(item)) ? cColorPass : CLR_DEFAULT;

	pModOptim->m_listAll.CurTxtColor = color;

	return 1;
}

IMPLEMENT_DYNAMIC(COptimModelDlg2, CDialog)

COptimModelDlg2::COptimModelDlg2(CModelData* Mod, Template* tmpl, int mode, CWnd* pParent)
	: CDialog(COptimModelDlg2::IDD, pParent)
{
	pModOptim = this;

	Mode = mode;

	ParentPrj = GetProjectByHItem(Mod->GetParentHItem());
	StartModel = Mod;
	CurTmpl = tmpl;

	PrePrep = StartModel->GetTransPreprocAsStr();

	copy(StartModel->TransNames.begin(), StartModel->TransNames.end(), inserter(TransesNames, TransesNames.begin()));

	Calc.InitCalc(StartModel);

	isTmpl = (Mode == C_OPT_MODE_TMPLCREATE || Mode == C_OPT_MODE_TMPLEDIT);
	byTmpl = (Mode == C_OPT_MODE_BYTMPL || Mode == C_OPT_MODE_TMPLEDIT);

	CTransferData* Trans = ParentPrj->GetTransfer(StartModel->GetTransName());
	MinRange = Trans->GetLowLimSpec();
	MaxRange = Trans->GetUpperLimSpec();

	CreateSpectra();
	CreateSpecPoints();
	SetAvalCrits();

	m_vCalibValid = C_OPTIM_SMPL_CALIB;

	CreateStartStep();
}

COptimModelDlg2::~COptimModelDlg2()
{
}

void COptimModelDlg2::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_TAB_1, m_Tab);
	DDX_Control(pDX, IDC_OPTIM_MODEL_RESULT_LIST, m_listResult);
	DDX_Control(pDX, IDC_SPECTRUM_LIST, m_listSpectra);
	DDX_Control(pDX, IDC_SPECPOINTS_ALL_LIST, m_listAll);
	DDX_Control(pDX, IDC_SPECPOINTS_EXC_LIST, m_listExc);
	DDX_Control(pDX, IDC_MODEL_TYPE, m_ModType);
	DDX_Control(pDX, IDC_SPECTRUM_TYPE, m_SpType);
	DDX_Control(pDX, IDC_OPTIM_TEST, m_Test);
	DDX_Control(pDX, IDC_OPTIM_CRITERION, m_MainCrit);
	DDX_Control(pDX, IDC_OPTIM_NCOMP_MEAN, m_NCompMean);
	DDX_Control(pDX, IDC_OPTIM_NCOMP_PLUS, m_NCompPlus);
	DDX_Control(pDX, IDC_OPTIM_NCOMP_STEP, m_NCompStep);
	DDX_Control(pDX, IDC_OPTIM_HIPER_MEAN, m_HiperMean);
	DDX_Control(pDX, IDC_OPTIM_HIPER_PLUS, m_HiperPlus);
	DDX_Control(pDX, IDC_OPTIM_HIPER_STEP, m_HiperStep);
	DDX_Control(pDX, IDC_OPTIM_NHARM_MEAN, m_NHarmMean);
	DDX_Control(pDX, IDC_OPTIM_NHARM_PLUS, m_NHarmPlus);
	DDX_Control(pDX, IDC_OPTIM_NHARM_STEP, m_NHarmStep);
	DDX_Control(pDX, IDC_OPTIM_NHARM_TYPE, m_NHarmType);
	DDX_Control(pDX, IDC_OPTIM_KFNL_MEAN, m_KfnlMean);
	DDX_Control(pDX, IDC_OPTIM_KFNL_PLUS, m_KfnlPlus);
	DDX_Control(pDX, IDC_OPTIM_KFNL_STEP, m_KfnlStep);
	DDX_Control(pDX, IDC_OPTIM_KFNL_TYPE, m_KfnlType);
	DDX_Control(pDX, IDC_OPTIM_LEFT_MEAN, m_LeftMean);
	DDX_Control(pDX, IDC_OPTIM_LEFT_PLUS, m_LeftPlus);
	DDX_Control(pDX, IDC_OPTIM_LEFT_STEP, m_LeftStep);
	DDX_Control(pDX, IDC_OPTIM_RIGHT_MEAN, m_RightMean);
	DDX_Control(pDX, IDC_OPTIM_RIGHT_PLUS, m_RightPlus);
	DDX_Control(pDX, IDC_OPTIM_RIGHT_STEP, m_RightStep);
	DDX_Control(pDX, IDC_ONOFF_5, m_OperationOnOff);
	DDX_Control(pDX, IDC_PREP_A, m_PrepA);
	DDX_Control(pDX, IDC_PREP_B, m_PrepB);
	DDX_Control(pDX, IDC_PREP_M, m_PrepM);
	DDX_Control(pDX, IDC_PREP_D, m_PrepD);
	DDX_Control(pDX, IDC_PREP_C, m_PrepC);
	DDX_Control(pDX, IDC_PREP_N, m_PrepN);
	DDX_Control(pDX, IDC_PREP_1, m_Prep1);
	DDX_Control(pDX, IDC_PREP_2, m_Prep2);
	DDX_Control(pDX, IDC_PREP_WITHOUT, m_PrepNo);
	DDX_Control(pDX, IDC_ONOFF_2, m_UseBest);
	DDX_Control(pDX, IDC_INPUT_PREP, m_AddPrep);
	DDX_Control(pDX, IDC_NUM_LETTER, m_MaxLetter);
	DDX_Control(pDX, IDC_MAX_BETTER, m_MaxBetter);
	DDX_Control(pDX, IDC_SPIN_LETTER, m_SpinLetter);
	DDX_Control(pDX, IDC_SPIN_LETTER_2, m_SpinLetter2);
	DDX_Control(pDX, IDC_CALIBVALID, m_CalibValid);
	DDX_Control(pDX, IDC_SORT_2, m_SortSpec);
	DDX_Control(pDX, IDC_MAXMODELS, m_MaxMod);
	DDX_Control(pDX, IDC_CRITERION_SEC, m_CritSEC);
	DDX_Control(pDX, IDC_CRITERION_SECV, m_CritSECV);
	DDX_Control(pDX, IDC_CRITERION_SECSECV, m_CritSECSECV);
	DDX_Control(pDX, IDC_CRITERION_SEV, m_CritSEV);
	DDX_Control(pDX, IDC_CRITERION_R2SEC, m_CritR2sec);
	DDX_Control(pDX, IDC_CRITERION_R2SECV, m_CritR2secv);
	DDX_Control(pDX, IDC_CRITERION_F, m_CritF);
	DDX_Control(pDX, IDC_CRITERION_ERR, m_CritErr);
	DDX_Control(pDX, IDC_CRITERION_SDV, m_CritSDV);
	DDX_Control(pDX, IDC_CRITERION_R2SEV, m_CritR2sev);
	DDX_Control(pDX, IDC_CRITERION_K1, m_CritK1);
	DDX_Control(pDX, IDC_CRITERION_K2, m_CritK2);
	DDX_Control(pDX, IDC_CRITERION_K3, m_CritK3);
	DDX_Control(pDX, IDC_CRITERION_K4, m_CritK4);
	DDX_Control(pDX, IDC_CRITERION_K5, m_CritK5);
	DDX_Control(pDX, IDC_CRITERION_K6, m_CritK6);
	DDX_Control(pDX, IDC_CRITERION_K7, m_CritK7);
	DDX_Control(pDX, IDC_SORT, m_Sort);
	DDX_Control(pDX, IDC_OUTS_CHECK_MAH, m_IsMah);
	DDX_Control(pDX, IDC_OUTS_CHECK_SEC, m_IsSEC);
	DDX_Control(pDX, IDC_OUTS_CHECK_SECV, m_IsSECV);
	DDX_Control(pDX, IDC_OUTS_CHECK_BAD, m_IsBad);
	DDX_Control(pDX, IDC_OUTS_CHECK_BOUND, m_IsBound);
	DDX_Control(pDX, IDC_OUTS_MAH, m_Mah);
	DDX_Control(pDX, IDC_OUTS_SEC, m_SEC);
	DDX_Control(pDX, IDC_OUTS_SECV, m_SECV);
	DDX_Control(pDX, IDC_OUTS_BOUND, m_Bound);

	DDX_CBIndex(pDX, IDC_MODEL_TYPE, m_vModType);
	DDX_CBIndex(pDX, IDC_SPECTRUM_TYPE, m_vSpType);
	DDX_CBIndex(pDX, IDC_OPTIM_TEST, m_vTest);
	DDX_CBIndex(pDX, IDC_OPTIM_CRITERION, m_vMainCrit);
	DDX_CBIndex(pDX, IDC_SORT, m_vSort);
	DDX_CBIndex(pDX, IDC_SORT_2, m_vSortSpec);
	DDX_CBIndex(pDX, IDC_OPTIM_NHARM_TYPE, m_vNHarmType);
	DDX_CBIndex(pDX, IDC_OPTIM_KFNL_TYPE, m_vKfnlType);

	cParcelDDX(pDX, IDC_OPTIM_NCOMP_MEAN, m_vNCompMean);
	cParcelDDV(pDX, ParcelLoadString(STRID_OPTIM_MODEL_DLG_DDV_NCOMPMEAN), m_vNCompMean, 1, 100);
	cParcelDDX(pDX, IDC_OPTIM_NCOMP_PLUS, m_vNCompPlus);
	cParcelDDV(pDX, ParcelLoadString(STRID_OPTIM_MODEL_DLG_DDV_NCOMPPLUS), m_vNCompPlus, 0, 100);
	cParcelDDX(pDX, IDC_OPTIM_NCOMP_STEP, m_vNCompStep);
	cParcelDDV(pDX, ParcelLoadString(STRID_OPTIM_MODEL_DLG_DDV_NCOMPSTEP), m_vNCompStep, 0, 2 * 100);

	cParcelDDX(pDX, IDC_OPTIM_HIPER_MEAN, m_vHiperMean);
	cParcelDDV(pDX, ParcelLoadString(STRID_OPTIM_MODEL_DLG_DDV_HIPERMEAN), m_vHiperMean, 0.0001, 1.e9);
	cParcelDDX(pDX, IDC_OPTIM_HIPER_PLUS, m_vHiperPlus);
	cParcelDDV(pDX, ParcelLoadString(STRID_OPTIM_MODEL_DLG_DDV_HIPERPLUS), m_vHiperPlus, 0., 1.e9);
	cParcelDDX(pDX, IDC_OPTIM_HIPER_STEP, m_vHiperStep);
	cParcelDDV(pDX, ParcelLoadString(STRID_OPTIM_MODEL_DLG_DDV_HIPERSTEP), m_vHiperStep, 0., 1.e9);

	bool chk_harm = (m_vModType == C_MOD_MODEL_HSO && m_vNHarmType != C_OPTIM_HQO_PNT);
	cParcelDDX(pDX, IDC_OPTIM_NHARM_MEAN, m_vNHarmMean);
	if(chk_harm)  cParcelDDV(pDX, ParcelLoadString(STRID_OPTIM_MODEL_DLG_DDV_NHARMMEAN), m_vNHarmMean, 3, 60);
	cParcelDDX(pDX, IDC_OPTIM_NHARM_PLUS, m_vNHarmPlus);
	if(chk_harm)  cParcelDDV(pDX, ParcelLoadString(STRID_OPTIM_MODEL_DLG_DDV_NHARMPLUS), m_vNHarmPlus, 0, 60);
	cParcelDDX(pDX, IDC_OPTIM_NHARM_STEP, m_vNHarmStep);
	if(chk_harm)  cParcelDDV(pDX, ParcelLoadString(STRID_OPTIM_MODEL_DLG_DDV_NHARMSTEP), m_vNHarmStep, 0, 60);

	bool chk_kfnl = (m_vModType == C_MOD_MODEL_HSO && m_vKfnlType != C_OPTIM_HQO_LIN);
	cParcelDDX(pDX, IDC_OPTIM_KFNL_MEAN, m_vKfnlMean);
	if(chk_kfnl)  cParcelDDV(pDX, ParcelLoadString(STRID_OPTIM_MODEL_DLG_DDV_KFNLMEAN), m_vKfnlMean, 0.1, 1.);
	cParcelDDX(pDX, IDC_OPTIM_KFNL_PLUS, m_vKfnlPlus);
	if(chk_kfnl)  cParcelDDV(pDX, ParcelLoadString(STRID_OPTIM_MODEL_DLG_DDV_KFNLPLUS), m_vKfnlPlus, 0., 1.);
	cParcelDDX(pDX, IDC_OPTIM_KFNL_STEP, m_vKfnlStep);
	if(chk_kfnl)  cParcelDDV(pDX, ParcelLoadString(STRID_OPTIM_MODEL_DLG_DDV_KFNLSTEP), m_vKfnlStep, 0., 2. * 1.);

	cParcelDDX(pDX, IDC_OPTIM_LEFT_MEAN, m_vLeftMean);
	cParcelDDV(pDX, ParcelLoadString(STRID_OPTIM_MODEL_DLG_DDV_LOWSPECMEAN), m_vLeftMean, MinRange, MaxRange);
	cParcelDDX(pDX, IDC_OPTIM_LEFT_PLUS, m_vLeftPlus);
	cParcelDDV(pDX, ParcelLoadString(STRID_OPTIM_MODEL_DLG_DDV_LOWSPECPLUS), m_vLeftPlus, 0, MaxRange - MinRange);
	cParcelDDX(pDX, IDC_OPTIM_LEFT_STEP, m_vLeftStep);
	cParcelDDV(pDX, ParcelLoadString(STRID_OPTIM_MODEL_DLG_DDV_LOWSPECSTEP), m_vLeftStep, 0, 2 * (MaxRange - MinRange));

	cParcelDDX(pDX, IDC_OPTIM_RIGHT_MEAN, m_vRightMean);
	cParcelDDV(pDX, ParcelLoadString(STRID_OPTIM_MODEL_DLG_DDV_UPPERSPECMEAN), m_vRightMean, MinRange, MaxRange);
	cParcelDDX(pDX, IDC_OPTIM_RIGHT_PLUS, m_vRightPlus);
	cParcelDDV(pDX, ParcelLoadString(STRID_OPTIM_MODEL_DLG_DDV_UPPERSPECPLUS), m_vRightPlus, 0, MaxRange - MinRange);
	cParcelDDX(pDX, IDC_OPTIM_RIGHT_STEP, m_vRightStep);
	cParcelDDV(pDX, ParcelLoadString(STRID_OPTIM_MODEL_DLG_DDV_UPPERSPECSTEP), m_vRightStep, 0, 2 * (MaxRange - MinRange));

	cParcelDDX(pDX, IDC_INPUT_PREP, m_vAddPrep);
	DDV_MaxChars(pDX, m_vAddPrep, 100);
	cParcelDDX(pDX, IDC_NUM_LETTER, m_vMaxLetter);
	cParcelDDV(pDX, ParcelLoadString(STRID_OPTIM_MODEL_DLG_DDV_MAXLETTER), m_vMaxLetter, 1, 7);
	cParcelDDX(pDX, IDC_MAX_BETTER, m_vMaxBetter);
	cParcelDDV(pDX, ParcelLoadString(STRID_OPTIM_MODEL_DLG_DDV_MAXBETTER), m_vMaxBetter, 1, 10);

	cParcelDDX(pDX, IDC_MAXMODELS, m_vMaxMod);

	cParcelDDX(pDX, IDC_OUTS_MAH, m_vMah);
	cParcelDDV(pDX, ParcelLoadString(STRID_OPTIM_MODEL_CRIT_DDV_MAXMAH), m_vMah, 0.001, 1.e9);
	cParcelDDX(pDX, IDC_OUTS_SEC, m_vSEC);
	cParcelDDV(pDX, ParcelLoadString(STRID_OPTIM_MODEL_CRIT_DDV_SEC), m_vSEC, 0.001, 10.);
	cParcelDDX(pDX, IDC_OUTS_SECV, m_vSECV);
	cParcelDDV(pDX, ParcelLoadString(STRID_OPTIM_MODEL_CRIT_DDV_SECV), m_vSECV, 0.001, 10.);
	cParcelDDX(pDX, IDC_OUTS_BOUND, m_vBound);
	cParcelDDV(pDX, ParcelLoadString(STRID_OPTIM_MODEL_CRIT_DDV_BOUND), m_vBound, 0., 50.);

	DDX_Check(pDX, IDC_PREP_A, m_vPrepA);
	DDX_Check(pDX, IDC_PREP_B, m_vPrepB);
	DDX_Check(pDX, IDC_PREP_M, m_vPrepM);
	DDX_Check(pDX, IDC_PREP_D, m_vPrepD);
	DDX_Check(pDX, IDC_PREP_C, m_vPrepC);
	DDX_Check(pDX, IDC_PREP_N, m_vPrepN);
	DDX_Check(pDX, IDC_PREP_1, m_vPrep1);
	DDX_Check(pDX, IDC_PREP_2, m_vPrep2);
	DDX_Check(pDX, IDC_PREP_WITHOUT, m_vPrepNo);
	DDX_Check(pDX, IDC_ONOFF_2, m_vUseBest);

	DDX_Check(pDX, IDC_CRITERION_SEC, m_vCritSEC);
	DDX_Check(pDX, IDC_CRITERION_SECV, m_vCritSECV);
	DDX_Check(pDX, IDC_CRITERION_SECSECV, m_vCritSECSECV);
	DDX_Check(pDX, IDC_CRITERION_SEV, m_vCritSEV);
	DDX_Check(pDX, IDC_CRITERION_R2SEC, m_vCritR2sec);
	DDX_Check(pDX, IDC_CRITERION_R2SECV, m_vCritR2secv);
	DDX_Check(pDX, IDC_CRITERION_F, m_vCritF);
	DDX_Check(pDX, IDC_CRITERION_ERR, m_vCritErr);
	DDX_Check(pDX, IDC_CRITERION_SDV, m_vCritSDV);
	DDX_Check(pDX, IDC_CRITERION_R2SEV, m_vCritR2sev);
	DDX_Check(pDX, IDC_CRITERION_K1, m_vCritK1);
	DDX_Check(pDX, IDC_CRITERION_K2, m_vCritK2);
	DDX_Check(pDX, IDC_CRITERION_K3, m_vCritK3);
	DDX_Check(pDX, IDC_CRITERION_K4, m_vCritK4);
	DDX_Check(pDX, IDC_CRITERION_K5, m_vCritK5);
	DDX_Check(pDX, IDC_CRITERION_K6, m_vCritK6);
	DDX_Check(pDX, IDC_CRITERION_K7, m_vCritK7);

	DDX_Check(pDX, IDC_OUTS_CHECK_MAH, m_vIsMah);
	DDX_Check(pDX, IDC_OUTS_CHECK_SEC, m_vIsSEC);
	DDX_Check(pDX, IDC_OUTS_CHECK_SECV, m_vIsSECV);
	DDX_Check(pDX, IDC_OUTS_CHECK_BAD, m_vIsBad);
	DDX_Check(pDX, IDC_OUTS_CHECK_BOUND, m_vIsBound);

	DDX_Check(pDX, IDC_ONOFF_5, m_vOperationOnOff);
}

BEGIN_MESSAGE_MAP(COptimModelDlg2, CDialog)

	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB_1, OnTabSelchange)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_LETTER, OnSpinLetterDeltaPos)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_LETTER_2, OnSpinLetter2DeltaPos)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_SPECTRUM_LIST, OnSpecListSelChange)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_OPTIM_MODEL_RESULT_LIST, OnResultListSelChange)
	ON_NOTIFY(HDN_ITEMCLICK, 0, OnResultListColumnClick)

	ON_BN_CLICKED(IDC_ACCEPT, OnAcceptResult)
	ON_BN_CLICKED(IDHELP, OnHelp)

	ON_CBN_SELCHANGE(IDC_MODEL_TYPE, OnModelTypeChange)
	ON_CBN_SELCHANGE(IDC_SPECTRUM_TYPE, OnSpTypeChange)
	ON_CBN_SELCHANGE(IDC_OPTIM_TEST, OnTestChange)
	ON_CBN_SELCHANGE(IDC_OPTIM_CRITERION, OnMainCritChange)
	ON_CBN_SELCHANGE(IDC_SORT, OnSortModels)
	ON_CBN_SELCHANGE(IDC_CALIBVALID, OnChangeCalibValid)
	ON_CBN_SELCHANGE(IDC_SORT_2, OnSortSpectra)
	ON_CBN_SELCHANGE(IDC_OPTIM_NHARM_TYPE, OnNHarmTypeChange)
	ON_CBN_SELCHANGE(IDC_OPTIM_KFNL_TYPE, OnKfnlTypeChange)

	ON_BN_CLICKED(IDC_EXCLUDE, OnExclude)
	ON_BN_CLICKED(IDC_ONOFF_5, OnOperationOnOff)
	ON_BN_CLICKED(IDC_CRITERION_SEC, OnSECOnOff)
	ON_BN_CLICKED(IDC_CRITERION_SECV, OnSECVOnOff)
	ON_BN_CLICKED(IDC_CRITERION_SECSECV, OnSECSECVOnOff)
	ON_BN_CLICKED(IDC_CRITERION_SEV, OnSEVOnOff)
	ON_BN_CLICKED(IDC_ADD, OnAdd)
	ON_BN_CLICKED(IDC_ADD_ALL, OnAddAll)
	ON_BN_CLICKED(IDC_REMOVE, OnRemove)
	ON_BN_CLICKED(IDC_REMOVE_ALL, OnRemoveAll)
	ON_BN_CLICKED(IDC_BACK, OnBack)
	ON_BN_CLICKED(IDC_FORWARD, OnForward)
	ON_BN_CLICKED(IDC_CALCULATE, OnCalculate)
	ON_BN_CLICKED(IDC_SETTARGET, OnSetTarget)
	ON_BN_CLICKED(IDC_PREP_A, OnCreatePreprocList)
	ON_BN_CLICKED(IDC_PREP_B, OnCreatePreprocList)
	ON_BN_CLICKED(IDC_PREP_M, OnCreatePreprocList)
	ON_BN_CLICKED(IDC_PREP_D, OnCreatePreprocList)
	ON_BN_CLICKED(IDC_PREP_C, OnCreatePreprocList)
	ON_BN_CLICKED(IDC_PREP_N, OnCreatePreprocList)
	ON_BN_CLICKED(IDC_PREP_1, OnCreatePreprocList)
	ON_BN_CLICKED(IDC_PREP_2, OnCreatePreprocList)
	ON_BN_CLICKED(IDC_PREP_WITHOUT, OnPreprocWithout)
	ON_BN_CLICKED(IDC_ONOFF_2, OnUseBestOnOff)
	ON_BN_CLICKED(IDC_OUTS_CHECK_MAH, OnOutsOnOff)
	ON_BN_CLICKED(IDC_OUTS_CHECK_SEC, OnOutsOnOff)
	ON_BN_CLICKED(IDC_OUTS_CHECK_SECV, OnOutsOnOff)
	ON_BN_CLICKED(IDC_OUTS_CHECK_BAD, OnOutsOnOff)
	ON_BN_CLICKED(IDC_PLOT, OnShowSpectra)

	ON_EN_CHANGE(IDC_NUM_LETTER, OnMaxLetterChange)
	ON_EN_CHANGE(IDC_MAX_BETTER, OnMaxBetterChange)
	ON_EN_CHANGE(IDC_INPUT_PREP, OnAddPrepChange)

	ON_EN_KILLFOCUS(IDC_OPTIM_LEFT_MEAN, OnChangeLeftMean)
	ON_EN_KILLFOCUS(IDC_OPTIM_LEFT_PLUS, OnChangeLeftPlus)
	ON_EN_KILLFOCUS(IDC_OPTIM_RIGHT_MEAN, OnChangeRightMean)
	ON_EN_KILLFOCUS(IDC_OPTIM_RIGHT_PLUS, OnChangeRightPlus)

END_MESSAGE_MAP()

BOOL COptimModelDlg2::OnInitDialog()
{
	CDialog::OnInitDialog();

	iscancel = false;

	int i;
	CString s;

	USES_CONVERSION;

	TC_ITEM TabCtrlItem;
	TabCtrlItem.mask = TCIF_TEXT;
	TabCtrlItem.pszText = (LPWSTR)GETSTRUI(STRID_OPTIM_MODEL_OUTS_HDR);
	m_Tab.InsertItem(0, &TabCtrlItem);
	TabCtrlItem.pszText = (LPWSTR)GETSTRUI(STRID_OPTIM_MODEL_SPEC_HDR);
	m_Tab.InsertItem(1, &TabCtrlItem);
	TabCtrlItem.pszText = (LPWSTR)GETSTRUI(STRID_OPTIM_MODEL_PRE_HDR);
	m_Tab.InsertItem(2, &TabCtrlItem);
	TabCtrlItem.pszText = (LPWSTR)GETSTRUI(STRID_OPTIM_MODEL_SET_HDR);
	m_Tab.InsertItem(3, &TabCtrlItem);
	TabCtrlItem.pszText = (LPWSTR)GETSTRUI(STRID_OPTIM_MODEL_CRIT_HDR);
	m_Tab.InsertItem(4, &TabCtrlItem);

	CreateSortList();

	m_ModType.ResetContent();
	for(i=C_MOD_MODEL_PLS; i<=C_MOD_MODEL_HSO; i++)
	{
		s = cGetModelTypeName(i);
		m_ModType.AddString(s);
	}
	m_ModType.SetCurSel(m_vModType);

	m_SpType.ResetContent();
	for(i=C_MOD_SPECTRUM_TRA; i<=C_MOD_SPECTRUM_BOTH; i++)
	{
		s = cGetSpectrumTypeName(i);
		m_SpType.AddString(s);
	}
	m_SpType.SetCurSel(m_vSpType);

	m_Test.ResetContent();
	for(i=C_OPTIM_TEST_EXCLUDE; i<=C_OPTIM_TEST_OUTS; i++)
	{
		s = cGetOptimOperName(i);
		m_Test.AddString(s);
	}
	m_Test.SetCurSel(m_vTest);

	m_MainCrit.ResetContent();
	ItInt itA;
	for(itA=AvalCrits.begin(); itA!=AvalCrits.end(); ++itA)
		m_MainCrit.AddString(cGetCritName(*itA));
	m_MainCrit.SetCurSel(m_vMainCrit);

	m_listResult.InsertColumn(0, ParcelLoadString(STRID_OPTIM_MODEL_DLG_COLLEFT), LVCFMT_RIGHT, 0, 0);
	m_listResult.InsertColumn(1, ParcelLoadString(STRID_OPTIM_MODEL_DLG_COLRIGHT), LVCFMT_RIGHT, 0, 1);
	m_listResult.InsertColumn(2, ParcelLoadString(STRID_OPTIM_MODEL_DLG_COLPREP), LVCFMT_RIGHT, 0, 2);
	m_listResult.InsertColumn(3, ParcelLoadString(STRID_OPTIM_MODEL_DLG_COLNCOMP), LVCFMT_RIGHT, 0, 3);
	m_listResult.InsertColumn(4, ParcelLoadString(STRID_OPTIM_MODEL_DLG_COLNEXC), LVCFMT_RIGHT, 0, 4);
	m_listResult.InsertColumn(5, ParcelLoadString(STRID_OPTIM_MODEL_DLG_COLMODEL), LVCFMT_RIGHT, 0, 5);
	m_listResult.InsertColumn(6, ParcelLoadString(STRID_OPTIM_MODEL_DLG_COLSPEC), LVCFMT_RIGHT, 0, 6);
	for(i=0; i<7; i++)
	{
		m_listResult.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	}
	m_listResult.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);
	m_listResult.SetIconList();

	s = _T("998");
	m_listAll.InsertColumn(0, s, LVCFMT_RIGHT, 0, 0);
	m_listExc.InsertColumn(0, s, LVCFMT_RIGHT, 0, 0);
	s = _T("999");
	m_listAll.InsertColumn(1, s, LVCFMT_LEFT, 0, 1);
	m_listExc.InsertColumn(1, s, LVCFMT_LEFT, 0, 1);

	for(i=0; i<2; i++)
	{
		m_listAll.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	}
	m_listAll.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

	m_listAll.SetUserCallback(LCF_CB_SI_TXTCOLOR, GetSpecPntColor);
	m_listAll.EnableUserCallback(LCF_CB_SI_TXTCOLOR, true);

	for(i=0; i<2; i++)
		m_listExc.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	m_listExc.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

	m_listSpectra.InsertColumn(0, ParcelLoadString(STRID_OPTIM_MODEL_OUTS_COLNUM), LVCFMT_CENTER, 0, 0);
	m_listSpectra.InsertColumn(1, ParcelLoadString(STRID_OPTIM_MODEL_OUTS_COLSPEC), LVCFMT_CENTER, 0, 1);
	m_listSpectra.InsertColumn(2, StartModel->GetModelParam(0)->ParamName, LVCFMT_RIGHT, 0, 2);
	m_listSpectra.InsertColumn(3, ParcelLoadString(STRID_OPTIM_MODEL_OUTS_COLUSE), LVCFMT_CENTER, 0, 3);
	for(i=0; i<2; i++)
		m_listSpectra.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	m_listSpectra.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

	FillTableSpectra();

	m_SortSpec.ResetContent();
	s = cGetSortFmtUp(ParcelLoadString(STRID_OPTIM_MODEL_OUTS_SORT_NAME));
	m_SortSpec.AddString(s);
	s = cGetSortFmtDown(ParcelLoadString(STRID_OPTIM_MODEL_OUTS_SORT_NAME));
	m_SortSpec.AddString(s);
	s = cGetSortFmtUp(StartModel->GetModelParam(0)->ParamName);
	m_SortSpec.AddString(s);
	s = cGetSortFmtDown(StartModel->GetModelParam(0)->ParamName);
	m_SortSpec.AddString(s);
	m_SortSpec.SetCurSel(m_vSort);

	m_CalibValid.ResetContent();
	s = ParcelLoadString(STRID_OPTIM_MODEL_OUTS_SAMCALIB);
	m_CalibValid.AddString(s);
	s = ParcelLoadString(STRID_OPTIM_MODEL_OUTS_SAMVALID);
	m_CalibValid.AddString(s);
	m_CalibValid.SetCurSel(m_vCalibValid);

	m_NHarmType.ResetContent();
	s = ParcelLoadString(STRID_OPTIM_MODEL_SET_NHARMPNT);
	m_NHarmType.AddString(s);
	s = ParcelLoadString(STRID_OPTIM_MODEL_SET_NHARMHARM);
	m_NHarmType.AddString(s);
	s = ParcelLoadString(STRID_OPTIM_MODEL_SET_NHARMALL);
	m_NHarmType.AddString(s);
	m_NHarmType.SetCurSel(m_vNHarmType);

	m_KfnlType.ResetContent();
	s = ParcelLoadString(STRID_OPTIM_MODEL_SET_KFNLLIN);
	m_KfnlType.AddString(s);
	s = ParcelLoadString(STRID_OPTIM_MODEL_SET_KFNLNLN);
	m_KfnlType.AddString(s);
	s = ParcelLoadString(STRID_OPTIM_MODEL_SET_KFNLALL);
	m_KfnlType.AddString(s);
	m_KfnlType.SetCurSel(m_vKfnlType);

	FillSpecLists();

	CWnd* pWindow = GetDlgItem(IDC_NUM_LETTER);
	if(pWindow != 0)
	{
		m_SpinLetter.SetBuddy(pWindow);
	    m_SpinLetter.SetRange(1, 7);
		m_SpinLetter.SetPos(m_vMaxLetter);
	}

	pWindow = GetDlgItem(IDC_MAX_BETTER);
	if(pWindow != 0)
	{
		m_SpinLetter2.SetBuddy(pWindow);
	    m_SpinLetter2.SetRange(1, 10);
		m_SpinLetter2.SetPos(m_vMaxBetter);
	}

	CString Hdr = cEmpty;
	switch(Mode)
	{
	case C_OPT_MODE_SINGLE:  Hdr = ParcelLoadString(STRID_OPTIM_MODEL_DLG_HDR_SINGLE);  break;
	case C_OPT_MODE_BYTMPL:  Hdr = ParcelLoadString(STRID_OPTIM_MODEL_DLG_HDR_BYTMPL);  break;
	case C_OPT_MODE_TMPLCREATE:  Hdr = ParcelLoadString(STRID_OPTIM_MODEL_DLG_HDR_TMPLCREATE);  break;
	case C_OPT_MODE_TMPLEDIT:  Hdr = ParcelLoadString(STRID_OPTIM_MODEL_DLG_HDR_TMPLEDIT);  break;
	}
	SetWindowText(Hdr);

	GetDlgItem(IDC_ACCEPT)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_DLG_ACCEPT));
	GetDlgItem(IDCANCEL)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_DLG_DECLINE));
	GetDlgItem(IDHELP)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_DLG_HELP));
	GetDlgItem(IDC_ADD)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_SPEC_EXCLUDE));
	GetDlgItem(IDC_ADD_ALL)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_SPEC_ADDALL));
	GetDlgItem(IDC_REMOVE)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_SPEC_INCLUDE));
	GetDlgItem(IDC_REMOVE_ALL)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_SPEC_REMOVEALL));
	GetDlgItem(IDC_BACK)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_DLG_BACK));
	GetDlgItem(IDC_FORWARD)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_DLG_FORWARD));
	GetDlgItem(IDC_CALCULATE)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_DLG_CALCULATE));
	GetDlgItem(IDC_SETTARGET)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_DLG_SET));
	GetDlgItem(IDC_CRITERION_SEC)->SetWindowText(cGetCritName(C_CRIT_SEC));
	GetDlgItem(IDC_CRITERION_SECV)->SetWindowText(cGetCritName(C_CRIT_SECV));
	GetDlgItem(IDC_CRITERION_SECSECV)->SetWindowText(cGetCritName(C_CRIT_SECSECV));
	GetDlgItem(IDC_CRITERION_SEV)->SetWindowText(cGetCritName(C_CRIT_SEV));
	GetDlgItem(IDC_CRITERION_R2SEC)->SetWindowText(cGetCritName(C_CRIT_R2SEC));
	GetDlgItem(IDC_CRITERION_R2SECV)->SetWindowText(cGetCritName(C_CRIT_R2SECV));
	GetDlgItem(IDC_CRITERION_F)->SetWindowText(cGetCritName(C_CRIT_F));
	GetDlgItem(IDC_CRITERION_ERR)->SetWindowText(cGetCritName(C_CRIT_ERR));
	GetDlgItem(IDC_CRITERION_SDV)->SetWindowText(cGetCritName(C_CRIT_SDV));
	GetDlgItem(IDC_CRITERION_R2SEV)->SetWindowText(cGetCritName(C_CRIT_R2SEV));
	GetDlgItem(IDC_CRITERION_K1)->SetWindowText(cGetCritName(C_CRIT_K1));
	GetDlgItem(IDC_CRITERION_K2)->SetWindowText(cGetCritName(C_CRIT_K2));
	GetDlgItem(IDC_CRITERION_K3)->SetWindowText(cGetCritName(C_CRIT_K3));
	GetDlgItem(IDC_CRITERION_K4)->SetWindowText(cGetCritName(C_CRIT_K4));
	GetDlgItem(IDC_CRITERION_K5)->SetWindowText(cGetCritName(C_CRIT_K5));
	GetDlgItem(IDC_CRITERION_K6)->SetWindowText(cGetCritName(C_CRIT_K6));
	GetDlgItem(IDC_CRITERION_K7)->SetWindowText(cGetCritName(C_CRIT_K7));
	GetDlgItem(IDC_STATIC_1)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_SET_MODTYPE));
	GetDlgItem(IDC_STATIC_2)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_SET_GK));
	GetDlgItem(IDC_STATIC_4)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_SET_PLUSMINUS));
	GetDlgItem(IDC_STATIC_5)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_SET_STEP));
	GetDlgItem(IDC_STATIC_6)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_SET_SPTYPE));
	GetDlgItem(IDC_STATIC_7)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_SET_HIPER));
	GetDlgItem(IDC_STATIC_9)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_SET_PLUSMINUS));
	GetDlgItem(IDC_STATIC_10)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_SET_STEP));
	GetDlgItem(IDC_STATIC_11)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_CRIT_OPTIM_TEST));
	GetDlgItem(IDC_STATIC_12)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_SET_NHARM));
	GetDlgItem(IDC_STATIC_13)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_SET_SUBTYPE));
	GetDlgItem(IDC_STATIC_14)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_SET_PLUSMINUS));
	GetDlgItem(IDC_STATIC_15)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_SET_STEP));
	GetDlgItem(IDC_STATIC_16)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_CRIT_CRITS));
	GetDlgItem(IDC_STATIC_17)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_SET_KFNL));
	GetDlgItem(IDC_STATIC_18)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_SET_SUBTYPE));
	GetDlgItem(IDC_STATIC_19)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_SET_PLUSMINUS));
	GetDlgItem(IDC_STATIC_20)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_SET_STEP));
	GetDlgItem(IDC_STATIC_21)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_CRIT_CRITEXC));
	GetDlgItem(IDC_STATIC_22)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_SPEC_LEFT));
	GetDlgItem(IDC_STATIC_23)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_SPEC_RIGHT));
	GetDlgItem(IDC_STATIC_24)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_SPEC_PLUSMINUS));
	GetDlgItem(IDC_STATIC_27)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_SPEC_STEP));
	GetDlgItem(IDC_STATIC_28)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_SPEC_PLUSMINUS));
	GetDlgItem(IDC_STATIC_31)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_SPEC_STEP));
	GetDlgItem(IDC_STATIC_32)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_SPEC_EXCFROMSPEC));
	GetDlgItem(IDC_PREP_A)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_PRE_PREPROC_A));
	GetDlgItem(IDC_PREP_B)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_PRE_PREPROC_B));
	GetDlgItem(IDC_PREP_M)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_PRE_PREPROC_M));
	GetDlgItem(IDC_PREP_D)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_PRE_PREPROC_D));
	GetDlgItem(IDC_PREP_C)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_PRE_PREPROC_C));
	GetDlgItem(IDC_PREP_N)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_PRE_PREPROC_N));
	GetDlgItem(IDC_PREP_1)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_PRE_PREPROC_1));
	GetDlgItem(IDC_PREP_2)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_PRE_PREPROC_2));
	GetDlgItem(IDC_PREP_WITHOUT)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_PRE_PREPROC_NO));
	GetDlgItem(IDC_STATIC_36)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_PRE_ADD));
	GetDlgItem(IDC_STATIC_37)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_PRE_MAX));
	GetDlgItem(IDC_STATIC_40)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_DLG_SORT));
	GetDlgItem(IDC_PLOT)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_DLG_PLOT));
	GetDlgItem(IDC_STATIC_42)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_DLG_SHOW));
	GetDlgItem(IDC_STATIC_43)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_DLG_SORT));
	GetDlgItem(IDC_STATIC_44)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_PRE_BEST));
	GetDlgItem(IDC_STATIC_45)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_CRIT_TAKE));
	GetDlgItem(IDC_STATIC_46)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_CRIT_BEST));
	GetDlgItem(IDC_OUTS_CHECK_MAH)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_CRIT_CHECK_MAH));
	GetDlgItem(IDC_OUTS_CHECK_SEC)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_CRIT_CHECK_SEC));
	GetDlgItem(IDC_OUTS_CHECK_SECV)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_CRIT_CHECK_SECV));
	GetDlgItem(IDC_OUTS_CHECK_BAD)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_CRIT_CHECK_BAD));
	GetDlgItem(IDC_OUTS_CHECK_BOUND)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_CRIT_CHECK_BOUND));
	GetDlgItem(IDC_STATIC_48)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_CRIT_SEC));
	GetDlgItem(IDC_STATIC_49)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_CRIT_SECV));
	GetDlgItem(IDC_STR_UNIT)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_CRIT_DEFUNIT));
	GetDlgItem(IDC_STATIC_47)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_CRIT_OPEREXC));
	GetDlgItem(IDC_STATIC_50)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_CRIT_OPEROUT));
	GetDlgItem(IDC_STATIC_51)->SetWindowText(ParcelLoadString(STRID_OPTIM_MODEL_DLG_OUTRES));

	if(PrePrep.IsEmpty())
		s = ParcelLoadString(STRID_OPTIM_MODEL_PRE_SPEC);
	else
		s.Format(ParcelLoadString(STRID_OPTIM_MODEL_PRE_SPECTRANS), PrePrep);
	GetDlgItem(IDC_STATIC_35)->SetWindowText(s);

	if(byTmpl)  CreateByTemplate();

//	if(Mode == C_OPT_MODE_BYTMPL)
		SetStep(iStep);
//	else
//		SetStep(0);

	byTmpl = false;

	ShowFields(C_OPT_PAGE_OUTS);

	GetDlgItem(IDHELP)->EnableWindow(pFrame->IsExistHelp());

	return true;
}

void COptimModelDlg2::OnAccept()
{
	CDialog::OnOK();
}

void COptimModelDlg2::OnCancel()
{
	iscancel = true;

	CDialog::OnCancel();
}

void COptimModelDlg2::OnAcceptResult()
{
	if(!isTmpl)
	{
		CModelData ModData(StartModel->GetParentHItem());
		ModData.Copy(*StartModel);

		if(!AcceptToModel(&ModData))
			return;

		if(ModData.CalculateModel())
		{
			CModelResultDlg ModResDlg(&ModData, C_MODRESDLG_MODE_EDIT);
			if(ModResDlg.DoModal() == IDOK)
			{
				StartModel->Copy(ModData);
				OnAccept();
			}
		}
	}
	else
	{
		if(!AcceptStep())
			return;

		Template Tmpl;
		Tmpl.Copy(*CurTmpl);

		if(!AcceptToTemplate(&Tmpl))
			return;

		Tmpl.SetPrjSettings(ParentPrj);

		int tmplmode = (Mode == C_OPT_MODE_TMPLCREATE) ? C_TMPL_MODE_CREATE : C_TMPL_MODE_EDIT;
		CTemplateNameDlg TmplNameDlg(&Tmpl, tmplmode);
		if(TmplNameDlg.DoModal() == IDOK)
		{
			CurTmpl->Copy(Tmpl);
			OnAccept();
		}
	}
}

void COptimModelDlg2::OnBack()
{
	SetStep(iStep - 1);
}

void COptimModelDlg2::OnForward()
{
	SetStep(iStep + 1);
}

void COptimModelDlg2::OnCalculate()
{
	if(!AcceptStep())
	{
		if(cr_tmpl == C_OPTIM_BYTMPL_YES)
			cr_tmpl = C_OPTIM_BYTMPL_ERR;
		return;
	}

	OptimStepSettings* CurStep = &OptimSteps2[iStep];

	VStr AllPreps;
	if(!CurStep->IsOperation)
	{
		CString s;
		ItIntInt it;
		for(it=PreprocListAdd.begin(); it!=PreprocListAdd.end(); ++it)
		{
			s = cGetPreprocAsStr(&(*it));
			AllPreps.push_back(s);
		}
		for(it=PreprocList.begin(); it!=PreprocList.end(); ++it)
		{
			s = cGetPreprocAsStr(&(*it));
			ItStr it1 = find(AllPreps.begin(), AllPreps.end(), s);
			if(it1 == AllPreps.end())
			{
				AllPreps.push_back(s);
			}
		}
	}
	else
	{
		if(PreprocListAdd.size() <= 0)
			AllPreps.push_back(cEmpty);
		else
			AllPreps.push_back(cGetPreprocAsStr(&PreprocListAdd[0]));
	}

	OptimResults NewResult(&CurStep->Crits, CurStep->MaxModels);

	CString s;

	int mode = (m_vOperationOnOff) ? ((m_vTest == C_OPTIM_TEST_EXCLUDE) ? C_OPTIM_TEST_EXC_POINTS_2
		: C_OPTIM_TEST_ANALIZE_OUTS_2) : C_OPTIM_CALC_ALL_MODELS_2;
	int NModels = Calc.SetInfo(CurStep, &NewResult, &AllPreps, mode);
	if(NModels <= 0)
	{
		ParcelError(ParcelLoadString(STRID_OPTIM_MODEL_DLG_ERROR_NONUMMODELS));
		return;
	}
	if(NModels > 100000)
	{
		s.Format(ParcelLoadString(STRID_OPTIM_MODEL_DLG_ERROR_MORENUMMODELS), 100000);
		ParcelError(s);
		return;
	}

	int ntmplsteps = (byTmpl) ? int(CurTmpl->Steps.size()) - 1 : 0;

	Calc.CalculateAllModels(byTmpl, iStep + 1, ntmplsteps);

	if(NewResult.NCalcModels <= 0)
	{
		if(!Calc.IsBreak)
			ParcelError(ParcelLoadString(STRID_OPTIM_MODEL_DLG_ERROR_NORESULT));

		if(cr_tmpl == C_OPTIM_BYTMPL_YES)
			cr_tmpl = C_OPTIM_BYTMPL_ERR;

		return;
	}
	if(cr_tmpl == C_OPTIM_BYTMPL_YES && Calc.IsBreak)
		cr_tmpl = C_OPTIM_BYTMPL_ERR;

	if(iStep < nSteps - 1)
	{
		OptimSteps2.erase(OptimSteps2.begin() + (iStep + 1), OptimSteps2.end());
		OptimRes.erase(OptimRes.begin() + (iStep + 1), OptimRes.end());
	}

	OptimRes.push_back(NewResult);
	OptimResults* CurRes = &OptimRes[iStep + 1];

	int cursort = (m_vOperationOnOff && m_vTest == C_OPTIM_TEST_EXCLUDE) ?
		CurStep->MainCrit : GetSortCrit();
	m_vSort = CurRes->FindCritInd(cursort);
	if(m_vSort <= 0)  m_vSort = 0;

	OptimStepSettings NewStep;
	NewStep.Copy(*CurStep);

	if(m_vOperationOnOff && m_vTest == C_OPTIM_TEST_OUTS)
		NewStep.ExcAllBadSpectra(&Calc.BadSpectra);
	NewStep.IsOperation = false;

	OptimSteps2.push_back(NewStep);
	nSteps = int(OptimSteps2.size());
	iStep++;

	SetStep(iStep);

	if(isTmpl || cr_tmpl == C_OPTIM_BYTMPL_YES)
		SetBestModel();
}

void COptimModelDlg2::OnSetTarget()
{
	int Ind = m_listResult.GetNextItem(-1, LVNI_SELECTED);

	SetBestModel(Ind);
}

void COptimModelDlg2::OnExclude()
{
	VOptSpec2* Spectra = (m_vCalibValid == C_OPTIM_SMPL_CALIB) ? &OptimCalib : &OptimValid;

	int N = int(Spectra->size());

	int sel = m_listSpectra.GetNextItem(-1, LVNI_SELECTED);
	if(sel < 0 || sel >= N)
		return;

	OptimSpectrum2* OptSp = &((*Spectra)[sel]);

	OptSp->Use = !OptSp->Use;

	CString s = (OptSp->Use) ? ParcelLoadString(STRID_OPTIM_MODEL_SPEC_EXCLUDE_TXT)
		: ParcelLoadString(STRID_OPTIM_MODEL_SPEC_INCLUDE_TXT);
	GetDlgItem(IDC_EXCLUDE)->SetWindowText(s);

	s = (OptSp->Use) ? ParcelLoadString(STRID_OPTIM_MODEL_SPEC_YES) : ParcelLoadString(STRID_OPTIM_MODEL_SPEC_NO);
	m_listSpectra.SetItemText(sel, 3, s);

	int NN = (m_vCalibValid == C_OPTIM_SMPL_CALIB) ? GetNCalibUse() : GetNValidUse();
	s.Format(ParcelLoadString(STRID_OPTIM_MODEL_OUTS_NUMSPECTRA), N, NN);
	GetDlgItem(IDC_STATIC_41)->SetWindowText(s);
}

void COptimModelDlg2::OnSpecListSelChange(NMHDR* /*pNMHDR*/, LRESULT *pResult)
{
	VOptSpec2* Spectra = (m_vCalibValid == C_OPTIM_SMPL_CALIB) ? &OptimCalib : &OptimValid;

	int N = int(Spectra->size());

	int sel = m_listSpectra.GetNextItem(-1, LVNI_SELECTED);
	if(sel < 0 || sel >= N)
		return;

	OptimSpectrum2* OptSp = &((*Spectra)[sel]);

	CString s = (OptSp->Use) ? ParcelLoadString(STRID_OPTIM_MODEL_SPEC_EXCLUDE_TXT)
		: ParcelLoadString(STRID_OPTIM_MODEL_SPEC_INCLUDE_TXT);
	GetDlgItem(IDC_EXCLUDE)->SetWindowText(s);

	*pResult = 0;
}

void COptimModelDlg2::OnResultListSelChange(NMHDR* /*pNMHDR*/, LRESULT *pResult)
{
	int Ind = m_listResult.GetNextItem(-1, LVNI_SELECTED);

	bool bmod = (CheckStepNum(iStep) && Ind >= 0 && Ind < OptimRes[iStep].GetNModels());
	GetDlgItem(IDC_SETTARGET)->EnableWindow(!isTmpl && bmod);

	*pResult = 0;
}

void COptimModelDlg2::OnResultListColumnClick(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NMLISTVIEW *pLV = (NMLISTVIEW *) pNMHDR;

	int item = pLV->iItem;

	if(!CheckStepNum(iStep))
		return;

	OptimResults* CurRes = &OptimRes[iStep];

	bool findsort = false;
	if(item < CurRes->GetNCrits())
	{
		m_vSort = item;
		findsort = true;
	}

	if(findsort)
	{
		m_Sort.SetCurSel(m_vSort);

		CurRes->Sort(GetSortCrit());
		FillTableRes(false, false);

		if(isTmpl)
			SetBestModel();
		if(m_vUseBest)
			SetBetterPreps();
	}

	*pResult = 0;
}

void COptimModelDlg2::OnTabSelchange(NMHDR* /*pNMHDR*/, LRESULT* pResult)
{
	if(noCheck)
	{
		noCheck = false;
		return;
	}
	if(!UpdateData())
	{
		noCheck = true;
		m_Tab.SetCurSel(m_vTab);
		noCheck = false;
		return;
	}
	m_vTab = m_Tab.GetCurSel();

	ShowFields();

	*pResult = 0;
}

void COptimModelDlg2::OnModelTypeChange()
{
	m_vModType = m_ModType.GetCurSel();

	CString s, s1;
	if(m_vModType == C_MOD_MODEL_HSO)
	{
		m_vNCompPlus = 0;
		m_vNCompStep = 0;
		s.Format(cFmt1, 0);
		m_NCompPlus.SetWindowText(s);
		m_NCompStep.SetWindowText(s);
	}
	else
	{
		m_vHiperPlus = 0.;
		m_vHiperStep = 0.;
		m_vNHarmPlus = 0;
		m_vNHarmStep = 0;
		m_vKfnlPlus = 0.;
		m_vKfnlStep = 0.;
		s.Format(cFmt1, 0);
		s1.Format(cFmt31, 0.);
		m_HiperPlus.SetWindowText(s1);
		m_HiperStep.SetWindowText(s1);
		m_NHarmPlus.SetWindowText(s);
		m_NHarmStep.SetWindowText(s);
		m_KfnlPlus.SetWindowText(s1);
		m_KfnlStep.SetWindowText(s1);

		m_vHiperMean = 0.1;
		s1.Format(cFmt31, m_vHiperMean);
		m_HiperMean.SetWindowText(s1);

		m_vNHarmMean = (m_vNHarmType == C_OPTIM_HQO_PNT) ? 0 : 3;
		s.Format(cFmt1, m_vNHarmMean);
		m_NHarmMean.SetWindowText(s);

		m_vKfnlMean = (m_vKfnlType == C_OPTIM_HQO_LIN) ? 0. : 0.1;
		s1.Format(cFmt31, m_vKfnlMean);
		m_KfnlMean.SetWindowText(s1);
	}

	EnableFields();
}

void COptimModelDlg2::OnSpTypeChange()
{
	m_vSpType = m_SpType.GetCurSel();
}

void COptimModelDlg2::OnTestChange()
{
	m_vTest = m_Test.GetCurSel();

	int show_exc = (m_vTab == C_OPT_PAGE_CRIT && m_vTest == C_OPTIM_TEST_EXCLUDE) ? SW_SHOW : SW_HIDE;
	int show_out = (m_vTab == C_OPT_PAGE_CRIT && m_vTest == C_OPTIM_TEST_OUTS) ? SW_SHOW : SW_HIDE;

	GetDlgItem(IDC_STATIC_47)->ShowWindow(show_exc);
	GetDlgItem(IDC_STATIC_21)->ShowWindow(show_exc);
	GetDlgItem(IDC_OPTIM_CRITERION)->ShowWindow(show_exc);
	GetDlgItem(IDC_STATIC_50)->ShowWindow(show_out);
	GetDlgItem(IDC_OUTS_CHECK_MAH)->ShowWindow(show_out);
	GetDlgItem(IDC_OUTS_CHECK_SEC)->ShowWindow(show_out);
	GetDlgItem(IDC_OUTS_CHECK_SECV)->ShowWindow(show_out);
	GetDlgItem(IDC_OUTS_CHECK_BAD)->ShowWindow(show_out);
	GetDlgItem(IDC_OUTS_CHECK_BOUND)->ShowWindow(show_out);
	GetDlgItem(IDC_OUTS_MAH)->ShowWindow(show_out);
	GetDlgItem(IDC_OUTS_SEC)->ShowWindow(show_out);
	GetDlgItem(IDC_OUTS_SECV)->ShowWindow(show_out);
	GetDlgItem(IDC_OUTS_BOUND)->ShowWindow(show_out);
	GetDlgItem(IDC_STATIC_48)->ShowWindow(show_out);
	GetDlgItem(IDC_STATIC_49)->ShowWindow(show_out);
	GetDlgItem(IDC_STR_UNIT)->ShowWindow(show_out);

	EnableFields();
}

void COptimModelDlg2::OnMainCritChange()
{
	m_vMainCrit = m_MainCrit.GetCurSel();
	if(m_vOperationOnOff && m_vTest == C_OPTIM_TEST_EXCLUDE)
	{
		SetCritFields(GetMainCrit());
		UpdateData(0);
	}

	EnableFields();
}

void COptimModelDlg2::OnOperationOnOff()
{
	if(!UpdateData())
		return;

	if(m_vOperationOnOff)
	{
		if(m_vTest == C_OPTIM_TEST_EXCLUDE)
			SetCritFields(GetMainCrit());
/*
		if(m_vSpType == C_MOD_SPECTRUM_BOTH)
			m_vSpType = C_MOD_SPECTRUM_ABS;
*/
		UpdateData(0);
	}

	EnableFields();
}

void COptimModelDlg2::OnSECOnOff()
{
	if(!UpdateData())
		return;

	if(!m_vCritSEC)
	{
		UnsetCritFields(C_CRIT_SEC);
		UpdateData(0);
	}

	EnableFields();
}

void COptimModelDlg2::OnSECVOnOff()
{
	if(!UpdateData())
		return;

	if(!m_vCritSECV)
	{
		UnsetCritFields(C_CRIT_SECV);
		UpdateData(0);
	}

	EnableFields();
}

void COptimModelDlg2::OnSECSECVOnOff()
{
	if(!UpdateData())
		return;

	if(!m_vCritSECSECV)
	{
		UnsetCritFields(C_CRIT_SECSECV);
		UpdateData(0);
	}

	EnableFields();
}

void COptimModelDlg2::OnSEVOnOff()
{
	if(!UpdateData())
		return;

	if(!m_vCritSEV)
	{
		UnsetCritFields(C_CRIT_SEV);
		UpdateData(0);
	}

	EnableFields();
}

void COptimModelDlg2::OnOutsOnOff()
{
	if(!UpdateData())
		return;

	if(!m_vIsMah && !m_vIsSEC && !m_vIsSECV && !m_vIsBad)
	{
		m_vIsBound = false;
		m_IsBound.SetCheck(false);
		UpdateData(0);
	}

	EnableFields();
}

void COptimModelDlg2::OnUseBestOnOff()
{
	if(!UpdateData())
		return;

	if(m_vUseBest)
		SetBetterPreps();

	EnableFields();
}

void COptimModelDlg2::OnNHarmTypeChange()
{
	m_vNHarmType = m_NHarmType.GetCurSel();

	CString s;
	if(m_vNHarmType == C_OPTIM_HQO_PNT)
	{
		m_vNHarmMean = 0;
		m_vNHarmPlus = 0;
		m_vNHarmStep = 0;

		s.Format(cFmt1, 0);
		m_NHarmMean.SetWindowText(s);
		m_NHarmPlus.SetWindowText(s);
		m_NHarmStep.SetWindowText(s);
	}
	else if(m_vNHarmMean == 0)
	{
		m_vNHarmMean = 3;

		s.Format(cFmt1, 3);
		m_NHarmMean.SetWindowText(s);
	}

	EnableFields();
}

void COptimModelDlg2::OnKfnlTypeChange()
{
	m_vKfnlType = m_KfnlType.GetCurSel();

	CString s;
	if(m_vKfnlType == C_OPTIM_HQO_LIN)
	{
		m_vKfnlMean = 0.;
		m_vKfnlPlus = 0.;
		m_vKfnlStep = 0.;

		s.Format(cFmt31, 0.);
		m_KfnlMean.SetWindowText(s);
		m_KfnlPlus.SetWindowText(s);
		m_KfnlStep.SetWindowText(s);
	}
	else if(m_vKfnlMean < 1.e-9)
	{
		m_vKfnlMean = 0.1;

		s.Format(cFmt31, 0.1);
		m_KfnlMean.SetWindowText(s);
	}

	EnableFields();
}

void COptimModelDlg2::OnAdd()
{
	if(!UpdateData())
		return;

	int low = max(MinRange, m_vLeftMean - m_vLeftPlus);
	int upper = min(MaxRange, m_vRightMean + m_vRightPlus);

	VDbl Points;
	ParentPrj->ExtractSpecPoints(low, upper, Points);
	int N = int(Points.size());
	int indS = ParentPrj->GetSpecPointInd(Points[0]);

	int Ind = -1, nsel = m_listAll.GetSelectedCount();
	if(nsel <= 0)  return;

	for(int i=0; i<nsel; i++)
	{
		Ind = m_listAll.GetNextItem(Ind, LVNI_SELECTED);
		if(Ind < 0 || Ind >= N)
			return;

		PointsExc.insert(Ind + indS);
	}

	FillListExc(0);

	UpdateData(0);
}

void COptimModelDlg2::OnAddAll()
{
	if(!UpdateData())
		return;

	int low = max(MinRange, m_vLeftMean - m_vLeftPlus);
	int upper = min(MaxRange, m_vRightMean + m_vRightPlus);

	VDbl Points;
	ParentPrj->ExtractSpecPoints(low, upper, Points);
	int N = int(Points.size());
	int indS = ParentPrj->GetSpecPointInd(Points[0]);

	for(int i=0; i<N; i++)
		PointsExc.insert(i + indS);

	FillListExc(0);

	UpdateData(0);
}

void COptimModelDlg2::OnRemove()
{
	if(!UpdateData())
		return;

	int low = max(MinRange, m_vLeftMean - m_vLeftPlus);
	int upper = min(MaxRange, m_vRightMean + m_vRightPlus);

	VDbl Points;
	ParentPrj->ExtractSpecPoints(low, upper, Points);
	int N = int(Points.size());
	int indS = ParentPrj->GetSpecPointInd(Points[0]);
	int indF = ParentPrj->GetSpecPointInd(Points[N-1]);

	int i, j, Ind = -1, nsel = m_listExc.GetSelectedCount();
	if(nsel <= 0)  return;

	int NN = int(PointsExc.size());
	VInt sel, mean;
	for(i=0; i<nsel; i++)
	{
		Ind = m_listExc.GetNextItem(Ind, LVNI_SELECTED);
		if(Ind < 0 || Ind >= NN)
			return;

		sel.push_back(Ind);
	}

	ItSInt it;
	for(i=0, j=0, it=PointsExc.begin(); j<nsel && it!=PointsExc.end(); ++it)
	{
		if((*it) < indS)  continue;
		if((*it) > indF)  break;

		if(sel[j] == i)
		{
			mean.push_back(*it);
			j++;
		}
		i++;
	}

	ItInt it2;
	for(it2=mean.begin(); it2<mean.end(); ++it2)
		PointsExc.erase(*it2);

	FillListExc(0);
	UpdateData(0);
}

void COptimModelDlg2::OnRemoveAll()
{
	if(!UpdateData())
		return;

	int low = max(MinRange, m_vLeftMean - m_vLeftPlus);
	int upper = min(MaxRange, m_vRightMean + m_vRightPlus);

	VDbl Points;
	ParentPrj->ExtractSpecPoints(low, upper, Points);
	int N = int(Points.size());
	int indS = ParentPrj->GetSpecPointInd(Points[0]);
	int indF = ParentPrj->GetSpecPointInd(Points[N-1]);

	VInt mean;
	ItSInt it;
	for(it=PointsExc.begin(); it!=PointsExc.end(); ++it)
	{
		if((*it) < indS)  continue;
		if((*it) > indF)  break;

		mean.push_back(*it);
	}

	ItInt it2;
	for(it2=mean.begin(); it2<mean.end(); ++it2)
		PointsExc.erase(*it2);

	FillListExc(-1);

	UpdateData(0);
}

void COptimModelDlg2::OnMaxLetterChange()
{
	if(!UpdateData())
		return;

	CreatePreprocListAll();
}

void COptimModelDlg2::OnMaxBetterChange()
{
	if(!UpdateData())
		return;

	SetBetterPreps();
}

void COptimModelDlg2::OnAddPrepChange()
{
	if(!UpdateData())
		return;

	CreatePreprocListAll();
}

void COptimModelDlg2::OnCreatePreprocList()
{
	if(!UpdateData())
		return;

	CreatePreprocListAll();
}

void COptimModelDlg2::OnPreprocWithout()
{
	if(!UpdateData())
		return;

	CreatePreprocListAll();
}

void COptimModelDlg2::OnSortModels()
{
	if(!UpdateData())
		return;

	if(!CheckStepNum(iStep))
		return;

	OptimResults* CurRes = &OptimRes[iStep];

	CurRes->Sort(GetSortCrit());
	FillTableRes(false, false);

	if(isTmpl)
		SetBestModel();
	if(m_vUseBest)
		SetBetterPreps();
}

void COptimModelDlg2::OnChangeCalibValid()
{
	m_vCalibValid = m_CalibValid.GetCurSel();

	FillTableSpectra();
}

void COptimModelDlg2::OnSortSpectra()
{
	if(!UpdateData())
		return;

	SortSpectra();

	FillTableSpectra();
}

void COptimModelDlg2::OnChangeLeftMean()
{
	if(iscancel)
		return;
	if(GetFocus() == GetDlgItem(IDCANCEL))
	{
		OnCancel();
		return;
	}

	int old = m_vLeftMean;

	if(!UpdateData())
		return;

	if(m_vLeftMean == old)
		return;

	if(m_vLeftMean - m_vLeftPlus >= m_vRightMean + m_vRightPlus)
	{
		ParcelError(ParcelLoadString(STRID_OPTIM_MODEL_DLG_ERROR_BADLEFTMEAN));
		m_vLeftMean = min(MaxRange, m_vRightMean + m_vRightPlus + m_vLeftPlus);
	}

	FillSpecLists();

	UpdateData(0);
}

void COptimModelDlg2::OnChangeLeftPlus()
{
	if(iscancel)
		return;
	if(GetFocus() == GetDlgItem(IDCANCEL))
	{
		OnCancel();
		return;
	}

	int old = m_vLeftPlus;

	if(!UpdateData())
		return;

	if(m_vLeftPlus == old)
		return;

	if(m_vLeftMean - m_vLeftPlus >= m_vRightMean + m_vRightPlus)
	{
		ParcelError(ParcelLoadString(STRID_OPTIM_MODEL_DLG_ERROR_BADLEFTPLUS));
		m_vLeftPlus = max(0, m_vLeftMean - m_vRightMean - m_vRightPlus);
	}

	FillSpecLists();

	UpdateData(0);
}

void COptimModelDlg2::OnChangeRightMean()
{
	if(iscancel)
		return;
	if(GetFocus() == GetDlgItem(IDCANCEL))
	{
		OnCancel();
		return;
	}

	int old = m_vRightMean;

	if(!UpdateData())
		return;

	if(m_vRightMean == old)
		return;

	if(m_vLeftMean - m_vLeftPlus >= m_vRightMean + m_vRightPlus)
	{
		ParcelError(ParcelLoadString(STRID_OPTIM_MODEL_DLG_ERROR_BADRIGHTMEAN));
		m_vRightMean = max(MinRange, m_vLeftMean - m_vLeftPlus - m_vRightPlus);
	}

	FillSpecLists();

	UpdateData(0);
}

void COptimModelDlg2::OnChangeRightPlus()
{
	if(iscancel)
		return;
	if(GetFocus() == GetDlgItem(IDCANCEL))
	{
		OnCancel();
		return;
	}

	int old = m_vRightPlus;

	if(!UpdateData())
		return;

	if(m_vRightPlus == old)
		return;

	if(m_vLeftMean - m_vLeftPlus >= m_vRightMean + m_vRightPlus)
	{
		ParcelError(ParcelLoadString(STRID_OPTIM_MODEL_DLG_ERROR_BADRIGHTPLUS));
		m_vRightPlus = max(0, m_vLeftMean - m_vLeftPlus - m_vRightMean);
	}

	FillSpecLists();

	UpdateData(0);
}

void COptimModelDlg2::OnShowSpectra()
{
	CTransferData* pTrans = ParentPrj->GetTransOwn();

	CShowSpectra ShowSp(&pTrans->SpecPoints);

	int i, j;

	bool isCalib, isValid;
	bool issel;

	ItOptSpec2 it, it2;
	for(i=0, it=OptimCalib.begin(); it!=OptimCalib.end(); ++it, i++)
	{
		CTransferData* pTr = ParentPrj->GetTransfer(it->TransName);
		if(pTr == NULL || pTr->GetType() == C_TRANS_TYPE_NORMAL)
			continue;
		CSampleData* Sam = pTr->GetSample(it->SampleName);
		if(Sam == NULL)
			continue;
		Spectrum* Spec = Sam->GetSpectrumByNum(it->Num);
		if(Spec == NULL)
			continue;

		issel = false;
		isCalib = true;
		isValid = false;
		for(j=0, it2=OptimValid.begin(); it2!=OptimValid.end(); ++it2, j++)
			if(!it2->Name.Compare(it->Name) && it2->Num == it->Num)
			{
				isValid = true;
				break;
			}
		if(m_vCalibValid == C_OPTIM_SMPL_CALIB)
			issel = (m_listSpectra.GetItemState(i, LVIS_SELECTED) == LVIS_SELECTED);
		else if(isValid)
			issel = (m_listSpectra.GetItemState(j, LVIS_SELECTED) == LVIS_SELECTED);

		int indT = GetTransNum(&(*it));

		int type = C_SPECTRA_TYPE_SIMPLE;
		if(isCalib && isValid)  type = C_SPECTRA_TYPE_MODEL;
		if(isCalib && !isValid)  type = C_SPECTRA_TYPE_CALIB;
		if(!isCalib && isValid)  type = C_SPECTRA_TYPE_VALID;

		SpectraShowSettings* NewSS = ShowSp.AddSpectraShow(it->SampleName, it->Num, indT + 1, 0, issel, type);
		if(NewSS == NULL)
			continue;

		copy(Spec->Data.begin(), Spec->Data.end(), inserter(NewSS->SpecData, NewSS->SpecData.begin()));
	}
	for(i=0, it=OptimValid.begin(); it!=OptimValid.end(); ++it, i++)
	{
		CTransferData* pTr = ParentPrj->GetTransfer(it->TransName);
		if(pTr == NULL || pTr->GetType() == C_TRANS_TYPE_NORMAL)
			continue;
		CSampleData* Sam = pTr->GetSample(it->SampleName);
		if(Sam == NULL)
			continue;
		Spectrum* Spec = Sam->GetSpectrumByNum(it->Num);
		if(Spec == NULL)
			continue;

		issel = false;
		isCalib = false;
		isValid = true;
		for(it2=OptimCalib.begin(); it2!=OptimCalib.end(); ++it2)
			if(!it2->Name.Compare(it->Name) && it2->Num == it->Num)
				break;
		if(it2 != OptimCalib.end())
			continue;

		if(m_vCalibValid == C_OPTIM_SMPL_VALID)
			issel = (m_listSpectra.GetItemState(i, LVIS_SELECTED) == LVIS_SELECTED);

		int indT = GetTransNum(&(*it));

		int type = C_SPECTRA_TYPE_VALID;

		SpectraShowSettings* NewSS = ShowSp.AddSpectraShow(it->SampleName, it->Num, indT + 1, 0, issel, type);
		if(NewSS == NULL)
			continue;

		copy(Spec->Data.begin(), Spec->Data.end(), inserter(NewSS->SpecData, NewSS->SpecData.begin()));
	}

	CShowSpectraDlg dlg(&ShowSp, C_PLOT_SET_MODE_MODEL, this);
	dlg.DoModal();
}

void COptimModelDlg2::OnSpinLetterDeltaPos(NMHDR *pNMHDR, LRESULT *pResult)
{
   if(pNMHDR == 0 || pResult == 0) return;

   NMUPDOWN *pNMUpDown = (NMUPDOWN *) pNMHDR;

   pNMUpDown->iDelta *= 1;
}

void COptimModelDlg2::OnSpinLetter2DeltaPos(NMHDR *pNMHDR, LRESULT *pResult)
{
   if(pNMHDR == 0 || pResult == 0) return;

   NMUPDOWN *pNMUpDown = (NMUPDOWN *) pNMHDR;

   pNMUpDown->iDelta *= 1;
}

void COptimModelDlg2::CreateSpectra()
{
	CString ParamName = StartModel->GetModelParam(0)->ParamName;

	int i;
	OptimCalib.clear();
	for(i=0; i<StartModel->GetNSamplesForCalib(); i++)
	{
		ModelSample* ModSam = StartModel->GetSampleForCalib(i);
		CTransferData* Trans = ParentPrj->GetTransfer(ModSam->TransName);
		CSampleData* Sam = Trans->GetSample(ModSam->SampleName);

		int indT = GetTransNum(ModSam);

		ItIntBool it2;
		for(it2=ModSam->Spectra.begin(); it2!=ModSam->Spectra.end(); ++it2)
		{
			OptimSpectrum2 NewOptSpec(ModSam->SampleName, ModSam->TransName, it2->first,
				Sam->GetReferenceData(ParamName)->Value, it2->second, indT + 1);
			OptimCalib.push_back(NewOptSpec);
		}
	}
	OptimValid.clear();
	for(i=0; i<StartModel->GetNSamplesForValid(); i++)
	{
		ModelSample* ModSam = StartModel->GetSampleForValid(i);
		CTransferData* Trans = ParentPrj->GetTransfer(ModSam->TransName);
		CSampleData* Sam = Trans->GetSample(ModSam->SampleName);

		int indT = GetTransNum(ModSam);

		ItIntBool it2;
		for(it2=ModSam->Spectra.begin(); it2!=ModSam->Spectra.end(); ++it2)
		{
			OptimSpectrum2 NewOptSpec(ModSam->SampleName, ModSam->TransName, it2->first,
				Sam->GetReferenceData(ParamName)->Value, it2->second, indT + 1);
			OptimValid.push_back(NewOptSpec);
		}
	}
}

void COptimModelDlg2::CreateSpecPoints()
{
	ParentPrj->ExtractSpecPoints(MinRange, MaxRange, PointsAll);
	NAllPoints = int(PointsAll.size());
}

void COptimModelDlg2::SetAvalCrits()
{
	AvalCrits.clear();
	for(int i=C_CRIT_SEC; i<=C_CRIT_K7; i++)
		if(IsCritAval(i))
			AvalCrits.push_back(i);
}

void COptimModelDlg2::CreateStartStep()
{
	OptimStepSettings NewStep;

	NewStep.ModelType = StartModel->GetModelType();
	NewStep.SpectrumType = StartModel->GetSpectrumType();

	NewStep.NCompMean = (byTmpl) ? CurTmpl->NCompMean : StartModel->GetNCompQnt();
	NewStep.NCompPlus = 0;
	NewStep.NCompStep = 0;
	NewStep.HiperMean = (byTmpl) ? CurTmpl->HiperMean : StartModel->GetDimHiper();
	NewStep.HiperPlus = 0;
	NewStep.HiperStep = 0.;
	NewStep.NHarmMean = (byTmpl) ? CurTmpl->NHarmMean : StartModel->GetNHarm();
	NewStep.NHarmPlus = 0;
	NewStep.NHarmStep = 0;
	NewStep.NHarmType = (NewStep.NHarmMean > 0) ? C_OPTIM_HQO_HARM : C_OPTIM_HQO_PNT;
	NewStep.KfnlMean = (byTmpl) ? CurTmpl->KfnlMean : StartModel->GetKfnl();
	NewStep.KfnlPlus = 0;
	NewStep.KfnlStep = 0.;
	NewStep.KfnlType = (NewStep.KfnlMean > 0.) ? C_OPTIM_HQO_NLN : C_OPTIM_HQO_LIN;

	NewStep.LeftMean = (byTmpl) ? CurTmpl->SpecLeftMean : StartModel->GetLowSpecRange();
	NewStep.LeftPlus = 0;
	NewStep.LeftStep = 0;
	NewStep.RightMean = (byTmpl) ? CurTmpl->SpecRightMean : StartModel->GetUpperSpecRange();
	NewStep.RightPlus = 0;
	NewStep.RightStep = 0;

	NewStep.IsOperation = false;
	NewStep.Operation = C_OPTIM_TEST_EXCLUDE;
	NewStep.MainCrit = C_CRIT_SEC;
	NewStep.SortCrit = C_CRIT_SEC;
	NewStep.MaxModels = 30;

	if(byTmpl)
		CurTmpl->GetExcPoints(NewStep.ExcPoints);
	else
		StartModel->GetExcPoints(NewStep.ExcPoints);

	GetCalibValid(NewStep.Calib, NewStep.Valid);

	NewStep.Preps.Adds.clear();
	if(StartModel->GetOwnPreprocAsStr().IsEmpty())
		NewStep.Preps.pNo = true;
	else
	{
		NewStep.Preps.pNo = false;
		NewStep.Preps.Adds.push_back(StartModel->GetOwnPreprocAsStr());
	}

	OptimSteps2.push_back(NewStep);

	OptimCritSet Crits;
	Crits.Empty();
	OptimResults NewResult(&Crits, NewStep.MaxModels);
	OptimRes.push_back(NewResult);

	nSteps = 1;
	iStep = 0;

	SetStep(0, false);
}

void COptimModelDlg2::CreateByTemplate()
{
	cr_tmpl = C_OPTIM_BYTMPL_YES;

	ItVStep itT, itNext;
	for(itT=CurTmpl->Steps.begin(); itT!=CurTmpl->Steps.end(); ++itT)
	{
		itNext = itT + 1;

		if(itT!=CurTmpl->Steps.begin())
			OnCalculate();

		if(cr_tmpl == C_OPTIM_BYTMPL_ERR)
			break;

		m_vModType = itT->ModelType;
		m_vSpType = itT->SpType;

		m_vNHarmType = itT->NHarmType;
		m_vKfnlType = itT->KfnlType;
		m_vNCompPlus = itT->NCompPlus;
		m_vNCompStep = itT->NCompStep;
		m_vHiperPlus = itT->HiperPlus;
		m_vHiperStep = itT->HiperStep;
		m_vNHarmPlus = itT->NHarmPlus;
		m_vNHarmStep = itT->NHarmStep;
		m_vKfnlPlus = itT->KfnlPlus;
		m_vKfnlStep = itT->KfnlStep;

		m_vLeftPlus = itT->SpecLeftPlus;
		m_vLeftStep = itT->SpecLeftStep;
		m_vRightPlus = itT->SpecRightPlus;
		m_vRightStep = itT->SpecRightStep;

		m_vOperationOnOff = itT->IsTest;
		m_vTest = itT->Operation;
		m_vMainCrit = itT->MainCrit;
		m_vMaxMod = itT->MaxModels;

		m_vMah = itT->outs.limMah;
		m_vSEC =itT->outs.limSEC;
		m_vSECV = itT->outs.limSECV;
		m_vBound = itT->outs.limBound;
		m_vIsMah = itT->outs.chMah;
		m_vIsSEC = itT->outs.chSEC;
		m_vIsSECV = itT->outs.chSECV;
		m_vIsBad = itT->outs.chBad;
		m_vIsBound = itT->outs.chBound;

		ClearCritFields();
		if(m_vOperationOnOff && m_vTest == C_OPTIM_TEST_EXCLUDE)
			SetCritFields(itT->MainCrit);
		else
		{
			if(itNext != CurTmpl->Steps.end())
				SetCritFields(itNext->SortCrit);
			else
				SetCritFields(CurTmpl->LastCrit);
		}

		OptimResults* CurRes = &OptimRes[iStep];
		m_vSort = CurRes->FindCritInd(itT->SortCrit);

		m_vPrepA = itT->prepsets.pA;
		m_vPrepB = itT->prepsets.pB;
		m_vPrepM = itT->prepsets.pM;
		m_vPrepD = itT->prepsets.pD;
		m_vPrepC = itT->prepsets.pC;
		m_vPrepN = itT->prepsets.pN;
		m_vPrep1 = itT->prepsets.p1;
		m_vPrep2 = itT->prepsets.p2;
		m_vPrepNo = itT->prepsets.pNo;
		m_vUseBest = itT->prepsets.UseBest;
		m_vMaxLetter = itT->prepsets.MaxLen;
		m_vMaxBetter = itT->prepsets.NumBest;
		m_vAddPrep = itT->prepsets.GetAddsAsStr();

		CreatePreprocListAll(false);

		UpdateData(0);
	}

	AcceptStep();

	cr_tmpl = C_OPTIM_BYTMPL_NOT;
}

bool COptimModelDlg2::CheckStepNum(int istep)
{
	return(istep >= 0 && istep < nSteps);
}

bool COptimModelDlg2::SetStep(int istep, bool needUpd)
{
	if(!CheckStepNum(istep))
		return false;

	iStep = istep;

	OptimStepSettings* CurStep = &OptimSteps2[iStep];

	m_vModType = CurStep->ModelType;
	m_vSpType = CurStep->SpectrumType;
	m_vTest = CurStep->Operation;
	m_vNHarmType = CurStep->NHarmType;
	m_vKfnlType = CurStep->KfnlType;
	m_vNCompMean = CurStep->NCompMean;
	m_vNCompPlus = CurStep->NCompPlus;
	m_vNCompStep = CurStep->NCompStep;
	m_vHiperMean = CurStep->HiperMean;
	m_vHiperPlus = CurStep->HiperPlus;
	m_vHiperStep = CurStep->HiperStep;
	m_vNHarmMean = CurStep->NHarmMean;
	m_vNHarmPlus = CurStep->NHarmPlus;
	m_vNHarmStep = CurStep->NHarmStep;
	m_vKfnlMean = CurStep->KfnlMean;
	m_vKfnlPlus = CurStep->KfnlPlus;
	m_vKfnlStep = CurStep->KfnlStep;
	m_vLeftMean = CurStep->LeftMean;
	m_vLeftPlus = CurStep->LeftPlus;
	m_vLeftStep = CurStep->LeftStep;
	m_vRightMean = CurStep->RightMean;
	m_vRightPlus = CurStep->RightPlus;
	m_vRightStep = CurStep->RightStep;
	m_vAddPrep = CurStep->Preps.GetAddsAsStr();
	m_vMaxLetter = CurStep->Preps.MaxLen;
	m_vMaxBetter = CurStep->Preps.NumBest;
	m_vMaxMod = CurStep->MaxModels;
	m_vMah = CurStep->Outs.limMah;
	m_vSEC = CurStep->Outs.limSEC;
	m_vSECV = CurStep->Outs.limSECV;
	m_vBound = CurStep->Outs.limBound;
	m_vIsMah = CurStep->Outs.chMah;
	m_vIsSEC = CurStep->Outs.chSEC;
	m_vIsSECV = CurStep->Outs.chSECV;
	m_vIsBad = CurStep->Outs.chBad;
	m_vIsBound = CurStep->Outs.chBound;
	m_vOperationOnOff = CurStep->IsOperation;
	m_vCritSEC = CurStep->Crits.bSEC;
	m_vCritSECV = CurStep->Crits.bSECV;
	m_vCritSECSECV = CurStep->Crits.bSECSECV;
	m_vCritSEV = CurStep->Crits.bSEV;
	m_vCritR2sec = CurStep->Crits.bR2sec;
	m_vCritR2secv = CurStep->Crits.bR2secv;
	m_vCritF = CurStep->Crits.bF;
	m_vCritErr = CurStep->Crits.bErr;
	m_vCritSDV = CurStep->Crits.bSDV;
	m_vCritR2sev = CurStep->Crits.bR2sev;
	m_vCritK1 = CurStep->Crits.bK1;
	m_vCritK2 = CurStep->Crits.bK2;
	m_vCritK3 = CurStep->Crits.bK3;
	m_vCritK4 = CurStep->Crits.bK4;
	m_vCritK5 = CurStep->Crits.bK5;
	m_vCritK6 = CurStep->Crits.bK6;
	m_vCritK7 = CurStep->Crits.bK7;
	m_vPrepA = CurStep->Preps.pA;
	m_vPrepB = CurStep->Preps.pB;
	m_vPrepM = CurStep->Preps.pM;
	m_vPrepD = CurStep->Preps.pD;
	m_vPrepC = CurStep->Preps.pC;
	m_vPrepN = CurStep->Preps.pN;
	m_vPrep1 = CurStep->Preps.p1;
	m_vPrep2 = CurStep->Preps.p2;
	m_vPrepNo = CurStep->Preps.pNo;
	m_vUseBest = CurStep->Preps.UseBest;

	CurStep->GetExcPoints(PointsExc);

	CurStep->GetCalibValid(OptimCalib, OptimValid);
	SortSpectra();

	SetSortCrit(CurStep->SortCrit);
	m_vMainCrit = CurStep->MainCrit;

	CreatePreprocListAll(needUpd);
	if(needUpd)
	{
		CString s, Hdr = cEmpty;
		switch(Mode)
		{
		case C_OPT_MODE_SINGLE:  Hdr = ParcelLoadString(STRID_OPTIM_MODEL_DLG_HDR_SINGLE);  break;
		case C_OPT_MODE_BYTMPL:  Hdr = ParcelLoadString(STRID_OPTIM_MODEL_DLG_HDR_BYTMPL);  break;
		case C_OPT_MODE_TMPLCREATE:  Hdr = ParcelLoadString(STRID_OPTIM_MODEL_DLG_HDR_TMPLCREATE);  break;
		case C_OPT_MODE_TMPLEDIT:  Hdr = ParcelLoadString(STRID_OPTIM_MODEL_DLG_HDR_TMPLEDIT);  break;
		}
		SetWindowText(Hdr);

		s.Format(ParcelLoadString(STRID_OPTIM_MODEL_DLG_SETTINGS), iStep + 1);
		GetDlgItem(IDC_STATIC_8)->SetWindowText(s);

		UpdateData(0);

		CreateSortList();
		FillTableSpectra();
		FillSpecLists();
		FillTableRes(true, true);

		ShowFields();
	}

	return true;
}

bool COptimModelDlg2::CheckOptimSettings()
{
	CString s;
	if(m_vLeftMean - m_vLeftPlus >= m_vRightMean + m_vRightPlus)
	{
		ParcelError(ParcelLoadString(STRID_OPTIM_MODEL_DLG_ERROR_RANGE));
		return false;
	}
	if(GetNCalibUse() <= 0)
	{
		ParcelError(ParcelLoadString(STRID_OPTIM_MODEL_DLG_ERROR_NOCALIB));
		return false;
	}
	if(GetNPreprocs() <= 0)
	{
		ParcelError(ParcelLoadString(STRID_OPTIM_MODEL_DLG_ERROR_PREPEMPTY));
		return false;
	}
	if(!IsAddPreprocValid)
	{
		ParcelError(ParcelLoadString(STRID_OPTIM_MODEL_DLG_ERROR_ADDPREPS));
		m_vTab = C_OPT_PAGE_PREP;
		m_Tab.SetCurSel(m_vTab);
		ShowFields();

		GetDlgItem(IDC_INPUT_PREP)->SetFocus();
		HWND hWndLastControl;
		GetDlgItem(IDC_INPUT_PREP, &hWndLastControl);
		::SendMessage(hWndLastControl, EM_SETSEL, 0, -1);
		return false;
	}
	if(!m_vCritSEC && !m_vCritSECV && !m_vCritSEV)
	{
		ParcelError(ParcelLoadString(STRID_OPTIM_MODEL_DLG_ERROR_CRITEMPTY));
		return false;
	}
	if(m_vMaxMod < 1 || m_vMaxMod > 10000)
	{
		s.Format(ParcelLoadString(STRID_OPTIM_MODEL_DLG_ERROR_MAXMODELS), 1, 10000);
		ParcelError(s);

		GetDlgItem(IDC_MAXMODELS)->SetFocus();
		HWND hWndLastControl;
		GetDlgItem(IDC_MAXMODELS, &hWndLastControl);
		::SendMessage(hWndLastControl, EM_SETSEL, 0, -1);
		return false;
	}

	return true;
}

bool COptimModelDlg2::AcceptStep()
{
	if(!UpdateData())
		return false;
	if(!CheckOptimSettings() || !CheckStepNum(iStep))
		return false;

	OptimStepSettings* CurStep = &OptimSteps2[iStep];

	CurStep->ModelType = m_vModType;
	CurStep->SpectrumType = m_vSpType;
	CurStep->Operation = m_vTest;
	CurStep->MainCrit = m_vMainCrit;
	CurStep->NHarmType = m_vNHarmType;
	CurStep->KfnlType = m_vKfnlType;
	CurStep->NCompMean = m_vNCompMean;
	CurStep->NCompPlus = m_vNCompPlus;
	CurStep->NCompStep = m_vNCompStep;
	CurStep->HiperMean = m_vHiperMean;
	CurStep->HiperPlus = m_vHiperPlus;
	CurStep->HiperStep = m_vHiperStep;
	CurStep->NHarmMean = m_vNHarmMean;
	CurStep->NHarmPlus = m_vNHarmPlus;
	CurStep->NHarmStep = m_vNHarmStep;
	CurStep->KfnlMean = m_vKfnlMean;
	CurStep->KfnlPlus = m_vKfnlPlus;
	CurStep->KfnlStep = m_vKfnlStep;
	CurStep->LeftMean = m_vLeftMean;
	CurStep->LeftPlus = m_vLeftPlus;
	CurStep->LeftStep = m_vLeftStep;
	CurStep->RightMean = m_vRightMean;
	CurStep->RightPlus = m_vRightPlus;
	CurStep->RightStep = m_vRightStep;
	CurStep->Preps.MaxLen = m_vMaxLetter;
	CurStep->Preps.NumBest = m_vMaxBetter;
	CurStep->MaxModels = m_vMaxMod;
	CurStep->Outs.limMah = m_vMah;
	CurStep->Outs.limSEC = m_vSEC;
	CurStep->Outs.limSECV = m_vSECV;
	CurStep->Outs.limBound = m_vBound;
	CurStep->Outs.chMah = (m_vIsMah) ? true : false;
	CurStep->Outs.chSEC = (m_vIsSEC) ? true : false;
	CurStep->Outs.chSECV = (m_vIsSECV) ? true : false;
	CurStep->Outs.chBad = (m_vIsBad) ? true : false;
	CurStep->Outs.chBound = (m_vIsBound) ? true : false;
	CurStep->IsOperation = (m_vOperationOnOff) ? true : false;
	CurStep->Crits.bSEC = (m_vCritSEC) ? true : false;
	CurStep->Crits.bSECV = (m_vCritSECV) ? true : false;
	CurStep->Crits.bSECSECV = (m_vCritSECSECV) ? true : false;
	CurStep->Crits.bSEV = (m_vCritSEV) ? true : false;
	CurStep->Crits.bR2sec = (m_vCritR2sec) ? true : false;
	CurStep->Crits.bR2secv = (m_vCritR2secv) ? true : false;
	CurStep->Crits.bF = (m_vCritF) ? true : false;
	CurStep->Crits.bErr = (m_vCritErr) ? true : false;
	CurStep->Crits.bSDV = (m_vCritSDV) ? true : false;
	CurStep->Crits.bR2sev = (m_vCritR2sev) ? true : false;
	CurStep->Crits.bK1 = (m_vCritK1) ? true : false;
	CurStep->Crits.bK2 = (m_vCritK2) ? true : false;
	CurStep->Crits.bK3 = (m_vCritK3) ? true : false;
	CurStep->Crits.bK4 = (m_vCritK4) ? true : false;
	CurStep->Crits.bK5 = (m_vCritK5) ? true : false;
	CurStep->Crits.bK6 = (m_vCritK6) ? true : false;
	CurStep->Crits.bK7 = (m_vCritK7) ? true : false;
	CurStep->Preps.pA = (m_vPrepA) ? true : false;
	CurStep->Preps.pB = (m_vPrepB) ? true : false;
	CurStep->Preps.pM = (m_vPrepM) ? true : false;
	CurStep->Preps.pD = (m_vPrepD) ? true : false;
	CurStep->Preps.pC = (m_vPrepC) ? true : false;
	CurStep->Preps.pN = (m_vPrepN) ? true : false;
	CurStep->Preps.p1 = (m_vPrep1) ? true : false;
	CurStep->Preps.p2 = (m_vPrep2) ? true : false;
	CurStep->Preps.pNo = (m_vPrepNo) ? true : false;
	CurStep->Preps.UseBest = (m_vUseBest) ? true : false;

	CurStep->SortCrit = GetSortCrit();
	ItIntInt it;
	CurStep->Preps.Adds.clear();
	for(it=PreprocListAdd.begin(); it!=PreprocListAdd.end(); ++it)
		CurStep->Preps.Adds.push_back(cGetPreprocAsStr(&(*it)));

	GetExcPoints(CurStep->ExcPoints);
	GetCalibValid(CurStep->Calib, CurStep->Valid);

	return true;
}

bool COptimModelDlg2::AcceptToModel(CModelData* ModData)
{
	if(!CheckStepNum(iStep))
		return false;

	OptimResults* CurRes = &OptimRes[iStep];

	int Ind = m_listResult.GetNextItem(-1, LVNI_SELECTED);
	if(Ind < 0 || Ind >= OptimRes[iStep].GetNModels())
		return false;

	OptimModel* OptMod = &CurRes->OptModels[Ind];

	CString s;

	ModData->SetModelType(OptMod->ModelType);
	ModData->SetSpectrumType(OptMod->SpectrumType);

	ModData->SetLowSpecRange(OptMod->RangeLeft);
	ModData->SetUpperSpecRange(OptMod->RangeRight);
	ModData->GetSpecPoints();

	ModData->SetNCompQnt(OptMod->NComp);
	if(ModData->GetModelType() == C_MOD_MODEL_HSO)
	{
		ModData->SetDimHiper(OptMod->Hiper);
		ModData->SetNHarm(OptMod->NHarm);
		ModData->SetKfnl(OptMod->Kfnl);
	}

	s = PrePrep + OptMod->Preproc;
	ModData->SetAllPreproc(s);

	SInt Points;
	OptMod->GetExcPoints(Points);
	ModData->SetExcPoints(Points);

	ItModSam itS;
	ModData->DeleteAllSamplesForCalib();
	for(itS=CurRes->SamplesForCalib.begin(); itS!=CurRes->SamplesForCalib.end(); ++itS)
		ModData->AddSampleForCalib(&(*itS));

	int i, N = ModData->GetNSamplesForCalib();
	for(i=N-1; i>=0; i--)
		if(ModData->GetSampleForCalib(i)->GetNSpectraUsed() <= 0)
			ModData->DeleteSampleForCalib(i);

	ModData->DeleteAllSamplesForValid();
	for(itS=CurRes->SamplesForValid.begin(); itS!=CurRes->SamplesForValid.end(); ++itS)
		ModData->AddSampleForValid(&(*itS));

	N = ModData->GetNSamplesForValid();
	for(i=N-1; i>=0; i--)
		if(ModData->GetSampleForValid(i)->GetNSpectraUsed() <= 0)
			ModData->DeleteSampleForValid(i);

	ModData->CalcModParamLimits();

	return true;
}

bool COptimModelDlg2::AcceptToTemplate(Template* Tmpl)
{
	Tmpl->Steps.clear();
	for(int i=0; i<nSteps; i++)
	{
		OptimStepSettings* OptStep = &OptimSteps2[i];

		if(i == 0)
		{
			Tmpl->NCompMean = OptStep->NCompMean;
			Tmpl->HiperMean = OptStep->HiperMean;
			Tmpl->NHarmMean = OptStep->NHarmMean;
			Tmpl->KfnlMean = OptStep->KfnlMean;
			Tmpl->SpecLeftMean = OptStep->LeftMean;
			Tmpl->SpecRightMean = OptStep->RightMean;

			OptStep->GetExcPoints(Tmpl->ExcPoints);
		}
		if(i == nSteps - 1)
			Tmpl->LastCrit = OptStep->SortCrit;

		TemplateStep TmplStep;

		TmplStep.Num = i;
		TmplStep.ModelType = OptStep->ModelType;
		TmplStep.SpType = OptStep->SpectrumType;
		TmplStep.NCompPlus = OptStep->NCompPlus;
		TmplStep.NCompStep = OptStep->NCompStep;
		TmplStep.HiperPlus = OptStep->HiperPlus;
		TmplStep.HiperStep = OptStep->HiperStep;
		TmplStep.NHarmPlus = OptStep->NHarmPlus;
		TmplStep.NHarmStep = OptStep->NHarmStep;
		TmplStep.NHarmType = OptStep->NHarmType;
		TmplStep.KfnlPlus = OptStep->KfnlPlus;
		TmplStep.KfnlStep = OptStep->KfnlStep;
		TmplStep.KfnlType = OptStep->KfnlType;
		TmplStep.SpecLeftPlus = OptStep->LeftPlus;
		TmplStep.SpecLeftStep = OptStep->LeftStep;
		TmplStep.SpecRightPlus = OptStep->RightPlus;
		TmplStep.SpecRightStep = OptStep->RightStep;

		TmplStep.prepsets.Copy(OptStep->Preps);
		TmplStep.outs.Copy(OptStep->Outs);

		TmplStep.MaxModels = OptStep->MaxModels;

		TmplStep.IsTest = OptStep->IsOperation;
		TmplStep.Operation = OptStep->Operation;
		TmplStep.MainCrit = OptStep->MainCrit;
		TmplStep.SortCrit = OptStep->SortCrit;

		Tmpl->Steps.push_back(TmplStep);
	}

	return true;
}

void COptimModelDlg2::CreateSortList()
{
	if(!CheckStepNum(iStep))
		return;

	OptimResults* CurRes = &OptimRes[iStep];

	m_Sort.ResetContent();

	if(CurRes->GetNCrits() <= 0)
	{
		m_Sort.AddString(ParcelLoadString(STRID_OPTIM_MODEL_DLG_NO));
		m_vSort = 0;
	}
	else
	{
		int i;
		ItInt it;
		for(i=0, it=CurRes->Crits.begin(); it!=CurRes->Crits.end(); ++it, i++)
			m_Sort.AddString(cGetCritName((*it)));

		OptimStepSettings* CurStep = &OptimSteps2[iStep];
		SetSortCrit(CurStep->SortCrit);
	}

	m_Sort.SetCurSel(m_vSort);
}

int COptimModelDlg2::GetSortCrit()
{
	if(!CheckStepNum(iStep))
		return -1;

	OptimResults* CurRes = &OptimRes[iStep];

	if(m_vSort < 0 || m_vSort >= CurRes->GetNCrits())
		return -1;

	return CurRes->Crits[m_vSort];
}

bool COptimModelDlg2::SetSortCrit(int crit)
{
	if(!CheckStepNum(iStep))
		return false;

	OptimResults* CurRes = &OptimRes[iStep];

	ItInt it;
	int i;
	for(i=0, it=CurRes->Crits.begin(); it!=CurRes->Crits.end(); ++it, i++)
		if(crit == (*it))
		{
			m_vSort = i;
			break;
		}

	CurRes->Sort(crit);

	return false;
}

bool COptimModelDlg2::IsCritAval(int crit)
{
	switch(crit)
	{
	case C_CRIT_SEV:
	case C_CRIT_ERR:
	case C_CRIT_SDV:
	case C_CRIT_R2SEV:
	case C_CRIT_K1:
	case C_CRIT_K3:
	case C_CRIT_K7:
		if(OptimValid.size() <= 0)
			return false;
		break;
	}

	return true;
}

int COptimModelDlg2::GetMainCrit()
{
	return AvalCrits[m_vMainCrit];
}

bool COptimModelDlg2::SetMainCrit(int crit)
{
	ItInt it;
	int i;
	for(i=0, it=AvalCrits.begin(); it!=AvalCrits.end(); ++it, i++)
	{
		if(crit == (*it))
		{
			m_vSort = i;
			return true;
		}
	}

	return false;
}

void COptimModelDlg2::GetCalibValid(VOptSpec2& calib, VOptSpec2& valid)
{
	calib.clear();
	copy(OptimCalib.begin(), OptimCalib.end(), inserter(calib, calib.begin()));

	valid.clear();
	copy(OptimValid.begin(), OptimValid.end(), inserter(valid, valid.begin()));
}

void COptimModelDlg2::GetExcPoints(SInt& points)
{
	points.clear();
	copy(PointsExc.begin(), PointsExc.end(), inserter(points, points.begin()));
}

void COptimModelDlg2::SortSpectra()
{
	bool dir = (m_vSortSpec % 2) ? true : false;
	SortSpecBy = (m_vSortSpec < 2) ? C_OPTIM_SORT_SPEC_NAME_2 : C_OPTIM_SORT_SPEC_REF_2;
	if(!dir)
	{
		sort(OptimCalib.begin(), OptimCalib.end(), SortOptimSpectraUp);
		sort(OptimValid.begin(), OptimValid.end(), SortOptimSpectraUp);
	}
	else
	{
		sort(OptimCalib.begin(), OptimCalib.end(), SortOptimSpectraDown);
		sort(OptimValid.begin(), OptimValid.end(), SortOptimSpectraDown);
	}
}

int COptimModelDlg2::GetNPointsShow()
{
	int low = max(MinRange, m_vLeftMean - m_vLeftPlus);
	int upper = min(MaxRange, m_vRightMean + m_vRightPlus);

	VDbl Points;
	ParentPrj->ExtractSpecPoints(low, upper, Points);
	int N = int(Points.size());

	return N;
}

int COptimModelDlg2::GetNExcPointsShow()
{
	int indS = ParentPrj->GetSpecPointInd(PointsAll[0]);
	int indF = ParentPrj->GetSpecPointInd(PointsAll[NAllPoints-1]);

	int N;
	ItSInt it;
	for(N=0, it=PointsExc.begin(); it!=PointsExc.end(); ++it)
	{
		if((*it) < indS)  continue;
		if((*it) > indF)  break;

		N++;
	}

	return N;
}

int COptimModelDlg2::GetNCalibUse()
{
	int N = 0;
	ItOptSpec2 it;
	for(it=OptimCalib.begin(); it!=OptimCalib.end(); ++it)
		if(it->Use)
			N++;

	return N;
}

int COptimModelDlg2::GetNValidUse()
{
	int N = 0;
	ItOptSpec2 it;
	for(it=OptimValid.begin(); it!=OptimValid.end(); ++it)
		if(it->Use)
			N++;

	return N;
}

int  COptimModelDlg2::GetNPreprocs()
{
	return int(PreprocList.size() + PreprocListAdd.size());
}

void COptimModelDlg2::FillSpecLists()
{
	FillListAll(0);
	FillListExc(0);
}

void COptimModelDlg2::FillListAll(int newsel)
{
	int low = max(MinRange, m_vLeftMean - m_vLeftPlus);
	int upper = min(MaxRange, m_vRightMean + m_vRightPlus);

	VDbl Points;
	ParentPrj->ExtractSpecPoints(low, upper, Points);
	int i, N = int(Points.size());

	m_listAll.DeleteAllItems();
	for(i=0; i<N; i++)
	{
		CString s;

		int ind = ParentPrj->GetSpecPointInd(Points[i]);
		s.Format(cFmt1, ind + 1);
		m_listAll.InsertItem(i, s);
		s.Format(cFmt86, Points[i]);
		m_listAll.SetItemText(i, 1, s);
	}
	for(i=0; i<2; i++)
		m_listAll.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);

	int sel = (newsel < 0 || newsel >= N) ? 0 : newsel;
	m_listAll.SetItemState(sel, LVIS_SELECTED, LVIS_SELECTED);

	CString s;
	s.Format(ParcelLoadString(STRID_OPTIM_MODEL_SPEC_ALLPOINTS), N);
	GetDlgItem(IDC_STATIC_33)->SetWindowText(s);
}

void COptimModelDlg2::FillListExc(int newsel)
{
	int low = max(MinRange, m_vLeftMean - m_vLeftPlus);
	int upper = min(MaxRange, m_vRightMean + m_vRightPlus);

	VDbl Points;
	ParentPrj->ExtractSpecPoints(low, upper, Points);
	int N = int(Points.size());
	int indS = ParentPrj->GetSpecPointInd(Points[0]);
	int indF = ParentPrj->GetSpecPointInd(Points[N-1]);

	m_listExc.DeleteAllItems();
	ItSInt it;
	int NN, i;
	for(NN=0, it=PointsExc.begin(); it!=PointsExc.end(); ++it)
	{
		if((*it) < indS)  continue;
		if((*it) > indF)  break;

		CString s;

		s.Format(cFmt1, (*it) + 1);
		m_listExc.InsertItem(NN, s);
		s.Format(cFmt86, Points[(*it) - indS]);
		m_listExc.SetItemText(NN, 1, s);

		NN++;
	}
	for(i=0; i<2; i++)
		m_listExc.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);

	int isel = (newsel < 0 || newsel >= NN) ? 0 : newsel;
	m_listExc.SetItemState(isel, LVIS_SELECTED, LVIS_SELECTED);

	CString s;
	s.Format(ParcelLoadString(STRID_OPTIM_MODEL_SPEC_EXCPOINTS), NN);
	GetDlgItem(IDC_STATIC_34)->SetWindowText(s);

	int Nall = GetNPointsShow();
	GetDlgItem(IDC_ADD)->EnableWindow(!(isTmpl && iStep > 0) && NN < Nall);
	GetDlgItem(IDC_ADD_ALL)->EnableWindow(!(isTmpl && iStep > 0) && NN < Nall);
	GetDlgItem(IDC_REMOVE)->EnableWindow(!(isTmpl && iStep > 0) && NN > 0);
	GetDlgItem(IDC_REMOVE_ALL)->EnableWindow(!(isTmpl && iStep > 0) && NN > 0);

	m_listAll.RedrawWindow();
}

void COptimModelDlg2::FillTableRes(bool bReMakeColumns, bool bDeleteItems)
{
	if(!CheckStepNum(iStep))
		return;

	OptimResults* CurRes = &OptimRes[iStep];

	CString s;
	int i, j, nColCrit = CurRes->GetNCrits();
	int nHQO = (CurRes->IsHQO()) ? 3 : 0;
	if(bReMakeColumns)
	{
		m_listResult.DeleteAllItems();
		int nCol = m_listResult.GetHeaderCtrl()->GetItemCount();
		for(i=nCol-1; i>=0; i--)
			m_listResult.DeleteColumn(i);

		ItInt itC;
		int N;
		for(N=0, itC=CurRes->Crits.begin(); itC!=CurRes->Crits.end(); ++itC, N++)
		{
			CString s = cGetCritName(*itC);
			m_listResult.InsertColumn(N, s, LVCFMT_RIGHT, 0, N);
		}
		m_listResult.InsertColumn(nColCrit, ParcelLoadString(STRID_OPTIM_MODEL_DLG_COLLEFT), LVCFMT_RIGHT, 0, nColCrit+2);
		m_listResult.InsertColumn(nColCrit+1, ParcelLoadString(STRID_OPTIM_MODEL_DLG_COLRIGHT), LVCFMT_RIGHT, 0, nColCrit+3);
		m_listResult.InsertColumn(nColCrit+2, ParcelLoadString(STRID_OPTIM_MODEL_DLG_COLPREP), LVCFMT_RIGHT, 0, nColCrit+4);
		m_listResult.InsertColumn(nColCrit+3, ParcelLoadString(STRID_OPTIM_MODEL_DLG_COLNCOMP), LVCFMT_RIGHT, 0, nColCrit+5);
		if(nHQO == 3)
		{
			m_listResult.InsertColumn(nColCrit+4, ParcelLoadString(STRID_OPTIM_MODEL_DLG_COLHIPER), LVCFMT_RIGHT, 0, nColCrit+6);
			m_listResult.InsertColumn(nColCrit+5, ParcelLoadString(STRID_OPTIM_MODEL_DLG_COLNHARM), LVCFMT_RIGHT, 0, nColCrit+7);
			m_listResult.InsertColumn(nColCrit+6, ParcelLoadString(STRID_OPTIM_MODEL_DLG_COLKFNL), LVCFMT_RIGHT, 0, nColCrit+8);
		}
		m_listResult.InsertColumn(nColCrit+4+nHQO, ParcelLoadString(STRID_OPTIM_MODEL_DLG_COLNEXC), LVCFMT_RIGHT, 0, nColCrit+nColCrit+6+nHQO);
		m_listResult.InsertColumn(nColCrit+5+nHQO, ParcelLoadString(STRID_OPTIM_MODEL_DLG_COLMODEL), LVCFMT_RIGHT, 0, nColCrit);
		m_listResult.InsertColumn(nColCrit+6+nHQO, ParcelLoadString(STRID_OPTIM_MODEL_DLG_COLSPEC), LVCFMT_RIGHT, 0, nColCrit+1);

		for(i=0; i<m_listResult.GetHeaderCtrl()->GetItemCount(); i++)
			m_listResult.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
		m_listResult.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);
	}

	int N = CurRes->GetNModels();
	if(N <= 0)
	{
		GetDlgItem(IDC_OPTIM_MODEL_RESULT_TXT)->SetWindowText(cEmpty);
		GetDlgItem(IDC_STATIC_39)->SetWindowText(cEmpty);
		SetSortIcons();
		return;
	}
	s.Format(ParcelLoadString(STRID_OPTIM_MODEL_DLG_NMODEL), N, CurRes->NCalcModels);
	GetDlgItem(IDC_OPTIM_MODEL_RESULT_TXT)->SetWindowText(s);
	s.Format(ParcelLoadString(STRID_OPTIM_MODEL_DLG_RESULT), iStep);
	GetDlgItem(IDC_STATIC_39)->SetWindowText(s);

	CurRes->Sort(GetSortCrit());

	bool ManyValid = (int(CurRes->SamplesForValid.size()) > 1);

	if(bDeleteItems) 
	{
		m_listResult.DeleteAllItems();
	}

	ItOptMod itM;
	for(i=0, itM=CurRes->OptModels.begin(); itM!=CurRes->OptModels.end(); ++itM, i++)
	{
		ItInt itC;
		for(j=0, itC=CurRes->Crits.begin(); itC!=CurRes->Crits.end(); ++itC, j++)
		{
			if(!ManyValid && (*itC) == C_CRIT_R2SEV)
			{
				s = ParcelLoadString(STRID_OPTIM_MODEL_DLG_DASH);
			}
			else
			{
				s.Format(cGetCritFormat(*itC), itM->GetValue(*itC));
			}

			if(j == 0 && bDeleteItems)  
			{
				m_listResult.InsertItem(i, s);
			}
			else
			{
				m_listResult.SetItemText(i, j, s);
			}
		}

		s.Format(cFmt1, itM->RangeLeft);
		if(nColCrit == 0 && bDeleteItems)  
		{
			m_listResult.InsertItem(i, s);
		}
		else
		{
			m_listResult.SetItemText(i, nColCrit, s);
		}

		s.Format(cFmt1, itM->RangeRight);
		m_listResult.SetItemText(i, nColCrit+1, s);
		s = PrePrep + itM->Preproc;
		m_listResult.SetItemText(i, nColCrit+2, s);
		s.Format(cFmt1, itM->NComp);
		m_listResult.SetItemText(i, nColCrit+3, s);
		if(nHQO > 0)
		{
			s.Format(cFmt64, itM->Hiper);
			m_listResult.SetItemText(i, nColCrit+4, s);
			s.Format(cFmt1, itM->NHarm);
			m_listResult.SetItemText(i, nColCrit+5, s);
			s.Format(cFmt31, itM->Kfnl);
			m_listResult.SetItemText(i, nColCrit+6, s);
		}
		s.Format(cFmt1, itM->GetNExcPoints());
		m_listResult.SetItemText(i, nColCrit+4+nHQO, s);
		s = cGetModelTypeNameShort(itM->ModelType);
		m_listResult.SetItemText(i, nColCrit+5+nHQO, s);
		s = cGetSpectrumTypeNameShort(itM->SpectrumType);
		m_listResult.SetItemText(i, nColCrit+6+nHQO, s);
	}

	SetSortIcons();
	for(i=0; i<m_listResult.GetHeaderCtrl()->GetItemCount(); i++)
	{
		m_listResult.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	}

	m_listResult.SetItemState(0, LVIS_SELECTED, LVIS_SELECTED);
}

void COptimModelDlg2::FillTableSpectra()
{
	if(!CheckStepNum(iStep))
		return;

	m_listSpectra.DeleteAllItems();

	VOptSpec2* Spectra = (m_vCalibValid == C_OPTIM_SMPL_CALIB) ? &OptimCalib : &OptimValid;
	int i, N = int(Spectra->size());
	int NN = (m_vCalibValid == C_OPTIM_SMPL_CALIB) ? GetNCalibUse() : GetNValidUse();
	CString s;

	ItOptSpec2 it;
	for(i=0, it=Spectra->begin(); it!=Spectra->end(); ++it, i++)
	{
		s.Format(cFmt1, i+1);
		m_listSpectra.InsertItem(i, s);
		
		m_listSpectra.SetItemText(i, 1, it->Name);
		s.Format(cFmt42, it->Ref);
		m_listSpectra.SetItemText(i, 2, s);
		s = (it->Use) ? ParcelLoadString(STRID_OPTIM_MODEL_DLG_USEYES) : ParcelLoadString(STRID_OPTIM_MODEL_DLG_USENO);
		m_listSpectra.SetItemText(i, 3, s);
	}

	for(i=0; i<m_listSpectra.GetHeaderCtrl()->GetItemCount(); i++)
		m_listSpectra.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	m_listSpectra.SetItemState(0, LVIS_SELECTED, LVIS_SELECTED);

	s.Format(ParcelLoadString(STRID_OPTIM_MODEL_OUTS_NUMSPECTRA), N, NN);
	GetDlgItem(IDC_STATIC_41)->SetWindowText(s);
}

void COptimModelDlg2::SetSortIcons()
{
	int iCol = -1;
	bool dir = true;

	if(CheckStepNum(iStep) && OptimRes[iStep].GetNModels() > 0)
		iCol = m_vSort;

	m_listResult.ShowSortIcons(iCol, dir);
}

void COptimModelDlg2::CreatePreprocListAll(bool needUpd)
{
	PreprocAval.clear();

	if(m_vPrepA)  PreprocAval.push_back(C_PREPROC_A);
	if(m_vPrepB)  PreprocAval.push_back(C_PREPROC_B);
	if(m_vPrepM)  PreprocAval.push_back(C_PREPROC_M);
	if(m_vPrepD)  PreprocAval.push_back(C_PREPROC_D);
	if(m_vPrepC)  PreprocAval.push_back(C_PREPROC_C);
	if(m_vPrepN)  PreprocAval.push_back(C_PREPROC_N);
	if(m_vPrep1)  PreprocAval.push_back(C_PREPROC_1);
	if(m_vPrep2)  PreprocAval.push_back(C_PREPROC_2);

	PreprocMaxLen = min(m_vMaxLetter, int(PreprocAval.size()));

	PreprocList.clear();

	VInt Old;
	Old.clear();
	CreatePreprocList(&Old);
	CreatePreprocListAdd(m_vAddPrep);

	if(needUpd)
	{
		CString s;
		s.Format(ParcelLoadString(STRID_OPTIM_MODEL_PRE_COUNT), GetNPreprocs());
		GetDlgItem(IDC_STATIC_38)->SetWindowText(s);
	}
}

void COptimModelDlg2::CreatePreprocList(VInt* Old)
{
	ItInt it;
	for(it=PreprocAval.begin(); it!=PreprocAval.end(); ++it)
	{
		if(!IsPreprocAddAvailable(Old, (*it)))
			continue;

		VInt NewP;
		copy(Old->begin(), Old->end(), inserter(NewP, NewP.begin()));
		NewP.push_back((*it));

		PreprocList.push_back(NewP);

		if(PreprocMaxLen > int(NewP.size()))
			CreatePreprocList(&NewP);
	}
}

void COptimModelDlg2::CreatePreprocListAdd(CString str)
{
	IsAddPreprocValid = true;

	VInt empPrep;
	PreprocListAdd.clear();
	if(m_vPrepNo)
		PreprocListAdd.push_back(empPrep);

	int i, N = int(_tcslen(str));
	if(N == 0)
	{
		if(m_vOperationOnOff)
			PreprocListAdd.push_back(empPrep);
		return;
	}

	VStr PrepStr;
	CString s = cEmpty;
	for(i=0; i<N; i++)
	{
		if(str[i] == ';' || str[i] == ',')
		{
			PrepStr.push_back(s);
			s = cEmpty;
			continue;
		}
		else
			s += str[i];
	}
	if(!s.IsEmpty())
		PrepStr.push_back(s);
	else
		IsAddPreprocValid = false;

	ItStr it;
	for(it=PrepStr.begin(); it!=PrepStr.end(); ++it)
	{
		CString tmp = (*it);
		tmp.Trim();

		VInt NewP;
		bool err = false, IsNew = false;

		int n = int(_tcslen(tmp));
		for(i=0; i<n; i++)
		{
			int prep = cGetPreprocIntByChar(tmp[i]);
			if(prep == C_PREPROC_0)
			{
				err = true;
				break;
			}

			if(IsPreprocAddAvailable(&NewP, prep))
			{
				NewP.push_back(prep);
				ItInt itf;
				itf = find(PreprocAval.begin(), PreprocAval.end(), prep);
				if(itf == PreprocAval.end() || PreprocMaxLen < int(NewP.size()))
					IsNew = true;
			}
			else
				err = true;
		}

		if(!err)
		{
//			if(IsNew)
			{
				ItIntInt it2 = find(PreprocListAdd.begin(), PreprocListAdd.end(), NewP);
				if(it2 == PreprocListAdd.end())
					PreprocListAdd.push_back(NewP);
//				else
//					IsAddPreprocValid = false;
			}
		}
		else
			IsAddPreprocValid = false;
	}
}

bool COptimModelDlg2::IsPreprocAddAvailable(VInt* prepline, int prep)
{
	VInt preps;
	cGetPreprocFromStr(PrePrep, preps);
	copy(prepline->begin(), prepline->end(), inserter(preps, preps.end()));

	if(prep <= C_PREPROC_0 || prep > C_PREPROC_2)
		return false;

	int n = int(preps.size());

	if(prep == C_PREPROC_A && n > 0)
		return false;

	ItInt it;
	for(it=preps.begin(); it!=preps.end(); ++it)
	{
		if(prep == (*it) || (prep == C_PREPROC_1 && (*it) == C_PREPROC_2) ||
		   (prep == C_PREPROC_2 && (*it) == C_PREPROC_1))
			return false;
	}
	if(n > 0 && prep == C_PREPROC_D && preps[n-1] == C_PREPROC_C)
		return false;
	if(n > 0 && prep == C_PREPROC_C && preps[n-1] == C_PREPROC_M)
		return false;
	if(n > 1 && prep == C_PREPROC_C && preps[n-1] == C_PREPROC_D && preps[n-2] == C_PREPROC_M)
		return false;

	return true;
}

void COptimModelDlg2::SetBetterPreps()
{
	if(!CheckStepNum(iStep))
		return;

	OptimResults* CurRes = &OptimRes[iStep];

	m_vPrepA = CurRes->IsPrepUse(C_PREPROC_A, m_vMaxBetter);
	m_vPrepB = CurRes->IsPrepUse(C_PREPROC_B, m_vMaxBetter);
	m_vPrepM = CurRes->IsPrepUse(C_PREPROC_M, m_vMaxBetter);
	m_vPrepD = CurRes->IsPrepUse(C_PREPROC_D, m_vMaxBetter);
	m_vPrepC = CurRes->IsPrepUse(C_PREPROC_C, m_vMaxBetter);
	m_vPrepN = CurRes->IsPrepUse(C_PREPROC_N, m_vMaxBetter);
	m_vPrep1 = CurRes->IsPrepUse(C_PREPROC_1, m_vMaxBetter);
	m_vPrep2 = CurRes->IsPrepUse(C_PREPROC_2, m_vMaxBetter);
	m_vPrepNo = CurRes->IsPrepUse(C_PREPROC_0, m_vMaxBetter);

	UpdateData(0);

	CreatePreprocListAll();
}

bool COptimModelDlg2::SetBestModel(int ind)
{
	if(!CheckStepNum(iStep))
		return false;

	OptimResults* CurRes = &OptimRes[iStep];
	if(ind < 0 || ind >= CurRes->GetNModels())
		return false;

	OptimModel *OptMod = &CurRes->OptModels[ind];

	m_vLeftMean = OptMod->RangeLeft;
	m_vRightMean = OptMod->RangeRight;
	m_vNCompMean = OptMod->NComp;
	if(OptMod->ModelType == C_MOD_MODEL_HSO)
	{
		m_vHiperMean = OptMod->Hiper;
		m_vNHarmMean = OptMod->NHarm;
		m_vKfnlMean = OptMod->Kfnl;
	}
	m_vNHarmType = (m_vNHarmMean == 0) ? C_OPTIM_HQO_PNT : C_OPTIM_HQO_HARM;
	m_vKfnlType = (m_vKfnlMean < 1.e-9) ? C_OPTIM_HQO_LIN : C_OPTIM_HQO_NLN;

	if(!isTmpl && cr_tmpl != C_OPTIM_BYTMPL_YES)
	{
		m_vLeftPlus = m_vLeftStep = 0;
		m_vRightPlus = m_vRightStep = 0;
		m_vNCompPlus = m_vNCompStep = 0;
		if(OptMod->ModelType == C_MOD_MODEL_HSO)
		{
			m_vHiperPlus = m_vHiperStep = 0;
			m_vNHarmPlus = m_vNHarmStep = 0;
			m_vKfnlPlus = m_vKfnlStep = 0;
		}

		m_vModType = OptMod->ModelType;
		m_vSpType = OptMod->SpectrumType;

		m_vPrepA = false;
		m_vPrepB = false;
		m_vPrepM = false;
		m_vPrepD = false;
		m_vPrepC = false;
		m_vPrepN = false;
		m_vPrep1 = false;
		m_vPrep2 = false;
		m_vPrepNo = (OptMod->Preproc.IsEmpty()) ? true : false;
		m_vMaxLetter = 1;
		m_vAddPrep = OptMod->Preproc;
	}
	else
	{
		if(!cCheckPrepAlready(m_vAddPrep, OptMod->Preproc))
		{
			if(!m_vAddPrep.IsEmpty())
				m_vAddPrep += _T("; ");
			m_vAddPrep += OptMod->Preproc;
		}
	}

	OptMod->GetExcPoints(PointsExc);

	ItModSam itS;
	for(itS=CurRes->SamplesForCalib.begin(); itS!=CurRes->SamplesForCalib.end(); ++itS)
	{
		ItOptSpec2 it2;
		for(it2=OptimCalib.begin(); it2!=OptimCalib.end(); ++it2)
			if((!itS->SampleName.Compare(it2->SampleName) && !itS->TransName.Compare(it2->TransName)))
				it2->Use = itS->Spectra[it2->Num];
	}
	for(itS=CurRes->SamplesForValid.begin(); itS!=CurRes->SamplesForValid.end(); ++itS)
	{
		ItOptSpec2 it2;
		for(it2=OptimValid.begin(); it2!=OptimValid.end(); ++it2)
			if((!itS->SampleName.Compare(it2->SampleName) && !itS->TransName.Compare(it2->TransName)))
				it2->Use = itS->Spectra[it2->Num];
	}

	CreatePreprocListAll(true);

	FillTableSpectra();
	FillSpecLists();
	UpdateData(0);
	EnableFields();

	return true;
}

void COptimModelDlg2::ClearCritFields()
{
	m_vCritSEC = false;
	m_vCritSECV = false;
	m_vCritSECSECV = false;
	m_vCritSEV = false;
	m_vCritR2sec = false;
	m_vCritR2secv = false;
	m_vCritF = false;
	m_vCritErr = false;
	m_vCritSDV = false;
	m_vCritR2sev = false;
	m_vCritK1 = false;
	m_vCritK2 = false;
	m_vCritK3 = false;
	m_vCritK4 = false;
	m_vCritK5 = false;
	m_vCritK6 = false;
	m_vCritK7 = false;
}

void COptimModelDlg2::SetCritFields(int crit)
{
	switch(crit)
	{
	case C_CRIT_SEC:  m_vCritSEC = true;  break;
	case C_CRIT_R2SEC:  m_vCritSEC = m_vCritR2sec = true;  break;
	case C_CRIT_SECV:  m_vCritSECV = true;  break;
	case C_CRIT_R2SECV:  m_vCritSECV = m_vCritR2secv = true;  break;
	case C_CRIT_F:  m_vCritSECV = m_vCritF = true;  break;
	case C_CRIT_SEV:  m_vCritSEV = true;  break;
	case C_CRIT_ERR:  m_vCritSEV = m_vCritErr = true;  break;
	case C_CRIT_SDV:  m_vCritSEV = m_vCritSDV = true;  break;
	case C_CRIT_R2SEV:  m_vCritSEV = m_vCritR2sev = true;  break;
	case C_CRIT_K1:  m_vCritSEC = m_vCritSEV = m_vCritK1 = true;  break;
	case C_CRIT_K2:  m_vCritSEC = m_vCritSECV = m_vCritK2 = true;  break;
	case C_CRIT_K3:  m_vCritSEC = m_vCritSEV = m_vCritK3 = true;  break;
	case C_CRIT_K4:  m_vCritSEC = m_vCritSECV = m_vCritK4 = true;  break;
	case C_CRIT_K5:  m_vCritSEC = m_vCritSECV = m_vCritK5 = true;  break;
	case C_CRIT_K6:  m_vCritSEC = m_vCritK6 = true;  break;
	case C_CRIT_K7:  m_vCritSEC = m_vCritSECV = m_vCritSEV = m_vCritK7 = true;  break;
	case C_CRIT_SECSECV:  m_vCritSECSECV = true;  break;
	default:  return;
	}
}

void COptimModelDlg2::UnsetCritFields(int crit)
{
	switch(crit)
	{
	case C_CRIT_SEC:
		m_vCritSEC = m_vCritSECSECV = m_vCritR2sec = m_vCritK1 = m_vCritK2 = m_vCritK3 = m_vCritK4 = m_vCritK5 = m_vCritK6 = m_vCritK7 = false;
		break;
	case C_CRIT_R2SEC:  m_vCritR2sec = false;  break;
	case C_CRIT_SECV:
		m_vCritSECV = m_vCritSECSECV = m_vCritR2secv = m_vCritF = m_vCritK2 = m_vCritK4 = m_vCritK5 = m_vCritK7 = false;
		break;
	case C_CRIT_R2SECV:  m_vCritR2secv = false;  break;
	case C_CRIT_F:  m_vCritF = false;  break;
	case C_CRIT_SEV:
		m_vCritSEV = m_vCritErr = m_vCritSDV = m_vCritR2sev = m_vCritK1 = m_vCritK3 = m_vCritK7 = false;
		break;
	case C_CRIT_ERR:  m_vCritErr = false;  break;
	case C_CRIT_SDV:  m_vCritSDV = false;  break;
	case C_CRIT_R2SEV:  m_vCritR2sev = false;  break;
	case C_CRIT_K1:  m_vCritK1 = false;  break;
	case C_CRIT_K2:  m_vCritK2 = false;  break;
	case C_CRIT_K3:  m_vCritK3 = false;  break;
	case C_CRIT_K4:  m_vCritK4 = false;  break;
	case C_CRIT_K5:  m_vCritK5 = false;  break;
	case C_CRIT_K6:  m_vCritK6 = false;  break;
	case C_CRIT_K7:  m_vCritK7 = false;  break;
	case C_CRIT_SECSECV:
		m_vCritSECSECV = false;
		break;
	default:  return;
	}
}

bool COptimModelDlg2::IsPointExc(int ind)
{
	int low = max(MinRange, m_vLeftMean - m_vLeftPlus);
	int upper = min(MaxRange, m_vRightMean + m_vRightPlus);

	VDbl Points;
	ParentPrj->ExtractSpecPoints(low, upper, Points);

	if(ind >= 0 && ind < int(Points.size()))
	{
		int iPnt = ParentPrj->GetSpecPointInd(Points[ind]);
		ItSInt it = find(PointsExc.begin(), PointsExc.end(), iPnt);
		if(it != PointsExc.end())
			return true;
	}

	return false;
}

int COptimModelDlg2::GetTransNum(ModelSample *pMSam)
{
	ItStr it;
	int i;
	for(i=0, it=TransesNames.begin(); it!=TransesNames.end(); ++it, i++)
		if(!it->Compare(pMSam->TransName))
			return i;

	return -1;
}

int COptimModelDlg2::GetTransNum(OptimSpectrum2* OptSp)
{
	ItStr it;
	int i;
	for(i=0, it=TransesNames.begin(); it!=TransesNames.end(); ++it, i++)
		if(!it->Compare(OptSp->TransName))
			return i;

	return -1;
}

void COptimModelDlg2::ShowFields(int act/* = -1*/)
{
	if(act >= C_OPT_PAGE_OUTS && act <= C_OPT_PAGE_SETS)
		m_vTab = act;

	int show1 = (m_vTab == C_OPT_PAGE_SETS) ? SW_SHOW : SW_HIDE;
	int show2 = (m_vTab == C_OPT_PAGE_SPEC) ? SW_SHOW : SW_HIDE;
	int show3 = (m_vTab == C_OPT_PAGE_PREP) ? SW_SHOW : SW_HIDE;
	int show4 = (m_vTab == C_OPT_PAGE_OUTS) ? SW_SHOW : SW_HIDE;
	int show5 = (m_vTab == C_OPT_PAGE_CRIT) ? SW_SHOW : SW_HIDE;

	GetDlgItem(IDC_MODEL_TYPE)->ShowWindow(show1);
	GetDlgItem(IDC_SPECTRUM_TYPE)->ShowWindow(show1);

	GetDlgItem(IDC_OPTIM_NCOMP_MEAN)->ShowWindow(show1);
	GetDlgItem(IDC_OPTIM_NCOMP_PLUS)->ShowWindow(show1);
	GetDlgItem(IDC_OPTIM_NCOMP_STEP)->ShowWindow(show1);

	GetDlgItem(IDC_OPTIM_HIPER_MEAN)->ShowWindow(show1);
	GetDlgItem(IDC_OPTIM_HIPER_PLUS)->ShowWindow(show1);
	GetDlgItem(IDC_OPTIM_HIPER_STEP)->ShowWindow(show1);

	GetDlgItem(IDC_OPTIM_NHARM_MEAN)->ShowWindow(show1);
	GetDlgItem(IDC_OPTIM_NHARM_PLUS)->ShowWindow(show1);
	GetDlgItem(IDC_OPTIM_NHARM_STEP)->ShowWindow(show1);
	GetDlgItem(IDC_OPTIM_NHARM_TYPE)->ShowWindow(show1);

	GetDlgItem(IDC_OPTIM_KFNL_MEAN)->ShowWindow(show1);
	GetDlgItem(IDC_OPTIM_KFNL_PLUS)->ShowWindow(show1);
	GetDlgItem(IDC_OPTIM_KFNL_STEP)->ShowWindow(show1);
	GetDlgItem(IDC_OPTIM_KFNL_TYPE)->ShowWindow(show1);

	GetDlgItem(IDC_STATIC_1)->ShowWindow(show1);
	GetDlgItem(IDC_STATIC_2)->ShowWindow(show1);
	GetDlgItem(IDC_STATIC_4)->ShowWindow(show1);
	GetDlgItem(IDC_STATIC_5)->ShowWindow(show1);
	GetDlgItem(IDC_STATIC_6)->ShowWindow(show1);
	GetDlgItem(IDC_STATIC_7)->ShowWindow(show1);
	GetDlgItem(IDC_STATIC_9)->ShowWindow(show1);
	GetDlgItem(IDC_STATIC_10)->ShowWindow(show1);
	GetDlgItem(IDC_STATIC_12)->ShowWindow(show1);
	GetDlgItem(IDC_STATIC_13)->ShowWindow(show1);
	GetDlgItem(IDC_STATIC_14)->ShowWindow(show1);
	GetDlgItem(IDC_STATIC_15)->ShowWindow(show1);
	GetDlgItem(IDC_STATIC_17)->ShowWindow(show1);
	GetDlgItem(IDC_STATIC_18)->ShowWindow(show1);
	GetDlgItem(IDC_STATIC_19)->ShowWindow(show1);
	GetDlgItem(IDC_STATIC_20)->ShowWindow(show1);

	GetDlgItem(IDC_STATIC_22)->ShowWindow(show2);
	GetDlgItem(IDC_STATIC_23)->ShowWindow(show2);
	GetDlgItem(IDC_STATIC_24)->ShowWindow(show2);
	GetDlgItem(IDC_STATIC_27)->ShowWindow(show2);
	GetDlgItem(IDC_STATIC_28)->ShowWindow(show2);
	GetDlgItem(IDC_STATIC_31)->ShowWindow(show2);
	GetDlgItem(IDC_STATIC_32)->ShowWindow(show2);
	GetDlgItem(IDC_STATIC_33)->ShowWindow(show2);
	GetDlgItem(IDC_STATIC_34)->ShowWindow(show2);
	GetDlgItem(IDC_OPTIM_LEFT_MEAN)->ShowWindow(show2);
	GetDlgItem(IDC_OPTIM_LEFT_PLUS)->ShowWindow(show2);
	GetDlgItem(IDC_OPTIM_LEFT_STEP)->ShowWindow(show2);
	GetDlgItem(IDC_OPTIM_RIGHT_MEAN)->ShowWindow(show2);
	GetDlgItem(IDC_OPTIM_RIGHT_PLUS)->ShowWindow(show2);
	GetDlgItem(IDC_OPTIM_RIGHT_STEP)->ShowWindow(show2);
	GetDlgItem(IDC_SPECPOINTS_ALL_LIST)->ShowWindow(show2);
	GetDlgItem(IDC_SPECPOINTS_EXC_LIST)->ShowWindow(show2);
	GetDlgItem(IDC_ADD)->ShowWindow(show2);
	GetDlgItem(IDC_ADD_ALL)->ShowWindow(show2);
	GetDlgItem(IDC_REMOVE)->ShowWindow(show2);
	GetDlgItem(IDC_REMOVE_ALL)->ShowWindow(show2);

	GetDlgItem(IDC_PREP_A)->ShowWindow(show3);
	GetDlgItem(IDC_PREP_B)->ShowWindow(show3);
	GetDlgItem(IDC_PREP_M)->ShowWindow(show3);
	GetDlgItem(IDC_PREP_D)->ShowWindow(show3);
	GetDlgItem(IDC_PREP_C)->ShowWindow(show3);
	GetDlgItem(IDC_PREP_N)->ShowWindow(show3);
	GetDlgItem(IDC_PREP_1)->ShowWindow(show3);
	GetDlgItem(IDC_PREP_2)->ShowWindow(show3);
	GetDlgItem(IDC_PREP_WITHOUT)->ShowWindow(show3);
	GetDlgItem(IDC_INPUT_PREP)->ShowWindow(show3);
	GetDlgItem(IDC_NUM_LETTER)->ShowWindow(show3);
	GetDlgItem(IDC_SPIN_LETTER)->ShowWindow(show3);
	GetDlgItem(IDC_SPIN_LETTER_2)->ShowWindow(show3);
	GetDlgItem(IDC_MAX_BETTER)->ShowWindow(show3);
	GetDlgItem(IDC_ONOFF_2)->ShowWindow(show3);
	GetDlgItem(IDC_STATIC_35)->ShowWindow(show3);
	GetDlgItem(IDC_STATIC_36)->ShowWindow(show3);
	GetDlgItem(IDC_STATIC_37)->ShowWindow(show3);
	GetDlgItem(IDC_STATIC_38)->ShowWindow(show3);
	GetDlgItem(IDC_STATIC_44)->ShowWindow(show3);

	GetDlgItem(IDC_STATIC_41)->ShowWindow(show4);
	GetDlgItem(IDC_SPECTRUM_LIST)->ShowWindow(show4);
	GetDlgItem(IDC_EXCLUDE)->ShowWindow(show4);
	GetDlgItem(IDC_PLOT)->ShowWindow(show4);
	GetDlgItem(IDC_CALIBVALID)->ShowWindow(show4);
	GetDlgItem(IDC_SORT_2)->ShowWindow(show4);
	GetDlgItem(IDC_STATIC_42)->ShowWindow(show4);
	GetDlgItem(IDC_STATIC_43)->ShowWindow(show4);

	GetDlgItem(IDC_CRITERION_SEC)->ShowWindow(show5);
	GetDlgItem(IDC_CRITERION_SECV)->ShowWindow(show5);
	GetDlgItem(IDC_CRITERION_SECSECV)->ShowWindow(show5);
	GetDlgItem(IDC_CRITERION_SEV)->ShowWindow(show5);
	GetDlgItem(IDC_CRITERION_R2SEC)->ShowWindow(show5);
	GetDlgItem(IDC_CRITERION_R2SECV)->ShowWindow(show5);
	GetDlgItem(IDC_CRITERION_F)->ShowWindow(show5);
	GetDlgItem(IDC_CRITERION_ERR)->ShowWindow(show5);
	GetDlgItem(IDC_CRITERION_SDV)->ShowWindow(show5);
	GetDlgItem(IDC_CRITERION_R2SEV)->ShowWindow(show5);
	GetDlgItem(IDC_CRITERION_K1)->ShowWindow(show5);
	GetDlgItem(IDC_CRITERION_K2)->ShowWindow(show5);
	GetDlgItem(IDC_CRITERION_K3)->ShowWindow(show5);
	GetDlgItem(IDC_CRITERION_K4)->ShowWindow(show5);
	GetDlgItem(IDC_CRITERION_K5)->ShowWindow(show5);
	GetDlgItem(IDC_CRITERION_K6)->ShowWindow(show5);
	GetDlgItem(IDC_CRITERION_K7)->ShowWindow(show5);
	GetDlgItem(IDC_STATIC_16)->ShowWindow(show5);
	GetDlgItem(IDC_STATIC_11)->ShowWindow(show5);
	GetDlgItem(IDC_OPTIM_TEST)->ShowWindow(show5);
	GetDlgItem(IDC_ONOFF_5)->ShowWindow(show5);

	int show_exc = (m_vTab == C_OPT_PAGE_CRIT && m_vTest == C_OPTIM_TEST_EXCLUDE) ? SW_SHOW : SW_HIDE;
	int show_out = (m_vTab == C_OPT_PAGE_CRIT && m_vTest == C_OPTIM_TEST_OUTS) ? SW_SHOW : SW_HIDE;

	GetDlgItem(IDC_STATIC_47)->ShowWindow(show_exc);
	GetDlgItem(IDC_STATIC_21)->ShowWindow(show_exc);
	GetDlgItem(IDC_OPTIM_CRITERION)->ShowWindow(show_exc);
	GetDlgItem(IDC_STATIC_50)->ShowWindow(show_out);
	GetDlgItem(IDC_OUTS_CHECK_MAH)->ShowWindow(show_out);
	GetDlgItem(IDC_OUTS_CHECK_SEC)->ShowWindow(show_out);
	GetDlgItem(IDC_OUTS_CHECK_SECV)->ShowWindow(show_out);
	GetDlgItem(IDC_OUTS_CHECK_BAD)->ShowWindow(show_out);
	GetDlgItem(IDC_OUTS_CHECK_BOUND)->ShowWindow(show_out);
	GetDlgItem(IDC_OUTS_MAH)->ShowWindow(show_out);
	GetDlgItem(IDC_OUTS_SEC)->ShowWindow(show_out);
	GetDlgItem(IDC_OUTS_SECV)->ShowWindow(show_out);
	GetDlgItem(IDC_OUTS_BOUND)->ShowWindow(show_out);
	GetDlgItem(IDC_STATIC_48)->ShowWindow(show_out);
	GetDlgItem(IDC_STATIC_49)->ShowWindow(show_out);
	GetDlgItem(IDC_STR_UNIT)->ShowWindow(show_out);

	EnableFields();
}

void COptimModelDlg2::EnableFields()
{
	bool closed = (isTmpl && iStep > 0);
	bool hqo = (m_vModType == C_MOD_MODEL_HSO);
	bool hqoharm = (hqo && m_vNHarmType != C_OPTIM_HQO_PNT);
	bool hqonln = (hqo && m_vKfnlType != C_OPTIM_HQO_LIN);

	CTransferData* pTrans = ParentPrj->GetTransfer(StartModel->GetTransName());
	bool chk1 = (pTrans->GetType() != C_TRANS_TYPE_NORMAL);

	GetDlgItem(IDC_STATIC_6)->EnableWindow(chk1);
	GetDlgItem(IDC_SPECTRUM_TYPE)->EnableWindow(chk1);

	GetDlgItem(IDC_STATIC_2)->EnableWindow(!closed);
	GetDlgItem(IDC_STATIC_4)->EnableWindow(!m_vOperationOnOff && !hqo);
	GetDlgItem(IDC_STATIC_5)->EnableWindow(!m_vOperationOnOff && !hqo);
	GetDlgItem(IDC_OPTIM_NCOMP_MEAN)->EnableWindow(!closed);
	GetDlgItem(IDC_OPTIM_NCOMP_PLUS)->EnableWindow(!m_vOperationOnOff && !hqo);
	GetDlgItem(IDC_OPTIM_NCOMP_STEP)->EnableWindow(!m_vOperationOnOff && !hqo);

	GetDlgItem(IDC_STATIC_7)->EnableWindow(hqo);
	GetDlgItem(IDC_STATIC_9)->EnableWindow(!m_vOperationOnOff && hqo);
	GetDlgItem(IDC_STATIC_10)->EnableWindow(!m_vOperationOnOff && hqo);
	GetDlgItem(IDC_OPTIM_HIPER_MEAN)->EnableWindow(!closed && hqo);
	GetDlgItem(IDC_OPTIM_HIPER_PLUS)->EnableWindow(!m_vOperationOnOff && hqo);
	GetDlgItem(IDC_OPTIM_HIPER_STEP)->EnableWindow(!m_vOperationOnOff && hqo);

	GetDlgItem(IDC_STATIC_12)->EnableWindow(hqo);
	GetDlgItem(IDC_STATIC_14)->EnableWindow(!m_vOperationOnOff && hqoharm);
	GetDlgItem(IDC_STATIC_15)->EnableWindow(!m_vOperationOnOff && hqoharm);
	GetDlgItem(IDC_STATIC_13)->EnableWindow(!m_vOperationOnOff && hqo);
	GetDlgItem(IDC_OPTIM_NHARM_MEAN)->EnableWindow(!closed && hqoharm);
	GetDlgItem(IDC_OPTIM_NHARM_PLUS)->EnableWindow(!m_vOperationOnOff && hqoharm);
	GetDlgItem(IDC_OPTIM_NHARM_STEP)->EnableWindow(!m_vOperationOnOff && hqoharm);
	GetDlgItem(IDC_OPTIM_NHARM_TYPE)->EnableWindow(hqo);

	GetDlgItem(IDC_STATIC_17)->EnableWindow(hqo);
	GetDlgItem(IDC_STATIC_19)->EnableWindow(!m_vOperationOnOff && hqonln);
	GetDlgItem(IDC_STATIC_20)->EnableWindow(!m_vOperationOnOff && hqonln);
	GetDlgItem(IDC_STATIC_18)->EnableWindow(!m_vOperationOnOff && hqo);
	GetDlgItem(IDC_OPTIM_KFNL_MEAN)->EnableWindow(!closed && hqonln);
	GetDlgItem(IDC_OPTIM_KFNL_PLUS)->EnableWindow(!m_vOperationOnOff && hqonln);
	GetDlgItem(IDC_OPTIM_KFNL_STEP)->EnableWindow(!m_vOperationOnOff && hqonln);
	GetDlgItem(IDC_OPTIM_KFNL_TYPE)->EnableWindow(hqo);

	GetDlgItem(IDC_OPTIM_LEFT_MEAN)->EnableWindow(!closed);
	GetDlgItem(IDC_OPTIM_LEFT_PLUS)->EnableWindow(!m_vOperationOnOff);
	GetDlgItem(IDC_OPTIM_LEFT_STEP)->EnableWindow(!m_vOperationOnOff);
	GetDlgItem(IDC_OPTIM_RIGHT_MEAN)->EnableWindow(!closed);
	GetDlgItem(IDC_OPTIM_RIGHT_PLUS)->EnableWindow(!m_vOperationOnOff);
	GetDlgItem(IDC_OPTIM_RIGHT_STEP)->EnableWindow(!m_vOperationOnOff);

	int Nall = GetNPointsShow();
	int Nexc = GetNExcPointsShow();
	GetDlgItem(IDC_ADD)->EnableWindow(!closed && Nexc < Nall);
	GetDlgItem(IDC_ADD_ALL)->EnableWindow(!closed && Nexc < Nall);
	GetDlgItem(IDC_REMOVE)->EnableWindow(!closed && Nexc > 0);
	GetDlgItem(IDC_REMOVE_ALL)->EnableWindow(!closed && Nexc > 0);

	bool bprep = (!m_vOperationOnOff && !m_vUseBest);
	GetDlgItem(IDC_PREP_A)->EnableWindow(bprep && PrePrep.IsEmpty());
	GetDlgItem(IDC_PREP_B)->EnableWindow(bprep && !cIsPrepInStr(PrePrep, C_PREPROC_B));
	GetDlgItem(IDC_PREP_M)->EnableWindow(bprep && !cIsPrepInStr(PrePrep, C_PREPROC_M));
	GetDlgItem(IDC_PREP_D)->EnableWindow(bprep && !cIsPrepInStr(PrePrep, C_PREPROC_D));
	GetDlgItem(IDC_PREP_C)->EnableWindow(bprep && !cIsPrepInStr(PrePrep, C_PREPROC_C));
	GetDlgItem(IDC_PREP_N)->EnableWindow(bprep && !cIsPrepInStr(PrePrep, C_PREPROC_N));
	GetDlgItem(IDC_PREP_1)->EnableWindow(bprep && !cIsPrepInStr(PrePrep, C_PREPROC_1) && !cIsPrepInStr(PrePrep, C_PREPROC_2));
	GetDlgItem(IDC_PREP_2)->EnableWindow(bprep && !cIsPrepInStr(PrePrep, C_PREPROC_2) && !cIsPrepInStr(PrePrep, C_PREPROC_1));
	GetDlgItem(IDC_PREP_WITHOUT)->EnableWindow(bprep);
	GetDlgItem(IDC_INPUT_PREP)->EnableWindow(!m_vUseBest);
	GetDlgItem(IDC_STATIC_37)->EnableWindow(!m_vOperationOnOff);
	GetDlgItem(IDC_NUM_LETTER)->EnableWindow(!m_vOperationOnOff);
	GetDlgItem(IDC_SPIN_LETTER)->EnableWindow(!m_vOperationOnOff);

	bool bstep = CheckStepNum(iStep);
	bool bres = (bstep && OptimRes[iStep].GetNModels() > 0);
	GetDlgItem(IDC_STATIC_44)->EnableWindow(!m_vOperationOnOff && bres);
	GetDlgItem(IDC_ONOFF_2)->EnableWindow(!m_vOperationOnOff && bres);
	GetDlgItem(IDC_MAX_BETTER)->EnableWindow(m_vUseBest && !m_vOperationOnOff && bres);
	GetDlgItem(IDC_SPIN_LETTER_2)->EnableWindow(m_vUseBest && !m_vOperationOnOff && bres);

	bool en_exc = (m_vOperationOnOff && m_vTest == C_OPTIM_TEST_EXCLUDE);
	bool en_out = (m_vOperationOnOff && m_vTest == C_OPTIM_TEST_OUTS);
	bool en_bound = (en_out && (m_vIsMah || m_vIsSEC || m_vIsSECV || m_vIsBad));

	GetDlgItem(IDC_STATIC_47)->EnableWindow(en_exc);
	GetDlgItem(IDC_STATIC_21)->EnableWindow(en_exc);
	GetDlgItem(IDC_OPTIM_CRITERION)->EnableWindow(en_exc);
	GetDlgItem(IDC_STATIC_50)->EnableWindow(en_out);
	GetDlgItem(IDC_OUTS_CHECK_MAH)->EnableWindow(en_out);
	GetDlgItem(IDC_OUTS_CHECK_SEC)->EnableWindow(en_out);
	GetDlgItem(IDC_OUTS_CHECK_SECV)->EnableWindow(en_out);
	GetDlgItem(IDC_OUTS_CHECK_BAD)->EnableWindow(en_out);
	GetDlgItem(IDC_OUTS_CHECK_BOUND)->EnableWindow(en_bound);
	GetDlgItem(IDC_OUTS_MAH)->EnableWindow(en_out);
	GetDlgItem(IDC_OUTS_SEC)->EnableWindow(en_out);
	GetDlgItem(IDC_OUTS_SECV)->EnableWindow(en_out);
	GetDlgItem(IDC_OUTS_BOUND)->EnableWindow(en_bound);
	GetDlgItem(IDC_STATIC_48)->EnableWindow(en_out);
	GetDlgItem(IDC_STATIC_49)->EnableWindow(en_out);
	GetDlgItem(IDC_STR_UNIT)->EnableWindow(en_bound);

	GetDlgItem(IDC_OPTIM_TEST)->EnableWindow(m_vOperationOnOff);

	bool bsort = (bstep && OptimRes[iStep].GetNCrits() > 0);
	GetDlgItem(IDC_STATIC_40)->EnableWindow(bsort && bres);
	GetDlgItem(IDC_SORT)->EnableWindow(bsort && bres);

	int Ind = m_listResult.GetNextItem(-1, LVNI_SELECTED);
	bool bmod = (bres && Ind >= 0 && Ind < OptimRes[iStep].GetNModels());
	GetDlgItem(IDC_SETTARGET)->EnableWindow(!isTmpl && bmod);

	GetDlgItem(IDC_BACK)->EnableWindow(iStep > 0);
	GetDlgItem(IDC_FORWARD)->EnableWindow(iStep < nSteps - 1);

	bool chkacc = (!isTmpl) ? bres : (nSteps > 1);
	GetDlgItem(IDC_ACCEPT)->EnableWindow(chkacc);

	GetDlgItem(IDC_EXCLUDE)->EnableWindow(!isTmpl);

	int cur_crit = GetMainCrit();
	bool bsev = (int(OptimValid.size() > 0));

	GetDlgItem(IDC_CRITERION_SEC)->EnableWindow(!(en_exc && cIsCritDerivated(cur_crit, C_CRIT_SEC)));
	GetDlgItem(IDC_CRITERION_R2SEC)->EnableWindow(m_vCritSEC && !(en_exc && cur_crit == C_CRIT_R2SEC));
	GetDlgItem(IDC_CRITERION_SECV)->EnableWindow(!(en_exc && cIsCritDerivated(cur_crit, C_CRIT_SECV)));
	GetDlgItem(IDC_CRITERION_R2SECV)->EnableWindow(m_vCritSECV && !(en_exc && cur_crit == C_CRIT_R2SECV));
	GetDlgItem(IDC_CRITERION_F)->EnableWindow(m_vCritSECV && !(en_exc && cur_crit == C_CRIT_F));
	GetDlgItem(IDC_CRITERION_SEV)->EnableWindow(bsev && !(en_exc && cIsCritDerivated(cur_crit, C_CRIT_SEV)));
	GetDlgItem(IDC_CRITERION_ERR)->EnableWindow(m_vCritSEV && !(en_exc && cur_crit == C_CRIT_ERR));
	GetDlgItem(IDC_CRITERION_SDV)->EnableWindow(m_vCritSEV && !(en_exc && cur_crit == C_CRIT_SDV));
	GetDlgItem(IDC_CRITERION_R2SEV)->EnableWindow(m_vCritSEV && !(en_exc && cur_crit == C_CRIT_R2SEV));
	GetDlgItem(IDC_CRITERION_K1)->EnableWindow(m_vCritSEC && m_vCritSEV && !(en_exc && cur_crit == C_CRIT_K1));
	GetDlgItem(IDC_CRITERION_K2)->EnableWindow(m_vCritSEC && m_vCritSECV && !(en_exc && cur_crit == C_CRIT_K2));
	GetDlgItem(IDC_CRITERION_K3)->EnableWindow(m_vCritSEC && m_vCritSEV && !(en_exc && cur_crit == C_CRIT_K3));
	GetDlgItem(IDC_CRITERION_K4)->EnableWindow(m_vCritSEC && m_vCritSECV && !(en_exc && cur_crit == C_CRIT_K4));
	GetDlgItem(IDC_CRITERION_K5)->EnableWindow(m_vCritSEC && m_vCritSECV && !(en_exc && cur_crit == C_CRIT_K5));
	GetDlgItem(IDC_CRITERION_K6)->EnableWindow(m_vCritSEC && !(en_exc && cur_crit == C_CRIT_K6));
	GetDlgItem(IDC_CRITERION_K7)->EnableWindow(m_vCritSEC && m_vCritSECV && m_vCritSEV && !(en_exc && cur_crit == C_CRIT_K7));
	GetDlgItem(IDC_CRITERION_SECSECV)->EnableWindow(GetDlgItem(IDC_CRITERION_SEC)->IsWindowEnabled() && GetDlgItem(IDC_CRITERION_SECV)->IsWindowEnabled());

	RedrawWindow();
}

void COptimModelDlg2::OnHelp()
{
	int IdHelp = DLGID_MAIN;
	switch(Mode)
	{
	case C_OPT_MODE_SINGLE:  IdHelp = DLGID_MODEL_OPTIM;  break;
	case C_OPT_MODE_BYTMPL:  IdHelp = DLGID_MODEL_OPTIM_BYTMPL;  break;
	case C_OPT_MODE_TMPLCREATE:  IdHelp = DLGID_MODEL_TEMPLATE_CREATE;  break;
	case C_OPT_MODE_TMPLEDIT:  IdHelp = DLGID_MODEL_TEMPLATE_CHANGE;  break;
	}

	pFrame->ShowHelp(IdHelp);
}
