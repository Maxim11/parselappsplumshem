#include "stdafx.h"

#include "Parcel.h"
#include "ParcelView.h"


void CParcelView::SetViewMode(HTREEITEM item, bool bFromScratch)
{
	int i, j, k, N;
	CString s, s1, s2;
	BranchInfo Info;
	int mode = GetBranchInfo(item, Info);
	int iList = 0;

	if(mode == TIM_DEVICE)
	{
		CDeviceData *Dev = Info.Device;

		GetDlgItem(IDC_VIEW_DEVICE_NAME)->ShowWindow(SW_SHOW);
		s.Format(ParcelLoadString(STRID_VIEW_DEV_NAME), Dev->GetName());
		GetDlgItem(IDC_VIEW_DEVICE_NAME)->SetFont(&FontHeader);
		GetDlgItem(IDC_VIEW_DEVICE_NAME)->SetWindowText(s);

		GetDlgItem(IDC_VIEW_DEVICE_PROJECTS)->ShowWindow(SW_HIDE);
		N = Dev->GetNProjects();
		s.Format(ParcelLoadString(STRID_VIEW_DEV_PROJECTS), N);
		GetDlgItem(IDC_VIEW_DEVICE_PROJECTS)->SetWindowText(s);
		GetDlgItem(IDC_VIEW_DEVICE_PROJECTS_LIST)->ShowWindow(SW_SHOW);
		m_list_DevicePrj.DeleteAllItems();
		for(i=0; i<N; i++)
		{
			CProjectData *Prj = Dev->GetProject(i);

			m_list_DevicePrj.InsertItem(i, Prj->GetName());
			s.Format(cFmt1, Prj->GetNMethods());
			m_list_DevicePrj.SetItemText(i, 1, s);
			s.Format(cFmt1, Prj->GetNModels());
			m_list_DevicePrj.SetItemText(i, 2, s);
			s.Format(cFmt1, Prj->GetNParams());
			m_list_DevicePrj.SetItemText(i, 3, s);
			s.Format(cFmt1_1, Prj->GetNSamples(), Prj->GetNSpectra());
			m_list_DevicePrj.SetItemText(i, 4, s);
			s.Format(cFmt1, Prj->GetNTransfers() - 1);
			m_list_DevicePrj.SetItemText(i, 5, s);
		}
		for(i=0; i<6; i++)
			m_list_DevicePrj.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	}
	else
	{
		GetDlgItem(IDC_VIEW_DEVICE_NAME)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_VIEW_DEVICE_PROJECTS)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_VIEW_DEVICE_PROJECTS_LIST)->ShowWindow(SW_HIDE);
	}



	if(mode == TIM_PROJECT)
	{
		CProjectData *Prj = Info.Project;
		CDeviceData *Dev = Info.Device;
		int DType = Dev->GetType();

		GetDlgItem(IDC_VIEW_PROJECT_NAME)->ShowWindow(SW_SHOW);
		s.Format(ParcelLoadString(STRID_VIEW_PRJ_NAME), Prj->GetName());
		GetDlgItem(IDC_VIEW_PROJECT_NAME)->SetFont(&FontHeader);
		GetDlgItem(IDC_VIEW_PROJECT_NAME)->SetWindowText(s);

		GetDlgItem(IDC_VIEW_PROJECT_SPECRANGE)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_VIEW_PROJECT_SPECRANGE)->SetWindowText(ParcelLoadString(STRID_VIEW_PRJ_SPECRANGE));
		GetDlgItem(IDC_VIEW_PROJECT_SPEC_LIST)->ShowWindow(SW_SHOW);

		m_list_ProjectSpec.DeleteAllItems();
		iList = 0;

		m_list_ProjectSpec.InsertItem(iList, ParcelLoadString(STRID_VIEW_PRJ_LOWRANGE));
		s.Format(cFmt1, Prj->GetLowLimSpecRange());
		m_list_ProjectSpec.SetItemText(iList, 1, s);
		iList++;

		m_list_ProjectSpec.InsertItem(iList, ParcelLoadString(STRID_VIEW_PRJ_UPPERRANGE));
		s.Format(cFmt1, Prj->GetUpperLimSpecRange());
		m_list_ProjectSpec.SetItemText(iList, 1, s);
		iList++;

		if(DType != C_DEV_TYPE_IMIT)
		{
			GetDlgItem(IDC_VIEW_PROJECT_SUBJCANAL)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_VIEW_PROJECT_SUBJCANAL)->SetWindowText(ParcelLoadString(STRID_VIEW_PRJ_SUBJCANAL));
			GetDlgItem(IDC_VIEW_PROJECT_SUBJ_LIST)->ShowWindow(SW_SHOW);

			m_list_ProjectSubj.DeleteAllItems();
			iList = 0;

			m_list_ProjectSubj.InsertItem(iList, ParcelLoadString(STRID_VIEW_PRJ_SUBJRESOL));
			s.Format(cFmt42, Prj->GetSubCanalResolutionMean());
			m_list_ProjectSubj.SetItemText(iList, 1, s);
			iList++;

			m_list_ProjectSubj.InsertItem(iList, ParcelLoadString(STRID_VIEW_PRJ_SUBJAPODIZE));
			m_list_ProjectSubj.SetItemText(iList, 1, cGetApodizeTypeName(Prj->GetSubCanalApodization()));
			iList++;

			m_list_ProjectSubj.InsertItem(iList, ParcelLoadString(STRID_VIEW_PRJ_FILLING));
			m_list_ProjectSubj.SetItemText(iList, 1, Prj->GetZeroFillingAsStr());
			iList++;
		}
		else
		{
			GetDlgItem(IDC_VIEW_PROJECT_SUBJCANAL)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_VIEW_PROJECT_SUBJ_LIST)->ShowWindow(SW_HIDE);
		}

		if(DType == C_DEV_TYPE_40 || DType == C_DEV_TYPE_20 || DType == C_DEV_TYPE_10M || DType == C_DEV_TYPE_12 || DType == C_DEV_TYPE_12M)
		{
			GetDlgItem(IDC_VIEW_PROJECT_REFCANAL)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_VIEW_PROJECT_REFCANAL)->SetWindowText(ParcelLoadString(STRID_VIEW_PRJ_REFCANAL));
			GetDlgItem(IDC_VIEW_PROJECT_REF_LIST)->ShowWindow(SW_SHOW);

			m_list_ProjectRef.DeleteAllItems();
			iList = 0;

			m_list_ProjectRef.InsertItem(iList, ParcelLoadString(STRID_VIEW_PRJ_USEREF));
			m_list_ProjectRef.SetItemText(iList, 1, Prj->GetUseRefCanalStr());
			iList++;

			if(Prj->IsUseRefCanal())
			{
				m_list_ProjectRef.InsertItem(iList, ParcelLoadString(STRID_VIEW_PRJ_REFRESOL));
				s.Format(cFmt42, Prj->GetRefCanalResolutionMean());
				m_list_ProjectRef.SetItemText(iList, 1, s);
				iList++;

				m_list_ProjectRef.InsertItem(iList, ParcelLoadString(STRID_VIEW_PRJ_REFAPODIZE));
				m_list_ProjectRef.SetItemText(iList, 1, cGetApodizeTypeName(Prj->GetRefCanalApodization()));
				iList++;
			}
		}
		else
		{
			GetDlgItem(IDC_VIEW_PROJECT_REFCANAL)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_VIEW_PROJECT_REF_LIST)->ShowWindow(SW_HIDE);
		}

		if(DType != C_DEV_TYPE_IMIT)
		{
			GetDlgItem(IDC_VIEW_PROJECT_MEASPLAN)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_VIEW_PROJECT_MEASPLAN)->SetWindowText(ParcelLoadString(STRID_VIEW_PRJ_MEASPLAN));
			GetDlgItem(IDC_VIEW_PROJECT_PLAN_LIST)->ShowWindow(SW_SHOW);

			m_list_ProjectPlan.DeleteAllItems();
			iList = 0;

			m_list_ProjectPlan.InsertItem(iList, ParcelLoadString(STRID_VIEW_PRJ_NSCANSUB));
			s.Format(cFmt1, Prj->GetNScansSubsample());
			m_list_ProjectPlan.SetItemText(iList, 1, s);
			iList++;

			m_list_ProjectPlan.InsertItem(iList, ParcelLoadString(STRID_VIEW_PRJ_NMEASURESUB));
			s.Format(cFmt1, Prj->GetNMeasuresSubsample());
			m_list_ProjectPlan.SetItemText(iList, 1, s);
			iList++;

			m_list_ProjectPlan.InsertItem(iList, ParcelLoadString(STRID_VIEW_PRJ_NSCANSTD));
			s.Format(cFmt1, Prj->GetNScansStandart());
			m_list_ProjectPlan.SetItemText(iList, 1, s);
			iList++;

			m_list_ProjectPlan.InsertItem(iList, ParcelLoadString(STRID_VIEW_PRJ_NMEASURESTD));
			m_list_ProjectPlan.SetItemText(iList, 1, cGetMeasureTypeName(Prj->GetNMeasuresStandart()));
			iList++;

			if(DType == C_DEV_TYPE_40 || DType == C_DEV_TYPE_12M)
			{
				m_list_ProjectPlan.InsertItem(iList, ParcelLoadString(STRID_VIEW_PRJ_LENGTH));
				s.Format(cFmt31, Prj->GetBathLength());
				m_list_ProjectPlan.SetItemText(iList, 1, s);
				iList++;
			}
		}
		else
		{
			GetDlgItem(IDC_VIEW_PROJECT_MEASPLAN)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_VIEW_PROJECT_PLAN_LIST)->ShowWindow(SW_HIDE);
		}

		if(DType == C_DEV_TYPE_40 || DType == C_DEV_TYPE_12M)
		{
			GetDlgItem(IDC_VIEW_PROJECT_TEMPLIMIT)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_VIEW_PROJECT_TEMPLIMIT)->SetWindowText(ParcelLoadString(STRID_VIEW_PRJ_SAMPLETEMP));
			GetDlgItem(IDC_VIEW_PROJECT_TEMP_LIST)->ShowWindow(SW_SHOW);

			m_list_ProjectTemp.DeleteAllItems();
			iList = 0;

			m_list_ProjectTemp.InsertItem(iList, ParcelLoadString(STRID_VIEW_PRJ_LOWTEMP));
			s.Format(cFmt1, Prj->GetLowLimTemp());
			m_list_ProjectTemp.SetItemText(iList, 1, s);
			iList++;

			m_list_ProjectTemp.InsertItem(iList, ParcelLoadString(STRID_VIEW_PRJ_UPPERTEMP));
			s.Format(cFmt1, Prj->GetUpperLimTemp());
			m_list_ProjectTemp.SetItemText(iList, 1, s);
			iList++;

			m_list_ProjectTemp.InsertItem(iList, ParcelLoadString(STRID_VIEW_PRJ_DIFFTEMP));
			s.Format(cFmt1, Prj->GetMaxDiffTemp());
			m_list_ProjectTemp.SetItemText(iList, 1, s);
			iList++;
		}
		else
		{
			GetDlgItem(IDC_VIEW_PROJECT_TEMPLIMIT)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_VIEW_PROJECT_TEMP_LIST)->ShowWindow(SW_HIDE);
		}

		for(i=0; i<2; i++)
			m_list_ProjectSpec.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
		for(i=0; i<2; i++)
			m_list_ProjectTemp.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
		for(i=0; i<2; i++)
			m_list_ProjectSubj.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
		for(i=0; i<2; i++)
			m_list_ProjectRef.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
		for(i=0; i<2; i++)
			m_list_ProjectPlan.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	}
	else
	{
		GetDlgItem(IDC_VIEW_PROJECT_NAME)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_VIEW_PROJECT_SPEC_LIST)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_VIEW_PROJECT_TEMP_LIST)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_VIEW_PROJECT_SUBJ_LIST)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_VIEW_PROJECT_REF_LIST)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_VIEW_PROJECT_PLAN_LIST)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_VIEW_PROJECT_SPECRANGE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_VIEW_PROJECT_TEMPLIMIT)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_VIEW_PROJECT_SUBJCANAL)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_VIEW_PROJECT_REFCANAL)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_VIEW_PROJECT_MEASPLAN)->ShowWindow(SW_HIDE);
	}



	if(mode == TIM_PARAMSET)
	{
		CProjectData *Prj = Info.Project;

		GetDlgItem(IDC_VIEW_PARAMS_HEAD)->ShowWindow(SW_SHOW);
		s.Format(ParcelLoadString(STRID_VIEW_PARSET_HEAD), Prj->GetName());
		GetDlgItem(IDC_VIEW_PARAMS_HEAD)->SetFont(&FontHeader);
		GetDlgItem(IDC_VIEW_PARAMS_HEAD)->SetWindowText(s);
		GetDlgItem(IDC_VIEW_PARAMS_TXT)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_VIEW_PARAMS_TXT)->SetWindowText(ParcelLoadString(STRID_VIEW_PARSET_TXT));
		GetDlgItem(IDC_VIEW_PARAMS_LIST)->ShowWindow(SW_SHOW);

		N = Prj->GetNParams();
		int sel = m_list_Params.GetNextItem(-1, LVNI_SELECTED);
		m_list_Params.DeleteAllItems();
		for(i=0; i<N; i++)
		{
			CParamData *Param = Prj->GetParam(i);
			m_list_Params.InsertItem(i, Param->GetName());
			s = Param->GetStdMoistureStr();
			m_list_Params.SetItemText(i, 1, s);
			m_list_Params.SetItemText(i, 2, Param->GetUnit());
			s.Format(cFmt1, Param->GetIFormat());
			m_list_Params.SetItemText(i, 3, s);
		}
		if(N > 0)
		{
			if(sel < 0 || sel >= N)
				sel = 0;
			m_list_Params.SetItemState(sel, LVIS_SELECTED, LVIS_SELECTED);

			FillParamsLists(Prj);
		}
		else
		{
			GetDlgItem(IDC_VIEW_PARAMSSAM)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_VIEW_PARAMSSAM_LIST)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_VIEW_PARAMSMOD)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_VIEW_PARAMSMOD_LIST)->ShowWindow(SW_HIDE);
		}

		for(i=0; i<4; i++)
			m_list_Params.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
		for(i=0; i<2; i++)
			m_list_ParamsSam.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	}
	else
	{
		GetDlgItem(IDC_VIEW_PARAMS_HEAD)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_VIEW_PARAMS_TXT)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_VIEW_PARAMS_LIST)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_VIEW_PARAMSSAM)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_VIEW_PARAMSSAM_LIST)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_VIEW_PARAMSMOD)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_VIEW_PARAMSMOD_LIST)->ShowWindow(SW_HIDE);
	}



	if(mode == TIM_SAMPLESET)
	{
		CProjectData *Prj = Info.Project;

		GetDlgItem(IDC_VIEW_SAMPLESET_HEAD)->ShowWindow(SW_SHOW);
		s.Format(ParcelLoadString(STRID_VIEW_SAMSET_HEAD), Prj->GetName());
		GetDlgItem(IDC_VIEW_SAMPLESET_HEAD)->SetFont(&FontHeader);
		GetDlgItem(IDC_VIEW_SAMPLESET_HEAD)->SetWindowText(s);
		GetDlgItem(IDC_VIEW_SAMPLESET_SETS)->ShowWindow(SW_SHOW);
		s.Format(ParcelLoadString(STRID_VIEW_SAMSET_TRANSES), Prj->GetNTransfers(), Prj->GetNAllSamples());
		GetDlgItem(IDC_VIEW_SAMPLESET_SETS)->SetWindowText(s);
		GetDlgItem(IDC_VIEW_SAMPLESET_SETS_LIST)->ShowWindow(SW_SHOW);

		N = Prj->GetNTransfers();
		m_list_SampleSets.DeleteAllItems();
		for(i=0; i<N; i++)
		{
			CTransferData* Trans = Prj->GetTransfer(i);
			s = (Trans->GetType() != C_TRANS_TYPE_OWN) ? Trans->GetName() : ParcelLoadString(STRID_TREE_SAMOWN);
			m_list_SampleSets.InsertItem(i, s);
			s = cGetTransTypeName(Trans->GetType());
			m_list_SampleSets.SetItemText(i, 1, s);
			s.Format(cFmt1_1, Trans->GetNSamples(), Trans->GetNSpectra());
			m_list_SampleSets.SetItemText(i, 2, s);
			s.Format(_T("%1d - %1d"), Trans->GetLowLimSpec(), Trans->GetUpperLimSpec());
			m_list_SampleSets.SetItemText(i, 3, s);
			if(i > 0)
			{
				m_list_SampleSets.SetItemText(i, 4, Trans->GetMasterDevName());
				m_list_SampleSets.SetItemText(i, 5, Trans->GetAllPreprocAsStr());
				s = Trans->GetDate().Format(cFmtDate);
				m_list_SampleSets.SetItemText(i, 6, s);
			}
		}
		for(i=0; i<7; i++)
			m_list_SampleSets.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	}
	else
	{
		GetDlgItem(IDC_VIEW_SAMPLESET_HEAD)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_VIEW_SAMPLESET_SETS)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_VIEW_SAMPLESET_SETS_LIST)->ShowWindow(SW_HIDE);
	}



	if(mode == TIM_SAMPLETRANS || mode == TIM_SAMPLEOWN)
	{
		CProjectData *Prj = Info.Project;
		CTransferData *Trans = Info.Trans;

		GetDlgItem(IDC_VIEW_SAMPLETRANS_HEAD)->ShowWindow(SW_SHOW);
		if(mode == TIM_SAMPLEOWN)
		{
			s.Format(ParcelLoadString(STRID_VIEW_TRANS_OWNHEAD), Prj->GetName());
		}
		else
		{
			s.Format(ParcelLoadString(STRID_VIEW_TRANS_HEAD), Trans->GetName());
		}
		GetDlgItem(IDC_VIEW_SAMPLETRANS_HEAD)->SetFont(&FontHeader);
		GetDlgItem(IDC_VIEW_SAMPLETRANS_HEAD)->SetWindowText(s);
		GetDlgItem(IDC_VIEW_SAMPLETRANS_SAMPLES)->ShowWindow(SW_SHOW);
		s.Format(ParcelLoadString(STRID_VIEW_TRANS_SAMPLES), Trans->GetNSamples(), Trans->GetNSpectra());
		GetDlgItem(IDC_VIEW_SAMPLETRANS_SAMPLES)->SetWindowText(s);
		GetDlgItem(IDC_VIEW_SAMPLETRANS_SAMPLES_LIST)->ShowWindow(SW_SHOW);

		N = Trans->GetNSamples();
		int sel = m_list_SamplesTrans.GetNextItem(-1, LVNI_SELECTED);

		if(bFromScratch)
		{
			m_list_SamplesTrans.DeleteAllItems();
			CreateSamplesTransList(Trans);
		}

		for(i=0; i<N; i++)
		{
			CSampleData *Sam = Trans->GetSample(i);
			if(mode == TIM_SAMPLETRANS)
			{
				s.Format(cFmtAstStr, Sam->GetName());
			}
			else
			{
				s = Sam->GetName();
			}

			if(bFromScratch)
			{
				m_list_SamplesTrans.InsertItem(i, s);
			}
			else
			{
				m_list_SamplesTrans.SetItemText(i, 0, s);
			}

			m_list_SamplesTrans.SetItemText(i, 1, Sam->GetIden2());
			m_list_SamplesTrans.SetItemText(i, 2, Sam->GetIden3());

			s.Format(cFmt1_1, Sam->GetNSpectraUsed(), Sam->GetNSpectra());
			m_list_SamplesTrans.SetItemText(i, 3, s);

			if(Sam->GetNSpectra())
			{
				CTime t = Sam->GetLastSpecDate();
				s = t.Format(cFmtDate2);
			}
			else
			{
				s = L"--";
			}
			m_list_SamplesTrans.SetItemText(i, m_nLSampTimeCol, s);

			for(j=0; j<Prj->GetNParams(); j++) // �������!
			{
				s = Sam->GetReferenceDataStr(j);
				m_list_SamplesTrans.SetItemText(i, j + m_nLSampParStart, s);
			}
		}

		if(N > 0)
		{
			if(sel < 0 || sel >= N)
			{
				sel = 0;
			}
			
			m_list_SamplesTrans.SetItemState(sel, LVIS_SELECTED, LVIS_SELECTED);

			CSampleData *Sam = Trans->GetSample(sel);
			FillSpectraTransList(Sam);
		}
		else
		{
			GetDlgItem(IDC_VIEW_SAMPLETRANS_SPECTRA)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_VIEW_SAMPLETRANS_SPECTRA_LIST)->ShowWindow(SW_HIDE);
		}

		if(mode == TIM_SAMPLETRANS && Trans->GetType() != C_TRANS_TYPE_SST)
		{
			GetDlgItem(IDC_VIEW_SAMPLETRANS_SETTINGS)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_VIEW_SAMPLETRANS_SETTINGS)->SetWindowText(ParcelLoadString(STRID_VIEW_TRANS_SETTINGS));
			GetDlgItem(IDC_VIEW_SAMPLETRANS_SETTINGS_LIST)->ShowWindow(SW_SHOW);

			m_list_TransSettings.DeleteAllItems();
			iList = 0;

			m_list_TransSettings.InsertItem(iList, ParcelLoadString(STRID_VIEW_TRANS_TYPE));
			s = cGetTransTypeName(Trans->GetType());
			m_list_TransSettings.SetItemText(iList, 1, s);
			iList++;

			m_list_TransSettings.InsertItem(iList, ParcelLoadString(STRID_VIEW_TRANS_SPECRANGE));
			s.Format(_T("%1d - %1d"), Trans->GetLowLimSpec(), Trans->GetUpperLimSpec());
			m_list_TransSettings.SetItemText(iList, 1, s);
			iList++;

			m_list_TransSettings.InsertItem(iList, ParcelLoadString(STRID_VIEW_TRANS_MASTERDEV));
			m_list_TransSettings.SetItemText(iList, 1, Trans->GetMasterDevName());
			iList++;

			m_list_TransSettings.InsertItem(iList, ParcelLoadString(STRID_VIEW_TRANS_PREPROCS));
			s = Trans->GetAllPreprocAsStr();
			if(s.IsEmpty() && Trans->GetType() == C_TRANS_TYPE_NORMAL)
				s = ParcelLoadString(STRID_VIEW_TRANS_NOPREP);
			m_list_TransSettings.SetItemText(iList, 1, s);
			iList++;

			m_list_TransSettings.InsertItem(iList, ParcelLoadString(STRID_VIEW_TRANS_DATE));
			s = Trans->GetDate().Format(cFmtDate);
			m_list_TransSettings.SetItemText(iList, 1, s);
			iList++;
		}
		else
		{
			GetDlgItem(IDC_VIEW_SAMPLETRANS_SETTINGS)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_VIEW_SAMPLETRANS_SETTINGS_LIST)->ShowWindow(SW_HIDE);
		}

		SetSamplesSortIcons();

		if(bFromScratch)
		{
			for(i=0; i<m_nLSampParStart; i++)
			{
				m_list_SamplesTrans.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
			}
		}

		for(i=0; i<3; i++)
		{
			m_list_SpectraTrans.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
		}

		for(i=0; i<2; i++)
		{
			m_list_TransSettings.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
		}
	}
	else
	{
		GetDlgItem(IDC_VIEW_SAMPLETRANS_HEAD)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_VIEW_SAMPLETRANS_SAMPLES)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_VIEW_SAMPLETRANS_SAMPLES_LIST)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_VIEW_SAMPLETRANS_SPECTRA)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_VIEW_SAMPLETRANS_SPECTRA_LIST)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_VIEW_SAMPLETRANS_SETTINGS)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_VIEW_SAMPLETRANS_SETTINGS_LIST)->ShowWindow(SW_HIDE);
	}



	if(mode == TIM_MODELSET)
	{
		CProjectData *Prj = Info.Project;

		GetDlgItem(IDC_VIEW_MODELSET_HEAD)->ShowWindow(SW_SHOW);
		s.Format(ParcelLoadString(STRID_VIEW_MODSET_HEAD), Prj->GetName());
		GetDlgItem(IDC_VIEW_MODELSET_HEAD)->SetFont(&FontHeader);
		GetDlgItem(IDC_VIEW_MODELSET_HEAD)->SetWindowText(s);

		GetDlgItem(IDC_VIEW_MODELSET_MODELS)->ShowWindow(SW_SHOW);
		s.Format(ParcelLoadString(STRID_VIEW_MODSET_MODELS), Prj->GetNModels());
		GetDlgItem(IDC_VIEW_MODELSET_MODELS)->SetWindowText(s);
		GetDlgItem(IDC_VIEW_MODELSET_MODELS_LIST)->ShowWindow(SW_SHOW);
		N = Info.Project->GetNModels();
		m_list_Models.DeleteAllItems();
		for(i=0; i<N; i++)
		{
			CModelData *Mod = Prj->GetModel(i);
			m_list_Models.InsertItem(i, Mod->GetName());
			m_list_Models.SetItemText(i, 1, cGetAnalysisTypeName(Mod->GetAnalysisType()));
			m_list_Models.SetItemText(i, 2, cGetModelTypeNameShort(Mod->GetModelType()));
			s = Mod->GetDate().Format(cFmtDate);
			m_list_Models.SetItemText(i, 3, s);
		}

		for(i=0; i<4; i++)
			m_list_Models.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	}
	else
	{
		GetDlgItem(IDC_VIEW_MODELSET_HEAD)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_VIEW_MODELSET_MODELS)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_VIEW_MODELSET_MODELS_LIST)->ShowWindow(SW_HIDE);
	}



	if(mode == TIM_MODEL)
	{
		CModelData *Mod = Info.Model;
		CProjectData *Prj = Info.Project;
		bool IsQnt = (Mod->GetAnalysisType() == C_ANALYSIS_QNT);
		bool IsHSO = (IsQnt && Mod->GetModelType() == C_MOD_MODEL_HSO);

		GetDlgItem(IDC_VIEW_MODEL_NAME)->ShowWindow(SW_SHOW);
		s.Format(ParcelLoadString(STRID_VIEW_MOD_NAME), Mod->GetName());
		GetDlgItem(IDC_VIEW_MODEL_NAME)->SetFont(&FontHeader);
		GetDlgItem(IDC_VIEW_MODEL_NAME)->SetWindowText(s);

		GetDlgItem(IDC_VIEW_MODEL_SETTINGS)->ShowWindow(SW_SHOW);
		s = ParcelLoadString(STRID_VIEW_MOD_SETTINGS);
		GetDlgItem(IDC_VIEW_MODEL_SETTINGS)->SetWindowText(s);
		GetDlgItem(IDC_VIEW_MODEL_SETTINGS_LIST)->ShowWindow(SW_SHOW);

		m_list_ModSet.DeleteAllItems();
		iList = 0;
		N = Mod->GetNModInfo();
		for(iList=0; iList<N; iList++)
		{
			SModInfo *pInf = Mod->GetModInfo(iList);
			m_list_ModSet.InsertItem(iList, pInf->SetName);
			m_list_ModSet.SetItemText(iList, 1, pInf->Value);
		}

		if(IsQnt)
		{
			GetDlgItem(IDC_VIEW_MODEL_PARAMS)->ShowWindow(SW_SHOW);
			s = ParcelLoadString(STRID_VIEW_MOD_PARAMS);
			GetDlgItem(IDC_VIEW_MODEL_PARAMS)->SetWindowText(s);
			GetDlgItem(IDC_VIEW_MODEL_PARAMS_LIST)->ShowWindow(SW_SHOW);
			N = Mod->GetNModelParams();
			m_list_ModParams.DeleteAllItems();
			for(i=0; i<N; i++)
			{
				ModelParam *Dat = Mod->GetModelParam(i);
				CString fmt = Prj->GetParam(Dat->ParamName)->GetFormat();

				m_list_ModParams.InsertItem(i, Dat->ParamName);

				if(IsQnt)
				{
					s.Format(cFmt53, Dat->LimMSD);
					m_list_ModParams.SetItemText(i, 1, s);
					s.Format(fmt, Dat->LowValue);
					m_list_ModParams.SetItemText(i, 2, s);
					s.Format(fmt, Dat->UpperValue);
					m_list_ModParams.SetItemText(i, 3, s);
					s.Format(fmt, Dat->CorrA);
					m_list_ModParams.SetItemText(i, 4, s);
					s.Format(fmt, Dat->CorrB);
					m_list_ModParams.SetItemText(i, 5, s);
				}
			}
		}
		else
		{
			GetDlgItem(IDC_VIEW_MODEL_PARAMS)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_VIEW_MODEL_PARAMS_LIST)->ShowWindow(SW_HIDE);
		}

		GetDlgItem(IDC_VIEW_MODEL_SAMPLES)->ShowWindow(SW_SHOW);
		s.Format(ParcelLoadString(STRID_VIEW_MOD_SAMPLES),
			Mod->GetNSamplesForCalib(), Mod->GetNSpectraForCalib());
		GetDlgItem(IDC_VIEW_MODEL_SAMPLES)->SetWindowText(s);
		GetDlgItem(IDC_VIEW_MODEL_SAMPLES_LIST)->ShowWindow(SW_SHOW);
		N = Mod->GetNSamplesForCalib();
		m_list_ModSamples.ResetContent();
		for(i=0; i<N; i++)
		{
			ModelSample* ModSam = Mod->GetSampleForCalib(i);
			s = Mod->GetSampleTransName(ModSam);
			m_list_ModSamples.AddString(s);
		}

		GetDlgItem(IDC_VIEW_MODEL_SAMPLESVAL)->ShowWindow(SW_SHOW);
		s.Format(ParcelLoadString(STRID_VIEW_MOD_SAMPLESVAL),
			Mod->GetNSamplesForValid(), Mod->GetNSpectraForValid());
		GetDlgItem(IDC_VIEW_MODEL_SAMPLESVAL)->SetWindowText(s);
		GetDlgItem(IDC_VIEW_MODEL_SAMPLESVAL_LIST)->ShowWindow(SW_SHOW);
		N = Mod->GetNSamplesForValid();
		m_list_ModSamplesVal.ResetContent();
		for(i=0; i<N; i++)
		{
			ModelSample* ModSam = Mod->GetSampleForValid(i);
			s = Mod->GetSampleTransName(ModSam);
			m_list_ModSamplesVal.AddString(s);
		}

		GetDlgItem(IDC_VIEW_MODEL_EXCLUDED)->ShowWindow(SW_SHOW);
		N = Mod->GetNExcPoints();
		s.Format(ParcelLoadString(STRID_VIEW_MOD_EXCLUDED), N);
		GetDlgItem(IDC_VIEW_MODEL_EXCLUDED)->SetWindowText(s);
		GetDlgItem(IDC_VIEW_MODEL_EXCLUDED_LIST)->ShowWindow(SW_SHOW);
		m_list_ModExcluded.ResetContent();
		for(i=0; i<N; i++)
		{
			s.Format(cFmt86, Mod->GetExcPointMean(i));
			m_list_ModExcluded.AddString(s);
		}

		for(i=0; i<6; i++)
			m_list_ModParams.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
		for(i=0; i<2; i++)
			m_list_ModSet.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	}
	else
	{
		GetDlgItem(IDC_VIEW_MODEL_NAME)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_VIEW_MODEL_PARAMS)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_VIEW_MODEL_PARAMS_LIST)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_VIEW_MODEL_SETTINGS)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_VIEW_MODEL_SETTINGS_LIST)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_VIEW_MODEL_SAMPLES)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_VIEW_MODEL_SAMPLES_LIST)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_VIEW_MODEL_SAMPLESVAL)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_VIEW_MODEL_SAMPLESVAL_LIST)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_VIEW_MODEL_EXCLUDED)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_VIEW_MODEL_EXCLUDED_LIST)->ShowWindow(SW_HIDE);
	}



	if(mode == TIM_METHODSET)
	{
		CProjectData *Prj = Info.Project;

		GetDlgItem(IDC_VIEW_METHODSET_HEAD)->ShowWindow(SW_SHOW);
		s.Format(ParcelLoadString(STRID_VIEW_MTDSET_HEAD), Prj->GetName());
		GetDlgItem(IDC_VIEW_METHODSET_HEAD)->SetFont(&FontHeader);
		GetDlgItem(IDC_VIEW_METHODSET_HEAD)->SetWindowText(s);
		GetDlgItem(IDC_VIEW_METHODSET_METHODS)->ShowWindow(SW_SHOW);
		s.Format(ParcelLoadString(STRID_VIEW_MTDSET_METHODS), Prj->GetNMethods());
		GetDlgItem(IDC_VIEW_METHODSET_METHODS)->SetWindowText(s);
		GetDlgItem(IDC_VIEW_METHODSET_METHODS_LIST)->ShowWindow(SW_SHOW);

		m_list_Methods.DeleteAllItems();
		N = Info.Project->GetNMethods();
		for(i=0; i<N; i++)
		{
			CMethodData *Mtd = Info.Project->GetMethod(i);
			m_list_Methods.InsertItem(i, Mtd->GetName());
			s = cGetAnalysisTypeName(Mtd->GetAnalysisType());
			m_list_Methods.SetItemText(i, 1, s);
			s = Mtd->GetDate().Format(cFmtDate);
			m_list_Methods.SetItemText(i, 2, s);
		}

		for(i=0; i<3; i++)
			m_list_Methods.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	}
	else
	{
		GetDlgItem(IDC_VIEW_METHODSET_HEAD)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_VIEW_METHODSET_METHODS)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_VIEW_METHODSET_METHODS_LIST)->ShowWindow(SW_HIDE);
	}



	if(mode == TIM_METHOD)
	{
		CMethodData *Mtd = Info.Method;
		CDeviceData *Dev = Info.Device;
		bool IsQnt = (Mtd->GetAnalysisType() == C_MTD_ANALYSIS_QNT);

		GetDlgItem(IDC_VIEW_METHOD_NAME)->ShowWindow(SW_SHOW);
		s.Format(ParcelLoadString(STRID_VIEW_MTD_NAME), Mtd->GetName());
		GetDlgItem(IDC_VIEW_METHOD_NAME)->SetFont(&FontHeader);
		GetDlgItem(IDC_VIEW_METHOD_NAME)->SetWindowText(s);

		GetDlgItem(IDC_VIEW_METHOD_INFO)->ShowWindow(SW_SHOW);
		s = ParcelLoadString(STRID_VIEW_MTD_PROPS);
		GetDlgItem(IDC_VIEW_METHOD_INFO)->SetWindowText(s);
		GetDlgItem(IDC_VIEW_METHOD_INFO_LIST)->ShowWindow(SW_SHOW);

		m_list_MtdInfo.DeleteAllItems();
		iList = 0;

		m_list_MtdInfo.InsertItem(iList, ParcelLoadString(STRID_VIEW_MTD_ANALYSISTYPE));
		s = cGetAnalysisTypeName(Mtd->GetAnalysisType());
		m_list_MtdInfo.SetItemText(iList, 1, s);
		iList++;

		m_list_MtdInfo.InsertItem(iList, ParcelLoadString(STRID_VIEW_MTD_MARK));
		s = Mtd->GetDate().Format(cFmtDate);
		m_list_MtdInfo.SetItemText(iList, 1, s);
		iList++;

		if(IsQnt)
		{
			m_list_MtdInfo.InsertItem(iList, ParcelLoadString(STRID_VIEW_MTD_MARKASMOIS));
			s = Mtd->GetMoisParam();
			if(s.IsEmpty())  s = ParcelLoadString(STRID_VIEW_MTD_NOMOIS);
			m_list_MtdInfo.SetItemText(iList, 1, s);
			iList++;

			GetDlgItem(IDC_VIEW_METHOD_RULES)->ShowWindow(SW_SHOW);
			s = ParcelLoadString(STRID_VIEW_MTD_RULES);
			GetDlgItem(IDC_VIEW_METHOD_RULES)->SetWindowText(s);
			GetDlgItem(IDC_VIEW_METHOD_RULES_LIST)->ShowWindow(SW_SHOW);

			N = Mtd->GetNRules();
			m_list_MtdRules.DeleteAllItems();
			for(i=0, k=0; i<N; i++, k++)
			{
				AnalyseRule *Rule = Mtd->GetRule(i);
				m_list_MtdRules.InsertItem(k, Rule->Name);
				s = Rule->GetStdMoistureStr();
				m_list_MtdRules.SetItemText(k, 1, s);
				s = (Rule->MainModel.IsEmpty()) ? ParcelLoadString(STRID_VIEW_MTD_NOMOD) : Rule->MainModel;
				m_list_MtdRules.SetItemText(k, 2, s);
				for(j=0; j<int(Rule->AddModels.size()); j++)
				{
					if(j > 0)
					{
						k++;
						m_list_MtdRules.InsertItem(k, cEmpty);
					}
					m_list_MtdRules.SetItemText(k, 3, Rule->AddModels[j]);
				}
			}

			GetDlgItem(IDC_VIEW_METHOD_QLTMODELS)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_VIEW_METHOD_QLTMODELS_LIST)->ShowWindow(SW_HIDE);
		}
		else
		{
			m_list_MtdInfo.InsertItem(iList, ParcelLoadString(STRID_VIEW_MTD_FORMULA));
			s = Mtd->Formula.GetAsString();
			m_list_MtdInfo.SetItemText(iList, 1, s);
			iList++;

			GetDlgItem(IDC_VIEW_METHOD_QLTMODELS)->ShowWindow(SW_SHOW);
			s = ParcelLoadString(STRID_VIEW_MTD_QLTMODELS);
			GetDlgItem(IDC_VIEW_METHOD_QLTMODELS)->SetWindowText(s);
			GetDlgItem(IDC_VIEW_METHOD_QLTMODELS_LIST)->ShowWindow(SW_SHOW);

			m_list_MtdQltModels.DeleteAllItems();
			ItIntStr itQ;
			for(i=0, itQ=Mtd->ModelQltNames.begin(); itQ!=Mtd->ModelQltNames.end(); ++itQ, i++)
			{
				s.Format(cFmt1, itQ->first + 1);
				m_list_MtdQltModels.InsertItem(i, s);
				m_list_MtdQltModels.SetItemText(i, 1, itQ->second);
			}

			GetDlgItem(IDC_VIEW_METHOD_RULES)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_VIEW_METHOD_RULES_LIST)->ShowWindow(SW_HIDE);
		}

		if(Dev->GetType() == C_DEV_TYPE_40 || Dev->GetType() == C_DEV_TYPE_12M)
		{
			m_list_MtdInfo.InsertItem(iList, ParcelLoadString(STRID_VIEW_MTD_LOWTEMP));
			s.Format(cFmt1, Mtd->GetLowTemp());
			m_list_MtdInfo.SetItemText(iList, 1, s);
			iList++;

			m_list_MtdInfo.InsertItem(iList, ParcelLoadString(STRID_VIEW_MTD_UPPERTEMP));
			s.Format(cFmt1, Mtd->GetUpperTemp());
			m_list_MtdInfo.SetItemText(iList, 1, s);
			iList++;

			m_list_MtdInfo.InsertItem(iList, ParcelLoadString(STRID_VIEW_MTD_DIFFTEMP));
			s.Format(cFmt1, Mtd->GetMaxDiffTemp());
			m_list_MtdInfo.SetItemText(iList, 1, s);
			iList++;
		}

		for(i=0; i<2; i++)
			m_list_MtdInfo.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
		for(i=0; i<4; i++)
			m_list_MtdRules.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
		for(i=0; i<2; i++)
			m_list_MtdQltModels.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	}
	else
	{
		GetDlgItem(IDC_VIEW_METHOD_NAME)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_VIEW_METHOD_INFO)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_VIEW_METHOD_INFO_LIST)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_VIEW_METHOD_RULES)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_VIEW_METHOD_RULES_LIST)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_VIEW_METHOD_QLTMODELS)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_VIEW_METHOD_QLTMODELS_LIST)->ShowWindow(SW_HIDE);
	}

}

void CParcelView::CreateSpecFonts()
{
	LOGFONT logFont;

	logFont.lfHeight = 16;
	logFont.lfWidth = 0;
	logFont.lfEscapement = 0;
	logFont.lfOrientation = 0;
	logFont.lfWeight = FW_BOLD;
	logFont.lfItalic = 0;
	logFont.lfUnderline = 0;
	logFont.lfStrikeOut = 0;
	logFont.lfCharSet = DEFAULT_CHARSET;
	logFont.lfOutPrecision = OUT_DEFAULT_PRECIS;
	logFont.lfClipPrecision = CLIP_DEFAULT_PRECIS;
	logFont.lfQuality = PROOF_QUALITY;
	logFont.lfPitchAndFamily = DEFAULT_PITCH | FF_DONTCARE;
	_tcscpy(logFont.lfFaceName, _T("Arial"));        // request a face name "Arial"

	VERIFY(FontHeader.CreateFontIndirect(&logFont));  // create the font
}

void CParcelView::FillParamsLists(CProjectData *Prj)
{
	int N = Prj->GetNParams();
	if(N <= 0)  return; 

	int ind = m_list_Params.GetNextItem(-1, LVNI_SELECTED);
	if(ind < 0 || ind >= N)
	{
		GetDlgItem(IDC_VIEW_PARAMSSAM)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_VIEW_PARAMSSAM_LIST)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_VIEW_PARAMSMOD)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_VIEW_PARAMSMOD_LIST)->ShowWindow(SW_HIDE);
		return;
	}
	GetDlgItem(IDC_VIEW_PARAMSSAM)->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_VIEW_PARAMSSAM_LIST)->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_VIEW_PARAMSMOD)->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_VIEW_PARAMSMOD_LIST)->ShowWindow(SW_SHOW);

	CParamData *Par = Prj->GetParam(ind);
	int i, iList = 0;
	CString s, ParName = Par->GetName();

	s.Format(ParcelLoadString(STRID_VIEW_PARSET_SAM), ParName);
	GetDlgItem(IDC_VIEW_PARAMSSAM)->SetWindowText(s);
	s.Format(ParcelLoadString(STRID_VIEW_PARSET_MOD), ParName);
	GetDlgItem(IDC_VIEW_PARAMSMOD)->SetWindowText(s);

	m_list_ParamsSam.DeleteAllItems();
	
	m_list_ParamsSam.DeleteColumn(1);
	s.Format(ParcelLoadString(STRID_VIEW_PARSET_COLSAM_VALUE), Par->GetUnit());
	m_list_ParamsSam.InsertColumn(1, s, LVCFMT_RIGHT, 0, 1);

	for(i=0; i<3; i++)
		m_list_ParamsSam.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);

	for(i=0, iList=0; i<Prj->GetNSamples(); i++)
	{
		CSampleData *Sam = Prj->GetSample(i);
		ReferenceData *Ref = Sam->GetReferenceData(ParName);
		if(Ref->bNoValue)
			continue;

		m_list_ParamsSam.InsertItem(iList, Sam->GetName());
		s.Format(Par->GetFormat(), Ref->Value);
		m_list_ParamsSam.SetItemText(iList, 1, s);
		iList++;
	}

	m_list_ParamsMod.ResetContent();
	for(i=0; i<Prj->GetNModels(); i++)
	{
		CModelData *Mod = Prj->GetModel(i);
		if(!Mod->IsModelParamExist(ParName))
			continue;

		m_list_ParamsMod.AddString(Mod->GetName());
	}
}

void CParcelView::CreateSamplesTransList(CTransferData* Trans)
{
	int i;
	int nColumnCount = m_list_SamplesTrans.GetHeaderCtrl()->GetItemCount();
	for(i=m_nLSampParStart; i<nColumnCount; i++)
	{
		m_list_SamplesTrans.DeleteColumn(m_nLSampParStart);
	}

	if(Trans == NULL)
		return;

	CProjectData* Prj = GetProjectByHItem(Trans->GetParentHItem());	// ��������!

	int NP = Prj->GetNParams();	// �������!
	for(i=0; i<NP; i++)
	{
		CParamData *Par = Prj->GetParam(i);	// �������!

		CString s;
		s.Format(cFmtS_S, Par->GetName(), Par->GetUnit());
		m_list_SamplesTrans.InsertColumn(i + m_nLSampParStart, s, LVCFMT_RIGHT, 0, i + m_nLSampParStart);
	}
	m_list_SamplesTrans.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

	for(i=0; i<NP + m_nLSampParStart; i++)
		m_list_SamplesTrans.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);

}

void CParcelView::FillSpectraTransList(CSampleData* Sam)
{
	m_list_SpectraTrans.DeleteAllItems();

	if(Sam == NULL)
	{
		GetDlgItem(IDC_VIEW_SAMPLETRANS_SPECTRA)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_VIEW_SAMPLETRANS_SPECTRA_LIST)->ShowWindow(SW_HIDE);
		return;
	}

	GetDlgItem(IDC_VIEW_SAMPLETRANS_SPECTRA)->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_VIEW_SAMPLETRANS_SPECTRA_LIST)->ShowWindow(SW_SHOW);

	CString s;

	s.Format(ParcelLoadString(STRID_VIEW_TRANS_SPECTRA), Sam->GetName());
	GetDlgItem(IDC_VIEW_SAMPLETRANS_SPECTRA)->SetWindowText(s);

	int i, N = Sam->GetNSpectra();
	for(i=0; i<N; i++)
	{
		Spectrum *Spec = Sam->GetSpectrumByInd(i);

		s.Format(cFmt02, Spec->Num);
		m_list_SpectraTrans.InsertItem(i, s);

		s = Spec->IsUseStr();
		m_list_SpectraTrans.SetItemText(i, 1, s);
		s = Spec->DateTime.Format(cFmtDate);
		m_list_SpectraTrans.SetItemText(i, 2, s);
	}
	if(N > 0)
		m_list_SpectraTrans.SetItemState(0, LVIS_SELECTED, LVIS_SELECTED);
}

void CParcelView::RecreateColumns()
{
	CString s;
	int i;
	int nCol;

	nCol = m_list_DevicePrj.GetHeaderCtrl()->GetItemCount();
	for(i=nCol-1; i>=0; i--)
		m_list_DevicePrj.DeleteColumn(i);
	m_list_DevicePrj.InsertColumn(0, ParcelLoadString(STRID_VIEW_DEV_COLPRJ), LVCFMT_LEFT, 0, 0);
	m_list_DevicePrj.InsertColumn(1, ParcelLoadString(STRID_VIEW_DEV_COLMTD), LVCFMT_LEFT, 0, 1);
	m_list_DevicePrj.InsertColumn(2, ParcelLoadString(STRID_VIEW_DEV_COLMOD), LVCFMT_LEFT, 0, 2);
	m_list_DevicePrj.InsertColumn(3, ParcelLoadString(STRID_VIEW_DEV_COLPAR), LVCFMT_LEFT, 0, 3);
	m_list_DevicePrj.InsertColumn(4, ParcelLoadString(STRID_VIEW_DEV_COLOWN), LVCFMT_LEFT, 0, 4);
	m_list_DevicePrj.InsertColumn(5, ParcelLoadString(STRID_VIEW_DEV_COLTRANS), LVCFMT_LEFT, 0, 5);
	for(i=0; i<6; i++)
		m_list_DevicePrj.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	m_list_DevicePrj.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

	nCol = m_list_ProjectSpec.GetHeaderCtrl()->GetItemCount();
	for(i=nCol-1; i>=0; i--)
		m_list_ProjectSpec.DeleteColumn(i);
	m_list_ProjectSpec.InsertColumn(0, ParcelLoadString(STRID_VIEW_PRJ_COLSPEC_PARAMETER), LVCFMT_LEFT, 0, 0);
	m_list_ProjectSpec.InsertColumn(1, ParcelLoadString(STRID_VIEW_PRJ_COLSPEC_VALUE), LVCFMT_RIGHT, 0, 1);
	for(i=0; i<2; i++)
		m_list_ProjectSpec.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	m_list_ProjectSpec.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

	nCol = m_list_ProjectTemp.GetHeaderCtrl()->GetItemCount();
	for(i=nCol-1; i>=0; i--)
		m_list_ProjectTemp.DeleteColumn(i);
	m_list_ProjectTemp.InsertColumn(0, ParcelLoadString(STRID_VIEW_PRJ_COLTEMP_PARAMETER), LVCFMT_LEFT, 0, 0);
	m_list_ProjectTemp.InsertColumn(1, ParcelLoadString(STRID_VIEW_PRJ_COLTEMP_VALUE), LVCFMT_RIGHT, 0, 1);
	for(i=0; i<2; i++)
		m_list_ProjectTemp.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	m_list_ProjectTemp.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

	nCol = m_list_ProjectSubj.GetHeaderCtrl()->GetItemCount();
	for(i=nCol-1; i>=0; i--)
		m_list_ProjectSubj.DeleteColumn(i);
	m_list_ProjectSubj.InsertColumn(0, ParcelLoadString(STRID_VIEW_PRJ_COLSUBJ_PARAMETER), LVCFMT_LEFT, 0, 0);
	m_list_ProjectSubj.InsertColumn(1, ParcelLoadString(STRID_VIEW_PRJ_COLSUBJ_VALUE), LVCFMT_RIGHT, 0, 1);
	for(i=0; i<2; i++)
		m_list_ProjectSubj.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	m_list_ProjectSubj.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

	nCol = m_list_ProjectRef.GetHeaderCtrl()->GetItemCount();
	for(i=nCol-1; i>=0; i--)
		m_list_ProjectRef.DeleteColumn(i);
	m_list_ProjectRef.InsertColumn(0, ParcelLoadString(STRID_VIEW_PRJ_COLREF_PARAMETER), LVCFMT_LEFT, 0, 0);
	m_list_ProjectRef.InsertColumn(1, ParcelLoadString(STRID_VIEW_PRJ_COLREF_VALUE), LVCFMT_RIGHT, 0, 1);
	for(i=0; i<2; i++)
		m_list_ProjectRef.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	m_list_ProjectRef.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

	nCol = m_list_ProjectPlan.GetHeaderCtrl()->GetItemCount();
	for(i=nCol-1; i>=0; i--)
		m_list_ProjectPlan.DeleteColumn(i);
	m_list_ProjectPlan.InsertColumn(0, ParcelLoadString(STRID_VIEW_PRJ_COLPLAN_PARAMETER), LVCFMT_LEFT, 0, 0);
	m_list_ProjectPlan.InsertColumn(1, ParcelLoadString(STRID_VIEW_PRJ_COLPLAN_VALUE), LVCFMT_RIGHT, 0, 1);
	for(i=0; i<2; i++)
		m_list_ProjectPlan.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	m_list_ProjectPlan.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

	nCol = m_list_Params.GetHeaderCtrl()->GetItemCount();
	for(i=nCol-1; i>=0; i--)
		m_list_Params.DeleteColumn(i);
	s = ParcelLoadString(STRID_VIEW_PARSET_COLPAR_NAME);
	m_list_Params.InsertColumn(0, s, LVCFMT_LEFT, 0, 0);
	s = ParcelLoadString(STRID_VIEW_PARSET_COLPAR_STDMOIS);
	m_list_Params.InsertColumn(1, s, LVCFMT_RIGHT, 0, 1);
	s = ParcelLoadString(STRID_VIEW_PARSET_COLPAR_UNIT);
	m_list_Params.InsertColumn(2, s, LVCFMT_RIGHT, 0, 2);
	s = ParcelLoadString(STRID_VIEW_PARSET_COLPAR_FORMAT);
	m_list_Params.InsertColumn(3, s, LVCFMT_RIGHT, 0, 3);
	for(i=0; i<4; i++)
		m_list_Params.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	m_list_Params.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

	nCol = m_list_ParamsSam.GetHeaderCtrl()->GetItemCount();
	for(i=nCol-1; i>=0; i--)
		m_list_ParamsSam.DeleteColumn(i);
	s = ParcelLoadString(STRID_VIEW_PARSET_COLSAM_SAMPLE);
	m_list_ParamsSam.InsertColumn(0, s, LVCFMT_LEFT, 0, 0);
	s.Format(ParcelLoadString(STRID_VIEW_PARSET_COLSAM_VALUE), cEmpty);
	m_list_ParamsSam.InsertColumn(1, s, LVCFMT_RIGHT, 0, 1);
	for(i=0; i<2; i++)
		m_list_ParamsSam.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	m_list_ParamsSam.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

	nCol = m_list_SampleSets.GetHeaderCtrl()->GetItemCount();
	for(i=nCol-1; i>=0; i--)
		m_list_SampleSets.DeleteColumn(i);
	s = ParcelLoadString(STRID_VIEW_SAMSET_COL_SAM);
	m_list_SampleSets.InsertColumn(0, s, LVCFMT_LEFT, 0, 0);
	s = ParcelLoadString(STRID_VIEW_SAMSET_COL_TYPE);
	m_list_SampleSets.InsertColumn(1, s, LVCFMT_LEFT, 0, 1);
	s = ParcelLoadString(STRID_VIEW_SAMSET_COL_NUM);
	m_list_SampleSets.InsertColumn(2, s, LVCFMT_LEFT, 0, 2);
	s = ParcelLoadString(STRID_VIEW_SAMSET_COL_RANGE);
	m_list_SampleSets.InsertColumn(3, s, LVCFMT_LEFT, 0, 3);
	s = ParcelLoadString(STRID_VIEW_SAMSET_COL_MASTER);
	m_list_SampleSets.InsertColumn(4, s, LVCFMT_LEFT, 0, 4);
	s = ParcelLoadString(STRID_VIEW_SAMSET_COL_PREPROC);
	m_list_SampleSets.InsertColumn(5, s, LVCFMT_LEFT, 0, 5);
	s = ParcelLoadString(STRID_VIEW_SAMSET_COL_DATE);
	m_list_SampleSets.InsertColumn(6, s, LVCFMT_LEFT, 0, 6);
	for(i=0; i<7; i++)
	{
		m_list_SampleSets.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	}
	m_list_SampleSets.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

	nCol = m_list_SamplesTrans.GetHeaderCtrl()->GetItemCount();
	for(i=nCol-1; i>=0; i--)
	{
		m_list_SamplesTrans.DeleteColumn(i);
	}
	s = ParcelLoadString(STRID_VIEW_TRANS_COLSAM_SAMPLE);
	m_list_SamplesTrans.InsertColumn(0, s, LVCFMT_LEFT, 0, 0);
	s = ParcelLoadString(STRID_VIEW_TRANS_COLSAM_IDEN2);
	m_list_SamplesTrans.InsertColumn(1, s, LVCFMT_RIGHT, 0, 1);
	s = ParcelLoadString(STRID_VIEW_TRANS_COLSAM_IDEN3);
	m_list_SamplesTrans.InsertColumn(2, s, LVCFMT_RIGHT, 0, 2);
	s = ParcelLoadString(STRID_VIEW_TRANS_COLSAM_NSPEC);
	m_list_SamplesTrans.InsertColumn(3, s, LVCFMT_RIGHT, 0, 3);
	s = ParcelLoadString(STRID_MEAS_DATE);
	m_list_SamplesTrans.InsertColumn(4, s, LVCFMT_RIGHT, 0, 4);
	m_nLSampTimeCol = 4;
	m_nLSampParStart = 5;
	for(i=0; i<m_nLSampParStart; i++)
	{
		m_list_SamplesTrans.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	}
	m_list_SamplesTrans.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);
	m_list_SamplesTrans.SetIconList();

	nCol = m_list_SpectraTrans.GetHeaderCtrl()->GetItemCount();
	for(i=nCol-1; i>=0; i--)
		m_list_SpectraTrans.DeleteColumn(i);
	s = ParcelLoadString(STRID_VIEW_TRANS_COLSPEC_NUM);
	m_list_SpectraTrans.InsertColumn(0, s, LVCFMT_LEFT, 0, 0);
	s = ParcelLoadString(STRID_VIEW_TRANS_COLSPEC_USED);
	m_list_SpectraTrans.InsertColumn(1, s, LVCFMT_RIGHT, 0, 1);
	s = ParcelLoadString(STRID_VIEW_TRANS_COLSPEC_DATE);
	m_list_SpectraTrans.InsertColumn(2, s, LVCFMT_RIGHT, 0, 2);
	for(i=0; i<3; i++)
		m_list_SpectraTrans.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	m_list_SpectraTrans.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

	nCol = m_list_TransSettings.GetHeaderCtrl()->GetItemCount();
	for(i=nCol-1; i>=0; i--)
		m_list_TransSettings.DeleteColumn(i);
	m_list_TransSettings.InsertColumn(0, ParcelLoadString(STRID_VIEW_TRANS_COLSETS_PARAMETR), LVCFMT_LEFT, 0, 0);
	m_list_TransSettings.InsertColumn(1, ParcelLoadString(STRID_VIEW_TRANS_COLSETS_VALUE), LVCFMT_RIGHT, 0, 1);
	for(i=0; i<2; i++)
		m_list_TransSettings.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	m_list_TransSettings.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

	nCol = m_list_Models.GetHeaderCtrl()->GetItemCount();
	for(i=nCol-1; i>=0; i--)
		m_list_Models.DeleteColumn(i);
	s = ParcelLoadString(STRID_VIEW_MODSET_COL_MODEL);
	m_list_Models.InsertColumn(0, s, LVCFMT_LEFT, 0, 0);
	s = ParcelLoadString(STRID_VIEW_MODSET_COL_ANTYPE);
	m_list_Models.InsertColumn(1, s, LVCFMT_RIGHT, 0, 1);
	s = ParcelLoadString(STRID_VIEW_MODSET_COL_MODTYPE);
	m_list_Models.InsertColumn(2, s, LVCFMT_RIGHT, 0, 2);
	s = ParcelLoadString(STRID_VIEW_MODSET_COL_DATE);
	m_list_Models.InsertColumn(3, s, LVCFMT_RIGHT, 0, 3);
	for(i=0; i<4; i++)
		m_list_Models.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	m_list_Models.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

	nCol = m_list_ModParams.GetHeaderCtrl()->GetItemCount();
	for(i=nCol-1; i>=0; i--)
		m_list_ModParams.DeleteColumn(i);
	s = ParcelLoadString(STRID_VIEW_MOD_COLPAR_NAME);
	m_list_ModParams.InsertColumn(0, s, LVCFMT_LEFT, 0, 0);
	s = ParcelLoadString(STRID_VIEW_MOD_COLPAR_MSD);
	m_list_ModParams.InsertColumn(1, s, LVCFMT_RIGHT, 0, 1);
	s = ParcelLoadString(STRID_VIEW_MOD_COLPAR_LOW);
	m_list_ModParams.InsertColumn(2, s, LVCFMT_RIGHT, 0, 2);
	s = ParcelLoadString(STRID_VIEW_MOD_COLPAR_UPPER);
	m_list_ModParams.InsertColumn(3, s, LVCFMT_RIGHT, 0, 3);
	s = ParcelLoadString(STRID_VIEW_MOD_COLPAR_CORRA);
	m_list_ModParams.InsertColumn(4, s, LVCFMT_RIGHT, 0, 4);
	s = ParcelLoadString(STRID_VIEW_MOD_COLPAR_CORRB);
	m_list_ModParams.InsertColumn(5, s, LVCFMT_RIGHT, 0, 5);
	for(i=0; i<6; i++)
		m_list_ModParams.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	m_list_ModParams.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

	nCol = m_list_ModSet.GetHeaderCtrl()->GetItemCount();
	for(i=nCol-1; i>=0; i--)
		m_list_ModSet.DeleteColumn(i);
	m_list_ModSet.InsertColumn(0, ParcelLoadString(STRID_VIEW_MOD_COLSET_PARAMETR), LVCFMT_LEFT, 0, 0);
	m_list_ModSet.InsertColumn(1, ParcelLoadString(STRID_VIEW_MOD_COLSET_VALUE), LVCFMT_RIGHT, 0, 1);
	for(i=0; i<2; i++)
		m_list_ModSet.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	m_list_ModSet.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

	nCol = m_list_Methods.GetHeaderCtrl()->GetItemCount();
	for(i=nCol-1; i>=0; i--)
		m_list_Methods.DeleteColumn(i);
	m_list_Methods.InsertColumn(0, ParcelLoadString(STRID_VIEW_MTDSET_COL_NAME), LVCFMT_LEFT, 0, 0);
	m_list_Methods.InsertColumn(1, ParcelLoadString(STRID_VIEW_MTDSET_COL_ANTYPE), LVCFMT_RIGHT, 0, 1);
	m_list_Methods.InsertColumn(2, ParcelLoadString(STRID_VIEW_MTDSET_COL_DATE), LVCFMT_RIGHT, 0, 1);
	for(i=0; i<3; i++)
		m_list_Methods.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	m_list_Methods.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

	nCol = m_list_MtdInfo.GetHeaderCtrl()->GetItemCount();
	for(i=nCol-1; i>=0; i--)
		m_list_MtdInfo.DeleteColumn(i);
	m_list_MtdInfo.InsertColumn(0, ParcelLoadString(STRID_VIEW_MTD_COLINFO_PARAMETR), LVCFMT_LEFT, 0, 0);
	m_list_MtdInfo.InsertColumn(1, ParcelLoadString(STRID_VIEW_MTD_COLINFO_VALUE), LVCFMT_RIGHT, 0, 1);
	for(i=0; i<2; i++)
		m_list_MtdInfo.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	m_list_MtdInfo.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

	nCol = m_list_MtdRules.GetHeaderCtrl()->GetItemCount();
	for(i=nCol-1; i>=0; i--)
		m_list_MtdRules.DeleteColumn(i);
	s = ParcelLoadString(STRID_VIEW_MTD_COLRULES_PARAM);
	m_list_MtdRules.InsertColumn(0, s, LVCFMT_LEFT, 0, 0);
	s = ParcelLoadString(STRID_VIEW_MTD_COLRULES_MOISTURE);
	m_list_MtdRules.InsertColumn(1, s, LVCFMT_RIGHT, 0, 1);
	s = ParcelLoadString(STRID_VIEW_MTD_COLRULES_DEFMODEL);
	m_list_MtdRules.InsertColumn(2, s, LVCFMT_RIGHT, 0, 2);
	s = ParcelLoadString(STRID_VIEW_MTD_COLRULES_MODELS);
	m_list_MtdRules.InsertColumn(3, s, LVCFMT_RIGHT, 0, 3);
	for(i=0; i<4; i++)
		m_list_MtdRules.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	m_list_MtdRules.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

	nCol = m_list_MtdQltModels.GetHeaderCtrl()->GetItemCount();
	for(i=nCol-1; i>=0; i--)
		m_list_MtdQltModels.DeleteColumn(i);
	m_list_MtdQltModels.InsertColumn(0, ParcelLoadString(STRID_VIEW_MTD_COLNUM), LVCFMT_LEFT, 0, 0);
	m_list_MtdQltModels.InsertColumn(1, ParcelLoadString(STRID_VIEW_MTD_COLMODNAME), LVCFMT_RIGHT, 0, 1);
	for(i=0; i<2; i++)
		m_list_MtdQltModels.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	m_list_MtdQltModels.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

}
