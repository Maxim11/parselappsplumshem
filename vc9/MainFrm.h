#ifndef _PARCEL_MAIN_FRM_H_
#define _PARCEL_MAIN_FRM_H_

#pragma once


const int C_MENU_FILE					= 0;
const int C_MENU_FILE_CREATE			= 0;
const int C_MENU_FILE_OPEN				= 1;
const int C_MENU_FILE_CLOSE				= 2;
const int C_MENU_FILE_DELETE			= 3;
const int C_MENU_FILE_SAVEAS			= 5;
const int C_MENU_FILE_EXIT				= 7;

const int C_MENU_DEVICE					= 1;
const int C_MENU_DEVICE_CREATE			= 0;
const int C_MENU_DEVICE_EDIT			= 1;
const int C_MENU_DEVICE_REMOVE			= 2;

const int C_MENU_PROJECT				= 2;
const int C_MENU_PROJECT_CREATE			= 0;
const int C_MENU_PROJECT_EDIT			= 1;
const int C_MENU_PROJECT_REMOVE			= 2;
const int C_MENU_PROJECT_CREATE_SST		= 4;
const int C_MENU_PROJECT_LOADOLD		= 6;
const int C_MENU_PROJECT_LOADOLD_SST	= 7;
const int C_MENU_PROJECT_COPYTOBASE		= 9;
const int C_MENU_PROJECT_COPYTOBASE_ALL	= 10;
const int C_MENU_PROJECT_EXPORT			= 12;
const int C_MENU_PROJECT_IMPORT			= 13;
const int C_MENU_PROJECT_TRANSFER		= 14;

const int C_MENU_PARAM					= 3;
const int C_MENU_PARAM_EDIT				= 0;

const int C_MENU_SAMPLE					= 4;
const int C_MENU_SAMPLE_EDIT			= 0;
const int C_MENU_SAMPLE_CREATE_SST		= 2;
const int C_MENU_SAMPLE_DELETE_SST		= 3;
const int C_MENU_SAMPLE_ADDTRANS		= 5;
const int C_MENU_SAMPLE_EDITTRANS		= 6;
const int C_MENU_SAMPLE_REMOVETRANS		= 7;

const int C_MENU_MODEL					= 5;
const int C_MENU_MODEL_CREATE			= 0;
const int C_MENU_MODEL_COPY				= 1;
const int C_MENU_MODEL_EDIT				= 2;
const int C_MENU_MODEL_REMOVE			= 3;
const int C_MENU_MODEL_RESULT			= 5;

const int C_MENU_METHOD					= 6;
const int C_MENU_METHOD_CREATE			= 0;
const int C_MENU_METHOD_COPY			= 1;
const int C_MENU_METHOD_EDIT			= 2;
const int C_MENU_METHOD_REMOVE			= 3;
const int C_MENU_METHOD_EXPORT			= 5;

const int C_MENU_VIEW					= 7;
const int C_MENU_VIEW_LANG				= 0;
/*
const int C_MENU_VIEW_TOOLBAR			= 2;
const int C_MENU_VIEW_STATUSBAR			= 3;
*/
const int C_MENU_VIEW_TOOLBAR			= 0;
const int C_MENU_VIEW_STATUSBAR			= 1;

const int C_MENU_HELP					= 8;
const int C_MENU_HELP_TOPIC				= 0;
const int C_MENU_HELP_ABOUT				= 2;


class CSplashScreenEx;

class CMainFrame : public CFrameWnd
{
	
protected:

	CMainFrame();

	DECLARE_DYNCREATE(CMainFrame)

public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual ~CMainFrame();

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

	CStatusBar  m_wndStatusBar;
	CToolBar    m_wndToolBar;

	CProgressCtrl *pProgressBar;

	CSplashScreenEx *pSplashScreen;

	int nSorts;
	int nLang;
	int iLang;

	CString WorkDirectory;
	CString BaseName;
	CString TemplateOptimName;
	CString TemplateQntName;
	CString TemplateQltName;

	CString HelpDirectory;
	CString HelpFileName;

public:

	CString GetBaseName() const { return BaseName;}
	void MakeMenu();

protected:

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnTimer(UINT Id);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnSize(UINT Type, int cx, int cy);
	afx_msg void OnClose();
	afx_msg void OnDestroy();

   LRESULT OnSetStatusBar(WPARAM wParam, LPARAM lParam);

   LRESULT OnOpenProgressBar(WPARAM wParam, LPARAM lParam);
   LRESULT OnCloseProgressBar(WPARAM wParam, LPARAM lParam);
   LRESULT OnSetProgressBar(WPARAM wParam, LPARAM lParam);

	afx_msg void OnBaseNew();
	void OnUpdateBaseNew(CCmdUI *pCmdUI);
	afx_msg void OnBaseLoad();
	void OnUpdateBaseLoad(CCmdUI *pCmdUI);
	afx_msg void OnBaseClose();
	void OnUpdateBaseClose(CCmdUI *pCmdUI);
	afx_msg void OnBaseDelete();
	void OnUpdateBaseDelete(CCmdUI *pCmdUI);
	afx_msg void OnBaseCopy();
	void OnUpdateBaseCopy(CCmdUI *pCmdUI);

	afx_msg void OnCreateNewDevice();
	void OnUpdateCreateNewDevice(CCmdUI *pCmdUI);
	afx_msg void OnChangeDeviceSettings();
	void OnUpdateChangeDeviceSettings(CCmdUI *pCmdUI);
	afx_msg void OnDeleteDevice();
	void OnUpdateDeleteDevice(CCmdUI *pCmdUI);
	afx_msg void OnCopyAllProjectsToBase();
	void OnUpdateCopyAllProjectsToBase(CCmdUI *pCmdUI);

	afx_msg void OnCreateNewProject();
	void OnUpdateCreateNewProject(CCmdUI *pCmdUI);
	afx_msg void OnChangeProjectSettings();
	void OnUpdateChangeProjectSettings(CCmdUI *pCmdUI);
	afx_msg void OnDeleteProject();
	void OnUpdateDeleteProject(CCmdUI *pCmdUI);
	afx_msg void OnCreateSSTProject();
	void OnUpdateCreateSSTProject(CCmdUI *pCmdUI);
	afx_msg void OnLoadOldProject();
	void OnUpdateLoadOldProject(CCmdUI *pCmdUI);
	afx_msg void OnLoadOldSSTProject();
	void OnUpdateLoadOldSSTProject(CCmdUI *pCmdUI);
	afx_msg void OnCopyProjectToBase();
	void OnUpdateCopyProjectToBase(CCmdUI *pCmdUI);
	afx_msg void OnExportProjectSettings();
	void OnUpdateExportProjectSettings(CCmdUI *pCmdUI);
	afx_msg void OnImportSpectra();
	void OnUpdateImportSpectra(CCmdUI *pCmdUI);
	afx_msg void OnProjectTransfer();
	void OnUpdateProjectTransfer(CCmdUI *pCmdUI);

	afx_msg void OnEditParamList();
	void OnUpdateEditParamList(CCmdUI *pCmdUI);

	afx_msg void OnEditSampleList();
	void OnUpdateEditSampleList(CCmdUI *pCmdUI);
	afx_msg void OnCreateSSTTransfer();
	void OnUpdateCreateSSTTransfer(CCmdUI *pCmdUI);
	afx_msg void OnDeleteSSTTransfer();
	void OnUpdateDeleteSSTTransfer(CCmdUI *pCmdUI);
	afx_msg void OnAddTransfer();
	void OnUpdateAddTransfer(CCmdUI *pCmdUI);
	afx_msg void OnEditTransfer();
	void OnUpdateEditTransfer(CCmdUI *pCmdUI);
	afx_msg void OnDeleteTransfer();
	void OnUpdateDeleteTransfer(CCmdUI *pCmdUI);
	afx_msg void OnReadOldSample();
	void OnUpdateReadOldSample(CCmdUI *pCmdUI);

	afx_msg void OnCreateNewModel();
	void OnUpdateCreateNewModel(CCmdUI *pCmdUI);
	afx_msg void OnCopyModel();
	void OnUpdateCopyModel(CCmdUI *pCmdUI);
	afx_msg void OnChangeModelSettings();
	void OnUpdateChangeModelSettings(CCmdUI *pCmdUI);
	afx_msg void OnDeleteModel();
	void OnUpdateDeleteModel(CCmdUI *pCmdUI);
	afx_msg void OnModelResult();
	void OnUpdateModelResult(CCmdUI *pCmdUI);

	afx_msg void OnCreateNewMethod();
	void OnUpdateCreateNewMethod(CCmdUI *pCmdUI);
	afx_msg void OnCopyMethod();
	void OnUpdateCopyMethod(CCmdUI *pCmdUI);
	afx_msg void OnChangeMethodSettings();
	void OnUpdateChangeMethodSettings(CCmdUI *pCmdUI);
	afx_msg void OnDeleteMethod();
	void OnUpdateDeleteMethod(CCmdUI *pCmdUI);
	afx_msg void OnExportMethod();
	void OnUpdateExportMethod(CCmdUI *pCmdUI);

	afx_msg void OnChangeLanguage(UINT nID);
	void OnUpdateChangeLanguage(CCmdUI *pCmdUI);

	afx_msg void OnHelpFinder();
	void OnUpdateHelpFinder(CCmdUI *pCmdUI);

public:

	bool IsExistHelp();
	void ShowHelp(int IdDlg);

	void SetStatusBar(CString text);
	void SetStatusBar(int Index, CString text);
	void ShowStatusBar(BOOL IsShow, BOOL IsDelay = false);

	void OpenProgressBar(int min, int max, int step);
    void CloseProgressBar();
	void SetProgressBar(int pos);
	void SetProgressBar();

	bool LoadInit();
	void SaveInit();

	void SetHelpFile(int ilang);
	CString GetLanguage();
	int GetLanguageInt() {return iLang;}

protected:

	DECLARE_MESSAGE_MAP()
};

extern CMainFrame* pFrame;

#endif
