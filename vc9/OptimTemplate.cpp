#include "stdafx.h"

#include "OptimTemplate.h"
#include "FileSystem.h"

using namespace filesystem;

Template::Template()
{
	Name = ParcelLoadString(STRID_DEFNAME_TMPL);
	Note = cEmpty;
	Date = 0;

	NCompMean = 3;
	HiperMean = 0.1;
	NHarmMean = 0;
	KfnlMean = 0.;
	SpecLeftMean = 8000;
	SpecRightMean = 14000;

	PrjSpecMin = 8000;
	PrjSpecMax = 14000;
	PrjResol = 2;
	PrjZeroFilling = 0;

	LastCrit = C_CRIT_SEC;
}

Template::~Template()
{
	Steps.clear();
}

void Template::Copy(Template& data)
{
	Name = data.Name;
	Note = data.Note;
	Date = data.Date;

	NCompMean = data.NCompMean;
	HiperMean = data.HiperMean;
	NHarmMean = data.NHarmMean;
	KfnlMean = data.KfnlMean;
	SpecLeftMean = data.SpecLeftMean;
	SpecRightMean = data.SpecRightMean;
	PrjSpecMin = data.PrjSpecMin;
	PrjSpecMax = data.PrjSpecMax;
	PrjResol = data.PrjResol;
	PrjZeroFilling = data.PrjZeroFilling;
	LastCrit = data.LastCrit;

	ExcPoints.clear();
	copy(data.ExcPoints.begin(), data.ExcPoints.end(), inserter(ExcPoints, ExcPoints.begin()));

	Steps.clear();
	copy(data.Steps.begin(), data.Steps.end(), inserter(Steps, Steps.begin()));
}

void Template::SetPrjSettings(CProjectData* Prj)
{
	PrjSpecMin = Prj->GetLowLimSpecRange();
	PrjSpecMax = Prj->GetUpperLimSpecRange();
	PrjResol = Prj->GetSubCanalResolution();
	PrjZeroFilling = Prj->GetZeroFillingInd();
}

bool Template::IsUseSEV()
{
	ItVStep it;
	for(it=Steps.begin(); it!=Steps.end(); ++it)
	{
		if(it->SortCrit == C_CRIT_SEV || it->SortCrit == C_CRIT_ERR || it->SortCrit == C_CRIT_SDV || it->SortCrit == C_CRIT_R2SEV ||
		   it->SortCrit == C_CRIT_K1 || it->SortCrit == C_CRIT_K3 || it->SortCrit == C_CRIT_K7)
		    return true;
	}

	return false;
}

bool Template::CheckPrjSettings(CProjectData* Prj)
{
	if(PrjSpecMin != Prj->GetLowLimSpecRange() || PrjSpecMax != Prj->GetUpperLimSpecRange() ||
	   PrjResol != Prj->GetSubCanalResolution() ||	PrjZeroFilling != Prj->GetZeroFillingInd())
		return false;

	return true;
}

void Template::GetExcPoints(SInt& points)
{
	points.clear();
	copy(ExcPoints.begin(), ExcPoints.end(), inserter(points, points.begin()));
}

COptimTemplate::COptimTemplate()
{
	TCHAR buf[PATH_LENGTH];
	GetCurrentDirectory(PATH_LENGTH, buf);
	CString s(buf);

	BaseDefName = MakePath(s, _T("\\Base\\defname.tof"));
	BaseName = cEmpty;
}

COptimTemplate::~COptimTemplate()
{
	Templates.clear();
}

CString COptimTemplate::GetBaseDefName()
{
	return BaseDefName;
}

CString COptimTemplate::GetBaseName()
{
	return BaseName;
}

void COptimTemplate::SetBaseName(CString name)
{
	BaseName = name;
}

CString COptimTemplate::GetBaseNameShort()
{
	CString ShortName = BaseName;
	int len = ShortName.GetLength();

	ShortName.Delete(len-4, 4);
	int res = ShortName.ReverseFind('\\');
	ShortName.Delete(0, res+1);

	return ShortName;
}

bool COptimTemplate::IsLoaded()
{
	if(BaseName.Compare(cEmpty))
		return true;

	return false;
}

int COptimTemplate::GetNTemplates()
{
	return int(Templates.size());
}

Template* COptimTemplate::GetTemplate(int ind)
{
	if(ind < 0 || ind >= GetNTemplates())
		return NULL;

	return &Templates[ind];
}

Template* COptimTemplate::GetTemplate(CString name)
{
	ItTmpl it;
	for(it=Templates.begin(); it!=Templates.end(); ++it)
		if(!name.Compare(it->Name))
			return &(*it);

	return NULL;
}

bool COptimTemplate::AddTemplate(Template* tmpl)
{
	if(GetTemplate(tmpl->Name) != NULL)
		return false;

	tmpl->Date = CTime::GetCurrentTime();

	tmpldb::PRTemplate Obj;
	GetDataForBase((*tmpl), Obj);
	if(!Database.TemplateAddEx(cEmpty, &Obj))
		return false;

	Templates.push_back(*tmpl);

	return true;
}

bool COptimTemplate::ChangeTemplate(int ind)
{
	Template* Tmpl = GetTemplate(ind);
	if(Tmpl == NULL)
		return false;

	Tmpl->Date = CTime::GetCurrentTime();

	tmpldb::PRTemplate Obj;
	GetDataForBase((*Tmpl), Obj);
	if(!Database.TemplateAddEx(Tmpl->Name, &Obj))
		return false;

	return true;
}

bool COptimTemplate::DeleteTemplate(int ind)
{
	if(ind < 0 || ind >= GetNTemplates())
		return false;

	ItTmpl it = Templates.begin() + ind;
	if(!Database.TemplateDelete(it->Name))
		return false;

	Templates.erase(it);

	return true;
}

bool COptimTemplate::DeleteTemplate(CString name)
{
	ItTmpl it;
	for(it=Templates.begin(); it!=Templates.end(); ++it)
		if(!name.Compare(it->Name))
		{
			if(!Database.TemplateDelete(it->Name))
				return false;

			Templates.erase(it);
			return true;
		}

	return false;
}

void COptimTemplate::DeleteAllTemplates()
{
	Templates.clear();
}

void COptimTemplate::GetDataForBase(Template& Tmpl, tmpldb::PRTemplate& Obj)
{
	Obj.TemplateName = Tmpl.Name;
	Obj.TemplateNote = Tmpl.Note;
	Obj.DateCreated = Tmpl.Date;
	Obj.NCompMean = Tmpl.NCompMean;
	Obj.HiperMean = Tmpl.HiperMean;
	Obj.NHarmMean = Tmpl.NHarmMean;
	Obj.KfnlMean = Tmpl.KfnlMean;
	Obj.SpecLeftMean = Tmpl.SpecLeftMean;
	Obj.SpecRightMean = Tmpl.SpecRightMean;
	Obj.PrjSpecMin = Tmpl.PrjSpecMin;
	Obj.PrjSpecMax = Tmpl.PrjSpecMax;
	Obj.PrjResol = Tmpl.PrjResol;
	Obj.PrjZeroFilling = Tmpl.PrjZeroFilling;
	Obj.LastCrit = Tmpl.LastCrit;

	Obj.SpDataExclude.clear();
	copy(Tmpl.ExcPoints.begin(), Tmpl.ExcPoints.end(), inserter(Obj.SpDataExclude, Obj.SpDataExclude.begin()));

	ItVStep it;
	Obj.Steps.clear();
	for(it=Tmpl.Steps.begin(); it!=Tmpl.Steps.end(); ++it)
	{
		tmpldb::PRTemplateStep Step;

		Step.StepNum = it->Num;
		Step.ModelType = it->ModelType;
		Step.SpType = it->SpType;
		Step.NCompPlus = it->NCompPlus;
		Step.NCompStep = it->NCompStep;
		Step.HiperPlus = it->HiperPlus;
		Step.HiperStep = it->HiperStep;
		Step.NHarmPlus = it->NHarmPlus;
		Step.NHarmStep = it->NHarmStep;
		Step.NHarmType = it->NHarmType;
		Step.KfnlPlus = it->KfnlPlus;
		Step.KfnlStep = it->KfnlStep;
		Step.KfnlType = it->KfnlType;
		Step.SpecLeftPlus = it->SpecLeftPlus;
		Step.SpecLeftStep = it->SpecLeftStep;
		Step.SpecRightPlus = it->SpecRightPlus;
		Step.SpecRightStep = it->SpecRightStep;
		Step.MaxModels = it->MaxModels;
		Step.MainCrit = it->MainCrit;
		Step.SortCrit = it->SortCrit;
		Step.IsTest = it->IsTest;
		Step.Operation = it->Operation;

		Step.Preproc.pA = it->prepsets.pA;
		Step.Preproc.pB = it->prepsets.pB;
		Step.Preproc.pM = it->prepsets.pM;
		Step.Preproc.pD = it->prepsets.pD;
		Step.Preproc.pC = it->prepsets.pC;
		Step.Preproc.pN = it->prepsets.pN;
		Step.Preproc.p1 = it->prepsets.p1;
		Step.Preproc.p2 = it->prepsets.p2;
		Step.Preproc.pNo = it->prepsets.pNo;
		Step.Preproc.pUseBest = it->prepsets.UseBest;
		Step.Preproc.pMax = it->prepsets.MaxLen;
		Step.Preproc.pBest = it->prepsets.NumBest;
		Step.Preproc.pAdd = it->prepsets.GetAddsAsStr();

		Step.Outs.chMah = it->outs.chMah;
		Step.Outs.chSEC = it->outs.chSEC;
		Step.Outs.chSECV = it->outs.chSECV;
		Step.Outs.chBad = it->outs.chBad;
		Step.Outs.chBound = it->outs.chBound;
		Step.Outs.limMah = it->outs.limMah;
		Step.Outs.limSEC = it->outs.limSEC;
		Step.Outs.limSECV = it->outs.limSECV;
		Step.Outs.limBound = it->outs.limBound;

		Obj.Steps.push_back(Step);
	}
}

void COptimTemplate::SetDataFromBase(Template& Tmpl, tmpldb::PRTemplate& Obj)
{
	Tmpl.Name = Obj.TemplateName;
	Tmpl.Note = Obj.TemplateNote;
	Tmpl.Date = Obj.DateCreated;
	Tmpl.NCompMean = Obj.NCompMean;
	Tmpl.HiperMean = Obj.HiperMean;
	Tmpl.NHarmMean = Obj.NHarmMean;
	Tmpl.KfnlMean = Obj.KfnlMean;
	Tmpl.SpecLeftMean = Obj.SpecLeftMean;
	Tmpl.SpecRightMean = Obj.SpecRightMean;
	Tmpl.PrjSpecMin = Obj.PrjSpecMin;
	Tmpl.PrjSpecMax = Obj.PrjSpecMax;
	Tmpl.PrjResol = Obj.PrjResol;
	Tmpl.PrjZeroFilling = Obj.PrjZeroFilling;
	Tmpl.LastCrit = Obj.LastCrit;

	Tmpl.ExcPoints.clear();
	copy(Obj.SpDataExclude.begin(), Obj.SpDataExclude.end(), inserter(Tmpl.ExcPoints, Tmpl.ExcPoints.begin()));

	Tmpl.Steps.clear();
	for(vector<tmpldb::PRTemplateStep>::const_iterator it=Obj.Steps.begin(); it!=Obj.Steps.end(); ++it)
	{
		TemplateStep NewStep;

		NewStep.Num = it->StepNum;
		NewStep.ModelType = it->ModelType;
		NewStep.SpType = it->SpType;
		NewStep.NCompPlus = it->NCompPlus;
		NewStep.NCompStep = it->NCompStep;
		NewStep.HiperPlus = it->HiperPlus;
		NewStep.HiperStep = it->HiperStep;
		NewStep.NHarmPlus = it->NHarmPlus;
		NewStep.NHarmStep = it->NHarmStep;
		NewStep.NHarmType = it->NHarmType;
		NewStep.KfnlPlus = it->KfnlPlus;
		NewStep.KfnlStep = it->KfnlStep;
		NewStep.KfnlType = it->KfnlType;
		NewStep.SpecLeftPlus = it->SpecLeftPlus;
		NewStep.SpecLeftStep = it->SpecLeftStep;
		NewStep.SpecRightPlus = it->SpecRightPlus;
		NewStep.SpecRightStep = it->SpecRightStep;
		NewStep.MaxModels = it->MaxModels;
		NewStep.MainCrit = it->MainCrit;
		NewStep.SortCrit = it->SortCrit;
		NewStep.IsTest = it->IsTest;
		NewStep.Operation = it->Operation;

		NewStep.prepsets.pA = it->Preproc.pA;
		NewStep.prepsets.pB = it->Preproc.pB;
		NewStep.prepsets.pM = it->Preproc.pM;
		NewStep.prepsets.pD = it->Preproc.pD;
		NewStep.prepsets.pC = it->Preproc.pC;
		NewStep.prepsets.pN = it->Preproc.pN;
		NewStep.prepsets.p1 = it->Preproc.p1;
		NewStep.prepsets.p2 = it->Preproc.p2;
		NewStep.prepsets.pNo = it->Preproc.pNo;
		NewStep.prepsets.UseBest = it->Preproc.pUseBest;
		NewStep.prepsets.MaxLen = it->Preproc.pMax;
		NewStep.prepsets.NumBest = it->Preproc.pBest;
		NewStep.prepsets.SetAddsFromStr(it->Preproc.pAdd);

		NewStep.outs.chMah = it->Outs.chMah;
		NewStep.outs.chSEC = it->Outs.chSEC;
		NewStep.outs.chSECV = it->Outs.chSECV;
		NewStep.outs.chBad = it->Outs.chBad;
		NewStep.outs.chBound = it->Outs.chBound;
		NewStep.outs.limMah = it->Outs.limMah;
		NewStep.outs.limSEC = it->Outs.limSEC;
		NewStep.outs.limSECV = it->Outs.limSECV;
		NewStep.outs.limBound = it->Outs.limBound;

		Tmpl.Steps.push_back(NewStep);
	}
}

bool COptimTemplate::LoadDatabase()
{
	Templates.clear();

	tmpldb::PRTemplate Obj;

	if(!Database.TemplatesRead())
		return false;

	while(!Database.IsEOF())
	{
		if(!Database.TemplateGet(&Obj))
			break;

		Template Tmpl;

		SetDataFromBase(Tmpl, Obj);

		Templates.push_back(Tmpl);

		Database.MoveNext();
	}
	Database.Free();

	return true;
}

bool COptimTemplate::LoadBase(CString fname, bool IsMsg)
{
	ParcelWait(true);

	Database.Close();

	if(!Database.Open(fname))
	{
		ParcelWait(false);
		if(IsMsg)
			ParcelError(ParcelLoadString(STRID_TEMPLATE_MANAGER_ERROR_DBNOOPEN));
		return false;
	}
	if(!Database.CheckDatabase())
	{
		ParcelWait(false);
		if(IsMsg)
			ParcelError(ParcelLoadString(STRID_TEMPLATE_MANAGER_ERROR_DBINCORRVERS));
		Database.Close();
		BaseName = cEmpty;
		return false;
	}
	if(!LoadDatabase())
	{
		ParcelWait(false);
		if(IsMsg)
			ParcelError(ParcelLoadString(STRID_TEMPLATE_MANAGER_ERROR_NOLOAD));
		Database.Close();
		BaseName = cEmpty;
		return false;
	}

	SetBaseName(fname);

	ParcelWait(false);

	return true;
}
