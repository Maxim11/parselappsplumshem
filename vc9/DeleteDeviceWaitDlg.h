#ifndef _DELETE_DEVICE_WAIT_DLG_H_
#define _DELETE_DEVICE_DLG_H_

#pragma once

#include "WaitingDlg.h"


///////////////////////////////////////////////////////////////////////////////////////////////////
// ������ ��������� �������� ���������� ����������������� �������

class CDeleteDeviceWaitDlg : public CWaitingDlg
{
	DECLARE_DYNAMIC(CDeleteDeviceWaitDlg)

public:

	CDeleteDeviceWaitDlg(int number, CWnd* pParent = NULL);		// �����������
	virtual ~CDeleteDeviceWaitDlg();							// ����������

	enum { IDD = CWaitingDlg::IDD };	// ������������� �������

protected:

	int Number;

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();						// ������������� �������

	virtual void OK();				// ��������� �������� ����� �� �������
	virtual void Stop();			// ��������� ����������� ����� �� �������
	virtual void Work();			// ������ �������� �������

	DECLARE_MESSAGE_MAP()
};

#endif
