#include "stdafx.h"

#include "ImitDeviceDlg.h"
#include "MainFrm.h"


IMPLEMENT_DYNAMIC(CImitDeviceDlg, CDialog)

CImitDeviceDlg::CImitDeviceDlg(CDeviceData* data, int mode, CWnd* pParent)
	: CDialog(CImitDeviceDlg::IDD, pParent)
{
	DevData = data;

	Mode = mode;

	m_vType = Untype(DevData->GetType());
	m_vNumber = DevData->GetNumber();
}

CImitDeviceDlg::~CImitDeviceDlg()
{
}

void CImitDeviceDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_DEVICE_TYPE, m_Type);
	DDX_Control(pDX, IDC_DEVICE_NUMBER, m_Number);

	DDX_CBIndex(pDX, IDC_DEVICE_TYPE, m_vType);

	cParcelDDX(pDX, IDC_DEVICE_NUMBER, m_vNumber);
	cParcelDDV(pDX, ParcelLoadString(STRID_CREATE_DEVICE_DDV_NUMBER), m_vNumber, 0, 99999999);
}


BEGIN_MESSAGE_MAP(CImitDeviceDlg, CDialog)

	ON_BN_CLICKED(IDC_ACCEPT, OnAccept)
	ON_BN_CLICKED(IDHELP, OnHelp)

END_MESSAGE_MAP()


BOOL CImitDeviceDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	int i;
	CString s;

	CString Hdr;
	switch(Mode)
	{
	case IDEVDLG_MODE_CREATE:
		Hdr = ParcelLoadString(STRID_CREATE_DEVICE_HDR_CREATE);
		break;
	case IDEVDLG_MODE_CHANGE:
	case IDEVDLG_MODE_CHANGENUM:
		Hdr = ParcelLoadString(STRID_CREATE_DEVICE_HDR_CHANGE);
		break;
	}
	SetWindowText(Hdr);

	m_Type.ResetContent();
	for(i=0; i<C_DEN_NTYPES; i++)
	{
		s = cGetDeviceTypeName(Retype(i));
		m_Type.AddString(s);
	}
	m_Type.SetCurSel(m_vType);

	GetDlgItem(IDC_ACCEPT)->SetWindowText(ParcelLoadString(STRID_CREATE_DEVICE_ACCEPT));
	GetDlgItem(IDCANCEL)->SetWindowText(ParcelLoadString(STRID_CREATE_DEVICE_DECLINE));
	GetDlgItem(IDHELP)->SetWindowText(ParcelLoadString(STRID_CREATE_DEVICE_HELP));
	GetDlgItem(IDC_STATIC_1)->SetWindowText(ParcelLoadString(STRID_CREATE_DEVICE_TYPE));
	GetDlgItem(IDC_STATIC_2)->SetWindowText(ParcelLoadString(STRID_CREATE_DEVICE_NUMBER));

	if(Mode == IDEVDLG_MODE_CHANGENUM)
	{
		GetDlgItem(IDC_DEVICE_TYPE)->EnableWindow(false);
		GetDlgItem(IDC_STATIC_1)->EnableWindow(false);
	}

	GetDlgItem(IDHELP)->EnableWindow(pFrame->IsExistHelp());

	return true;
}

void CImitDeviceDlg::OnAccept()
{
	if(!UpdateData())
		return;

	if(Mode == IDEVDLG_MODE_CREATE || m_vNumber != DevData->GetNumber())
	{
		CDeviceData *dev = pRoot->GetDeviceByNum(m_vNumber);
		if(dev != NULL)
		{
			CString s;
			s.Format(ParcelLoadString(STRID_CREATE_DEVICE_ERROR_NAME), m_vNumber);
			ParcelError(s);
			return;
		}
	}
	if(m_vNumber == 0)
	{
		ParcelError(ParcelLoadString(STRID_CREATE_DEVICE_ERROR_NUMBER));
		return;
	}

	DevData->SetType(Retype(m_vType));
	DevData->SetNumber(m_vNumber);

	CDialog::OnOK();
}

void CImitDeviceDlg::OnCancel()
{
	if(Mode == IDEVDLG_MODE_CHANGE && IsChanged())
	{
		if(!ParcelConfirm(ParcelLoadString(STRID_CREATE_DEVICE_ASK_DOCANCEL), false))
			return;
	}

	CDialog::OnCancel();
}

bool CImitDeviceDlg::IsChanged()
{
	return true;
}

int CImitDeviceDlg::Retype(int ind)
{
	int itype = C_DEV_TYPE_10M;
	switch(ind)
	{
	case 0:  itype = C_DEV_TYPE_10M;  break;
	case 1:  itype = C_DEV_TYPE_10;  break;
	case 2:  itype = C_DEV_TYPE_40;  break;
	case 3:  itype = C_DEV_TYPE_02M;  break;
	case 4:  itype = C_DEV_TYPE_02;  break;
	case 5:  itype = C_DEV_TYPE_08;  break;
	case 6:  itype = C_DEV_TYPE_20;  break;
	case 7:  itype = C_DEV_TYPE_12;  break;
	case 8:  itype = C_DEV_TYPE_100;  break;
	case 9:  itype = C_DEV_TYPE_12M;  break;
	case 10:  itype = C_DEV_TYPE_IMIT;  break;
	}

	return itype;
}

int CImitDeviceDlg::Untype(int type)
{
	int ind = 0;
	switch(type)
	{
	case C_DEV_TYPE_10M:  ind = 0;  break;
	case C_DEV_TYPE_10:  ind = 1;  break;
	case C_DEV_TYPE_40:  ind = 2;  break;
	case C_DEV_TYPE_02M:  ind = 3;  break;
	case C_DEV_TYPE_02:  ind = 4;  break;
	case C_DEV_TYPE_08:  ind = 5;  break;
	case C_DEV_TYPE_20:  ind = 6;  break;
	case C_DEV_TYPE_12:  ind = 7;  break;
	case C_DEV_TYPE_100:  ind = 8;  break;
	case C_DEV_TYPE_12M:  ind = 9;  break;
	case C_DEV_TYPE_IMIT:  ind = 10;  break;
	}

	return ind;
}

void CImitDeviceDlg::OnHelp()
{
	int IdHelp = DLGID_MAIN;
	switch(Mode)
	{
	case IDEVDLG_MODE_CREATE: IdHelp = DLGID_DEVICE_CREATE;  break;
	case IDEVDLG_MODE_CHANGE: IdHelp = DLGID_DEVICE_CHANGE;  break;
	}

	pFrame->ShowHelp(IdHelp);
}
