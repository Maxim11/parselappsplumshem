////////////////////////////////////////////////////////////////////////////
/**
 * \file LumReport_types.h 
 * \brief �������� ����� ��� ���������� ���������� RptCreator.dll
 * \author �������� �������� i-gen@yandex.ru
 * \version 1.0
 * \date    2007
 *
 */
////////////////////////////////////////////////////////////////////////////
#ifndef _LUMEX_REPORT__ALL_TYPES_DEFINED_H__
#define _LUMEX_REPORT__ALL_TYPES_DEFINED_H__

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include <string>
#include <sstream>
#include <fstream>

/**
 * \namespace lumreport ����� ������������ ���������� �������
 */
namespace lumreport
{


#ifdef _UNICODE
	typedef wchar_t					tchar;
	typedef std::wstring			tstring;
	typedef std::wistringstream		tistringstream;
	typedef std::wostringstream		tostringstream;
	typedef std::wofstream			tofstream;
	typedef std::wifstream			tifstream;
#else
	typedef char					tchar;
	typedef std::string				tstring;
	typedef std::istringstream		tistringstream;
	typedef std::ostringstream		tostringstream;
	typedef std::ofstream			tofstream;
	typedef std::ifstream			tifstream;
#endif

	typedef unsigned int			uint;
	typedef tstring					RObjID;
	typedef tstring					RText;
	typedef const wchar_t*			CWSTR;
	typedef unsigned long			Index;
	typedef unsigned long			ErrCode;
	typedef unsigned long			WidgetType;
	typedef unsigned long			Unit;
	typedef unsigned char			RAlign;
	typedef unsigned char			IncludeType;
	typedef unsigned char			byte;
	typedef unsigned char			OrientationType;



#ifndef interface
#define interface struct
#endif


//-------------------------------------------------------------------------
/**
 * \defgroup error_codes ���� ������ ��� ������ � ����������� LumReportGen.dll
 */
/*@{*/
//-------------------------------------------------------------------------
const ErrCode RERROR_OK						= 0;	///< �������� ��������� �������
const ErrCode RERROR_INIT_FAIL				= 1;	///< ������ ��� �������������
const ErrCode RERROR_BAD_ARGUMENT			= 2;	///< ��������� �������� ���������
const ErrCode RERROR_BAD_FILE				= 3;	///< ������ ��� ������/������ �����
const ErrCode RERROR_SYSTEM_ERROR			= 4;	///< ��������� ������
const ErrCode RERROR_NOT_SUPPORTED			= 5;	///< ������������� ����������� �� ��������������
const ErrCode RERROR_LAYOUT_DOESNOT_EXIST	= 6;	///< �� ������ ������ (����)
const ErrCode RERROR_UNKNOWN_WIDGET			= 7;	///< ����������� ������
//-------------------------------------------------------------------------
/*@}*/


//-------------------------------------------------------------------------
/**
* \defgroup widget_types ���� �������������� ��������
*/
/*@{*/
//-------------------------------------------------------------------------
const WidgetType WGT_UNKNOWN				= 0;			///< ����������� ������
const WidgetType WGT_STATIC_TEXT			= 1;			///< ��������� ������ � ������������� ������ �� ����� �������� �������
const WidgetType WGT_DYNAMIC_TEXT			= 1 << 1;		///< ��������� ������ � ������������� ������ �� ����� �������� ������
const WidgetType WGT_STATIC_PICTURE			= 1 << 2;		///< ������ � ���������, �������������� �� ����� �������� �������
const WidgetType WGT_DYNAMIC_PICTURE		= 1 << 3;		///< ������ � ���������, �������������� �� ����� �������� ������
const WidgetType WGT_TABLE					= 1 << 4;		///< ������ � ��������
const WidgetType WGT_INPUT_BOX				= 1 << 5;		///< ������ � ����� ����� � ������
const WidgetType WGT_LABELED_DYNAMIC_TEXT	= 1 << 6;		///< ������ � ����� ��������� �����
//-------------------------------------------------------------------------
/*@}*/



//-------------------------------------------------------------------------
/**
* \struct Wuid
* \brief ���������� ������������� ������� (��� ���������� �����)
*
* ������ � ���������� ������ widget_id ����� ���������
* ��� ����������� � ��������� (� ������ ���������� � 
* ��������� counter)
*/
struct Wuid
{
	CWSTR	widget_id;		///< ���������� ��� �������
	Index	counter;		///< ����� ����������

	/// .ctor
	Wuid( CWSTR wid = 0, Index c = 0 ) : widget_id(wid), counter(c){}
};
//-------------------------------------------------------------------------


//-------------------------------------------------------------------------
/**
* \struct Version
* \brief ������ ��������
*/
struct Version
{
	Index	major;		///< ������� �����
	Index	minor;		///< ������� �����
	Index	build;		///< ����� ������

	/// .ctor
	Version( Index ma = 0L, Index mi = 0L, Index bl = 0L ) : major(ma), minor(mi), build(bl){}

	/// �������� ������
	bool operator<( Version const& rhs )const
	{
		if( major > rhs.major ) return false;
		else if( major < rhs.major ) return true;

		if( minor > rhs.minor ) return false;
		else if( minor < rhs.minor ) return true;

		return build < rhs.build;
	}

	/// �������� ��������� �� ���������
	bool operator==( Version const& rhs )const
	{
		return (major == rhs.major)
			&& (minor == rhs.minor)
			&& (build == rhs.build);
	}	
};
//-------------------------------------------------------------------------


//-------------------------------------------------------------------------
/**
* \struct Color
* \brief ����
*/
struct Color 
{
	byte r;			///< �������
	byte g;			///< �������
	byte b;			///< �����
	byte alpha;		///< ������������

	/// .ctor
	Color( byte r_ = 0, byte g_ = 0, byte b_ = 0, byte a_ = 0 )	: r(r_), g(g_), b(b_), alpha(a_){}

	/// copy .ctor
	Color( COLORREF clr ) : r(GetRValue(clr)), g(GetGValue(clr)), b(GetBValue(clr)), alpha(0){}

	/// �������� ��������� 
	friend bool operator==( const Color& lhs, const Color& rhs )
	{
		return (lhs.r == rhs.r) && (lhs.g == rhs.g) && (lhs.b == rhs.b);
	}

	/// �������� �����������
	friend bool operator!=( const Color& lhs, const Color& rhs )
	{
		return (lhs.r != rhs.r) || (lhs.g != rhs.g) || (lhs.b != rhs.b);
	}

};
//-------------------------------------------------------------------------




//-------------------------------------------------------------------------
/**
* \struct FontInfo
* \brief ����� � ��� ��������
*/
struct FontInfo
{
	CWSTR	family;			///< ���������
	Index	size;			///< ������
	Color	forecolor;		///< ���� ������
	Color	bkgdcolor;		///< ���� ����
	bool	bold;			///< ������ ��� ���
	bool	italic;			///< ��������� ��� ���
	bool	underline;		///< ������������ ��� ���
	bool	strike;			///< ����������� ��� ���
	bool	can_modify;		///< ����� �� ���������� ������������� � ��������� ��������
}; // Font
//-------------------------------------------------------------------------


//-------------------------------------------------------------------------
/**
* \struct PaletteInfo
* \brief ��������� ���������� �� ������� ��� ���������� ����� (UTF-16)
*/
struct PaletteInfo
{
	CWSTR		palette_id;				///< ���������� ����� �������
	CWSTR		short_name;				///< ������� ���
	Version		application_version;	///< ������ ����������, ������������� �����
	Version		editor_version;			///< ������ ��������� ��������
	CWSTR		owner;					///< �������� �������
	CWSTR		appname;				///< ��� ����������, ������������� �����
	CWSTR		description;			///< ������� �������� (�� ����������)
	Index		widget_count;			///< ���������� ��������
	Index		language_count;			///< ���������� �������������� ������
	FontInfo	document_font;			///< ����� ����� ��������� (������)
};
//-------------------------------------------------------------------------


//-------------------------------------------------------------------------
/**
* \struct Size
* \brief �������� �������
*/
struct Size
{
	double x;	///< �� ��� �
	double y;	///< �� ��� y

	/// .ctor
	Size( double xx = 0.0, double yy = 0.0 ) : x(xx), y(yy){}
};
//-------------------------------------------------------------------------


const Unit unitPixel	= 1;	///< ������� ����������� � ��������
const Unit unitPercent	= 2;	///< ������� ����������� � ���������
//-------------------------------------------------------------------------
/**
* \struct WidgetSize
* \brief ������� �������
*/
struct WidgetSize
{
	Size min;		///< ����������� �������
	Size max;		///< ������������ �������
	Size def;		///< ��������� (�� ���������)
	Unit unit;		///< � ����� ��������

	/// .ctor
	WidgetSize( Unit unt = unitPercent ) : unit(unt){}

	bool IsInPixel()const		{ return unit == unitPixel; }
	bool IsInPercent()const		{ return unit == unitPercent; }
};
//-------------------------------------------------------------------------



//-------------------------------------------------------------------------
/**
* \struct WidgetInfo
* \brief\ ��������� ���������� �� ������� ��� ���������� ����� (UTF-16)
*/
struct WidgetInfo
{
	WidgetType	type;					///< ��� �������
	Wuid		wuid;					///< ������������� �������
	CWSTR		short_name;				///< ������� ���
	CWSTR		description;			///< ������� ��������
	bool		mandatory;				///< ������� ��������������
	bool		resizeable;				///< ������� ����������������
	bool		ratio;					///< ������� ����, ��� ������� ����� ���������� ������ ���������������
	WidgetSize	size;					///< ������������ ������ ��� 'static_picture', 'dynamic_picture', 'table'
	FontInfo	font;					///< ������������ ������ ��� 'static_text', 'dynamic_text', 'input_box', ��� 'table' ����� ���������� ���������� ������ � ������� ������ �������, ��� 'labeled_dynamic_text' - ��� �������������� �����, ��� 'table' ����� ���������� ���������� ������ � ������� ������ �������
	CWSTR		dataex;					///< �������� ������� ����� ��������� ��� ���� ����������

	bool IsStaticText()const			{ return type == WGT_STATIC_TEXT; }
	bool IsDynamicText()const			{ return type == WGT_DYNAMIC_TEXT; }
	bool IsStaticPicture()const			{ return type == WGT_STATIC_PICTURE; }
	bool IsDynamicPicture()const		{ return type == WGT_DYNAMIC_PICTURE; }
	bool IsTable()const					{ return type == WGT_TABLE; }
	bool IsInputBox()const				{ return type == WGT_INPUT_BOX; }
	bool IsLabeledDynamicText()const	{ return type == WGT_LABELED_DYNAMIC_TEXT; }
};
//-------------------------------------------------------------------------


//-------------------------------------------------------------------------
/**
* \struct TableInfo
* \brief ��������� ���������� �� ������� (�������) ��� ���������� ����� (UTF-16)
*/
struct TableInfo
{
	Index		column_count;			///< ���������� ��������
	Index		row_count;				///< ���������� ����� (��� ������������� ����)
	Index		border;					///< ������� ����� (������ �����)
	Index		cellspacing;			///< ����������� ����������
	Index		cellpadding;			///< ������ �� ������� ������ �� ����� � �������
	FontInfo	caption_font;			///< ����� ��� ������������ ������� ��� ��������
	FontInfo	header_font;			///< ����� ��� ������������� ���� �������
	FontInfo	body_font;				///< ����� ��� ����� � ������� �������
	FontInfo	note_font;				///< ����� ��� ��������� ��� ��������
	bool		modify_caption;			///< ����� �� �������� ������������ ������� � �������
	bool		modify_note;			///< ����� �� �������� ������ �������� ��� ��������
	CWSTR		caption;				///< ���������
	CWSTR		note;					///< ����������
	RAlign		caption_halign;			///< �������������� ������������ ������������� ������ ��� ��������
	RAlign		note_halign;			///< �������������� ������������ �������������� ������ ��� ��������
};
//-------------------------------------------------------------------------


const OrientationType oLeftToRight	= 1;	///< ���������� ���� ��������� ������ � ��������� �������: ����� + ������
const OrientationType oTopToBottom	= 2;	///< ���������� ���� ��������� ������ � ��������� �������: ������� + ������
//-------------------------------------------------------------------------



//-------------------------------------------------------------------------
/**
* \struct LabeledTextInfo
* \brief ��������� ���������� �� ������� (WGT_LABELED_DYNAMIC_TEXT) ��� ���������� ����� (UTF-16)
*/
struct LabeledTextInfo
{
	FontInfo		label_font;				///< ����� ��� ������������ ������� (������. �����)
	FontInfo		text_font;				///< ����� ��� ������, ������� ����� ����������� ��� ��������� ������
	CWSTR			label_text;				///< ����������� ������� � ������
	CWSTR			dummy_text;				///< ��� ��������� ��������: ��������� �������������� �����
	OrientationType	orientation;			///< \a oLeftToRight - ����� �������, � ������ �����. \a oTopToBottom - ������ �������, � ����� �����
	double			label_width;			///< ������ ��� oLeftToRight ( width_of( label + text ) = 100% )
	RAlign			label_halign;			///< �������������� ������������ ����������. �������
	RAlign			text_halign;			///< �������������� ������������ ��������������� ������
	bool			is_dynamic_label;		///< ����� �� ����� ��� label ��������������� ����������� �����������
};
//-------------------------------------------------------------------------






const RAlign vTop		= 1;	///< ������������ ����������: � �������� ����
const RAlign vMiddle	= 2;	///< ������������ ����������: � ��������
const RAlign vBottom	= 3;	///< ������������ ����������: � ������� ����
const RAlign hLeft		= 4;	///< �������������� ����������: � ������ ����
const RAlign hCenter	= 5;	///< �������������� ����������: �� ������
const RAlign hRight		= 6;	///< �������������� ����������: � ������� ����

//-------------------------------------------------------------------------
/**
* \struct WidgetAlign
* \brief ��������� ������������ �������
*/

struct WidgetAlign
{
	RAlign horizontal;	///< ��������������
	RAlign vertical;	///< ������������

	/// �����������
	WidgetAlign( RAlign h = hLeft, RAlign v = vTop ) : horizontal(h), vertical(v){}
};
//-------------------------------------------------------------------------


const IncludeType CELL_IS_EMPTY		= 0;	///< ���������� ������ ����: �����
const IncludeType CELL_HAS_WIDGET	= 1;	///< ���������� ������ ����: ������
const IncludeType CELL_HAS_HLAYOUT	= 2;	///< ���������� ������ ����: �������������� ����
const IncludeType CELL_HAS_VLAYOUT	= 3;	///< ���������� ������ ����: ������������ ���� (���� �� ������������)
//-------------------------------------------------------------------------



//-------------------------------------------------------------------------
/**
* \struct LayoutCellInfo
* \brief ���������� � ������ �������, ������� �������� ���� ������ ���� ������ ������
*/
struct LayoutCellInfo
{
	CWSTR		ref_id;			///< ������������� ����, ��� ���������� � ������ (������ ��� ������)
	Index		ref_counter;	///< ������� (counter) ����, ��� ���������� � ������ (����� ����� ������ ��� �������)
	IncludeType	include_type;	///< ��� ���������� � ������ (���)
	WidgetAlign	align;			///< ������������ � ������ �����������
	double		width;			///< ������ (� ��������� �� ������ ���������)
	double		height;			///< ������ (���� �� ������������)
	CWSTR		dataex;			///< �������� ������� ����� ��������� ��� ���� ����������
};
//-------------------------------------------------------------------------


//-------------------------------------------------------------------------
/**
* \struct HLayout
* \brief �������������� "������������" ���������� �����
*/
struct HLayout
{
	CWSTR				layout_id;		///< ������������� ����
	Index				cell_count;		///< ���������� ����� (LayoutCellInfoExA) � ������� �� ��������� m_cell_array
	LayoutCellInfo*		cell_array;		///< ��������� �� ������ ����� (���������� ����� � ������� ����� m_cell_count)
	CWSTR				dataex;			///< �������� ������� ����� ��������� ��� ���� ����������
};
//-------------------------------------------------------------------------



//-------------------------------------------------------------------------
/**
* \struct TemplateInfo
* \brief ��������� ���������� �� ������� ��� ���������� �����
*/
struct TemplateInfo
{
	PaletteInfo		palette_info;		///< ���������� � �������
	CWSTR			language;			///< ���� �������
	Index			layout_count;		///< ����� �������������� �����
	Index			widget_count;		///< ����� �������� (����� � �������)
	Version			editor_version;		///< ������ ��������� �������
	double			view_scale;			///< ����������� ���������������� ������������� ������� � ���������
	CWSTR			dataex;				///< �������� ������� ����� ��������� ��� ���� ����������
};
//-------------------------------------------------------------------------




}	//namespace lumreport


#endif // _LUMEX_REPORT__ALL_TYPES_DEFINED_H__
