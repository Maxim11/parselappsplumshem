////////////////////////////////////////////////////////////////////////////
/**
* \file LumReport_Api.h 
* \brief ��������� ������ ������� ����� ����������� � DLL ��� ��������� �������
* \author �������� �������� i-gen@yandex.ru
* \version 1.0
* \date    2007
*
*/
////////////////////////////////////////////////////////////////////////////
#ifndef _LUMEX_REPORT__API_DEFINED_H__
#define _LUMEX_REPORT__API_DEFINED_H__

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "LumReport_types.h"

#ifdef REPORT_DLL
#define REPORT_DLL_API extern "C" __declspec (dllexport)
#else
#define REPORT_DLL_API extern "C" __declspec (dllimport)
#endif


namespace lumreport
{

//-------------------------------------------------------------------------
/**
* \struct TableHint
* \brief ���������� �� �������� �������, ��������������� � ��������-��������
*/
struct TableHint
{
	Index		col_count;	///< ���������� ��������
	Index		row_count;	///< ���������� ����� (��� ���������)
	CWSTR		caption;	///< ������ ��� ��������
	CWSTR		note;		///< ����������� - ��������� (��� ��������)

	/// .ctor
	TableHint( Index cols = 0, Index rows = 0, CWSTR c = NULL, CWSTR n = NULL ) 
		: col_count(cols)
		, row_count(rows)
		, caption( c )
		, note( n ){}
};
//-------------------------------------------------------------------------


//-------------------------------------------------------------------------
/**
* \struct ColumnHint
* \brief ���������� �� ������� �������
*/
struct ColumnHint
{
	RAlign		halign;		///< �������������� ������������ ������ � ������ ������ �������
	CWSTR		header;		///< ������ � ��������� �������

	/// .ctor
	ColumnHint( RAlign ha = hLeft, CWSTR h = NULL ) : halign(ha), header(h){}
};
//-------------------------------------------------------------------------



//-------------------------------------------------------------------------
/**
* \struct TextListItem
* \brief ������� �������� ������
*
* �������� ����� � ��������� �� ��������� ������� ������. ��������� ������� ������ ��������� ������� ��������� �� ���������
*/
struct TextListItem
{
	CWSTR			text;	///< ��������� ������
	TextListItem*	next;	///< ��������� �� ���������

	// .ctor
	TextListItem( CWSTR txt = NULL, TextListItem* ptr = NULL ) : text(txt), next(ptr){}

	bool IsLast()const	{ return next == NULL; }
	bool IsEmpty()const { return text == NULL; }
};
//-------------------------------------------------------------------------



//-------------------------------------------------------------------------
/**
* \struct IAppCallback
* \brief �������, ����������� �����������.
* 
* ����� ���������� ��� ��������� ������ �� DLL
*/
interface IAppCallback
{
	/// ��������� ��������� ������ ��� "dynamic_text" �� Wuid �������
	virtual CWSTR	GetTextFor( Wuid wuid )const = 0;

	/// ��������� ������� ��������� ����� ��� "labeled_text" � ��.
	virtual TextListItem const* GetTextArrayFor( Wuid wuid )const = 0;

	/// �������� ������ �� �������� ��� ������� � Wuid
	virtual CWSTR	GetPicturePathFor( Wuid wuid )const = 0;

	/// ��������� ���������� �� �������� ������� ��� ������� � Wuid
	virtual TableHint const* GetTableHintFor( Wuid wuid )const = 0;

	/// ������ � ���������� � �������� column (������ �� ����)  ��� ������� � ������� �� Wuid
	virtual ColumnHint const* GetTableHeaderHintFor( Wuid wuid, Index column )const = 0;

	/// ������ �� ������ ��� �������:
	virtual CWSTR GetTableCellFor( Wuid wuid, Index row, Index column )const = 0;

	/// ��������� ���������� � �����, �� ������� ������� ������������� ������, ��� ������� ����� ������� ������ (�� ��������� ������)
	virtual void SetReportLanguage( CWSTR language ) = 0;

}; // interface IAppCallback
//-------------------------------------------------------------------------



//-------------------------------------------------------------------------
/**
* \struct ICreatorApi
* \brief �������, �������������� DLL-� ��� ��������� �������
*/
interface ICreatorApi
{
	/**
	 * \brief ��������� ���� ������ ��� ��������� ������� API
	 */ 
	virtual ErrCode GetLastError() = 0;

	/**
	 * \brief ��������� ���������� �� ������ ��� ��������� ������� API
	 */ 
	virtual CWSTR GetErrorInfo( ErrCode err ) = 0;

	/**
	 * \brief �������� ������ �� �������
	 *
	 * true - �������, false - ���� (����������� � GetLastError)
	 */ 
	virtual bool CreateReport( CWSTR template_path, CWSTR report_path, IAppCallback* pAppImpl ) = 0;


};
//-------------------------------------------------------------------------


} // namespace lumreport
 

/**
* \addgroup dll_exported_function �������������� ������� �����������
*/
/*@{*/

/// ������ ���������� ��� �������� ������� �� ��������:
REPORT_DLL_API lumreport::ICreatorApi* REPORTGEN_QueryCreatorApi();

/// ������� � ����� ������ ���������� ���������� �����
REPORT_DLL_API void REPORTGEN_ReleaseCreatorApi();

/*@}*/


#endif // _LUMEX_REPORT__API_DEFINED_H__
