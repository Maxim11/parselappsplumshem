#include "stdafx.h"

#include "ConvertWaitDlg.h"
#include "BranchInfo.h"

IMPLEMENT_DYNAMIC(CConvertWaitDlg, CWaitingDlg)

CConvertWaitDlg::CConvertWaitDlg(CDeviceData* dev, bool sst, CWnd* pParent) : CWaitingDlg(pParent)
{
	DevData = dev;
	IsSST = sst;

	Comment = ParcelLoadString(STRID_CONVERT_WAIT_COMMENT);
}

CConvertWaitDlg::~CConvertWaitDlg()
{
}

void CConvertWaitDlg::DoDataExchange(CDataExchange* pDX)
{
	CWaitingDlg::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CConvertWaitDlg, CWaitingDlg)
END_MESSAGE_MAP()


BOOL CConvertWaitDlg::OnInitDialog()
{
	CWaitingDlg::OnInitDialog();

	return true;
}

void CConvertWaitDlg::OK()
{
	CWaitingDlg::OK();
}

void CConvertWaitDlg::Stop()
{
	CWaitingDlg::Stop();
}

void CConvertWaitDlg::Work()
{
	ParcelWait(true);
	
	bool res = DevData->AcceptOldApj(IsSST);

	ParcelWait(false);

	if(res)
		OK();
	else
		Stop();
}
