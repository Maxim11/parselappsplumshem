#ifndef _WAITING_DLG_H_
#define _WAITING_DLG_H_

#pragma once

#include "Resource.h"


///////////////////////////////////////////////////////////////////////////////////////////////////
// ������� ������ ��������� ��������� ������

class CWaitingDlg : public CDialog
{
	DECLARE_DYNAMIC(CWaitingDlg)

public:

	CWaitingDlg(CWnd* pParent = NULL);		// �����������
	virtual ~CWaitingDlg();					// ����������

	enum { IDD = IDD_WAITING };				// ������������� �������

protected:

	CString Comment;						// �����������

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();						// ������������� �������

	virtual void OnOK()
	{};
	virtual void OnCancel()
	{};

	virtual void OK();				// ��������� �������� ����� �� �������
	virtual void Stop();			// ��������� ����������� ����� �� �������
	virtual void Work();			// ������ �������� �������

	afx_msg void OnTimer(UINT Id);	// ��������� ������� "������ ��������� �� �������"

protected:

	DECLARE_MESSAGE_MAP()
};

#endif
