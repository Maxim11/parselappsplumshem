// WaitingLoadDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Parcel.h"
#include "MainFrm.h"
#include "memdc.h"
#include "WaitingLoadDlg.h"


// CWaitingLoadDlg dialog

IMPLEMENT_DYNAMIC(CWaitingLoadDlg, CDialog)

CWaitingLoadDlg::CWaitingLoadDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CWaitingLoadDlg::IDD, pParent)
{
	m_bDone = false;
}

CWaitingLoadDlg::~CWaitingLoadDlg()
{
}

void CWaitingLoadDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
//	DDX_Control(pDX, IDC_LOGO, m_logo);
}


BEGIN_MESSAGE_MAP(CWaitingLoadDlg, CDialog)
	ON_WM_TIMER()
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()


// CWaitingLoadDlg message handlers

BOOL CWaitingLoadDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  Add extra initialization here
	m_bmp.DeleteObject();
	m_bmp.LoadBitmap(pFrame->GetLanguageInt() == C_LANG_RUSSIAN ? IDB_SPLASH_BITMAP_RUS : IDB_SPLASH_BITMAP_ENG);


	BITMAP bitmap;
	m_bmp.GetBitmap(&bitmap);
	m_size = CSize( bitmap.bmWidth, bitmap.bmHeight );
	m_size = CSize( bitmap.bmWidth, bitmap.bmHeight );

	CRect rcDesktop;
	CWnd::GetDesktopWindow()->GetWindowRect( &rcDesktop );
	int l = (rcDesktop.Width() - m_size.cx)/2;
	int t = (rcDesktop.Height() - m_size.cy)/2;
//	::SetWindowPos( this->m_hWnd, HWND_TOPMOST, l, t, m_size.cx, m_size.cy, /*SWP_NOMOVE*/SWP_SHOWWINDOW );
	::SetWindowPos( this->m_hWnd, HWND_TOP, l, t, m_size.cx, m_size.cy, /*SWP_NOMOVE*/SWP_SHOWWINDOW );

//	m_logo.SetBitmap((HBITMAP)m_bmp.GetSafeHandle());
	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CWaitingLoadDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: Add your message handler code here and/or call default

	CDialog::OnTimer(nIDEvent);
}

void CWaitingLoadDlg::OnCancel()
{
	if(!m_bDone) return;
	CDialog::OnCancel();
}

void CWaitingLoadDlg::OnOK()
{
	if(!m_bDone) return;
	CDialog::OnOK();
}

BOOL CWaitingLoadDlg::OnEraseBkgnd(CDC* pDC)
{
	CRect rect;
	GetClientRect(&rect);

	CodeProject::CMemDC mDC(pDC, rect);

	CDC bitmapDC;
	bitmapDC.CreateCompatibleDC(pDC);

	CBitmap* pOldBitmap = bitmapDC.SelectObject(&m_bmp);

	mDC.BitBlt( 0, 0, m_size.cx, m_size.cy, &bitmapDC, 0, 0, SRCCOPY );

	CPen pen(PS_SOLID, 1, RGB(255, 255, 255));
	CPen* oldpen = mDC.SelectObject(&pen);

	CFont font;
	font.CreatePointFont(90, _T("Arial"), pDC);
	CFont* oldfont = mDC.SelectObject(&font);
	mDC.SetBkMode(TRANSPARENT);

	int x = 30;
	int y = m_size.cy - 26;
	CString str = pFrame->GetLanguageInt() == C_LANG_RUSSIAN ? _T("��� MS Windows�2000/XP/7/8/10") : _T("For MS Windows�2000/XP/7/8/10");
	mDC.SetTextColor(RGB(255, 255, 255));
	mDC.TextOut(x, y, str);

	str = pFrame->GetLanguageInt() == C_LANG_RUSSIAN ? _T("� ��� �������-���������, 2008") : _T("� Lumex Instruments, 2008");
	CSize sz = mDC.GetTextExtent(str);
	x = m_size.cx - sz.cx - 30;
	mDC.TextOut(x, y, str);

	mDC.SelectObject(oldfont);
	mDC.SelectObject(oldpen);

	bitmapDC.SelectObject(pOldBitmap);
	return TRUE;
}
