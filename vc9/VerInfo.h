#pragma once

//#include <WinVer.h>

#ifndef _countof
#define _countof(array) (sizeof(array)/sizeof(array[0]))
#endif

/////////////////////////////////////////////////////////////////////////////
// CModuleVersion version information about a module
//
// To use:
//
// TCHAR szValue[128];
// CModuleVersion VersionInfo;
//
// if (VersionInfo.GetFileVersionInfo(_T("mymodule")))
// {
//        // Information is in version, you can call GetValue
//        VersionInfo.GetValue(_T("CompanyName"), szValue, _countof(szValue));
//        // or
//        VersionInfo.GetCompanyName(szValue, _countof(szValue));
// }
//
// You can also call the static fn DllGetVersion to get DLLVERSIONINFO.
//

class CModuleVersion : public VS_FIXEDFILEINFO
{
protected:

	struct LANGANDCODEPAGE
	{
		WORD wLanguage;        // language
		WORD wCodePage;        // code page
	};

public:
	CModuleVersion()
	{
		m_pVersionInfo = NULL;    // raw version info data 
	}

	~CModuleVersion()
	{
		if (m_pVersionInfo)
			delete [] m_pVersionInfo;
	}

	// Get file version information for a given module
	bool GetFileVersionInfo(IN LPCTSTR lpszModuleName)
	{
		// Get module handle
		HMODULE hModule = ::GetModuleHandle(lpszModuleName);
		if (hModule == NULL && lpszModuleName != NULL)
			return false;

		return GetFileVersionInfo(hModule);
	}

	// Get file version information for a given module
	bool GetFileVersionInfo(IN HMODULE hModule)
	{
		m_Translate.wLanguage = MAKELANGID(LANG_NEUTRAL, SUBLANG_NEUTRAL);
		m_Translate.wCodePage = 1252;    // default = ANSI code page

		memset((VS_FIXEDFILEINFO*)this, 0, sizeof(VS_FIXEDFILEINFO));

		// Get module file name
		TCHAR szFileName[_MAX_PATH];
		DWORD dwLength = ::GetModuleFileName(hModule, szFileName,
			_countof(szFileName));
		if (dwLength <= 0)
			return false;

		// Read file version info
		DWORD dwDummyHandle; // will always be set to zero
		dwLength = ::GetFileVersionInfoSize(szFileName, &dwDummyHandle);
		if (dwLength <= 0)
			return false;

		if (m_pVersionInfo)
		{
			delete [] m_pVersionInfo;
			m_pVersionInfo = NULL;
		}

		m_pVersionInfo = new BYTE[dwLength]; // allocate version info
		if (!::GetFileVersionInfo(szFileName, 0, dwLength, m_pVersionInfo))
			return false;

		LPVOID lpBuffer;
		UINT nLength;
		if (!::VerQueryValue(m_pVersionInfo, _T("\\"), &lpBuffer, &nLength))
			return false;

		// Copy fixed info to myself, which am derived from VS_FIXEDFILEINFO
		*(VS_FIXEDFILEINFO*)this = *(VS_FIXEDFILEINFO*)lpBuffer;

		// Get translation info
		if (VerQueryValue(m_pVersionInfo, _T("\\VarFileInfo\\Translation"),
			&lpBuffer, &nLength) && nLength >= 4)
			m_Translate = *(LANGANDCODEPAGE*)lpBuffer;

		return dwSignature == VS_FFI_SIGNATURE;
	}

	// Returns value by key name
	bool GetValue(IN LPCTSTR lpszKeyName, OUT LPTSTR lpszValue, IN DWORD dwSize)
	{
		if (!m_pVersionInfo)
			return false;
		else
		{
			LPVOID lpBuffer;
			UINT nLength;
			TCHAR szBlock[256];

			nLength = wsprintf(szBlock, _T("\\StringFileInfo\\%04x%04x\\%s"),
				m_Translate.wLanguage, m_Translate.wCodePage, lpszKeyName);

			if (nLength <= 0)
				return false;

			if (!::VerQueryValue(m_pVersionInfo, szBlock, &lpBuffer, &nLength))
				return false;

			return NULL != lstrcpyn(lpszValue, (LPCTSTR)lpBuffer, dwSize);
		}
	}

	// Returns comments
	bool GetComments(OUT LPTSTR lpszComments, IN DWORD dwSize)
	{ return GetValue(_T("Comments"), lpszComments, dwSize); }

	// Returns company name
	bool GetCompanyName(OUT LPTSTR lpszCompanyName, IN DWORD dwSize)
	{ return GetValue(_T("CompanyName"), lpszCompanyName, dwSize); }

	// Returns company name
	bool GetFileCompanyName(OUT LPTSTR lpszCompanyName, IN DWORD dwSize)
	{ return GetValue(_T("CompanyName"), lpszCompanyName, dwSize); }

	// Returns file description
	bool GetFileDescription(OUT LPTSTR lpszDescription, IN DWORD dwSize)
	{ return GetValue(_T("FileDescription"), lpszDescription, dwSize); }

	// Returns file version
	bool GetFileVersion(OUT LPTSTR lpszVersion, IN DWORD dwSize)
	{ return GetValue(_T("FileVersion"), lpszVersion, dwSize); }

	// Returns internal name
	bool GetInternalName(OUT LPTSTR lpszInternalName, IN DWORD dwSize)
	{ return GetValue(_T("InternalName"), lpszInternalName, dwSize); }

	// Returns legal copyright
	bool GetLegalCopyright(OUT LPTSTR lpszLegalCopyright, IN DWORD dwSize)
	{ return GetValue(_T("LegalCopyright"), lpszLegalCopyright, dwSize); }

	// Returns legal trademarks
	bool GetLegalTrademarks(OUT LPTSTR lpszLegalTrademarks, IN DWORD dwSize)
	{ return GetValue(_T("LegalTrademarks"), lpszLegalTrademarks, dwSize); }

	// Returns original file name
	bool GetOriginalFileName(OUT LPTSTR lpszOriginalFileName, IN DWORD dwSize)
	{ return GetValue(_T("OriginalFilename"), lpszOriginalFileName, dwSize); }

	// Returns product name
	bool GetProductName(OUT LPTSTR lpszProductName, IN DWORD dwSize)
	{ return GetValue(_T("ProductName"), lpszProductName, dwSize); }

	// Returns product version
	bool GetProductVersion(OUT LPTSTR lpszProductVersion, IN DWORD dwSize)
	{ return GetValue(_T("ProductVersion"), lpszProductVersion, dwSize); }

	// Returns private build
	bool GetPrivateBuild(OUT LPTSTR lpszPrivateBuild, IN DWORD dwSize)
	{ return GetValue(_T("PrivateBuild"), lpszPrivateBuild, dwSize); }

	// Returns special build
	bool GetSpecialBuild(OUT LPTSTR lpszSpecialBuild, IN DWORD dwSize)
	{ return GetValue(_T("SpecialBuild"), lpszSpecialBuild, dwSize); }

	// Returns file version
	bool GetFileVersion(OUT WORD& wMajor, OUT WORD& wMinor, OUT WORD& wBuild, OUT WORD& wPrivate)
	{
		TCHAR szVersion[64];
		if (GetFileVersion(szVersion, _countof(szVersion)))
			return ParseVesionString(szVersion, wMajor, wMinor, wBuild, wPrivate);
		else
			return false;
	}

	// Returns product version
	bool GetProductVersion(OUT WORD& wMajor, OUT WORD& wMinor, OUT WORD& wBuild, OUT WORD& wPrivate)
	{
		TCHAR szVersion[64];
		if (GetProductVersion(szVersion, _countof(szVersion)))
			return ParseVesionString(szVersion, wMajor, wMinor, wBuild, wPrivate);
		else
			return false;
	}

	bool ParseVesionString(IN LPTSTR lpszVersion, OUT WORD& wMajor,
		OUT WORD& wMinor, OUT WORD& wBuild, OUT WORD& wPrivate)
	{
		wMajor = 0;
		wMinor = 0;
		wBuild = 0;
		wPrivate = 0;

		LPTSTR lpszDelimit =  _T(".,");

		// Get first token (Major version number)
		LPTSTR lpszToken = _tcstok(lpszVersion, lpszDelimit);
		if (lpszToken == NULL)  // end of string
			return false;        // string ended prematurely
		wMajor = (WORD)_ttoi(lpszToken);

		// Get second token (Minor version number)
		lpszToken = _tcstok(NULL, lpszDelimit);
		if (lpszToken == NULL)    // end of string
			return false;        // string ended prematurely
		wMinor = (WORD)_ttoi(lpszToken);

		// Get third token (Build number)
		lpszToken = _tcstok(NULL, lpszDelimit);
		if (lpszToken == NULL)    // end of string
			return false;        // string ended prematurely
		wBuild = (WORD)_ttoi(lpszToken);

		// Get four token (Private number)
		lpszToken = _tcstok(NULL, lpszDelimit);
		if (lpszToken == NULL)    // end of string
			return false;        // string ended prematurely
		wPrivate = (WORD)_ttoi(lpszToken);

		return true;
	}

	// Get DLL Version by calling DLL's DllGetVersion proc
	static bool DllGetVersion(IN LPCTSTR lpszModuleName, OUT DLLVERSIONINFO& dvi)
	{
		bool bResult = false;

		HMODULE hModule = LoadLibrary(lpszModuleName);

		if (hModule != NULL)
		{
			// Must use GetProcAddress because the DLL might not implement 
			// DllGetVersion. Depending upon the DLL, the lack of implementation of the 
			// function may be a version marker in itself.
			//
			DLLGETVERSIONPROC pDllGetVersion =
				(DLLGETVERSIONPROC)GetProcAddress(hModule, (LPCSTR)"DllGetVersion");

			if (pDllGetVersion != NULL)
			{
				memset(&dvi, 0, sizeof(dvi));    // clear
				dvi.cbSize = sizeof(dvi);        // set size for Windows

				bResult = SUCCEEDED((*pDllGetVersion)(&dvi));
			}

			FreeLibrary(hModule);
		}

		return bResult;
	}

protected:
	BYTE*            m_pVersionInfo;    // all version info
	LANGANDCODEPAGE    m_Translate;    // translation info
};

