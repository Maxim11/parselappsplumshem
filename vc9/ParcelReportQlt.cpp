#include "stdafx.h"

#include "Parcel.h"

#include "ParcelReport.h"
#include "ConstTypes.h"
#include "BranchInfo.h"
#include "RootData.h"
#include "FileSystem.h"

#include <math.h>

using namespace filesystem;

CParcelReportQlt::CParcelReportQlt(CModelQltResultDlg *pmoddlg)
{
	pModDlg = pmoddlg;
	pMod = pModDlg->ModData;
	pRes = &pMod->ModelResult;
	pPrj = GetProjectByHItem(pMod->GetParentHItem());

	pMod->FillModInfo();
	pMod->CreateSamplesRest();
}

CWSTR CParcelReportQlt::GetTextFor(Wuid wuid) const
{
	CString sid = wuid.widget_id;
	CString Txt;

	if(!sid.Compare(_T("calib_samples_spectra")) && wuid.counter == 0)
	{
		Txt.Format(ParcelLoadString(STRID_VIEW_MOD_SAMPLES), pMod->GetNSamplesForCalib(), pMod->GetNSpectraForCalib());
		return Txt;
	}
	else if(!sid.Compare(_T("valid_samples_spectra")) && wuid.counter == 0)
	{
		Txt.Format(ParcelLoadString(STRID_VIEW_MOD_SAMPLESVAL), pMod->GetNSamplesForValid(), pMod->GetNSpectraForValid());
		return Txt;
	}
	else if(!sid.Compare(_T("excluded_points")) && wuid.counter == 0)
	{
		Txt.Format(ParcelLoadString(STRID_VIEW_MOD_EXCLUDED), pMod->GetNExcPoints());
		return Txt;
	}

	return cEmpty;
}

TextListItem const* CParcelReportQlt::GetTextArrayFor(Wuid wuid) const
{
	CString sid = wuid.widget_id;
	CString Txt, Tmp;
	CDeviceData *pDev = GetDeviceByHItem(pPrj->GetParentHItem());
	TextListItem const* NewLT;
	TextListItem* NewLT2;

	if(!sid.Compare(_T("instrument")) && wuid.counter == 0)
	{
		Txt = pDev->GetName();
		NewLT2 = new TextListItem(Txt, NULL);
		NewLT = new TextListItem(cEmpty, NewLT2);
		return NewLT;
	}
	else if(!sid.Compare(_T("report_created")) && wuid.counter == 0)
	{
		CTime Date = CTime::GetCurrentTime();
		Txt = Date.Format(cFmtDate);
		NewLT2 = new TextListItem(Txt, NULL);
		NewLT = new TextListItem(cEmpty, NewLT2);
		return NewLT;
	}
	else if(!sid.Compare(_T("software_version")) && wuid.counter == 0)
	{
		int n1, n2, n3;
		theApp.GetAppVer(&n1, &n2, &n3);
		Tmp.Format(ParcelLoadString(STRID_ABOUT_VERSION), n1, n2, n3);
		Txt = ParcelLoadString(STRID_ABOUT_PARCEL) + _T(". ") + Tmp;
		NewLT2 = new TextListItem(Txt, NULL);
		NewLT = new TextListItem(cEmpty, NewLT2);
		return NewLT;
	}
	else if(!sid.Compare(_T("Model")) && wuid.counter == 0)
	{
		Txt = pMod->GetName();
		NewLT2 = new TextListItem(Txt, NULL);
		NewLT = new TextListItem(cEmpty, NewLT2);
		return NewLT;
	}

	NewLT = new TextListItem(cEmpty, NULL);
	return NewLT;
}

CWSTR CParcelReportQlt::GetPicturePathFor(Wuid wuid) const
{
	CString sid = wuid.widget_id;
	CString FileName, Path = pRoot->GetReportPictureFolder(C_TEMPLATE_QLT);

	if(!sid.Compare(_T("MD_calib_histogram")) && wuid.counter == 0)
	{
		FileName = MakePath(Path, _T("report_picture_MD_calib_histogram.png"));
		pModDlg->MakeGraphPicture(FileName, C_PGRAPH_MDCALIB);
	}
	else if(!sid.Compare(_T("MD_valid_histogram")) && wuid.counter == 0)
	{
		FileName = MakePath(Path, _T("report_picture_MD_valid_histogram.png"));
		pModDlg->MakeGraphPicture(FileName, C_PGRAPH_MDVALID);
	}
	else if(!sid.Compare(_T("scores_graph")) && wuid.counter == 0)
	{
		FileName = MakePath(Path, _T("report_picture_scores_graph.png"));
		pModDlg->MakeGraphPicture(FileName, C_PGRAPH_SCORES);
	}
	else if(!sid.Compare(_T("loadings_graph")) && wuid.counter == 0)
	{
		FileName = MakePath(Path, _T("report_picture_loadings_graph.png"));
		pModDlg->MakeGraphPicture(FileName, C_PGRAPH_SPLOAD);
	}

	return FileName;
}

TableHint const* CParcelReportQlt::GetTableHintFor(Wuid wuid) const
{
	CString sid = wuid.widget_id;
	CString Txt, Tmp;
	int NSC = pMod->GetNSamplesForCalib();
	int NSpC = pMod->GetNSpectraForCalib();
	int NSV = pMod->GetNSamplesForValid();
	int NSpV = pMod->GetNSpectraForValid();
	int NExc = pMod->GetNExcPoints();
	int NLR = pRes->GetNLoadingResult();
	int NGK = pMod->GetNCompQnt();
	int NSR = pMod->GetNSamplesRest();

	TableHint const* NewTH;

	if(!sid.Compare(_T("model_parameters")) && wuid.counter == 0)
		NewTH = new TableHint(2, pMod->GetNModInfo(), cEmpty, cEmpty);
	else if(!sid.Compare(_T("calib_samples")) && wuid.counter == 0)
		NewTH = new TableHint(2, NSC, cEmpty, cEmpty);
	else if(!sid.Compare(_T("valid_samples")) && wuid.counter == 0)
		NewTH = new TableHint(2, NSV, cEmpty, cEmpty);
	else if(!sid.Compare(_T("excluded")) && wuid.counter == 0)
		NewTH = new TableHint(2, NExc, cEmpty, cEmpty);
	else if(!sid.Compare(_T("MD_calib_table")) && wuid.counter == 0)
		NewTH = new TableHint(4, NSpC, cEmpty, cEmpty);
	else if(!sid.Compare(_T("MD_valid_table")) && wuid.counter == 0)
		NewTH = new TableHint(4, NSpV, cEmpty, cEmpty);
	else if(!sid.Compare(_T("scores_table")) && wuid.counter == 0)
		NewTH = new TableHint(NGK + 2, NSpC, cEmpty, cEmpty);
	else if(!sid.Compare(_T("loadings_table")) && wuid.counter == 0)
		NewTH = new TableHint(NGK + 2, NLR, cEmpty, cEmpty);
	else if(!sid.Compare(_T("leftover")) && wuid.counter == 0)
		NewTH = new TableHint(2, NSR, cEmpty, cEmpty);
	else
		NewTH = new TableHint(1, 1, cEmpty, cEmpty);

	return NewTH;
}

ColumnHint const* CParcelReportQlt::GetTableHeaderHintFor(Wuid wuid, Index col) const
{
	CString sid = wuid.widget_id;
	ColumnHint const* NewCH;
	CString s, fmtGK = ParcelLoadString(STRID_MODEL_RESULT_SCORES_COL_GK);
	int NGK = pMod->GetNCompQnt();

	if(!sid.Compare(_T("model_parameters")) && wuid.counter == 0)
	{
		switch(col)
		{
		case 0:  NewCH = new ColumnHint(hLeft, ParcelLoadString(STRID_VIEW_MOD_COLSET_PARAMETR));  return NewCH;
		case 1:  NewCH = new ColumnHint(hRight, ParcelLoadString(STRID_VIEW_MOD_COLSET_VALUE));  return NewCH;
		}
	}
	else if(!sid.Compare(_T("calib_samples")) && wuid.counter == 0 && int(col) < pMod->GetNModelParams() + 2)
	{
		switch(col)
		{
		case 0:  NewCH = new ColumnHint(hLeft, ParcelLoadString(STRID_CREATE_MODEL_SMP_COLSAM));  return NewCH;
		case 1:  NewCH = new ColumnHint(hRight, ParcelLoadString(STRID_CREATE_MODEL_SMP_COLNSPEC));  return NewCH;
		}
	}
	else if(!sid.Compare(_T("valid_samples")) && wuid.counter == 0 && int(col) < pMod->GetNModelParams() + 2)
	{
		switch(col)
		{
		case 0:  NewCH = new ColumnHint(hLeft, ParcelLoadString(STRID_CREATE_MODEL_SMP_COLSAM));  return NewCH;
		case 1:  NewCH = new ColumnHint(hRight, ParcelLoadString(STRID_CREATE_MODEL_SMP_COLNSPEC));  return NewCH;
		}
	}
	else if(!sid.Compare(_T("excluded")) && wuid.counter == 0)
	{
		switch(col)
		{
		case 0:  NewCH = new ColumnHint(hLeft, cEmpty);  return NewCH;
		case 1:  NewCH = new ColumnHint(hLeft, cEmpty);  return NewCH;
		}
	}
	else if(!sid.Compare(_T("MD_calib_table")) && wuid.counter == 0)
	{
		switch(col)
		{
		case 0:  NewCH = new ColumnHint(hLeft, ParcelLoadString(STRID_MODEL_RESULT_COL_NUM));  return NewCH;
		case 1:  NewCH = new ColumnHint(hLeft, ParcelLoadString(STRID_MODEL_RESULT_OUT_COL_SPEC));  return NewCH;
		case 2:  NewCH = new ColumnHint(hRight, ParcelLoadString(STRID_MODEL_RESULT_OUT_COL_MAH));  return NewCH;
		case 3:  NewCH = new ColumnHint(hRight, ParcelLoadString(STRID_MODEL_RESULT_OUT_COL_EXCEED));  return NewCH;
		}
	}
	else if(!sid.Compare(_T("MD_valid_table")) && wuid.counter == 0)
	{
		switch(col)
		{
		case 0:  NewCH = new ColumnHint(hLeft, ParcelLoadString(STRID_MODEL_RESULT_COL_NUM));  return NewCH;
		case 1:  NewCH = new ColumnHint(hLeft, ParcelLoadString(STRID_MODEL_RESULT_OUT_COL_SPEC));  return NewCH;
		case 2:  NewCH = new ColumnHint(hRight, ParcelLoadString(STRID_MODEL_RESULT_OUT_COL_MAH));  return NewCH;
		case 3:  NewCH = new ColumnHint(hRight, ParcelLoadString(STRID_MODEL_RESULT_OUT_COL_EXCEED));  return NewCH;
		}
	}
	else if(!sid.Compare(_T("scores_table")) && wuid.counter == 0 && int(col) < NGK + 2)
	{
		switch(col)
		{
		case 0:  NewCH = new ColumnHint(hLeft, ParcelLoadString(STRID_MODEL_RESULT_COL_NUM));  return NewCH;
		case 1:  NewCH = new ColumnHint(hLeft, ParcelLoadString(STRID_MODEL_RESULT_SCORES_COL_SPEC));  return NewCH;
		default:  s.Format(fmtGK, col - 2 + 1);  NewCH = new ColumnHint(hRight, s);  return NewCH;
		}
	}
	else if(!sid.Compare(_T("loadings_table")) && wuid.counter == 0 && int(col) < NGK + 2)
	{
		switch(col)
		{
		case 0:  NewCH = new ColumnHint(hLeft, ParcelLoadString(STRID_MODEL_RESULT_COL_NUM));  return NewCH;
		case 1:  NewCH = new ColumnHint(hLeft, ParcelLoadString(STRID_MODEL_RESULT_SPECLOAD_COL_WAVENUM));  return NewCH;
		default:  s.Format(fmtGK, col - 2 + 1);  NewCH = new ColumnHint(hRight, s);  return NewCH;
		}
	}
	else if(!sid.Compare(_T("leftover")) && wuid.counter == 0 && int(col) < pMod->GetNModelParams() + 2)
	{
		switch(col)
		{
		case 0:  NewCH = new ColumnHint(hLeft, ParcelLoadString(STRID_CREATE_MODEL_SMP_COLSAM));  return NewCH;
		case 1:  NewCH = new ColumnHint(hRight, ParcelLoadString(STRID_CREATE_MODEL_SMP_COLNSPEC));  return NewCH;
		}
	}
	else
		NewCH = new ColumnHint(hLeft, cEmpty);

	return NewCH;
}

CWSTR CParcelReportQlt::GetTableCellFor(Wuid wuid, Index row, Index col) const
{
	CString sid = wuid.widget_id;
	int NGK = pMod->GetNCompQnt();
	CString s, fmt;

	if(!sid.Compare(_T("model_parameters")) && wuid.counter == 0 && int(col) < 2 && int(row) < pMod->GetNModInfo())
	{
		switch(col)
		{
		case 0:  return pMod->GetModInfo(row)->SetName;
		case 1:  return pMod->GetModInfo(row)->Value;
		}
	}
	else if(!sid.Compare(_T("calib_samples")) && wuid.counter == 0 && int(col) < pMod->GetNModelParams() + 2 && int(row) < pMod->GetNSamplesForCalib())
	{
		ModelSample *Dat = pMod->GetSampleForCalib(row);
		CTransferData* pTr = pPrj->GetTransfer(Dat->TransName);
		CSampleData *pSam = pTr->GetSample(Dat->SampleName);
		switch(col)
		{
		case 0:  return pMod->GetSampleTransName(Dat);
		case 1:  s.Format(_T("%s (%1d)"), Dat->GetUsesSpectrumStr(), Dat->GetNSpectra());  return s;
		default:  s = pSam->GetReferenceDataStr(pMod->GetModelParam(col - 2)->ParamName);  return s;
		}
	}
	else if(!sid.Compare(_T("valid_samples")) && wuid.counter == 0 && int(col) < pMod->GetNModelParams() + 2 && int(row) < pMod->GetNSamplesForValid())
	{
		ModelSample *Dat = pMod->GetSampleForValid(row);
		CTransferData* pTr = pPrj->GetTransfer(Dat->TransName);
		CSampleData *Sam = pTr->GetSample(Dat->SampleName);
		switch(col)
		{
		case 0:  return pMod->GetSampleTransName(Dat);
		case 1:  s.Format(_T("%s (%1d)"), Dat->GetUsesSpectrumStr(), Dat->GetNSpectra());  return s;
		default:  s = Sam->GetReferenceDataStr(pMod->GetModelParam(col - 2)->ParamName);  return s;
		}
	}
	else if(!sid.Compare(_T("excluded")) && wuid.counter == 0 && int(col) < 2 && int(row) < pMod->GetNExcPoints())
	{
		switch(col)
		{
		case 0:  s.Format(cFmt1, pMod->GetExcPoint(row) + 1);  return s;
		case 1:  s.Format(cFmt86, pMod->GetExcPointMean(row));  return s;
		}
	}
	else if(!sid.Compare(_T("MD_calib_table")) && wuid.counter == 0 && int(col) < 4 && int(row) < pRes->GetNCalibResult())
	{
		SpectrumResult* pSpR = pRes->GetCalibResult(row);
		switch(col)
		{
		case 0:  s.Format(cFmt1, row + 1);  return s;
		case 1:  return pSpR->Name;
		case 2:  s.Format(cFmt42, pSpR->Out_Mah);  return s;
		case 3:  s.Format(cFmt42, pMod->GetMaxMah() - pSpR->Out_Mah);  return s;
		}
	}
	else if(!sid.Compare(_T("MD_valid_table")) && wuid.counter == 0 && int(col) < 4 && int(row) < pRes->GetNValidResult())
	{
		SpectrumValResult* pSpVR = pRes->GetValidResult(row);
		switch(col)
		{
		case 0:  s.Format(cFmt1, row + 1);  return s;
		case 1:  return pSpVR->Name;
		case 2:  s.Format(cFmt42, pSpVR->Out_Mah);  return s;
		case 3:  s.Format(cFmt42, pMod->GetMaxMah() - pSpVR->Out_Mah);  return s;
		}
	}
	else if(!sid.Compare(_T("scores_table")) && wuid.counter == 0 && int(col) < NGK + 2 && int(row) < pRes->GetNCalibResult())
	{
		SpectrumResult* pSpR = pRes->GetCalibResult(row);
		switch(col)
		{
		case 0:  s.Format(cFmt1, row + 1);  return s;
		case 1:  return pSpR->Name;
		default:  s.Format(cFmt86, pSpR->Scores_GK[col - 2]);  return s;
		}
	}
	else if(!sid.Compare(_T("loadings_table")) && wuid.counter == 0 && int(col) < NGK + 2 && int(row) < pRes->GetNLoadingResult())
	{
		LoadingResult *pLR = pRes->GetLoadingResultByInd(row);
		switch(col)
		{
		case 0:  s.Format(cFmt1, pLR->SpecPoint + 1);  return s;
		case 1:  s.Format(cFmt86, pPrj->GetSpecPoint(pLR->SpecPoint));  return s;
		default:  s.Format(cFmt86, pLR->Spec_GK[col - 2]);  return s;
		}
	}
	else if(!sid.Compare(_T("leftover")) && wuid.counter == 0 && int(col) < pMod->GetNModelParams() + 2 && int(row) < pMod->GetNSamplesRest())
	{
		ModelSample *Dat = pMod->GetSampleRest(row);
		CTransferData* pTr = pPrj->GetTransfer(Dat->TransName);
		CSampleData *pSam = pTr->GetSample(Dat->SampleName);
		switch(col)
		{
		case 0:  return pMod->GetSampleTransName(Dat);
		case 1:  s.Format(_T("%s (%1d)"), Dat->GetUnUsesSpectrumStr(), Dat->GetNSpectra());  return s;
		default:  s = pSam->GetReferenceDataStr(pMod->GetModelParam(col - 2)->ParamName);  return s;
		}
	}

	return cEmpty;
}

void CParcelReportQlt::SetReportLanguage(CWSTR language)
{
	return;
}
