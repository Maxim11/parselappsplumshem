#include "stdafx.h"

#include "BranchInfo.h"
#include "CreateParamDlg.h"
#include "MainFrm.h"


IMPLEMENT_DYNAMIC(CCreateParamDlg, CDialog)

CCreateParamDlg::CCreateParamDlg(CParamData* data, int mode, CWnd* pParent)
	: CDialog(CCreateParamDlg::IDD, pParent)
{
	Mode = mode;

	ParData = data;
	ParentPrj = GetProjectByHItem(ParData->GetParentHItem());

	m_vName = ParData->GetName();
	m_vUse = ParData->IsUseStdMoisture();
	m_vStdMois = ParData->GetStdMoisture();
	m_vUnit = ParData->GetUnit();
	m_vFormat = ParData->GetIFormat();

	IsBtnClk = false;
}

CCreateParamDlg::~CCreateParamDlg()
{
}

void CCreateParamDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_INPUT_NAME, m_Name);
	DDX_Control(pDX, IDC_USE, m_Use);
	DDX_Control(pDX, IDC_STANDART_MOISTURE, m_StdMois);
	DDX_Control(pDX, IDC_INPUT_UNIT, m_Unit);
	DDX_Control(pDX, IDC_INPUT_FORMAT, m_Format);

	DDX_Control(pDX, IDC_SPIN_LETTER, m_SpinLetter);

	cParcelDDX(pDX, IDC_INPUT_NAME, m_vName);
	DDV_MaxChars(pDX, m_vName, 50);
	cParcelDDX(pDX, IDC_INPUT_UNIT, m_vUnit);
	DDV_MaxChars(pDX, m_vUnit, 20);
	cParcelDDX(pDX, IDC_INPUT_FORMAT, m_vFormat);
	cParcelDDV(pDX, ParcelLoadString(STRID_CREATE_PARAM_DDV_FORMAT), m_vFormat, 0, 7);

	DDX_Check(pDX, IDC_USE, m_vUse);

	if(m_vUse)
	{
		cParcelDDX(pDX, IDC_STANDART_MOISTURE, m_vStdMois);
		cParcelDDV(pDX, ParcelLoadString(STRID_CREATE_PARAM_DDV_STDMOIS), m_vStdMois, 0., 100.);
	}
}


BEGIN_MESSAGE_MAP(CCreateParamDlg, CDialog)

	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_LETTER, OnSpinLetterDeltaPos)

	ON_BN_CLICKED(IDC_ACCEPT, OnAccept)
	ON_BN_CLICKED(IDHELP, OnHelp)
	ON_BN_CLICKED(IDC_USE, OnSetUse)

END_MESSAGE_MAP()


BOOL CCreateParamDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	CString Hdr;
	switch(Mode)
	{
	case PARDLG_MODE_CREATE:
		Hdr = ParcelLoadString(STRID_CREATE_PARAM_HDR_CREATE);
		break;
	case PARDLG_MODE_CHANGE:
		Hdr = ParcelLoadString(STRID_CREATE_PARAM_HDR_CHANGE);
		break;
	}
	SetWindowText(Hdr);

	CWnd* pWindow = GetDlgItem(IDC_INPUT_FORMAT);
	if(pWindow != 0)
	{
		m_SpinLetter.SetBuddy(pWindow);
	    m_SpinLetter.SetRange(0, 7);
		m_SpinLetter.SetPos(m_vFormat);
	}

	GetDlgItem(IDC_STR_UNIT)->SetWindowText(m_vUnit);

	GetDlgItem(IDC_ACCEPT)->SetWindowText(ParcelLoadString(STRID_CREATE_PARAM_ACCEPT));
	GetDlgItem(IDCANCEL)->SetWindowText(ParcelLoadString(STRID_CREATE_PARAM_DECLINE));
	GetDlgItem(IDHELP)->SetWindowText(ParcelLoadString(STRID_CREATE_PARAM_HELP));

	GetDlgItem(IDC_STATIC_1)->SetWindowText(ParcelLoadString(STRID_CREATE_PARAM_NAME));
	GetDlgItem(IDC_STATIC_2)->SetWindowText(ParcelLoadString(STRID_CREATE_PARAM_UNIT));
	GetDlgItem(IDC_STATIC_3)->SetWindowText(ParcelLoadString(STRID_CREATE_PARAM_FORMAT));
	GetDlgItem(IDC_STATIC_5)->SetWindowText(ParcelLoadString(STRID_CREATE_PARAM_STDMOIS));

	GetDlgItem(IDC_USE)->SetWindowText(ParcelLoadString(STRID_CREATE_PARAM_USE));

	if(!m_vUse)
	{
		GetDlgItem(IDC_STR_UNIT)->EnableWindow(false);
		GetDlgItem(IDC_STANDART_MOISTURE)->EnableWindow(false);
		GetDlgItem(IDC_STANDART_MOISTURE)->SetWindowText(ParcelLoadString(STRID_CREATE_PARAM_NODATA));
	}

	bool enname = (ParentPrj->GetParamKeepStatus(ParData->GetName()) == C_KEEP_FREE);
	GetDlgItem(IDC_STATIC_1)->EnableWindow(enname);
	GetDlgItem(IDC_INPUT_NAME)->EnableWindow(enname);

	GetDlgItem(IDHELP)->EnableWindow(pFrame->IsExistHelp());

	return true;
}

void CCreateParamDlg::OnAccept()
{
	if(!UpdateData())
		return;

	CString s;

	m_vName.Trim();
	if(m_vName.IsEmpty())
	{
		ParcelError(ParcelLoadString(STRID_CREATE_PARAM_ERROR_EMPTYNAME));
		return;
	}
	if(Mode == PARDLG_MODE_CREATE || ParData->GetName().Compare(m_vName))
	{
		CParamData *Par = ParentPrj->GetParam(m_vName);
		if(Par != NULL)
		{
			s.Format(ParcelLoadString(STRID_CREATE_PARAM_ERROR_NAME), m_vName);
			ParcelError(s);
			return;
		}
	}

	m_vUnit.Trim();
	if(m_vUnit.IsEmpty())
	{
		ParcelError(ParcelLoadString(STRID_CREATE_PARAM_ERROR_EMPTYUNIT));
		return;
	}

	ParData->SetName(m_vName);
	ParData->SetFormat(m_vFormat);
	ParData->SetUnit(m_vUnit);
	if(m_vUse)
		ParData->SetUseStdMoisture(true);
	else
		ParData->SetUseStdMoisture(false);
	ParData->SetStdMoisture(m_vStdMois);

	CDialog::OnOK();
}

void CCreateParamDlg::OnCancel()
{
	if(Mode == PARDLG_MODE_CHANGE && IsChanged())
	{
		if(!ParcelConfirm(ParcelLoadString(STRID_CREATE_PARAM_ASK_DOCANCEL), false))
			return;
	}

	CDialog::OnCancel();
}

void CCreateParamDlg::OnSetUse()
{
	int Use = m_Use.GetCheck();

	GetDlgItem(IDC_STR_UNIT)->EnableWindow(m_Use.GetCheck());
	GetDlgItem(IDC_STANDART_MOISTURE)->EnableWindow(m_Use.GetCheck());

	if(Use)
	{
		CString s;
		s.Format(cFmt31, m_vStdMois);
		GetDlgItem(IDC_STANDART_MOISTURE)->SetWindowText(s);
	}
	else
		GetDlgItem(IDC_STANDART_MOISTURE)->SetWindowText(ParcelLoadString(STRID_CREATE_PARAM_NODATA));
}

void CCreateParamDlg::OnSpinLetterDeltaPos(NMHDR *pNMHDR, LRESULT *pResult)
{
   if(pNMHDR == 0 || pResult == 0) return;

   NMUPDOWN *pNMUpDown = (NMUPDOWN *) pNMHDR;

   pNMUpDown->iDelta *= 1;
}

bool CCreateParamDlg::IsChanged()
{
	return true;
}

void CCreateParamDlg::OnHelp()
{
	int IdHelp = DLGID_MAIN;
	switch(Mode)
	{
	case PARDLG_MODE_CREATE: IdHelp = DLGID_PARAM_CREATE;  break;
	case PARDLG_MODE_CHANGE: IdHelp = DLGID_PARAM_CHANGE;  break;
	}

	pFrame->ShowHelp(IdHelp);
}
