#include "stdafx.h"


#include "file4spa.h"
#include "FileSystem.h"

namespace fs = filesystem;

namespace file4spa
{


//-------------------------------------------------------------------------
// ���������� ������������ ������ � ����
bool SaveSpaDataAtFile( LPCTSTR path, const SpaData& spadata )
{
	if( !path || spadata.empty() ) return false; 

	CFile file;

	if( !file.Open( path, CFile::modeCreate | CFile::modeWrite ) )
		return false;

	UINT		size = UINT( spadata.size() * sizeof(double) );
	const void*	data = reinterpret_cast<const void*>( &spadata[0] );
	file.Write( data, size );
	file.Close();

	return true;
}
//-------------------------------------------------------------------------
// �������������� ������������ ������ �� �����
bool ReadSpaDataFromFile( LPCTSTR path, SpaData& spadata )
{
	if( !path || !fs::IsExistFile(path) ) return false; 

	LONG fsize = fs::GetFileSize(path);
	if( fsize <= 0L || fsize % sizeof(double) != 0 ) return false; // ��������� ����� ����� � ��������� double

	CFile file;
	if( !file.Open( path, CFile::modeRead | CFile::shareDenyNone ) )
		return false;

	spadata.resize( size_t( fsize / sizeof(double) ) );

	UINT	size = UINT(fsize);
	void*	data = reinterpret_cast<void*>( &spadata[0] );

	file.Seek( 0, CFile::begin );
	bool ok = (size == file.Read( data, size ) );

	file.Close();

	return ok;
}
//-------------------------------------------------------------------------

} // namespace file4spa


