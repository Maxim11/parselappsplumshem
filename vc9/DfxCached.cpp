#include "stdafx.h"

#include "DfxCached.h"


CDfxCache::CDfxCache(CString fname)
{
	if(!fname.IsEmpty())
		Load(fname);
}

void CDfxCache::Clear()
{
	m_strings.RemoveAll();
	m_idx = 0;
	m_cnt = 0;
}

int CDfxCache::CheckFile(CString fname)
{
	if(_tcscmp(fname, m_fname))
	{
		CString str;
		str.Format(_T("Wrong DfxCache request: %s, %s"), fname, m_fname);
		ParcelError(str);
		return 0;
	}
	else
		return 1;
}

int CDfxCache::Load(CString fname)
{
	Clear();

	m_fname = fname;

	FILE *f = _tfopen(fname, _T("rt"));
	if(!f) return 0;
/*
	USES_CONVERSION;

	static char str[DFX_MAX_STRLEN];
	while(fgets(str, DFX_MAX_STRLEN - 4, f))
	{
		if(strlen(str))
		{
			CString ts = A2T(str);
			m_strings.Add(ts);
		}
	}
*/
//->

	static char str[DFX_MAX_STRLEN];
	TCHAR strw[DFX_MAX_STRLEN];
	while(fgets(str, DFX_MAX_STRLEN - 4, f))
	{
		if(strlen(str))
		{
			MultiByteToWideChar(CP_ACP, 0, str, -1, strw, DFX_MAX_STRLEN-1);
			CString ts = strw;
			m_strings.Add(ts);
		}
	}
//<-

	fclose(f);

	m_cnt = int(m_strings.GetSize());
	m_fname = fname;

	return m_cnt;
}

CString CDfxCache::GetString(int idx)
{
	if(idx < m_cnt) 
		return m_strings[idx];
	else
		return cEmpty;
}

CString CDfxCache::GetNextString()
{
	if(m_idx < m_cnt) 
		return m_strings[m_idx++];
	else
		return 0;
}

int CDfxCache::GetNextString(TCHAR* str)
{
	if((m_idx < m_cnt) && (str != 0)) 
	{
		_tcscpy(str, m_strings[m_idx++]);
		return 1;
	}
	else
		return 0;
}

TCHAR* CDfxCache::GetProfileItem(CString fname, CString section, CString key)
{
	if(!fname)  return 0;
	if(!_tcslen(fname))  return 0;
	if(!CheckFile(fname))  return 0;

	int secfound=0;
	int keyfound=0;
	static TCHAR str[DFX_MAX_STRLEN];

	// searching for section
	ResetIdx();
	while(GetNextString(str))
	{
		secfound = DfxCheckProfileSection(str, section);
		if(secfound) break;
	}

	if(secfound)
	{
	// searching for key
		while(GetNextString(str))
		{
			keyfound = DfxCheckProfileKey(str, key);
			if(keyfound) break;
		}
	}

	if(keyfound <= 0) return 0;

	TCHAR *p1 = _tcstok(_tcschr(str, '=') + 1, _T("\n"));
	while(_istspace(*p1))  p1++;

	TCHAR *p = p1 + _tcslen(p1) - 1;
	while(_istspace(*p))
	{
		*p-- = 0;
	}
	return p1;
}

CString CDfxCache::GetProfileString(CString fname, CString section, CString key, TCHAR* sval)
{
	TCHAR *p = GetProfileItem(fname, section, key);
	if(!p) 
	{
		return sval;
	}
	else
	{
		TCHAR *p1 = p;
		while(*p1 == '<') p1++;

		TCHAR *p2 = p1 + _tcslen(p1) - 1;
		while(*p2 == '>')
		{
			*p2-- = 0;
		}
		return p1;
	}
}

unsigned CDfxCache::GetProfileHex(CString fname, CString section, CString key, unsigned uval)
{
	TCHAR *p = GetProfileItem(fname, section, key);
	if(!p) 
	{
		return uval;
	}
	else
	{
		unsigned u;
		_stscanf(p, _T("%X"), &u);
		return u;
	}
}

int CDfxCache::GetProfileInt(CString fname, CString section, CString key, int ival)
{
	TCHAR *p = GetProfileItem(fname, section, key);
	if(!p) 
		return ival;
	else
		return _tstoi(p);
}

double CDfxCache::GetProfileDouble(CString fname, CString section, CString key, double dval)
{
	TCHAR *p = GetProfileItem(fname, section, key);
	if(!p) 
		return dval;
	else
		return _tstof(p);
}

int CDfxCache::GetProfileIntArray(CString fname, CString section, CString key, int imaxsize, int *pdata)
{
	TCHAR *p = GetProfileItem(fname, section, key);
	if(!p) 
	{
		return 0;
	}
	int isize = _tstoi(_tcstok(p, _T(" \t\n\r")));
	if(isize > imaxsize) isize = imaxsize;
	for(int i=0; i<isize; i++)
	{
		pdata[i] = _tstoi(_tcstok(0, _T(" \t\n\r")));
	}
	return isize;
}

int CDfxCache::DfxCheckProfileSection(CString chkstr, CString section, int soft/* = 0*/)
{
	TCHAR str[DFX_MAX_STRLEN];
	_tcscpy(str, chkstr);
	TCHAR *p=str;
	while(_istspace(*p)) p++;
	if(*p == '[') 
	{
		p = _tcstok(++p, _T("]"));
		int ok = soft ? !_tcsncmp(p, section, _tcslen(section)) : !_tcscmp(p, section);
		if(ok)
			return 1;
		else
			return 0;
	}
	return 0;
}

int CDfxCache::DfxCheckProfileKey(CString chkstr, CString key)
{
	TCHAR str[DFX_MAX_STRLEN];
	_tcscpy(str, chkstr);

	TCHAR *p=str;
	while(_istspace(*p)) p++;
	if(*p) 
	{
		if(*p == '[') return -1;
		p = _tcstok(p, _T("="));
		if(!_tcscmp(p, key))
		{
			return 1;
		}
	}
	return 0;
}

int CDfxCache::GetNStrings()
{
	return int(m_strings.GetSize());
}

void CDfxCache::DfxPutProfileItem(CString fname, CString section, CString key, CString item)
{
	if(!fname.IsEmpty())
		Load(fname);

	struct _stat stat_buf;
	int nBufSize = 2*DFX_MAX_STRLEN;
	if(_tstat(fname, &stat_buf) == 0)
	{
		nBufSize += stat_buf.st_size;
	}
	TCHAR* strbuff = new TCHAR[nBufSize];
	TCHAR* pbuf = strbuff;

	TCHAR str[DFX_MAX_STRLEN];
	FILE *f = _tfopen(fname, _T("rt"));
	if(f)
	{
		int secfound=0;
		int keyfound=0;

		ResetIdx();
		// searching for section
//		while(_fgetts(str, DFX_MAX_STRLEN-4, f))
		while(GetNextString(str))
		{
			secfound = DfxCheckProfileSection(str, section);
			pbuf += _stprintf(pbuf, _T("%s"), str);

			if(secfound) break;
		}

		if(!secfound)
		{
			pbuf += _stprintf(pbuf, _T("[%s]\n"), section);
			pbuf += _stprintf(pbuf, _T("%s=%s\n\n"), key, item);
		}
		else
		{
		// searching for key
//			while(_fgetts(str, DFX_MAX_STRLEN-4, f))
			while(GetNextString(str))
			{
				keyfound = DfxCheckProfileKey(str, key);
				if(keyfound == 1) 
				{
					pbuf += _stprintf(pbuf, _T("%s=%s\n"), key, item);
					break;
				}
				if(keyfound == -1) 
				{
					pbuf += _stprintf(pbuf, _T("%s=%s\n\n"), key, item);
					pbuf += _stprintf(pbuf, _T("%s"), str);
					break;
				}
				if(keyfound == 0) 
				{
					if(_tcschr(str, '='))
					{
						pbuf += _stprintf(pbuf, _T("%s"), str);
					}
				}
			}
			if(!keyfound)
			{
				pbuf += _stprintf(pbuf, _T("%s=%s\n\n"), key, item);
			}
		}

//		while(_fgetts(str, DFX_MAX_STRLEN-4, f))
		while(GetNextString(str))
		{
			pbuf += _stprintf(pbuf, _T("%s"), str);
		}
		fclose(f);
	}
	else
	{
		pbuf += _stprintf(pbuf, _T("[%s]\n"), section);
		pbuf += _stprintf(pbuf, _T("%s=%s\n\n"), key, item);
	}

	USES_CONVERSION;
/**/
	FILE *fout = fopen(T2A(fname),  "wt");
	fprintf(fout, "%s", T2A(strbuff));
/*/
	FILE *fout = _tfopen(fname,  _T("wt"));
	_ftprintf(fout, _T("%s"), strbuff);
/**/
	fclose(fout);

	delete [] strbuff;
}

void CDfxCache::DfxPutProfileString(CString fname, CString section, CString key, CString sval)
{
	TCHAR p[DFX_MAX_STRLEN];
	_tcscpy(p, _T("<"));
	if(sval)
	{
		_tcsncat(p, sval, 1000);
	}
	_tcscat(p, _T(">"));
	DfxPutProfileItem(fname, section, key, p);
}

void CDfxCache::DfxPutProfileHex(CString fname, CString section, CString key, unsigned uval)
{
	TCHAR str[256];
	_stprintf(str, _T("%08X"), uval);
	DfxPutProfileItem(fname, section, key, str);
}

void CDfxCache::DfxPutProfileInt(CString fname, CString section, CString key, int ival)
{
	TCHAR str[256];
	_stprintf(str, _T("%d"), ival);
	DfxPutProfileItem(fname, section, key, str);
}

void CDfxCache::DfxPutProfileDouble(CString fname, CString section, CString key, double dval)
{
	TCHAR str[256];
	_stprintf(str, _T("%25.18e"), dval);
	DfxPutProfileItem(fname, section, key, str);
}

void CDfxCache::DfxPutProfileIntArray(CString fname, CString section, CString key, int isize, int *pdata)
{
	TCHAR str[DFX_MAX_STRLEN];
	TCHAR *p = str;

	p += _stprintf(p, _T("%d"), isize);

	for(int i=0; i<isize; i++)
	{
		p += _stprintf(p, _T(" %d"), pdata[i]);
	}

	DfxPutProfileItem(fname, section, key, str);
}
