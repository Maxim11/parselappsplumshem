#ifndef _IMIT_DEVICE_DLG_H_
#define _IMIT_DEVICE_DLG_H_

#pragma once

#include "Resource.h"
#include "RootData.h"

// ��������� ������ ������ �������

const int IDEVDLG_MODE_CREATE	 = 0;	// ����� �������� ������ �������
const int IDEVDLG_MODE_CHANGE	 = 1;	// ����� ��������� �������� �������
const int IDEVDLG_MODE_CHANGENUM = 2;	// ����� ��������� �������� ������� (������ �����)

///////////////////////////////////////////////////////////////////////////////////////////////////
// ������ �������� �������� �������

class CImitDeviceDlg : public CDialog
{
	DECLARE_DYNAMIC(CImitDeviceDlg)

public:

	CImitDeviceDlg(CDeviceData* data, int mode, CWnd* pParent = NULL);	// �����������
	virtual ~CImitDeviceDlg();									// ����������

	enum { IDD = IDD_CREATE_DEVICE_IMIT };	// ������������� �������

	CEdit m_Number;			// ��������������� ���� "����� �������"
	CComboBox m_Type;		// ������ ��� ������ "���� �������"

protected:

	CDeviceData *DevData;	// ��������� �� ������������� ������

	int Mode;				// ����� ������

	int m_vType;			// ������ ���������� "���� �������"
	int m_vNumber;			// ������ ���������� "������ �������"

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();						// ������������� �������

	virtual void OnOK()
	{};
	virtual void OnCancel();		// ��������� ������� "�������� ���� ������ � ����� �� �������"

	afx_msg void OnAccept();		// ��������� ������� "������� ������ � ����� �� �������"
	afx_msg void OnHelp();			// ��������� ������� "������� �������"

	bool IsChanged();
	int Retype(int ind);
	int Untype(int type);

	DECLARE_MESSAGE_MAP()
};

#endif
