#include "stdafx.h"

#include "CreateSSTWaitDlg.h"
#include "BranchInfo.h"

IMPLEMENT_DYNAMIC(CCreateSSTWaitDlg, CWaitingDlg)

CCreateSSTWaitDlg::CCreateSSTWaitDlg(CProjectData* prj, VPlate* plates, CWnd* pParent)
	: CWaitingDlg(pParent)
{
	Plates = plates;
	pPrj = prj;

	Comment = ParcelLoadString(STRID_CREATE_SST_WAIT_COMMENT);
}

CCreateSSTWaitDlg::~CCreateSSTWaitDlg()
{
}

void CCreateSSTWaitDlg::DoDataExchange(CDataExchange* pDX)
{
	CWaitingDlg::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CCreateSSTWaitDlg, CWaitingDlg)
END_MESSAGE_MAP()


BOOL CCreateSSTWaitDlg::OnInitDialog()
{
	CWaitingDlg::OnInitDialog();

	return true;
}

void CCreateSSTWaitDlg::OK()
{
	CWaitingDlg::OK();
}

void CCreateSSTWaitDlg::Stop()
{
	CWaitingDlg::Stop();
}

void CCreateSSTWaitDlg::Work()
{
	ParcelWait(true);
	
	bool res = pPrj->CreateSST(Plates);

	ParcelWait(false);

	if(res)
		OK();
	else
		Stop();
}
