#include "stdafx.h"

#include "ParamData.h"


CParamData::CParamData(HTREEITEM hParent)
{
	Name = ParcelLoadString(STRID_DEFNAME_PAR);
	hParentItem = hParent;
	UseStdMoisture = false;
	StdMoisture = -1.;
	Unit = "%";
	IFormat = 2;
}

CParamData::~CParamData()
{
}

void CParamData::Copy(CParamData& data)
{
	hParentItem = data.GetParentHItem();

	Name = data.GetName();

	UseStdMoisture = data.IsUseStdMoisture();
	StdMoisture = data.GetStdMoisture();
	Unit = data.GetUnit();
	IFormat = data.GetIFormat();
}

HTREEITEM CParamData::GetParentHItem()
{
	return hParentItem;
}

void CParamData::SetParentHItem(HTREEITEM item)
{
	hParentItem = item;
}

CString CParamData::GetName()
{
	return Name;
}

void CParamData::SetName(CString name)
{
	Name = name;
}

bool CParamData::IsUseStdMoisture()
{
	return UseStdMoisture;
}

void CParamData::SetUseStdMoisture(bool flag)
{
	UseStdMoisture = flag;
}

double CParamData::GetStdMoisture()
{
	return StdMoisture;
}

CString CParamData::GetStdMoistureStr()
{
	CString s;

	if(UseStdMoisture)
		s.Format(cFmt31, StdMoisture);
	else
		s = ParcelLoadString(STRID_FIELD_NA);

	return s;
}

void CParamData::SetStdMoisture(double val)
{
	StdMoisture = val;
}

CString CParamData::GetUnit()
{
	return Unit;
}

void CParamData::SetUnit(CString str)
{
	Unit = str;
}

int CParamData::GetIFormat()
{
	return IFormat;
}

CString CParamData::GetFormat()
{
	return cGetFormat(IFormat);
}

void CParamData::SetFormat(int dig)
{
	IFormat = dig;
}

void CParamData::SetFormat(CString fmt)
{
	int l = fmt.GetLength();
	if(fmt[l-1] == 'f')
	{
		CString Fmt = fmt;
		Fmt.Delete(0, l-2);
		Fmt.Delete(1);
		IFormat = _tstoi(Fmt);
	}
	else
		IFormat = 0;
}

void CParamData::GetDataForBase(prdb::PRIndex& Obj)
{
	Obj.IdxName = Name;
	Obj.IsUseStHumidity = UseStdMoisture;
	Obj.StHumidity = StdMoisture;
	Obj.IdxUnits = Unit;
	Obj.IdxFormat = IFormat;
}

void CParamData::SetDataFromBase(prdb::PRIndex& Obj)
{
	Name = Obj.IdxName;
	UseStdMoisture = Obj.IsUseStHumidity;
	StdMoisture = Obj.StHumidity;
	Unit = Obj.IdxUnits;
	IFormat = Obj.IdxFormat;
}
