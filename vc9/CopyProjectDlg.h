#ifndef _COPY_PROJECT_DLG_H_
#define _COPY_PROJECT_DLG_H_

#pragma once

#include "Resource.h"
#include "ProjectData.h"

///////////////////////////////////////////////////////////////////////////////////////////////////
// ������ ������ ��������� ������� ��� �����������

class CCopyProjectDlg : public CDialog
{
	DECLARE_DYNAMIC(CCopyProjectDlg)

public:

	CCopyProjectDlg(CProjectData* data, CWnd* pParent = NULL);		// �����������
	virtual ~CCopyProjectDlg();										// ����������

	enum { IDD = IDD_COPY_PROJECT };	// ������������� �������

	CComboBox m_ParDev;			// ������ ��� ������ "������������� �������" ��� �����������
	CComboBox m_ParPrj;			// ������ ��� ������ "����������� �������"
	CComboBox m_CopySet;		// ������ ��� ������ "����� �����������"

protected:

	CProjectData *PrjData;		// ��������� �� ������������� ������
	CDeviceData *ParentDev;		// ��������� �� ������, ������������ ��� �������

	int m_vParDev;				// ������ ���������� "������������� �������"
	int m_vParPrj;				// ������ ���������� "����������� �������"
	int m_vCopySet;				// ������ ���������� "����� �����������"

	VStr DevNames;				// ������ ���� ��������, �������� ��� ������������
	VStr PrjNames;				// ������ ���� ��������, �������� ��� �����������

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();						// ������������� �������

	virtual void OnOK()
	{};
	virtual void OnCancel();					// ��������� ������� "�������� ���� � ����� �� �������"

	afx_msg void OnAccept();					// ��������� ������� "������� ������ � ����� �� �������"
	afx_msg void OnHelp();						// ��������� ������� "������� �������"
	afx_msg void OnParDevChange();				// ��������� ������� "�������� ������������ ������"

	void FillListProjects();					// ��������� ������ ���� �������� ��������

	DECLARE_MESSAGE_MAP()
};

#endif
