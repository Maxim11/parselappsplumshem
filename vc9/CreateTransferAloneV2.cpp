#include "stdafx.h"

#include "RootData.h"
#include "BranchInfo.h"
#include "CreateTransferAloneV2.h"
#include "ShowSpectraDlg.h"
#include "LoadSpectraPBDlg.h"
#include "MainFrm.h"

#include <math.h>


//static CProjectData *pPrj;

CCreateTransferAloneV2::CCreateTransferAloneV2(CTransferData* trans, CDeviceData* pDev, CProjectData* pPrj, int itrans)
{
	CalcInfo.TransData = trans;
	PrjData = GetProjectByHItem(trans->GetParentHItem());

	MDev = pDev;
	MPrj = pPrj;
	iTrans = itrans;
}

bool CCreateTransferAloneV2::Create()
{
	if(!AcceptData()) return false;

	if(CalcInfo.bNoCorr && !CalcInfo.LoadSpectraData())	return false;

	bool bOK = CalcInfo.MakeCorrection(cEmpty);

	CalcInfo.FreeSpectraData();

	return bOK;
}

bool CCreateTransferAloneV2::AcceptData()
{
	CTransferData *MTrans = MPrj->GetTransfer(iTrans);
	if(MTrans == NULL) return false; 


	CString strName;
	strName.Format(L"%08d", MTrans->GetMasterDevNumber());

	CalcInfo.TransData->SetName(strName);
	CalcInfo.TransData->SetMasterDevType(MTrans->GetType());
	CalcInfo.TransData->SetMasterDevNumber(MTrans->GetMasterDevNumber());

	CalcInfo.bNoCorr = true;

	CalcInfo.TransData->SetLowLimSpec(max(PrjData->GetLowLimSpecRange(), MPrj->GetLowLimSpecRange()));
	CalcInfo.TransData->SetUpperLimSpec(min(PrjData->GetUpperLimSpecRange(), MPrj->GetUpperLimSpecRange()));

	CalcInfo.PrjM = MPrj;
	CalcInfo.ModM = NULL;
	CalcInfo.iTransM = iTrans;

	CalcInfo.MSamNames.clear();
	int nSam = MTrans->GetNSamples();
	for(int i=0; i<nSam; i++)
	{
		CalcInfo.MSamNames.push_back(MTrans->GetSample(i)->GetName());
	}

	CalcInfo.MParNames.clear();
	int nPar = MPrj->GetNParams();
	for(int i=0; i<nPar; i++)
	{
		CalcInfo.MParNames.push_back(MPrj->GetParam(i)->GetName());
	}

	CalcInfo.SamplesCorr.clear();

	CalcInfo.Preprocs.clear();
	TransPrepInfo NewPrep;
	VInt empPrep;
	NewPrep.Preproc = empPrep;
	CalcInfo.Preprocs.push_back(NewPrep);

	return true;
}


