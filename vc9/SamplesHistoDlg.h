#ifndef _SAMPLES_HISTO_DLG_H_
#define _SAMPLES_HISTO_DLG_H_

#pragma once

#include "Resource.h"
#include "ProjectData.h"
#include "TransferData.h"

// ��������� ������ ������ �������

const int C_SAMPLESH_CALIB		= 0;
const int C_SAMPLESH_VALID		= 1;

// ������ ������������ ����
/*
const int C_SAMPLESH_MENU_PREV		= 11;
const int C_SAMPLESH_MENU_NEXT		= 12;
const int C_SAMPLESH_MENU_FULL		= 13;
const int C_SAMPLESH_MENU_AUTO		= 14;
const int C_SAMPLESH_MENU_LEFT		= 15;
const int C_SAMPLESH_MENU_RIGHT		= 16;
*/
const int C_SAMPLESH_MENU_LEGEND	= 17;
const int C_SAMPLESH_MENU_GRID		= 18;

///////////////////////////////////////////////////////////////////////////////////////////////////
// ������ ������ ����������� ������������� �������� �� ����������� ������

class CSamplesHistoDlg : public CDialog
{
	DECLARE_DYNAMIC(CSamplesHistoDlg)

public:

	CSamplesHistoDlg(VPSam *data, VModPar* params, CProjectData* prj, int mode, CWnd* pParent = NULL);	// �����������
	virtual ~CSamplesHistoDlg();										// ����������

	enum { IDD = IDD_SAMPLES_HISTO };	// ������������� �������

	CComboBox m_Param;
	CEdit m_NPart;
	CSpinButtonCtrl m_SpinLetter;

	CMenu *pMenu1;

protected:

	int Mode;

	int m_vParam;
	int m_vNPart;

	VPSam* Data;
	VModPar* Params;
	CProjectData* ParentPrj;

	VDbl y;
	VDbl x;

	CLumChartDataSet* pGraph;
	CLumChart *pChart;

	POSITION posInfo;

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();						// ������������� �������
	virtual void OnCancel();

	afx_msg void OnSpectraListSelChange();
	afx_msg void OnDefault();
	afx_msg void OnChangeParam();
	afx_msg	void OnNPartChange();
	afx_msg void OnHelp();				// ��������� ������� "������� �������"

	void OnSpinLetterDeltaPos(NMHDR *pNMHDR, LRESULT *pResult);
/*
	void OnUserMenuPrev();
	void OnUserMenuNext();
	void OnUserMenuFull();
	void OnUserMenuAuto();
	void OnUserMenuLeft();
	void OnUserMenuRight();
	void OnUserMenuLegend();
*/
	void OnUserMenuGrid();

	int CalcDefault();

	void CreateGraph();
	void CreateChart();

	void BuildUserPopupMenu();

public:

	void ShowInfo(CRect* rect);
	void RemoveInfo();

	void SetStatusMenu();

protected:

	DECLARE_MESSAGE_MAP()
};

#endif
