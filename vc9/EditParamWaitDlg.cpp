#include "stdafx.h"

#include "EditParamWaitDlg.h"
#include "BranchInfo.h"

IMPLEMENT_DYNAMIC(CEditParamWaitDlg, CWaitingDlg)

CEditParamWaitDlg::CEditParamWaitDlg(CProjectData* prj, bool mode, CParamData* newpar, CParamData* oldpar, CWnd* pParent) : CWaitingDlg(pParent)
{
	PrjData = prj;
	NewParam = newpar;
	OldParam = oldpar;
	Mode = mode;

	Comment = (Mode == C_MODE_PARAM_WAIT_CREATE) ? ParcelLoadString(STRID_ADD_PARAM_WAIT_COMMENT) : ParcelLoadString(STRID_EDIT_PARAM_WAIT_COMMENT);
}

CEditParamWaitDlg::~CEditParamWaitDlg()
{
}

void CEditParamWaitDlg::DoDataExchange(CDataExchange* pDX)
{
	CWaitingDlg::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CEditParamWaitDlg, CWaitingDlg)
END_MESSAGE_MAP()


BOOL CEditParamWaitDlg::OnInitDialog()
{
	CWaitingDlg::OnInitDialog();

	return true;
}

void CEditParamWaitDlg::OK()
{
	CWaitingDlg::OK();
}

void CEditParamWaitDlg::Stop()
{
	CWaitingDlg::Stop();
}

void CEditParamWaitDlg::Work()
{
	ParcelWait(true);
	
	bool res;
	if(Mode == C_MODE_PARAM_WAIT_CREATE)
		res = PrjData->AddParam(NewParam);
	else
		res = PrjData->ChangeParam(OldParam, NewParam);

	ParcelWait(false);

	if(res)
		OK();
	else
		Stop();
}
