#include "stdafx.h"

#include "TransferWaitDlg.h"
#include "BranchInfo.h"

IMPLEMENT_DYNAMIC(CTransferWaitDlg, CWaitingDlg)

CTransferWaitDlg::CTransferWaitDlg(CTransferCalculate* trans, int mode, CWnd* pParent) : CWaitingDlg(pParent)
{
	TransCalc = trans;
	Mode = mode;
	Comment = ParcelLoadString(STRID_TRANSFER_WAIT_COMMENT);
}

CTransferWaitDlg::~CTransferWaitDlg()
{
}

void CTransferWaitDlg::DoDataExchange(CDataExchange* pDX)
{
	CWaitingDlg::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CTransferWaitDlg, CWaitingDlg)
END_MESSAGE_MAP()


BOOL CTransferWaitDlg::OnInitDialog()
{
	CWaitingDlg::OnInitDialog();

	return true;
}

void CTransferWaitDlg::OK()
{
	CWaitingDlg::OK();
}

void CTransferWaitDlg::Stop()
{
	CWaitingDlg::Stop();
}

void CTransferWaitDlg::Work()
{
	ParcelWait(true);
	
	bool res = (Mode == C_MODE_TRANSFER_NORMAL) ? TransCalc->AcceptTransfer() : TransCalc->AcceptWithout();

	ParcelWait(false);

	if(res)
		OK();
	else
		Stop();
}
