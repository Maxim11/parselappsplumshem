#ifndef _TRANS_CALC_H_
#define _TRANS_CALC_H_

#include "ProjectData.h"
#include "SpLumChem.h"


struct SampleCorrDistance
{
	CString SCName;
	double dist;
};

typedef vector<SampleCorrDistance> VSCDist;
typedef vector<SampleCorrDistance>::iterator ItSCDist;

struct TransPrepInfo
{
	bool IsCalc;

	VInt Preproc;
	double DistMin;
	double DistMax;
	double DistMean;
	VSCDist DistSpec;

	TransPrepInfo():DistMin(0.),DistMax(0.),DistMean(0.),IsCalc(false)
	{};
};

typedef vector<TransPrepInfo> VTransPrepInfo;
typedef vector<TransPrepInfo>::iterator ItTransPrepInfo;

typedef vector<CSampleProxy> VSamProxy;
typedef vector<CSampleProxy>::iterator ItSamProxy;

class CTransferCalculate
{
public:

	CTransferCalculate();
	~CTransferCalculate();

public:

	CTransferData* TransData;
	CProjectData* PrjM;
	CModelData* ModM;
	int iTransM;

	VStr MSamNames;
	VStr MParNames;
	VModSam SamplesCorr;

	VTransPrepInfo Preprocs;

	VDbl SpecPoints;

	VSamProxy SamplesMA, SamplesMC, SamplesSC, SamplesSA;
	CFreqScale FreqScale;
	CSpecPreprocData_Parcel* pPPD;

	bool bNoCorr;
	bool IsBreak;

	void *pSpCorr;

public:

	bool CalculateCorrection();
	bool MakeCorrection(CString prepstr);
	void DeleteCorrection();

	bool AcceptTransfer();
	bool AcceptWithout();

	bool LoadSpectraData();
	void FreeSpectraData();

	TransPrepInfo* GetPrepInfo(int ind);

	void FillSampleProxy(VSamProxy& SamplesMA, VSamProxy& SamplesMC, VSamProxy& SamplesSC);
	void FillFreqScale(CFreqScale& FreqScale);
	void FillSpecPreproc(vector<string>& prePar);

	void GetSpecPoints();
	int GetSpecPointInd(double value);

	CString GetErrorStr(int ecode);
};

#endif
