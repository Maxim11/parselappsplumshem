#ifndef _PARCEL_BRANCH_INFO_H_
#define _PARCEL_BRANCH_INFO_H_

#pragma once

#include "DeviceData.h"
#include "PRDBProject.h"
#include "OptimTemplate.h"


const int TIM_NONE = -1;
const int TIM_ROOT = 0;
const int TIM_DEVICE = 1;
const int TIM_PROJECT = 2;
const int TIM_SAMPLESET = 3;
const int TIM_MODELSET = 4;
const int TIM_METHODSET = 5;
const int TIM_SAMPLEOWN = 6;
const int TIM_SAMPLETRANS = 7;
const int TIM_MODEL = 8;
const int TIM_METHOD = 9;
const int TIM_PARAMSET = 10;

struct BranchInfo
{
	CDeviceData* Device;
	CProjectData* Project;
	CTransferData* Trans;
	CModelData* Model;
	CMethodData* Method;

	HTREEITEM hDevice;
	HTREEITEM hProject;
	HTREEITEM hParamSet;
	HTREEITEM hSampleSet;
	HTREEITEM hModelSet;
	HTREEITEM hMethodSet;
	HTREEITEM hSampleTrans;
	HTREEITEM hModel;
	HTREEITEM hMethod;

	BranchInfo():Device(0),Project(0),Trans(0),Model(0),Method(0),hDevice(0),hProject(0),hParamSet(0),
		hSampleSet(0),hSampleTrans(0),hModelSet(0),hMethodSet(0),hModel(0),hMethod(0)
	{};
	void Clear()
	{
		Device = NULL;
		Project = NULL;
		Trans = NULL;
		Model = NULL;
		Method = NULL;

		hDevice = NULL;
		hProject = NULL;
		hParamSet = NULL;
		hSampleSet = NULL;
		hSampleTrans = NULL;
		hModelSet = NULL;
		hMethodSet = NULL;
		hModel = NULL;
		hMethod = NULL;
	}
};

int GetBranchInfo(HTREEITEM item, BranchInfo &Info);
CDeviceData* GetDeviceByHItem(HTREEITEM item);
CProjectData* GetProjectByHItem(HTREEITEM item);
CTransferData* GetTransferByHItem(HTREEITEM item);
COptimTemplate* GetOptimTemplates();

CPRDBProject* GetDatabase();

#endif
