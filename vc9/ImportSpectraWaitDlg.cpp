#include "stdafx.h"

#include "ImportSpectraWaitDlg.h"
#include "BranchInfo.h"

IMPLEMENT_DYNAMIC(CImportSpectraWaitDlg, CWaitingDlg)

CImportSpectraWaitDlg::CImportSpectraWaitDlg(CDeviceData* dev, CString prjname, apjdb::PRApjProject* apjx,
		VImpSam* isam, bool rep, CWnd* pParent) : CWaitingDlg(pParent)
{
	DevData = dev;
	PrjName = prjname;
	Apjx = apjx;
	ISamples = isam;
	repRef = rep;

	Comment = ParcelLoadString(STRID_IMPORT_SPECTRA_WAIT_COMMENT);
}

CImportSpectraWaitDlg::~CImportSpectraWaitDlg()
{
}

void CImportSpectraWaitDlg::DoDataExchange(CDataExchange* pDX)
{
	CWaitingDlg::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CImportSpectraWaitDlg, CWaitingDlg)
END_MESSAGE_MAP()


BOOL CImportSpectraWaitDlg::OnInitDialog()
{
	CWaitingDlg::OnInitDialog();

	return true;
}

void CImportSpectraWaitDlg::OK()
{
	CWaitingDlg::OK();
}

void CImportSpectraWaitDlg::Stop()
{
	CWaitingDlg::Stop();
}

void CImportSpectraWaitDlg::Work()
{
	ParcelWait(true);
	
	bool res = DevData->AcceptImportSpectra(PrjName, Apjx, ISamples, repRef);

	ParcelWait(false);

	if(res)
		OK();
	else
		Stop();
}
