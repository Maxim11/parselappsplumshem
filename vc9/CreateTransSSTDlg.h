#ifndef _CREATE_TRANS_SST_DLG_H_
#define _CREATE_TRANS_SST_DLG_H_

#pragma once

#include "Resource.h"
#include "ProjectData.h"

///////////////////////////////////////////////////////////////////////////////////////////////////
// ������ �������� ������ ����������� ��������

// �������� �������

const int C_TRANSSSTDLG_PAGE_FRAME	= 0;		// �������� ������
const int C_TRANSSSTDLG_PAGE_PLATE1 = 1;		// �������� 1-� ��������
const int C_TRANSSSTDLG_PAGE_PLATE2 = 2;		// �������� 2-� ��������
const int C_TRANSSSTDLG_PAGE_PLATE3 = 3;		// �������� 3-� ��������

class CCreateTransSSTDlg : public CDialog
{
	DECLARE_DYNAMIC(CCreateTransSSTDlg)

public:

	CCreateTransSSTDlg(CProjectData* prj, VPlate* plates, CWnd* pParent = NULL);		// �����������
	virtual ~CCreateTransSSTDlg();													// ����������

	enum { IDD = IDD_CREATE_TRANS_SST };	// ������������� �������

	CTabCtrl  m_Tab;		// �������� �� ��������

	CComboBox m_Frame;		// ������ ��� ������ "������"
	CComboBox m_Plate1;		// ������ ��� ������ "1-� ��������"
	CComboBox m_Plate2;		// ������ ��� ������ "2-� ��������"
	CComboBox m_Plate3;		// ������ ��� ������ "3-� ��������"
	CEdit m_Koeff1;			// ���� ��� ������������� "������������ ��������� 1-� ��������"
	CEdit m_Koeff2;			// ���� ��� ������������� "������������ ��������� 2-� ��������"
	CEdit m_Koeff3;			// ���� ��� ������������� "������������ ��������� 3-� ��������"
	CEdit m_Width1;			// ���� ��� ������������� "������� 1-� ��������"
	CEdit m_Width2;			// ���� ��� ������������� "������� 2-� ��������"
	CEdit m_Width3;			// ���� ��� ������������� "������� 3-� ��������"
	CEdit m_AddWidth11;		// ���� ��� ������������� "1-� �������������� ������� 1-� ��������"
	CEdit m_AddWidth12;		// ���� ��� ������������� "1-� �������������� ������� 2-� ��������"
	CEdit m_AddWidth13;		// ���� ��� ������������� "1-� �������������� ������� 3-� ��������"
	CEdit m_AddWidth21;		// ���� ��� ������������� "2-� �������������� ������� 1-� ��������"
	CEdit m_AddWidth22;		// ���� ��� ������������� "2-� �������������� ������� 2-� ��������"
	CEdit m_AddWidth23;		// ���� ��� ������������� "2-� �������������� ������� 3-� ��������"
	CEdit m_AddWidth31;		// ���� ��� ������������� "3-� �������������� ������� 1-� ��������"
	CEdit m_AddWidth32;		// ���� ��� ������������� "3-� �������������� ������� 2-� ��������"
	CEdit m_AddWidth33;		// ���� ��� ������������� "3-� �������������� ������� 3-� ��������"
	CEdit m_AddWidth41;		// ���� ��� ������������� "4-� �������������� ������� 1-� ��������"
	CEdit m_AddWidth42;		// ���� ��� ������������� "4-� �������������� ������� 2-� ��������"
	CEdit m_AddWidth43;		// ���� ��� ������������� "4-� �������������� ������� 3-� ��������"

protected:

	CProjectData* ParentPrj;
	VPlate *Plates;			// ��������� �� ������������ ��������� �������

	int m_vTab;				// ����� �������� ��������

	int m_vFrame;			// ������ ��������� "������"
	int m_vPlate1;			// ������ ��������� "1-� ��������"
	int m_vPlate2;			// ������ ��������� "2-� ��������"
	int m_vPlate3;			// ������ ��������� "3-� ��������"
	double m_vKoeff1;		// ��������� �������� "������������ ��������� 1-� ��������"
	double m_vKoeff2;		// ��������� �������� "������������ ��������� 2-� ��������"
	double m_vKoeff3;		// ��������� �������� "������������ ��������� 3-� ��������"
	double m_vWidth1;		// ��������� �������� "������� 1-� ��������"
	double m_vWidth2;		// ��������� �������� "������� 2-� ��������"
	double m_vWidth3;		// ��������� �������� "������� 3-� ��������"
	double m_vAddWidth11;	// ��������� �������� "1-� �������������� ������� 1-� ��������"
	double m_vAddWidth12;	// ��������� �������� "1-� �������������� ������� 2-� ��������"
	double m_vAddWidth13;	// ��������� �������� "1-� �������������� ������� 3-� ��������"
	double m_vAddWidth21;	// ��������� �������� "2-� �������������� ������� 1-� ��������"
	double m_vAddWidth22;	// ��������� �������� "2-� �������������� ������� 2-� ��������"
	double m_vAddWidth23;	// ��������� �������� "2-� �������������� ������� 3-� ��������"
	double m_vAddWidth31;	// ��������� �������� "3-� �������������� ������� 1-� ��������"
	double m_vAddWidth32;	// ��������� �������� "3-� �������������� ������� 2-� ��������"
	double m_vAddWidth33;	// ��������� �������� "3-� �������������� ������� 3-� ��������"
	double m_vAddWidth41;	// ��������� �������� "4-� �������������� ������� 1-� ��������"
	double m_vAddWidth42;	// ��������� �������� "4-� �������������� ������� 2-� ��������"
	double m_vAddWidth43;	// ��������� �������� "4-� �������������� ������� 3-� ��������"

	VModSam SamNames;		// ������ �������� ��������

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();						// ������������� �������

	virtual void OnOK()
	{};
	virtual void OnCancel();		// ��������� ������� "�������� ���� ������ � ����� �� �������"

	afx_msg void OnAccept();		// ��������� ������� "������� ������ � ����� �� �������"
	afx_msg void OnTabSelchange(NMHDR* pNMHDR, LRESULT* pResult);	// ��������� ������� "������� �� ������ ��������"
	afx_msg void OnSelect();		// ��������� ������� "������� �������"
	afx_msg void OnHelp();				// ��������� ������� "������� �������"

	void ShowFields(int page = -1);		// ������� �� ����� ��������, ��������������� �������� ��������

	void CreateSamData();
	void FillStartData();

	DECLARE_MESSAGE_MAP()
};

#endif
