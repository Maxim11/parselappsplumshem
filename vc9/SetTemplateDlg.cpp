#include "stdafx.h"

#include "UniString\\nStr.h"
#include "SetTemplateDlg.h"
#include "RootData.h"
#include "MainFrm.h"
#include "FileSystem.h"

using namespace filesystem;

IMPLEMENT_DYNAMIC(CSetTemplateDlg, CDialog)

CSetTemplateDlg::CSetTemplateDlg(int mode, CWnd* pParent)
	: CDialog(CSetTemplateDlg::IDD, pParent)
{
	Mode = mode;

	m_vName = MakePath(pRoot->GetTemplateFolder(Mode), pRoot->GetTemplateName(Mode));
	Lang = pFrame->GetLanguage();
}

CSetTemplateDlg::~CSetTemplateDlg()
{
}

void CSetTemplateDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_TEMPLATE_NAME, m_Name);

	cParcelDDX(pDX, IDC_TEMPLATE_NAME, m_vName);
	DDV_MaxChars(pDX, m_vName, 200);
}


BEGIN_MESSAGE_MAP(CSetTemplateDlg, CDialog)

	ON_BN_CLICKED(IDC_ACCEPT, OnAccept)
	ON_BN_CLICKED(IDHELP, OnHelp)
	ON_BN_CLICKED(IDC_EDIT_TEMPLATE, OnEdit)
	ON_BN_CLICKED(IDC_CREATE_TEMPLATE, OnCreate)
	ON_BN_CLICKED(IDC_OPEN_TEMPLATE, OnOpen)

END_MESSAGE_MAP()


BOOL CSetTemplateDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	CString Hdr;
	switch(Mode)
	{
	case C_TEMPLATE_QNT:  Hdr = ParcelLoadString(STRID_SET_TEMPLATE_HDR_QNT);  break;
	case C_TEMPLATE_QLT:  Hdr = ParcelLoadString(STRID_SET_TEMPLATE_HDR_QLT);  break;
	}
	SetWindowText(Hdr);

	GetDlgItem(IDC_ACCEPT)->SetWindowText(ParcelLoadString(STRID_SET_TEMPLATE_ACCEPT));
	GetDlgItem(IDCANCEL)->SetWindowText(ParcelLoadString(STRID_SET_TEMPLATE_DECLINE));
	GetDlgItem(IDHELP)->SetWindowText(ParcelLoadString(STRID_SET_TEMPLATE_HELP));
	GetDlgItem(IDC_EDIT_TEMPLATE)->SetWindowText(ParcelLoadString(STRID_SET_TEMPLATE_EDIT));
	GetDlgItem(IDC_CREATE_TEMPLATE)->SetWindowText(ParcelLoadString(STRID_SET_TEMPLATE_CREATE));
	GetDlgItem(IDC_OPEN_TEMPLATE)->SetWindowText(ParcelLoadString(STRID_SET_TEMPLATE_OPEN));
	GetDlgItem(IDC_STATIC_1)->SetWindowText(ParcelLoadString(STRID_SET_TEMPLATE_NAME));

	GetDlgItem(IDHELP)->EnableWindow(pFrame->IsExistHelp());

	return true;
}

void CSetTemplateDlg::OnAccept()
{
	if(!UpdateData())
		return;

	m_vName.Trim();
	if(m_vName.IsEmpty())
	{
		ParcelError(ParcelLoadString(STRID_SET_TEMPLAT_ERROR_EMPTYNAME));
		return;
	}

	CString s = ExtractFileName(m_vName);
	pRoot->SetTemplateName(s, Mode);

	CDialog::OnOK();
}

void CSetTemplateDlg::OnCancel()
{
	CDialog::OnCancel();
}

void CSetTemplateDlg::OnEdit()
{
	UpdateData();

	m_vName.Trim();
	if(!IsExistPath(m_vName))
	{
		CString s;
		s.Format(ParcelLoadString(STRID_SET_TEMPLAT_ERROR_WRONGPATH), m_vName);
		ParcelError(s);
		return;
	}

	CString strCmdLine = MakeEditorCommandLine( m_vName
												, NULL
												, NULL /*m_paletteID*/
												, pRoot->GetTemplateFolder(Mode));
	RunEditor( strCmdLine );
}

void CSetTemplateDlg::OnCreate()
{
	UpdateData();
	CString strCmdLine = MakeEditorCommandLine( NULL /*strTmplPath*/
												, pRoot->GetPaletteFullName()
												, NULL
												, pRoot->GetTemplateFolder(Mode));
	RunEditor( strCmdLine );
}

void CSetTemplateDlg::OnOpen()
{
	CString Title = ParcelLoadString(STRID_STDTITLE_OPENTEMPLATE);
	CString Filter = LoadString(IDS_TEMPLATEFILE_FILTER);
	CString DefExt = LoadString(IDS_TEMPLATEFILE_DEFEXT);

	CString Folder = pRoot->GetTemplateFolder(Mode);
	SetCurrentDirectory(Folder);
	CString FileName = MakePath(Folder, pRoot->GetTemplateName(Mode));

	CFileDialog Dialog(TRUE, DefExt, FileName,
					   OFN_EXPLORER | OFN_HIDEREADONLY | OFN_PATHMUSTEXIST | OFN_OVERWRITEPROMPT,
					   Filter, this);

	Dialog.m_ofn.lpstrTitle = Title;

	if(Dialog.DoModal() == IDOK)
	{
		FileName = Dialog.GetPathName();
		if(FileName.IsEmpty())
		{
			ParcelError(ParcelLoadString(STRID_STDERR_FNEMPTY));
			return;
		}
		if(!PathFileExists(FileName))
		{
			ParcelError(ParcelLoadString(STRID_STDERR_FILENOEXIST));
			return;
		}

		m_vName = FileName;
		UpdateData(0);
	}
}

void CSetTemplateDlg::OnHelp()
{
	int IdHelp = DLGID_MODEL_RESULT_SET_TEMPLATE;

	pFrame->ShowHelp(IdHelp);
}

CString CSetTemplateDlg::MakeEditorCommandLine( LPCTSTR tmplPath, LPCTSTR palettePath, LPCTSTR paletteID, LPCTSTR tmplDir )
{
	//���������:
	//LumTemplateEditor.exe [/fif] [/aot] [/ot palette_path [/pfile file_path | /pfolder folder_path] ] [/op palette_path [/lg language_name]] [/l </an application_name> /avj application_version_major> </avn application_version_minor> </sn palette_id> </lg language> ]

	///fif 				- Fail If False �		���� �� ����� �������� ���������� � ���������� /l ��� /op ��� /ot ���������� ����, 
	//	�� ���������� ���������. 
	//	���� ���� �������� �����������, �� ��� ���� �� ����� �������� ���������� ������ � ����� 
	//	Ready.

	//	/aot				- Always On Top - 	���������� ����������� � ������ ������ ���� ����. ������ minimize � maximize �����������.

	//	/ot <����_�_�����>                  - Open Template -	��������� ������ �� ���������� ����. ���� ������ ���� �������� � �������. �� ��������� � ���������� �/op�

	//	/uilang <���� UI>					- ���� ����������������� ����������

	//	/pfile		- ��� ����� �������
	//	/pfolder		- ��� ����� ��� ������ ����������� �������

	//	/op <����_�_�����>		- Open Palette - 	��������� ������ ������� � ������ ������ �� ���������� �����. ���� ������ ���� 
	//	�������� � �������.

	//	/lg						- ���� �������.

	//	/l				- paLette -		���� � ����� ./Palette �������, ������� ������������� ��������� ����������, 
	//	� ��������� ��. 

	//	�������������� ��������� (��� ��������� �����������):

	///an <application_name>		- ��� ���������� (� ��������, ���� ��������� ����)
	//	/avj <application_version_major>	- ������ ����������.
	//	/avn <application_version_minor>	- ������ ����������.
	//	/sn <palette_id>			- �������� ������� (� ��������, ���� ��������� ����).
	//	/lg <language>				- ���� �������.


	//	����������:
	//����������� ��������� ��������� ����������: /fif, /aot � ���� ��: /ot, /op, /l
	//	�� ����������� ��������� ��������� ����������: /ot, /op, /l. � ���� ������ ���������� ������ ���� �������, ������� ����� ������� ���������: /l, /ot, /op

	CString strExe		= MakePath(pRoot->GetWorkFolder(), _T("LumTemplateEditor.exe"));
	CString strLng		= _T("");
	CString strAot		= _T(" /dm /aot");
	CString strArg		= _T("");
	CString strDir		= _T("");

	strLng.Format( _T(" /uilang %s"), Lang/*nstr::uiLanguage()*/ );

	if( tmplPath != NULL )
	{
		strArg.Format( _T(" /ot \"%s\""), tmplPath );
	}
	else if( palettePath != NULL )
	{
		strArg.Format( _T(" /op \"%s\" /lg %s"), palettePath, Lang/*nstr::uiLanguage()*/ );
	}

	if( tmplDir != NULL )
	{
		strDir.Format( _T(" /tfolder \"%s\""), tmplDir );
	}

	CString strCmdLine = _T("");
	strCmdLine.Format( _T("%s %s%s%s%s")
		, strExe
		, strLng
		, strAot
		, strArg
		, strDir );

	//TRACE(_T("%s\n"), (LPCTSTR)strCmdLine );

	return strCmdLine;
}
//-------------------------------------------------------------------------
void CSetTemplateDlg::RunEditor( CString& cmdline )
{
	STARTUPINFO         si;
	PROCESS_INFORMATION pi;

	SetCurrDir appdir(pRoot->GetWorkFolder());

	ZeroMemory( &si, sizeof(STARTUPINFO) );
	ZeroMemory( &pi, sizeof(PROCESS_INFORMATION) );

	si.cb          = sizeof( STARTUPINFO );
	si.dwFlags     = STARTF_USESHOWWINDOW;
	si.wShowWindow = SW_SHOWNORMAL;

	LPTSTR lpszExe = cmdline.GetBuffer(0);
	::CreateProcessW( NULL, lpszExe, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi );
	::WaitForInputIdle( GetCurrentProcess(), INFINITE );

	// Loop until process terminates.
	//...............................
	if( pi.hProcess )
	{
		DWORD dwExitCode = STILL_ACTIVE;
		while( dwExitCode == STILL_ACTIVE ) 
		{
			::WaitForSingleObject( pi.hProcess, 1000 );
			::GetExitCodeProcess( pi.hProcess, &dwExitCode );
		}			
	}
}
