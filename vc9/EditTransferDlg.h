#ifndef _EDIT_TRANSFER_DLG_H_
#define _EDIT_TRANSFER_DLG_H_

#pragma once

#include "Resource.h"
#include "TransferData.h"


///////////////////////////////////////////////////////////////////////////////////////////////////
// ������ �������������� ������ ������������ ��������

class CEditTransferDlg : public CDialog
{
	DECLARE_DYNAMIC(CEditTransferDlg)

public:

	CEditTransferDlg(CTransferData* data, CWnd* pParent = NULL);		// �����������
	virtual ~CEditTransferDlg();										// ����������

	enum { IDD = IDD_EDIT_TRANSFER };	// ������������� �������

	CEdit m_Name;				// ���� ��� �������������� "����� ����������"

protected:

	CTransferData* TransData;	// ��������� �� ������������� ����������
	CProjectData* ParentPrj;	// ��������� �� ������, ������������ ��� ����������

	CString m_vName;			// ��������� �������� "����� ����������"

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();						// ������������� �������

	virtual void OnOK()
	{};
	virtual void OnCancel();			// ��������� ������� "�������� ���� � ����� �� �������"

	afx_msg void OnAccept();			// ��������� ������� "������� ������ � ����� �� �������"
	afx_msg void OnHelp();				// ��������� ������� "������� �������"

	bool IsChanged();

	DECLARE_MESSAGE_MAP()
};

#endif
