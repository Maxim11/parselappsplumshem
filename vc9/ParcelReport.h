#ifndef _PARCEL_REPORT_H_
#define _PARCEL_REPORT_H_

#pragma once

#include "LumReport/LumReport_Api.h"
#include "ModelResultDlg.h"
#include "ModelQltResultDlg.h"

using namespace lumreport;

class CParcelReportQnt : public IAppCallback
{
public:

	CParcelReportQnt(CModelResultDlg *pmoddlg, int iparam);
	~CParcelReportQnt() {};

	CModelResultDlg* pModDlg;
	CModelData* pMod;
	CProjectData* pPrj;
	CModelResult* pRes;
	int CurPar;
	CString ParFmt;

protected:

	virtual CWSTR GetTextFor(Wuid wuid) const;
	virtual TextListItem const* GetTextArrayFor(Wuid wuid) const;
	virtual CWSTR GetPicturePathFor(Wuid wuid) const;
	virtual TableHint const* GetTableHintFor(Wuid wuid) const;
	virtual ColumnHint const* GetTableHeaderHintFor(Wuid wuid, Index col) const;
	virtual CWSTR GetTableCellFor(Wuid wuid, Index row, Index col) const;

	virtual void SetReportLanguage(CWSTR language);

};


class CParcelReportQlt : public IAppCallback
{
public:

	CParcelReportQlt(CModelQltResultDlg *pmoddlg);
	~CParcelReportQlt() {};

	CModelQltResultDlg* pModDlg;
	CModelData* pMod;
	CProjectData* pPrj;
	CModelResult* pRes;

protected:

	virtual CWSTR GetTextFor(Wuid wuid) const;
	virtual TextListItem const* GetTextArrayFor(Wuid wuid) const;
	virtual CWSTR GetPicturePathFor(Wuid wuid) const;
	virtual TableHint const* GetTableHintFor(Wuid wuid) const;
	virtual ColumnHint const* GetTableHeaderHintFor(Wuid wuid, Index col) const;
	virtual CWSTR GetTableCellFor(Wuid wuid, Index row, Index col) const;

	virtual void SetReportLanguage(CWSTR language);

};

#endif
