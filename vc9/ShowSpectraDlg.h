#ifndef _SHOW_SPECTRA_DLG_H_
#define _SHOW_SPECTRA_DLG_H_

#pragma once

#include "Resource.h"
#include "ShowSpectra.h"
#include "PlotSettingsDlg.h"
#include "afxwin.h"


// ������ ������������ ����

const int C_SHOWSPEC_MENU_PREV		= 11;
const int C_SHOWSPEC_MENU_NEXT		= 12;
const int C_SHOWSPEC_MENU_FULL		= 13;
const int C_SHOWSPEC_MENU_AUTO		= 14;
const int C_SHOWSPEC_MENU_LEFT		= 15;
const int C_SHOWSPEC_MENU_RIGHT		= 16;
const int C_SHOWSPEC_MENU_LEGEND	= 17;
const int C_SHOWSPEC_MENU_GRID		= 18;

///////////////////////////////////////////////////////////////////////////////////////////////////
// ������ ������ �������� ��������

class CShowSpectraDlg : public CDialog
{
	DECLARE_DYNAMIC(CShowSpectraDlg)

public:

	CShowSpectraDlg(CShowSpectra *data, int mode, CWnd* pParent = NULL);	// �����������
	virtual ~CShowSpectraDlg();												// ����������

	enum { IDD = IDD_SHOW_SPECTRA };	// ������������� �������

	CListBox m_list;					// ������ �������� ��� ������

	CMenu *pMenu1;
	bool m_bShowMarkers;

protected:

	int Mode;

	CShowSpectra* Data;

	vector<CLumChartDataSet> Graphs;
	CLumChart *pChart;

	POSITION posInfo;
	POSITION posCoord;

	VStr selS;					// ����� ���������� ��������

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();						// ������������� �������

	afx_msg void OnSpectraListSelChange();
	afx_msg void OnSettings();							// ��������� ������� "�������� ��������� �����������"
	afx_msg void OnHelp();				// ��������� ������� "������� �������"

	void OnUserMenuPrev();
	void OnUserMenuNext();
	void OnUserMenuFull();
	void OnUserMenuAuto();
	void OnUserMenuLeft();
	void OnUserMenuRight();
//	void OnUserMenuLegend();
	void OnUserMenuGrid();

	void FillList();									// ��������� ������ ��������

	void CreateGraphs();
	void ShowSelection();
	void CreateChart();

	void BuildUserPopupMenu();

public:

	void ShowSpecInfo(CRect* rect, bool isFrame);
	void ShowCoord(CPoint* pnt);
	void RemoveInfo();

	void SetStatusMenu();

protected:

	DECLARE_MESSAGE_MAP()
public:
	CButton m_checkShowMarkers;
	afx_msg void OnBnClickedShowMarkers();
};

#endif
