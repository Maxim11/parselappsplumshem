#ifndef _PARCEL_DEVICE_DATA_H_
#define _PARCEL_DEVICE_DATA_H_

#pragma once

#include "ProjectData.h"
#include "PRDBProject.h"
#include "LoadOldDatas.h"

typedef list<CProjectData> LPrj;
typedef list<CProjectData>::iterator ItPrj;

struct ImportSample
{
	CString SamName;
	MapIntBool Spectra;
	bool Diff;
	bool Include;

	int GetNSpectra();
	int GetNNewSpectra();
};

typedef vector<ImportSample> VImpSam;
typedef vector<ImportSample>::iterator ItImpSam;

///////////////////////////////////////////////////////////////////////////////////////////////////
// ����� ��� �������� �������

class CDeviceData
{
public:

	CDeviceData();					// �����������
	~CDeviceData();					// ����������

	void Copy(CDeviceData& data);	// ����������� ������

protected:

	HTREEITEM hItem;			// ������ ���� � ������, ���������������� �������

	int Type;					// ��� ������� (������)
	int Number;					// ����� �������

	VDbl ResolList;				// ������ ��������� ���������� ����������� � �������� �������

	// ��� �����������
	ProjectOldData* Apj;
	bool without;
	bool check_wave;

public:

	LPrj Projects;				// ������ �������� ������� ��� ������� �������

	CProjectData* pNewCopyPrj;
	CTreeCtrl* pTree;

public:

	HTREEITEM GetHItem();				// �������� ������ ���� � ������, ��������������� �������
	void SetHItem(HTREEITEM item);		// ���������� ������ ���� � ������, ��������������� �������

	void SetType(int ind);
	int GetType();						// �������� ��� �������
	CString GetTypeStr();				// �������� �������� ���� �������

	void SetNumber(int num);
	int GetNumber();					// �������� ����� �������

	CString GetName();					// �������� ������ ��� �������

	int GetNProjects();							// �������� ����� �������� ��� ������� �������
	CProjectData* GetProject(HTREEITEM item);	// ����� ������ �� ������� ���� � ������
	CProjectData* GetProject(CString name);		// ����� ������ �� �����
	CProjectData* GetProject(int ind);			// ����� ������ �� ����������� ������ � ������
	CProjectData* GetLastProject();				// ����� ������, ��������� � ������
	HTREEITEM GetLastProjectHItem();			// �������� ������ ���� � ������ ��� �������, ���������� � ������
	CProjectData* AddNewProject(CTreeCtrl* curTree);				// ������� ����� ������
	CProjectData* CreateSSTProject(CTreeCtrl* curTree);				// ������� ����������� ������
	CProjectData* AddOldApj(CString FileName, CTreeCtrl* pTree, bool IsSST = false);	// �������������� � �������� ������ ������ �������
	bool AcceptOldApj(bool IsSST);
	bool AcceptCopyPrj();
	CProjectData* ImportSpectra(CString FileName, CTreeCtrl* pTree);// ������������� ������� � ����� ���� � ������������ ������ �������
	bool ReadApjx(CString FileName, apjdb::PRApjProject& apjxData);
	bool AcceptImportSpectra(CString PrjName, apjdb::PRApjProject* Apjx, VImpSam* ISamples, bool repRef);
	bool ChangeProjectSettings(HTREEITEM item);	// �������� ��������� �������
	bool DeleteProject(HTREEITEM item);			// ������� ������
	bool DeleteProject(CString name);			// ������� ������

	bool GetDeviceData(CDeviceData& copydev, CString PrjName = cEmpty);

	int GetNResolutions();						// �������� ����� ��������� ���������� �������
	double GetResolutionFromList(int ind);		// �������� ��������� �������� ���������� �������
	int GetResolutionIndByMean(double mean);	// �������� ������ ���������� �� ��������

	bool SetResolList();						// ��������� ������ ��������� �������� ����������

	bool IsReadyForTrans(CProjectData *SPrj);	// ����� �� �������������� ��� �������� ��� ������-������
	bool IsSpectraExist();						// �������� ������, ���������� �� �������, ������������������ ��� ������� �������

	CString GetSSTName();
	bool IsSSTExist();

	void FreeAllSpectraData();

	void GetDataForBase(prdb::PRAppliance& Obj);
	void SetDataFromBase(prdb::PRAppliance& Obj);

	bool LoadDatabase(CTreeCtrl* pTree);
};

#endif
