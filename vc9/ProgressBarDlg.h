#ifndef _PROGRESS_BAR_DLG_H_
#define _PROGRESS_BAR_DLG_H_

#pragma once

#include "Resource.h"


///////////////////////////////////////////////////////////////////////////////////////////////////
// ������� ������ ��������� ��������� ������

class CProgressBarDlg : public CDialog
{
	DECLARE_DYNAMIC(CProgressBarDlg)

public:

	CProgressBarDlg(CWnd* pParent = NULL);		// �����������
	virtual ~CProgressBarDlg();					// ����������

	enum { IDD = IDD_PROGRESS_BAR };			// ������������� �������

protected:

	CWnd* pParentWnd;

	CProgressCtrl m_Progress;		// ���������� ������� ��������� ��������

	CString Hdr;					// ��������� ����
	CString Comment;				// ����������� � ������� ������
	CString Status;					// ������� ��������� �������� ������

	int Start;						// ��������� ������� �������
	int Finish;						// �������� ������� �������
	int Step;						// ��� �� �������
	int Position;					// ������� ������� �� �������

	int Elapsed;					// ����� ��������
	int Estimated;					// ������ ������� ��������� ������

	bool IsStopped;					// ������� ���������� ���������� �������
	bool IsPause;					// ������� ���������� ������������� �������

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();						// ������������� �������

	virtual void OnOK()
	{};
	virtual void OnCancel()
	{};

	virtual void OK();				// ��������� �������� ����� �� �������
	virtual void Stop();			// ��������� ����������� ����� �� �������
	virtual void Work();			// ������ �������� �������
	virtual void ResumeWork();		// ����������� ������� ����� �����

	afx_msg void OnTimer(UINT Id);	// ��������� ������� "������ ��������� �� �������"
	afx_msg void OnStop();			// ��������� ������� "�������� �������"
	afx_msg void OnPause();			// ��������� ������� "������������� �������"

protected:

	void SetHeader(CString str);	// ��������� ��������� ����
	void SetComment(CString str);	// ��������� �����������
	void SetStatus(CString str);	// ��������� ��������� ��������
	
	void SetRangeStep(int start, int finish, int step);	// ��������� ���������� �������
	void SetPosition(int pos);							// ��������� ������� ������� �� �������

	void SetElapsed(int sec);			// ��������� ������� ������ ��������
	void SetEstimated(int sec);			// ��������� ������ ������� ��������� ��������

	void ShowCurStatus();				// ����������� �������� ��������� ��������

	CString ConvertTimeToStr(int time);	// �������� ����� � ���� ������

	bool CheckStopPressed();			// �������� ������� �� ��������� ��������
	bool CheckPausePressed();			// �������� ������� �� ������������ ��������

	void UpdateAllWindows();

	DECLARE_MESSAGE_MAP()
};

#endif
