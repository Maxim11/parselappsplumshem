#ifndef _PARCEL_VIEW_H_
#define _PARCEL_VIEW_H_

#pragma once

#include "ParcelDoc.h"

#include "BranchInfo.h"
#include "RootData.h"
#include "ParcelListCtrl.h"


const int COMMAND_MODE_CREATE	= 0;
const int COMMAND_MODE_COPY		= 1;

class CParcelView : public CFormView
{
protected:

	CParcelView();
	DECLARE_DYNCREATE(CParcelView)

	CTreeCtrl m_Tree;
	CImageList m_ImageList;

	CRootData Data;

	int active;
	BranchInfo ActiveInfo;

	CListCtrl m_list_DevicePrj;

	CListCtrl m_list_ProjectSpec;
	CListCtrl m_list_ProjectTemp;
	CListCtrl m_list_ProjectSubj;
	CListCtrl m_list_ProjectRef;
	CListCtrl m_list_ProjectPlan;

	CListCtrl m_list_Params;
	CListCtrl m_list_ParamsSam;
	CListBox m_list_ParamsMod;

	CListCtrl m_list_SampleSets;

	CParcelListCtrl m_list_SamplesTrans;
	int m_nLSampParStart;
	int m_nLSampTimeCol;

	CListCtrl m_list_SpectraTrans;
	CListCtrl m_list_TransSettings;

	CListCtrl m_list_Models;

	CListCtrl m_list_ModParams;
	CListCtrl m_list_ModSet;
	CListBox m_list_ModSamples;
	CListBox m_list_ModSamplesVal;
	CListBox m_list_ModExcluded;

	CListCtrl m_list_Methods;

	CListCtrl m_list_MtdInfo;
	CListCtrl m_list_MtdRules;
	CListCtrl m_list_MtdQltModels;

public:

	enum{ IDD = IDD_PARCEL_FORM };

	bool needStartLoad;

	bool ExecuteBaseNew(CWnd *wnd);
	bool ExecuteBaseLoad(CWnd *wnd);
	bool ExecuteBaseClose();
	bool ExecuteBaseDelete();
	bool ExecuteBaseCopy(CWnd *wnd);
	bool CopyBaseTo(CString fname);
	bool StartBaseOpen();

	void CreateNewDevice();
	void ChangeDeviceSettings();
	void DeleteDevice();
	void CopyDeviceToBase(CWnd *wnd);
	bool IsCreateDeviceEnable();
	bool IsChangeDeviceSettingsEnable();
	bool IsDeleteDeviceEnable();
	bool IsCopyDeviceToBaseEnable();

	void CreateNewProject();
	void ChangeProjectSettings();
	void DeleteProject();
	void CreateSSTProject();
	void LoadOldProject(CWnd *wnd);
	void LoadOldSSTProject(CWnd *wnd);
	void CopyProjectToBase(CWnd* wnd);
	void ExportProjectSettings(CWnd *wnd);
	void ImportSpectra(CWnd *wnd);
	void ProjectTransfer();
	bool IsCreateProjectEnable();
	bool IsChangeProjectSettingsEnable();
	bool IsDeleteProjectEnable();
	bool IsCreateSSTProjectEnable();
	bool IsLoadOldProjectEnable();
	bool IsLoadOldSSTProjectEnable();
	bool IsCopyProjectToBaseEnable();
	bool IsExportProjectSettingsEnable();
	bool IsImportSpectraEnable();
	bool IsProjectTransferEnable();


	void EditParamList();
	bool IsEditParamListEnable();

	void EditSampleList();
	void CreateSSTTransfer();
	void DeleteSSTTransfer();
	void AddTransfer();
	void AddTransfer(CTransferData* TransData);
	void EditTransfer();
	void DeleteTransfer();
	bool IsEditSampleListEnable();
	bool IsCreateSSTTransferEnable();
	bool IsDeleteSSTTransferEnable();
	bool IsAddTransferEnable();
	bool IsEditTransferEnable();
	bool IsDeleteTransferEnable();
	bool IsReadOldSampleEnable();
	bool IsGetStoreSampleEnable();

	void CreateNewModel(int mode = COMMAND_MODE_CREATE);
	void AddNewModel(CModelData* ModData);
	void ChangeModelSettings();
	void DeleteModel();
	void ModelResult();
	bool IsCreateModelEnable();
	bool IsCopyModelEnable();
	bool IsChangeModelSettingsEnable();
	bool IsDeleteModelEnable();
	bool IsModelResultEnable();

	void InsertNewMethod(CMethodData* MtdData);
	void CreateNewMethod(int mode = COMMAND_MODE_CREATE);
	void ChangeMethodSettings();
	void DeleteMethod();
	void ExportMethod(CWnd *wnd);
	bool IsCreateMethodEnable();
	bool IsCopyMethodEnable();
	bool IsChangeMethodSettingsEnable();
	bool IsDeleteMethodEnable();
	bool IsExportMethodEnable();

	void ChangeLanguage(int lang);

	afx_msg void OnSize(UINT Type, int cx, int cy);
	afx_msg void OnTimer(UINT Id);	// ��������� ������� "������ ��������� �� �������"
	afx_msg void OnTvnSelchangedTree1(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnTvnItemexpandedTree1(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnParamsListSelChange(NMHDR* /*pNMHDR*/, LRESULT *pResult);
	afx_msg void OnSamplesTransListSelChange(NMHDR* /*pNMHDR*/, LRESULT *pResult);
	afx_msg void OnSamplesTransListColumnClick(NMHDR* pNMHDR, LRESULT* pResult);

	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate(); // called first time after construct

// Implementation
public:
	virtual ~CParcelView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	CParcelDoc* GetDocument() const;

protected:

	CFont FontHeader;

	void SetViewMode(HTREEITEM item, bool bFromScratch=true);

	void CreateSpecFonts();
	void FillParamsLists(CProjectData *Prj);
	void CreateSamplesTransList(CTransferData* Trans);
	void FillSpectraTransList(CSampleData *Sam);
	void RecreateColumns();

	void SetSamplesSortIcons();

	DECLARE_MESSAGE_MAP()

};

extern CParcelView *pView;


#ifndef _DEBUG  // debug version in ParcelView.cpp
inline CParcelDoc* CParcelView::GetDocument() const
   { return reinterpret_cast<CParcelDoc*>(m_pDocument); }
#endif

#endif
