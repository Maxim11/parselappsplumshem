#include "stdafx.h"

#include "RootData.h"
#include "BranchInfo.h"
#include "CreateProjectDlg.h"
#include "CopyProjectDlg.h"
#include "MainFrm.h"


IMPLEMENT_DYNAMIC(CCreateProjectDlg, CDialog)

CCreateProjectDlg::CCreateProjectDlg(CProjectData* data, int mode, CWnd* pParent)
	: CDialog(CCreateProjectDlg::IDD, pParent)
{
	PrjData = data;
	Mode = mode;

	ParentDev = GetDeviceByHItem(PrjData->GetParentHItem());

	m_vName = (Mode == PRJDLG_MODE_CREATE) ? cEmpty : PrjData->GetName();

	SetSettings(false);
}

CCreateProjectDlg::~CCreateProjectDlg()
{
}

void CCreateProjectDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_INPUT_NAME, m_Name);
	DDX_Control(pDX, IDC_LOWLIM_SPEC, m_LowSpec);
	DDX_Control(pDX, IDC_UPPERLIM_SPEC, m_UpperSpec);
	DDX_Control(pDX, IDC_NSCAN_SUBSAMP, m_NScanSub);
	DDX_Control(pDX, IDC_NMEAS_SUBSAMP, m_NMeasureSub);
	DDX_Control(pDX, IDC_NSCAN_STDSAMP, m_NScanStd);
	DDX_Control(pDX, IDC_NMEAS_STDSAMP, m_NMeasureStd);
	DDX_Control(pDX, IDC_BATH_LENGTH, m_BathLength);
	DDX_Control(pDX, IDC_ZERO_FILLING, m_ZeroFill);
	DDX_Control(pDX, IDC_SUBCANAL_RESOL, m_NResolSubj);
	DDX_Control(pDX, IDC_SUBCANAL_APODIZE, m_NApodizeSubj);
	DDX_Control(pDX, IDC_USE_REFCANAL, m_UseRef);
	DDX_Control(pDX, IDC_REFCANAL_RESOL, m_NResolRef);
	DDX_Control(pDX, IDC_REFCANAL_APODIZE, m_NApodizeRef);
	DDX_Control(pDX, IDC_LOWLIM_TEMP, m_LowTemp);
	DDX_Control(pDX, IDC_UPPERLIM_TEMP, m_UpperTemp);
	DDX_Control(pDX, IDC_MAXDIFF_TEMP, m_MaxDiff);

	DDX_CBIndex(pDX, IDC_NMEAS_STDSAMP, m_vNMeasureStd);
	DDX_CBIndex(pDX, IDC_ZERO_FILLING, m_vZeroFill);
	DDX_CBIndex(pDX, IDC_SUBCANAL_RESOL, m_vNResolSubj);
	DDX_CBIndex(pDX, IDC_SUBCANAL_APODIZE, m_vNApodizeSubj);
	DDX_CBIndex(pDX, IDC_REFCANAL_RESOL, m_vNResolRef);
	DDX_CBIndex(pDX, IDC_REFCANAL_APODIZE, m_vNApodizeRef);

	cParcelDDX(pDX, IDC_INPUT_NAME, m_vName);
	DDV_MaxChars(pDX, m_vName, 50);
	cParcelDDX(pDX, IDC_LOWLIM_SPEC, m_vLowSpec);
	cParcelDDV(pDX, ParcelLoadString(STRID_CREATE_PROJECT_DDV_LOWLIM), m_vLowSpec, 0, 100000);
	cParcelDDX(pDX, IDC_UPPERLIM_SPEC, m_vUpperSpec);
	cParcelDDV(pDX, ParcelLoadString(STRID_CREATE_PROJECT_DDV_UPPERLIM), m_vUpperSpec, 0, 100000);
	cParcelDDX(pDX, IDC_NSCAN_SUBSAMP, m_vNScanSub);
	cParcelDDV(pDX, ParcelLoadString(STRID_CREATE_PROJECT_DDV_SCANSUB), m_vNScanSub, 1, 100000);
	cParcelDDX(pDX, IDC_NMEAS_SUBSAMP, m_vNMeasureSub);
	cParcelDDV(pDX, ParcelLoadString(STRID_CREATE_PROJECT_DDV_MEASSUB), m_vNMeasureSub, 1, 1000);
	cParcelDDX(pDX, IDC_NSCAN_STDSAMP, m_vNScanStd);
	cParcelDDV(pDX, ParcelLoadString(STRID_CREATE_PROJECT_DDV_SCANSTD), m_vNScanStd, 1, 100000);
	cParcelDDX(pDX, IDC_BATH_LENGTH, m_vBathLength);
	cParcelDDV(pDX, ParcelLoadString(STRID_CREATE_PROJECT_DDV_BATHLEN), m_vBathLength, 6., 35.);
	cParcelDDX(pDX, IDC_LOWLIM_TEMP, m_vLowTemp);
	cParcelDDV(pDX, ParcelLoadString(STRID_CREATE_PROJECT_DDV_LOWTEMP), m_vLowTemp, -5, 50);
	cParcelDDX(pDX, IDC_UPPERLIM_TEMP, m_vUpperTemp);
	cParcelDDV(pDX, ParcelLoadString(STRID_CREATE_PROJECT_DDV_UPPERTEMP), m_vUpperTemp, -5, 50);
	cParcelDDX(pDX, IDC_MAXDIFF_TEMP, m_vMaxDiff);
	cParcelDDV(pDX, ParcelLoadString(STRID_CREATE_PROJECT_DDV_MAXDIFF), m_vMaxDiff, 0, 55);

	DDX_Check(pDX, IDC_USE_REFCANAL, m_vUseRef);
}

BEGIN_MESSAGE_MAP(CCreateProjectDlg, CDialog)

	ON_CBN_SELCHANGE(IDC_SUBCANAL_RESOL, OnSubCanalResolChange)

	ON_BN_CLICKED(IDC_ACCEPT, OnAccept)
	ON_BN_CLICKED(IDHELP, OnHelp)
	ON_BN_CLICKED(IDC_COPY, OnCopyFrom)
	ON_BN_CLICKED(IDC_USE_REFCANAL, OnSetEnablingRefCanal)

END_MESSAGE_MAP()


BOOL CCreateProjectDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	int i;
	CString s;

	CString Hdr;
	switch(Mode)
	{
	case PRJDLG_MODE_CREATE:
		Hdr = ParcelLoadString(STRID_CREATE_PROJECT_HDR_CREATE);
		break;
	case PRJDLG_MODE_CHANGE:
		Hdr = ParcelLoadString(STRID_CREATE_PROJECT_HDR_CHANGE);
		break;
	case PRJDLG_MODE_CREATE_SST:
		Hdr = ParcelLoadString(STRID_CREATE_PROJECT_HDR_CREATE_SST);
		break;
	case PRJDLG_MODE_CHANGE_SST:
		Hdr = ParcelLoadString(STRID_CREATE_PROJECT_HDR_CHANGE_SST);
		break;
	}
	SetWindowText(Hdr);

	m_NMeasureStd.ResetContent();
	for(i=C_MEASURE_ONE; i<=C_MEASURE_EVERY; i++)
	{
		s = cGetMeasureTypeName(i);
		m_NMeasureStd.AddString(s);
	}
	m_NMeasureStd.SetCurSel(m_vNMeasureStd);

	m_NResolSubj.ResetContent();
	for(i=0; i<ParentDev->GetNResolutions(); i++)
	{
		s.Format(cFmt42, ParentDev->GetResolutionFromList(i));
		m_NResolSubj.AddString(s);
	}
	m_NResolSubj.SetCurSel(m_vNResolSubj);

	m_ZeroFill.ResetContent();
	for(i=0; i<PrjData->GetNFillings(); i++)
	{
		s.Format(ParcelLoadString(STRID_CREATE_PROJECT_ZF_FMT), PrjData->GetFillingFromList(i));
		m_ZeroFill.AddString(s);
	}
	m_ZeroFill.SetCurSel(m_vZeroFill);

	m_NApodizeSubj.ResetContent();
	for(i=C_APODIZE_RECTAN; i<=C_APODIZE_BESSEL; i++)
	{
		s = cGetApodizeTypeName(i);
		m_NApodizeSubj.AddString(s);
	}
	m_NApodizeSubj.SetCurSel(m_vNApodizeSubj);

	m_NResolRef.ResetContent();
	for(i=0; i<=m_vNResolSubj; i++)
	{
		s.Format(cFmt42, ParentDev->GetResolutionFromList(i));
		m_NResolRef.AddString(s);
	}
	m_NResolRef.SetCurSel(m_vNResolRef);

	m_NApodizeRef.ResetContent();
	for(i=C_APODIZE_RECTAN; i<=C_APODIZE_BESSEL; i++)
	{
		s = cGetApodizeTypeName(i);
		m_NApodizeRef.AddString(s);
	}
	m_NApodizeRef.SetCurSel(m_vNApodizeRef);

	int DType = ParentDev->GetType();
	if((DType != C_DEV_TYPE_40) && (DType != C_DEV_TYPE_12M))
	{
		GetDlgItem(IDC_BATH_LENGTH)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_LOWLIM_TEMP)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_UPPERLIM_TEMP)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_MAXDIFF_TEMP)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STATIC_7)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STATIC_22)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STATIC_13)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STATIC_14)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STATIC_15)->ShowWindow(SW_HIDE);

		if(DType != C_DEV_TYPE_10M && DType != C_DEV_TYPE_20 && DType != C_DEV_TYPE_12)
		{
			GetDlgItem(IDC_USE_REFCANAL)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_REFCANAL_RESOL)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_REFCANAL_APODIZE)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_STATIC_11)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_STATIC_12)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_STATIC_20)->ShowWindow(SW_HIDE);
		}
		else
			OnSetEnablingRefCanal();

		if(DType == C_DEV_TYPE_IMIT)
		{
			GetDlgItem(IDC_STATIC_18)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_STATIC_19)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_SUBCANAL_RESOL)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_SUBCANAL_APODIZE)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_ZERO_FILLING)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_STATIC_9)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_STATIC_10)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_STATIC_8)->ShowWindow(SW_HIDE);

			GetDlgItem(IDC_STATIC_21)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_NSCAN_SUBSAMP)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_NMEAS_SUBSAMP)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_NSCAN_STDSAMP)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_NMEAS_STDSAMP)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_STATIC_3)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_STATIC_4)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_STATIC_5)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_STATIC_6)->ShowWindow(SW_HIDE);
		}
	}
	else
		OnSetEnablingRefCanal();

	if(Mode == PRJDLG_MODE_CHANGE || Mode == PRJDLG_MODE_CHANGE_SST)
	{
		int nMtd = ParentDev->GetProject(PrjData->GetName())->GetNMethods();
		if(nMtd > 0)
		{
			GetDlgItem(IDC_STATIC_22)->EnableWindow(false);
			GetDlgItem(IDC_STATIC_13)->EnableWindow(false);
			GetDlgItem(IDC_STATIC_14)->EnableWindow(false);
			GetDlgItem(IDC_STATIC_15)->EnableWindow(false);
			GetDlgItem(IDC_LOWLIM_TEMP)->EnableWindow(false);
			GetDlgItem(IDC_UPPERLIM_TEMP)->EnableWindow(false);
			GetDlgItem(IDC_MAXDIFF_TEMP)->EnableWindow(false);
		}
	}

	GetDlgItem(IDC_ACCEPT)->SetWindowText(ParcelLoadString(STRID_CREATE_PROJECT_ACCEPT));
	GetDlgItem(IDCANCEL)->SetWindowText(ParcelLoadString(STRID_CREATE_PROJECT_DECLINE));
	GetDlgItem(IDHELP)->SetWindowText(ParcelLoadString(STRID_CREATE_PROJECT_HELP));
	GetDlgItem(IDC_COPY)->SetWindowText(ParcelLoadString(STRID_CREATE_PROJECT_COPYFROM));
	GetDlgItem(IDC_STATIC_16)->SetWindowText(ParcelLoadString(STRID_CREATE_PROJECT_PRJNAME));
	GetDlgItem(IDC_STATIC_17)->SetWindowText(ParcelLoadString(STRID_CREATE_PROJECT_SPECRANGE));
	GetDlgItem(IDC_STATIC_1)->SetWindowText(ParcelLoadString(STRID_CREATE_PROJECT_LOWLIM_SPEC));
	GetDlgItem(IDC_STATIC_2)->SetWindowText(ParcelLoadString(STRID_CREATE_PROJECT_UPPERLIM_SPEC));
	GetDlgItem(IDC_STATIC_18)->SetWindowText(ParcelLoadString(STRID_CREATE_PROJECT_RESANDAPO));
	GetDlgItem(IDC_STATIC_19)->SetWindowText(ParcelLoadString(STRID_CREATE_PROJECT_SUBJCANAL));
	GetDlgItem(IDC_STATIC_9)->SetWindowText(ParcelLoadString(STRID_CREATE_PROJECT_SUBJRESOL));
	GetDlgItem(IDC_STATIC_10)->SetWindowText(ParcelLoadString(STRID_CREATE_PROJECT_SUBJAPODIZE));
	GetDlgItem(IDC_STATIC_8)->SetWindowText(ParcelLoadString(STRID_CREATE_PROJECT_ZERO_FILLING));
	GetDlgItem(IDC_STATIC_20)->SetWindowText(ParcelLoadString(STRID_CREATE_PROJECT_REFCANAL));
	GetDlgItem(IDC_USE_REFCANAL)->SetWindowText(ParcelLoadString(STRID_CREATE_PROJECT_USE_REFCANAL));
	GetDlgItem(IDC_STATIC_11)->SetWindowText(ParcelLoadString(STRID_CREATE_PROJECT_REFRESOL));
	GetDlgItem(IDC_STATIC_12)->SetWindowText(ParcelLoadString(STRID_CREATE_PROJECT_REFAPODIZE));
	GetDlgItem(IDC_STATIC_21)->SetWindowText(ParcelLoadString(STRID_CREATE_PROJECT_MEASPLAN));
	GetDlgItem(IDC_STATIC_3)->SetWindowText(ParcelLoadString(STRID_CREATE_PROJECT_NSCAN_SUBSAMP));
	GetDlgItem(IDC_STATIC_4)->SetWindowText(ParcelLoadString(STRID_CREATE_PROJECT_NMEAS_SUBSAMP));
	GetDlgItem(IDC_STATIC_5)->SetWindowText(ParcelLoadString(STRID_CREATE_PROJECT_NSCAN_STDSAMP));
	GetDlgItem(IDC_STATIC_6)->SetWindowText(ParcelLoadString(STRID_CREATE_PROJECT_NMEAS_STDSAMP));
	GetDlgItem(IDC_STATIC_7)->SetWindowText(ParcelLoadString(STRID_CREATE_PROJECT_BATH_LENGTH));
	GetDlgItem(IDC_STATIC_22)->SetWindowText(ParcelLoadString(STRID_CREATE_PROJECT_SAMPLETEMP));
	GetDlgItem(IDC_STATIC_13)->SetWindowText(ParcelLoadString(STRID_CREATE_PROJECT_LOWLIM_TEMP));
	GetDlgItem(IDC_STATIC_14)->SetWindowText(ParcelLoadString(STRID_CREATE_PROJECT_UPPERLIM_TEMP));
	GetDlgItem(IDC_STATIC_15)->SetWindowText(ParcelLoadString(STRID_CREATE_PROJECT_MAXDIFF_TEMP));

	if(Mode != PRJDLG_MODE_CREATE)
		GetDlgItem(IDC_COPY)->ShowWindow(SW_HIDE);

	SetSettings(true);

	GetDlgItem(IDHELP)->EnableWindow(pFrame->IsExistHelp());

	return true;
}

void CCreateProjectDlg::OnAccept()
{
	if(!UpdateData())
		return;

	CString s;

	m_vName.Trim();
	if(m_vName.IsEmpty())
	{
		ParcelError(ParcelLoadString(STRID_CREATE_PROJECT_ERROR_EMPTYNAME));
		return;
	}
	if(!cIsBadSymbolInName(m_vName))
	{
		ParcelError(ParcelLoadString(STRID_CREATE_PROJECT_ERROR_BADSYMBOL));
		return;
	}
	if(Mode == PRJDLG_MODE_CREATE || PrjData->GetName().Compare(m_vName))
	{
		CProjectData *prj = ParentDev->GetProject(m_vName);
		if(prj != NULL)
		{
			s.Format(ParcelLoadString(STRID_CREATE_PROJECT_ERROR_NAME), m_vName);
			ParcelError(s);
			return;
		}
	}
	if((Mode == PRJDLG_MODE_CREATE || Mode == PRJDLG_MODE_CHANGE) && !m_vName.Compare(ParentDev->GetSSTName()))
	{
		s.Format(ParcelLoadString(STRID_CREATE_PROJECT_ERROR_SSTNAME), m_vName);
		ParcelError(s);
		return;
	}
	if(m_vLowSpec > m_vUpperSpec)
	{
		s = ParcelLoadString(STRID_CREATE_PROJECT_ERROR_SPECRANGE);
		ParcelError(s);
		return;
	}
	if(m_vLowTemp > m_vUpperTemp)
	{
		s = ParcelLoadString(STRID_CREATE_PROJECT_ERROR_SAMPLETEMP);
		ParcelError(s);
		return;
	}

	PrjData->SetName(m_vName);

	PrjData->SetLowLimSpecRange(m_vLowSpec);
	PrjData->SetUpperLimSpecRange(m_vUpperSpec);
	PrjData->SetNScansSubsample(m_vNScanSub);
	PrjData->SetNMeasuresSubsample(m_vNMeasureSub);
	PrjData->SetNScansStandart(m_vNScanStd);
	PrjData->SetNMeasuresStandart(m_vNMeasureStd);
	PrjData->SetZeroFillingInd(m_vZeroFill);

	if(ParentDev->GetType() == C_DEV_TYPE_40 || ParentDev->GetType() == C_DEV_TYPE_12M)
	{
		PrjData->SetBathLength(m_vBathLength);
	}

	PrjData->SetSubCanalResolution(m_vNResolSubj);
	PrjData->SetSubCanalApodization(m_vNApodizeSubj);
	if(m_vUseRef)
	{
		PrjData->SetUseRefCanal(true);
		PrjData->SetRefCanalResolution(m_vNResolRef);
		PrjData->SetRefCanalApodization(m_vNApodizeRef);
	}
	else  PrjData->SetUseRefCanal(false);

	PrjData->SetLowLimTemp(m_vLowTemp);
	PrjData->SetUpperLimTemp(m_vUpperTemp);
	PrjData->SetMaxDiffTemp(m_vMaxDiff);

	PrjData->CalcSpecPoints();

	CDialog::OnOK();
}

void CCreateProjectDlg::OnCancel()
{
	if((Mode == PRJDLG_MODE_CHANGE || Mode == PRJDLG_MODE_CHANGE_SST) && IsChanged())
	{
		if(!ParcelConfirm(ParcelLoadString(STRID_CREATE_PROJECT_ASK_DOCANCEL), false))
			return;
	}

	CDialog::OnCancel();
}

void CCreateProjectDlg::OnCopyFrom()
{
	m_Name.GetWindowText(m_vName);

	if(pRoot->GetNProjects() <= 0)
	{
		ParcelError(ParcelLoadString(STRID_CREATE_PROJECT_ERROR_COPY));
		return;
	}

	CCopyProjectDlg dlg(PrjData, this);
	if(dlg.DoModal() != IDOK)
		return;

	PrjData->Copy(*PrjData->pCopies);
	PrjData->SetParentHItem(ParentDev->GetHItem());

	SetSettings();
}

void CCreateProjectDlg::OnSubCanalResolChange()
{
	if(!UpdateData())
		return;

	int i;
	CString s;

	PrjData->SetZeroFillingInd(m_vZeroFill);
	PrjData->SetRefCanalResolution(m_vNResolRef);
	PrjData->SetSubCanalResolution(m_vNResolSubj);

	if(PrjData->IsRefCanalEnable() && m_vNResolRef != PrjData->GetRefCanalResolution())
	{
		if(m_vUseRef)
		{
			s = ParcelLoadString(STRID_CREATE_PROJECT_WARNING_REFCHANGE);
			ParcelInformation(s);
		}
		m_vNResolRef = PrjData->GetRefCanalResolution();
	}

	if(m_vZeroFill != PrjData->GetZeroFillingInd())
	{
		s = ParcelLoadString(STRID_CREATE_PROJECT_WARNING_ZEROCHANGE);
		ParcelInformation(s);
		m_vZeroFill = PrjData->GetZeroFillingInd();
	}

	m_NResolRef.ResetContent();
	for(i=0; i<=m_vNResolSubj; i++)
	{
		s.Format(cFmt42, ParentDev->GetResolutionFromList(i));
		m_NResolRef.AddString(s);
	}
	m_NResolRef.SetCurSel(m_vNResolRef);

	m_ZeroFill.ResetContent();
	for(i=0; i<PrjData->GetNFillings(); i++)
	{
		s.Format(ParcelLoadString(STRID_CREATE_PROJECT_ZF_FMT), PrjData->GetFillingFromList(i));
		m_ZeroFill.AddString(s);
	}
	m_ZeroFill.SetCurSel(m_vZeroFill);

	UpdateData(0);
}

void CCreateProjectDlg::OnSetEnablingRefCanal()
{
	GetDlgItem(IDC_STATIC_11)->EnableWindow(m_UseRef.GetCheck());
	GetDlgItem(IDC_STATIC_12)->EnableWindow(m_UseRef.GetCheck());
	GetDlgItem(IDC_REFCANAL_RESOL)->EnableWindow(m_UseRef.GetCheck());
	GetDlgItem(IDC_REFCANAL_APODIZE)->EnableWindow(m_UseRef.GetCheck());
}

void CCreateProjectDlg::SetSettings(bool upd)
{
	m_vLowSpec = PrjData->GetLowLimSpecRange();
	m_vUpperSpec = PrjData->GetUpperLimSpecRange();
	m_vNScanSub = PrjData->GetNScansSubsample();
	m_vNMeasureSub = PrjData->GetNMeasuresSubsample();
	m_vNScanStd = PrjData->GetNScansStandart();
	m_vNMeasureStd = PrjData->GetNMeasuresStandart();
	m_vBathLength = PrjData->GetBathLength();
	m_vZeroFill = PrjData->GetZeroFillingInd();
	m_vNResolSubj = PrjData->GetSubCanalResolution();
	m_vNApodizeSubj = PrjData->GetSubCanalApodization();
	m_vUseRef = PrjData->IsUseRefCanal();
	m_vNResolRef = PrjData->GetRefCanalResolution();
	m_vNApodizeRef = PrjData->GetRefCanalApodization();
	m_vLowTemp = PrjData->GetLowLimTemp();
	m_vUpperTemp = PrjData->GetUpperLimTemp();
	m_vMaxDiff = PrjData->GetMaxDiffTemp();

	if(upd)
	{
		bool ch = (PrjData->CopyMode == C_PRJ_COPY_WITHSPECTRA || PrjData->CopyMode == C_PRJ_COPY_WITHTRANS ||
			((Mode == PRJDLG_MODE_CHANGE || Mode == PRJDLG_MODE_CHANGE_SST) && (PrjData->GetNTransfers() > 1 || PrjData->GetNAllSpectrum() > 0)));

		bool ch2 = !(PrjData->GetNParams() > 0 || PrjData->GetNTransfers() > 0 || PrjData->GetNSamples() > 0 ||
			(Mode == PRJDLG_MODE_CREATE_SST || Mode == PRJDLG_MODE_CHANGE_SST));

		GetDlgItem(IDC_INPUT_NAME)->EnableWindow(ch2);
		GetDlgItem(IDC_STATIC_16)->EnableWindow(ch2);
		GetDlgItem(IDC_STATIC_1)->EnableWindow(!ch);
		GetDlgItem(IDC_STATIC_2)->EnableWindow(!ch);
		GetDlgItem(IDC_STATIC_9)->EnableWindow(!ch);
		GetDlgItem(IDC_STATIC_10)->EnableWindow(!ch);
		GetDlgItem(IDC_STATIC_8)->EnableWindow(!ch);
		GetDlgItem(IDC_STATIC_17)->EnableWindow(!ch);
		GetDlgItem(IDC_STATIC_19)->EnableWindow(!ch);
		GetDlgItem(IDC_LOWLIM_SPEC)->EnableWindow(!ch);
		GetDlgItem(IDC_UPPERLIM_SPEC)->EnableWindow(!ch);
		GetDlgItem(IDC_SUBCANAL_RESOL)->EnableWindow(!ch);
		GetDlgItem(IDC_SUBCANAL_APODIZE)->EnableWindow(!ch);
		GetDlgItem(IDC_ZERO_FILLING)->EnableWindow(!ch);

		UpdateData(0);
	}
}

bool CCreateProjectDlg::IsChanged()
{
	return true;
}

void CCreateProjectDlg::OnHelp()
{
	int IdHelp = DLGID_MAIN;
	switch(Mode)
	{
	case PRJDLG_MODE_CREATE: IdHelp = DLGID_PROJECT_CREATE;  break;
	case PRJDLG_MODE_CHANGE: IdHelp = DLGID_PROJECT_CHANGE;  break;
	case PRJDLG_MODE_CREATE_SST:
	case PRJDLG_MODE_CHANGE_SST:
		IdHelp = DLGID_PROJECT_SST_CREATE;  break;
	}

	pFrame->ShowHelp(IdHelp);
}
