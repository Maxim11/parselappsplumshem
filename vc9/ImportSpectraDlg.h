#ifndef _IMPORT_SPECTRA_DLG_H_
#define _IMPORT_SPECTRA_DLG_H_

#pragma once

#include "DeviceData.h"
#include "ParcelListCtrl.h"
#include "Resource.h"

///////////////////////////////////////////////////////////////////////////////////////////////////
// ������ �������������� ������ ��������

class CImportSpectraDlg : public CDialog
{
	DECLARE_DYNAMIC(CImportSpectraDlg)

public:

	CImportSpectraDlg(CDeviceData* dev, apjdb::PRApjProject* apjx, CProjectData* pprj, CWnd* pParent = NULL);	// �����������
	virtual ~CImportSpectraDlg();																				// ����������

	enum { IDD = IDD_IMPORT_SPECTRA };			// ������������� �������

	CParcelListCtrl m_listSam;		// ������ ��������
	CParcelListCtrl m_listRef;		// ������ ����������� ������
	CEdit m_Name;					// ���� ��� �������������� "����� �������"
	CComboBox m_Prj;				// ������ ��� ������ "�������"
	CButton m_Diff;					// ��������� �������� "�������� ��� �������� � ���������� � ����������� ������"
	CButton	m_NewPrj;				// ��������� "��������� ����� ������"

protected:

	CDeviceData* Device;
	apjdb::PRApjProject* Apjx;
	CProjectData* pPrj;

	CString m_vName;				// ��������� �������� "����� �������"
	int m_vPrj;						// ������ ���������� "�������"
	BOOL m_vDiff;					// ������������� �������� "�������� ��� �������� � ���������� � ����������� ������"
	BOOL m_vNewPrj;					// ������������� �������� "��������� ����� ������"

	bool IsSST;

	VStr PrjNames;
	VStr ParNames;
	VImpSam Samples;

	int NSamples;

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();						// ������������� �������

	virtual void OnOK()
	{};
	virtual void OnCancel();				// ��������� ������� "�������� ���� � ����� �� �������"

	afx_msg void OnAccept();				// ��������� ������� "������� ������ � ����� �� �������"
	afx_msg void OnAdd();					// ��������� ������� "��������� ������ �������� ���������� ��������"
	afx_msg void OnAddAll();				// ��������� ������� "��������� ������ �������� ���� ��������"
	afx_msg void OnRemove();				// ��������� ������� "��������� ������ �������� ���������� ��������"
	afx_msg void OnRemoveAll();				// ��������� ������� "��������� ������ �������� ���� ��������"
	afx_msg	void OnSetNewPrj();				// ��������� ������� "���. ��. �������� �������� ������ �������"
	afx_msg void OnProjectListChange();
	afx_msg void OnMakeDiffChange();
	afx_msg void OnSamplesListSelChange(NMHDR* /*pNMHDR*/, LRESULT *pResult);
	afx_msg void OnHelp();					// ��������� ������� "������� �������"

	void FillListSam(int sel);				// ��������� ������ ��������
	void FillListRef();						// ��������� ������ ���. ������

	void EnableFields();
	void EnableButtonsAll();

	void CreateListPrj();
	void CreateSamples();
	PRApjParam* GetFileParam(CString name);
	int GetNImporting();
	int GetNNewSpectra();

	bool IsDiffExist();

public:

	bool IsSampleAval(int ind);
	bool IsSampleDiff(int ind);
	bool IsCurSampleRefDiff(int iref);

protected:

	DECLARE_MESSAGE_MAP()
};

#endif
