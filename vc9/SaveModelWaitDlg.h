#ifndef _SAVE_MODEL_WAIT_DLG_H_
#define _SAVE_MODEL_WAIT_DLG_H_

#pragma once

#include "WaitingDlg.h"
#include "DeviceData.h"


///////////////////////////////////////////////////////////////////////////////////////////////////
// ������ ��������� �������� ���������� ����������������� �������

class CSaveModelWaitDlg : public CWaitingDlg
{
	DECLARE_DYNAMIC(CSaveModelWaitDlg)

public:

	CSaveModelWaitDlg(CProjectData* prj, CModelData* mod, CModelData* old, CWnd* pParent = NULL);	// �����������
	virtual ~CSaveModelWaitDlg();									// ����������

	enum { IDD = CWaitingDlg::IDD };	// ������������� �������

protected:

	CProjectData* PrjData;
	CModelData* ModData;
	CModelData* OldMod;

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();						// ������������� �������

	virtual void OK();				// ��������� �������� ����� �� �������
	virtual void Stop();			// ��������� ����������� ����� �� �������
	virtual void Work();			// ������ �������� �������

	DECLARE_MESSAGE_MAP()
};

#endif
