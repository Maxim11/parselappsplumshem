#ifndef _PARCEL_PROJECT_DATA_H_
#define _PARCEL_PROJECT_DATA_H_

#pragma once

#include "ParamData.h"
#include "TransferData.h"
#include "ModelData.h"
#include "MethodData.h"
#include "PRDBProject.h"
#include "PRDBSpectra.h"


typedef list<CParamData> LPar;
typedef list<CParamData>::iterator ItPar;
typedef list<CTransferData> LTrans;
typedef list<CTransferData>::iterator ItTrans;
typedef list<CModelData> LMod;
typedef list<CModelData>::iterator ItMod;
typedef list<CMethodData> LMtd;
typedef list<CMethodData>::iterator ItMtd;

// ��������� ���� "���������" ��������

const int C_KEEP_FREE		= 0;	// ������ �������� ��� �������� � ��������������
const int C_KEEP_NODELETE	= 1;	// ������ ������ �� ��������
const int C_KEEP_NOEDIT		= 2;	// ������ ������ �� �������� � ��������������
const int C_KEEP_NOTFOUND	= 3;	// ������ �� ������

///////////////////////////////////////////////////////////////////////////////////////////////////
// ����� ��� �������� ������� �������

class CProjectData
{
public:

	CProjectData(HTREEITEM hParent, bool sst = false);	// �����������
	~CProjectData();									// ����������

	void Copy(CProjectData& data);					// ����������� ������
	void SortSamples(CString name, bool dir_up);	// ���������� ������� ��������
	bool SortSamples(int ntype, bool bdir, int paridx);		// ���������� ������� �������� �� �������
	void SortAllSamples();							// ���������� ������� �������� ���� ������� �� ������� ���������
	void SortSamples(CString transdata = cOwn);		// ���������� ������� �������� ������ �� ������� ���������

protected:

	HTREEITEM hItem;			// ������ ���� � ������, ���������������� �������

	HTREEITEM hParentItem;		// ������ ���� � ������, ���������������� ������������� �������

	HTREEITEM hParamSetItem;	// ������ ���� � ������, ���������������� ������ ����������� ��� ������� �������
	HTREEITEM hSampleSetItem;	// ������ ���� � ������, ���������������� ������� ������ �������� ��� ������� �������
	HTREEITEM hModelSetItem;	// ������ ���� � ������, ���������������� ������ ������� ��� ������� �������
	HTREEITEM hMethodSetItem;	// ������ ���� � ������, ���������������� ������ ������� ��� ������� �������

	int DevNumber;				// ����� ������������� �������

	CString Name;				// ��� �������

	bool SST;					// ������� ������������ �������

	CTransferData* TransOwn;

	int LowLimSpecRange;		// ������ ������� ������������� ���������
	int UpperLimSpecRange;		// ������� ������� ������������� ���������
	int NScansSubsample;		// ����� ������ ����������
	int NMeasuresSubsample;		// ����� ��������� ����������
	int NScansStandart;			// ����� ������ �������
	double BathLength;			// ����� ������
	bool UseRefCanal;			// ������� ������������� �������� ������
	int LowLimTemp;				// ������ ������ ����������� �������
	int UpperLimTemp;			// ������� ������ ����������� �������
	int MaxDiffTemp;			// ���������� ������� ����������� ������� �� ����������� ���������� �����

	int NMeasuresStandart;		// ����� ��������� ������� (������)
	int SubCanalResolution;		// ���������� ����������� ������ (������)
	int SubCanalApodization;	// ���������� ����������� ������ (������)
	int ZeroFilling;			// ���������� ������ (������)
	int RefCanalResolution;		// ���������� �������� ������ (������)
	int RefCanalApodization;	// ���������� �������� ������ (������)

	CString SamSortName;		// ��� ���������� ��� ���������� ��������. ���� ����� -- ���������� �� ����� �������
	bool SamSortDir;			// ����������� ���������� ��������.

	VDbl SpecPoints;			// ������ ������������ ����� (���)

	VInt FillingList;			// ������ ��������� ���������� ������

public:

	LPar Params;				// ������ ����������� ��� ������� �������
	LTrans Transfers;			// ������ ������� �������� ��� ������� �������
	LMod Models;				// ������ ������������� ������� ��� ������� �������
	LMtd Methods;				// ������ ������� ���������� ��� ������� �������

	int CopyMode;				// ����� �����������
	CProjectData* pCopies;		// ��������� �� ���������� ������
	CTreeCtrl* pTree;

public:

	HTREEITEM GetHItem();					// �������� ������ ���� � ������, ��������������� �������
	void SetHItem(HTREEITEM item);			// ���������� ������ � ������, ��������������� �������

	HTREEITEM GetParentHItem();				// �������� ������ ���� � ������, ��������������� �������
	void SetParentHItem(HTREEITEM item);	// ���������� ������ ���� � ������, ��������������� �������

	int GetDevNumber();						// �������� ����� ������������� �������

	HTREEITEM GetParamSetHItem();			// �������� ������ ���� � ������, ��������������� ������ �����������
	void SetParamSetHItem(HTREEITEM item);	// ���������� ������ ���� � ������, ��������������� ������ �����������

	HTREEITEM GetSampleSetHItem();			// �������� ������ ���� � ������, ��������������� ������ ��������
	void SetSampleSetHItem(HTREEITEM item);	// ���������� ������ ���� � ������, ��������������� ������ ��������

	HTREEITEM GetSampleTransferHItem(CString name);				// �������� ������ ���� � ������, ��������������� ������ ��������
	bool SetSampleTransferHItem(CString name, HTREEITEM item);	// ���������� ������ ���� � ������, ��������������� ������ ��������

	HTREEITEM GetModelSetHItem();			// �������� ������ ���� � ������, ��������������� ������ �������
	void SetModelSetHItem(HTREEITEM item);	// ���������� ������ ���� � ������, ��������������� ������ �������

	HTREEITEM GetMethodSetHItem();			// �������� ������ ���� � ������, ��������������� ������ �������
	void SetMethodSetHItem(HTREEITEM item);	// ���������� ������ ���� � ������, ��������������� ������ �������

	CString GetName();						// �������� ��� �������
	void SetName(CString name);				// ���������� ��� �������

	bool IsSST();							// �������� ������� ������������ �������
	void SetSST(bool sst);					// ���������� ������� ������������ �������

	CTransferData* GetTransSST();
	HTREEITEM GetTransSSTHItem();

	CTransferData* GetTransOwn();
	HTREEITEM GetTransOwnHItem();

	int GetLowLimSpecRange();			// �������� ������ ������� ������������� ���������
	void SetLowLimSpecRange(int lim);	// ���������� ������ ������� ������������� ���������

	int GetUpperLimSpecRange();			// �������� ������� ������� ������������� ���������
	void SetUpperLimSpecRange(int lim);	// ���������� ������� ������� ������������� ���������

	int GetNScansSubsample();			// �������� ����� ������ ����������
	void SetNScansSubsample(int num);	// ���������� ����� ������ ����������

	int GetNMeasuresSubsample();		// �������� ����� ��������� ����������
	void SetNMeasuresSubsample(int num);// ���������� ����� ��������� ����������

	int GetNScansStandart();			// �������� ����� ������ �������
	void SetNScansStandart(int scans);	// ���������� ����� ������ �������

	double GetBathLength();				// �������� ����� ������
	void SetBathLength(double len);		// ���������� ����� ������

	bool IsUseRefCanal();				// �������� ������� ������������� �������� ������
	CString GetUseRefCanalStr();		// �������� �����: ������������� �������� ������
	void SetUseRefCanal(bool use);		// ���������� ������� ������������� �������� ������

	int GetLowLimTemp();				// �������� ������ ������ ����������� �������
	void SetLowLimTemp(int lim);		// ���������� ������ ������ ����������� �������

	int GetUpperLimTemp();				// �������� ������� ������ ����������� �������
	void SetUpperLimTemp(int lim);		// ���������� ������� ������ ����������� �������

	int GetMaxDiffTemp();				// �������� ���������� ������� ����. ������� �� ����. ���������� �����
	void SetMaxDiffTemp(int diff);		// ���������� ���������� ������� ����. ������� �� ����. ���������� �����

	int GetNMeasuresStandart();				// �������� ����� ��������� ������� ��� ������ � ��� (������ ��������� ��������)
	bool SetNMeasuresStandart(int ind);		// ���������� ����� ��������� ������� �� ������� � ���

	int GetSubCanalResolution();			// �������� ���������� ����������� ������ ��� ������ � ���
	double GetSubCanalResolutionMean();		// �������� ���������� ����������� ������
	bool SetSubCanalResolution(int ind);	// ���������� ���������� ����������� ������ �� ������� � ���

	int GetSubCanalApodization();			// �������� ���������� ����������� ������ ��� ������ � ���
	bool SetSubCanalApodization(int ind);	// ���������� ���������� ����������� ������ �� ������� � ���

	int GetZeroFillingInd();				// �������� ���������� ������ ��� ������ � ���
	int GetZeroFillingMean();				// �������� ���������� ������
	CString GetZeroFillingAsStr();			// �������� ���������� ������� � ���� ������
	bool SetZeroFillingInd(int ind);		// ���������� ���������� ������ �� ������� � ���
	bool SetZeroFillingMean(int val);		// ���������� ���������� ������ �� ��������

	bool IsRefCanalEnable();

	int GetRefCanalResolution();			// �������� ���������� �������� ������ ��� ������ � ���
	double GetRefCanalResolutionMean();		// �������� ���������� �������� ������
	bool SetRefCanalResolution(int ind);	// ���������� ���������� �������� ������ �� ������� � ���

	int GetRefCanalApodization();			// �������� ���������� �������� ������ ��� ������ � ���
	bool SetRefCanalApodization(int ind);	// ���������� ���������� �������� ������ �� ������� � ���

	CString GetSamSortName();
	void SetSamSortName(CString name);
	bool GetSamSortDir();
	void SetSamSortDir(bool flag);

	int GetNSpecPoints();						// �������� ����� ������������ �����
	double GetSpecPoint(int indSP);				// �������� ������������ �����
	int GetSpecPointInd(double value);			// �������� ������������ ����� ��� ������ � ������

	int GetNParams();							// �������� ����� ����������� ��� ������� �������
	CParamData* GetParam(CString name);			// �������� ���������� �� �����
	CParamData* GetParam(int ind);				// �������� ���������� �� ����������� ������ � ������
	CParamData* GetLastParam();					// �������� ����������, ��������� � ������
	bool AddParam(CParamData* NewPar);			// �������� ���������� � ������
	bool ChangeParam(CParamData* OldPar, CParamData* NewPar);			// �������� ����������
	bool EditParamList();						// ������������� ������ �����������
	bool DeleteParam(int ind);					// ������� ���������� �� ������� � ������
	bool DeleteParam(CString name);				// ������� ���������� �� �����
	int GetParamKeepStatus(int ind);			// �������� "���������" ��������� �� ����������� ������ � ������
	int GetParamKeepStatus(CString name);		// �������� "���������" ��������� �� �����
	bool IsParamMethodExist(int ind);

	int GetNTransfers();						// �������� ����� ������� ����������� ��������
	CTransferData* GetTransfer(HTREEITEM item);	// �������� ����� ������������ �������� �� ������� ���� � ������
	CTransferData* GetTransfer(int ind);		// �������� ����� ������������ �������� �� ����������� ������
	CTransferData* GetTransfer(CString name);	// �������� ����� ������������ �������� �� �����
	CTransferData* GetLastTransfer();			// �������� �����, ��������� � ������
	HTREEITEM GetLastTransferHItem();			// �������� ������ ���� � ������ ��� ������, ���������� � ������
	bool AddOwnTrans();
	bool ChangeOwnTransfer();
	bool AddTransfer(CTransferData* NewTrans);	// �������� ����� � ������

	CTransferData* AddNewTransfer();			// ������� ����� ����� ������������ ��������
	bool ProjectTransfer();						// ������� ��� ���������

	CTransferData* AddSSTTransfer(CTreeCtrl* curTree);
	bool CreateSST(VPlate *pPlates);
	bool ChangeTransfer(HTREEITEM item);		// ������������� ����� ������������ ��������
	bool DeleteTransfer(HTREEITEM item);		// ������� ����� ������������ ��������
	int GetTransferKeepStatus(HTREEITEM item);

	int GetNSamples();							// �������� ����� �������� � ������ ������
	int GetNAllSamples();						// �������� ����� ����� �������� (�� ���� �������)
	int GetNSpectra();							// �������� ����� �������� � ������ ������
	int GetNAllSpectrum();						// �������� ����� ����� �������� (�� ���� �������)
	int GetNSpectraUsed();						// �������� ����� ������������ �������� � ������ ������
	int GetNAllUsesSpectrum();					// �������� ����� ����� ������������ �������� (�� ���� �������)
	CSampleData* GetSample(CString name);		// �������� ������� �� ����� � ������ ������
	CSampleData* GetSample(int ind);			// �������� ������� �� ����������� ������ � ������ � ������ ������
	bool AddSample(CSampleData* NewSam);		// �������� ������� � ������ �����



	bool ChangeSampleNoSpectra(CSampleData* OldSam, CSampleData* NewSam, CTransferData* pTrans);
	bool ChangeSample(CSampleData* OldSam, CSampleData* NewSam);	// �������� ������� ������� ������
	bool EditSampleList(HTREEITEM item);		// ������������� ����� ��������
	bool DeleteSample(int ind);					// ������� ������� �� ������� ������ �� ������� � ������
	bool DeleteSample(CString name);			// ������� ������� �� ������� ������ �� �����
	int GetSampleKeepStatus(int ind);			// �������� "���������" ������� �� ������� ������ �� ����������� ������ � ������
	int GetSampleKeepStatus(CString name);		// �������� "���������" ������� �� ������� ������ �� �����
	int GetSpectrumKeepStatus(CString samname, int ind);	// �������� "���������" ������� �� ����������� ������

	CSampleData* GetTransSample(CString transName, CString name);		// �������� ������� �� �����
	int GetTransSampleKeepStatus(CString transName, CString name);		// �������� "���������" ������� �� �����
	bool IsSampleRefKeep(CString namesam, CString nametrans, CString nameref);	// �������� "���������" ������������ ������� �� �����


	int GetNModels();							// �������� ����� ������� ��� ������� �������
	int GetNQntModels();						// �������� ����� �������������� ������� ��� ������� �������
	int GetNQltModels();						// �������� ����� ������������ ������� ��� ������� �������
	CModelData* GetModel(HTREEITEM item);		// �������� ������ �� ������� ���� � ������
	CModelData* GetModel(CString name);			// �������� ������ �� �����
	CModelData* GetModel(int ind);				// �������� ������ �� ����������� ������ � ������
	CModelData* GetQltModel(int ind);
	CModelData* GetLastModel();					// �������� ������, ��������� � ������
	HTREEITEM GetLastModelHItem();				// �������� ������ ���� � ������ ��� ������, ��������� � ������
	CModelData* AddNewModel();					// ������� ����� ������
	CModelData* AddExistingModel(CModelData &Mod, CModelData* OldMod = NULL);
	CModelData* AddCopyModel(HTREEITEM item);	// ������� ����� ������
	CModelData* AddTransModel(CModelData* pMod, CString);	// ������� ����� ������
	bool ChangeModelSettings(HTREEITEM item);	// �������� ��������� ������
	bool DeleteModel(HTREEITEM item);			// ������� ������
	bool ModelResult(HTREEITEM item);			// �������� ���������� ���������� ������
	int GetModelKeepStatus(HTREEITEM item);		// �������� "���������" ������

	int GetNMethods();							// �������� ����� ������� ��� ������� �������
	CMethodData* GetMethod(HTREEITEM item);		// �������� ����� �� ������� ���� � ������
	CMethodData* GetMethod(CString name);		// �������� ����� �� �����
	CMethodData* GetMethod(int ind);			// �������� ����� �� ����������� ������ � ������
	CMethodData* GetLastMethod();				// �������� �����, ��������� � ������
	HTREEITEM GetLastMethodHItem();				// �������� ������ ���� � ������ ��� ������, ���������� � ������
	CMethodData* AddNewMethod();				// ������� ����� �����
	CMethodData* AddNewMethod(CMethodData *pNewMtd);	// 
	CMethodData* AddExistingMethod(CMethodData &Mtd);
	CMethodData* AddCopyMethod(HTREEITEM item);	// ������� ����� ������
	bool ChangeMethodSettings(HTREEITEM item);	// �������� ��������� ������
	bool DeleteMethod(HTREEITEM item);			// ������� �����
	bool ExportMethod(HTREEITEM item, CString fname);	// �������������� �����
	bool CProjectData::SaveMethodToQntExportBase(CString fname, mtddb::PRMethodQnt* Obj);
	bool CProjectData::SaveMethodToQltExportBase(CString fname, mtddb::PRMethodQlt* Obj);

	bool ExportSettings(CString fname);			// �������������� ���������

	int GetNFillings();							// �������� ����� ��������� �������� ���������� ������
	int GetFillingFromList(int ind);			// �������� ��������� �������� ���������� ������

	void CalcSpecPoints();						// ���������� ������ ������������ �����
	void ExtractSpecPoints(int low, int upper, VDbl& points);

	void FillZeroFillingList();					// ��������� ������ ��������� �������� ���������� ������

	bool IsReadyForTrans(CProjectData *SPrj);	// ����� �� �������������� ��� �������� ��� ������ ������-�������
	bool IsExistMaster();						// ���������� �� ������, ������� ����� ��������

	void GetDataForBase(prdb::PRAnalisisProject& Obj);
	void SetDataFromBase(prdb::PRAnalisisProject& Obj);
	bool LoadDatabase(CTreeCtrl* pTree);

	bool GetDataForExportSettings(apjdb::PRApjProject& Obj);

};

#endif
