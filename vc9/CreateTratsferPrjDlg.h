#pragma once
#include "afxwin.h"
#include "afxcmn.h"


// CCreateTratsferPrjDlg dialog

class CCreateTratsferPrjDlg : public CDialog
{
	DECLARE_DYNAMIC(CCreateTratsferPrjDlg)

	void InitControls();
	int FillComboDevice();
	int FillComboProject();

	int FillListModAll();
	int FillListModSel();

	int FillListMetAll(bool bClearSel);
	int FillListMetSel();

	void UpdateMethods();
	void OnModAdd();
	void OnModDel();

	bool ValidateSelectedModels();
	bool ValidateSelectedMethods();

	bool TransferSamples();
	bool TransferSamplesV2();

	bool TransferModels();
	CModelData* TransferSingleModel(CModelData* pModel);

	bool TransferMethods();
	CMethodData* TransferSingleMethod(CMethodData* pMethod);

public:
	CCreateTratsferPrjDlg(HTREEITEM item, CWnd* pParent = NULL);   // standard constructor
	virtual ~CCreateTratsferPrjDlg();


// Dialog Data
	enum { IDD = IDD_CREATE_TRANSFER_PRJ };

	CProjectData* pPrjSlave;		// ��������� �� slave-������ �������
	CString strDevSlave;

	CDeviceData* pDevM;				// ��������� �� ������-������
	CProjectData* pPrjM;			// ��������� �� ������ ������-�������
	CTransferData* pTransNew;		// ��������� �� ������������ ����� ��������

	int iDevM;						// ������ ���������� ���������� �������
	int iPrjM;						// ������ ���������� ���������� �������

	VStr vDevNames;					// ������ ���� �������� ��� �������� ��������
	VStr vPrjNames;					// ������ ���� �������� ��� �������� ��������
	VStr vModNamesAll;				// ������ ���� �������� ��� �������� �������
	VStr vModNamesSel;				// ������ ���� ��������� ��� �������� �������

	VStr vMetNamesAll;				// ������ ���� �������� ��� �������� �������
	VStr vMetNamesSel;				// ������ ���� ��������� ��� �������� �������

	VModSam SamNames;				// ������ ��������� ��������
	VStr MSamNames;					// ������ ��������, ��������� ��� ��������
	VStr MParNames;					// ������ ����������� ��� ��������



protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:

	CStatic m_promptDevice;
	CStatic m_promptProject;
	CComboBox m_comboDevice;
	CComboBox m_comboProject;

	CStatic m_promptModAll;
	CListCtrl m_listModAll;
	CStatic m_promptModSel;
	CListCtrl m_listModSel;
	CButton m_butModAdd;
	CButton m_butModAddAll;
	CButton m_butModDel;
	CButton m_butModDelAll;

	CStatic m_promptMetAll;
	CListCtrl m_listMetAll;
	CStatic m_promptMetSel;
	CListCtrl m_listMetSel;
	CButton m_butMetAdd;
	CButton m_butMetAddAll;
	CButton m_butMetDel;
	CButton m_butMetDelAll;

	CButton m_butCreate;
	CButton m_butHelp;
	CButton m_butClose;

//
	virtual BOOL OnInitDialog();
//
	afx_msg void OnCbnSelchangeComboDevice();
	afx_msg void OnCbnSelchangeComboProject();
//
	afx_msg void OnBnClickedButtonModAdd();
	afx_msg void OnBnClickedButtonModAddAll();
	afx_msg void OnBnClickedButtonModDel();
	afx_msg void OnBnClickedButtonModDelAll();
	afx_msg void OnLvnItemchangedListModelsAll(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnLvnItemchangedListModelsSel(NMHDR *pNMHDR, LRESULT *pResult);
//
	afx_msg void OnBnClickedButtonMetAdd();
	afx_msg void OnBnClickedButtonMetAddAll();
	afx_msg void OnBnClickedButtonMetDel();
	afx_msg void OnBnClickedButtonMetDelAll();
	afx_msg void OnLvnItemchangedListMethodsAll(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnLvnItemchangedListMethodsSel(NMHDR *pNMHDR, LRESULT *pResult);
//
	afx_msg void OnBnClickedButtonCreate();
	afx_msg void OnBnClickedHelp();
protected:
	virtual void OnOK();
	virtual void OnCancel();
};
