#include "stdafx.h"

#include "RootData.h"
#include "BranchInfo.h"
#include "CopyProjectDlg.h"
#include "MainFrm.h"


IMPLEMENT_DYNAMIC(CCopyProjectDlg, CDialog)

CCopyProjectDlg::CCopyProjectDlg(CProjectData* data, CWnd* pParent) : CDialog(CCopyProjectDlg::IDD, pParent)
{
	PrjData = data;
	ParentDev = GetDeviceByHItem(PrjData->GetParentHItem());

	m_vParDev = 0;
	m_vParPrj = 0;
	m_vCopySet = C_PRJ_COPY_ONLYSETTINGS;
}

CCopyProjectDlg::~CCopyProjectDlg()
{
}

void CCopyProjectDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_DEVICE_LIST, m_ParDev);
	DDX_Control(pDX, IDC_PROJECT_LIST, m_ParPrj);
	DDX_Control(pDX, IDC_COPYSET_LIST, m_CopySet);

	DDX_CBIndex(pDX, IDC_DEVICE_LIST, m_vParDev);
	DDX_CBIndex(pDX, IDC_PROJECT_LIST, m_vParPrj);
	DDX_CBIndex(pDX, IDC_COPYSET_LIST, m_vCopySet);
}

BEGIN_MESSAGE_MAP(CCopyProjectDlg, CDialog)

	ON_CBN_SELCHANGE(IDC_DEVICE_LIST, OnParDevChange)

	ON_BN_CLICKED(IDC_ACCEPT, OnAccept)
	ON_BN_CLICKED(IDHELP, OnHelp)

END_MESSAGE_MAP()


BOOL CCopyProjectDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	int i;
	CString s;

	SetWindowText(ParcelLoadString(STRID_COPY_PROJECT_HDR));

	DevNames.clear();
	ItDev itD;
	for(itD=pRoot->Devices.begin(); itD!=pRoot->Devices.end(); ++itD)
	{
		if(itD->GetNProjects() > 0)
			DevNames.push_back(itD->GetName());
	}

	m_ParDev.ResetContent();
	m_vParDev = 0;
	ItStr itS;
	for(i=0, itS=DevNames.begin(); itS!=DevNames.end(); ++itS, i++)
	{
		m_ParDev.AddString(*itS);
		if(!pRoot->GetDevice(*itS)->GetName().Compare(ParentDev->GetName()))
			m_vParDev = i;
	}
	m_ParDev.SetCurSel(m_vParDev);

	FillListProjects();

	GetDlgItem(IDC_ACCEPT)->SetWindowText(ParcelLoadString(STRID_COPY_PROJECT_ACCEPT));
	GetDlgItem(IDCANCEL)->SetWindowText(ParcelLoadString(STRID_COPY_PROJECT_DECLINE));
	GetDlgItem(IDHELP)->SetWindowText(ParcelLoadString(STRID_COPY_PROJECT_HELP));

	GetDlgItem(IDC_STATIC_23)->SetWindowText(ParcelLoadString(STRID_COPY_PROJECT_COPY));
	GetDlgItem(IDC_STATIC_24)->SetWindowText(ParcelLoadString(STRID_COPY_PROJECT_PARDEV));
	GetDlgItem(IDC_STATIC_25)->SetWindowText(ParcelLoadString(STRID_COPY_PROJECT_PARPRJ));
	GetDlgItem(IDC_STATIC_26)->SetWindowText(ParcelLoadString(STRID_COPY_PROJECT_COPYSET));

	GetDlgItem(IDHELP)->EnableWindow(pFrame->IsExistHelp());

	return true;
}

void CCopyProjectDlg::OnAccept()
{
	if(!UpdateData())
		return;

	CDeviceData *CurDev = pRoot->GetDevice(DevNames[m_vParDev]);
	if(CurDev == NULL)
		return;
	CProjectData *CurPrj = CurDev->GetProject(m_vParPrj);
	if(CurPrj == NULL)
		return;

	PrjData->pCopies = CurPrj;
	PrjData->CopyMode = m_vCopySet;

	CDialog::OnOK();
}

void CCopyProjectDlg::OnCancel()
{
	CDialog::OnCancel();
}

void CCopyProjectDlg::OnHelp()
{
	pFrame->ShowHelp(DLGID_PROJECT_COPY);
}

void CCopyProjectDlg::OnParDevChange()
{
	if(!UpdateData())
		return;

	FillListProjects();
}

void CCopyProjectDlg::FillListProjects()
{
	PrjNames.clear();

	CDeviceData *CurDev = pRoot->GetDevice(DevNames[m_vParDev]);
	int i, N = CurDev->GetNProjects();
	for(i=0; i<N; i++)
		PrjNames.push_back(CurDev->GetProject(i)->GetName());

	bool IsOwnDev = (!pRoot->GetDevice(DevNames[m_vParDev])->GetName().Compare(ParentDev->GetName()));

	m_ParPrj.ResetContent();
	m_vParPrj = 0;
	if(N > 0)
	{
		ItStr itS;
		for(i=0, itS=PrjNames.begin(); itS!=PrjNames.end(); ++itS, i++)
		{
			m_ParPrj.AddString(*itS);
			if(IsOwnDev && !PrjData->GetName().Compare(*itS))
				m_vParPrj = i;
		}
		m_ParPrj.SetCurSel(m_vParPrj);
	}

	m_CopySet.ResetContent();
	int fin = (IsOwnDev) ? C_PRJ_COPY_WITHSPECTRA : C_PRJ_COPY_WITHSAMPLES;
	for(i=C_PRJ_COPY_ONLYSETTINGS; i<=fin; i++)
		m_CopySet.AddString(cGetPrjCopyName(i));
	if(m_vCopySet >= C_PRJ_COPY_WITHSPECTRA && !IsOwnDev)
		m_vCopySet = C_PRJ_COPY_WITHSAMPLES;
	m_CopySet.SetCurSel(m_vCopySet);
}
