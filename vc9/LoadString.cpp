#include "stdafx.h"

#include "Parcel.h"
#include "MessageDlg.h"
#include "UniString\\nStr.h"


CString LoadString(int Id)
{
HINSTANCE hInstancePrev = AfxGetResourceHandle();

AfxSetResourceHandle(theApp.m_hInstance);

CString String;
String.LoadString(Id);

AfxSetResourceHandle(hInstancePrev);

return String;
}

void InitLang()
{
	TCHAR buf[PATH_LENGTH];
	GetCurrentDirectory(PATH_LENGTH, buf);
	CString s(buf);
	s += _T("\\Lang");

	nstr::InitNStrSupport(s.GetBuffer(0));

	SetLanguage(C_LANG_RUSSIAN);
}

void SetLanguage(int indLang)
{
	nstr::Singleton<nstr::NStrManager>::Instance()->SetCurrentLanguage((nstr::ELang)indLang);
}

CString ParcelLoadString(int ID)
{
	CString answer = cEmpty;

	if(ID > 0 && ID < nstr::C_MAX_STR_NUM)
		answer = nstr::Singleton<nstr::NStrManager>::Instance()->GetTextUI((STRID)ID);

	return answer;
}

LPCTSTR GETSTRUI(int ID)
{
	return nstr::Singleton<nstr::NStrManager>::Instance()->GetTextUI((STRID)ID);
}


bool ParcelConfirm(CString Text, BOOL IsYes)
{
//	return ParcelConfirm(Text, cEmpty, IsYes);

	int type = (IsYes) ? C_MESSAGE_CONFIRM_YES : C_MESSAGE_CONFIRM_NO;
	CMessageDlg dlg(type, Text);
	if(dlg.DoModal() == IDOK)
		return true;

	return false;
}
/*
bool ParcelConfirm(CString Text, CString Title, BOOL IsYes)
{
	CString Title2 = (Title.IsEmpty()) ? ParcelLoadString(STRID_MESSAGE_CONFIRM) : Title;

	int Style = MB_YESNO | MB_ICONQUESTION | MB_SYSTEMMODAL;
	Style |= (IsYes) ? MB_DEFBUTTON1 : MB_DEFBUTTON2;

	HWND hWindow = ::GetFocus();
	int Code = ::MessageBox(::GetActiveWindow(), Text, Title2, Style);
	::SetFocus(hWindow);

	return Code == IDYES;
}
*/
void ParcelInformation(CString Text)
{
//	return ParcelInformation(Text, cEmpty);

	CMessageDlg dlg(C_MESSAGE_INFO, Text);
	dlg.DoModal();
}
/*
void ParcelInformation(CString Text, CString Title)
{
	CString Title2 = (Title.IsEmpty()) ? ParcelLoadString(STRID_MESSAGE_INFO) : Title;

	int Style = MB_OK | MB_ICONINFORMATION | MB_SYSTEMMODAL;

	HWND hWindow = ::GetFocus();
	::MessageBox(::GetActiveWindow(), Text, Title2, Style);
	::SetFocus(hWindow);
}
*/
void ParcelError(CString Text)
{
//	return ParcelError(Text, cEmpty);

	CMessageDlg dlg(C_MESSAGE_ERROR, Text);
	dlg.DoModal();
}
/*
void ParcelError(CString Text, CString Title)
{
	CString Title2 = (Title.IsEmpty()) ? ParcelLoadString(STRID_MESSAGE_ERROR) : Title;

	int Style = MB_OK | MB_ICONEXCLAMATION | MB_SYSTEMMODAL;

	HWND hWindow = ::GetFocus();
	::MessageBox(::GetActiveWindow(), Text, Title2, Style);
	::SetFocus(hWindow);
}
*/
void ParcelWait(BOOL IsWait)
{
	static HCURSOR hCursorPrev = 0;
	static int Count = 0;

	if(IsWait)
	{
		Count++;
		if(Count == 1)
			hCursorPrev = ::SetCursor(::LoadCursor(0, IDC_WAIT));
	}
	else
	{
		Count--;
		if(Count == 0)
			hCursorPrev = ::SetCursor(hCursorPrev);
	}
}

void ParcelProcess(BOOL IsWait)
{
	static HCURSOR hCursorPrev = 0;
	static int Count = 0;

	if(IsWait)
	{
		Count++;
		if(Count == 1)
			hCursorPrev = ::SetCursor(::LoadCursor(0, IDC_APPSTARTING));
	}
	else
	{
		Count--;
		if(Count == 0)
			hCursorPrev = ::SetCursor(hCursorPrev);
	}
}
