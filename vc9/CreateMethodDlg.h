#ifndef _CREATE_METHOD_DLG_H_
#define _CREATE_METHOD_DLG_H_

#pragma once

#include "Resource.h"
#include "MethodData.h"
#include "ParcelListCtrl.h"

// ��������� ������ ������ �������

const int MTDDLG_MODE_CREATE = 0;	// ����� �������� ������ ������ ����������
const int MTDDLG_MODE_CHANGE = 1;	// ����� ��������� �������� ������ ����������
const int MTDDLG_MODE_COPY	 = 2;	// ����� ����������� ������ ����������

///////////////////////////////////////////////////////////////////////////////////////////////////
// ������ ��������/�������������� ������ �����������

class CCreateMethodDlg : public CDialog
{
	DECLARE_DYNAMIC(CCreateMethodDlg)

public:

	CCreateMethodDlg(CMethodData* data, int mode = MTDDLG_MODE_CREATE, CWnd* pParent = NULL);	// �����������
	virtual ~CCreateMethodDlg();																// ����������

	enum { IDD = IDD_CREATE_METHOD };	// ������������� �������

	CParcelListCtrl m_listParAll;	// �������, ���������� ������ ���� �������� �����������
	CListBox m_listParSel;			// �������, ���������� ������ ���� �����������, �����. ��������� ��������
	CParcelListCtrl m_listModAll;	// �������, ���������� ������ ���� �������� �������
	CListBox m_listModSel;			// �������, ���������� ������ �������, ��������� ��� ��������������� �������
	CParcelListCtrl m_listModQltAll;	// �������, ���������� ������ ���� �������� ������� ��� ������������� �������
	CListCtrl m_listModQltSel;		// �������, ���������� ������ �������, ��������� ��� ������������� �������
	CEdit m_Name;					// ���� ��� �������������� "����� �������"
	CEdit m_Low;					// ���� ��� �������������� "������� ������� ����������� �������"
	CEdit m_Upper;					// ���� ��� �������������� "�������� ������� ����������� �������"
	CEdit m_Diff;					// ���� ��� �������������� "����������� ������� ����. ������� �� ����. �����"
	CEdit m_StdMois;				// ���� ��� �������������� "����������� ���������"
	CComboBox m_Type;				// ������ ��� ������ "���� �������"
	CComboBox m_Mois;				// ������ ��� ������ "������� ��� ���������"
	CComboBox m_DefMod;				// ������ ��� ������ "������ �� ���������"
	CButton	m_Use;					// ��������� �������� "������������ ����������� ���������"

protected:

	CMethodData *MtdData;			// ��������� �� ������������� �����
	CProjectData *ParentPrj;		// ��������� �� ������, ������������ ��� ������
	CDeviceData *GrandParentDev;	// ��������� �� ������, ������������ ��� ������

	int Mode;						// ����� ������

	bool exist_qnt;
	int NAllParams;					// ����� ����� �������� ����������� ��� �������

	VStr Names;						// ������ ���� �������� ����������� ��� �������
	VStr CurModNames;
	CString m_vName;				// ��������� �������� "����� �������"
	int m_vLow;						// ��������� �������� "������� ������� ����. �������"
	int m_vUpper;					// ��������� �������� "�������� ������� ����. �������"
	int m_vDiff;					// ��������� �������� "����������� ������� ����. ������� �� ����. �����"
	double m_vStdMois;
	int m_vType;					// ������ ���������� "���� �������"
	int m_vMois;					// ������ ��������� "������� ��� ���������"
	int m_vDefMod;					// ������ ��������� "������ �� ���������"
	BOOL m_vUse;					// ������� ������������� ����������� ���������

	ItRul CurRule;					// ��������� �� ������� ������� �������

	int NAllQltModels;
	VStr QltAllNames;
	VStr QltNames;

	QltFormula Formula;

	bool IsBtnClk;

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();						// ������������� �������

	virtual void OnOK()
	{};
	virtual void OnCancel();				// ��������� ������� "�������� ���� � ����� �� �������"

	afx_msg void OnAccept();				// ��������� ������� "������� ������ � ����� �� �������"
	afx_msg void OnChangeAnalysisType();	// ��������� ������� "�������� ��� �������"
	afx_msg void OnChangeLabelMoisture();	// ��������� ������� "�������� ������� ��� ���������"
	afx_msg void OnChangeDefModel();		// ��������� ������� "�������� ������ �� ���������"
	afx_msg void OnParamsSelListSelChange();// ��������� ������� "�������� �������� � ������ ��������� �����������"
	afx_msg void OnAdd();					// ��������� ������� "������� ����� ������� �������"
	afx_msg void OnAddAll();				// ��������� ������� "������� ����� ������� ������� ��� ���� �����������"
	afx_msg void OnRemove();				// ��������� ������� "������� ������� �������"
	afx_msg void OnRemoveAll();				// ��������� ������� "������� ��� ������� �������"
	afx_msg void OnAdd2();					// ��������� ������� "�������� ������ � ������"
	afx_msg void OnAddAll2();				// ��������� ������� "�������� ��� �������� ������ � ������"
	afx_msg void OnRemove2();				// ��������� ������� "������� ������ �� ������"
	afx_msg void OnRemoveAll2();			// ��������� ������� "������� ��� ������ �� ������"
	afx_msg void OnStdMoisChanged();		// ��������� ������� "�������� ����������� ���������"
	afx_msg void OnSetUse();				// ��������� ������� "���������� ������� ������������� ����������� ���������"
	afx_msg void OnAdd4();
	afx_msg void OnAddAll4();
	afx_msg void OnRemove4();
	afx_msg void OnRemoveAll4();
	afx_msg void OnInsertMEAN();
	afx_msg void OnInsertAND();
	afx_msg void OnInsertOR();
	afx_msg void OnInsertOPEN();
	afx_msg void OnInsertCLOSE();
	afx_msg void OnBack();
	afx_msg void OnClear();
	afx_msg void OnHelp();					// ��������� ������� "������� �������"

	void ShowFields();
	void ShowFormula();

	void FillListParamSel(int newsel = -1);	// ��������� ������� ���� �����������, �����. ��������� ��������
	void FillListModelAll(int newsel = -1);	// ��������� ������ ���� �������� �������
	void FillListModelSel(int newsel = -1);	// ��������� ������ ���� ��������� �������

	void FillListQltModelSel(int newsel = -1);	// ��������� ������ ���� ��������� ������������ �������

	bool IsAvailableModelExist(CString name);	// ��������� ������������� ������� ��� ������� ����������

	bool IsChanged();

public:

	bool IsParamSel(int ind);
	bool IsModelQltSel(int ind);
	bool IsModelQntSel(int ind);

protected:

	DECLARE_MESSAGE_MAP()
};

#endif
