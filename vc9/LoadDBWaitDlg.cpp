#include "stdafx.h"

#include "LoadDBWaitDlg.h"
#include "BranchInfo.h"
#include "RootData.h"

IMPLEMENT_DYNAMIC(CLoadDBWaitDlg, CWaitingDlg)

CLoadDBWaitDlg::CLoadDBWaitDlg(CTreeCtrl* tree, int mode, CWnd* pParent) : CWaitingDlg(pParent)
{
	pTree = tree;
	Mode = mode;

	Comment = (Mode == C_MODE_LOADDB_LOAD) ? ParcelLoadString(STRID_LOADDB_WAIT_COMMENT_LOAD) :
		ParcelLoadString(STRID_LOADDB_WAIT_COMMENT_CLOSE);
}

CLoadDBWaitDlg::~CLoadDBWaitDlg()
{
}

void CLoadDBWaitDlg::DoDataExchange(CDataExchange* pDX)
{
	CWaitingDlg::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CLoadDBWaitDlg, CWaitingDlg)
END_MESSAGE_MAP()


BOOL CLoadDBWaitDlg::OnInitDialog()
{
	CWaitingDlg::OnInitDialog();

	return true;
}

void CLoadDBWaitDlg::OK()
{
	CWaitingDlg::OK();
}

void CLoadDBWaitDlg::Stop()
{
	CWaitingDlg::Stop();
}

void CLoadDBWaitDlg::Work()
{
	ParcelWait(true);
	
	if(Mode == C_MODE_LOADDB_LOAD)
	{
		bool res = pRoot->LoadDatabase(pTree);
		ParcelWait(false);
		if(res)
			OK();
		else
			Stop();
	}
	else
	{
		pRoot->Database.Close();
		OK();
	}
}
