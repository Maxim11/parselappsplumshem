#ifndef _COPY_BASE_WAIT_DLG_H_
#define _COPY_BASE_WAIT_DLG_H_

#pragma once

#include "WaitingDlg.h"
#include "ParcelView.h"


///////////////////////////////////////////////////////////////////////////////////////////////////
// ������ ��������� �������� ���������� �������������� �������

class CCopyBaseWaitDlg : public CWaitingDlg
{
	DECLARE_DYNAMIC(CCopyBaseWaitDlg)

public:

	CCopyBaseWaitDlg(CString fname, CWnd* pParent = NULL);	// �����������
	virtual ~CCopyBaseWaitDlg();								// ����������

	enum { IDD = CWaitingDlg::IDD };	// ������������� �������

protected:

	CString FileName;

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();						// ������������� �������

	virtual void OK();				// ��������� �������� ����� �� �������
	virtual void Stop();			// ��������� ����������� ����� �� �������
	virtual void Work();			// ������ �������� �������

	DECLARE_MESSAGE_MAP()
};

#endif
