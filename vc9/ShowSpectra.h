#ifndef _PARCEL_SHOW_SPECTRA_H_
#define _PARCEL_SHOW_SPECTRA_H_

#pragma once

#include "TransferData.h"

struct SpectraShowSettings
{
	CString Name;
	int type;
	bool IsSel;

	int shift;
	VDbl SpecData;

	SpectraShowSettings(CString name, int num, int indT = 0, int sh = 0):type(C_SPECTRA_TYPE_ALL),IsSel(true),shift(sh)
	{
		CString fmt = cEmpty;
		for(int i=0; i<indT; i++)
			fmt += '*';
		fmt += _T("%s-%02d");
		Name.Format(fmt, name, num);
	};
	~SpectraShowSettings()
	{
		SpecData.clear();
	}
};

typedef vector<SpectraShowSettings> VSSS;
typedef vector<SpectraShowSettings>::iterator ItSSS;

struct SpectraShowData
{
	CString Name;
	COLORREF Color;

	VDbl x;
	VDbl y;
};

typedef vector<SpectraShowData> VSSD;
typedef vector<SpectraShowData>::iterator ItSSD;

class CShowSpectra
{
public:

	CShowSpectra(VDbl* points);
	~CShowSpectra();

	void Copy(CShowSpectra& data);

protected:

	VDbl Points;

	int NPnt;

	int Type;

public:

	VSSS Spectra;
	VSSD ShowData;

public:

	VDbl* GetPoints();

	int GetNPnt();

	int GetNSpectraType(int type);
	int GetNSpectraSel();
	int GetNShowData();

	SpectraShowSettings* GetSpectraShow(int type, int ind);
	SpectraShowSettings* GetSpectraShowSel(int ind);
	SpectraShowSettings* GetSpectraShowSel(CString name);
	SpectraShowSettings* AddSpectraShow(CString samname, int num, int indT, int sh, bool issel, int type = C_SPECTRA_TYPE_SIMPLE);

	int GetType();
	void SetType(int type);

	void SetSelAll(bool sel);

	void CreateShowData();

	double GetYMin();
	double GetYMax();

};

#endif
