#ifndef _PARCEL_MODEL_DATA_H_
#define _PARCEL_MODEL_DATA_H_

#pragma once

#include "SampleData.h"
#include "ModelResult.h"

// ��������� ���������� ������ � ��������� � ���� �����������

struct ModelParam
{
	CString ParamName;		// ��� ����������
	double LimMSD;			// ���������� ��� �����������
	double LowValue;		// ������ �������� ��������� �����������
	double UpperValue;		// ������� �������� ��������� �����������
	double CorrB;			// ��������� b
	double CorrA;			// ��������� a

	ModelParam(CString name):ParamName(name),LimMSD(1.),LowValue(0.),UpperValue(1.e9),CorrB(1.),CorrA(0.)
	{};
	void Copy(ModelParam& modparam);
};

typedef vector<ModelParam> VModPar;
typedef vector<ModelParam>::iterator ItModPar;

// ��������� ������� ������ � �� �������

struct ModelSample
{
	CString SampleName;			// ��� �������
	CString TransName;			// ��� ������
	int TransType;				// ��� ������
	MapIntBool Spectra;			// ������ ��������� ��������� ��������
	CTime tLastSpecDate;		// from trans

//	sort data
	int iTransSort;				// 
	ReferenceData *pRefSort;	//


	ModelSample(CSampleData* SamData);
	~ModelSample()
	{
		Spectra.clear();
	};

	void Copy(ModelSample* modsample);
	void SetAll(bool bUse = true);
	void Rebuild(CSampleData* SamData);
	int GetNSpectra();
	int GetNSpectraUsed();
	CString GetUsesSpectrumStr();
	CString GetUnUsesSpectrumStr();
//	CTime GetLastSpecDate();						// �������� ���� ���������� ��������� �������
	bool IsSpectrum(int num);
	bool IsSpectrumUsed(int num);

	bool IsOwn();
	bool IsTransN();
};

typedef vector<ModelSample> VModSam;
typedef vector<ModelSample>::iterator ItModSam;

///////////////////////////////////////////////////////////////////////////////////////////////////
// ��������� ��� �������� ����� �������� � ������

struct SModInfo
{
	CString SetName;
	CString Value;
};

typedef vector<SModInfo> VModInfo;
typedef vector<SModInfo>::iterator ItModInfo;

///////////////////////////////////////////////////////////////////////////////////////////////////
// ����� ��� �������� ������������� ������

class CModelData
{
public:

	CModelData(HTREEITEM hParent);		// �����������
	~CModelData();						// ����������

	void Copy(CModelData& data, bool bReplaceOwn=false, CString NewTransName=_T(""), int NewTransType = C_TRANS_TYPE_NOTCORR);		// ����������� ������

protected:

	HTREEITEM hItem;					// ������ ���� � ������, ���������������� ������

	HTREEITEM hParentItem;				// ������ ���� � ������, ���������������� ������������� �������

	CString Name;						// ��� ������
	CTime Date;							// ���� � ����� ��������

	CString TransName;					// ��� ������ ������������ ��������

	int NCompQlt;						// ����� ������� ��������� ��� ������������� �������
	int NCompQnt;						// ����� ������� ��������� ��� ��������������� �������
	double DimHiper;					// ������ ���������
	int NHarm;							// ����� ��������
	double Kfnl;						// ����������� ������������
	double MaxMah;						// ���������� ���������� ������������
	double MaxMSD;						// ���������� ��� �������� ��� ������������� �������
	bool UseSECV;						// ������� ������������ �� SECV
	int LowSpecRange;					// ������ ������� ������������� ���������
	int UpperSpecRange;					// ������� ������� ������������� ���������

	int AnalysisType;					// ��� ������� (������)
	int ModelType;						// ��� ������ (������)
	int SpectrumType;					// ��� ������� (������)

	CString SamSortName;		// ��� ���������� ��� ���������� ��������. ���� ����� -- ���������� �� ����� �������
	bool SamSortDir;			// ����������� ���������� ��������.

	VInt Preproc;						// ������ �������������

public:

	VModPar ModelParams;				// ������ ����������� ��� �����������
	VStr TransNames;					// ������ ���� ������������ �������

	CModelResult ModelResult;			// ���������� ���������� ������

protected:

	VModSam SamplesRest;
	VModSam SamplesForCalib;				// ������ ���� �������� ��� �����������
	VModSam SamplesForValid;				// ������ ���� �������� ��� ���������

	VDbl SpecPoints;					// ������ ������������ ����� (���)
	SInt ExcPoints;						// ������ ����������� ������������ �����

	VModInfo Info;

public:

	HTREEITEM GetHItem();				// �������� ������ ���� � ������, ��������������� ������
	void SetHItem(HTREEITEM item);		// ���������� ������ ���� � ������, ��������������� ������

	HTREEITEM GetParentHItem();			// �������� ������ ���� � ������, ��������������� �������
	void SetParentHItem(HTREEITEM item);

	CString GetName();					// �������� ��� ������
	void SetName(CString name);			// ���������� ��� ������

	CTime GetDate();					// �������� ���� � ����� �������� ������
	void SetDate();						// ���������� ������� ���� � ����� ��� ���� � ����� �������� ������

	int GetNTransNames();				// �������� ����� ������������ �������
	CString GetTransName();				// �������� ��� ������ ������������ ��������
	int GetTransInd();					// ���������� ����� ������ � ������ ������� ������� �������
	void SetTransName2(CString name);	// ���������� ��� ������ ������������ ��������
	void SetTransName(CString name);	// ���������� ��� ������ ������������ ��������
	void SetTransNameByInd(int ind);	// ���������� ��� ������ ������������ �������� �� ����������� ������
	void ConformTrans(CString oldname);	// �������� ������ � ������������ ���������� ������

	int GetNCompQlt();					// �������� ����� ������� ��������� ��� ������������� �������
	void SetNCompQlt(int num);			// ���������� ����� ������� ��������� ��� ������������� �������

	int GetNCompQnt();					// �������� ����� ������� ��������� ��� ��������������� �������
	void SetNCompQnt(int num);			// ���������� ����� ������� ��������� ��� ��������������� �������

	double GetDimHiper();				// �������� ������ ���������
	void SetDimHiper(double dim);		// ���������� ������ ���������

	int GetNHarm();						// �������� ����� ��������
	void SetNHarm(int num);				// ���������� ����� ��������

	double GetKfnl();					// �������� ����������� ������������
	void SetKfnl(double kf);			// ���������� ����������� ������������

	double GetMaxMah();					// �������� ���������� ���������� ������������
	void SetMaxMah(double max);			// ���������� ���������� ���������� ������������

	double GetMaxMSD();					// �������� ���������� ��� �����������
	void SetMaxMSD(double max);			// ���������� ���������� ��� �����������

	bool IsUseSECV();					// �������� ������� ������������ �� SECV
	CString GetUseSECVStr();			// �������� �����: ������������ �� SECV
	void SetUseSECV(bool use);			// ���������� ������� ������������ �� SECV

	int GetLowSpecRange();				// �������� ������ ������� ������������� ���������
	void SetLowSpecRange(int lim);		// ���������� ������ ������� ������������� ���������

	int GetUpperSpecRange();			// �������� ������� ������� ������������� ���������
	void SetUpperSpecRange(int lim);	// ���������� ������� ������� ������������� ���������

	int GetAnalysisType();					// �������� ��� ������� ��� ������ � ��� (������ ��������� ��������)
	bool SetAnalysisType(int ind);			// ���������� ��� ������� �� ������� � ���

	int GetModelType();						// �������� ��� ������ ��� ������ � ���
	int GetModelHSOType();					// �������� ��� HSO ������ ��� ������ � ���
	bool SetModelType(int ind);				// ���������� ��� ������ �� ������� � ���

	int GetSpectrumType();					// �������� ��� ������� ��� ������ � ���
	bool SetSpectrumType(int ind);			// ���������� ��� ������� �� ������� � ���

	CString GetSamSortName();
	void SetSamSortName(CString name);
	bool GetSamSortDir();
	void SetSamSortDir(bool flag);

	int GetNPreproc();						// �������� ����� �������������
	int GetPreprocInd(int num);				// �������� ������������� ��� ������ � ���
	CString GetAllPreprocAsStr();			// �������� ��� ������������� � ���� ������
	CString GetOwnPreprocAsStr();
	CString GetTransPreprocAsStr();
	void GetAllPreproc(VInt& vect);			// �������� ��� ������������� � ���� ������� ��������
	int GetPreprocNum(int ind);				// �������� ����� ������������� �� ������� � ��� (-1 ���� �����������).
	void SetAllPreproc(VInt* vect);			// ���������� ��� ������������� �� ������� ��������
	void SetAllPreproc(CString prepstr);
	int GetNTransPreproc();					// �������� ����� ������������� ������ ������������ ��������

	int GetNModelParams();					// �������� ����� ����������� ������
	ModelParam* GetModelParam(int ind);		// �������� ���������� ������ � ��������� � ��� ����������� �� �������
	ModelParam* GetModelParam(CString name);// �������� ���������� ������ � ��������� � ��� ����������� �� �����
	bool IsModelParamExist(CString name);	// �������� ������� ������������� ���������� ������
	bool AddModelParam(CString name);		// ������� ����� ���������� ������
	bool AddModelParam(ModelParam* modpar);	// ������� ����� ���������� ������ � ���������� ��������� �����������
	bool DeleteModelParam(CString name);	// ������� ���������� ������
	void DeleteAllModelParams();			// ������� ��� ���������� ������

	int GetNSamplesRest();
	ModelSample* GetSampleRest(int ind);
	ModelSample* GetSampleRest(CString name, CString transname = cOwn);
	bool AddSampleRest(ModelSample *modsample);
	void DeleteAllSamplesRest();
	void CreateSamplesRest();

	int GetNSamplesForCalib();												// �������� ����� �������� ��� �����������
	int GetNSpectraForCalib();												// �������� ����� �������� ��� �����������
	ModelSample* GetSampleForCalib(int ind);								// �������� ������� ��� ����������� �� ������� � ������
	ModelSample* GetSampleForCalib(CString name, CString transname);	// �������� ������� ��� ����������� �� �����
	bool AddSampleForCalib(ModelSample *modsample);							// �������� ������� ��� �����������
	bool DeleteSampleForCalib(CString name, CString transname);				// ������� ������� ��� �����������
	bool DeleteSampleForCalib(int ind);										// ������� ������� ��� �����������
	void DeleteAllSamplesForCalib();										// ������� ��� ������� ��� �����������
	int GetNSamplesForCalibOwn();
	int GetNSamplesForCalibTrans();
	ModelSample* GetSampleForCalibOwn(int ind);
	ModelSample* GetSampleForCalibTrans(int ind);

	int GetNSamplesForValid();												// �������� ����� �������� ��� ���������
	int GetNSpectraForValid();												// �������� ����� �������� ��� ���������
	ModelSample* GetSampleForValid(int ind);								// �������� ������� ��� ���������
	ModelSample* GetSampleForValid(CString name, CString transname);		// �������� ������� ��� ��������� �� �����
	bool AddSampleForValid(ModelSample *modsample);							// �������� ��� ������� ��� ���������
	bool DeleteSampleForValid(CString name, CString transname);				// ������� ������� ��� ���������
	bool DeleteSampleForValid(int ind);										// ������� ������� ��� ���������
	void DeleteAllSamplesForValid();										// ������� ��� ������� ��� ���������
	int GetNSamplesForValidOwn();
	int GetNSamplesForValidTrans();
	ModelSample* GetSampleForValidOwn(int ind);
	ModelSample* GetSampleForValidTrans(int ind);

	void GetCalibValid(VModSam& calib, VModSam& valid);
	void SetCalibValid(VModSam& calib, VModSam& valid);

	int GetNSpecPoints();					// �������� ����� ������������ �����
	double GetSpecPoint(int indSP);			// �������� ������������ �����

	int GetNExcPoints();					// �������� ����� ����������� ������������ �����
	int GetExcPoint(int indEP);				// �������� ����������� ������������ ����� ��� ������ � ���
	double GetExcPointMean(int indEP);		// �������� ����������� ������������ �����
	void GetExcPoints(SInt& points);		// �������� ��� ����������� �����
	void SetExcPoints(SInt& points);		// ���������� ��� ����������� �����
	void DeleteAllExcPoints();				// ������� ��� ����������� ������������ �����

	bool IsSampleAvailable(CSampleData* data);	// ��������� ������� �� ����������

	void GetSpecPoints();					// �������� ������ ������������ ����� �� ������� �������
	int CalcLeftSpecPointInd(int left);		// ������� ������ ������������ ����� �� ������� ������������� ���������
	void CalcModParamLimits();				// ���������� ����������� �� ���������� ������

	int GetNFactor();						// �������� ����� �������� (��� ���������� �������� �������)

	CString GetSampleTransName(ModelSample *pMSam); // �������� ��� ������� � ������ ���������

	bool IsReadyForTrans();					// ����� �� �������������� ��� �������� ��� ������ ������-�������
	bool IsReadyForTransPrj();				// ����� �� �������������� ��� �������� ������� ��� ������ ������-�������
	bool IsExistCalibValid();				// ��������, �� ������������ �� ������� ��� �������������� � ������������� ������������

	bool CalculateModel(bool exp = false);

	void FillModInfo();
	int GetNModInfo();
	SModInfo* GetModInfo(int ind);

	bool GetSpectraInfo(VLSI& PSam, bool bVal = false, bool bAll = false);
	bool LoadSpectraData(bool bVal = false, bool bAll = false);				// ��������� ������������ ������ �� ����
	void FreeSpectraData();													// ���������� ������������ ������

	void GetDataForBase(prdb::PRModel& Obj);
	void SetDataFromBase(prdb::PRModel& Obj);

};

#endif
