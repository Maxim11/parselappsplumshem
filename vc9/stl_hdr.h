#ifndef _PARCEL_STL_HDR_H_
#define _PARCEL_STL_HDR_H_

#pragma once
#pragma warning( disable : 4702 )


#include <vector>
#include <list>
#include <set>
#include <map>
#include <algorithm>

#pragma warning( default : 4702 )

using namespace std;

typedef vector<TCHAR> VChr;
typedef vector<TCHAR>::iterator ItChr;
typedef vector<CString> VStr;
typedef vector<CString>::iterator ItStr;
typedef vector<int> VInt;
typedef vector<int>::iterator ItInt;
typedef vector<double> VDbl;
typedef vector<double>::iterator ItDbl;
typedef vector<VDbl> MDbl;
typedef vector<VDbl>::iterator ItDblDbl;
typedef vector<VInt> MInt;
typedef vector<VInt>::iterator ItIntInt;
typedef set<int> SInt;
typedef set<int>::iterator ItSInt;
typedef set<double> SDbl;
typedef set<double>::iterator ItSDbl;
typedef pair<int, bool> PIntBool;
typedef map<int, bool> MapIntBool;
typedef map<int, bool>::iterator ItIntBool;
typedef pair<int, double> PIntDbl;
typedef map<int, double> MapIntDbl;
typedef map<int, double>::iterator ItIntDbl;
typedef vector<PIntDbl> VIntDbl;
typedef vector<PIntDbl>::iterator ItVIntDbl;
typedef vector<VIntDbl> MIntDbl;
typedef vector<VIntDbl>::iterator ItMIntDbl;
typedef pair<double, double> PDblDbl;
typedef vector<PDblDbl> VDblDbl;
typedef vector<PDblDbl>::iterator ItVDblDbl;
typedef pair<int, CString> PIntStr;
typedef map<int, CString> MapIntStr;
typedef map<int, CString>::iterator ItIntStr;

#endif
