#include "stdafx.h"

#include "Parcel.h"
#include "MainFrm.h"
#include "wm.h"
#include "ParcelView.h"
//#include "DfxCached.h"
#include "LoadDBWaitDlg.h"
#include "SplashScreenEx.h"
#include "FileSystem.h"

CMainFrame* pFrame = NULL;

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)

	ON_WM_CREATE()
	ON_WM_TIMER()
	ON_WM_SIZE()
	ON_WM_CLOSE()
	ON_WM_DESTROY()
	ON_WM_GETMINMAXINFO()

	ON_MESSAGE(PARCEL_WM_SET_STATUS_BAR, OnSetStatusBar)

	ON_MESSAGE(PARCEL_WM_OPEN_PROGRESS_BAR, OnOpenProgressBar)
	ON_MESSAGE(PARCEL_WM_CLOSE_PROGRESS_BAR, OnCloseProgressBar)
	ON_MESSAGE(PARCEL_WM_SET_PROGRESS_BAR, OnSetProgressBar)

	ON_COMMAND(ID_BASE_NEW, OnBaseNew)
	ON_UPDATE_COMMAND_UI(ID_BASE_NEW, OnUpdateBaseNew)
	ON_COMMAND(ID_BASE_LOAD, OnBaseLoad)
	ON_UPDATE_COMMAND_UI(ID_BASE_LOAD, OnUpdateBaseLoad)
	ON_COMMAND(ID_BASE_CLOSE, OnBaseClose)
	ON_UPDATE_COMMAND_UI(ID_BASE_CLOSE, OnUpdateBaseClose)
	ON_COMMAND(ID_BASE_DELETE, OnBaseDelete)
	ON_UPDATE_COMMAND_UI(ID_BASE_DELETE, OnUpdateBaseDelete)
	ON_COMMAND(ID_BASE_COPY, OnBaseCopy)
	ON_UPDATE_COMMAND_UI(ID_BASE_COPY, OnUpdateBaseCopy)

	ON_COMMAND(ID_DEVICE_CREATENEWDEVICE, OnCreateNewDevice)
	ON_UPDATE_COMMAND_UI(ID_DEVICE_CREATENEWDEVICE, OnUpdateCreateNewDevice)
	ON_COMMAND(ID_DEVICE_CHANGEDEVICESETTINGS, OnChangeDeviceSettings)
	ON_UPDATE_COMMAND_UI(ID_DEVICE_CHANGEDEVICESETTINGS, OnUpdateChangeDeviceSettings)
	ON_COMMAND(ID_DEVICE_DELETEDEVICE, OnDeleteDevice)
	ON_UPDATE_COMMAND_UI(ID_DEVICE_DELETEDEVICE, OnUpdateDeleteDevice)

	ON_COMMAND(ID_PROJECT_CREATENEWPROJECT, OnCreateNewProject)
	ON_UPDATE_COMMAND_UI(ID_PROJECT_CREATENEWPROJECT, OnUpdateCreateNewProject)
	ON_COMMAND(ID_PROJECT_CHANGEPROJECTSETTINGS, OnChangeProjectSettings)
	ON_UPDATE_COMMAND_UI(ID_PROJECT_CHANGEPROJECTSETTINGS, OnUpdateChangeProjectSettings)
	ON_COMMAND(ID_PROJECT_DELETEPROJECT, OnDeleteProject)
	ON_UPDATE_COMMAND_UI(ID_PROJECT_DELETEPROJECT, OnUpdateDeleteProject)
	ON_COMMAND(ID_PROJECT_CREATE_SST, OnCreateSSTProject)
	ON_UPDATE_COMMAND_UI(ID_PROJECT_CREATE_SST, OnUpdateCreateSSTProject)
	ON_COMMAND(ID_PROJECT_LOADOLD, OnLoadOldProject)
	ON_UPDATE_COMMAND_UI(ID_PROJECT_LOADOLD, OnUpdateLoadOldProject)
	ON_COMMAND(ID_PROJECT_LOADOLD_SST, OnLoadOldSSTProject)
	ON_UPDATE_COMMAND_UI(ID_PROJECT_LOADOLD_SST, OnUpdateLoadOldSSTProject)
	ON_COMMAND(ID_PROJECT_COPYTOBASE, OnCopyProjectToBase)
	ON_UPDATE_COMMAND_UI(ID_PROJECT_COPYTOBASE, OnUpdateCopyProjectToBase)
	ON_COMMAND(ID_PROJECT_COPYTOBASE_ALL, OnCopyAllProjectsToBase)
	ON_UPDATE_COMMAND_UI(ID_PROJECT_COPYTOBASE_ALL, OnUpdateCopyAllProjectsToBase)
	ON_COMMAND(ID_PROJECT_EXPORT, OnExportProjectSettings)
	ON_UPDATE_COMMAND_UI(ID_PROJECT_EXPORT, OnUpdateExportProjectSettings)
	ON_COMMAND(ID_PROJECT_IMPORT, OnImportSpectra)
	ON_UPDATE_COMMAND_UI(ID_PROJECT_IMPORT, OnUpdateImportSpectra)
	ON_COMMAND(ID_PROJECT_TRANSFER, OnProjectTransfer)
	ON_UPDATE_COMMAND_UI(ID_PROJECT_TRANSFER, OnUpdateProjectTransfer)

	ON_COMMAND(ID_PARAM_CHANGELIST, OnEditParamList)
	ON_UPDATE_COMMAND_UI(ID_PARAM_CHANGELIST, OnUpdateEditParamList)

	ON_COMMAND(ID_SAMPLE_EDITSAMPLELIST, OnEditSampleList)
	ON_UPDATE_COMMAND_UI(ID_SAMPLE_EDITSAMPLELIST, OnUpdateEditSampleList)
	ON_COMMAND(ID_SAMPLE_CREATE_SST, OnCreateSSTTransfer)
	ON_UPDATE_COMMAND_UI(ID_SAMPLE_CREATE_SST, OnUpdateCreateSSTTransfer)
	ON_COMMAND(ID_SAMPLE_DELETE_SST, OnDeleteSSTTransfer)
	ON_UPDATE_COMMAND_UI(ID_SAMPLE_DELETE_SST, OnUpdateDeleteSSTTransfer)
	ON_COMMAND(ID_SAMPLE_ADDTRANS, OnAddTransfer)
	ON_UPDATE_COMMAND_UI(ID_SAMPLE_ADDTRANS, OnUpdateAddTransfer)
	ON_COMMAND(ID_SAMPLE_EDITTRANS, OnEditTransfer)
	ON_UPDATE_COMMAND_UI(ID_SAMPLE_EDITTRANS, OnUpdateEditTransfer)
	ON_COMMAND(ID_SAMPLE_DELETETRANS, OnDeleteTransfer)
	ON_UPDATE_COMMAND_UI(ID_SAMPLE_DELETETRANS, OnUpdateDeleteTransfer)

	ON_COMMAND(ID_MODEL_CREATENEWMODEL, OnCreateNewModel)
	ON_UPDATE_COMMAND_UI(ID_MODEL_CREATENEWMODEL, OnUpdateCreateNewModel)
	ON_COMMAND(ID_MODEL_COPYMODEL, OnCopyModel)
	ON_UPDATE_COMMAND_UI(ID_MODEL_COPYMODEL, OnUpdateCopyModel)
	ON_COMMAND(ID_MODEL_CHANGEMODELSETTINGS, OnChangeModelSettings)
	ON_UPDATE_COMMAND_UI(ID_MODEL_CHANGEMODELSETTINGS, OnUpdateChangeModelSettings)
	ON_COMMAND(ID_MODEL_DELETEMODEL, OnDeleteModel)
	ON_UPDATE_COMMAND_UI(ID_MODEL_DELETEMODEL, OnUpdateDeleteModel)
	ON_COMMAND(ID_MODEL_MODELRESULT, OnModelResult)
	ON_UPDATE_COMMAND_UI(ID_MODEL_MODELRESULT, OnUpdateModelResult)

	ON_COMMAND(ID_METHOD_CREATENEWMETHOD, OnCreateNewMethod)
	ON_UPDATE_COMMAND_UI(ID_METHOD_CREATENEWMETHOD, OnUpdateCreateNewMethod)
	ON_COMMAND(ID_METHOD_COPYMETHOD, OnCopyMethod)
	ON_UPDATE_COMMAND_UI(ID_METHOD_COPYMETHOD, OnUpdateCopyMethod)
	ON_COMMAND(ID_METHOD_CHANGEMETHODSETTINGS, OnChangeMethodSettings)
	ON_UPDATE_COMMAND_UI(ID_METHOD_CHANGEMETHODSETTINGS, OnUpdateChangeMethodSettings)
	ON_COMMAND(ID_METHOD_DELETEMETHOD, OnDeleteMethod)
	ON_UPDATE_COMMAND_UI(ID_METHOD_DELETEMETHOD, OnUpdateDeleteMethod)
	ON_COMMAND(ID_METHOD_EXPORTMETHOD, OnExportMethod)
	ON_UPDATE_COMMAND_UI(ID_METHOD_EXPORTMETHOD, OnUpdateExportMethod)
/*
	ON_COMMAND_RANGE(ID_VIEW_LANG_0, ID_VIEW_LANG_MAX, OnChangeLanguage)
	ON_UPDATE_COMMAND_UI_RANGE(ID_VIEW_LANG_0, ID_VIEW_LANG_MAX, OnUpdateChangeLanguage)
*/
	ON_COMMAND(ID_HELP_FINDER, OnHelpFinder)
	ON_UPDATE_COMMAND_UI(ID_HELP_FINDER, OnUpdateHelpFinder)
	ON_COMMAND(ID_HELP, OnHelpFinder)
/*
	ON_COMMAND(ID_CONTEXT_HELP, CFrameWnd::OnContextHelp)
	ON_COMMAND(ID_DEFAULT_HELP, CFrameWnd::OnHelpFinder)
*/
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};


CMainFrame::CMainFrame()
{
	pFrame = this;
	pProgressBar = NULL;
	pSplashScreen = NULL;
	nSorts = 0;
	nLang = 2;
	iLang = C_LANG_RUSSIAN;
}

CMainFrame::~CMainFrame()
{
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	LoadInit();

//	int indSplash = (iLang == C_LANG_RUSSIAN) ? IDB_SPLASH_BITMAP : IDB_SPLASH_ENG_BITMAP;

/*
	pSplashScreen = new CSplashScreenEx();
	CRect RectS(350, 160, 570, 175);

	pSplashScreen->Create(this, _T(""), 0, WS_CHILD | CSS_FADE | CSS_CENTERSCREEN | CSS_SHADOW | CSS_HIDEONCLICK);
	pSplashScreen->SetBitmap(indSplash, 255, 0, 255);
	pSplashScreen->SetTextFont(_T("Impact"), 100, CSS_TEXT_UNDERLINE);
	pSplashScreen->SetTextRect(RectS);
	pSplashScreen->SetTextColor(RGB(255, 255, 255));
	pSplashScreen->SetTextFormat(DT_SINGLELINE | DT_RIGHT | DT_VCENTER);
	pSplashScreen->Show();

	SetTimer(1, 5000, 0);
*/
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;
	}

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;
	}

	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar);

	SetLanguage(iLang);
	SetHelpFile(iLang);
	pView->Data.Templates.SetBaseName(TemplateOptimName);
	if(!BaseName.IsEmpty())
	{
		pView->Data.SetBaseFileName(BaseName);
	}

	pRoot->SetTemplateName(TemplateQntName, C_TEMPLATE_QNT);
	pRoot->SetTemplateName(TemplateQltName, C_TEMPLATE_QLT);

	MakeMenu();

	return 0;
}

void CMainFrame::OnTimer(UINT nIDEvent)
{/*
	if(nIDEvent == 1) {
		TCHAR			szValue[128];
		TCHAR			szPath[_MAX_PATH];
		CModuleVersion	VersionInfo;

		GetModuleFileName(NULL, szPath, _MAX_PATH);

		if(VersionInfo.GetFileVersionInfo(szPath))
			VersionInfo.GetProductVersion(szValue, _countof(szValue));

		if(pSplashScreen->IsWindowVisible())
			pSplashScreen->SetText(szValue);
	}
*/
	if(nIDEvent == 1)
	{

		if(pSplashScreen && pSplashScreen->IsWindowVisible())
		{
			pSplashScreen->Hide();
			pSplashScreen = NULL;
		}
		ShowWindow(SW_SHOWMAXIMIZED);

	}

	if(nIDEvent == 2)
	{
		KillTimer(1);
		if(pSplashScreen)
		{
			pSplashScreen->Hide();
			pSplashScreen = NULL;
		}
		ShowWindow(SW_SHOWMAXIMIZED);
	}

	KillTimer(nIDEvent);

	CFrameWnd::OnTimer(nIDEvent);
}

void CMainFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize = CPoint(940, 750);

	CFrameWnd::OnGetMinMaxInfo(lpMMI);
}

void CMainFrame::OnSize(UINT Type, int cx, int cy)
{
	CFrameWnd::OnSize(Type, cx, cy);
}

void CMainFrame::OnClose()
{
//	if(IDYES != AfxMessageBox(GETSTRUI(1200), MB_YESNO)) return;
	if(!ParcelConfirm(GETSTRUI(1200), true)) return;

	SaveInit();

	CLoadDBWaitDlg dlg2(NULL, C_MODE_LOADDB_CLOSE);
	dlg2.DoModal();

	CFrameWnd::OnClose();
}

void CMainFrame::OnDestroy()
{
	CFrameWnd::OnDestroy();
}

void CMainFrame::MakeMenu()
{
	CMenu *pMenu = GetMenu();
	CMenu *psubmenu = NULL;
//	CMenu *psubsubmenu = NULL;

	CString s;
	UINT id;

// Menu "File". ������ 0
	s = ParcelLoadString(STRID_MENU_FILE);
	pMenu->ModifyMenu(C_MENU_FILE, MF_BYPOSITION, C_MENU_FILE, s);

	psubmenu = pMenu->GetSubMenu(C_MENU_FILE);
	if(psubmenu == NULL)
		return;

	id = psubmenu->GetMenuItemID(C_MENU_FILE_CREATE);
	s = ParcelLoadString(STRID_MENU_FILE_NEWBASE);
    psubmenu->ModifyMenu(id, MF_BYCOMMAND, id, s);
	id = psubmenu->GetMenuItemID(C_MENU_FILE_OPEN);
	s = ParcelLoadString(STRID_MENU_FILE_LOADBASE);
    psubmenu->ModifyMenu(id, MF_BYCOMMAND, id, s);
	id = psubmenu->GetMenuItemID(C_MENU_FILE_CLOSE);
	s = ParcelLoadString(STRID_MENU_FILE_CLOSEBASE);
    psubmenu->ModifyMenu(id, MF_BYCOMMAND, id, s);
	id = psubmenu->GetMenuItemID(C_MENU_FILE_DELETE);
	s = ParcelLoadString(STRID_MENU_FILE_DELETEBASE);
    psubmenu->ModifyMenu(id, MF_BYCOMMAND, id, s);
	id = psubmenu->GetMenuItemID(C_MENU_FILE_SAVEAS);
	s = ParcelLoadString(STRID_MENU_FILE_SAVEBASE);
    psubmenu->ModifyMenu(id, MF_BYCOMMAND, id, s);
	id = psubmenu->GetMenuItemID(C_MENU_FILE_EXIT);
	s = ParcelLoadString(STRID_MENU_FILE_EXIT);
    psubmenu->ModifyMenu(id, MF_BYCOMMAND, id, s);

// Menu "Instrument". ������ 1
	s = ParcelLoadString(STRID_MENU_DEVICE);
	pMenu->ModifyMenu(C_MENU_DEVICE, MF_BYPOSITION, C_MENU_DEVICE, s);

	psubmenu = pMenu->GetSubMenu(C_MENU_DEVICE);
	if(psubmenu == NULL)
		return;

	id = psubmenu->GetMenuItemID(C_MENU_DEVICE_CREATE);
	s = ParcelLoadString(STRID_MENU_DEVICE_CREATE);
	psubmenu->ModifyMenu(id, MF_BYCOMMAND, id, s);
	id = psubmenu->GetMenuItemID(C_MENU_DEVICE_EDIT);
	s = ParcelLoadString(STRID_MENU_DEVICE_CHANGE);
    psubmenu->ModifyMenu(id, MF_BYCOMMAND, id, s);
	id = psubmenu->GetMenuItemID(C_MENU_DEVICE_REMOVE);
	s = ParcelLoadString(STRID_MENU_DEVICE_DELETE);
    psubmenu->ModifyMenu(id, MF_BYCOMMAND, id, s);

// Menu "Project". ������ 2
	s = ParcelLoadString(STRID_MENU_PROJECT);
	pMenu->ModifyMenu(C_MENU_PROJECT, MF_BYPOSITION, C_MENU_PROJECT, s);

	psubmenu = pMenu->GetSubMenu(C_MENU_PROJECT);
	if(psubmenu == NULL)
		return;

	id = psubmenu->GetMenuItemID(C_MENU_PROJECT_CREATE);
	s = ParcelLoadString(STRID_MENU_PROJECT_CREATE);
    psubmenu->ModifyMenu(id, MF_BYCOMMAND, id, s);
	id = psubmenu->GetMenuItemID(C_MENU_PROJECT_EDIT);
	s = ParcelLoadString(STRID_MENU_PROJECT_CHANGE);
    psubmenu->ModifyMenu(id, MF_BYCOMMAND, id, s);
	id = psubmenu->GetMenuItemID(C_MENU_PROJECT_REMOVE);
	s = ParcelLoadString(STRID_MENU_PROJECT_DELETE);
    psubmenu->ModifyMenu(id, MF_BYCOMMAND, id, s);
	id = psubmenu->GetMenuItemID(C_MENU_PROJECT_CREATE_SST);
	s = ParcelLoadString(STRID_MENU_PROJECT_CREATE_SST);
    psubmenu->ModifyMenu(id, MF_BYCOMMAND, id, s);
	id = psubmenu->GetMenuItemID(C_MENU_PROJECT_LOADOLD);
	s = ParcelLoadString(STRID_MENU_PROJECT_CONVERT);
    psubmenu->ModifyMenu(id, MF_BYCOMMAND, id, s);
	id = psubmenu->GetMenuItemID(C_MENU_PROJECT_LOADOLD_SST);
	s = ParcelLoadString(STRID_MENU_PROJECT_CONVERT_SST);
    psubmenu->ModifyMenu(id, MF_BYCOMMAND, id, s);
	id = psubmenu->GetMenuItemID(C_MENU_PROJECT_COPYTOBASE);
	s = ParcelLoadString(STRID_MENU_PROJECT_COPYTOBASE);
    psubmenu->ModifyMenu(id, MF_BYCOMMAND, id, s);
	id = psubmenu->GetMenuItemID(C_MENU_PROJECT_COPYTOBASE_ALL);
	s = ParcelLoadString(STRID_MENU_PROJECT_COPYTOBASE_ALL);
    psubmenu->ModifyMenu(id, MF_BYCOMMAND, id, s);
	id = psubmenu->GetMenuItemID(C_MENU_PROJECT_EXPORT);
	s = ParcelLoadString(STRID_MENU_PROJECT_EXPORTSETTINGS);
    psubmenu->ModifyMenu(id, MF_BYCOMMAND, id, s);
	id = psubmenu->GetMenuItemID(C_MENU_PROJECT_IMPORT);
	s = ParcelLoadString(STRID_MENU_PROJECT_LOADSPECTRA);
    psubmenu->ModifyMenu(id, MF_BYCOMMAND, id, s);
	id = psubmenu->GetMenuItemID(C_MENU_PROJECT_TRANSFER);
	s = GETSTRUI(1159);
    psubmenu->ModifyMenu(id, MF_BYCOMMAND, id, s);

// Menu "Parameter". ������ 3
	s = ParcelLoadString(STRID_MENU_PARAM);
	pMenu->ModifyMenu(C_MENU_PARAM, MF_BYPOSITION, C_MENU_PARAM, s);

	psubmenu = pMenu->GetSubMenu(C_MENU_PARAM);
	if(psubmenu == NULL)
		return;

	id = psubmenu->GetMenuItemID(C_MENU_PARAM_EDIT);
	s = ParcelLoadString(STRID_MENU_PARAM_EDITLIST);
    psubmenu->ModifyMenu(id, MF_BYCOMMAND, id, s);

// Menu "Sample". ������ 4
	s = ParcelLoadString(STRID_MENU_SAMPLE);
	pMenu->ModifyMenu(C_MENU_SAMPLE, MF_BYPOSITION, C_MENU_SAMPLE, s);

	psubmenu = pMenu->GetSubMenu(C_MENU_SAMPLE);
	if(psubmenu == NULL)
		return;

	id = psubmenu->GetMenuItemID(C_MENU_SAMPLE_EDIT);
	s = ParcelLoadString(STRID_MENU_SAMPLE_EDITLIST);
    psubmenu->ModifyMenu(id, MF_BYCOMMAND, id, s);
	id = psubmenu->GetMenuItemID(C_MENU_SAMPLE_CREATE_SST);
	s = ParcelLoadString(STRID_MENU_SAMPLE_CREATE_SST);
    psubmenu->ModifyMenu(id, MF_BYCOMMAND, id, s);
	id = psubmenu->GetMenuItemID(C_MENU_SAMPLE_DELETE_SST);
	s = ParcelLoadString(STRID_MENU_SAMPLE_DELETE_SST);
    psubmenu->ModifyMenu(id, MF_BYCOMMAND, id, s);
	id = psubmenu->GetMenuItemID(C_MENU_SAMPLE_ADDTRANS);
	s = ParcelLoadString(STRID_MENU_SAMPLE_ADDTRANS);
    psubmenu->ModifyMenu(id, MF_BYCOMMAND, id, s);
	id = psubmenu->GetMenuItemID(C_MENU_SAMPLE_EDITTRANS);
	s = ParcelLoadString(STRID_MENU_SAMPLE_EDITTRANS);
    psubmenu->ModifyMenu(id, MF_BYCOMMAND, id, s);
	id = psubmenu->GetMenuItemID(C_MENU_SAMPLE_REMOVETRANS);
	s = ParcelLoadString(STRID_MENU_SAMPLE_REMOVETRANS);
    psubmenu->ModifyMenu(id, MF_BYCOMMAND, id, s);

// Menu "Model". ������ 5
	s = ParcelLoadString(STRID_MENU_MODEL);
	pMenu->ModifyMenu(C_MENU_MODEL, MF_BYPOSITION, C_MENU_MODEL, s);

	psubmenu = pMenu->GetSubMenu(C_MENU_MODEL);
	if(psubmenu == NULL)
		return;

	id = psubmenu->GetMenuItemID(C_MENU_MODEL_CREATE);
	s = ParcelLoadString(STRID_MENU_MODEL_CREATE);
    psubmenu->ModifyMenu(id, MF_BYCOMMAND, id, s);
	id = psubmenu->GetMenuItemID(C_MENU_MODEL_COPY);
	s = ParcelLoadString(STRID_MENU_MODEL_COPY);
    psubmenu->ModifyMenu(id, MF_BYCOMMAND, id, s);
	id = psubmenu->GetMenuItemID(C_MENU_MODEL_EDIT);
	s = ParcelLoadString(STRID_MENU_MODEL_CHANGE);
    psubmenu->ModifyMenu(id, MF_BYCOMMAND, id, s);
	id = psubmenu->GetMenuItemID(C_MENU_MODEL_REMOVE);
	s = ParcelLoadString(STRID_MENU_MODEL_DELETE);
    psubmenu->ModifyMenu(id, MF_BYCOMMAND, id, s);
	id = psubmenu->GetMenuItemID(C_MENU_MODEL_RESULT);
	s = ParcelLoadString(STRID_MENU_MODEL_RESULT);
    psubmenu->ModifyMenu(id, MF_BYCOMMAND, id, s);

// Menu "Method". ������ 6
	s = ParcelLoadString(STRID_MENU_METHOD);
	pMenu->ModifyMenu(C_MENU_METHOD, MF_BYPOSITION, C_MENU_METHOD, s);

	psubmenu = pMenu->GetSubMenu(C_MENU_METHOD);
	if(psubmenu == NULL)
		return;

	id = psubmenu->GetMenuItemID(C_MENU_METHOD_CREATE);
	s = ParcelLoadString(STRID_MENU_METHOD_CREATE);
    psubmenu->ModifyMenu(id, MF_BYCOMMAND, id, s);
	id = psubmenu->GetMenuItemID(C_MENU_METHOD_COPY);
	s = ParcelLoadString(STRID_MENU_METHOD_COPY);
    psubmenu->ModifyMenu(id, MF_BYCOMMAND, id, s);
	id = psubmenu->GetMenuItemID(C_MENU_METHOD_EDIT);
	s = ParcelLoadString(STRID_MENU_METHOD_CHANGE);
    psubmenu->ModifyMenu(id, MF_BYCOMMAND, id, s);
	id = psubmenu->GetMenuItemID(C_MENU_METHOD_REMOVE);
	s = ParcelLoadString(STRID_MENU_METHOD_DELETE);
    psubmenu->ModifyMenu(id, MF_BYCOMMAND, id, s);
	id = psubmenu->GetMenuItemID(C_MENU_METHOD_EXPORT);
	s = ParcelLoadString(STRID_MENU_METHOD_EXPORT);
    psubmenu->ModifyMenu(id, MF_BYCOMMAND, id, s);

// Menu "View". ������ 7
	s = ParcelLoadString(STRID_MENU_VIEW);
	pMenu->ModifyMenu(C_MENU_VIEW, MF_BYPOSITION, C_MENU_VIEW, s);

	psubmenu = pMenu->GetSubMenu(C_MENU_VIEW);
	if(psubmenu == NULL)
		return;
/*
	s = ParcelLoadString(STRID_MENU_VIEW_LANG);
    psubmenu->ModifyMenu(C_MENU_VIEW_LANG, MF_BYPOSITION, C_MENU_VIEW_LANG, s);

	psubsubmenu = psubmenu->GetSubMenu(C_MENU_VIEW_LANG);
	psubsubmenu->DeleteMenu(1, MF_BYPOSITION);
	psubsubmenu->DeleteMenu(0, MF_BYPOSITION);
	s = ParcelLoadString(STRID_MENU_VIEW_LANG_RUS);
	psubsubmenu->AppendMenu(MF_CHECKED | MF_ENABLED, ID_VIEW_LANG_0, s);
	s = ParcelLoadString(STRID_MENU_VIEW_LANG_ENG);
	psubsubmenu->AppendMenu(MF_CHECKED | MF_ENABLED, ID_VIEW_LANG_0 + 1, s);
	psubsubmenu->CheckMenuRadioItem(ID_VIEW_LANG_0, ID_VIEW_LANG_0 + nLang, ID_VIEW_LANG_0 + iLang, MF_BYCOMMAND);
*/
	id = psubmenu->GetMenuItemID(C_MENU_VIEW_TOOLBAR);
	s = ParcelLoadString(STRID_MENU_VIEW_TOOLBAR);
    psubmenu->ModifyMenu(id, MF_BYCOMMAND, id, s);
	id = psubmenu->GetMenuItemID(C_MENU_VIEW_STATUSBAR);
	s = ParcelLoadString(STRID_MENU_VIEW_STATUSBAR);
    psubmenu->ModifyMenu(id, MF_BYCOMMAND, id, s);

// Menu "HELP". ������ 8
	s = ParcelLoadString(STRID_MENU_HELP);
	pMenu->ModifyMenu(C_MENU_HELP, MF_BYPOSITION, C_MENU_HELP, s);

	psubmenu = pMenu->GetSubMenu(C_MENU_HELP);
	if(psubmenu == NULL)
		return;

	id = psubmenu->GetMenuItemID(C_MENU_HELP_TOPIC);
	s = ParcelLoadString(STRID_MENU_HELP_TOPIC);
    psubmenu->ModifyMenu(id, MF_BYCOMMAND, id, s);
	id = psubmenu->GetMenuItemID(C_MENU_HELP_ABOUT);
	s = ParcelLoadString(STRID_MENU_HELP_ABOUT);
    psubmenu->ModifyMenu(id, MF_BYCOMMAND, id, s);
}

LRESULT CMainFrame::OnSetStatusBar(WPARAM wParam, LPARAM lParam)
{
	int Index = (int) wParam;
	TCHAR *pText = (TCHAR*) lParam;
	CString Txt(pText);

	SetStatusBar(Index, Txt);

	return 0;
}

LRESULT CMainFrame::OnOpenProgressBar(WPARAM wParam, LPARAM lParam)
{
	ShowStatusBar(true);

	int Step = (int) wParam, Max = (int) LOWORD(lParam), Min = (int) HIWORD(lParam);
	OpenProgressBar(Min, Max, Step);

	return 0;
}

LRESULT CMainFrame::OnCloseProgressBar(WPARAM, LPARAM)
{
	CloseProgressBar();
	ShowStatusBar(true);

	return 0;
}

LRESULT CMainFrame::OnSetProgressBar(WPARAM wParam, LPARAM lParam)
{
	BOOL IsPosition = (BOOL) wParam;

	if(!IsPosition)
		SetProgressBar();
	else
	{
		int Position = (int) lParam;
		SetProgressBar(Position);
	}

	return 0;
}

void CMainFrame::OnBaseNew()
{
	pView->ExecuteBaseNew(this);
}

void CMainFrame::OnUpdateBaseNew(CCmdUI *pCmdUI)
{
	if(pCmdUI == 0)  return;

	pCmdUI->Enable(true);
}

void CMainFrame::OnBaseLoad()
{
	pView->ExecuteBaseLoad(this);
}

void CMainFrame::OnUpdateBaseLoad(CCmdUI *pCmdUI)
{
	if(pCmdUI == 0)  return;

	pCmdUI->Enable(true);
}

void CMainFrame::OnBaseClose()
{
	pView->ExecuteBaseClose();
}

void CMainFrame::OnUpdateBaseClose(CCmdUI *pCmdUI)
{
	if(pCmdUI == 0)  return;

	pCmdUI->Enable(pRoot->Database.IsOpen());
}

void CMainFrame::OnBaseDelete()
{
	pView->ExecuteBaseDelete();
}

void CMainFrame::OnUpdateBaseDelete(CCmdUI *pCmdUI)
{
	if(pCmdUI == 0)  return;

	pCmdUI->Enable(pRoot->Database.IsOpen());
}

void CMainFrame::OnBaseCopy()
{
	pView->ExecuteBaseCopy(this);
}

void CMainFrame::OnUpdateBaseCopy(CCmdUI *pCmdUI)
{
	if(pCmdUI == 0)  return;

	pCmdUI->Enable(pRoot->Database.IsOpen());
}

void CMainFrame::OnCreateNewDevice()
{
	pView->CreateNewDevice();
}

void CMainFrame::OnUpdateCreateNewDevice(CCmdUI *pCmdUI)
{
	if(pCmdUI == 0)  return;

	pCmdUI->Enable(pRoot->Database.IsOpen() && pView->IsCreateDeviceEnable());
}

void CMainFrame::OnChangeDeviceSettings()
{
	pView->ChangeDeviceSettings();
}

void CMainFrame::OnUpdateChangeDeviceSettings(CCmdUI *pCmdUI)
{
	if(pCmdUI == 0)  return;

	pCmdUI->Enable(pView->IsChangeDeviceSettingsEnable());
}

void CMainFrame::OnDeleteDevice()
{
	pView->DeleteDevice();
}

void CMainFrame::OnUpdateDeleteDevice(CCmdUI *pCmdUI)
{
	if(pCmdUI == 0)  return;

	pCmdUI->Enable(pView->IsDeleteDeviceEnable());
}

void CMainFrame::OnCopyAllProjectsToBase()
{
	pView->CopyDeviceToBase(this);
}

void CMainFrame::OnUpdateCopyAllProjectsToBase(CCmdUI *pCmdUI)
{
	if(pCmdUI == 0)  return;

	pCmdUI->Enable(pView->IsCopyDeviceToBaseEnable());
}

void CMainFrame::OnCreateNewProject()
{
	pView->CreateNewProject();
}

void CMainFrame::OnUpdateCreateNewProject(CCmdUI *pCmdUI)
{
	if(pCmdUI == 0)  return;

	pCmdUI->Enable(pView->IsCreateProjectEnable());
}

void CMainFrame::OnChangeProjectSettings()
{
	pView->ChangeProjectSettings();
}

void CMainFrame::OnUpdateChangeProjectSettings(CCmdUI *pCmdUI)
{
	if(pCmdUI == 0)  return;

	pCmdUI->Enable(pView->IsChangeProjectSettingsEnable());
}

void CMainFrame::OnDeleteProject()
{
	pView->DeleteProject();
}

void CMainFrame::OnUpdateDeleteProject(CCmdUI *pCmdUI)
{
	if(pCmdUI == 0)  return;

	pCmdUI->Enable(pView->IsDeleteProjectEnable());
}

void CMainFrame::OnCreateSSTProject()
{
	pView->CreateSSTProject();
}

void CMainFrame::OnUpdateCreateSSTProject(CCmdUI *pCmdUI)
{
	if(pCmdUI == 0)  return;

	pCmdUI->Enable(pView->IsCreateSSTProjectEnable());
}

void CMainFrame::OnLoadOldProject()
{
	pView->LoadOldProject(this);
}

void CMainFrame::OnUpdateLoadOldProject(CCmdUI *pCmdUI)
{
	if(pCmdUI == 0)  return;

	pCmdUI->Enable(pView->IsLoadOldProjectEnable());
}

void CMainFrame::OnLoadOldSSTProject()
{
	pView->LoadOldSSTProject(this);
}

void CMainFrame::OnUpdateLoadOldSSTProject(CCmdUI *pCmdUI)
{
	if(pCmdUI == 0)  return;

	pCmdUI->Enable(pView->IsLoadOldSSTProjectEnable());
}

void CMainFrame::OnCopyProjectToBase()
{
	pView->CopyProjectToBase(this);
}

void CMainFrame::OnUpdateCopyProjectToBase(CCmdUI *pCmdUI)
{
	if(pCmdUI == 0)  return;

	pCmdUI->Enable(pView->IsCopyProjectToBaseEnable());
}

void CMainFrame::OnExportProjectSettings()
{
	pView->ExportProjectSettings(this);
}

void CMainFrame::OnUpdateExportProjectSettings(CCmdUI *pCmdUI)
{
	if(pCmdUI == 0)  return;

	pCmdUI->Enable(pView->IsExportProjectSettingsEnable());
}

void CMainFrame::OnImportSpectra()
{
	pView->ImportSpectra(this);
}

void CMainFrame::OnUpdateImportSpectra(CCmdUI *pCmdUI)
{
	if(pCmdUI == 0)  return;

	pCmdUI->Enable(pView->IsImportSpectraEnable());
}

void CMainFrame::OnEditParamList()
{
	pView->EditParamList();
}

void CMainFrame::OnUpdateEditParamList(CCmdUI *pCmdUI)
{
	if(pCmdUI == 0)  return;

	pCmdUI->Enable(pView->IsEditParamListEnable());
}

void CMainFrame::OnEditSampleList()
{
	pView->EditSampleList();
}

void CMainFrame::OnUpdateEditSampleList(CCmdUI *pCmdUI)
{
	if(pCmdUI == 0)  return;

	pCmdUI->Enable(pView->IsEditSampleListEnable());
}

void CMainFrame::OnCreateSSTTransfer()
{
	pView->CreateSSTTransfer();
}

void CMainFrame::OnUpdateCreateSSTTransfer(CCmdUI *pCmdUI)
{
	if(pCmdUI == 0)  return;

	pCmdUI->Enable(pView->IsCreateSSTTransferEnable());
}

void CMainFrame::OnDeleteSSTTransfer()
{
	pView->DeleteSSTTransfer();
}

void CMainFrame::OnUpdateDeleteSSTTransfer(CCmdUI *pCmdUI)
{
	if(pCmdUI == 0)  return;

	pCmdUI->Enable(pView->IsDeleteSSTTransferEnable());
}

void CMainFrame::OnAddTransfer()
{
	pView->AddTransfer();
}

void CMainFrame::OnUpdateAddTransfer(CCmdUI *pCmdUI)
{
	if(pCmdUI == 0)  return;

	pCmdUI->Enable(pView->IsAddTransferEnable());
}

void CMainFrame::OnProjectTransfer()
{
	pView->ProjectTransfer();
}

void CMainFrame::OnUpdateProjectTransfer(CCmdUI *pCmdUI)
{
	if(pCmdUI == 0)  return;

	pCmdUI->Enable(pView->IsProjectTransferEnable());
}

void CMainFrame::OnEditTransfer()
{
	pView->EditTransfer();
}

void CMainFrame::OnUpdateEditTransfer(CCmdUI *pCmdUI)
{
	if(pCmdUI == 0)  return;

	pCmdUI->Enable(pView->IsEditTransferEnable());
}

void CMainFrame::OnDeleteTransfer()
{
	pView->DeleteTransfer();
}

void CMainFrame::OnUpdateDeleteTransfer(CCmdUI *pCmdUI)
{
	if(pCmdUI == 0)  return;

	pCmdUI->Enable(pView->IsDeleteTransferEnable());
}

void CMainFrame::OnCreateNewModel()
{
	pView->CreateNewModel();
}

void CMainFrame::OnUpdateCreateNewModel(CCmdUI *pCmdUI)
{
	if(pCmdUI == 0)  return;

	pCmdUI->Enable(pView->IsCreateModelEnable());
}

void CMainFrame::OnCopyModel()
{
	pView->CreateNewModel(COMMAND_MODE_COPY);
}

void CMainFrame::OnUpdateCopyModel(CCmdUI *pCmdUI)
{
	if(pCmdUI == 0)  return;

	pCmdUI->Enable(pView->IsCopyModelEnable());
}

void CMainFrame::OnChangeModelSettings()
{
	pView->ChangeModelSettings();
}

void CMainFrame::OnUpdateChangeModelSettings(CCmdUI *pCmdUI)
{
	if(pCmdUI == 0)  return;

	pCmdUI->Enable(pView->IsChangeModelSettingsEnable());
}

void CMainFrame::OnDeleteModel()
{
	pView->DeleteModel();
}

void CMainFrame::OnUpdateDeleteModel(CCmdUI *pCmdUI)
{
	if(pCmdUI == 0)  return;

	pCmdUI->Enable(pView->IsDeleteModelEnable());
}

void CMainFrame::OnModelResult()
{
	pView->ModelResult();
}

void CMainFrame::OnUpdateModelResult(CCmdUI *pCmdUI)
{
	if(pCmdUI == 0)  return;

	pCmdUI->Enable(pView->IsModelResultEnable());
}

void CMainFrame::OnCreateNewMethod()
{
	pView->CreateNewMethod();
}

void CMainFrame::OnUpdateCreateNewMethod(CCmdUI *pCmdUI)
{
	if(pCmdUI == 0)  return;

	pCmdUI->Enable(pView->IsCreateMethodEnable());
}

void CMainFrame::OnCopyMethod()
{
	pView->CreateNewMethod(COMMAND_MODE_COPY);
}

void CMainFrame::OnUpdateCopyMethod(CCmdUI *pCmdUI)
{
	if(pCmdUI == 0)  return;

	pCmdUI->Enable(pView->IsCopyMethodEnable());
}

void CMainFrame::OnChangeMethodSettings()
{
	pView->ChangeMethodSettings();
}

void CMainFrame::OnUpdateChangeMethodSettings(CCmdUI *pCmdUI)
{
	if(pCmdUI == 0)  return;

	pCmdUI->Enable(pView->IsChangeMethodSettingsEnable());
}

void CMainFrame::OnDeleteMethod()
{
	pView->DeleteMethod();
}

void CMainFrame::OnUpdateDeleteMethod(CCmdUI *pCmdUI)
{
	if(pCmdUI == 0)  return;

	pCmdUI->Enable(pView->IsDeleteMethodEnable());
}

void CMainFrame::OnExportMethod()
{
	pView->ExportMethod(this);
}

void CMainFrame::OnUpdateExportMethod(CCmdUI *pCmdUI)
{
	if(pCmdUI == 0)  return;

	pCmdUI->Enable(pView->IsExportMethodEnable());
}

void CMainFrame::OnChangeLanguage(UINT nID)
{
	CMenu *pMenu = GetMenu();
	if(pMenu == NULL)
		return;
	CMenu *psubmenu = pMenu->GetSubMenu(C_MENU_VIEW);
	if(psubmenu == NULL)
		return;
	CMenu *psubsubmenu = psubmenu->GetSubMenu(C_MENU_VIEW_LANG);
	if(psubsubmenu == NULL)
		return;

	psubsubmenu->CheckMenuRadioItem(ID_VIEW_LANG_0, ID_VIEW_LANG_0 + nSorts, nID, MF_BYCOMMAND);

	if(iLang != int(nID - ID_VIEW_LANG_0))
	{
		iLang = nID - ID_VIEW_LANG_0;
		pView->ChangeLanguage(iLang);
	}
}

void CMainFrame::OnUpdateChangeLanguage(CCmdUI *pCmdUI)
{
	if(pCmdUI == 0)  return;

	pCmdUI->Enable(false);
}

void CMainFrame::OnHelpFinder()
{
	if(GetForegroundWindow() != this)
	{
		return;
	}

	if(!IsExistHelp())
		return;

	ShowHelp(DLGID_MAIN);
}

void CMainFrame::OnUpdateHelpFinder(CCmdUI *pCmdUI)
{
	if(pCmdUI == 0)  return;

	pCmdUI->Enable(filesystem::IsExistFile(HelpFileName));
}

bool CMainFrame::IsExistHelp()
{
	return filesystem::IsExistFile(HelpFileName);
}

void CMainFrame::ShowHelp(int IdDlg)
{
	CString NamePage = HelpFileName + cGetHelpPageName(IdDlg);

	HWND hHelpWnd = ::HtmlHelp(this->m_hWnd, NamePage, HH_DISPLAY_TOPIC, NULL);
	::ShowWindow(hHelpWnd, SW_MAXIMIZE);
}

void CMainFrame::SetStatusBar(CString text)
{
	SetStatusBar(0, text);
}

void CMainFrame::SetStatusBar(int Index, CString text)
{
	m_wndStatusBar.SetPaneText(Index, text);
}

void CMainFrame::ShowStatusBar(BOOL IsShow, BOOL IsDelay)
{
	pFrame->ShowControlBar(&m_wndStatusBar, IsShow, IsDelay);
}

void CMainFrame::OpenProgressBar(int min, int max, int step)
{
	if(pProgressBar != 0)
	{
		delete pProgressBar;
		pProgressBar = NULL;
	}
	pProgressBar = new CProgressCtrl();

	DWORD Style = WS_CHILD | WS_VISIBLE;
	CRect Rect;

	m_wndStatusBar.GetClientRect(&Rect);

	if(!pProgressBar->Create(Style, Rect, &m_wndStatusBar, 0))
	{
		delete pProgressBar;
		pProgressBar = NULL;
	}
	else
	{
		pProgressBar->SetRange((short)min, (short)max);
		pProgressBar->SetStep(step);
		pProgressBar->SetPos(min);
	}
}

void CMainFrame::CloseProgressBar()
{
	if(pProgressBar != 0)
	{
		delete pProgressBar;
		pProgressBar = NULL;
	}
}

void CMainFrame::SetProgressBar(int pos)
{
	if(pProgressBar != 0)
		pProgressBar->SetPos(pos);
}

void CMainFrame::SetProgressBar()
{
	if(pProgressBar != NULL)
		pProgressBar->StepIt();
}

bool CMainFrame::LoadInit()
{
	TCHAR buf[PATH_LENGTH];
	GetCurrentDirectory(PATH_LENGTH, buf);
	CString s(buf);

	WorkDirectory = s;
	HelpDirectory = s + _T("\\Help");
/*
	CDfxCache dfx(cFini);
	iLang = dfx.GetProfileInt(cFini, _T("ViewSettings"), _T("Language"), 0);
	BaseName = dfx.GetProfileString(cFini, _T("FileNames"), _T("Base"), _T(""));
	TemplateOptimName = dfx.GetProfileString(cFini, _T("FileNames"), _T("TemplateOptim"), _T(""));
	TemplateQntName = dfx.GetProfileString(cFini, _T("FileNames"), _T("TemplateQnt"), _T("Template.wtl"));
	TemplateQltName = dfx.GetProfileString(cFini, _T("FileNames"), _T("TemplateQlt"), _T("Template.wtl"));
*/

	CString str;

	BaseName = L"";
	if(theApp.GetRegString(L"FileNames", L"Base", str))
	{
		if(PathFileExists(str)) BaseName = str;
	}

	TemplateOptimName = L"";
	if(theApp.GetRegString(L"FileNames", L"TemplateOptim", str))
	{
		TemplateOptimName = str;
	}

	TemplateQntName = L"Template.wtl";
	if(theApp.GetRegString(L"FileNames", L"TemplateQnt", str))
	{
		TemplateQntName = str;
	}

	TemplateQntName = L"Template.wtl";
	if(theApp.GetRegString(L"FileNames", L"TemplateQlt", str))
	{
		TemplateQltName = str;
	}

	iLang = 0;
	theApp.GetRegInt(L"ViewSettings", L"iLang", &iLang);
	SetHelpFile(iLang);

	return true;
}

void CMainFrame::SaveInit()
{
	SetCurrentDirectory(WorkDirectory);
/*
	CDfxCache dfx(cFini);
	dfx.DfxPutProfileInt(cFini, _T("ViewSettings"), _T("Language"), iLang);
	dfx.DfxPutProfileString(cFini, _T("FileNames"), _T("Base"), pView->Data.GetBaseFileName());
	dfx.DfxPutProfileString(cFini, _T("FileNames"), _T("TemplateOptim"), pView->Data.Templates.GetBaseName());
	dfx.DfxPutProfileString(cFini, _T("FileNames"), _T("TemplateQnt"), pRoot->GetTemplateName(C_TEMPLATE_QNT));
	dfx.DfxPutProfileString(cFini, _T("FileNames"), _T("TemplateQlt"), pRoot->GetTemplateName(C_TEMPLATE_QLT));
*/
	CString str;

	str = pView->Data.GetBaseFileName();
	theApp.PutRegString(L"FileNames", L"Base", str);

	str = pView->Data.Templates.GetBaseName();
	theApp.PutRegString(L"FileNames", L"TemplateOptim", str);

	str = pRoot->GetTemplateName(C_TEMPLATE_QNT);
	theApp.PutRegString(L"FileNames", L"TemplateQnt", str);

	str = pRoot->GetTemplateName(C_TEMPLATE_QLT);
	theApp.PutRegString(L"FileNames", L"TemplateQlt", str);

	theApp.PutRegInt(L"ViewSettings", L"iLang", iLang);
}

void CMainFrame::SetHelpFile(int ilang)
{
	HelpFileName = HelpDirectory + _T("\\");
	switch(ilang)
	{
	case C_LANG_RUSSIAN:  HelpFileName += cFhelpRU;  break;
	case C_LANG_ENGLISH:  HelpFileName += cFhelpEN;  break;
	}
}

CString CMainFrame::GetLanguage()
{
	switch(iLang)
	{
	case C_LANG_RUSSIAN:  return _T("russian");
	case C_LANG_ENGLISH:  return _T("english");
	}

	return cEmpty;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.style = WS_OVERLAPPED | WS_CAPTION | FWS_ADDTOTITLE
		 | WS_THICKFRAME | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_MAXIMIZE | WS_SYSMENU;

	return TRUE;
}


#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif
