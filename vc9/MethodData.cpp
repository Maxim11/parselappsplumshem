#include "stdafx.h"

#include "BranchInfo.h"
#include "MethodData.h"


void AnalyseRule::Copy(AnalyseRule& rule)
{
	Name = rule.Name;
	MainModel = rule.MainModel;
	Moisture = rule.Moisture;
	UseStdMoisture = rule.UseStdMoisture;

	AddModels.clear();
	copy(rule.AddModels.begin(), rule.AddModels.end(), inserter(AddModels, AddModels.begin()));
}

CString AnalyseRule::GetStdMoistureStr()
{
	CString s;

	if(UseStdMoisture)
		s.Format(cFmt31, Moisture);
	else
		s = ParcelLoadString(STRID_FIELD_NA);

	return s;
}

void QltFormula::Copy(QltFormula& data)
{
	Data.clear();
	copy(data.Data.begin(), data.Data.end(), inserter(Data, Data.begin()));
}

int QltFormula::CheckFormula(int nMod)
{
	int N = int(Data.size());
	if(N <= 0)
		return C_MTD_QLTFORM_ERR_EMPTY;

	int i;
	MapIntBool Models;
	for(i=0; i<nMod; i++)
		Models[i] = false;

	int nBr = 0;
	bool bOper = false;
	bool isMod = false;
	ItQFE it;
	for(it=Data.begin(); it!=Data.end(); ++it)
	{
		switch(it->Type)
		{
		case C_MTD_QLTFORM_MEAN:
			if(it->ModelInd < 0 && it->ModelInd >= N)
				return C_MTD_QLTFORM_ERR_MANYMODELS;
			Models[it->ModelInd] = true;
			isMod = true;
			bOper = false;
			break;
		case C_MTD_QLTFORM_AND:
		case C_MTD_QLTFORM_OR:
			if(bOper)
				return C_MTD_QLTFORM_ERR_DBLOPER;
			bOper = true;
			break;
		case C_MTD_QLTFORM_OPEN:  nBr++;  bOper = false;  break;
		case C_MTD_QLTFORM_CLOSE:  nBr--;  bOper = false;  break;
		}

		if(nBr != 0 && nBr != 1)
			return C_MTD_QLTFORM_ERR_DBLBR;
	}
	if(!isMod)
		return C_MTD_QLTFORM_ERR_NOMODELS;
	for(i=0; i<nMod; i++)
		if(!Models[i])
			return C_MTD_QLTFORM_ERR_NOTALLMODELS;
	if(nBr != 0)
		return C_MTD_QLTFORM_ERR_DBLBR;
	if(bOper)
		return C_MTD_QLTFORM_ERR_BADEND;

	return C_MTD_QLTFORM_ERR_OK;
}

CString QltFormula::GetCheckFormulaResult(int nMod)
{
	return cGetQltFormulaError(CheckFormula(nMod));
}

bool QltFormula::IsNextElementEnable(int elem)
{
	int len = int(Data.size());
	int prev = (len <= 0) ? -1 : Data[len-1].Type;
	int nBr = 0;
	int nMod = 0;

	ItQFE it;
	for(it=Data.begin(); it!=Data.end(); ++it)
	{
		switch(it->Type)
		{
		case C_MTD_QLTFORM_MEAN:  nMod++;  break;
		case C_MTD_QLTFORM_OPEN:  nBr++;  break;
		case C_MTD_QLTFORM_CLOSE:  nBr--;  break;
		}
	}

	if(nMod >= 7 && elem != C_MTD_QLTFORM_CLOSE)
		return false;

	bool res = false;
	switch(elem)
	{
	case C_MTD_QLTFORM_MEAN:
		res = (len <= 0 || prev == C_MTD_QLTFORM_AND || prev == C_MTD_QLTFORM_OR || prev == C_MTD_QLTFORM_OPEN);
		break;
	case C_MTD_QLTFORM_AND:
	case C_MTD_QLTFORM_OR:  res = (prev == C_MTD_QLTFORM_MEAN || prev == C_MTD_QLTFORM_CLOSE);  break;
	case C_MTD_QLTFORM_OPEN:
		res = ((nBr == 0) && (len <= 0 || prev == C_MTD_QLTFORM_AND || prev == C_MTD_QLTFORM_OR));
		break;
	case C_MTD_QLTFORM_CLOSE:
		res = ((nBr == 1) && (prev == C_MTD_QLTFORM_MEAN));
		break;
	}

	return res;
}

CString QltFormula::GetAsString()
{
	CString str = cEmpty;

	ItQFE it;
	for(it=Data.begin(); it!=Data.end(); ++it)
	{
		CString s1 = cEmpty;
		switch(it->Type)
		{
		case C_MTD_QLTFORM_MEAN:  s1.Format(ParcelLoadString(STRID_FORMULA_ELEMENT_MEAN), it->ModelInd + 1);  break;
		case C_MTD_QLTFORM_AND:  s1 = ParcelLoadString(STRID_FORMULA_ELEMENT_AND);  break;
		case C_MTD_QLTFORM_OR:  s1 = ParcelLoadString(STRID_FORMULA_ELEMENT_OR);  break;
		case C_MTD_QLTFORM_OPEN:  s1 = ParcelLoadString(STRID_FORMULA_ELEMENT_OPEN);  break;
		case C_MTD_QLTFORM_CLOSE:  s1 = ParcelLoadString(STRID_FORMULA_ELEMENT_CLOSE);  break;
		}
		str += s1;
	}

	return str;
}

void QltFormula::Clear()
{
	Data.clear();
}

bool QltFormula::AddElement(int nMod, int elem, int iMod)
{
	if(elem == C_MTD_QLTFORM_MEAN && (iMod < 0 || iMod >= nMod))
		return false;
	if(!IsNextElementEnable(elem))
		return false;

	QltFormulaElement NewElem(elem, iMod);
	Data.push_back(NewElem);

	return true;
}

void QltFormula::DeleteLast()
{
	if(int(Data.size()) <= 0)
		return;

	ItQFE it = Data.end() - 1;
	Data.erase(it);
}

int QltFormula::GetLength()
{
	return int(Data.size());
}

CMethodData::CMethodData(HTREEITEM hParent)
{
	hItem = NULL;
	hParentItem = hParent;

	Name = ParcelLoadString(STRID_DEFNAME_MTD);
	SetDate();

	CProjectData *Prj = GetProjectByHItem(hParentItem);

	AnalysisType = C_MTD_ANALYSIS_QNT;

	MoisParam = "";
	LowTemp = Prj->GetLowLimTemp();
	UpperTemp = Prj->GetUpperLimTemp();
	MaxDiffTemp = Prj->GetMaxDiffTemp();
}

CMethodData::~CMethodData()
{
	Rules.clear();
	ModelQltNames.clear();
}

void CMethodData::Copy(CMethodData& data)
{
	int i;

	hItem = data.GetHItem();
	hParentItem = data.GetParentHItem();

	Name = data.GetName();
	Date = data.GetDate();

	AnalysisType = data.GetAnalysisType();

	MoisParam = data.GetMoisParam();
	LowTemp = data.GetLowTemp();
	UpperTemp = data.GetUpperTemp();
	MaxDiffTemp = data.GetMaxDiffTemp();

	ModelQltNames.clear();
	copy(data.ModelQltNames.begin(), data.ModelQltNames.end(), inserter(ModelQltNames, ModelQltNames.begin()));

	Formula.Copy(data.Formula);

	Rules.clear();
	for(i=0; i<data.GetNRules(); i++)
	{
		AnalyseRule *OldRule = data.GetRule(i);
		AnalyseRule NewRule(OldRule->Name);
		NewRule.Copy(*OldRule);
		Rules.push_back(NewRule);
	}
}

HTREEITEM CMethodData::GetHItem()
{
	return hItem;
}

void CMethodData::SetHItem(HTREEITEM item)
{
	hItem = item;
}

HTREEITEM CMethodData::GetParentHItem()
{
	return hParentItem;
}

void CMethodData::SetParentHItem(HTREEITEM item)
{
	hParentItem = item;
}

CString CMethodData::GetName()
{
	return Name;
}

void CMethodData::SetName(CString name)
{
	Name = name;
}

CTime CMethodData::GetDate()
{
	return Date;
}

void CMethodData::SetDate()
{
	Date = CTime::GetCurrentTime();
}

CString CMethodData::GetMoisParam()
{
	return MoisParam;
}

void CMethodData::SetMoisParam(CString param)
{
	MoisParam = param;
}

int CMethodData::GetLowTemp()
{
	return LowTemp;
}

void CMethodData::SetLowTemp(int temp)
{
	LowTemp = temp;
}

int CMethodData::GetUpperTemp()
{
	return UpperTemp;
}

void CMethodData::SetUpperTemp(int temp)
{
	UpperTemp = temp;
}

int CMethodData::GetMaxDiffTemp()
{
	return MaxDiffTemp;
}

void CMethodData::SetMaxDiffTemp(int temp)
{
	MaxDiffTemp = temp;
}

int CMethodData::GetAnalysisType()
{
	return AnalysisType;
}

bool CMethodData::SetAnalysisType(int ind)
{
	if(ind < C_ANALYSIS_QNT || ind > C_ANALYSIS_QLT)
		return false;

	AnalysisType = ind;

	return true;
}

int CMethodData::GetNRules()
{
	return int(Rules.size());
}

AnalyseRule* CMethodData::GetRule(int ind)
{
	if(ind < 0 || ind >= int(Rules.size()))
		return NULL;

	return &(Rules[ind]);
}

AnalyseRule* CMethodData::GetRule(CString name)
{
	ItRul it;
	for(it=Rules.begin(); it!=Rules.end(); ++it)
		if(!(*it).Name.Compare(name))
			return &(*it);

	return NULL;
}

bool CMethodData::IsMethodRuleExist(CString name)
{
	ItRul it;
	for(it=Rules.begin(); it!=Rules.end(); ++it)
		if(!(*it).Name.Compare(name))
			return true;

	return false;
}

// ������������ �� ������ � ������
bool CMethodData::IsQntModelUsed(CString name)
{
	ItRul itR;
	for(itR=Rules.begin(); itR != Rules.end(); itR++)
	{
		if(!(*itR).MainModel.Compare(name)) return true;

		ItStr itS;
		for(itS=(*itR).AddModels.begin(); itS!=(*itR).AddModels.end(); itS++)
		{
			if(!(*itS).Compare(name)) return true;
		}
	}
	return false;
}

int CMethodData::GetQntModelNames(VStr& names)
{
	names.clear();
	ItRul itR;
	for(itR=Rules.begin(); itR != Rules.end(); itR++)
	{

		if((*itR).MainModel != L"")
		{
			bool bFound = false;
			int n = names.size();
			for(int i=0; i<n; i++)
			{
				if(!(*itR).MainModel.Compare(names[i]))
				{
					bFound = true;
					break;
				}
			}
			if(!bFound) names.push_back((*itR).MainModel);
		}

		ItStr itS;
		for(itS=(*itR).AddModels.begin(); itS!=(*itR).AddModels.end(); itS++)
		{
			if((*itS) != L"")
			{
				bool bFound = false;
				int n = names.size();
				for(int i=0; i<n; i++)
				{
					if(!(*itS).Compare(names[i]))
					{
						bFound = true;
						break;
					}
				}
				if(!bFound) names.push_back(*itS);
			}
		}
	}

	return names.size();
}

bool CMethodData::AddRule(AnalyseRule& rule)
{
	AnalyseRule *pRule = GetRule(rule.Name);
	if(pRule != NULL)
	{
		pRule->MainModel = rule.MainModel;
		pRule->AddModels.clear();
		copy(rule.AddModels.begin(), rule.AddModels.end(), inserter(pRule->AddModels, pRule->AddModels.begin()));
		pRule->Moisture = rule.Moisture;
		pRule->UseStdMoisture = rule.UseStdMoisture;
		return false;
	}

	Rules.push_back(rule);

	return true;
}

bool CMethodData::DeleteRule(int ind)
{
	if(ind < 0 || ind >= int(Rules.size()))
		return false;

	ItRul it = Rules.begin() + ind;

	Rules.erase(it);

	return true;
}

bool CMethodData::DeleteRule(CString name)
{
	ItRul it;
	for(it=Rules.begin(); it!=Rules.end(); ++it)
		if(!(*it).Name.Compare(name))
		{
			Rules.erase(it);
			return true;
		}

	return false;
}


int CMethodData::GetNModelQltNames()
{
	return int(ModelQltNames.size());
}

CString CMethodData::GetModelQltName(int ind)
{
	if(ind < 0 || ind >= int(ModelQltNames.size()))
		return NULL;

	return ModelQltNames[ind];
}

// ������������ �� Qlt-������ � ������
bool CMethodData::IsQltModelUsed(CString name)
{
	ItIntStr it;
	for(it=ModelQltNames.begin(); it!=ModelQltNames.end(); ++it)
	{
		if(!it->second.Compare(name))
		{
			return true;
		}
	}
	return false;
}

bool CMethodData::AddModelQltName(int ind, CString name)
{
	ModelQltNames[ind] = name;

	return true;
}

bool CMethodData::DeleteModelQltName(int ind)
{
	if(ind < 0 || ind >= int(ModelQltNames.size()))
		return false;

	ModelQltNames.erase(ind);

	return true;
}

bool CMethodData::DeleteModelQltName(CString name)
{
	ItIntStr it;
	for(it=ModelQltNames.begin(); it!=ModelQltNames.end(); ++it)
		if(!it->second.Compare(name))
		{
			ModelQltNames.erase(it->first);
			return true;
		}

	return false;
}

void CMethodData::DeleteAllModelQltNames()
{
	ModelQltNames.clear();
}

void CMethodData::GetDataForBase(prdb::PRMethod& Obj)
{
	Obj.MethodName = Name;
	Obj.DateCreated = Date;
	Obj.AnalisisType = AnalysisType;
	Obj.IndexAsHum = MoisParam;
	Obj.TempRangeRealDown = LowTemp;
	Obj.TempRangeRealUp = UpperTemp;
	Obj.TempDiff = MaxDiffTemp;

	Obj.QltModels.clear();
	copy(ModelQltNames.begin(), ModelQltNames.end(), inserter(Obj.QltModels, Obj.QltModels.begin()));

	Obj.Formula.clear();
	ItQFE itQ;
	for(itQ=Formula.Data.begin(); itQ!=Formula.Data.end(); ++itQ)
	{
		prdb::PRMethod::PRMTElement NewElem;
		NewElem.Type = itQ->Type;
		NewElem.ModelInd = itQ->ModelInd;

		Obj.Formula.push_back(NewElem);
	}

	Obj.Index.clear();
	ItRul it;
	for(it=Rules.begin(); it!=Rules.end(); ++it)
	{
		prdb::PRMethod::PRMTParam NewRule;
		NewRule.IndexName = it->Name;
		NewRule.Moisture = it->Moisture;
		NewRule.UseStdMoisture = it->UseStdMoisture;
		NewRule.MainModel = it->MainModel;
		copy(it->AddModels.begin(), it->AddModels.end(), inserter(NewRule.AddModels, NewRule.AddModels.begin()));
		Obj.Index.push_back(NewRule);
	}
}

void CMethodData::SetDataFromBase(prdb::PRMethod& Obj)
{
	Name = Obj.MethodName;
	Date = Obj.DateCreated;
	AnalysisType = Obj.AnalisisType;
	MoisParam = Obj.IndexAsHum;
	LowTemp = Obj.TempRangeRealDown;
	UpperTemp = Obj.TempRangeRealUp;
	MaxDiffTemp = Obj.TempDiff;

	ModelQltNames.clear();
	copy(Obj.QltModels.begin(), Obj.QltModels.end(), inserter(ModelQltNames, ModelQltNames.begin()));

	Formula.Clear();
	vector<prdb::PRMethod::PRMTElement>::iterator itQ;
	for(itQ=Obj.Formula.begin(); itQ!=Obj.Formula.end(); ++itQ)
	{
		QltFormulaElement NewElem(itQ->Type, itQ->ModelInd);

		Formula.Data.push_back(NewElem);
	}

	Rules.clear();
	int i, N = int(Obj.Index.size());
	for(i=0; i<N; i++)
	{
		AnalyseRule NewRule(Obj.Index[i].IndexName);
		NewRule.Moisture = Obj.Index[i].Moisture;
		NewRule.UseStdMoisture = Obj.Index[i].UseStdMoisture;
		NewRule.MainModel = Obj.Index[i].MainModel;
		copy(Obj.Index[i].AddModels.begin(), Obj.Index[i].AddModels.end(),
			inserter(NewRule.AddModels, NewRule.AddModels.begin()));

		Rules.push_back(NewRule);
	}
}

bool CMethodData::GetDataForExportQnt(mtddb::PRMethodQnt& Obj)
{
	if(AnalysisType != C_ANALYSIS_QNT)
		return false;

	CProjectData *ParentPrj = GetProjectByHItem(hParentItem);
	CDeviceData *ParentDev = GetDeviceByHItem(ParentPrj->GetParentHItem());

	Obj.MethodName = Name;
	Obj.DateCreated = Date;
	Obj.MoisParam = MoisParam;
	Obj.DevNum = ParentDev->GetNumber();
	Obj.DevType = cGetDeviceID0(ParentDev->GetType());
	Obj.SpecRangeLow = ParentPrj->GetLowLimSpecRange();
	Obj.SpecRangeUpper = ParentPrj->GetUpperLimSpecRange();
	Obj.ResSub = ParentPrj->GetSubCanalResolution();
	Obj.ApodSub = ParentPrj->GetSubCanalApodization();
	Obj.ZeroFill = ParentPrj->GetZeroFillingInd();
	Obj.UseRef = ParentPrj->IsUseRefCanal();
	Obj.ResRef = ParentPrj->GetRefCanalResolution();
	Obj.ApodRef = ParentPrj->GetRefCanalApodization();
	Obj.SmplScan = ParentPrj->GetNScansSubsample();
	Obj.SmplMeas = ParentPrj->GetNMeasuresSubsample();
	Obj.StdScan = ParentPrj->GetNScansStandart();
	Obj.StdMeas = ParentPrj->GetNMeasuresStandart();
	Obj.Xtransform = true;
	Obj.UseBath = !((ParentDev->GetType() == C_DEV_TYPE_40 || ParentDev->GetType() == C_DEV_TYPE_12M) && ParentPrj->IsSST());
	Obj.BathLength = ParentPrj->GetBathLength();
	Obj.IsSST = ParentPrj->IsSST();
	Obj.TempLow = LowTemp;
	Obj.TempUpper = UpperTemp;
	Obj.TempDiff = MaxDiffTemp;

	Obj.MtdModels.clear();
	Obj.MtdParams.clear();
	ItRul it;
	for(it=Rules.begin(); it!=Rules.end(); ++it)
	{
		CParamData* Param = ParentPrj->GetParam(it->Name);

		mtddb::PRMtdParam NewMtdPar;

		NewMtdPar.ParamName = it->Name;
		NewMtdPar.Unit = Param->GetUnit();
		NewMtdPar.Format = Param->GetFormat();
		NewMtdPar.UseDefModel = (!it->MainModel.IsEmpty());
		NewMtdPar.DefModel = it->MainModel;
		NewMtdPar.UseRefMois = Param->IsUseStdMoisture();
		NewMtdPar.RefMois = Param->GetStdMoisture();
		NewMtdPar.UseStdMois = it->UseStdMoisture;
		NewMtdPar.StdMois = it->Moisture;

		ItStr it2;
		for(it2=it->AddModels.begin(); it2!=it->AddModels.end(); ++it2)
		{
			mtddb::PRParModel NewParMod;

			CModelData* Model = ParentPrj->GetModel((*it2));
			ExportResult* ExpRes = &Model->ModelResult.ExpResult;

			vector<mtddb::PRMtdModel>::iterator itMM;
			for(itMM=Obj.MtdModels.begin(); itMM!=Obj.MtdModels.end(); ++itMM)
			{
				if(!itMM->ModelName.Compare(Model->GetName()))
					break;
			}
			if(itMM == Obj.MtdModels.end())
			{
				mtddb::PRMtdModel NewMtdMod;

				Model->CalculateModel(true);

				NewMtdMod.ModelName = ExpRes->ModelName;
				NewMtdMod.ModelType = ExpRes->ModelType;
				NewMtdMod.SpecRangeLow = ExpRes->SpecRangeLow;
				NewMtdMod.SpecRangeUpper = ExpRes->SpecRangeUpper;
				NewMtdMod.FreqMin = ExpRes->FreqMin;
				NewMtdMod.FreqStep = ExpRes->FreqStep;
				NewMtdMod.NFreq = ExpRes->NFreq;
				NewMtdMod.SpType = ExpRes->SpType;
				NewMtdMod.NSpec = ExpRes->NSpec;
				NewMtdMod.Preproc = ExpRes->Preproc;
				NewMtdMod.MaxMah = ExpRes->MaxMah;
				NewMtdMod.MeanMah = ExpRes->MeanMah;
				NewMtdMod.IsTrans = ExpRes->IsTrans;
				NewMtdMod.FreqMinTrans = ExpRes->FreqMinTrans;
				NewMtdMod.FreqStepTrans = ExpRes->FreqStepTrans;
				NewMtdMod.NFreqTrans = ExpRes->NFreqTrans;
				NewMtdMod.SpTypeTrans = ExpRes->SpTypeTrans;
				NewMtdMod.PreprocTrans = ExpRes->PreprocTrans;
				NewMtdMod.T0 = ExpRes->T0;
				NewMtdMod.FMax = ExpRes->FMax;

				copy(ExpRes->ExcPoints.begin(), ExpRes->ExcPoints.end(), inserter(NewMtdMod.ExcPoints, NewMtdMod.ExcPoints.begin()));
				copy(ExpRes->MeanSpec.begin(), ExpRes->MeanSpec.end(), inserter(NewMtdMod.MeanSpec, NewMtdMod.MeanSpec.begin()));
				copy(ExpRes->MeanSpecMSC.begin(), ExpRes->MeanSpecMSC.end(), inserter(NewMtdMod.MeanSpecMSC, NewMtdMod.MeanSpecMSC.begin()));
				copy(ExpRes->MeanSpecStd.begin(), ExpRes->MeanSpecStd.end(), inserter(NewMtdMod.MeanSpecStd, NewMtdMod.MeanSpecStd.begin()));
				copy(ExpRes->MeanSpecTrans.begin(), ExpRes->MeanSpecTrans.end(), inserter(NewMtdMod.MeanSpecTrans, NewMtdMod.MeanSpecTrans.begin()));
				copy(ExpRes->MeanSpecMSCTrans.begin(), ExpRes->MeanSpecMSCTrans.end(), inserter(NewMtdMod.MeanSpecMSCTrans, NewMtdMod.MeanSpecMSCTrans.begin()));
				copy(ExpRes->MeanSpecStdTrans.begin(), ExpRes->MeanSpecStdTrans.end(), inserter(NewMtdMod.MeanSpecStdTrans, NewMtdMod.MeanSpecStdTrans.begin()));
				copy(ExpRes->MeanSpecHQO.begin(), ExpRes->MeanSpecHQO.end(), inserter(NewMtdMod.MeanSpecHQO, NewMtdMod.MeanSpecHQO.begin()));
				copy(ExpRes->MeanSpecMSCHQO.begin(), ExpRes->MeanSpecMSCHQO.end(), inserter(NewMtdMod.MeanSpecMSCHQO, NewMtdMod.MeanSpecMSCHQO.begin()));
				copy(ExpRes->MeanSpecStdHQO.begin(), ExpRes->MeanSpecStdHQO.end(), inserter(NewMtdMod.MeanSpecStdHQO, NewMtdMod.MeanSpecStdHQO.begin()));

				copy(ExpRes->Factors.begin(), ExpRes->Factors.end(), inserter(NewMtdMod.Factors, NewMtdMod.Factors.begin()));
				copy(ExpRes->Scores.begin(), ExpRes->Scores.end(), inserter(NewMtdMod.Scores, NewMtdMod.Scores.begin()));

				Obj.MtdModels.push_back(NewMtdMod);
				itMM = Obj.MtdModels.end() - 1;

				Model->FreeSpectraData();
			}

			ExportParamResult* ExpParRes = ExpRes->GetExpParRes(it->Name);
			if(ExpParRes == NULL)
				continue;

			NewParMod.ModelName = ExpRes->ModelName;
			NewParMod.LowCalibRange = ExpParRes->LowCalibRange;
			NewParMod.UpperCalibRange = ExpParRes->UpperCalibRange;
			NewParMod.MaxSKO = ExpParRes->MaxSKO;
			NewParMod.CorrB = ExpParRes->CorrB;
			NewParMod.CorrA = ExpParRes->CorrA;
			NewParMod.SEC = ExpParRes->SEC;
			NewParMod.MeanComp = ExpParRes->MeanComp;
			NewParMod.MeanCompTrans = ExpParRes->MeanCompTrans;
			NewParMod.MeanCompStd = ExpParRes->MeanCompStd;

			copy(ExpParRes->CalibrLin.begin(), ExpParRes->CalibrLin.end(), inserter(NewParMod.CalibrLin, NewParMod.CalibrLin.begin()));
			copy(ExpParRes->CalibrNonLin.begin(), ExpParRes->CalibrNonLin.end(), inserter(NewParMod.CalibrNonLin, NewParMod.CalibrNonLin.begin()));

			NewMtdPar.ParModels.push_back(NewParMod);
		}

		Obj.MtdParams.push_back(NewMtdPar);
	}

	vector<mtddb::PRMtdModel>::iterator itMM;
	for(itMM=Obj.MtdModels.begin(); itMM!=Obj.MtdModels.end(); ++itMM)
	{
		CModelData* Model = ParentPrj->GetModel(itMM->ModelName);
		if(Model != NULL)
			Model->ModelResult.ExpResult.Clear();
	}

	return true;
}

bool CMethodData::GetDataForExportQlt(mtddb::PRMethodQlt& Obj)
{
	if(AnalysisType != C_ANALYSIS_QLT)
		return false;

	CProjectData *ParentPrj = GetProjectByHItem(hParentItem);
	CDeviceData *ParentDev = GetDeviceByHItem(ParentPrj->GetParentHItem());

	Obj.MethodName = Name;
	Obj.DateCreated = Date;
	Obj.DevNum = ParentDev->GetNumber();
	Obj.DevType = cGetDeviceID0(ParentDev->GetType());
	Obj.SpecRangeLow = ParentPrj->GetLowLimSpecRange();
	Obj.SpecRangeUpper = ParentPrj->GetUpperLimSpecRange();
	Obj.ResSub = ParentPrj->GetSubCanalResolution();
	Obj.ApodSub = ParentPrj->GetSubCanalApodization();
	Obj.ZeroFill = ParentPrj->GetZeroFillingInd();
	Obj.UseRef = ParentPrj->IsUseRefCanal();
	Obj.ResRef = ParentPrj->GetRefCanalResolution();
	Obj.ApodRef = ParentPrj->GetRefCanalApodization();
	Obj.SmplScan = ParentPrj->GetNScansSubsample();
	Obj.SmplMeas = ParentPrj->GetNMeasuresSubsample();
	Obj.StdScan = ParentPrj->GetNScansStandart();
	Obj.StdMeas = ParentPrj->GetNMeasuresStandart();
	Obj.Xtransform = true;
	Obj.UseBath = !((ParentDev->GetType() == C_DEV_TYPE_40 || ParentDev->GetType() == C_DEV_TYPE_12M) && ParentPrj->IsSST());
	Obj.BathLength = ParentPrj->GetBathLength();
	Obj.TempLow = LowTemp;
	Obj.TempUpper = UpperTemp;
	Obj.TempDiff = MaxDiffTemp;

	Obj.MtdQltModels.clear();
	ItIntStr it2;
	for(it2=ModelQltNames.begin(); it2!=ModelQltNames.end(); ++it2)
	{
		mtddb::PRMtdQltModel NewQltMod;

		CModelData* Model = ParentPrj->GetModel(it2->second);
		ExportQltResult* ExpQltRes = &Model->ModelResult.ExpQltResult;

		Model->CalculateModel(true);

		NewQltMod.ModelName = ExpQltRes->ModelName;
		NewQltMod.ModelInd = it2->first;
		NewQltMod.SpecRangeLow = ExpQltRes->SpecRangeLow;
		NewQltMod.SpecRangeUpper = ExpQltRes->SpecRangeUpper;
		NewQltMod.FreqMin = ExpQltRes->FreqMin;
		NewQltMod.FreqStep = ExpQltRes->FreqStep;
		NewQltMod.NFreq = ExpQltRes->NFreq;
		NewQltMod.SpType = ExpQltRes->SpType;
		NewQltMod.Preproc = ExpQltRes->Preproc;
		NewQltMod.MaxMah = ExpQltRes->MaxMah;
		NewQltMod.MaxSKO = ExpQltRes->MaxSKO;
		NewQltMod.MeanMah = ExpQltRes->MeanMah;
		NewQltMod.IsTrans = ExpQltRes->IsTrans;
		NewQltMod.FreqMinTrans = ExpQltRes->FreqMinTrans;
		NewQltMod.FreqStepTrans = ExpQltRes->FreqStepTrans;
		NewQltMod.NFreqTrans = ExpQltRes->NFreqTrans;
		NewQltMod.SpTypeTrans = ExpQltRes->SpTypeTrans;
		NewQltMod.PreprocTrans = ExpQltRes->PreprocTrans;

		copy(ExpQltRes->ExcPoints.begin(), ExpQltRes->ExcPoints.end(), inserter(NewQltMod.ExcPoints, NewQltMod.ExcPoints.begin()));
		copy(ExpQltRes->MeanSpec.begin(), ExpQltRes->MeanSpec.end(), inserter(NewQltMod.MeanSpec, NewQltMod.MeanSpec.begin()));
		copy(ExpQltRes->MeanSpecMSC.begin(), ExpQltRes->MeanSpecMSC.end(), inserter(NewQltMod.MeanSpecMSC, NewQltMod.MeanSpecMSC.begin()));
		copy(ExpQltRes->MeanSpecStd.begin(), ExpQltRes->MeanSpecStd.end(), inserter(NewQltMod.MeanSpecStd, NewQltMod.MeanSpecStd.begin()));
		copy(ExpQltRes->MeanSpecTrans.begin(), ExpQltRes->MeanSpecTrans.end(), inserter(NewQltMod.MeanSpecTrans, NewQltMod.MeanSpecTrans.begin()));
		copy(ExpQltRes->MeanSpecMSCTrans.begin(), ExpQltRes->MeanSpecMSCTrans.end(), inserter(NewQltMod.MeanSpecMSCTrans, NewQltMod.MeanSpecMSCTrans.begin()));
		copy(ExpQltRes->MeanSpecStdTrans.begin(), ExpQltRes->MeanSpecStdTrans.end(), inserter(NewQltMod.MeanSpecStdTrans, NewQltMod.MeanSpecStdTrans.begin()));

		copy(ExpQltRes->Factors.begin(), ExpQltRes->Factors.end(), inserter(NewQltMod.Factors, NewQltMod.Factors.begin()));

		Obj.MtdQltModels.push_back(NewQltMod);

		Model->FreeSpectraData();
	}

	Obj.Formula.clear();
	ItQFE itF;
	for(itF=Formula.Data.begin(); itF!=Formula.Data.end(); ++itF)
	{
		mtddb::PRMtdQltElement NewElem;

		NewElem.Type = itF->Type;
		NewElem.ModelInd = itF->ModelInd;

		Obj.Formula.push_back(NewElem);
	}

	vector<mtddb::PRMtdQltModel>::iterator itMM;
	for(itMM=Obj.MtdQltModels.begin(); itMM!=Obj.MtdQltModels.end(); ++itMM)
	{
		CModelData* Model = ParentPrj->GetModel(itMM->ModelName);
		if(Model != NULL)
			Model->ModelResult.ExpQltResult.Clear();
	}

	return true;
}
