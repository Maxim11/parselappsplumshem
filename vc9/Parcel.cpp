#include "stdafx.h"

#include "Parcel.h"
#include "MainFrm.h"
#include "ParcelDoc.h"
#include "ParcelView.h"
#include "wm.h"
#include <locale.h>

#include "VerInfo.h"

// CParcelApp

BEGIN_MESSAGE_MAP(CParcelApp, CWinApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, CWinApp::OnFileNew)
//	ON_COMMAND(ID_FILE_OPEN, CWinApp::OnFileOpen)
	ON_COMMAND(ID_APP_EXIT, OnAppExit)
END_MESSAGE_MAP()


// CParcelApp construction

CParcelApp::CParcelApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only CParcelApp object

CParcelApp theApp;

// CParcelApp initialization

BOOL CParcelApp::InitInstance()
{
	// InitCommonControls() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	InitCommonControls();

	CWinApp::InitInstance();
	InitLLTimer();
	StartLLTimer();

	InitLang();
	_tsetlocale(LC_CTYPE, L"russian");

	// Initialize OLE libraries
	if (!AfxOleInit())
	{
		AfxMessageBox(IDP_OLE_INIT_FAILED);
		return FALSE;
	}
	AfxEnableControlContainer();
	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization
	SetRegistryKey(_T("Local AppWizard-Generated Applications"));
	LoadStdProfileSettings(4);  // Load standard INI file options (including MRU)
	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views
	CSingleDocTemplate* pDocTemplate;
	pDocTemplate = new CSingleDocTemplate(
		IDR_MAINFRAME,
		RUNTIME_CLASS(CParcelDoc),
		RUNTIME_CLASS(CMainFrame),       // main SDI frame window
		RUNTIME_CLASS(CParcelView));
	if (!pDocTemplate)
		return FALSE;
	AddDocTemplate(pDocTemplate);
	// Parse command line for standard shell commands, DDE, file open
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);
	// Dispatch commands specified on the command line.  Will return FALSE if
	// app was launched with /RegServer, /Register, /Unregserver or /Unregister.
	if (!ProcessShellCommand(cmdInfo))
		return FALSE;
	// The one and only window has been initialized, so show and update it
	m_pMainWnd->ShowWindow(SW_SHOWMAXIMIZED);
//	m_pMainWnd->ShowWindow(SW_SHOWMINIMIZED);
	m_pMainWnd->UpdateWindow();

//	m_pMainWnd->SetWindowText(ParcelLoadString(STRID_PARCEL_HEADER));
	m_pMainWnd->SetWindowText(_T("Parcel"));

//	if(IDYES == AfxMessageBox(ParcelLoadString(1199), MB_YESNO))
	
	pView->needStartLoad = false;
	if(pFrame->GetBaseName() != _T("")) 
	{
		if(ParcelConfirm(ParcelLoadString(1199), true))	
		{
			pView->needStartLoad = true;
		}
	}

	pView->SendMessage(WM_TIMER, PARCEL_WM_OPEN_TIMER);
//	m_pMainWnd->ShowWindow(SW_SHOWMAXIMIZED);
//	pView->SendMessage(WM_SIZE);


	// call DragAcceptFiles only if there's a suffix
	//  In an SDI app, this should occur after ProcessShellCommand
	return TRUE;
}


BOOL RegDelnode (HKEY hKeyRoot, TCHAR* lpSubKey);

int CParcelApp::DelRegSection(const TCHAR* strSection)
{
	TCHAR str[MAX_PATH*2];
	_tcscpy(str,_T("SOFTWARE\\Lumex\\Parcel\\"));
	_tcscat(str, strSection);
	return RegDelnode(REGROOT, str);
}

int CParcelApp::DelRegValue(const TCHAR* strSection, const TCHAR* strValue)
{
	HKEY hk;
	DWORD dwDisp; 
	CString strPath = _T("SOFTWARE\\Lumex\\Parcel\\");
	strPath += strSection;
	if(RegCreateKeyEx(REGROOT, strPath, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_WRITE, NULL, &hk, &dwDisp)) 
	{
		AfxMessageBox(_T("Could not create the registry key.")); 
		return 0;
	}
	int iErr = RegDeleteValue(hk, strValue); 
	RegCloseKey(hk); 

	return (iErr == 0);
}


int CParcelApp::PutRegString(const TCHAR* strSection, const TCHAR* strKey, const TCHAR* strValue)
{
   HKEY hk;
   DWORD dwDisp; 
   CString strPath = _T("SOFTWARE\\Lumex\\Parcel\\");
   strPath += strSection;

   if(RegCreateKeyEx(REGROOT, strPath, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_WRITE, NULL, &hk, &dwDisp)) 
   {
      AfxMessageBox(_T("Could not create the registry key.")); 
      return 0;
   }

   int nLen = _tcslen(strValue) + 1;
   if(RegSetValueEx(hk, strKey, 0, REG_SZ, (LPBYTE) strValue, nLen*2)) 
   {
      AfxMessageBox(_T("Could not set the key.")); 
      RegCloseKey(hk); 
      return 0;
   }

   RegCloseKey(hk); 
   return 1;
}

int CParcelApp::GetRegString(const TCHAR* strSection, const TCHAR* strKey, CString &strValue)
{
   HKEY hk;
   DWORD dwDisp, dwSize; 
   CString strPath = _T("SOFTWARE\\Lumex\\Parcel\\");
   strPath += strSection;

   if(RegCreateKeyEx(REGROOT, strPath, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_READ, NULL, &hk, &dwDisp)) 
   {
      AfxMessageBox(_T("Could not create the registry key.")); 
      return 0;
   }

   dwSize = MAX_PATH*2;
   TCHAR szBuff[MAX_PATH];
   if(RegQueryValueEx(hk, strKey, 0, 0, (LPBYTE) &szBuff, &dwSize)) 
   {
//      AfxMessageBox(_T("Could not get the key.")); 
      RegCloseKey(hk); 
	  strValue = _T("");
      return 0;
   }

   strValue = szBuff;
   RegCloseKey(hk); 
   return 1;
}

int CParcelApp::PutRegInt(const TCHAR* strSection, const TCHAR* strKey, int nValue)
{
   HKEY hk;
   DWORD dwData, dwDisp; 
   CString strPath = _T("SOFTWARE\\Lumex\\Parcel\\");
   strPath += strSection;

   if(RegCreateKeyEx(REGROOT, strPath, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_WRITE, NULL, &hk, &dwDisp)) 
   {
      AfxMessageBox(_T("Could not create the registry key.")); 
      return 0;
   }

   dwData = nValue;
   if(RegSetValueEx(hk, strKey, 0, REG_DWORD, (LPBYTE) &dwData, sizeof(DWORD))) 
   {
      AfxMessageBox(_T("Could not set the key.")); 
      RegCloseKey(hk); 
      return 0;
   }

   RegCloseKey(hk); 
   return 1;
}

int CParcelApp::GetRegInt(const TCHAR* strSection, const TCHAR* strKey, int *pnValue)
{
   HKEY hk;
   DWORD dwData, dwDisp, dwSize; 
   CString strPath = _T("SOFTWARE\\Lumex\\Parcel\\");
   strPath += strSection;

   if(RegCreateKeyEx(REGROOT, strPath, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_READ, NULL, &hk, &dwDisp)) 
   {
      AfxMessageBox(_T("Could not create the registry key.")); 
      return 0;
   }

   dwSize = sizeof(DWORD);
   if(RegQueryValueEx(hk, strKey, 0, 0, (LPBYTE) &dwData, &dwSize)) 
   {
//      AfxMessageBox(_T("Could not get the key.")); 
      RegCloseKey(hk); 
      return 0;
   }

   *pnValue = dwData;
   RegCloseKey(hk); 
   return 1;
}

CString CParcelApp::GetAppVer(int *pnV1, int *pnV2, int *pnV3)
{
	TCHAR			szValue[128];
	TCHAR			szPath[_MAX_PATH];
	CModuleVersion	VersionInfo;
	GetModuleFileName(NULL, szPath, _MAX_PATH);

	if(VersionInfo.GetFileVersionInfo(szPath))
		VersionInfo.GetProductVersion(szValue, _countof(szValue));

	TCHAR *p = _tcstok(szValue, L" .,");
	int nv1 = _tstoi(p);
	p = _tcstok(0, L" .,");
	int nv2 = _tstoi(p);
	p = _tcstok(0, L" .,");
	int nv3 = _tstoi(p);

	CString strVersion;
	strVersion.Format(L"%d.%d.%d", nv1, nv2, nv3);

	if(pnV1) *pnV1 = nv1;
	if(pnV2) *pnV2 = nv2;
	if(pnV3) *pnV3 = nv3;

	return strVersion;
}

///////////////////////
BOOL RegDelnodeRecurse (HKEY hKeyRoot, LPTSTR lpSubKey)
{
    LPTSTR lpEnd;
    LONG lResult;
    DWORD dwSize;
    TCHAR szName[MAX_PATH];
    HKEY hKey;
    FILETIME ftWrite;

    // First, see if we can delete the key without having
    // to recurse.

    lResult = RegDeleteKey(hKeyRoot, lpSubKey);

    if (lResult == ERROR_SUCCESS) 
        return TRUE;

    lResult = RegOpenKeyEx (hKeyRoot, lpSubKey, 0, KEY_READ, &hKey);

    if (lResult != ERROR_SUCCESS) 
    {
        return FALSE;
    }

    // Check for an ending slash and add one if it is missing.

    lpEnd = lpSubKey + lstrlen(lpSubKey);

    if (*(lpEnd - 1) != TEXT('\\')) 
    {
        *lpEnd =  TEXT('\\');
        lpEnd++;
        *lpEnd =  TEXT('\0');
    }

    // Enumerate the keys

    dwSize = MAX_PATH;
    lResult = RegEnumKeyEx(hKey, 0, szName, &dwSize, NULL,
                           NULL, NULL, &ftWrite);

    if (lResult == ERROR_SUCCESS) 
    {
        do {

            lstrcpy (lpEnd, szName);

            if (!RegDelnodeRecurse(hKeyRoot, lpSubKey)) {
                break;
            }

            dwSize = MAX_PATH;

            lResult = RegEnumKeyEx(hKey, 0, szName, &dwSize, NULL,
                                   NULL, NULL, &ftWrite);

        } while (lResult == ERROR_SUCCESS);
    }

    lpEnd--;
    *lpEnd = TEXT('\0');

    RegCloseKey (hKey);

    // Try again to delete the key.

    lResult = RegDeleteKey(hKeyRoot, lpSubKey);

    if (lResult == ERROR_SUCCESS) 
        return TRUE;

    return FALSE;
}

BOOL RegDelnode (HKEY hKeyRoot, TCHAR* lpSubKey)
{
    TCHAR szDelKey[2 * MAX_PATH];

    lstrcpy (szDelKey, lpSubKey);
    return RegDelnodeRecurse(hKeyRoot, szDelKey);
}


// CAboutDlg dialog used for App About
class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();						// ������������� �������

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CAboutDlg::OnBnClickedOk)
END_MESSAGE_MAP()

BOOL CAboutDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	CFont BigFont;

	LOGFONT logFont;

	logFont.lfHeight = 16;
	logFont.lfWidth = 0;
	logFont.lfEscapement = 0;
	logFont.lfOrientation = 0;
	logFont.lfWeight = FW_BOLD;
	logFont.lfItalic = 0;
	logFont.lfUnderline = 0;
	logFont.lfStrikeOut = 0;
	logFont.lfCharSet = DEFAULT_CHARSET;
	logFont.lfOutPrecision = OUT_DEFAULT_PRECIS;
	logFont.lfClipPrecision = CLIP_DEFAULT_PRECIS;
	logFont.lfQuality = PROOF_QUALITY;
	logFont.lfPitchAndFamily = DEFAULT_PITCH | FF_DONTCARE;
	_tcscpy(logFont.lfFaceName, _T("Arial"));        // request a face name "Arial"

	VERIFY(BigFont.CreateFontIndirect(&logFont));  // create the font

	GetDlgItem(IDC_STATIC_1)->SetFont(&BigFont);

	SetWindowText(ParcelLoadString(STRID_ABOUT_HDR));

	TCHAR			szValue[128];
	TCHAR			szPath[_MAX_PATH];
	CModuleVersion	VersionInfo;
	GetModuleFileName(NULL, szPath, _MAX_PATH);

	if(VersionInfo.GetFileVersionInfo(szPath))
		VersionInfo.GetProductVersion(szValue, _countof(szValue));

	CString Version;
//	Version.Format(ParcelLoadString(STRID_ABOUT_VERSION), C_ABOUT_V1, C_ABOUT_V2, C_ABOUT_V3);
	TCHAR *p = _tcstok(szValue, L" .,");
	int nv1 = _tstoi(p);
	p = _tcstok(0, L" .,");
	int nv2 = _tstoi(p);
	p = _tcstok(0, L" .,");
	int nv3 = _tstoi(p);
	Version.Format(L"v %d.%d.%d", nv1, nv2, nv3);

	GetDlgItem(IDOK)->SetWindowText(ParcelLoadString(STRID_ABOUT_EXIT));
	GetDlgItem(IDC_STATIC_1)->SetWindowText(ParcelLoadString(STRID_ABOUT_PARCEL));
	GetDlgItem(IDC_STATIC_2)->SetWindowText(ParcelLoadString(STRID_ABOUT_LONGNAME));
	GetDlgItem(IDC_STATIC_3)->SetWindowText(Version);
	GetDlgItem(IDC_STATIC_4)->SetWindowText(ParcelLoadString(STRID_ABOUT_COPYRIGHT));
	GetDlgItem(IDC_STATIC_5)->SetWindowText(ParcelLoadString(STRID_ABOUT_COMPANY));

	return true;
}

// App command to run the dialog
void CParcelApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

void CParcelApp::OnAppExit()
{
	CWinApp::OnAppExit();
}

// CParcelApp message handlers


void CAboutDlg::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
	OnOK();
}
