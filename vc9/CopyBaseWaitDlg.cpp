#include "stdafx.h"

#include "CopyBaseWaitDlg.h"
#include "BranchInfo.h"

IMPLEMENT_DYNAMIC(CCopyBaseWaitDlg, CWaitingDlg)

CCopyBaseWaitDlg::CCopyBaseWaitDlg(CString fname, CWnd* pParent) : CWaitingDlg(pParent)
{
	FileName = fname;
	Comment = ParcelLoadString(STRID_COPY_BASE_WAIT_COMMENT);
}

CCopyBaseWaitDlg::~CCopyBaseWaitDlg()
{
}

void CCopyBaseWaitDlg::DoDataExchange(CDataExchange* pDX)
{
	CWaitingDlg::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CCopyBaseWaitDlg, CWaitingDlg)
END_MESSAGE_MAP()


BOOL CCopyBaseWaitDlg::OnInitDialog()
{
	CWaitingDlg::OnInitDialog();

	return true;
}

void CCopyBaseWaitDlg::OK()
{
	CWaitingDlg::OK();
}

void CCopyBaseWaitDlg::Stop()
{
	CWaitingDlg::Stop();
}

void CCopyBaseWaitDlg::Work()
{
	ParcelWait(true);
	
	bool res = pView->CopyBaseTo(FileName);

	ParcelWait(false);

	if(res)
		OK();
	else
		Stop();
}
