#ifndef _CREATE_SST_WAIT_DLG_H_
#define _CREATE_SST_WAIT_DLG_H_

#pragma once

#include "WaitingDlg.h"
#include "ProjectData.h"


///////////////////////////////////////////////////////////////////////////////////////////////////
// ������ ��������� �������� ��������� apjx-�����

class CCreateSSTWaitDlg : public CWaitingDlg
{
	DECLARE_DYNAMIC(CCreateSSTWaitDlg)

public:

	CCreateSSTWaitDlg(CProjectData* prj, VPlate* plates, CWnd* pParent = NULL);	// �����������
	virtual ~CCreateSSTWaitDlg();												// ����������

	enum { IDD = CWaitingDlg::IDD };	// ������������� �������

protected:

	CProjectData* pPrj;
	VPlate* Plates;

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();						// ������������� �������

	virtual void OK();				// ��������� �������� ����� �� �������
	virtual void Stop();			// ��������� ����������� ����� �� �������
	virtual void Work();			// ������ �������� �������

	DECLARE_MESSAGE_MAP()
};

#endif
