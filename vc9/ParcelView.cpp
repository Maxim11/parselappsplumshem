#include "stdafx.h"

#include "Parcel.h"
#include "ParcelDoc.h"
#include "wm.h"
#include "MainFrm.h"
#include "ParcelView.h"
#include "LoadDBWaitDlg.h"
#include "CopyToBaseWaitDlg.h"
#include "CopyBaseWaitDlg.h"
#include "FileSystem.h"

#include "SetTemplateDlg.h"

using namespace filesystem;

CParcelView *pView;

IMPLEMENT_DYNCREATE(CParcelView, CFormView)

BEGIN_MESSAGE_MAP(CParcelView, CFormView)

	ON_WM_SIZE()
	ON_WM_TIMER()

	ON_NOTIFY(TVN_SELCHANGED, IDC_TREE1, OnTvnSelchangedTree1)
	ON_NOTIFY(TVN_ITEMEXPANDED, IDC_TREE1, OnTvnItemexpandedTree1)

	ON_NOTIFY(LVN_ITEMCHANGED, IDC_VIEW_PARAMS_LIST, OnParamsListSelChange)

	ON_NOTIFY(LVN_ITEMCHANGED, IDC_VIEW_SAMPLETRANS_SAMPLES_LIST, OnSamplesTransListSelChange)
	ON_NOTIFY(HDN_ITEMCLICK, 0, OnSamplesTransListColumnClick)

END_MESSAGE_MAP()

CParcelView::CParcelView() : CFormView(CParcelView::IDD)
{
	pView = this;
	pRoot = &Data;

	needStartLoad = false;

	active = TIM_NONE;
}

CParcelView::~CParcelView()
{
}

void CParcelView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_TREE1, m_Tree);

	DDX_Control(pDX, IDC_VIEW_DEVICE_PROJECTS_LIST, m_list_DevicePrj);

	DDX_Control(pDX, IDC_VIEW_PROJECT_SPEC_LIST, m_list_ProjectSpec);
	DDX_Control(pDX, IDC_VIEW_PROJECT_TEMP_LIST, m_list_ProjectTemp);
	DDX_Control(pDX, IDC_VIEW_PROJECT_SUBJ_LIST, m_list_ProjectSubj);
	DDX_Control(pDX, IDC_VIEW_PROJECT_REF_LIST, m_list_ProjectRef);
	DDX_Control(pDX, IDC_VIEW_PROJECT_PLAN_LIST, m_list_ProjectPlan);

	DDX_Control(pDX, IDC_VIEW_PARAMS_LIST, m_list_Params);
	DDX_Control(pDX, IDC_VIEW_PARAMSSAM_LIST, m_list_ParamsSam);
	DDX_Control(pDX, IDC_VIEW_PARAMSMOD_LIST, m_list_ParamsMod);

	DDX_Control(pDX, IDC_VIEW_SAMPLESET_SETS_LIST, m_list_SampleSets);

	DDX_Control(pDX, IDC_VIEW_SAMPLETRANS_SAMPLES_LIST, m_list_SamplesTrans);
	DDX_Control(pDX, IDC_VIEW_SAMPLETRANS_SPECTRA_LIST, m_list_SpectraTrans);
	DDX_Control(pDX, IDC_VIEW_SAMPLETRANS_SETTINGS_LIST, m_list_TransSettings);

	DDX_Control(pDX, IDC_VIEW_MODELSET_MODELS_LIST, m_list_Models);

	DDX_Control(pDX, IDC_VIEW_MODEL_PARAMS_LIST, m_list_ModParams);
	DDX_Control(pDX, IDC_VIEW_MODEL_SETTINGS_LIST, m_list_ModSet);
	DDX_Control(pDX, IDC_VIEW_MODEL_SAMPLES_LIST, m_list_ModSamples);
	DDX_Control(pDX, IDC_VIEW_MODEL_SAMPLESVAL_LIST, m_list_ModSamplesVal);
	DDX_Control(pDX, IDC_VIEW_MODEL_EXCLUDED_LIST, m_list_ModExcluded);

	DDX_Control(pDX, IDC_VIEW_METHODSET_METHODS_LIST, m_list_Methods);

	DDX_Control(pDX, IDC_VIEW_METHOD_INFO_LIST, m_list_MtdInfo);
	DDX_Control(pDX, IDC_VIEW_METHOD_RULES_LIST, m_list_MtdRules);
	DDX_Control(pDX, IDC_VIEW_METHOD_QLTMODELS_LIST, m_list_MtdQltModels);
}

BOOL CParcelView::PreCreateWindow(CREATESTRUCT& cs)
{
	return CFormView::PreCreateWindow(cs);
}

void CParcelView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	CreateSpecFonts();

	GetParentFrame()->RecalcLayout();
//	ResizeParentToFit();

	USES_CONVERSION;

	if(Data.GetHItem() == NULL)
	{
		TVINSERTSTRUCT tvInsert;
		tvInsert.hParent = NULL;
		tvInsert.hInsertAfter = NULL;
		tvInsert.item.mask = TVIF_TEXT;
		tvInsert.item.pszText = A2W(T2A(cEmpty));

		HTREEITEM hRoot = m_Tree.InsertItem(&tvInsert);

		Data.SetHItem(hRoot);

		m_ImageList.Create(IDB_BITMAP1, 16, 1, RGB(255, 255, 255));
		m_Tree.SetImageList(&m_ImageList, TVSIL_NORMAL);

		m_Tree.SetItemImage(hRoot, 8, 8);
	}

	RecreateColumns();

	if(!Data.Templates.GetBaseName().IsEmpty())
	{
		Data.Templates.LoadBase(Data.Templates.GetBaseName(), false);
	}
/*
	if(needStartLoad)
        StartBaseOpen();
*/
}

void CParcelView::OnSize(UINT Type, int cx, int cy)
{
	CFormView::OnSize(Type, cx, cy);

	if(!m_Tree)
		return;

	CRect RectClnt;
	GetClientRect(RectClnt);

	CRect RectTree;
	m_Tree.GetWindowRect(RectTree);

	ScreenToClient(&RectTree);

	int width = RectTree.Width();
	int height = RectClnt.Height() - 2 * RectTree.top;

	m_Tree.MoveWindow(RectTree.left, RectTree.top, width, height);


	CRect RectList;

	m_list_SamplesTrans.GetWindowRect(RectList);
	ScreenToClient(&RectList);
	width = RectClnt.Width() - RectTree.Width() - 4 * RectTree.left;
	height = RectList.Height();
	m_list_SamplesTrans.MoveWindow(RectList.left, RectList.top, width, height);

	m_list_SampleSets.GetWindowRect(RectList);
	ScreenToClient(&RectList);
	width = RectClnt.Width() - RectTree.Width() - 4 * RectTree.left;
	height = RectList.Height();
	m_list_SampleSets.MoveWindow(RectList.left, RectList.top, width, height);

	m_list_Models.GetWindowRect(RectList);
	ScreenToClient(&RectList);
	width = RectList.Width();
	height = RectTree.Height()/2;
	m_list_Models.MoveWindow(RectList.left, RectList.top, width, height);
}

#include "WaitingLoadDlg.h"
UINT __cdecl TPGD_DataThread( LPVOID pParam )
{
	CWaitingLoadDlg* pDlg = (CWaitingLoadDlg*) pParam;
	Sleep(200);
	pView->StartBaseOpen();
	pDlg->Done();
	pDlg->SendMessage(WM_CLOSE);

	return 0;
}


void CParcelView::OnTimer(UINT Id)
{
	if(Id == PARCEL_WM_OPEN_TIMER)
	{
		CWaitingLoadDlg dlg;
		AfxBeginThread(TPGD_DataThread, &dlg);
		CString str; 
		pFrame->GetWindowTextW(str);
		pFrame->SetWindowText(ParcelLoadString(878));
		dlg.DoModal();
		pFrame->SetWindowText(str);
		pFrame->SendMessage(WM_TIMER,2);
	}

	if(Id == PARCEL_WM_EXIT_TIMER)
	{
		KillTimer(Id);
		theApp.OnAppExit();
	}
}

bool CParcelView::ExecuteBaseNew(CWnd* wnd)
{
	CString Title = ParcelLoadString(STRID_STDTITLE_BASE_NEW);
	CString Filter = LoadString(IDS_FILE_FILTER);
	CString DefExt = LoadString(IDS_FILE_DEFEXT);

	CString OldFileName = Data.GetBaseDefFileName();
	CString FileName = OldFileName;

	CFileDialog Dialog(FALSE, DefExt, cEmpty,
					   OFN_EXPLORER | OFN_HIDEREADONLY | OFN_PATHMUSTEXIST | OFN_OVERWRITEPROMPT,
					   Filter, wnd);

	Dialog.m_ofn.lpstrTitle = Title;

	if(Dialog.DoModal() == IDOK)
	{
		FileName = Dialog.GetPathName();
		if(FileName.IsEmpty())
		{
			ParcelError(ParcelLoadString(STRID_STDERR_FNEMPTY));
			return false;
		}

		ParcelWait(true);

		Data.Database.Close();

		Data.DeleteBase(FileName);

		if(!Data.Database.CreateDatabase(FileName))
		{
			ParcelWait(false);
			ParcelError(ParcelLoadString(STRID_STDERR_DBNOCREATE));
			return false;
		}
		Data.CreateSpectraDataDirectory(FileName);

		Data.SetBaseFileName(FileName);
		if(!Data.LoadDatabase(&m_Tree))
		{
			Data.SetBaseFileName(OldFileName);
			ParcelWait(false);
			ParcelError(ParcelLoadString(STRID_STDERR_DBNOLOAD));
			return false;
		}

		ParcelWait(false);

		return true;
	}

	return false;
}

bool CParcelView::ExecuteBaseLoad(CWnd *wnd)
{
	CString Title = ParcelLoadString(STRID_STDTITLE_BASE_LOAD);
	CString Filter = LoadString(IDS_FILE_FILTER);
	CString DefExt = LoadString(IDS_FILE_DEFEXT);

	CString OldFileName = Data.GetBaseDefFileName();
	CString FileName = OldFileName;

	CFileDialog Dialog(TRUE, DefExt, cEmpty,
					   OFN_EXPLORER | OFN_HIDEREADONLY | OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST,
					   Filter, wnd);

	Dialog.m_ofn.lpstrTitle = Title;

	if(Dialog.DoModal() == IDOK)
	{
		FileName = Dialog.GetPathName();
		if(FileName.IsEmpty())
		{
			ParcelError(ParcelLoadString(STRID_STDERR_FNEMPTY));
			return false;
		}

		ParcelWait(true);

		Data.Database.Close();

		if(!Data.Database.Open(FileName))
		{
			ParcelWait(false);
			ParcelError(ParcelLoadString(STRID_STDERR_DBNOOPEN));
			ParcelWait(true);
			Data.Database.Open(Data.GetBaseFileName());
			ParcelWait(false);
			return false;
		}
		if(!Data.Database.CheckDatabase())
		{
			ParcelWait(false);
			ParcelError(ParcelLoadString(STRID_STDERR_DBINCORRVERS));
			ParcelWait(true);
			Data.Database.Close();
			Data.Database.Open(Data.GetBaseFileName());
			ParcelWait(false);
			return false;
		}

		Data.SetBaseFileName(FileName);
		CLoadDBWaitDlg dlg(&m_Tree);
		if(dlg.DoModal() != IDOK)
		{
			Data.SetBaseFileName(OldFileName);
			ParcelWait(false);
			ParcelError(ParcelLoadString(STRID_STDERR_DBNOLOAD));
			ParcelWait(true);
			Data.Database.Close();
			Data.Database.Open(Data.GetBaseFileName());
			CLoadDBWaitDlg dlg2(&m_Tree);
			dlg2.DoModal();
			ParcelWait(false);
			return false;
		}

		ParcelWait(false);
		return true;
	}

	return false;
}

bool CParcelView::ExecuteBaseClose()
{
	Data.CloseDatabase(&m_Tree);

	return true;
}

bool CParcelView::ExecuteBaseDelete()
{
	CString Txt, fmt = ParcelLoadString(STRID_ASK_DELBASE);
	Txt.Format(fmt, Data.GetBaseName());
	if(!ParcelConfirm(Txt, false))
		return false;

	Data.DeleteDatabase(&m_Tree);

	return true;
}

bool CParcelView::ExecuteBaseCopy(CWnd* wnd)
{
	CString Title = ParcelLoadString(STRID_STDTITLE_BASE_COPY);
	CString Filter = LoadString(IDS_FILE_FILTER);
	CString DefExt = LoadString(IDS_FILE_DEFEXT);

	CString OldFileName = Data.GetBaseDefFileName();
	CString FileName = OldFileName;

	CFileDialog Dialog(FALSE, DefExt, cEmpty,
					   OFN_EXPLORER | OFN_HIDEREADONLY | OFN_PATHMUSTEXIST | OFN_OVERWRITEPROMPT,
					   Filter, wnd);

	Dialog.m_ofn.lpstrTitle = Title;

	if(Dialog.DoModal() == IDOK)
	{
		FileName = Dialog.GetPathName();
		if(FileName.IsEmpty())
		{
			ParcelError(ParcelLoadString(STRID_STDERR_FNEMPTY));
			return false;
		}

		if(!FileName.Compare(Data.GetBaseFileName()))
		{
//			ParcelError(ParcelLoadString(STRID_STDERR_COPYTOSELF));
			return true;
		}

		CCopyBaseWaitDlg dlg2(FileName);
		if(dlg2.DoModal() != IDOK)
			return false;

		Data.SetBaseFileName(FileName);
		m_Tree.SetItemText(m_Tree.GetRootItem(), Data.GetBaseName());

		return true;
	}

	return false;
}

bool CParcelView::CopyBaseTo(CString fname)
{
	Data.Database.Close();

	if(!Data.CopyBase(fname))
	{
		ParcelError(ParcelLoadString(STRID_STDERR_DBNOCOPY));
		Data.Database.Open(Data.GetBaseFileName());
		return false;
	}

	if(!Data.Database.Open(fname))
	{
		ParcelError(ParcelLoadString(STRID_STDERR_DBNOOPEN));
		Data.Database.Open(Data.GetBaseFileName());
		return false;
	}

	return true;
}

bool CParcelView::StartBaseOpen()
{
	if(!needStartLoad) return true;

	CString s, FileName = Data.GetBaseFileName();

	if(!FileName.IsEmpty() && PathFileExists(FileName))
	{
		ParcelWait(true);
		if(Data.Database.Open(FileName))
		{
			if(Data.Database.ReadDatabaseVersion() == prdb::PRDB_CURRENT_VERSION)
			{
				if(Data.LoadDatabase(&m_Tree))
				{
					Data.SetBaseFileName(FileName);
					m_Tree.SetItemText(m_Tree.GetRootItem(), Data.GetBaseName());
					ParcelWait(false);
					return true;
				}
				else
				{
					Data.Database.Close();

					s.Format(ParcelLoadString(STRID_STDERR_STARTBASE_NOLOAD), FileName);
					ParcelError(s);
					ParcelWait(false);
					return false;
				}
			}
			else
			{
				Data.Database.Close();
				s.Format(ParcelLoadString(STRID_STDERR_STARTBASE_INCVERS), FileName);
				ParcelError(s);
				ParcelWait(false);
				return false;
			}
		}
		else
		{
			s.Format(ParcelLoadString(STRID_STDERR_STARTBASE_NOOPEN), FileName);
			ParcelError(s);
			ParcelWait(false);
			return false;
		}
	}
	else
	{
		s.Format(ParcelLoadString(STRID_STDERR_STARTBASE_NOEXIST), FileName);
		ParcelError(s);
		return false;
	}
}

void CParcelView::CreateNewDevice()
{
	HTREEITEM hParentItem = Data.GetHItem();
	HTREEITEM hLastItem = Data.GetLastDeviceHItem();

	CDeviceData* DevData = Data.AddNewDevice();
	if(DevData != NULL)
	{
		HTREEITEM hItem = m_Tree.InsertItem(TVIF_TEXT, DevData->GetName(), 0, 0, 0, 0, 0, hParentItem, hLastItem);

		DevData->SetHItem(hItem);

		m_Tree.SetItemImage(hItem, 9, 9);

		m_Tree.Select(hItem, TVGN_CARET);
		m_Tree.Expand(hParentItem, TVE_EXPAND);
		m_Tree.SetItemImage(hParentItem, 1, 1);
	}
}

void CParcelView::ChangeDeviceSettings()
{
	HTREEITEM hItem = ActiveInfo.hDevice;

	if(Data.ChangeDeviceSettings(hItem))
	{
		m_Tree.SetItemText(hItem, ActiveInfo.Device->GetName());
		SetViewMode(hItem);
	}
}

void CParcelView::DeleteDevice()
{
	HTREEITEM hItem = ActiveInfo.hDevice;
	CString Txt, fmt = ParcelLoadString(STRID_ASK_DELDEVICE);
	Txt.Format(fmt, ActiveInfo.Device->GetName());
	if(!ParcelConfirm(Txt, false))
		return;

	if(!Data.DeleteDevice(hItem))
		return;

	m_Tree.DeleteItem(hItem);

	if(Data.GetNDevices() == 0)
	{
		m_Tree.Expand(Data.GetHItem(), TVE_COLLAPSE);
		m_Tree.SetItemImage(Data.GetHItem(), 8, 8);
	}
}

void CParcelView::CopyDeviceToBase(CWnd *wnd)
{
	CString Title = ParcelLoadString(STRID_STDTITLE_COPYTOBASE);
	CString Filter = LoadString(IDS_FILE_FILTER);
	CString DefExt = LoadString(IDS_FILE_DEFEXT);

	CString OldFileName = Data.GetBaseFileName();
	CString FileName = OldFileName;

	CFileDialog Dialog(FALSE, DefExt, cEmpty,
					   OFN_EXPLORER | OFN_HIDEREADONLY | OFN_PATHMUSTEXIST,
					   Filter, wnd);

	Dialog.m_ofn.lpstrTitle = Title;

	if(Dialog.DoModal() == IDOK)
	{
		FileName = Dialog.GetPathName();
		if(FileName.IsEmpty())
		{
			ParcelError(ParcelLoadString(STRID_STDERR_FNEMPTY));
			return;
		}
		if(!FileName.Compare(Data.GetBaseFileName()))
		{
			ParcelError(ParcelLoadString(STRID_STDERR_SAMEBASE));
			return;
		}

		CDeviceData CopyDev;
		if(!ActiveInfo.Device->GetDeviceData(CopyDev))
			return;

		CCopyToBaseWaitDlg dlg(FileName, &CopyDev, &m_Tree, cEmpty);
		if(dlg.DoModal() == IDOK)
		{
			CDeviceData* NewDev = Data.pCopiedDev;
			if(NewDev != NULL)
			{
				active = GetBranchInfo(NewDev->GetHItem(), ActiveInfo);
				m_Tree.Select(NewDev->GetHItem(), TVGN_CARET);
				SetViewMode(NewDev->GetHItem());
				Data.pCopiedDev = NULL;
			}
		}
	}
}

bool CParcelView::IsCreateDeviceEnable()
{
	return (active == TIM_ROOT || active == TIM_DEVICE);
}

bool CParcelView::IsChangeDeviceSettingsEnable()
{
	return (active == TIM_DEVICE && !ActiveInfo.Device->IsSpectraExist());
}

bool CParcelView::IsDeleteDeviceEnable()
{
	return (active == TIM_DEVICE);
}

bool CParcelView::IsCopyDeviceToBaseEnable()
{
	return (active == TIM_DEVICE || active == TIM_PROJECT);
}

void CParcelView::CreateNewProject()
{
	CProjectData* PrjData = ActiveInfo.Device->AddNewProject(&m_Tree);
	if(PrjData != NULL)
	{
		PrjData->CopyMode = C_PRJ_COPY_ONLYSETTINGS;
	}
}

void CParcelView::ChangeProjectSettings()
{
	HTREEITEM hItem = ActiveInfo.hProject;

	if(ActiveInfo.Device->ChangeProjectSettings(hItem))
	{
		m_Tree.SetItemText(hItem, ActiveInfo.Project->GetName());
		SetViewMode(hItem);
	}
}

void CParcelView::DeleteProject()
{
	HTREEITEM hItem = ActiveInfo.hProject;
	CString Txt, fmt = ParcelLoadString(STRID_ASK_DELPROJECT);
	Txt.Format(fmt, ActiveInfo.Project->GetName());
	if(!ParcelConfirm(Txt, false))
		return;

	if(!ActiveInfo.Device->DeleteProject(hItem))
		return;
	m_Tree.DeleteItem(hItem);

	if(ActiveInfo.Device->GetNProjects() == 0)
	{
		m_Tree.Expand(ActiveInfo.hDevice, TVE_COLLAPSE);
		m_Tree.SetItemImage(ActiveInfo.hDevice, 9, 9);
	}
}

void CParcelView::CreateSSTProject()
{
	CProjectData* PrjData = ActiveInfo.Device->CreateSSTProject(&m_Tree);
	if(PrjData != NULL)
	{
		PrjData->CopyMode = C_PRJ_COPY_ONLYSETTINGS;
	}
}

void CParcelView::LoadOldProject(CWnd *wnd)
{
	CString Title = ParcelLoadString(STRID_STDTITLE_LOADOLDPRJ);
	CString Filter = LoadString(IDS_OLDAPJ_FILTER);
	CString DefExt = LoadString(IDS_OLDAPJ_DEFEXT);

	CString Folder = pRoot->GetBaseFolder();
	SetCurrentDirectory(Folder);
	CString FileName = MakePath(Folder, _T("default"));
	FileName += DefExt;

	CFileDialog Dialog2(TRUE, DefExt, cEmpty,
					    OFN_EXPLORER | OFN_HIDEREADONLY | OFN_PATHMUSTEXIST | OFN_OVERWRITEPROMPT,
					    Filter, wnd);

	Dialog2.m_ofn.lpstrTitle = Title;

	if(Dialog2.DoModal() == IDOK)
	{
		FileName = Dialog2.GetPathName();
		if(FileName.IsEmpty())
		{
			ParcelError(ParcelLoadString(STRID_STDERR_FNEMPTY));
			return;
		}
		if(!PathFileExists(FileName))
		{
			ParcelError(ParcelLoadString(STRID_STDERR_FILENOEXIST));
			return;
		}

		CProjectData *Prj = ActiveInfo.Device->AddOldApj(FileName, &m_Tree);
		if(Prj == NULL)
			return;
	}
}

void CParcelView::LoadOldSSTProject(CWnd *wnd)
{
	CString Title = ParcelLoadString(STRID_STDTITLE_LOADOLDSSTPRJ);
	CString Filter = LoadString(IDS_OLDAPJ_FILTER);
	CString DefExt = LoadString(IDS_OLDAPJ_DEFEXT);

	CString Folder = pRoot->GetBaseFolder();
	SetCurrentDirectory(Folder);
	CString FileName = MakePath(Folder, _T("default"));
	FileName += DefExt;

	CFileDialog Dialog2(TRUE, DefExt, cEmpty,
					    OFN_EXPLORER | OFN_HIDEREADONLY | OFN_PATHMUSTEXIST | OFN_OVERWRITEPROMPT,
					    Filter, wnd);

	Dialog2.m_ofn.lpstrTitle = Title;

	if(Dialog2.DoModal() == IDOK)
	{
		FileName = Dialog2.GetPathName();
		if(FileName.IsEmpty())
		{
			ParcelError(ParcelLoadString(STRID_STDERR_FNEMPTY));
			return;
		}
		if(!PathFileExists(FileName))
		{
			ParcelError(ParcelLoadString(STRID_STDERR_FILENOEXIST));
			return;
		}

		CProjectData *Prj = ActiveInfo.Device->AddOldApj(FileName, &m_Tree, true);
		if(Prj == NULL)
			return;
	}
}

void CParcelView::CopyProjectToBase(CWnd* wnd)
{
	CString Title = ParcelLoadString(STRID_STDTITLE_COPYTOBASE);
	CString Filter = LoadString(IDS_FILE_FILTER);
	CString DefExt = LoadString(IDS_FILE_DEFEXT);

	CString OldFileName = Data.GetBaseFileName();
	CString FileName = OldFileName;

	CFileDialog Dialog(FALSE, DefExt, cEmpty,
					   OFN_EXPLORER | OFN_HIDEREADONLY | OFN_PATHMUSTEXIST,
					   Filter, wnd);

	Dialog.m_ofn.lpstrTitle = Title;

	if(Dialog.DoModal() == IDOK)
	{
		FileName = Dialog.GetPathName();
		if(FileName.IsEmpty())
		{
			ParcelError(ParcelLoadString(STRID_STDERR_FNEMPTY));
			return;
		}
		if(!FileName.Compare(Data.GetBaseFileName()))
		{
			ParcelError(ParcelLoadString(STRID_STDERR_SAMEBASE));
			return;
		}

		CString PrjName = ActiveInfo.Project->GetName();

		CDeviceData CopyDev;
		if(!ActiveInfo.Device->GetDeviceData(CopyDev, PrjName))
			return;

		CCopyToBaseWaitDlg dlg(FileName, &CopyDev, &m_Tree, PrjName);
		if(dlg.DoModal() == IDOK)
		{
			CDeviceData* NewDev = Data.GetLastDevice();
			active = GetBranchInfo(NewDev->GetHItem(), ActiveInfo);
			m_Tree.Select(NewDev->GetHItem(), TVGN_CARET);
			SetViewMode(NewDev->GetHItem());
		}
	}
}

void CParcelView::ExportProjectSettings(CWnd *wnd)
{
	if(ActiveInfo.Project == NULL)
		return;

	CString Title = ParcelLoadString(STRID_STDTITLE_EXPORTPROJECT);
	CString Filter = LoadString(IDS_APJFILE_FILTER);
	CString DefExt = LoadString(IDS_APJFILE_DEFEXT);

	CString Folder = pRoot->GetBaseFolder();
	SetCurrentDirectory(Folder);
	CString FileName = MakePath(Folder, ActiveInfo.Project->GetName());
	FileName += DefExt;

	CFileDialog Dialog(FALSE, DefExt, FileName,
					   OFN_EXPLORER | OFN_HIDEREADONLY | OFN_PATHMUSTEXIST | OFN_OVERWRITEPROMPT,
					   Filter, wnd);

	Dialog.m_ofn.lpstrTitle = Title;

	if(Dialog.DoModal() == IDOK)
	{
		FileName = Dialog.GetPathName();
		if(FileName.IsEmpty())
		{
			ParcelError(ParcelLoadString(STRID_STDERR_FNEMPTY));
			return;
		}
		if(PathFileExists(FileName))
		{
			if(!ParcelConfirm(ParcelLoadString(STRID_STDERR_FILEALREADYEXIST), false))
				return;
		}

		Data.DeleteBaseApjx(FileName);

		ActiveInfo.Project->ExportSettings(FileName);
	}
}

void CParcelView::ImportSpectra(CWnd* wnd)
{
	if(ActiveInfo.Device == NULL)
		return;

	CString Title = ParcelLoadString(STRID_STDTITLE_IMPORTSPECTRA);
	CString Filter = LoadString(IDS_APJFILE_FILTER);
	CString DefExt = LoadString(IDS_APJFILE_DEFEXT);

	CString Folder = pRoot->GetBaseFolder();
	SetCurrentDirectory(Folder);
	CString FileName = MakePath(Folder, _T("default"));
	FileName += DefExt;

	CFileDialog Dialog(TRUE, DefExt, cEmpty,
					   OFN_EXPLORER | OFN_HIDEREADONLY | OFN_PATHMUSTEXIST | OFN_OVERWRITEPROMPT,
					   Filter, wnd);

	Dialog.m_ofn.lpstrTitle = Title;

	if(Dialog.DoModal() == IDOK)
	{
		FileName = Dialog.GetPathName();
		if(FileName.IsEmpty())
		{
			ParcelError(ParcelLoadString(STRID_STDERR_FNEMPTY));
			return;
		}
		if(!PathFileExists(FileName))
		{
			ParcelError(ParcelLoadString(STRID_STDERR_FILENOEXIST));
			return;
		}

		CProjectData *Prj = ActiveInfo.Device->ImportSpectra(FileName, &m_Tree);
		if(Prj == NULL)
			return;
	}
}


bool CParcelView::IsCreateProjectEnable()
{
	return (active == TIM_DEVICE || active == TIM_PROJECT);
}

bool CParcelView::IsChangeProjectSettingsEnable()
{
	return (active == TIM_PROJECT);
}

bool CParcelView::IsDeleteProjectEnable()
{
	return (active == TIM_PROJECT);
}

bool CParcelView::IsCreateSSTProjectEnable()
{
	return ((active == TIM_DEVICE || active == TIM_PROJECT) && !ActiveInfo.Device->IsSSTExist()
		&& ActiveInfo.Device->GetType() != C_DEV_TYPE_IMIT);
}

bool CParcelView::IsLoadOldProjectEnable()
{
	return (active == TIM_DEVICE || active == TIM_PROJECT);
}

bool CParcelView::IsLoadOldSSTProjectEnable()
{
	return ((active == TIM_DEVICE || active == TIM_PROJECT) && !ActiveInfo.Device->IsSSTExist()
		&& ActiveInfo.Device->GetType() != C_DEV_TYPE_IMIT);
}

bool CParcelView::IsCopyProjectToBaseEnable()
{
	return (active == TIM_PROJECT);
}

bool CParcelView::IsExportProjectSettingsEnable()
{
	return (active == TIM_PROJECT);
}

bool CParcelView::IsImportSpectraEnable()
{
	return (active == TIM_DEVICE || active == TIM_PROJECT);
}

bool CParcelView::IsProjectTransferEnable()
{
	return (active == TIM_PROJECT);
}


void CParcelView::EditParamList()
{
	if(ActiveInfo.Project->EditParamList())
	{
		m_Tree.Select(ActiveInfo.hParamSet, TVGN_CARET);
		SetViewMode(ActiveInfo.hParamSet);
	}
}

bool CParcelView::IsEditParamListEnable()
{
	return (active == TIM_PARAMSET);
}

void CParcelView::EditSampleList()
{
	if(ActiveInfo.Project->EditSampleList(ActiveInfo.hSampleTrans))
	{
		m_Tree.Select(ActiveInfo.hSampleTrans, TVGN_CARET);
		SetViewMode(ActiveInfo.hSampleTrans);
	}
}

void CParcelView::CreateSSTTransfer()
{
	HTREEITEM hParentItem = ActiveInfo.hSampleSet;
	HTREEITEM hLastItem = ActiveInfo.Project->GetLastTransferHItem();

	CTransferData* pTrans = ActiveInfo.Project->AddSSTTransfer(&m_Tree);
	if(pTrans != NULL)
	{
		m_Tree.Select(hLastItem, TVGN_CARET);
		m_Tree.Select(pTrans->GetHItem(), TVGN_CARET);
		m_Tree.Expand(hParentItem, TVE_EXPAND);
		m_Tree.SetItemImage(hParentItem, 7, 7);
	}
}

void CParcelView::DeleteSSTTransfer()
{
	HTREEITEM hItem = ActiveInfo.Project->GetTransSSTHItem();
	CString Txt = ParcelLoadString(STRID_ASK_DELTRANSSST);
	if(!ParcelConfirm(Txt, false))
		return;

	if(!ActiveInfo.Project->DeleteTransfer(hItem))
		return;

	m_Tree.DeleteItem(hItem);

	if(ActiveInfo.Project->GetNTransfers() == 0)
	{
		m_Tree.Expand(ActiveInfo.hSampleSet, TVE_COLLAPSE);
		m_Tree.SetItemImage(ActiveInfo.hSampleSet, 10, 10);
	}
}

void CParcelView::ProjectTransfer()
{
	if(ActiveInfo.Project->ProjectTransfer())
	{
	}
}

void CParcelView::AddTransfer()
{
	CTransferData* TransData = ActiveInfo.Project->AddNewTransfer();
	AddTransfer(TransData);
}

void CParcelView::AddTransfer(CTransferData* TransData)
{
	HTREEITEM hParentItem = ActiveInfo.hSampleSet;
	HTREEITEM hLastItem = ActiveInfo.Project->GetLastTransferHItem();

	if(TransData != NULL)
	{
		HTREEITEM hItem = m_Tree.InsertItem(TVIF_TEXT, TransData->GetName(), 0, 0, 0, 0, 0,
			hParentItem, hLastItem);

		TransData->SetHItem(hItem);

		m_Tree.SetItemImage(hItem, 11, 11);

		m_Tree.Select(hItem, TVGN_CARET);
		m_Tree.Expand(hParentItem, TVE_EXPAND);
		m_Tree.SetItemImage(hParentItem, 7, 7);
	}
}

void CParcelView::EditTransfer()
{
	HTREEITEM hItem = ActiveInfo.hSampleTrans;

	if(ActiveInfo.Project->ChangeTransfer(hItem))
	{
		m_Tree.SetItemText(hItem, ActiveInfo.Trans->GetName());
		SetViewMode(hItem);
	}
}

void CParcelView::DeleteTransfer()
{
	HTREEITEM hItem = ActiveInfo.hSampleTrans;
	CString Txt, fmt = ParcelLoadString(STRID_ASK_DELTRANS);
	Txt.Format(fmt, ActiveInfo.Trans->GetName());
	if(!ParcelConfirm(Txt, false))
		return;

	if(!ActiveInfo.Project->DeleteTransfer(hItem))
		return;

	m_Tree.DeleteItem(hItem);

	if(ActiveInfo.Project->GetNTransfers() == 0)
	{
		m_Tree.Expand(ActiveInfo.hSampleSet, TVE_COLLAPSE);
		m_Tree.SetItemImage(ActiveInfo.hSampleSet, 10, 10);
	}
}

bool CParcelView::IsEditSampleListEnable()
{
//	return (active == TIM_SAMPLEOWN || (active == TIM_SAMPLETRANS && ActiveInfo.Trans->GetType() != C_TRANS_TYPE_NORMAL));
	return (active == TIM_SAMPLEOWN) || (active == TIM_SAMPLETRANS);
}

bool CParcelView::IsCreateSSTTransferEnable()
{
	return ((active == TIM_SAMPLESET || active == TIM_SAMPLEOWN) && ActiveInfo.Project->IsSST() &&
		ActiveInfo.Project->GetTransSST() == NULL);
}

bool CParcelView::IsDeleteSSTTransferEnable()
{
	return (active == TIM_SAMPLETRANS && ActiveInfo.Trans->GetType() == C_TRANS_TYPE_SST);
}

bool CParcelView::IsAddTransferEnable()
{
	return ((active == TIM_SAMPLESET || active == TIM_SAMPLEOWN || active == TIM_SAMPLETRANS) &&
		ActiveInfo.Project->IsExistMaster() && !ActiveInfo.Project->IsSST());
}

bool CParcelView::IsEditTransferEnable()
{
	return (active == TIM_SAMPLETRANS && !ActiveInfo.Project->IsSST()
		&& ActiveInfo.Project->GetTransferKeepStatus(ActiveInfo.hSampleTrans) == C_KEEP_FREE);
}

bool CParcelView::IsDeleteTransferEnable()
{
	return (active == TIM_SAMPLETRANS && !ActiveInfo.Project->IsSST()
		&& ActiveInfo.Project->GetTransferKeepStatus(ActiveInfo.hSampleTrans) == C_KEEP_FREE);
}

bool CParcelView::IsReadOldSampleEnable()
{
	return false;
//	return (active == TIM_SAMPLESET || active == TIM_SAMPLEOWN);
}

bool CParcelView::IsGetStoreSampleEnable()
{
	return false;
//	return (active == TIM_SAMPLESET || active == TIM_SAMPLEOWN);
}

void CParcelView::AddNewModel(CModelData* ModData)
{
	HTREEITEM hParentItem = ActiveInfo.hModelSet;
	HTREEITEM hLastItem = ActiveInfo.Project->GetLastModelHItem();
	if(ModData != NULL)
	{
		HTREEITEM hItem = m_Tree.InsertItem(TVIF_TEXT, ModData->GetName(), 0, 0, 0, 0, 0,
			hParentItem, hLastItem);

		ModData->SetHItem(hItem);

		m_Tree.SetItemImage(hItem, 11, 11);

		m_Tree.Select(hItem, TVGN_CARET);
		m_Tree.Expand(hParentItem, TVE_EXPAND);
		m_Tree.SetItemImage(hParentItem, 7, 7);
	}
}

void CParcelView::CreateNewModel(int mode)
{
	HTREEITEM hParentItem = ActiveInfo.hModelSet;
	HTREEITEM hLastItem = ActiveInfo.Project->GetLastModelHItem();

	CModelData* ModData = (mode == COMMAND_MODE_CREATE) ? ActiveInfo.Project->AddNewModel()
														: ActiveInfo.Project->AddCopyModel(ActiveInfo.hModel);
	if(ModData != NULL)
	{
		HTREEITEM hItem = m_Tree.InsertItem(TVIF_TEXT, ModData->GetName(), 0, 0, 0, 0, 0,
			hParentItem, hLastItem);

		ModData->SetHItem(hItem);

		m_Tree.SetItemImage(hItem, 11, 11);

		m_Tree.Select(hItem, TVGN_CARET);
		m_Tree.Expand(hParentItem, TVE_EXPAND);
		m_Tree.SetItemImage(hParentItem, 7, 7);
	}
}

void CParcelView::ChangeModelSettings()
{
	HTREEITEM hItem = ActiveInfo.hModel;

	if(ActiveInfo.Project->ChangeModelSettings(hItem))
	{
		m_Tree.SetItemText(hItem, ActiveInfo.Model->GetName());
		SetViewMode(hItem);
	}
}

void CParcelView::DeleteModel()
{
	HTREEITEM hItem = ActiveInfo.hModel;
	CString Txt, fmt = ParcelLoadString(STRID_ASK_DELMODEL);
	Txt.Format(fmt, ActiveInfo.Model->GetName());
	if(!ParcelConfirm(Txt, true))
		return;

	if(!ActiveInfo.Project->DeleteModel(hItem))
		return;

	m_Tree.DeleteItem(hItem);

	if(ActiveInfo.Project->GetNModels() == 0)
	{
		m_Tree.Expand(ActiveInfo.hModelSet, TVE_COLLAPSE);
		m_Tree.SetItemImage(ActiveInfo.hModelSet, 10, 10);
	}
}

void CParcelView::ModelResult()
{
	HTREEITEM hItem = ActiveInfo.hModel;

	bool secv = ActiveInfo.Model->IsUseSECV();
	if(ActiveInfo.Model->GetAnalysisType() == C_ANALYSIS_QNT)
	{
		CString Txt = ParcelLoadString(STRID_ASK_CALCSECV);
		ActiveInfo.Model->SetUseSECV(ParcelConfirm(Txt, ActiveInfo.Model->IsUseSECV()));
	}

	if(ActiveInfo.Project->ModelResult(hItem))
	{
		SetViewMode(hItem);
	}
	
	ActiveInfo.Model->SetUseSECV(secv);
}

bool CParcelView::IsCreateModelEnable()
{
	return (active == TIM_PROJECT || active == TIM_MODELSET || active == TIM_MODEL);
}

bool CParcelView::IsCopyModelEnable()
{
	return (active == TIM_MODEL);
}

bool CParcelView::IsChangeModelSettingsEnable()
{
	return (active == TIM_MODEL && ActiveInfo.Project->GetModelKeepStatus(ActiveInfo.hModel) == C_KEEP_FREE);
}

bool CParcelView::IsDeleteModelEnable()
{
	return (active == TIM_MODEL && ActiveInfo.Project->GetModelKeepStatus(ActiveInfo.hModel) == C_KEEP_FREE);
}

bool CParcelView::IsModelResultEnable()
{
	return (active == TIM_MODEL);
}

void CParcelView::InsertNewMethod(CMethodData* MtdData)
{
	HTREEITEM hParentItem = ActiveInfo.hMethodSet;
	HTREEITEM hLastItem = ActiveInfo.Project->GetLastMethodHItem();

	if(MtdData != NULL)
	{
		HTREEITEM hItem = m_Tree.InsertItem(TVIF_TEXT, MtdData->GetName(), 0, 0, 0, 0, 0, hParentItem, hLastItem);

		MtdData->SetHItem(hItem);

		m_Tree.SetItemImage(hItem, 11, 11);

		m_Tree.Select(hItem, TVGN_CARET);
		m_Tree.Expand(hParentItem, TVE_EXPAND);
		m_Tree.SetItemImage(hParentItem, 7, 7);
	}
}

void CParcelView::CreateNewMethod(int mode)
{
	HTREEITEM hParentItem = ActiveInfo.hMethodSet;
	HTREEITEM hLastItem = ActiveInfo.Project->GetLastMethodHItem();

	CMethodData* MtdData = (mode == COMMAND_MODE_CREATE) ? ActiveInfo.Project->AddNewMethod()
														 : ActiveInfo.Project->AddCopyMethod(ActiveInfo.hMethod);
	if(MtdData != NULL)
	{
		HTREEITEM hItem = m_Tree.InsertItem(TVIF_TEXT, MtdData->GetName(), 0, 0, 0, 0, 0, hParentItem, hLastItem);

		MtdData->SetHItem(hItem);

		m_Tree.SetItemImage(hItem, 11, 11);

		m_Tree.Select(hItem, TVGN_CARET);
		m_Tree.Expand(hParentItem, TVE_EXPAND);
		m_Tree.SetItemImage(hParentItem, 7, 7);
	}
}

void CParcelView::ChangeMethodSettings()
{
	HTREEITEM hItem = ActiveInfo.hMethod;

	if(ActiveInfo.Project->ChangeMethodSettings(hItem))
	{
		m_Tree.SetItemText(hItem, ActiveInfo.Method->GetName());
		SetViewMode(hItem);
	}
}

void CParcelView::DeleteMethod()
{
	HTREEITEM hItem = ActiveInfo.hMethod;
	CString Txt, fmt = ParcelLoadString(STRID_ASK_DELMETHOD);
	Txt.Format(fmt, ActiveInfo.Method->GetName());
	if(!ParcelConfirm(Txt, false))
		return;

	if(!ActiveInfo.Project->DeleteMethod(hItem))
		return;

	m_Tree.DeleteItem(hItem);

	if(ActiveInfo.Project->GetNMethods() == 0)
	{
		m_Tree.Expand(ActiveInfo.hMethodSet, TVE_COLLAPSE);
		m_Tree.SetItemImage(ActiveInfo.hMethodSet, 10, 10);
	}
}

void CParcelView::ExportMethod(CWnd *wnd)
{
	if(ActiveInfo.Method == NULL)
		return;

	bool qnt = (ActiveInfo.Method->GetAnalysisType() == C_ANALYSIS_QNT);

	CString Title = (qnt) ? ParcelLoadString(STRID_STDTITLE_EXPORTMTDQNT) : ParcelLoadString(STRID_STDTITLE_EXPORTMTDQLT);
	CString Filter = (qnt) ? LoadString(IDS_MTDFILE_FILTER) : LoadString(IDS_MTDQLTFILE_FILTER);
	CString DefExt = (qnt) ? LoadString(IDS_MTDFILE_DEFEXT) : LoadString(IDS_MTDQLTFILE_DEFEXT);

	CString Folder = pRoot->GetBaseFolder();
	SetCurrentDirectory(Folder);
	CString FileName = MakePath(Folder, ActiveInfo.Method->GetName());
	FileName += DefExt;

	CFileDialog Dialog(FALSE, DefExt, FileName,
					   OFN_EXPLORER | OFN_HIDEREADONLY | OFN_PATHMUSTEXIST | OFN_OVERWRITEPROMPT,
					   Filter, wnd);

	Dialog.m_ofn.lpstrTitle = Title;

	if(Dialog.DoModal() == IDOK)
	{
		FileName = Dialog.GetPathName();
		if(FileName.IsEmpty())
		{
			ParcelError(ParcelLoadString(STRID_STDERR_FNEMPTY));
			return;
		}
		if(PathFileExists(FileName))
		{
			if(!ParcelConfirm(ParcelLoadString(STRID_STDERR_FILEALREADYEXIST), false))
				return;
		}

		ActiveInfo.Project->ExportMethod(ActiveInfo.hMethod, FileName);
	}
}

bool CParcelView::IsCreateMethodEnable()
{
	return ((active == TIM_PROJECT || active == TIM_METHODSET || active == TIM_METHOD) &&
		ActiveInfo.Project != NULL && ActiveInfo.Project->GetNModels() > 0);
}

bool CParcelView::IsCopyMethodEnable()
{
	return (active == TIM_METHOD);
}

bool CParcelView::IsChangeMethodSettingsEnable()
{
	return (active == TIM_METHOD);
}

bool CParcelView::IsDeleteMethodEnable()
{
	return (active == TIM_METHOD);
}

bool CParcelView::IsExportMethodEnable()
{
	return (active == TIM_METHOD);
}

void CParcelView::ChangeLanguage(int lang)
{
	SetLanguage(lang);
	pFrame->SetHelpFile(lang);

	RecreateColumns();

	CLoadDBWaitDlg dlg2(&m_Tree);
	dlg2.DoModal();

	pFrame->SetWindowText(ParcelLoadString(STRID_PARCEL_HEADER));

	pFrame->MakeMenu();

	pFrame->ShowWindow(SW_HIDE);
	pFrame->ShowWindow(SW_SHOW);
}

void CParcelView::OnTvnSelchangedTree1(NMHDR* /*pNMHDR*/, LRESULT* pResult)
{
//	LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);

	HTREEITEM hItem = m_Tree.GetSelectedItem();
	active = GetBranchInfo(hItem, ActiveInfo);

	SetViewMode(hItem);

	*pResult = 0;
}

void CParcelView::OnTvnItemexpandedTree1(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);

	BranchInfo Info;
	int select = GetBranchInfo(pNMTreeView->itemNew.hItem, Info);
	int image = 0;
	switch(select)
	{
	case TIM_ROOT:
		image = (Data.GetNDevices() > 0) ? ((pNMTreeView->action == TVE_COLLAPSE) ? 0 : 1) : 8;
		break;
	case TIM_DEVICE:
		image = (Info.Device->GetNProjects() > 0) ? ((pNMTreeView->action == TVE_COLLAPSE) ? 2 : 3) : 9;
		break;
	case TIM_PROJECT:
		image = (pNMTreeView->action == TVE_COLLAPSE) ? 4 : 5;
		break;
	case TIM_SAMPLESET:
		image = (pNMTreeView->action == TVE_COLLAPSE) ? 6 : 7;
		break;
	case TIM_MODELSET:
		image = (Info.Project->GetNModels() > 0) ? ((pNMTreeView->action == TVE_COLLAPSE) ? 6 : 7) : 10;
		break;
	case TIM_METHODSET:
		image = (Info.Project->GetNMethods() > 0) ? ((pNMTreeView->action == TVE_COLLAPSE) ? 6 : 7) : 10;
		break;
	}

	m_Tree.SetItemImage(pNMTreeView->itemNew.hItem, image, image);

	*pResult = 0;
}

void CParcelView::OnParamsListSelChange(NMHDR* /*pNMHDR*/, LRESULT *pResult)
{
	FillParamsLists(ActiveInfo.Project);

	*pResult = 0;
}

void CParcelView::OnSamplesTransListSelChange(NMHDR* /*pNMHDR*/, LRESULT *pResult)
{
	int sel = m_list_SamplesTrans.GetNextItem(-1, LVNI_SELECTED);
	CSampleData *Sam = ActiveInfo.Trans->GetSample(sel);

	FillSpectraTransList(Sam);

	*pResult = 0;
}

void CParcelView::OnSamplesTransListColumnClick(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NMLISTVIEW *pLV = (NMLISTVIEW *) pNMHDR;

	int item = pLV->iItem;

	if(ActiveInfo.Project == NULL)
	{
		*pResult = 0;
		return;
	}

	CString name = ActiveInfo.Project->GetSamSortName();
	bool bdir = ActiveInfo.Project->GetSamSortDir();
	int ntype = 0;
	int idxpar = 0;

	bool findsort = false;
	if(item == 0)	// sample name
	{
		if(name.IsEmpty())
		{
			bdir = !bdir;
		}
		ntype = C_SORT_SAMPLE;
		findsort = true;
	}
	else if(item == m_nLSampTimeCol)	//	time
	{
		if(name == cSortTime)
		{
			bdir = !bdir;
		}
		ntype = C_SORT_TIME;
		findsort = true;
	}
	else if(item >= m_nLSampParStart)
	{
		idxpar = item - m_nLSampParStart;
		CString par = ActiveInfo.Project->GetParam(idxpar)->GetName();
		if(!par.Compare(name))
		{
			bdir = !bdir;
		}
		ntype = C_SORT_PARAM;
		findsort = true;
	}
	else
	{
		findsort = false;
	}

	if(findsort && ActiveInfo.Project->SortSamples(ntype, bdir, idxpar))
	{
		HTREEITEM hItem = (ActiveInfo.hSampleTrans != NULL) ? ActiveInfo.hSampleTrans : ActiveInfo.hSampleSet;
		m_Tree.Select(hItem, TVGN_CARET);
		SetViewMode(hItem, false);
	}

	*pResult = 0;
}

void CParcelView::SetSamplesSortIcons()
{
	int iCol = -1;
	bool dir = true;

	CProjectData* Prj = ActiveInfo.Project;
	if(Prj != NULL)
	{
		CString name = Prj->GetSamSortName();
		dir = Prj->GetSamSortDir();

		if(name.IsEmpty())
		{
			iCol = 0;
		}
		else if(name == cSortTime)
		{
			iCol = m_nLSampTimeCol;
		}
		else
		{
			ItPar it;
			int i;
			for(i=0, it=Prj->Params.begin(); it!=Prj->Params.end(); ++it, i++)
			{
				if(!name.Compare(it->GetName()))
				{
					iCol = m_nLSampParStart + i;
					break;
				}
			}
		}
	}

	m_list_SamplesTrans.ShowSortIcons(iCol, dir);
}

// CParcelView diagnostics

#ifdef _DEBUG
void CParcelView::AssertValid() const
{
	CFormView::AssertValid();
}

void CParcelView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CParcelDoc* CParcelView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CParcelDoc)));
	return (CParcelDoc*)m_pDocument;
}
#endif //_DEBUG
