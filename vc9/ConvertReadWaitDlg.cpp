#include "stdafx.h"

#include "ConvertReadWaitDlg.h"
#include "BranchInfo.h"

IMPLEMENT_DYNAMIC(CConvertReadWaitDlg, CWaitingDlg)

CConvertReadWaitDlg::CConvertReadWaitDlg(CLoadOldDatas* pload, ProjectOldData* papj, CString fname, CWnd* pParent)
	: CWaitingDlg(pParent)
{
	LoadData = pload;
	OldApj = papj;
	FileName = fname;

	Comment = ParcelLoadString(STRID_CONVERT_READ_WAIT_COMMENT);
}

CConvertReadWaitDlg::~CConvertReadWaitDlg()
{
}

void CConvertReadWaitDlg::DoDataExchange(CDataExchange* pDX)
{
	CWaitingDlg::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CConvertReadWaitDlg, CWaitingDlg)
END_MESSAGE_MAP()


BOOL CConvertReadWaitDlg::OnInitDialog()
{
	CWaitingDlg::OnInitDialog();

	return true;
}

void CConvertReadWaitDlg::OK()
{
	CWaitingDlg::OK();
}

void CConvertReadWaitDlg::Stop()
{
	CWaitingDlg::Stop();
}

void CConvertReadWaitDlg::Work()
{
	ParcelWait(true);
	
	bool res = LoadData->LoadOldProject(FileName, (*OldApj));

	ParcelWait(false);

	if(res)
		OK();
	else
		Stop();
}
