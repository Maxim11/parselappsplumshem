#include "stdafx.h"

#include "LoadSpectraPBDlg.h"
#include "BranchInfo.h"


IMPLEMENT_DYNAMIC(CLoadSpectraPBDlg, CProgressBarDlg)

CLoadSpectraPBDlg::CLoadSpectraPBDlg(VLSI* psam, CWnd* pParent) : CProgressBarDlg(pParent)
{
	PSam = psam;
	NAll = 0;
	CurSmpl = 0;
	ipb = 0;
}

CLoadSpectraPBDlg::~CLoadSpectraPBDlg()
{
}

void CLoadSpectraPBDlg::DoDataExchange(CDataExchange* pDX)
{
	CProgressBarDlg::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CLoadSpectraPBDlg, CProgressBarDlg)
END_MESSAGE_MAP()


BOOL CLoadSpectraPBDlg::OnInitDialog()
{
	CProgressBarDlg::OnInitDialog();

	CString s;

	NAll = 0;
	ItLSI it;
	for(it=PSam->begin(); it!=PSam->end(); ++it)
		NAll += int(it->Nums.size());

	SetHeader(ParcelLoadString(STRID_LOAD_SPECTRA_PB_HDR));
	SetComment(ParcelLoadString(STRID_LOAD_SPECTRA_PB_COMMENT));
	SetRangeStep(0, NAll, 1);
	SetStatus(ParcelLoadString(STRID_LOAD_SPECTRA_PB_STATUS));

	GetDlgItem(IDC_PAUSE)->EnableWindow(false);

	return true;
}

void CLoadSpectraPBDlg::OK()
{
	CProgressBarDlg::OK();
}

void CLoadSpectraPBDlg::Stop()
{
	CProgressBarDlg::Stop();
}

void CLoadSpectraPBDlg::Work()
{
	CurSmpl = 0;
	ipb = 0;
	passtime = 0;

	LoadSpectra();
}

void CLoadSpectraPBDlg::LoadSpectra()
{
	ParcelProcess(true);

	__time64_t ctime, stime;
	_time64(&stime);

	CString s;
	bool bStop = false, bPause = false;
	int i, Nc = int(PSam->size());
	for(i=CurSmpl; i<Nc; i++)
	{
		bStop = CheckStopPressed();
		if(bStop)
			break;
		bPause = CheckPausePressed();
		if(bPause)
		{
			_time64(&ctime);
			passtime += int(ctime - stime);
			break;
		}

		(*PSam)[i].pSam->LoadSpectraData(&((*PSam)[i].Nums));

		CurSmpl++;

		ipb += int((*PSam)[i].Nums.size());
		SetPosition(ipb);

		_time64(&ctime);
		int elapsed = passtime + int(ctime - stime);
		int estimated = (ipb > 0) ? int(elapsed * NAll / ipb) : 0;

		SetElapsed(elapsed);
		SetEstimated(estimated);
		UpdateAllWindows();
	}

	ParcelProcess(false);

	if(bPause)
		return;
	if(bStop)
		Stop();
	else
		OK();
}

void CLoadSpectraPBDlg::ResumeWork()
{
	LoadSpectra();
}
