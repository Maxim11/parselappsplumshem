#ifndef _DELETE_PROJECT_WAIT_DLG_H_
#define _DELETE_PROJECT_DLG_H_

#pragma once

#include "WaitingDlg.h"
#include "DeviceData.h"


///////////////////////////////////////////////////////////////////////////////////////////////////
// ������ ��������� �������� ���������� ����������������� �������

class CDeleteProjectWaitDlg : public CWaitingDlg
{
	DECLARE_DYNAMIC(CDeleteProjectWaitDlg)

public:

	CDeleteProjectWaitDlg(CDeviceData* dev, CString name, CWnd* pParent = NULL);		// �����������
	virtual ~CDeleteProjectWaitDlg();									// ����������

	enum { IDD = CWaitingDlg::IDD };	// ������������� �������

protected:

	CDeviceData* DevData;	// ��������� �� ������, � ������� ������������ ��������
	CString PrjName;

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();						// ������������� �������

	virtual void OK();				// ��������� �������� ����� �� �������
	virtual void Stop();			// ��������� ����������� ����� �� �������
	virtual void Work();			// ������ �������� �������

	DECLARE_MESSAGE_MAP()
};

#endif
