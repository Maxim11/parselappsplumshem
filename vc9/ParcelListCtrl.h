#ifndef _PARCEL_LIST_CTRL_H_
#define _PARCEL_LIST_CTRL_H_

#include "ListCtrlWithCustomDraw.h"
#include "resource.h"


#define LCF_SI_OK	0
#define LCF_SI_ERR	1

typedef int (LC_SI_CALLBACK)(int, int, void*);

#define LCF_CB_SI_TXTCOLOR		0
#define LCF_CB_SI_BGCOLOR		1
#define LCF_CB_SI_NUM			2

struct LC_SI_CALLBACKS
{
	LC_SI_CALLBACK* pFunc[LCF_CB_SI_NUM];
	UINT nFlags;
};

class CParcelListCtrl : public CListCtrlWithCustomDraw
{
public:

	CParcelListCtrl();

	virtual ~CParcelListCtrl();

	CImageList* pSortIcons;

	COLORREF CurTxtColor;
	COLORREF CurBGColor;

public:

	virtual bool IsDraw();
	virtual bool OnDraw(CDC* pDC, const CRect& rc);
	virtual bool IsNotifyItemDraw();
	virtual bool IsNotifySubItemDraw(int nItem, UINT nState, LPARAM lParam);
	virtual COLORREF TextColorForSubItem(int nItem, int nSubItem, UINT nState, LPARAM lParam);
	virtual COLORREF BkColorForSubItem(int nItem, int nSubItem, UINT nState, LPARAM lParam);

	void SetIconList();
	void ShowSortIcons(int iCol, bool isUp);

	LC_SI_CALLBACKS Callbacks;
	void ResetUserCallbacks();
	void SetUserCallback(UINT nEvent, LC_SI_CALLBACK *pCallback);
	int EnableUserCallback(UINT nEvent, bool bEnable);

protected:

	DECLARE_MESSAGE_MAP()
};

#endif
