#ifndef _PARCEL_MODEL_CALCULATE_H_
#define _PARCEL_MODEL_CALCULATE_H_

#include "ProjectData.h"
#include "SpLumChem.h"

const int CALCERR_OK		= 0;
const int CALCERR_NOSPEC	= 1;
const int CALCERR_NOREF		= 2;
const int CALCERR_NOCOMP	= 3;
const int CALCERR_MATH		= 4;
const int CALCERR_QLTFACT	= 5;
const int CALCERR_QNTLOAD	= 6;
const int CALCERR_QLTLOAD	= 7;
const int CALCERR_SAMPLE	= 8;

class CModelCalculate
{
public:

	CModelCalculate(CModelData* Mod);
	~CModelCalculate();

protected:

	CModelData* Model;
	CProjectData* Prj;
	CTransferData* Trans;

	bool IsAvr;
	bool IsHSO;

	bool IsTrans;
	bool IsTransNormal;

public:

	bool CalculateModel(bool isExp = false);

protected:

	void FillSampleProxyOwn(vector<CSampleProxy>& Samples, bool IsPre = false);
	void FillSampleProxyTrans(vector<CSampleProxy>& Samples);
	void FillSampleProxySEVOwn(vector<CSampleProxy>& Samples, bool IsPre = false);
	void FillSampleProxySEVTrans(vector<CSampleProxy>& Samples);
	void ExtractFromSampleProxyOwn(vector<CSampleProxy>& SamplesOwn, vector<CSampleProxy>& Samples);

	void FillFreqScaleModel(CFreqScale& FreqScale);
	void FillFreqScaleTrans(CFreqScale& FreqScale);
	void FillSpecPreproc(CSpecPreproc& prePar);
	void FillSpecPreprocData(CSpecPreprocData_Parcel& preData);
	void FillModelMMP(MODEL_MMP& MMP);
	void FillModelResult(CQnt_Parcel *pQnt, CModelResult *pMRes);
	void FillHSOModelResult(CQnt_Parcel *pQnt, CQnt_Parcel *pQlt, CModelResult *pMRes);
	void FillQltModelResult(CQnt_Parcel *pQnt, CModelResult *pMRes);

	void FillModelResultExp(CQnt_Parcel *pQnt, CQnt_Parcel *pQlt, CModelResult *pMRes);
	void FillQltModelResultExp(CQnt_Parcel *pQlt, CModelResult *pMRes);

	void CalcAllTrendLine(CModelResult *pMRes);

	double GetT0(int diff);
	CString GetTmpParName(int ind);

	void CalcError(int ecode, const TCHAR *data);
	CString GetErrorStr(int ecode);
};

#endif
