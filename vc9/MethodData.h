#ifndef _PARCEL_METHOD_DATA_H_
#define _PARCEL_METHOD_DATA_H_

#pragma once

#include "PRDBMethod.h"

// ��������� ���� �������

const int C_MTD_ANALYSIS_QNT = 0;	// �������������� ������
const int C_MTD_ANALYSIS_QLT = 1;	// ������������ ������

// ��������� ������� �������

struct AnalyseRule
{
	CString Name;			// ��� ����������
	CString MainModel;		// ������ �� ���������
	VStr AddModels;			// ������ �������������� �������
	double Moisture;		// ����������� ���������
	bool UseStdMoisture;	// ������� ������������� ����������� ���������

	AnalyseRule(CString name):Name(name),MainModel(cEmpty),Moisture(0.),UseStdMoisture(false)
	{};
	~AnalyseRule()
	{
		AddModels.clear();
	};
	void Copy(AnalyseRule& rule);
	CString AnalyseRule::GetStdMoistureStr();

};

typedef vector<AnalyseRule> VRul;
typedef vector<AnalyseRule>::iterator ItRul;

// ��������� ������� ������� ������ ������������� �������

struct QltFormulaElement
{
	int Type;			// ��� ��������
	int ModelInd;		// �������������� �������� (������ ������)

	QltFormulaElement(int type, int mind):Type(type),ModelInd(mind)
	{};
};

typedef vector<QltFormulaElement> VQFE;
typedef vector<QltFormulaElement>::iterator ItQFE;

struct QltFormula
{
	VQFE Data;

	~QltFormula()
	{
		Clear();
	};

	void Copy(QltFormula& data);
	int CheckFormula(int nMod);
	CString GetCheckFormulaResult(int nMod);
	bool IsNextElementEnable(int elem);
	CString GetAsString();
	void Clear();
	bool AddElement(int nMod, int elem, int iMod = -1);
	void DeleteLast();
	int GetLength();

};

///////////////////////////////////////////////////////////////////////////////////////////////////
// ����� ��� �������� ������ ����������

class CMethodData
{
public:

	CMethodData(HTREEITEM hParent);		// �����������
	~CMethodData();						// ����������

	void Copy(CMethodData& data);		// ����������� ������

protected:

	HTREEITEM hItem;					// ������ ���� � ������, ���������������� ������

	HTREEITEM hParentItem;				// ������ ���� � ������, ���������������� ������������� �������

	CString Name;						// ��� ������

	CTime Date;							// ���� � ����� ��������
	CString MoisParam;					// ��� ���������� "��� ���������"
	int LowTemp;						// ������ ������ ����������� �������
	int UpperTemp;						// ������� ������ ����������� �������
	int MaxDiffTemp;					// ���������� ������� ����. ������� �� ����. ���������� �����

	int AnalysisType;					// ��� ������� (������)

public:

	VRul Rules;							// ������ ������ �������

	MapIntStr ModelQltNames;			// ������ ���� ������� ��� ������������� �������
	QltFormula Formula;

public:

	HTREEITEM GetHItem();					// �������� ������ ���� � ������, ��������������� ������
	void SetHItem(HTREEITEM item);			// ���������� ������ ���� � ������, ��������������� ������

	HTREEITEM GetParentHItem();				// �������� ������ ���� � ������, ��������������� �������
	void SetParentHItem(HTREEITEM item);

	CString GetName();						// �������� ��� ������
	void SetName(CString name);				// ���������� ��� ������

	CTime GetDate();						// �������� ���� � ����� �������� ������
	void SetDate();							// ���������� ������� ���� � ����� ��� ���� � ����� �������� ������

	CString GetMoisParam();					// �������� ��� ���������� "��� ���������"
	void SetMoisParam(CString param);		// ���������� ��� ���������� "��� ���������"

	int GetLowTemp();						// �������� ������ ������ ����������� �������
	void SetLowTemp(int temp);				// ���������� ������ ������ ����������� �������

	int GetUpperTemp();						// ���������� ������� ������ ����������� �������
	void SetUpperTemp(int temp);			// �������� ������� ������ ����������� �������

	int GetMaxDiffTemp();					// �������� ���������� ������� ����. ������� �� ����. �����
	void SetMaxDiffTemp(int temp);			// ���������� ���������� ������� ����. ������� �� ����. �����

	int GetAnalysisType();					// �������� ��� ������� ��� ������ � ��� 9������ ��������� ��������)
	bool SetAnalysisType(int ind);			// ���������� ��� ������� �� ������� � ���

	int GetNRules();						// �������� ����� ������ �������
	AnalyseRule* GetRule(int ind);			// �������� ������� ������� �� ������� � ������
	AnalyseRule* GetRule(CString name);		// �������� ������� ������� �� ����� ����������
	bool IsMethodRuleExist(CString name);	// �������� ������� ������������� ������� �������
	bool AddRule(AnalyseRule& rule);		// ������� ����� ������� �������
	bool DeleteRule(int ind);				// ������� ������� ������� �� ������� � ������
	bool DeleteRule(CString name);			// ������� ������� ������� �� ����� ����������
	bool IsQntModelUsed(CString name);		// ������������ �� ������ � ������
	int GetQntModelNames(VStr& names);		// ������ ���� ������� � ������

	int GetNModelQltNames();				// �������� ����� ���� ������� ��� ������������� �������
	CString GetModelQltName(int ind);		// �������� ��� ������ ��� ������������� ������� �� ������� � ������
	bool AddModelQltName(int ind, CString name);	// �������� ��� ������ ��� ������������� �������
	bool DeleteModelQltName(int ind);		// ������� ��� ������ ��� ������������� ������� �� ������� � ������
	bool DeleteModelQltName(CString name);	// ������� ��� ������ ��� ������������� �������
	void DeleteAllModelQltNames();			// ������� ��� ����� ������� ��� ������������� �������
	bool IsQltModelUsed(CString name);		// ������������ �� ������ � ������

	void GetDataForBase(prdb::PRMethod& Obj);
	void SetDataFromBase(prdb::PRMethod& Obj);

	bool GetDataForExportQnt(mtddb::PRMethodQnt& Obj);
	bool GetDataForExportQlt(mtddb::PRMethodQlt& Obj);

};

#endif
