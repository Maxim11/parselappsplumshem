#ifndef _CONVERT_ACCEPT_DLG_H_
#define _CONVERT_ACCEPT_DLG_H_

#pragma once

#include "Resource.h"
#include "LoadOldDatas.h"

///////////////////////////////////////////////////////////////////////////////////////////////////
// ������ ����������� ����������� ����������� ������ ������

class CConvertAcceptDlg : public CDialog
{
	DECLARE_DYNAMIC(CConvertAcceptDlg)

public:

	CConvertAcceptDlg(CLoadOldDatas* pload, CWnd* pParent = NULL);	// �����������
	virtual ~CConvertAcceptDlg();									// ����������

	enum { IDD = IDD_CONVERT_ACCEPT };	// ������������� �������

	CListCtrl m_list;		// ������ ������ �����������
	CButton m_NoNum;		// ��������� �������� "��������� ������� ��� ������ �������"
	CButton m_CheckWave;	// ��������� �������� "��������� ������� � ������������ ������ �����"
	CEdit m_Unit;

protected:

	CLoadOldDatas* pLoad;	// ��������� �� ����������� ������ ������

	CString m_vUnit;

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();						// ������������� �������

	virtual void OnOK()
	{};
	virtual void OnCancel();			// ��������� ������� "�������� ���� � ����� �� �������"

	afx_msg void OnAccept();			// ��������� ������� "������� ������ � ����� �� �������"
	afx_msg void OnHelp();				// ��������� ������� "������� �������"

	void FillList();					// ��������� ������� �� ������� ������ �����������

	DECLARE_MESSAGE_MAP()
};

#endif
