#include "stdafx.h"

#include "OptimCalculate.h"
#include "BranchInfo.h"
#include "CalcOptimPBDlg.h"

#include <math.h>

static void *pCalibQnt = NULL;

COptimCalculate::COptimCalculate()
{
}

COptimCalculate::~COptimCalculate()
{
}

void COptimCalculate::InitCalc(CModelData* mod)
{
	StartModel = mod;
	ParentPrj = GetProjectByHItem(StartModel->GetParentHItem());
	Trans = ParentPrj->GetTransfer(StartModel->GetTransName()); 

	ParamName = StartModel->GetModelParam(0)->ParamName;

	int i;

	SamplesForCalib.clear();
	for(i=0; i<StartModel->GetNSamplesForCalib(); i++)
	{
		ModelSample* MSam = StartModel->GetSampleForCalib(i);
		SamplesForCalib.push_back(*MSam);
	}

	SamplesForValid.clear();
	for(i=0; i<StartModel->GetNSamplesForValid(); i++)
	{
		ModelSample* MSam = StartModel->GetSampleForValid(i);
		SamplesForValid.push_back(*MSam);
	}
}

int COptimCalculate::SetInfo(OptimStepSettings* step, OptimResults* res, VStr* preps, int mode)
{
	StepData = step;
	Results = res;

	AllPreprocs.clear();
	copy(preps->begin(), preps->end(), inserter(AllPreprocs, AllPreprocs.begin()));

	Mode = mode;

	ClearResults();

	SetCalibValidIncludes();
	SetPoints();

	return CalcNumModels();
}

ModelSample* COptimCalculate::GetSampleForCalib(CString name, CString transname)
{
	int i;
	ItModSam it;
	for(i=0, it=SamplesForCalib.begin(); it!=SamplesForCalib.end(); ++it, i++)
		if(!it->SampleName.Compare(name) && !it->TransName.Compare(transname))
			return &(*it);

	return NULL;
}

ModelSample* COptimCalculate::GetSampleForValid(CString name, CString transname)
{
	int i;
	ItModSam it;
	for(i=0, it=SamplesForValid.begin(); it!=SamplesForValid.end(); ++it, i++)
		if(!it->SampleName.Compare(name) && !it->TransName.Compare(transname))
			return &(*it);

	return NULL;
}

void COptimCalculate::GetSamplesCalibValid(VModSam& calib, VModSam& valid)
{
	calib.clear();
	valid.clear();
	copy(SamplesForCalib.begin(), SamplesForCalib.end(), inserter(calib, calib.begin()));
	copy(SamplesForValid.begin(), SamplesForValid.end(), inserter(valid, valid.begin()));
}

int COptimCalculate::GetModelType()
{
	return StepData->ModelType;
}

int COptimCalculate::GetSpectrumType()
{
	return StepData->SpectrumType;
}

int COptimCalculate::GetModelTransType()
{
	return Trans->GetType();
}

int COptimCalculate::GetNAllSpecPoints()
{
	return NAllPoints;
}

void COptimCalculate::GetLowUpperPoints(int left, int right, int& low, int& upper)
{
	VDbl Points;
	ParentPrj->ExtractSpecPoints(left, right, Points);
	int N = int(Points.size());
	low = ParentPrj->GetSpecPointInd(Points[0]);
	upper = ParentPrj->GetSpecPointInd(Points[N-1]);
}

int COptimCalculate::GetNUsePoints()
{
	return NAllPoints - int(AllExcPoints.size());
}

bool COptimCalculate::SetPoints()
{
	int specmin = max(Trans->GetLowLimSpec(), StepData->LeftMean - StepData->LeftPlus);
	int specmax = min(Trans->GetUpperLimSpec(), StepData->RightMean + StepData->RightPlus);

	ParentPrj->ExtractSpecPoints(specmin, specmax, AllSpecPoints);
	NAllPoints = int(AllSpecPoints.size());

	int indS = ParentPrj->GetSpecPointInd(AllSpecPoints[0]);
	int indF = ParentPrj->GetSpecPointInd(AllSpecPoints[NAllPoints - 1]);

	AllExcPoints.clear();
	ItSInt it;
	for(it=StepData->ExcPoints.begin(); it!=StepData->ExcPoints.end(); ++it)
	{
		if((*it) < indS)  continue;
		if((*it) > indF)  break;

		AllExcPoints.insert(*it);
	}

	return true;
}

void COptimCalculate::GetExcPoints(SInt& points)
{
	points.clear();
	copy(AllExcPoints.begin(), AllExcPoints.end(), inserter(points, points.begin()));
}

void COptimCalculate::GetCrits(VInt &crits)
{
	crits.clear();
	copy(Results->Crits.begin(), Results->Crits.end(), inserter(crits, crits.begin()));
}

int COptimCalculate::GetMainCrit()
{
	return StepData->MainCrit;
}

void COptimCalculate::CalcMinMaxConc(double& min, double& max)
{
	min = max = 0.;
	ItModSam it;
	for(it=SamplesForCalib.begin(); it!=SamplesForCalib.end(); ++it)
	{
		CTransferData *pTrans = ParentPrj->GetTransfer(it->TransName);
		CSampleData *Sam = pTrans->GetSample(it->SampleName);
		ReferenceData* Ref = Sam->GetReferenceData(ParamName);

		if(it == SamplesForCalib.begin())
			min = max = Ref->Value;
		else
		{
			if(min > Ref->Value)  min = Ref->Value;
			if(max < Ref->Value)  max = Ref->Value;
		}
	}
}

void COptimCalculate::SetCalibValidIncludes()
{
	ItOptSpec2 it;
	for(it=StepData->Calib.begin(); it!=StepData->Calib.end(); ++it)
	{
		ModelSample* ModSam = GetSampleForCalib(it->SampleName, it->TransName);
		ModSam->Spectra[it->Num] = it->Use;
	}

	for(it=StepData->Valid.begin(); it!=StepData->Valid.end(); ++it)
	{
		ModelSample* ModSam = GetSampleForValid(it->SampleName, it->TransName);
		ModSam->Spectra[it->Num] = it->Use;
	}
}

void COptimCalculate::ExcAllBadSpectra()
{
	ItBOSp2 itB;
	for(itB=BadSpectra.begin(); itB!=BadSpectra.end(); itB++)
	{
		ModelSample* MSam = GetSampleForCalib(itB->SampleName, itB->TransName);
		if(MSam == NULL) continue;
		MSam->Spectra[itB->Num] = false;
/*
		OptimSpectrum2* Calib = StepData->GetCalib(itB->SampleName, itB->TransName, itB->Num);
		if(Calib == NULL) continue;
		Calib->Use = false;
*/
	}
}

void COptimCalculate::MakeSets()
{
	int tmpI, count, i;
	double tmpD;
	VInt VtmpI;
	ItInt itI;
	VDbl VtmpD;
	ItDbl itD;

	LeftSet.clear();
	RightSet.clear();
	NCompSet.clear();
	HiperSet.clear();
	NHarmSet.clear();
	KfnlSet.clear();

	if(Mode != C_OPTIM_CALC_ALL_MODELS_2)
	{
		LeftSet.insert(StepData->LeftMean);
		RightSet.insert(StepData->RightMean);
		NCompSet.insert(StepData->NCompMean);
		if(StepData->ModelType == C_MOD_MODEL_HSO)
		{
			HiperSet.insert(StepData->HiperMean);
			NHarmSet.insert(StepData->NHarmMean);
			KfnlSet.insert(StepData->KfnlMean);
		}
		else
		{
			HiperSet.insert(0.);
			NHarmSet.insert(0);
			KfnlSet.insert(0.);
		}

		return;
	}

	LeftSet.insert(StepData->LeftMean);
	if(StepData->LeftStep > 0)
	{
		count = int(StepData->LeftPlus / StepData->LeftStep);
		for(i=1; i<=count; i++)
		{
			tmpI = StepData->LeftMean - i * StepData->LeftStep;
			if(tmpI >= Trans->GetLowLimSpec())  LeftSet.insert(tmpI);
			tmpI = StepData->LeftMean + i * StepData->LeftStep;
			if(tmpI <= Trans->GetUpperLimSpec())  LeftSet.insert(tmpI);
		}
	}

	RightSet.insert(StepData->RightMean);
	if(StepData->RightStep > 0)
	{
		count = int(StepData->RightPlus / StepData->RightStep);
		for(i=1; i<=count; i++)
		{
			tmpI = StepData->RightMean - i * StepData->RightStep;
			if(tmpI >= Trans->GetLowLimSpec())  RightSet.insert(tmpI);
			tmpI = StepData->RightMean + i * StepData->RightStep;
			if(tmpI <= Trans->GetUpperLimSpec())  RightSet.insert(tmpI);
		}
	}

	if(StepData->ModelType == C_MOD_MODEL_PLS || StepData->ModelType == C_MOD_MODEL_PCR)
	{
		NCompSet.insert(StepData->NCompMean);
		if(StepData->NCompStep > 0)
		{
			count = int(StepData->NCompPlus / StepData->NCompStep);
			for(i=1; i<=count; i++)
			{
				tmpI = StepData->NCompMean - i * StepData->NCompStep;
				if(tmpI >= 1)  NCompSet.insert(tmpI);
				tmpI = StepData->NCompMean + i * StepData->NCompStep;
				if(tmpI <= 100)  NCompSet.insert(tmpI);
			}
		}
	}
	else
		NCompSet.insert(StepData->NCompMean);

	if(StepData->ModelType == C_MOD_MODEL_HSO)
	{
		HiperSet.insert(StepData->HiperMean);
		if(StepData->HiperStep > 1.e-9)
		{
			count = int(StepData->HiperPlus / StepData->HiperStep);
			for(i=1; i<=count; i++)
			{
				tmpD = StepData->HiperMean - i * StepData->HiperStep;
				if(tmpD >= 0.0001)  HiperSet.insert(tmpD);
				tmpD = StepData->HiperMean + i * StepData->HiperStep;
				if(tmpD <= 1.e9)  HiperSet.insert(tmpD);
			}
		}

		if(StepData->NHarmType == C_OPTIM_HQO_PNT || StepData->NHarmType == C_OPTIM_HQO_HALL)
			NHarmSet.insert(0);
		if(StepData->NHarmType == C_OPTIM_HQO_HARM || StepData->NHarmType == C_OPTIM_HQO_HALL)
		{
			NHarmSet.insert(StepData->NHarmMean);
			if(StepData->NHarmStep > 0)
			{
				count = int(StepData->NHarmPlus / StepData->NHarmStep);
				for(i=1; i<=count; i++)
				{
					tmpI = StepData->NHarmMean - i * StepData->NHarmStep;
					if(tmpI >= 3)  NHarmSet.insert(tmpI);
					tmpI = StepData->NHarmMean + i * StepData->NHarmStep;
					if(tmpI <= 60)  NHarmSet.insert(tmpI);
				}
			}
		}

		if(StepData->KfnlType == C_OPTIM_HQO_LIN || StepData->KfnlType == C_OPTIM_HQO_LALL)
			KfnlSet.insert(0.);
		if(StepData->KfnlType == C_OPTIM_HQO_NLN || StepData->KfnlType == C_OPTIM_HQO_LALL)
		{
			KfnlSet.insert(StepData->KfnlMean);
			if(StepData->KfnlStep > 0)
			{
				count = int(StepData->KfnlPlus / StepData->KfnlStep);
				for(i=1; i<=count; i++)
				{
					tmpD = StepData->KfnlMean - i * StepData->KfnlStep;
					if(tmpD >= 0.1)  KfnlSet.insert(tmpD);
					tmpD = StepData->KfnlMean + i * StepData->KfnlStep;
					if(tmpD <= 1.0)  KfnlSet.insert(tmpD);
				}
			}
		}
	}
	else
	{
		HiperSet.insert(0.);
		NHarmSet.insert(0);
		KfnlSet.insert(0.);
	}
}

int COptimCalculate::CalcNumModels()
{
	int NModels = 0;

	MakeSets();

	if(Mode == C_OPTIM_TEST_EXC_POINTS_2)
		return 2 * GetNUsePoints();
	if(Mode == C_OPTIM_TEST_ANALIZE_OUTS_2)
		return 1;

	int iAv;
	bool bAv;
	for(iAv=0; iAv<=1; iAv++)
	{
		bAv = (iAv == 0);

		int iSp;
		int sSp = (StepData->SpectrumType == C_MOD_SPECTRUM_TRA || StepData->SpectrumType == C_MOD_SPECTRUM_BOTH) ?
			C_MOD_SPECTRUM_TRA : C_MOD_SPECTRUM_ABS;
		int fSp = (StepData->SpectrumType == C_MOD_SPECTRUM_ABS || StepData->SpectrumType == C_MOD_SPECTRUM_BOTH) ?
			C_MOD_SPECTRUM_ABS : C_MOD_SPECTRUM_TRA;
		for(iSp=sSp; iSp<=fSp; iSp++)
		{
			ItSInt itRL, itRR;
			for(itRL=LeftSet.begin(); itRL!=LeftSet.end(); ++itRL)
			{
				int RangeLeft = (*itRL);
				for(itRR=RightSet.begin(); itRR!=RightSet.end(); ++itRR)
				{
					int RangeRight = (*itRR);
					if(RangeLeft >= RangeRight)
						continue;

					ItStr itPrep;
					for(itPrep=AllPreprocs.begin(); itPrep!=AllPreprocs.end(); ++itPrep)
					{
						CString Prep = (*itPrep);
						if(Prep.IsEmpty())
						{
							if(bAv)
								continue;
						}
						else
							if(Prep[0] == 'A')
							{
								if(!bAv)
									continue;
							}
							else
								if(bAv)
									continue;

						ItSInt itComp;
						for(itComp=NCompSet.begin(); itComp!=NCompSet.end(); ++itComp)
						{
							ItSDbl itHiper;
							for(itHiper=HiperSet.begin(); itHiper!=HiperSet.end(); ++itHiper)
							{
								ItSInt itNHarm;
								for(itNHarm=NHarmSet.begin(); itNHarm!=NHarmSet.end(); ++itNHarm)
								{
									ItSDbl itKfnl;
									for(itKfnl=KfnlSet.begin(); itKfnl!=KfnlSet.end(); ++itKfnl)
									{
										NModels++;
									}
								}
							}
						}
					}
				}
			}
		}
	}

	return NModels;
}

int COptimCalculate::GetNCalcModels()
{
	return Results->NCalcModels;
}

void COptimCalculate::CalculateAllModels(bool byTmpl, int istep, int nstep)
{
	IsBreak = false;

	bool IsErr = false;
	if(Mode == C_OPTIM_TEST_ANALIZE_OUTS_2)
	{
		CModelData ModData(StartModel->GetParentHItem());
		ModData.Copy(*StartModel);

		ModData.SetModelType(StepData->ModelType);
		ModData.SetSpectrumType(StepData->SpectrumType);
		ModData.SetAllPreproc(AllPreprocs[0]);

		ModData.SetLowSpecRange(StepData->LeftMean);
		ModData.SetUpperSpecRange(StepData->RightMean);
		ModData.GetSpecPoints();
		ModData.SetExcPoints(AllExcPoints);

		ModData.SetNCompQnt(StepData->NCompMean);
		if(ModData.GetModelType() == C_MOD_MODEL_HSO)
		{
			ModData.SetDimHiper(StepData->HiperMean);
			ModData.SetNHarm(StepData->NHarmMean);
			ModData.SetKfnl(StepData->KfnlMean);
		}

		ModData.DeleteAllSamplesForCalib();
		ItModSam itSC;
		for(itSC=SamplesForCalib.begin(); itSC!=SamplesForCalib.end(); ++itSC)
		{
			ModData.AddSampleForCalib(&(*itSC));
		}

		if(StepData->Outs.chSECV)
			ModData.SetUseSECV(true);
		if(!ModData.CalculateModel())
			IsErr = true;
		else
		{
			CModelResult* ModRes = &ModData.ModelResult;
			ParamSumResult *pPSR = ModRes->GetPSResult(0);

			SInt ExcSp;
			int i, N = ModRes->GetNCalibResult();
			double RefMin = 0., RefMax = 0.;
			for(i=0; i<N; i++)
			{
				SpectrumResult* pSpR = ModRes->GetCalibResult(i);
				SpectrumParamResult *pSpPR = &pSpR->SpecParamData[0];

				if(i == 0)
					RefMin = RefMax = pSpPR->Reference;
				else
				{
					if(RefMin > pSpPR->Reference)  RefMin = pSpPR->Reference;
					if(RefMax < pSpPR->Reference)  RefMax = pSpPR->Reference;
				}

				if(StepData->Outs.chSEC && fabs(pSpPR->SEC_RefPred) > pPSR->SEC * StepData->Outs.limSEC)
				{
					ExcSp.insert(i);
					continue;
				}
				if(StepData->Outs.chSECV && fabs(pSpPR->SECV_RefPred) > pPSR->SECV * StepData->Outs.limSECV)
				{
					ExcSp.insert(i);
					continue;
				}
				if(StepData->Outs.chMah && pSpR->Out_Mah > StepData->Outs.limMah)
				{
					ExcSp.insert(i);
					continue;
				}
				if(StepData->Outs.chBad && pSpPR->Bad_Remainder > pPSR->norm)
				{
					ExcSp.insert(i);
					continue;
				}
			}

			double RefShift = (RefMax - RefMin) * StepData->Outs.limBound / 100.;
			double RefBoundMin = RefMin + RefShift;
			double RefBoundMax = RefMax - RefShift;

			ItSInt it;
			BadSpectra.clear();
			for(it=ExcSp.begin(); it!=ExcSp.end(); ++it)
			{
				SpectrumResult* pSpR = ModRes->GetCalibResult(*it);
				SpectrumParamResult *pSpPR = &pSpR->SpecParamData[0];

				if(StepData->Outs.chBound && (pSpPR->Reference <= RefBoundMin || pSpPR->Reference >= RefBoundMax))
					continue;

				BadOutsSpectrum2 NewBadSp;
				NewBadSp.SampleName = pSpR->SamName;
				NewBadSp.TransName = pSpR->TransName;
				NewBadSp.Num = pSpR->SpecNum;

				BadSpectra.push_back(NewBadSp);
			}

			if(int(BadSpectra.size()) >= N)
				IsErr = true;
			else
				StartModel->Copy(ModData);
		}
	}

	if(IsErr)
		return;

	ExcAllBadSpectra();

	GetSamplesCalibValid(Results->SamplesForCalib, Results->SamplesForValid);

	CCalcOptimPBDlg dlg(this, byTmpl, istep, nstep, Mode);
	IsErr = (dlg.DoModal() != IDOK);

	ItBOSp2 itB;
	for(itB=BadSpectra.begin(); itB!=BadSpectra.end(); itB++)
	{
		ModelSample* MSam = GetSampleForCalib(itB->SampleName, itB->TransName);
		if(MSam == NULL)
			continue;

		MSam->Spectra[itB->Num] = true;
	}
}

bool COptimCalculate::AddModelResult(OptimModel& mod)
{
	return Results->AddOptimModel(mod);
}

void COptimCalculate::ClearResults()
{
	Results->ClearResults();
}

void COptimCalculate::FillSampleProxyCalib(vector<CSampleProxy>& Samples)
{
	if(Trans->GetType() != C_TRANS_TYPE_NORMAL)
		FillSampleProxyCalibOwn(Samples);
	else
	{
		vector<CSampleProxy> SamplesOwn, SamplesTrans, SamplesOwnTmp;
		CFreqScale FreqScaleTrans;
		CSpecPreprocData_Parcel preData;

		FillSampleProxyCalibOwn(SamplesOwn, true);

		if(SamplesOwn.size() > 0)
		{
			FillFreqScaleTrans(FreqScaleTrans);
			FillSpecPreprocData(preData);

			USES_CONVERSION;

			string strPreproc(T2A(StartModel->GetTransPreprocAsStr()));

			IRCalibrate_PreCreate(&SamplesOwn, &FreqScaleTrans,	strPreproc, &preData);

			ExtractFromSampleProxyOwn(SamplesOwn, SamplesOwnTmp);
		}

		FillSampleProxyCalibTrans(SamplesTrans);

		vector<CSampleProxy>::iterator itOwn = SamplesOwnTmp.begin();
		vector<CSampleProxy>::iterator itTrans = SamplesTrans.begin();
		ItModSam it;
		int i;
		for(i=0, it=SamplesForCalib.begin(); it!=SamplesForCalib.end(); ++it)
		{
			if(it->GetNSpectraUsed() <= 0)
				continue;

			if(!SamplesForCalib[i].IsTransN())
			{
				Samples.push_back(*itOwn);
				++itOwn;
			}
			else
			{
				Samples.push_back(*itTrans);
				++itTrans;
			}

			i++;
		}
	}
}

void COptimCalculate::FillSampleProxyValid(vector<CSampleProxy>& Samples)
{
	if(Trans->GetType() != C_TRANS_TYPE_NORMAL)
		FillSampleProxyValidOwn(Samples);
	else
	{
		vector<CSampleProxy> SamplesOwn, SamplesTrans, SamplesOwnTmp;
		CFreqScale FreqScaleTrans;
		CSpecPreprocData_Parcel preData;

		FillSampleProxyValidOwn(SamplesOwn, true);

		if(SamplesOwn.size() > 0)
		{
			FillFreqScaleTrans(FreqScaleTrans);
			FillSpecPreprocData(preData);

			USES_CONVERSION;

			string strPreproc(T2A(StartModel->GetTransPreprocAsStr()));

			IRCalibrate_PreCreate(&SamplesOwn, &FreqScaleTrans,	strPreproc, &preData);

			ExtractFromSampleProxyOwn(SamplesOwn, SamplesOwnTmp);
		}

		FillSampleProxyValidTrans(SamplesTrans);

		vector<CSampleProxy>::iterator itOwn = SamplesOwnTmp.begin();
		vector<CSampleProxy>::iterator itTrans = SamplesTrans.begin();
		ItModSam it;
		int i;
		for(i=0, it=SamplesForValid.begin(); it!=SamplesForValid.end(); ++it)
		{
			if(it->GetNSpectraUsed() <= 0)
				continue;

			if(!SamplesForValid[i].IsTransN())
			{
				Samples.push_back(*itOwn);
				++itOwn;
			}
			else
			{
				Samples.push_back(*itTrans);
				++itTrans;
			}

			i++;
		}
	}
}

void COptimCalculate::FillSampleProxyCalibOwn(vector<CSampleProxy>& Samples, bool IsPre)
{
	Samples.clear();

	int i;
	ItModSam it;
	for(it=SamplesForCalib.begin(); it!=SamplesForCalib.end(); ++it)
	{
		if(it->IsTransN())
			continue;
		if(it->GetNSpectraUsed() <= 0)
			continue;

		CSampleProxy OneSample;

		CTransferData *pTr = ParentPrj->GetTransfer(it->TransName);
		if(pTr == NULL)
			continue;

		int minfreq = (IsPre) ? pTr->GetSpecPointInd(Trans->GetSpecPoint(0))
			: pTr->GetSpecPointInd(AllSpecPoints[0]);
		int maxfreq = (IsPre) ? pTr->GetSpecPointInd(Trans->GetSpecPoint(Trans->GetNSpecPoints() - 1))
			: pTr->GetSpecPointInd(AllSpecPoints[NAllPoints - 1]);

		CSampleData* Sam = pTr->GetSample(it->SampleName);

		ReferenceData* Ref = Sam->GetReferenceData(ParamName);
		OneSample.vCompConc.push_back(Ref->Value);

		ItIntBool it2;
		for(it2=it->Spectra.begin(); it2!=it->Spectra.end(); ++it2)
		{
			if(!it2->second)
				continue;

			Spectrum* Spec = Sam->GetSpectrumByNum(it2->first);
			if(Spec == NULL)
				continue;

			VDbl SpD;
			for(i=minfreq; i<=maxfreq; i++)
				SpD.push_back(Spec->Data[i]);

			OneSample.mSpectra.push_back(SpD);
		}

		Samples.push_back(OneSample);
	}
}

void COptimCalculate::FillSampleProxyValidOwn(vector<CSampleProxy>& Samples, bool IsPre)
{
	Samples.clear();

	int i;
	ItModSam it;
	for(it=SamplesForValid.begin(); it!=SamplesForValid.end(); ++it)
	{
		if(it->IsTransN())
			continue;
		if(it->GetNSpectraUsed() <= 0)
			continue;

		CSampleProxy OneSample;

		CTransferData *pTr = ParentPrj->GetTransfer(it->TransName);
		if(pTr == NULL)
			continue;

		int minfreq = (IsPre) ? pTr->GetSpecPointInd(Trans->GetSpecPoint(0))
			: pTr->GetSpecPointInd(AllSpecPoints[0]);
		int maxfreq = (IsPre) ? pTr->GetSpecPointInd(Trans->GetSpecPoint(Trans->GetNSpecPoints() - 1))
			: pTr->GetSpecPointInd(AllSpecPoints[NAllPoints - 1]);

		CSampleData* Sam = pTr->GetSample(it->SampleName);

		ReferenceData* Ref = Sam->GetReferenceData(ParamName);
		OneSample.vCompConc.push_back(Ref->Value);

		ItIntBool it2;
		for(it2=it->Spectra.begin(); it2!=it->Spectra.end(); ++it2)
		{
			if(!it2->second)
				continue;

			Spectrum* Spec = Sam->GetSpectrumByNum(it2->first);
			if(Spec == NULL)
				continue;

			VDbl SpD;
			for(i=minfreq; i<=maxfreq; i++)
				SpD.push_back(Spec->Data[i]);

			OneSample.mSpectra.push_back(SpD);
		}

		Samples.push_back(OneSample);
	}
}

void COptimCalculate::FillSampleProxyCalibTrans(vector<CSampleProxy>& Samples)
{
	Samples.clear();

	int minfreq = Trans->GetSpecPointInd(AllSpecPoints[0]);	
	int maxfreq = Trans->GetSpecPointInd(AllSpecPoints[NAllPoints - 1]);

	int i;
	ItModSam it;
	for(it=SamplesForCalib.begin(); it!=SamplesForCalib.end(); ++it)
	{
		if(!it->IsTransN())
			continue;
		if(it->GetNSpectraUsed() <= 0)
			continue;

		CSampleProxy OneSample;

		CTransferData *pTr = ParentPrj->GetTransfer(it->TransName);
		if(pTr == NULL)
			continue;

		CSampleData* Sam = pTr->GetSample(it->SampleName);

		ReferenceData* Ref = Sam->GetReferenceData(ParamName);
		OneSample.vCompConc.push_back(Ref->Value);

		ItIntBool it2;
		for(it2=it->Spectra.begin(); it2!=it->Spectra.end(); ++it2)
		{
			if(!it2->second)
				continue;

			Spectrum* Spec = Sam->GetSpectrumByNum(it2->first);
			if(Spec == NULL)
				continue;

			VDbl SpD;
			for(i=minfreq; i<=maxfreq; i++)
				SpD.push_back(Spec->Data[i]);

			OneSample.mSpectra.push_back(SpD);
		}

		Samples.push_back(OneSample);
	}
}

void COptimCalculate::FillSampleProxyValidTrans(vector<CSampleProxy>& Samples)
{
	Samples.clear();

	int minfreq = Trans->GetSpecPointInd(AllSpecPoints[0]);	
	int maxfreq = Trans->GetSpecPointInd(AllSpecPoints[NAllPoints - 1]);

	int i;
	ItModSam it;
	for(it=SamplesForValid.begin(); it!=SamplesForValid.end(); ++it)
	{
		if(!it->IsTransN())
			continue;
		if(it->GetNSpectraUsed() <= 0)
			continue;

		CSampleProxy OneSample;

		CTransferData *pTr = ParentPrj->GetTransfer(it->TransName);
		if(pTr == NULL)
			continue;

		CSampleData* Sam = pTr->GetSample(it->SampleName);

		ReferenceData* Ref = Sam->GetReferenceData(ParamName);
		OneSample.vCompConc.push_back(Ref->Value);

		ItIntBool it2;
		for(it2=it->Spectra.begin(); it2!=it->Spectra.end(); ++it2)
		{
			if(!it2->second)
				continue;

			Spectrum* Spec = Sam->GetSpectrumByNum(it2->first);
			if(Spec == NULL)
				continue;

			VDbl SpD;
			for(i=minfreq; i<=maxfreq; i++)
				SpD.push_back(Spec->Data[i]);

			OneSample.mSpectra.push_back(SpD);
		}

		Samples.push_back(OneSample);
	}
}

void COptimCalculate::ExtractFromSampleProxyOwn(vector<CSampleProxy>& SamplesOwn, vector<CSampleProxy>& Samples)
{
	int minfreq = Trans->GetSpecPointInd(AllSpecPoints[0]);	
	int maxfreq = Trans->GetSpecPointInd(AllSpecPoints[NAllPoints - 1]);

	vector<CSampleProxy>::iterator it;
	for(it=SamplesOwn.begin(); it!=SamplesOwn.end(); ++it)
	{
		CSampleProxy OneSample;

		copy(it->vCompConc.begin(), it->vCompConc.end(), inserter(OneSample.vCompConc, OneSample.vCompConc.begin()));

		ItDblDbl it2;
		for(it2=it->mSpectra.begin(); it2!=it->mSpectra.end(); ++it2)
		{
			VDbl OneSpec;

			copy(it2->begin() + minfreq, it2->begin() + (maxfreq + 1), inserter(OneSpec, OneSpec.begin()));

			OneSample.mSpectra.push_back(OneSpec);
		}

		Samples.push_back(OneSample);
	}
}

void COptimCalculate::FillFreqScale(CFreqScale& FreqScale)
{
	FreqScale.nNumberOfFreq = NAllPoints;
	if(FreqScale.nNumberOfFreq > 0)
	{
		FreqScale.fStartFreq = AllSpecPoints[0];
		if(FreqScale.nNumberOfFreq > 1)
			FreqScale.fStepFreq = AllSpecPoints[1] - AllSpecPoints[0];
	}
}

void COptimCalculate::FillFreqScaleTrans(CFreqScale& FreqScale)
{
	FreqScale.nNumberOfFreq = Trans->GetNSpecPoints();
	if(FreqScale.nNumberOfFreq > 0)
	{
		FreqScale.fStartFreq = Trans->GetSpecPoint(0);
		if(FreqScale.nNumberOfFreq > 1)
			FreqScale.fStepFreq = Trans->GetSpecPoint(1) - Trans->GetSpecPoint(0);
	}
}

void COptimCalculate::FillSpecPreproc(CSpecPreproc& prePar, SInt& ExcPoints)
{
	USES_CONVERSION;

	string st(T2A(StartModel->GetTransPreprocAsStr()));
	prePar.strPreprocParTrans = st;

	int shift = ParentPrj->GetSpecPointInd(AllSpecPoints[0]);

	prePar.vFreqExcluded.clear();
	ItSInt it;
	int i;
	for(i=0, it=ExcPoints.begin(); it!=ExcPoints.end(); ++it, i++)
	{
		int pnt = (*it) - shift;
		if(pnt < 0 || pnt >= int(AllSpecPoints.size()))
			continue;

		prePar.vFreqExcluded.push_back(pnt);
	}

	string s1(T2A(ParamName));
	prePar.strCompName.push_back(s1);
}

CString COptimCalculate::GetOwnPreprocAsStr(CString prep)
{
	CString TransPrep = StartModel->GetTransPreprocAsStr();
	CString OwnPrep = prep;

	int len = TransPrep.GetLength();

	OwnPrep.Delete(0, len);

	return OwnPrep;
}

void COptimCalculate::FillSpecPreprocData(CSpecPreprocData_Parcel& preData)
{
	CSampleData* ArtSample = Trans->GetArtSample();
	if(ArtSample == NULL)
		return;

	ArtSample->LoadSpectraData();

	Spectrum* MeanSpec = ArtSample->GetSpectrumByNum(C_TRANS_SPECNUM_MEAN);
	if(MeanSpec != NULL)
		copy(MeanSpec->Data.begin(), MeanSpec->Data.end(), inserter(preData.meanSpectrum, preData.meanSpectrum.begin()));

	Spectrum* MeanSpecMSC = ArtSample->GetSpectrumByNum(C_TRANS_SPECNUM_MEANMSC);
	if(MeanSpecMSC != NULL)
		copy(MeanSpecMSC->Data.begin(), MeanSpecMSC->Data.end(), inserter(preData.meanSpectrumMSC, preData.meanSpectrumMSC.begin()));

	Spectrum* MeanSpecStd = ArtSample->GetSpectrumByNum(C_TRANS_SPECNUM_MEANSTD);
	if(MeanSpecStd != NULL)
		copy(MeanSpecStd->Data.begin(), MeanSpecStd->Data.end(), inserter(preData.meanStdSpec, preData.meanStdSpec.begin()));

	ArtSample->FreeSpectraData();
}

void COptimCalculate::FillQntOutput(CQntOutput& qntOut)
{
	qntOut.bSEC = StepData->Crits.bSEC;
	qntOut.bSECV =StepData->Crits.bSECV;
//	qntOut.bSECSECV =StepData->Crits.bSECSECV;
	qntOut.bSEV = StepData->Crits.bSEV;

	double tmp = 0.;
	qntOut.vSEC.push_back(tmp);
	qntOut.vR2StatSEC.push_back(tmp);
	qntOut.vSECV.push_back(tmp);
	qntOut.vR2StatSECV.push_back(tmp);
	qntOut.vFStatSECV.push_back(tmp);
	qntOut.vSEV.push_back(tmp);
	qntOut.vErr.push_back(tmp);
	qntOut.vSDV.push_back(tmp);
	qntOut.vR2StatSEV.push_back(tmp);
}

int COptimCalculate::GetStartSpecInd()
{
	return ParentPrj->GetSpecPointInd(AllSpecPoints[0]);
}
