#include "stdafx.h"

#include "ShowSpectra.h"

CShowSpectra::CShowSpectra(VDbl* points)
{
	copy(points->begin(), points->end(), inserter(Points, Points.begin()));

	NPnt = int(Points.size());

	Type = C_SPECTRA_TYPE_ALL;
}

CShowSpectra::~CShowSpectra()
{
}

void CShowSpectra::Copy(CShowSpectra& data)
{
	Points.clear();
	copy(data.GetPoints()->begin(), data.GetPoints()->end(), inserter(Points, Points.begin()));

	NPnt = data.GetNPnt();
	Type = data.GetType();

	Spectra.clear();
	copy(data.Spectra.begin(), data.Spectra.end(), inserter(Spectra, Spectra.begin()));
}

VDbl* CShowSpectra::GetPoints()
{
	return &Points;
}

int CShowSpectra::GetNPnt()
{
	return NPnt;
}

int CShowSpectra::GetNSpectraType(int type)
{
	int N = 0;
	ItSSS it;
	for(it=Spectra.begin(); it!=Spectra.end(); ++it)
		if(it->type & type)	
			N++;

	return N;
}

int CShowSpectra::GetNSpectraSel()
{
	int N = 0;
	ItSSS it;
	for(it=Spectra.begin(); it!=Spectra.end(); ++it)
		if(it->IsSel)	
			N++;

	return N;
}

int CShowSpectra::GetNShowData()
{
	return int(ShowData.size());
}

SpectraShowSettings* CShowSpectra::GetSpectraShow(int type, int ind)
{
	if(ind < 0 || ind >=GetNSpectraType(type))
		return NULL;

	int i;
	ItSSS it;
	for(i=0, it=Spectra.begin(); it!=Spectra.end(); ++it)
		if(it->type & type)
		{
			if(i == ind)
				return &(*it);

			i++;
		}

	return NULL;
}

SpectraShowSettings* CShowSpectra::GetSpectraShowSel(int ind)
{
	if(ind < 0 || ind >=GetNSpectraSel())
		return NULL;

	int i;
	ItSSS it;
	for(i=0, it=Spectra.begin(); it!=Spectra.end(); ++it)
		if(it->IsSel)
		{
			if(i == ind)
				return &(*it);

			i++;
		}

	return NULL;
}

SpectraShowSettings* CShowSpectra::GetSpectraShowSel(CString name)
{
	ItSSS it;
	for(it=Spectra.begin(); it!=Spectra.end(); ++it)
		if(it->IsSel && !it->Name.Compare(name))
			return &(*it);

	return NULL;
}

SpectraShowSettings* CShowSpectra::AddSpectraShow(CString samname, int num, int indT, int sh, bool issel, int type)
{
	SpectraShowSettings NewSpec(samname, num, indT, sh);
	NewSpec.type = type;
	NewSpec.IsSel = issel;

	Spectra.push_back(NewSpec);

	int len = int(Spectra.size());
	return &Spectra[len-1];
}

int CShowSpectra::GetType()
{
	return Type;
}

void CShowSpectra::SetType(int type)
{
	Type = type;
	ItSSS it;
	for(it=Spectra.begin(); it!=Spectra.end(); ++it)
		if(!(it->type & Type))
			it->IsSel = false;
}

void CShowSpectra::SetSelAll(bool sel)
{
	ItSSS it;
	for(it=Spectra.begin(); it!=Spectra.end(); ++it)
		if(it->type & Type)
			it->IsSel = sel;
}

void CShowSpectra::CreateShowData()
{
	ShowData.clear();

	int i;
	ItSSS it;
	for(i=0, it=Spectra.begin(); it!=Spectra.end(); ++it)
	{
		if(!it->IsSel)
			continue;

		ItDbl it_s = Points.begin() + it->shift;
		ItDbl it_f = it_s + int(it->SpecData.size());

		SpectraShowData NewData;

		NewData.Name = it->Name;
		NewData.Color = cGetDefColor(i);
		copy(it_s, it_f, inserter(NewData.x, NewData.x.begin()));

		ItDbl it2;
		for(it2=it->SpecData.begin(); it2!=it->SpecData.end(); ++it2)
		{
			double val = (*it2) * 100.;
			NewData.y.push_back(val);
		}

		ShowData.push_back(NewData);

		i++;
	}
}

double CShowSpectra::GetYMin()
{
	double ymin = 1.e9;
	ItSSD it;
	for(it=ShowData.begin(); it!=ShowData.end(); ++it)
	{
		ItDbl it2;
		for(it2=it->y.begin(); it2!=it->y.end(); ++it2)
			if(ymin > (*it2))
				ymin = (*it2);
	}

	return ymin;
}

double CShowSpectra::GetYMax()
{
	double ymax = 0;
	ItSSD it;
	for(it=ShowData.begin(); it!=ShowData.end(); ++it)
	{
		ItDbl it2;
		for(it2=it->y.begin(); it2!=it->y.end(); ++it2)
			if(ymax < (*it2))
				ymax = (*it2);
	}

	return ymax;
}
