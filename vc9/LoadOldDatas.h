#ifndef _PARCEL_LOAD_OLD_DATAS_H_
#define _PARCEL_LOAD_OLD_DATAS_H_

#include "DfxCached.h"


struct SampleOldData
{
	CString Name;
	CString Iden2;
	int NSpectra;
	int Spectra[NSAMPLE_SPEC_MAX];
};

struct RefOldData
{
	CString Name;
	double Value;
};

typedef vector<RefOldData> VROD;
typedef vector<RefOldData>::iterator ItROD;

struct SpectrumOldData
{
	int DevNumber;
	int PrjLowSpec;
	int PrjUpperSpec;
	double PrjResolution;
	int PrjApodize;
	int PrjZeroFilling;
	bool PrjXTransform;
	double WaveLength;
	CTime DateTime;
	VDbl SpecData;
	int num;
};

typedef vector<SpectrumOldData> VSOD;
typedef vector<SpectrumOldData>::iterator ItSOD;

struct ParamOldData
{
	CString Name;
	CString Format;
	int IFormat;
	CString Unit;
	double HumidRef;
};

struct SampleOldDataFull
{
	SampleOldData OldSam;
	VROD RefData;
	VSOD SpecData;
};

struct ModelParOldData
{
	CString MPName;
	double MPSKO;
	double MinRange;
	double MaxRange;
	double Corr;
};

typedef vector<ModelParOldData> VMPOD;
typedef vector<ModelParOldData>::iterator ItMPOD;

struct ModelSamOldData
{
	CString Name;
	int nSpec;
	int SpecNums[NSAMPLE_SPEC_MAX];
};

typedef vector<ModelSamOldData> VMSOD;
typedef vector<ModelSamOldData>::iterator ItMSOD;

struct ModelOldData
{
	CString PrjName;
	CString ModelName;
	CString FileName;
	int AnType;
	int SpecType;
	int ModelType;
	int LowSpecRange;
	int UpperSpecRange;
	int IsAvr;
	CString Preproc;
	int NComp;
	int MaxMah;

	int nParams;
	VMPOD Params;
	int nCalib;
	VMSOD Calib;
	int nValid;
	VMSOD Valid;
};

struct RuleOldData
{
	CString ParamName;
	CString DefModel;
	double StdMois;
};

typedef vector<RuleOldData> VRlOD;
typedef vector<RuleOldData>::iterator ItRlOD;

struct MethodOldData
{
	CString PrjName;
	CString MethodName;
	int AnType;
	int IndMois;
	int nModels;
	VStr Models;
	int MinTemp;
	int MaxTemp;
	int DiffTemp;

	int nRules;
	VRlOD Rules;
};

struct ProjectOldData
{
	int DevNum;

	CString Name;
	CString Dir;
	int LowSpec;
	int UpperSpec;
	int ResCode;
	int Apodize;
	bool UseRef;
	int ResCodeRef;
	double RealRes;
	int ApodizeRef;
	int ZeroFilling;
	int NScanSample;
	int NMeasSample;
	int NScanStd;
	int NMeasStd;
	int BathLength;
	int MinTemp;
	int MaxTemp;
	int DiffTemp;

	vector<ParamOldData> Params;

	VStr SamNames;
	vector<SampleOldDataFull> Samples;

	vector<ModelOldData> Models;
	vector<MethodOldData> Methods;
};

const int C_CONVERT_WITHOUT_NOT			= 0;
const int C_CONVERT_WITHOUT_DECLINE		= 1;
const int C_CONVERT_WITHOUT_ACCEPT		= 2;

const int C_CONVERT_BADWAVE_NOT			= 0;
const int C_CONVERT_BADWAVE_DECLINE		= 1;
const int C_CONVERT_BADWAVE_ACCEPT		= 2;

const int C_CONVERT_EMPTYUNIT_NOT		= 0;
const int C_CONVERT_EMPTYUNIT_EXIST		= 1;

struct CheckData
{
	VStr Errors;
	VInt ErrNums;

	int DevNumber;

	double WaveLen;
	int LowSpec;
	int UpperSpec;
	double Resol;
	int Apod;
	int Zero;

	VStr ParNames;
	VStr SamNames;
	VStr ModFiles;

	int WithoutNumber;
	int EmptyUnit;
	int BadWave;
	CString ReplaceUnit;
};

class CLoadOldDatas
{
public:

	CLoadOldDatas();
	~CLoadOldDatas();

	CheckData Checks;

public:

	bool LoadOldProject(CString fname, ProjectOldData &OldPrj);
	bool LoadOldSampleFull(CString name, CString path, SampleOldDataFull &OldSamF, bool IsImit = false);
	bool LoadOldSample(CString fname, SampleOldData &OldSam);
	bool LoadOldRef(CString fname, VROD &OldRef);
	bool LoadOldSpectrum(CString fname, SpectrumOldData &OldSpec, bool IsImit = false);
	bool LoadOldModel(CString name, CString path, CString PrjName, ModelOldData &OldModel, vector<SampleOldDataFull>* Samples);
	bool LoadOldMethod(CString name, CString path, CString PrjName, MethodOldData &OldMethod);

};

#endif
