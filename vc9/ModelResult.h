#ifndef _PARCEL_MODEL_RESULT_H_
#define _PARCEL_MODEL_RESULT_H_

#pragma once

const int C_MODRES_SORT_CALIB_NAME			= 0;
const int C_MODRES_SORT_CALIB_REF			= 1;
const int C_MODRES_SORT_CALIB_SEC_PRED		= 2;
const int C_MODRES_SORT_CALIB_SEC_DIFF		= 3;
const int C_MODRES_SORT_CALIB_SEC_PROC		= 4;
const int C_MODRES_SORT_CALIB_SECV_PRED		= 5;
const int C_MODRES_SORT_CALIB_SECV_DIFF		= 6;
const int C_MODRES_SORT_CALIB_SECV_PROC		= 7;
const int C_MODRES_SORT_CALIB_SECV_SEC		= 8;
const int C_MODRES_SORT_CALIB_OUT_MD		= 9;
const int C_MODRES_SORT_CALIB_OUT_EXCEED	= 10;
const int C_MODRES_SORT_CALIB_BAD_PRED		= 11;
const int C_MODRES_SORT_CALIB_BAD_DIFF		= 12;
const int C_MODRES_SORT_CALIB_BAD_PROC		= 13;
const int C_MODRES_SORT_CALIB_BAD_REST		= 14;
const int C_MODRES_SORT_CALIB_GK_0			= 15;

const int C_MODRES_SORT_VALID_NAME			= 0;
const int C_MODRES_SORT_VALID_REF			= 1;
const int C_MODRES_SORT_VALID_PRED			= 2;
const int C_MODRES_SORT_VALID_DIFF			= 3;
const int C_MODRES_SORT_VALID_PROC			= 4;
const int C_MODRES_SORT_VALID_DELTA			= 5;
const int C_MODRES_SORT_VALID_MD			= 6;
const int C_MODRES_SORT_VALID_EXCEED		= 7;

// �������� �������� ��� ���������

const int C_PGRAPH_SAMCALIB		= 0;
const int C_PGRAPH_SAMVALID		= 1;
const int C_PGRAPH_SEC			= 2;
const int C_PGRAPH_SECV			= 3;
const int C_PGRAPH_SEV			= 4;
const int C_PGRAPH_MDCALIB		= 5;
const int C_PGRAPH_MDVALID		= 6;
const int C_PGRAPH_BAD			= 7;
const int C_PGRAPH_SCORES		= 8;
const int C_PGRAPH_SPLOAD		= 9;
const int C_PGRAPH_CHLOAD		= 10;

struct ParamSumResult
{
	CString ParamName;
	double SEC;
	double R2sec;
	double SECV;
	double R2secv;
	double F;
	double SEV;
	double SDV;
	double R2sev;
	double e;
	double t;
	double t0;
	double norm;

	double tSECb;
	double tSECa;
	double tSECVb;
	double tSECVa;
	double tSEVb;
	double tSEVa;

	VDbl Chem_GK;

	ParamSumResult(CString name):ParamName(name),SEC(0.),R2sec(0.),SECV(0.),R2secv(0.),F(0.),SEV(0.),SDV(0.),
		R2sev(0.),e(0.),t(0.),t0(0.),norm(0.),tSECb(0.),tSECa(0.),tSECVb(0.),tSECVa(0.),tSEVb(0.),tSEVa(0.)
	{};
	~ParamSumResult()
	{
		Chem_GK.clear();
	};
};

typedef vector<ParamSumResult> VParSumRes;
typedef vector<ParamSumResult>::iterator ItParSumRes;

struct SpectrumParamResult
{
	CString ParamName;
	double Reference;
	double SEC_Reference;
	double SEC_Prediction;
	double SEC_RefPred;
	double SEC_Proc;
	double SECV_Reference;
	double SECV_Prediction;
	double SECV_RefPred;
	double SECV_Proc;
	double SECV_SEC;
	double Bad_Reference;
	double Bad_Prediction;
	double Bad_RefPred;
	double Bad_Proc;
	double Bad_Remainder;

};

typedef vector<SpectrumParamResult> VSpecParRes;
typedef vector<SpectrumParamResult>::iterator ItSpecParRes;

struct SpectrumResult
{
	CString SamName;
	CString TransName;
	int SpecNum;

	CString Name;

	double Out_Mah;

	VDbl Scores_GK;
	VSpecParRes SpecParamData;

	SpectrumResult(CString name, int num):SamName(cEmpty),TransName(cOwn),SpecNum(num),Out_Mah(0.),Name(name)
	{};
	~SpectrumResult()
	{
		Scores_GK.clear();
		SpecParamData.clear();
	};
};

typedef vector<SpectrumResult> VSpecRes;
typedef vector<SpectrumResult>::iterator ItSpecRes;

struct SpectrumValParamResult
{
	CString ParamName;
	double Reference;
	double Prediction;
	double RefPred;
	double Proc;
	double Delta;
	double Corrected;
};

typedef vector<SpectrumValParamResult> VSpecValParRes;
typedef vector<SpectrumValParamResult>::iterator ItSpecValParRes;

struct SpectrumValResult
{
	CString SamName;
	CString TransName;
	int SpecNum;
	CString Name;

	double Out_Mah;
	VSpecValParRes SpecValParamData;

	SpectrumValResult(CString name, int num):SamName(cEmpty),TransName(cOwn),SpecNum(num),Out_Mah(0.),Name(name)
	{};
	~SpectrumValResult()
	{
		SpecValParamData.clear();
	};
};

typedef vector<SpectrumValResult> VSpecValRes;
typedef vector<SpectrumValResult>::iterator ItSpecValRes;

struct LoadingResult
{
	int SpecPoint;
	VDbl Spec_GK;

	LoadingResult(int spec):SpecPoint(spec)
	{};
	~LoadingResult()
	{
		Spec_GK.clear();
	};
};

typedef vector<LoadingResult> VLoadRes;
typedef vector<LoadingResult>::iterator ItLoadRes;

const int C_GRAPH_MODE_SEC_V	= 0;
const int C_GRAPH_MODE_HISTO	= 1;
const int C_GRAPH_MODE_PNTSET	= 2;
const int C_GRAPH_MODE_CURVE	= 3;

struct GraphInfo
{
	int mode;
	bool sel;

	int N;
	VStr Names;
	VDbl x;
	VDbl y;
	double xmin, xmax, ymin, ymax;
	double a, b;

	void CalcParams();
	void CalcHistoParams();
	int GetIndByName(CString name);
	CString GetNameByInd(int ind);
};

typedef vector<GraphInfo> VGI;
typedef vector<LoadingResult>::iterator ItGI;

struct ExportParamResult
{
	CString ParamName;

	double LowCalibRange;
	double UpperCalibRange;
	double MaxSKO;
	double CorrB;
	double CorrA;
	double SEC;
	double MeanComp;
	double MeanCompTrans;
	double MeanCompStd;

	VDbl CalibrLin;
	VDbl CalibrNonLin;
};

typedef vector<ExportParamResult> VExpPRes;
typedef vector<ExportParamResult>::iterator ItExpPRes;

struct ExportResult
{
	CString ModelName;
	int ModelType;
	int SpecRangeLow;
	int SpecRangeUpper;
	double FreqMin;
	double FreqStep;
	int NFreq;
	int SpType;
	int NSpec;
	CString Preproc;
	double MaxMah;
	double MeanMah;
	VInt ExcPoints;

	bool IsTrans;
	double FreqMinTrans;
	double FreqStepTrans;
	int NFreqTrans;
	int SpTypeTrans;
	CString PreprocTrans;
	VDbl MeanSpecTrans;
	VDbl MeanSpecMSCTrans;
	VDbl MeanSpecStdTrans;

	double T0;
	double FMax;
	VDbl MeanSpec;
	VDbl MeanSpecMSC;
	VDbl MeanSpecStd;

	VDbl MeanSpecHQO;
	VDbl MeanSpecMSCHQO;
	VDbl MeanSpecStdHQO;

	VExpPRes ParRes;

	MDbl Factors;
	MDbl Scores;

	void Clear();
	ExportParamResult* GetExpParRes(CString name);
};

struct ExportQltResult
{
	CString ModelName;
	int SpecRangeLow;
	int SpecRangeUpper;
	double FreqMin;
	double FreqStep;
	int NFreq;
	int SpType;
	CString Preproc;
	double MaxMah;
	double MaxSKO;
	double MeanMah;
	VInt ExcPoints;

	bool IsTrans;
	double FreqMinTrans;
	double FreqStepTrans;
	int NFreqTrans;
	int SpTypeTrans;
	CString PreprocTrans;

	VDbl MeanSpecTrans;
	VDbl MeanSpecMSCTrans;
	VDbl MeanSpecStdTrans;

	VDbl MeanSpec;
	VDbl MeanSpecMSC;
	VDbl MeanSpecStd;

	MDbl Factors;

	void Clear();
};

class CModelResult
{
public:

	CModelResult();							// �����������
	~CModelResult();						// ����������

public:

	VStr TransNames;

	VParSumRes PSResult;
	VSpecRes CalibResult;
	VSpecValRes ValidResult;
	VLoadRes LoadRes;

	ExportResult ExpResult;
	ExportQltResult ExpQltResult;

	int SortCalibProp;
	bool SortCalibDir;
	int SortValidProp;
	bool SortValidDir;

protected:

	bool IsAvr;
	int NVal;

	GraphInfo GraphSEC;
	GraphInfo GraphSECV;
	GraphInfo GraphSEV;
	GraphInfo HistoCalib;
	GraphInfo HistoValid;
	GraphInfo HistoSamCalib;
	GraphInfo HistoSamValid;
	GraphInfo HistoBad;
	GraphInfo GraphScores;
	VGI GraphsSpecLoad;
	VDblDbl MasksSpecLoad;
	GraphInfo GraphChemLoad;

public:

	void Copy(CModelResult& data, bool bReplaceOwn=false, CString NewTransName=_T(""));
	void Clear();

	int GetNGK();

	int GetNVal();
	void SetNVal(int nval);

	int GetNPSResult();
	ParamSumResult* GetPSResult(CString name);
	ParamSumResult* GetPSResult(int ind);
	ParamSumResult* AddPSResult(CString name);

	int GetNCalibResult();
	SpectrumResult* GetCalibResult(CString name);
	SpectrumResult* GetCalibResult(int ind);
	int GetCalibIndByName(CString name);
	CString GetCalibNameByInd(int ind);
	SpectrumResult* AddCalibResult(CString samname, CString transname, int num, bool IsAv = false);

	int GetNValidResult();
	SpectrumValResult* GetValidResult(CString name);
	SpectrumValResult* GetValidResult(int ind);
	int GetValidIndByName(CString name);
	CString GetValidNameByInd(int ind);
	SpectrumValResult* AddValidResult(CString samname, CString transname, int num);

	int GetNLoadingResult();
	LoadingResult* GetLoadingResult(int spec);
	LoadingResult* GetLoadingResultByInd(int ind);
	LoadingResult* AddLoadingResult(int spec);

	void CalcCorrections(VDbl& corrA, VDbl& corrB, int nFactor);

	bool IsAverage();
	void SetAverage(bool avr);

	void CreateGraphSEC(int iParam);
	void CreateGraphSECV(int iParam);
	void CreateGraphSEV(int iParam);
	void CreateHistoCalib();
	void CreateHistoValid();
	bool CreateHistoSamCalib(int iParam);
	bool CreateHistoSamValid(int iParam);
	void CreateHistoBad(int iParam);
	void CreateGraphScores(int gk1, int gk2);
	void CreateGraphsSpecLoad(HTREEITEM hItem);
	void CreateGraphChemLoad(int gk1, int gk2);

	GraphInfo* GetGraphSEC();
	GraphInfo* GetGraphSECV();
	GraphInfo* GetGraphSEV();
	GraphInfo* GetHistoCalib();
	GraphInfo* GetHistoValid();
	GraphInfo* GetHistoSamCalib();
	GraphInfo* GetHistoSamValid();
	GraphInfo* GetHistoBad();
	GraphInfo* GetGraphScores();
	GraphInfo* GetGraphSpecLoad(int gk);
	VDblDbl* GetAllMasksSpecLoad();
	GraphInfo* GetGraphChemLoad();

	CString GetSampleTransName(CString samname, CString transname); // �������� ��� ������� � ������ ���������

	void SortCalib(int prop, bool dir, int ind, double maxmah);
	void SortValid(int prop, bool dir, int ind, double maxmah);
};

#endif
