#include "stdafx.h"

#include "DeleteDeviceWaitDlg.h"
#include "BranchInfo.h"

IMPLEMENT_DYNAMIC(CDeleteDeviceWaitDlg, CWaitingDlg)

CDeleteDeviceWaitDlg::CDeleteDeviceWaitDlg(int number, CWnd* pParent) : CWaitingDlg(pParent)
{
	Number = number;

	Comment = ParcelLoadString(STRID_DEVICE_DELETE_WAIT_COMMENT);
}

CDeleteDeviceWaitDlg::~CDeleteDeviceWaitDlg()
{
}

void CDeleteDeviceWaitDlg::DoDataExchange(CDataExchange* pDX)
{
	CWaitingDlg::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDeleteDeviceWaitDlg, CWaitingDlg)
END_MESSAGE_MAP()


BOOL CDeleteDeviceWaitDlg::OnInitDialog()
{
	CWaitingDlg::OnInitDialog();

	return true;
}

void CDeleteDeviceWaitDlg::OK()
{
	CWaitingDlg::OK();
}

void CDeleteDeviceWaitDlg::Stop()
{
	CWaitingDlg::Stop();
}

void CDeleteDeviceWaitDlg::Work()
{
	ParcelWait(true);
	
	bool res = GetDatabase()->ApplianceDelete(Number);

	ParcelWait(false);

	if(res)
		OK();
	else
		Stop();
}
