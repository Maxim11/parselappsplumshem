#ifndef _SHOW_SPECTRUM_TEXT_DLG_H_
#define _SHOW_SPECTRUM_TEXT_DLG_H_

#pragma once

#include "Resource.h"


///////////////////////////////////////////////////////////////////////////////////////////////////
// ������ �������������� ������ ��������

class CShowSpectrumTextDlg : public CDialog
{
	DECLARE_DYNAMIC(CShowSpectrumTextDlg)

public:

	CShowSpectrumTextDlg(VDbl* points, VDbl* data, CString name, CWnd* pParent = NULL);		// �����������
	virtual ~CShowSpectrumTextDlg();														// ����������

	enum { IDD = IDD_SHOW_SPECTRUM_TEXT };	// ������������� �������

	CListCtrl m_list;

protected:

	VDbl SpecPoints;
	VDbl SpecData;
	CString SpecName;

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();						// ������������� �������
/*
	virtual void OnOK();				// ��������� ������� "������� ������ � ����� �� �������"
	virtual void OnCancel();			// ��������� ������� "�������� ���� � ����� �� �������"
*/

	afx_msg void OnHelp();				// ��������� ������� "������� �������"

	DECLARE_MESSAGE_MAP()
};

#endif
