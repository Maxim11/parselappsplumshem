#include "stdafx.h"

#include "BranchInfo.h"
#include "CreateSampleDlg.h"
#include "ShowSpectraDlg.h"
#include "ShowSpectrumTextDlg.h"
#include "MainFrm.h"


static bool iscancel = false;

IMPLEMENT_DYNAMIC(CCreateSampleDlg, CDialog)

CCreateSampleDlg::CCreateSampleDlg(CSampleData* data, int mode, CWnd* pParent)
	: CDialog(CCreateSampleDlg::IDD, pParent)
{
	Mode = mode;

	SamData = data;
	ParentPrj = GetProjectByHItem(SamData->GetParentHItem());

	NewSpecNum = CalcNewSpecNum();

	CurRef = (SamData->GetNReferenceData() > 0) ? SamData->RefData.begin() : SamData->RefData.end();
	SelRef = CurRef;
	CurSpec = (SamData->GetNSpectra() > 0) ? SamData->SpecData.begin() : SamData->SpecData.end();
	IsEditRef = false;

	m_vName = SamData->GetName();
	m_vIden2 = SamData->GetIden2();
	m_vIden3 = SamData->GetIden3();

	if(CurRef != SamData->RefData.end())
	{
		m_vNoValue = CurRef->bNoValue;
		m_vValue = CurRef->Value;
	}
	else
	{
		m_vNoValue = true;
		m_vValue = 0.;
	}

	IsBtnClk = false;
}

CCreateSampleDlg::~CCreateSampleDlg()
{
}

void CCreateSampleDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_REFDATA_LIST, m_listRef);
	DDX_Control(pDX, IDC_SPECTRUM_LIST, m_listSpec);

	DDX_Control(pDX, IDC_INPUT_NAME, m_Name);
	DDX_Control(pDX, IDC_INPUT_IDEN2, m_Iden2);
	DDX_Control(pDX, IDC_INPUT_IDEN3, m_Iden3);
	DDX_Control(pDX, IDC_SET_NO_VALUE, m_NoValue);

	cParcelDDX(pDX, IDC_INPUT_NAME, m_vName);
	DDV_MaxChars(pDX, m_vName, 50);
	cParcelDDX(pDX, IDC_INPUT_IDEN2, m_vIden2);
	DDV_MaxChars(pDX, m_vIden2, 40);
	cParcelDDX(pDX, IDC_INPUT_IDEN3, m_vIden3);
	DDV_MaxChars(pDX, m_vIden3, 255);

	DDX_Check(pDX, IDC_SET_NO_VALUE, m_vNoValue);

	if(!m_vNoValue)
	{
		cParcelDDX(pDX, IDC_INPUT_VALUE, m_vValue);
		cParcelDDV(pDX, ParcelLoadString(STRID_CREATE_SAMPLE_DDV_VALUE), m_vValue, 0., 1.0e9);
	}
}


BEGIN_MESSAGE_MAP(CCreateSampleDlg, CDialog)

	ON_BN_CLICKED(IDC_SET_NO_VALUE, OnChangeRefData)

	ON_BN_CLICKED(IDC_ACCEPT, OnAccept)
	ON_BN_CLICKED(IDHELP, OnHelp)
	ON_BN_CLICKED(IDC_REMOVE, OnRemove)
	ON_BN_CLICKED(IDC_EXCLUDE, OnExclude)
	ON_BN_CLICKED(IDC_PLOT, OnShowPlot)
	ON_BN_CLICKED(IDC_PLOT_2, OnShowText)

	ON_NOTIFY(LVN_ITEMCHANGED, IDC_REFDATA_LIST, OnRefDataListSelChange)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_SPECTRUM_LIST, OnSpectraListSelChange)

	ON_EN_SETFOCUS(IDC_INPUT_VALUE, OnStartRefData)
	ON_EN_KILLFOCUS(IDC_INPUT_VALUE, OnChangeRefData)

	ON_WM_KEYUP()
END_MESSAGE_MAP()


BOOL CCreateSampleDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	iscancel = false;

	CString Hdr;
	switch(Mode)
	{
	case SAMDLG_MODE_CREATE:
		Hdr = ParcelLoadString(STRID_CREATE_SAMPLE_HDR_CREATE);
		break;
	case SAMDLG_MODE_CHANGE:
		Hdr = ParcelLoadString(STRID_CREATE_SAMPLE_HDR_CHANGE);
		break;
	case SAMDLG_MODE_SHOW:
		Hdr = ParcelLoadString(STRID_CREATE_SAMPLE_HDR_SHOW);
		break;
	}
	SetWindowText(Hdr);

	CString s;
	int i;

	s = ParcelLoadString(STRID_CREATE_SAMPLE_COLPARAM);
	m_listRef.InsertColumn(0, s, LVCFMT_LEFT, 0, 0);
	s = ParcelLoadString(STRID_CREATE_SAMPLE_COLVALUE);
	m_listRef.InsertColumn(1, s, LVCFMT_RIGHT, 0, 1);
	s = ParcelLoadString(STRID_CREATE_SAMPLE_COLUNIT);
	for(i=0; i<3; i++)
		m_listRef.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	m_listRef.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

	FillListRef();

	s = ParcelLoadString(STRID_CREATE_SAMPLE_COLSPNUM);
	m_listSpec.InsertColumn(0, s, LVCFMT_LEFT, 0, 0);
	s = ParcelLoadString(STRID_CREATE_SAMPLE_COLSPUSE);
	m_listSpec.InsertColumn(1, s, LVCFMT_RIGHT, 0, 1);
	s = ParcelLoadString(STRID_CREATE_SAMPLE_COLSPDATE);
	m_listSpec.InsertColumn(2, s, LVCFMT_RIGHT, 0, 2);
	for(i=0; i<3; i++)
		m_listSpec.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	m_listSpec.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

	FillListSpec();

	GetDlgItem(IDC_ACCEPT)->SetWindowText(ParcelLoadString(STRID_CREATE_SAMPLE_ACCEPT));
	GetDlgItem(IDCANCEL)->SetWindowText(ParcelLoadString(STRID_CREATE_SAMPLE_DECLINE));
	GetDlgItem(IDHELP)->SetWindowText(ParcelLoadString(STRID_CREATE_SAMPLE_HELP));
	GetDlgItem(IDC_STATIC_1)->SetWindowText(ParcelLoadString(STRID_CREATE_SAMPLE_NAME));
	GetDlgItem(IDC_STATIC_2)->SetWindowText(ParcelLoadString(STRID_CREATE_SAMPLE_IDEN2));
	GetDlgItem(IDC_STATIC_3)->SetWindowText(ParcelLoadString(STRID_CREATE_SAMPLE_IDEN3));
	GetDlgItem(IDC_STATIC_4)->SetWindowText(ParcelLoadString(STRID_CREATE_SAMPLE_REFDATA));
	GetDlgItem(IDC_STATIC_5)->SetWindowText(ParcelLoadString(STRID_CREATE_SAMPLE_SPECLIST));
	GetDlgItem(IDC_STATIC_7)->SetWindowText(ParcelLoadString(STRID_CREATE_SAMPLE_COMMENT));
	GetDlgItem(IDC_SET_NO_VALUE)->SetWindowText(ParcelLoadString(STRID_CREATE_SAMPLE_NODATATXT));
	GetDlgItem(IDC_REMOVE)->SetWindowText(ParcelLoadString(STRID_CREATE_SAMPLE_REMOVE));
	GetDlgItem(IDC_PLOT)->SetWindowText(ParcelLoadString(STRID_CREATE_SAMPLE_PLOT));
	GetDlgItem(IDC_PLOT_2)->SetWindowText(ParcelLoadString(STRID_CREATE_SAMPLE_TEXT));

	if(Mode == SAMDLG_MODE_SHOW)
	{
		GetDlgItem(IDC_SET_NO_VALUE)->EnableWindow(false);
		GetDlgItem(IDC_INPUT_VALUE)->EnableWindow(false);
		GetDlgItem(IDC_INPUT_NAME)->EnableWindow(false);
	}

//	GetDlgItem(IDC_PLOT_2)->EnableWindow(false);

	GetDlgItem(IDHELP)->EnableWindow(pFrame->IsExistHelp());

	return true;
}

void CCreateSampleDlg::OnAccept()
{
	if(!UpdateData())
		return;

	CString s;

	m_vName.Trim();
	if(m_vName.IsEmpty())
	{
		ParcelError(ParcelLoadString(STRID_CREATE_SAMPLE_ERROR_EMPTYNAME));
		return;
	}
	if(m_vName[0] == '*')
	{
		ParcelError(ParcelLoadString(STRID_CREATE_SAMPLE_ERROR_ASTERNAME));
		return;
	}
	if(Mode == SAMDLG_MODE_CREATE || SamData->GetName().Compare(m_vName))
	{
		CSampleData *sam = ParentPrj->GetSample(m_vName);
		if(sam != NULL)
		{
			s.Format(ParcelLoadString(STRID_CREATE_SAMPLE_ERROR_NAME), m_vName);
			ParcelError(s);
			return;
		}
	}

	SamData->SetName(m_vName);
	SamData->SetIden2(m_vIden2);
	SamData->SetIden3(m_vIden3);

	CDialog::OnOK();
}

void CCreateSampleDlg::OnOK()
{
	if(GetFocus() == GetDlgItem(IDC_INPUT_VALUE))
	{
		GetDlgItem(IDC_REFDATA_LIST)->SetFocus();
	}
/*
	if(!IsEditRef) 
		FillFields();
*/
}

void CCreateSampleDlg::OnCancel()
{
	if(Mode == SAMDLG_MODE_CHANGE && IsChanged())
	{
		if(!ParcelConfirm(ParcelLoadString(STRID_CREATE_SAMPLE_ASK_DOCANCEL), false))
			return;
	}

	iscancel = true;

	CDialog::OnCancel();
}

void CCreateSampleDlg::OnChangeRefData()
{
	if(iscancel)
		return;
	if(GetFocus() == GetDlgItem(IDCANCEL))
	{
		OnCancel();
		return;
	}

	if(!IsEditRef)
		SelRef = CurRef;

	if(SelRef == SamData->RefData.end())
		return;

	bool old = SelRef->bNoValue;

	SelRef->bNoValue = (m_NoValue.GetCheck()) ? true : false;
	if(SelRef->bNoValue)
	{
		if(!old)
		{
			GetDlgItem(IDC_INPUT_VALUE)->SetWindowText(ParcelLoadString(STRID_CREATE_SAMPLE_NODATA));
			FillListRef();
		}
		GetDlgItem(IDC_STR_UNIT)->EnableWindow(false);
		GetDlgItem(IDC_INPUT_VALUE)->EnableWindow(false);
		FillListRef();
		return;
	}

	GetDlgItem(IDC_STR_UNIT)->EnableWindow(true);
	GetDlgItem(IDC_INPUT_VALUE)->EnableWindow(true);

	if(old)
	{
		CString s;
		s.Format(ParentPrj->GetParam(SelRef->Name)->GetFormat(), SelRef->Value);
		GetDlgItem(IDC_INPUT_VALUE)->SetWindowText(s);
		FillListRef();
		return;
	}

	if(!UpdateData())
		return;

	SelRef->Value = m_vValue;
	IsEditRef = false;

	FillListRef();
}

void CCreateSampleDlg::OnStartRefData()
{
	SelRef = CurRef;
	IsEditRef = true;
}

void CCreateSampleDlg::OnRemove()
{
	if(!ParcelConfirm(ParcelLoadString(STRID_CREATE_SAMPLE_ASK_DEL), false))
		return;

	int sel = int(CurSpec - SamData->SpecData.begin());
	if(sel < 0 || sel >= int(SamData->SpecData.size()))
		return;

	int num = CurSpec->Num;
	if(!SamData->DeleteSpectrum(num))
		return;

	FillListSpec();
}

void CCreateSampleDlg::OnExclude()
{
	if(CurSpec == SamData->SpecData.end())
		return;
	int sel = int(CurSpec - SamData->SpecData.begin());
	if(sel < 0 || sel >= int(SamData->SpecData.size()))
		return;

	CurSpec->bUse = !CurSpec->bUse;

	CString s = (CurSpec->bUse) ? ParcelLoadString(STRID_CREATE_SAMPLE_EXCLUDE)
		: ParcelLoadString(STRID_CREATE_SAMPLE_INCLUDE);
	GetDlgItem(IDC_EXCLUDE)->SetWindowText(s);

	s = (CurSpec->bUse) ? ParcelLoadString(STRID_CREATE_SAMPLE_YES) : ParcelLoadString(STRID_CREATE_SAMPLE_NO);
	m_listSpec.SetItemText(sel, 1, s);

	GetDlgItem(IDC_SPECTRUM_LIST)->SetFocus();
}

void CCreateSampleDlg::OnShowPlot()
{
	CTransferData* pTrans = ParentPrj->GetTransOwn();

	CShowSpectra ShowSp(&pTrans->SpecPoints);

	ItSpc it;
	int i;
	for(i=0, it=SamData->SpecData.begin(); it!=SamData->SpecData.end(); ++it, i++)
	{
		bool issel = (m_listSpec.GetItemState(i, LVIS_SELECTED) == LVIS_SELECTED);

		SpectraShowSettings* NewSS = ShowSp.AddSpectraShow(SamData->GetName(), it->Num, 0, 0, issel);
		if(NewSS == NULL)
			continue;

		copy(it->Data.begin(), it->Data.end(), inserter(NewSS->SpecData, NewSS->SpecData.begin()));
	}

	CShowSpectraDlg dlg(&ShowSp, C_PLOT_SET_MODE_SAMPLE, this);
	dlg.DoModal();
}

void CCreateSampleDlg::OnShowText()
{
	CTransferData* pTrans = ParentPrj->GetTransOwn();

	VDbl points;
	VDbl data;
	int i, j, N = pTrans->GetNSpecPoints();
	for(i=0; i<N; i++)
		points.push_back(pTrans->GetSpecPoint(i));

	ItSpc it;
	CString SpName = SamData->GetName();
	CString fmt = _T("%s-%02d");
	CString tmp = SpName;
	for(j=0, it=SamData->SpecData.begin(); it!=SamData->SpecData.end(); ++it, j++)
	{
		if(m_listSpec.GetItemState(j, LVIS_SELECTED) == LVIS_SELECTED)
		{
			copy(it->Data.begin(), it->Data.end(), inserter(data, data.begin()));
			SpName.Format(fmt, tmp, it->Num);
			break;
		}
	}

	CShowSpectrumTextDlg dlg(&points, &data, SpName, this);
	dlg.DoModal();
}

void CCreateSampleDlg::OnRefDataListSelChange(NMHDR* /*pNMHDR*/, LRESULT *pResult)
{
	int sel = m_listRef.GetNextItem(-1, LVNI_SELECTED);
	if(sel < 0 || sel >= int(SamData->RefData.size()))
		return;

	CurRef = SamData->RefData.begin() + sel;

	if(!IsEditRef)
		FillFields();

	*pResult = 0;
}

void CCreateSampleDlg::OnSpectraListSelChange(NMHDR* /*pNMHDR*/, LRESULT *pResult)
{
	int sel = m_listSpec.GetNextItem(-1, LVNI_SELECTED);
	if(sel < 0 || sel >= int(SamData->SpecData.size()))
		return;

	CurSpec = SamData->SpecData.begin() + sel;

	CString s = (CurSpec->bUse) ? ParcelLoadString(STRID_CREATE_SAMPLE_EXCLUDE)
		: ParcelLoadString(STRID_CREATE_SAMPLE_INCLUDE);
	GetDlgItem(IDC_EXCLUDE)->SetWindowText(s);

	if(Mode == SAMDLG_MODE_SHOW)
	{
		GetDlgItem(IDC_EXCLUDE)->EnableWindow(false);
		GetDlgItem(IDC_REMOVE)->EnableWindow(false);
	}
	if(Mode == SAMDLG_MODE_CHANGE && int(SamData->SpecData.size()) > 0)
	{
		int keep = ParentPrj->GetSpectrumKeepStatus(SamData->GetName(), sel);
		GetDlgItem(IDC_EXCLUDE)->EnableWindow(keep == C_KEEP_FREE || keep == C_KEEP_NOTFOUND);
		GetDlgItem(IDC_REMOVE)->EnableWindow(keep == C_KEEP_FREE || keep == C_KEEP_NOTFOUND);
	}

	*pResult = 0;
}

void CCreateSampleDlg::FillListRef()
{
	m_listRef.DeleteAllItems();

	CString s;
	ItRef it;
	int i;
	for(i=0, it=SamData->RefData.begin(); it!=SamData->RefData.end(); ++it, i++)
	{
		CParamData *Par = ParentPrj->GetParam(it->Name);
		s.Format(cFmtS_S, Par->GetName(), Par->GetUnit());
		m_listRef.InsertItem(i, s);

		if(it->bNoValue)
			s = ParcelLoadString(STRID_CREATE_SAMPLE_NODATA);
		else
			s.Format(Par->GetFormat(), it->Value);
		m_listRef.SetItemText(i, 1, s);
	}
	for(i=0; i<3; i++)
		m_listRef.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);

	int sel = (CurRef != SamData->RefData.end()) ? int(CurRef - SamData->RefData.begin()) : 0;
	m_listRef.SetItemState(sel, LVIS_SELECTED, LVIS_SELECTED);

	FillFields();
}

void CCreateSampleDlg::FillFields()
{
	CString s;
	if(CurRef == SamData->RefData.end())
	{
		GetDlgItem(IDC_STATIC_6)->SetWindowText(ParcelLoadString(STRID_CREATE_SAMPLE_PARAM));
		GetDlgItem(IDC_STR_UNIT)->SetWindowText(ParcelLoadString(STRID_CREATE_SAMPLE_DEFUNIT));
		GetDlgItem(IDC_INPUT_VALUE)->SetWindowText(ParcelLoadString(STRID_CREATE_SAMPLE_NODATA));

		GetDlgItem(IDC_SET_NO_VALUE)->EnableWindow(false);
		GetDlgItem(IDC_STR_UNIT)->EnableWindow(false);
		GetDlgItem(IDC_INPUT_VALUE)->EnableWindow(false);
	}
	else
	{
		m_vNoValue = CurRef->bNoValue;
		m_vValue = CurRef->Value;
		bool ch3 = !(ParentPrj->IsSampleRefKeep(SamData->GetName(), cOwn, CurRef->Name));
		bool ch2 = !CurRef->bNoValue;
		int stat = ParentPrj->GetSampleKeepStatus(SamData->GetName());
		bool ch4 = (stat == C_KEEP_FREE || stat == C_KEEP_NOTFOUND);
		
		CParamData *Par = ParentPrj->GetParam(CurRef->Name);
		if(ch2)
		{
			s.Format(Par->GetFormat(), CurRef->Value);
			GetDlgItem(IDC_INPUT_VALUE)->SetWindowText(s);
		}
		else
			GetDlgItem(IDC_INPUT_VALUE)->SetWindowText(ParcelLoadString(STRID_CREATE_SAMPLE_NODATA));

		m_NoValue.SetCheck(m_vNoValue);

		s.Format(ParcelLoadString(STRID_CREATE_SAMPLE_FMTPARNAME), CurRef->Name);
		GetDlgItem(IDC_STATIC_6)->SetWindowText(s);
		GetDlgItem(IDC_STR_UNIT)->SetWindowText(Par->GetUnit());

		GetDlgItem(IDC_STR_UNIT)->EnableWindow(ch3 && ch2);
		GetDlgItem(IDC_STATIC_1)->EnableWindow(ch4);

		if(Mode == SAMDLG_MODE_SHOW)
		{
			GetDlgItem(IDC_INPUT_NAME)->EnableWindow(false);
			GetDlgItem(IDC_SET_NO_VALUE)->EnableWindow(false);
			GetDlgItem(IDC_INPUT_VALUE)->EnableWindow(false);
		}
		else
		{
			GetDlgItem(IDC_INPUT_NAME)->EnableWindow(ch4);
			GetDlgItem(IDC_SET_NO_VALUE)->EnableWindow(ch3);
			GetDlgItem(IDC_INPUT_VALUE)->EnableWindow(ch3 && ch2);
		}
	}

	GetDlgItem(IDC_REFDATA_LIST)->SetFocus();
}

void CCreateSampleDlg::FillListSpec()
{
	m_listSpec.DeleteAllItems();

	CString s;
	ItSpc it;
	int i;
	for(i=0, it=SamData->SpecData.begin(); it!=SamData->SpecData.end(); ++it, i++)
	{
		s.Format(cFmt02, it->Num);
		m_listSpec.InsertItem(i, s);

		s = (it->bUse) ? ParcelLoadString(STRID_CREATE_SAMPLE_YES) : ParcelLoadString(STRID_CREATE_SAMPLE_NO);
		m_listSpec.SetItemText(i, 1, s);
		s = it->DateTime.Format(cFmtDate);
		m_listSpec.SetItemText(i, 2, s);
	}
	for(i=0; i<3; i++)
		m_listSpec.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);

	int sel = (CurSpec != SamData->SpecData.end()) ? int(CurSpec - SamData->SpecData.begin()) : 0;
	m_listSpec.SetItemState(sel, LVIS_SELECTED, LVIS_SELECTED);

	if(SamData->SpecData.size() == 0)
	{
		GetDlgItem(IDC_EXCLUDE)->SetWindowText(ParcelLoadString(STRID_CREATE_SAMPLE_EXCLUDE));
		GetDlgItem(IDC_REMOVE)->EnableWindow(false);
		GetDlgItem(IDC_EXCLUDE)->EnableWindow(false);
		GetDlgItem(IDC_PLOT)->EnableWindow(false);
		GetDlgItem(IDC_PLOT_2)->EnableWindow(false);
		GetDlgItem(IDC_STATIC_5)->EnableWindow(false);
	}
	else
	{
		int keep = ParentPrj->GetSpectrumKeepStatus(SamData->GetName(), sel);
		if(Mode == SAMDLG_MODE_SHOW || (Mode == SAMDLG_MODE_CHANGE && keep != C_KEEP_FREE && keep != C_KEEP_NOTFOUND))
		{
			GetDlgItem(IDC_EXCLUDE)->EnableWindow(false);
			GetDlgItem(IDC_REMOVE)->EnableWindow(false);
		}
		else
		{
			GetDlgItem(IDC_EXCLUDE)->EnableWindow(true);
			GetDlgItem(IDC_REMOVE)->EnableWindow(true);
		}

		GetDlgItem(IDC_PLOT)->EnableWindow(true);
		GetDlgItem(IDC_PLOT_2)->EnableWindow(true);
		GetDlgItem(IDC_STATIC_5)->EnableWindow(true);
	}
}

int CCreateSampleDlg::CalcNewSpecNum()
{
	int max = 0;
	ItSpc it;
	for(it=SamData->SpecData.begin(); it!=SamData->SpecData.end(); ++it)
		if(max < (*it).Num)
			max = (*it).Num;

	return max + 1;
}

bool CCreateSampleDlg::IsChanged()
{
	return true;
}

void CCreateSampleDlg::OnHelp()
{
	int IdHelp = DLGID_MAIN;
	switch(Mode)
	{
	case SAMDLG_MODE_CREATE: IdHelp = DLGID_SAMPLE_CREATE;  break;
	case SAMDLG_MODE_CHANGE: IdHelp = DLGID_SAMPLE_CHANGE;  break;
	}

	pFrame->ShowHelp(IdHelp);
}

void CCreateSampleDlg::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	// TODO: Add your message handler code here and/or call default

	CDialog::OnKeyUp(nChar, nRepCnt, nFlags);
}
