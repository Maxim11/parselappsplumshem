#ifndef _EDIT_SAMPLE_LIST_DLG_H_
#define _EDIT_SAMPLE_LIST_DLG_H_

#pragma once

#include "SampleData.h"
#include "ParcelListCtrl.h"
#include "Resource.h"


///////////////////////////////////////////////////////////////////////////////////////////////////
// ������ �������������� ������ ��������

class CEditSampleListDlg : public CDialog
{
	DECLARE_DYNAMIC(CEditSampleListDlg)

public:

	CEditSampleListDlg(CProjectData* PrjData, LSam* samplesdata, HTREEITEM item, CWnd* pParent = NULL);	// �����������
	virtual ~CEditSampleListDlg();																		// ����������

	enum { IDD = IDD_EDIT_SAMPLE_LIST };	// ������������� �������

	CParcelListCtrl m_list;					// ������ ��������
//	CListCtrl m_list;					// ������ ��������

protected:

	CProjectData *ParentPrj;				// ��������� �� ������, ������������ ��� ��������
	CTransferData* Trans;
	LSam* SamplesList;						// ��������� �� ������ �������� ��� ��������������
	int m_nParStart;
	int m_nTimeCol;

	int GetSamSel();
	VStr SamSel;


protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();						// ������������� �������
/*
	virtual void OnOK();				// ��������� ������� "������� ������ � ����� �� �������"
	virtual void OnCancel();			// ��������� ������� "�������� ���� � ����� �� �������"
*/
	afx_msg void OnAdd();				// ��������� ������� "������� ����� �������"
	afx_msg void OnEdit();				// ��������� ������� "�������� �������"
	afx_msg void OnRemove();			// ��������� ������� "������� �������"
	afx_msg void OnHelp();				// ��������� ������� "������� �������"

	afx_msg void OnSamplesListSelChange(NMHDR* /*pNMHDR*/, LRESULT *pResult);// ��������� ������� "�������� �������� � ������ ��������"
	afx_msg void OnSamplesListColumnClick(NMHDR* pNMHDR, LRESULT* pResult);

	void FillList(int sel, bool bFromScratch = true);		// ��������� ������ ��������
	int GetNUsesSpectrum();							// ������� ������ ����� ���������� ��������

	void SetSortIcons();

public:

	bool IsSampleUse(int ind);

protected:

	DECLARE_MESSAGE_MAP()
};

#endif
