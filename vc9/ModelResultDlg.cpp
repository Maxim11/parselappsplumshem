#include "stdafx.h"

#include "BranchInfo.h"
#include "ModelResultDlg.h"
#include "ChooseGKDlg.h"
#include "SetTemplateDlg.h"
#include "MainFrm.h"
#include "ParcelReport.h"
#include "RootData.h"
#include "FileSystem.h"
#include "Registry/RegKey.h"

#include <math.h>

using namespace filesystem;


IMPLEMENT_DYNAMIC(CModelResultDlg, CDialog)

static CModelResultDlg* pModRes = NULL;

static GraphInfo* pGISEC = NULL;
static GraphInfo* pGISECV = NULL;
static GraphInfo* pGISEV = NULL;
static GraphInfo* pHICalib = NULL;
static GraphInfo* pHIValid = NULL;
static GraphInfo* pHISamCalib = NULL;
static GraphInfo* pHISamValid = NULL;
static GraphInfo* pHIBad = NULL;
static GraphInfo* pGIScores = NULL;
static GraphInfo* pGIChemLoad = NULL;

static double fGraphLine0(double x)
{
	return x;
}

double fHistoMDL(double /*x*/)
{
	return pModRes->GetMaxMah();
}

double fHistoNorm(double /*x*/)
{
	return pModRes->GetNorm();
}

double fGraphLineSEC(double x)
{
	return pGISEC->b * x + pGISEC->a;
}

double fGraphLineSECV(double x)
{
	return pGISECV->b * x + pGISECV->a;
}

double fGraphLineSEV(double x)
{
	return pGISEV->b * x + pGISEV->a;
}

static int GetTextColor(int item, int subitem, void* /*pData*/)
{
	if(pModRes == NULL)
		return 0;

	int iTab = pModRes->GetCurTab();
	COLORREF color = CLR_DEFAULT;
	switch(iTab)
	{
	case C_MODRES_PAGE_SEC:
	case C_MODRES_PAGE_SECV:
	case C_MODRES_PAGE_SEV:
		if(subitem != 0 && pModRes->IsResultBad(item))
			color = cColorErr;
		break;
	case C_MODRES_PAGE_OUT:
		if(subitem != 0 && pModRes->IsResultBad(item))
			color = cColorErr;
		break;
	case C_MODRES_PAGE_BAD:
		if(subitem != 0 && pModRes->IsResultBad(item))
			color = cColorErr;
		break;
	}

	pModRes->m_listBig.CurTxtColor = color;

	return 1;
}

static int LbClickCallback(UINT /*nFlags*/, CPoint point, void *pData)
{
	int sh = 3;
	if(pModRes->GetCurTab() == C_MODRES_PAGE_OUT || pModRes->GetCurTab() == C_MODRES_PAGE_BAD)
		sh = 1;
	if(pModRes->GetCurTab() == C_MODRES_PAGE_SPECLOAD)
		sh = 2;

	CRect rect;
	rect.left = point.x - sh;
	rect.right = point.x + sh;
	rect.top = point.y - sh;
	rect.bottom = point.y + sh;

	if(pModRes != NULL)
	{
		pModRes->SelectByRect(&rect, false);
		pModRes->ShowCoord(&point);
	}

	return 1;
}

static int LbUnClickCallback(UINT /*nFlags*/, CPoint /*point*/, void *pData)
{
	if(pModRes != NULL)
	{
		pModRes->RemoveInfo();
		if(pData != NULL)
		{
			CRect *pRect = (CRect*)pData;
			pModRes->SelectByRect(pRect);
		}
	}

	return 1;
}

static int RbClickCallback(UINT /*nFlags*/, CPoint /*point*/, void *pData)
{
	return 1;
}

static int RbUnClickCallback(UINT /*nFlags*/, CPoint /*point*/, void *pData)
{
	if(pModRes != NULL)
		pModRes->SetStatusMenu();

	return 1;
}

CModelResultDlg::CModelResultDlg(CModelData* Mod, int mode, CWnd* pParent) : CDialog(CModelResultDlg::IDD, pParent)
{
	pModRes = this;

	StartModel = Mod;
	Mode = mode;

	ModData = new(CModelData)(StartModel->GetParentHItem());
	ModData->Copy(*StartModel);
	ModTmp.clear();

	m_vTab = C_MODRES_PAGE_SEC;
	m_vParam = 0;
	m_vShow = C_MODRES_SHOW_TABLE;
	m_vCalibValid = C_MODRES_SMPL_CALIB;
	m_vSEC = 2.;
	m_vSECV = 2.;
	m_vSEV = 2.;
	m_vCorrB = ModData->GetModelParam(m_vParam)->CorrB;
	m_vCorrA = ModData->GetModelParam(m_vParam)->CorrA;
	m_vMaxMah = Mod->GetMaxMah();
	m_vOXsc = 0;
	m_vOYsc = (ModData->GetNCompQnt() > 1) ? 1 : 0;
	m_vOXch = 0;
	m_vOYch = (ModData->GetNCompQnt() > 1) ? 1 : 0;

	IsAvr = ModData->ModelResult.IsAverage();

	pGraphSEC = NULL;
	pGraphSECV = NULL;
	pGraphSEV = NULL;
	pGraphLine0 = NULL;
	pGraphLineSEC = NULL;
	pGraphLineSECV = NULL;
	pGraphLineSEV = NULL;
	pHistoCalib = NULL;
	pHistoValid = NULL;
	pHistoSamCalib = NULL;
	pHistoSamValid = NULL;
	pHistoBad = NULL;
	pHistoMDL = NULL;
	pHistoNorm = NULL;
	pGraphScores = NULL;
	pGraphChemLoad = NULL;

	pMarkSEC = NULL;
	pMarkSECV = NULL;
	pMarkSEV = NULL;
	pMarkHCalib = NULL;
	pMarkHValid = NULL;
	pMarkHBad = NULL;
	pMarkScores = NULL;
	pMarkChemLoad = NULL;

	posInfo = NULL;
	posCoord = NULL;
	posLegendSEC_V = NULL;
	posLegendOut = NULL;
	posLegendBad = NULL;
	posLegendSpec = NULL;
	posLegendChem = NULL;

	pMenu1 = new CMenu;
	pMenu1->CreatePopupMenu();

	show_legend = false;
	show_trend = true;
	show_teor = true;
	show_mdl = true;
	show_limit = true;
}

CModelResultDlg::~CModelResultDlg()
{
	if(ModData != NULL)  delete ModData;
	ModTmp.clear();

	if(pGraphSEC != NULL)  delete pGraphSEC;
	if(pGraphSECV != NULL)  delete pGraphSECV;
	if(pGraphSEV != NULL)  delete pGraphSEV;
	if(pGraphLine0 != NULL)  delete pGraphLine0;
	if(pGraphLineSEC != NULL)  delete pGraphLineSEC;
	if(pGraphLineSECV != NULL)  delete pGraphLineSECV;
	if(pGraphLineSEV != NULL)  delete pGraphLineSEV;
	if(pHistoCalib != NULL)  delete pHistoCalib;
	if(pHistoValid != NULL)  delete pHistoValid;
	if(pHistoSamCalib != NULL)  delete pHistoSamCalib;
	if(pHistoSamValid != NULL)  delete pHistoSamValid;
	if(pHistoBad != NULL)  delete pHistoBad;
	if(pHistoMDL != NULL)  delete pHistoMDL;
	if(pHistoNorm != NULL)  delete pHistoNorm;
	if(pGraphScores != NULL)  delete pGraphScores;
	GraphsSpecLoad.clear();
	MasksSLPos.clear();
	if(pGraphChemLoad != NULL)  delete pGraphChemLoad;

	if(pMarkSEC != NULL)  delete[] pMarkSEC;
	if(pMarkSECV != NULL)  delete[] pMarkSECV;
	if(pMarkSEV != NULL)  delete[] pMarkSEV;
	if(pMarkHCalib != NULL)  delete[] pMarkHCalib;
	if(pMarkHValid != NULL)  delete[] pMarkHValid;
	if(pMarkHBad != NULL)  delete[] pMarkHBad;
	if(pMarkScores != NULL)  delete[] pMarkScores;
	if(pMarkChemLoad != NULL)  delete[] pMarkChemLoad;

	pMenu1->DestroyMenu();
	delete pMenu1;
}

void CModelResultDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_TAB_1, m_Tab);
	DDX_Control(pDX, IDC_MODEL_RESULT_TABLEBIG, m_listBig);
	DDX_Control(pDX, IDC_MODEL_RESULT_TABLESMALL, m_listSmall);
	DDX_Control(pDX, IDC_PARAM, m_Param);
	DDX_Control(pDX, IDC_SHOW, m_Show);
	DDX_Control(pDX, IDC_CALIBVALID, m_CalibValid);
	DDX_Control(pDX, IDC_CRIT, m_SEC);
	DDX_Control(pDX, IDC_CRIT2, m_SECV);
	DDX_Control(pDX, IDC_CRIT3, m_SEV);
	DDX_Control(pDX, IDC_MAXMAH, m_MaxMah);
	DDX_Control(pDX, IDC_CORR_CALIB_B, m_CorrB);
	DDX_Control(pDX, IDC_CORR_CALIB_A, m_CorrA);
	DDX_Control(pDX, IDC_AXE_OX, m_OXsc);
	DDX_Control(pDX, IDC_AXE_OY, m_OYsc);
	DDX_Control(pDX, IDC_AXE_OX_2, m_OXch);
	DDX_Control(pDX, IDC_AXE_OY_2, m_OYch);

	DDX_CBIndex(pDX, IDC_PARAM, m_vParam);
	DDX_CBIndex(pDX, IDC_SHOW, m_vShow);
	DDX_CBIndex(pDX, IDC_CALIBVALID, m_vCalibValid);
	DDX_CBIndex(pDX, IDC_AXE_OX, m_vOXsc);
	DDX_CBIndex(pDX, IDC_AXE_OY, m_vOYsc);
	DDX_CBIndex(pDX, IDC_AXE_OX_2, m_vOXch);
	DDX_CBIndex(pDX, IDC_AXE_OY_2, m_vOYch);

	cParcelDDX(pDX, IDC_CRIT, m_vSEC);
	cParcelDDV(pDX, ParcelLoadString(STRID_MODEL_RESULT_DDV_SEC), m_vSEC, 0.001, 10.);
	cParcelDDX(pDX, IDC_CRIT2, m_vSECV);
	cParcelDDV(pDX, ParcelLoadString(STRID_MODEL_RESULT_DDV_SECV), m_vSECV, 0.001, 10.);
	cParcelDDX(pDX, IDC_CRIT3, m_vSEV);
	cParcelDDV(pDX, ParcelLoadString(STRID_MODEL_RESULT_DDV_SEV), m_vSEV, 0.001, 10.);
	cParcelDDX(pDX, IDC_CORR_CALIB_B, m_vCorrB);
	cParcelDDV(pDX, ParcelLoadString(STRID_MODEL_RESULT_DDV_CORR_B), m_vCorrB, -1.e9, 1.e9);
	cParcelDDX(pDX, IDC_CORR_CALIB_A, m_vCorrA);
	cParcelDDV(pDX, ParcelLoadString(STRID_MODEL_RESULT_DDV_CORR_A), m_vCorrA, -1.e9, 1.e9);
	cParcelDDX(pDX, IDC_MAXMAH, m_vMaxMah);
	cParcelDDV(pDX, ParcelLoadString(STRID_MODEL_RESULT_DDV_MAXMAH), m_vMaxMah, 0.1, 1.e9);

}

BEGIN_MESSAGE_MAP(CModelResultDlg, CDialog)

	ON_COMMAND(C_MODRES_MENU_PREV, OnUserMenuPrev)
	ON_COMMAND(C_MODRES_MENU_NEXT, OnUserMenuNext)
	ON_COMMAND(C_MODRES_MENU_FULL, OnUserMenuFull)
	ON_COMMAND(C_MODRES_MENU_AUTO, OnUserMenuAuto)
	ON_COMMAND(C_MODRES_MENU_LEFT, OnUserMenuLeft)
	ON_COMMAND(C_MODRES_MENU_RIGHT, OnUserMenuRight)
	ON_COMMAND(C_MODRES_MENU_LEGEND, OnUserMenuLegend)
	ON_COMMAND(C_MODRES_MENU_GRID, OnUserMenuGrid)
	ON_COMMAND(C_MODRES_MENU_TREND, OnUserMenuTrend)
	ON_COMMAND(C_MODRES_MENU_TEOR, OnUserMenuTeor)
	ON_COMMAND(C_MODRES_MENU_MDL, OnUserMenuMDL)
	ON_COMMAND(C_MODRES_MENU_LIMIT, OnUserMenuLimit)

	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB_1, OnTabSelchange)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_MODEL_RESULT_TABLEBIG, OnTableBigSelChange)
	ON_NOTIFY(HDN_ITEMCLICK, 0, OnTableBigColumnClick)

	ON_CBN_SELCHANGE(IDC_PARAM, OnChangeParam)
	ON_CBN_SELCHANGE(IDC_SHOW, OnChangeView)
	ON_CBN_SELCHANGE(IDC_CALIBVALID, OnChangeCalibValid)
	ON_CBN_SELCHANGE(IDC_AXE_OX, OnChangeAxeGKsc)
	ON_CBN_SELCHANGE(IDC_AXE_OY, OnChangeAxeGKsc)
	ON_CBN_SELCHANGE(IDC_AXE_OX_2, OnChangeAxeGKch)
	ON_CBN_SELCHANGE(IDC_AXE_OY_2, OnChangeAxeGKch)

	ON_BN_CLICKED(IDC_CANCEL, OnClose)
	ON_BN_CLICKED(IDC_ACCEPT, OnAccept)
	ON_BN_CLICKED(IDC_SET_TEMPLATE, OnSetTemplate)
	ON_BN_CLICKED(IDC_CREATE_PROTOCOL, OnCreateProtocol)
	ON_BN_CLICKED(IDHELP, OnHelp)
	ON_BN_CLICKED(IDC_EXCLUDE, OnExclude)
	ON_BN_CLICKED(IDC_BACK, OnBack)
	ON_BN_CLICKED(IDC_BACK_ALL, OnBackAll)
	ON_BN_CLICKED(IDC_RECALC, OnRecalc)
	ON_BN_CLICKED(IDC_TREND, OnFromTrend)
	ON_BN_CLICKED(IDC_REFRESH, OnRefresh)
	ON_BN_CLICKED(IDC_SETTINGS, OnSettings)

END_MESSAGE_MAP()

BOOL CModelResultDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	USES_CONVERSION;

	CString s;
	int i;

	TC_ITEM TabCtrlItem;
	TabCtrlItem.mask = TCIF_TEXT;
	TabCtrlItem.pszText = (LPWSTR)GETSTRUI(STRID_MODEL_RESULT_SEC_HDR);
	m_Tab.InsertItem(C_MODRES_PAGE_SEC, &TabCtrlItem);
	TabCtrlItem.pszText = (LPWSTR)GETSTRUI(STRID_MODEL_RESULT_SECV_HDR);
	m_Tab.InsertItem(C_MODRES_PAGE_SECV, &TabCtrlItem);
	TabCtrlItem.pszText = (LPWSTR)GETSTRUI(STRID_MODEL_RESULT_SEV_HDR);
	m_Tab.InsertItem(C_MODRES_PAGE_SEV, &TabCtrlItem);
	TabCtrlItem.pszText = (LPWSTR)GETSTRUI(STRID_MODEL_RESULT_OUT_HDR);
	m_Tab.InsertItem(C_MODRES_PAGE_OUT, &TabCtrlItem);
	TabCtrlItem.pszText = (LPWSTR)GETSTRUI(STRID_MODEL_RESULT_BAD_HDR);
	m_Tab.InsertItem(C_MODRES_PAGE_BAD, &TabCtrlItem);
	TabCtrlItem.pszText = (LPWSTR)GETSTRUI(STRID_MODEL_RESULT_SCORES_HDR);
	m_Tab.InsertItem(C_MODRES_PAGE_SCORES, &TabCtrlItem);
	TabCtrlItem.pszText = (LPWSTR)GETSTRUI(STRID_MODEL_RESULT_SPECLOAD_HDR);
	m_Tab.InsertItem(C_MODRES_PAGE_SPECLOAD, &TabCtrlItem);
	TabCtrlItem.pszText = (LPWSTR)GETSTRUI(STRID_MODEL_RESULT_CHEMLOAD_HDR);
	m_Tab.InsertItem(C_MODRES_PAGE_CHEMLOAD, &TabCtrlItem);

	m_CalibValid.ResetContent();
	s = ParcelLoadString(STRID_MODEL_RESULT_SAMCALIB);
	m_CalibValid.AddString(s);
	s = ParcelLoadString(STRID_MODEL_RESULT_SAMVALID);
	m_CalibValid.AddString(s);
	m_CalibValid.SetCurSel(m_vCalibValid);

	m_Param.ResetContent();
	for(i=0; i<ModData->GetNModelParams(); i++)
	{
		s = ModData->GetModelParam(i)->ParamName;
		m_Param.AddString(s);
	}
	m_Param.SetCurSel(m_vParam);

	m_OXsc.ResetContent();
	m_OYsc.ResetContent();
	m_OXch.ResetContent();
	m_OYch.ResetContent();
	CString fmtGK = ParcelLoadString(STRID_MODEL_RESULT_SPECLOAD_COL_GK);
	for(i=0; i<ModData->GetNCompQnt(); i++)
	{
		s.Format(fmtGK, i + 1);
		m_OXsc.AddString(s);
		m_OYsc.AddString(s);
		m_OXch.AddString(s);
		m_OYch.AddString(s);
	}
	m_OXsc.SetCurSel(m_vOXsc);
	m_OYsc.SetCurSel(m_vOYsc);
	m_OXch.SetCurSel(m_vOXch);
	m_OYch.SetCurSel(m_vOYch);

	s = ParcelLoadString(STRID_MODEL_RESULT_COL_NUM);
	m_listBig.InsertColumn(0, s, LVCFMT_CENTER, 0, 0);
	m_listBig.SetColumnWidth(0, LVSCW_AUTOSIZE_USEHEADER);
	m_listBig.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);
	m_listBig.SetIconList();
	m_listBig.SetUserCallback(LCF_CB_SI_TXTCOLOR, GetTextColor);
	m_listBig.EnableUserCallback(LCF_CB_SI_TXTCOLOR, true);

	s = ParcelLoadString(STRID_MODEL_RESULT_COL_PARAM);
	m_listSmall.InsertColumn(0, s, LVCFMT_LEFT, 0, 0);
	m_listSmall.SetColumnWidth(0, LVSCW_AUTOSIZE_USEHEADER);
	m_listSmall.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

	CreateChart();

	CString Hdr;
	Hdr.Format(ParcelLoadString(STRID_MODEL_RESULT_HDR), ModData->GetName());
	SetWindowText(Hdr);

	GetDlgItem(IDC_ACCEPT)->SetWindowText(ParcelLoadString(STRID_MODEL_RESULT_ACCEPT));
	GetDlgItem(IDHELP)->SetWindowText(ParcelLoadString(STRID_MODEL_RESULT_HELP));
	GetDlgItem(IDC_SET_TEMPLATE)->SetWindowText(ParcelLoadString(STRID_MODEL_RESULT_SET_TEMPLATE));
	GetDlgItem(IDC_CREATE_PROTOCOL)->SetWindowText(ParcelLoadString(STRID_MODEL_RESULT_CREATE_PROTOCOL));
	GetDlgItem(IDC_STATIC_11)->SetWindowText(ParcelLoadString(STRID_MODEL_RESULT_PARAMETER));
	GetDlgItem(IDC_STATIC_12)->SetWindowText(ParcelLoadString(STRID_MODEL_RESULT_SHOWAS));
	GetDlgItem(IDC_STATIC_13)->SetWindowText(ParcelLoadString(STRID_MODEL_RESULT_SHOWSAMPLES));
	GetDlgItem(IDC_STATIC_3)->SetWindowText(ParcelLoadString(STRID_MODEL_RESULT_OUTSDEFINE));
	GetDlgItem(IDC_STATIC_4)->SetWindowText(ParcelLoadString(STRID_MODEL_RESULT_CRIT));
	GetDlgItem(IDC_STATIC_6)->SetWindowText(ParcelLoadString(STRID_MODEL_RESULT_CORRS));
	GetDlgItem(IDC_EXCLUDE)->SetWindowText(ParcelLoadString(STRID_MODEL_RESULT_EXCLUDESPEC));
	GetDlgItem(IDC_BACK)->SetWindowText(ParcelLoadString(STRID_MODEL_RESULT_BACK));
	GetDlgItem(IDC_BACK_ALL)->SetWindowText(ParcelLoadString(STRID_MODEL_RESULT_BACK_ALL));
	GetDlgItem(IDC_STATIC_7)->SetWindowText(ParcelLoadString(STRID_MODEL_RESULT_CORR));
	GetDlgItem(IDC_STATIC_8)->SetWindowText(ParcelLoadString(STRID_MODEL_RESULT_CORRB));
	GetDlgItem(IDC_STATIC_9)->SetWindowText(ParcelLoadString(STRID_MODEL_RESULT_CORRA));
	GetDlgItem(IDC_RECALC)->SetWindowText(ParcelLoadString(STRID_MODEL_RESULT_RECALC));
	GetDlgItem(IDC_TREND)->SetWindowText(ParcelLoadString(STRID_MODEL_RESULT_FROMTREND));
	GetDlgItem(IDC_REFRESH)->SetWindowText(ParcelLoadString(STRID_MODEL_RESULT_REFRESH));
	GetDlgItem(IDC_STATIC_10)->SetWindowText(ParcelLoadString(STRID_MODEL_RESULT_MAXMAX));
	GetDlgItem(IDC_STATIC_14)->SetWindowText(ParcelLoadString(STRID_MODEL_RESULT_AXIS));
	GetDlgItem(IDC_STATIC_15)->SetWindowText(ParcelLoadString(STRID_MODEL_RESULT_AXE_OY));
	GetDlgItem(IDC_STATIC_16)->SetWindowText(ParcelLoadString(STRID_MODEL_RESULT_AXE_OX));
	GetDlgItem(IDC_SETTINGS)->SetWindowText(ParcelLoadString(STRID_MODEL_RESULT_SETTINGS));

	switch(Mode)
	{
	case C_MODRESDLG_MODE_EDIT:
		GetDlgItem(IDC_CANCEL)->SetWindowText(ParcelLoadString(STRID_MODEL_RESULT_DECLINE));
		GetDlgItem(IDC_SET_TEMPLATE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CREATE_PROTOCOL)->ShowWindow(SW_HIDE);
		break;
	case C_MODRESDLG_MODE_SHOW:
		GetDlgItem(IDC_CANCEL)->SetWindowText(ParcelLoadString(STRID_MODEL_RESULT_EXIT));
		GetDlgItem(IDC_ACCEPT)->ShowWindow(SW_HIDE);
	}

	bool bEdit = (Mode == C_MODRESDLG_MODE_EDIT);

	GetDlgItem(IDC_STATIC_7)->EnableWindow(bEdit);
	GetDlgItem(IDC_STATIC_8)->EnableWindow(bEdit);
	GetDlgItem(IDC_STATIC_9)->EnableWindow(bEdit);
	GetDlgItem(IDC_STATIC_6)->EnableWindow(bEdit);
	GetDlgItem(IDC_RECALC)->EnableWindow(bEdit);
	GetDlgItem(IDC_TREND)->EnableWindow(bEdit);
	GetDlgItem(IDC_CORR_CALIB_B)->EnableWindow(bEdit);
	GetDlgItem(IDC_CORR_CALIB_A)->EnableWindow(bEdit);
	GetDlgItem(IDC_EXCLUDE)->EnableWindow(bEdit);
//	GetDlgItem(IDC_BACK)->EnableWindow(bEdit);
//	GetDlgItem(IDC_BACK_ALL)->EnableWindow(bEdit);
	GetDlgItem(IDC_BACK)->EnableWindow(false);
	GetDlgItem(IDC_BACK_ALL)->EnableWindow(false);

	ShowPage(C_MODRES_PAGE_SEC);

	GetDlgItem(IDHELP)->EnableWindow(pFrame->IsExistHelp());

	return true;
}

void CModelResultDlg::OnAccept()
{
	if(Mode == C_MODRESDLG_MODE_EDIT)
		StartModel->Copy(*ModData);

	CDialog::OnOK();
}

void CModelResultDlg::OnCancel()
{
	OnClose();
}

void CModelResultDlg::OnClose()
{
	CDialog::OnCancel();
}

void CModelResultDlg::OnTabSelchange(NMHDR* /*pNMHDR*/, LRESULT* pResult)
{
	m_vTab = m_Tab.GetCurSel();

	ShowPage();

	*pResult = 0;
}

void CModelResultDlg::OnTableBigSelChange(NMHDR* /*pNMHDR*/, LRESULT *pResult)
{
	int Ind = -1, i, nsel = m_listBig.GetSelectedCount();
	if(m_vTab == C_MODRES_PAGE_SEV || (m_vTab == C_MODRES_PAGE_OUT && m_vCalibValid == C_MODRES_SMPL_VALID))
	{
		selV.clear();
		for(i=0; i<nsel; i++)
		{
			Ind = m_listBig.GetNextItem(Ind, LVNI_SELECTED);
			selV.push_back(ModData->ModelResult.GetValidNameByInd(Ind));
		}
	}
	else if(m_vTab != C_MODRES_PAGE_SPECLOAD && m_vTab != C_MODRES_PAGE_CHEMLOAD)
	{
		selC.clear();
		for(i=0; i<nsel; i++)
		{
			Ind = m_listBig.GetNextItem(Ind, LVNI_SELECTED);
			selC.push_back(ModData->ModelResult.GetCalibNameByInd(Ind));
		}
	}
	ColoredAllGraphs();

	*pResult = 0;
}

void CModelResultDlg::OnTableBigColumnClick(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NMLISTVIEW *pLV = (NMLISTVIEW *) pNMHDR;

	int item = pLV->iItem;

	bool isValid = GetCurCalibValid();

	if((m_vTab == C_MODRES_PAGE_SECV && !ModData->IsUseSECV()) ||
	   (isValid && ModData->ModelResult.GetNValidResult() <= 0))
	{
		*pResult = 0;
		return;
	}

	int prop = (!isValid) ? ModData->ModelResult.SortCalibProp : ModData->ModelResult.SortValidProp;
	bool dir = (!isValid) ? ModData->ModelResult.SortCalibDir : ModData->ModelResult.SortValidDir;
	int ind = m_vParam;
	int gk = 0;

	bool findsort = false;
	switch(m_vTab)
	{
	case C_MODRES_PAGE_SEC:
		if(item == 1)
		{
			if(prop == C_MODRES_SORT_CALIB_NAME)  dir = !dir;
			else  prop = C_MODRES_SORT_CALIB_NAME;
			findsort = true;
		}
		if(item == 2)
		{
			if(prop == C_MODRES_SORT_CALIB_REF)  dir = !dir;
			else  prop = C_MODRES_SORT_CALIB_REF;
			findsort = true;
		}
		if(item == 3)
		{
			if(prop == C_MODRES_SORT_CALIB_SEC_PRED)  dir = !dir;
			else  prop = C_MODRES_SORT_CALIB_SEC_PRED;
			findsort = true;
		}
		if(item == 4)
		{
			if(prop == C_MODRES_SORT_CALIB_SEC_DIFF)  dir = !dir;
			else  prop = C_MODRES_SORT_CALIB_SEC_DIFF;
			findsort = true;
		}
		if(item == 5)
		{
			if(prop == C_MODRES_SORT_CALIB_SEC_PROC)  dir = !dir;
			else  prop = C_MODRES_SORT_CALIB_SEC_PROC;
			findsort = true;
		}
		break;
	case C_MODRES_PAGE_SECV:
		if(item == 1)
		{
			if(prop == C_MODRES_SORT_CALIB_NAME)  dir = !dir;
			else  prop = C_MODRES_SORT_CALIB_NAME;
			findsort = true;
		}
		if(item == 2)
		{
			if(prop == C_MODRES_SORT_CALIB_REF)  dir = !dir;
			else  prop = C_MODRES_SORT_CALIB_REF;
			findsort = true;
		}
		if(item == 3)
		{
			if(prop == C_MODRES_SORT_CALIB_SECV_PRED)  dir = !dir;
			else  prop = C_MODRES_SORT_CALIB_SECV_PRED;
			findsort = true;
		}
		if(item == 4)
		{
			if(prop == C_MODRES_SORT_CALIB_SECV_DIFF)  dir = !dir;
			else  prop = C_MODRES_SORT_CALIB_SECV_DIFF;
			findsort = true;
		}
		if(item == 5)
		{
			if(prop == C_MODRES_SORT_CALIB_SECV_PROC)  dir = !dir;
			else  prop = C_MODRES_SORT_CALIB_SECV_PROC;
			findsort = true;
		}
		if(item == 6)
		{
			if(prop == C_MODRES_SORT_CALIB_SECV_SEC)  dir = !dir;
			else  prop = C_MODRES_SORT_CALIB_SECV_SEC;
			findsort = true;
		}
		break;
	case C_MODRES_PAGE_SEV:
		if(item == 1)
		{
			if(prop == C_MODRES_SORT_VALID_NAME)  dir = !dir;
			else  prop = C_MODRES_SORT_VALID_NAME;
			findsort = true;
		}
		if(item == 2)
		{
			if(prop == C_MODRES_SORT_VALID_REF)  dir = !dir;
			else  prop = C_MODRES_SORT_VALID_REF;
			findsort = true;
		}
		if(item == 3)
		{
			if(prop == C_MODRES_SORT_VALID_PRED)  dir = !dir;
			else  prop = C_MODRES_SORT_VALID_PRED;
			findsort = true;
		}
		if(item == 4)
		{
			if(prop == C_MODRES_SORT_VALID_DIFF)  dir = !dir;
			else  prop = C_MODRES_SORT_VALID_DIFF;
			findsort = true;
		}
		if(item == 5)
		{
			if(prop == C_MODRES_SORT_VALID_PROC)  dir = !dir;
			else  prop = C_MODRES_SORT_VALID_PROC;
			findsort = true;
		}
		if(item == 6)
		{
			if(prop == C_MODRES_SORT_VALID_DELTA)  dir = !dir;
			else  prop = C_MODRES_SORT_VALID_DELTA;
			findsort = true;
		}
		break;
	case C_MODRES_PAGE_OUT:
		if(isValid)
		{
			if(item == 1)
			{
				if(prop == C_MODRES_SORT_VALID_NAME)  dir = !dir;
				else  prop = C_MODRES_SORT_VALID_NAME;
				findsort = true;
			}
			if(item == 2)
			{
				if(prop == C_MODRES_SORT_VALID_MD)  dir = !dir;
				else  prop = C_MODRES_SORT_VALID_MD;
				findsort = true;
			}
			if(item == 3)
			{
				if(prop == C_MODRES_SORT_VALID_EXCEED)  dir = !dir;
				else  prop = C_MODRES_SORT_VALID_EXCEED;
				findsort = true;
			}
		}
		else
		{
			if(item == 1)
			{
				if(prop == C_MODRES_SORT_CALIB_NAME)  dir = !dir;
				else  prop = C_MODRES_SORT_CALIB_NAME;
				findsort = true;
			}
			if(item == 2)
			{
				if(prop == C_MODRES_SORT_CALIB_OUT_MD)  dir = !dir;
				else  prop = C_MODRES_SORT_CALIB_OUT_MD;
				findsort = true;
			}
			if(item == 3)
			{
				if(prop == C_MODRES_SORT_CALIB_OUT_EXCEED)  dir = !dir;
				else  prop = C_MODRES_SORT_CALIB_OUT_EXCEED;
				findsort = true;
			}
		}
		break;
	case C_MODRES_PAGE_BAD:
		if(item == 1)
		{
			if(prop == C_MODRES_SORT_CALIB_NAME)  dir = !dir;
			else  prop = C_MODRES_SORT_CALIB_NAME;
			findsort = true;
		}
		if(item == 2)
		{
			if(prop == C_MODRES_SORT_CALIB_REF)  dir = !dir;
			else  prop = C_MODRES_SORT_CALIB_REF;
			findsort = true;
		}
		if(item == 3)
		{
			if(prop == C_MODRES_SORT_CALIB_BAD_PRED)  dir = !dir;
			else  prop = C_MODRES_SORT_CALIB_BAD_PRED;
			findsort = true;
		}
		if(item == 4)
		{
			if(prop == C_MODRES_SORT_CALIB_BAD_DIFF)  dir = !dir;
			else  prop = C_MODRES_SORT_CALIB_BAD_DIFF;
			findsort = true;
		}
		if(item == 5)
		{
			if(prop == C_MODRES_SORT_CALIB_BAD_PROC)  dir = !dir;
			else  prop = C_MODRES_SORT_CALIB_BAD_PROC;
			findsort = true;
		}
		if(item == 6)
		{
			if(prop == C_MODRES_SORT_CALIB_BAD_REST)  dir = !dir;
			else  prop = C_MODRES_SORT_CALIB_BAD_REST;
			findsort = true;
		}
		break;
	case C_MODRES_PAGE_SCORES:
		if(item == 1)
		{
			if(prop == C_MODRES_SORT_CALIB_NAME)  dir = !dir;
			else  prop = C_MODRES_SORT_CALIB_NAME;
			findsort = true;
		}
		if(item > 1)
		{
			gk = item - 2;
			if(prop == C_MODRES_SORT_CALIB_GK_0 + gk)  dir = !dir;
			prop = C_MODRES_SORT_CALIB_GK_0 + gk;
			findsort = true;
		}
		break;
	}

	if(findsort)
	{
		if(isValid)
			ModData->ModelResult.SortValid(prop, dir, ind, gk);
		else
			ModData->ModelResult.SortCalib(prop, dir, ind, gk);

		FillTableBig();
	}

	*pResult = 0;
}

void CModelResultDlg::OnChangeParam()
{
	m_vParam = m_Param.GetCurSel();

	m_vCorrB = ModData->GetModelParam(m_vParam)->CorrB;
	m_vCorrA = ModData->GetModelParam(m_vParam)->CorrA;

	UpdateData(0);

	SortAll();

	CreateGraphSEC();
	CreateGraphSECV();
	CreateGraphSEV();
	CreateHistoBad();

	ShowPage();
}

void CModelResultDlg::OnChangeView()
{
	m_vShow = m_Show.GetCurSel();

	ShowPage();
}

void CModelResultDlg::OnChangeCalibValid()
{
	m_vCalibValid = m_CalibValid.GetCurSel();

	ShowPage();
}

void CModelResultDlg::OnChangeAxeGKsc()
{
	m_vOXsc = m_OXsc.GetCurSel();
	m_vOYsc = m_OYsc.GetCurSel();

	UpdateData(0);

	CreateGraphScores();

	ShowPage();
}

void CModelResultDlg::OnChangeAxeGKch()
{
	m_vOXch = m_OXch.GetCurSel();
	m_vOYch = m_OYch.GetCurSel();

	UpdateData(0);

	CreateGraphChemLoad();

	ShowPage();
}

void CModelResultDlg::OnSettings()
{
	CChooseGKDlg dlg(&ModData->ModelResult, this);
	if(dlg.DoModal() != IDOK)
		return;

	CreateLegendSpec();
	pChart->GetUserLegend(posLegendSpec)->Enable(GetNGKShow() > 0 && show_legend);

	ShowPage();
}

void CModelResultDlg::OnSetTemplate()
{
	CSetTemplateDlg dlg(C_TEMPLATE_QNT);
	dlg.DoModal();
}

void CModelResultDlg::OnCreateProtocol()
{
	CParcelReportQnt theReport(pModRes, m_vParam);

	CString TmplPath = MakePath(pRoot->GetTemplateFolder(C_TEMPLATE_QNT), pRoot->GetTemplateName(C_TEMPLATE_QNT));
	CString HtmlPath = pRoot->GetReportPath(C_TEMPLATE_QNT);
	CString PicPath = pRoot->GetReportPictureFolder(C_TEMPLATE_QNT);

	ICreatorApi* pCreator = REPORTGEN_QueryCreatorApi();
	if(NULL != pCreator)
	{
		DeleteAllContentFromDirectory(PicPath);
		if(!pCreator->CreateReport(TmplPath, HtmlPath, static_cast<IAppCallback*>(&theReport)))
		{
			CString strMsg = pCreator->GetErrorInfo(pCreator->GetLastError());
			AfxMessageBox(strMsg);
		}
		REPORTGEN_ReleaseCreatorApi();
	}

 	registry::RegKey key(true);
	if(key.Open(_T("SOFTWARE\\Lumex\\LumReportViewer"), REGROOT))
	{
		key[_T("ReportPath")] = pRoot->GetReportPath(C_TEMPLATE_QNT);
		key[_T("AlwaysTop")] = _T("0");
		key[_T("UiLanguage")] = pFrame->GetLanguage();

		key.Close();

		CString strViewer = MakePath(pRoot->GetWorkFolder(), _T("LumReportViewer.exe"));

		STARTUPINFO si;
		PROCESS_INFORMATION pi;

		ZeroMemory(&si, sizeof(STARTUPINFO));
		ZeroMemory(&pi, sizeof(PROCESS_INFORMATION));

		si.cb = sizeof(STARTUPINFO);
		si.dwFlags = STARTF_USESHOWWINDOW;
		si.wShowWindow = SW_SHOWNORMAL;

		LPTSTR lpszViewer = strViewer.GetBuffer(0);
		::CreateProcessW(NULL, lpszViewer, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi);
		::WaitForInputIdle(GetCurrentProcess(), INFINITE);

		if(pi.hProcess)
		{
			DWORD dwExitCode = STILL_ACTIVE;
			while(dwExitCode == STILL_ACTIVE)
			{
				::WaitForSingleObject(pi.hProcess, 1000);
				::GetExitCodeProcess(pi.hProcess, &dwExitCode);
			}           
		}
	}
}

void CModelResultDlg::OnSECChanged()
{
	double old = m_vSEC;

	if(!UpdateData())
	{
		GetDlgItem(IDC_CRIT)->SetFocus();
		HWND hWndLastControl;
		GetDlgItem(IDC_CRIT, &hWndLastControl);
		::SendMessage(hWndLastControl, EM_SETSEL, 0, -1);
		return;
	}

	if(fabs(old - m_vSEC) > 1.e-9)
	{
		FillTableBig();
		ColoredGraphSEC();
	}
}

void CModelResultDlg::OnSECVChanged()
{
	double old = m_vSECV;

	if(!UpdateData())
	{
		GetDlgItem(IDC_CRIT2)->SetFocus();
		HWND hWndLastControl;
		GetDlgItem(IDC_CRIT2, &hWndLastControl);
		::SendMessage(hWndLastControl, EM_SETSEL, 0, -1);
		return;
	}

	if(fabs(old - m_vSECV) > 1.e-9)
	{
		FillTableBig();
		ColoredGraphSECV();
	}
}

void CModelResultDlg::OnSEVChanged()
{
	double old = m_vSEV;

	if(!UpdateData())
	{
		GetDlgItem(IDC_CRIT3)->SetFocus();
		HWND hWndLastControl;
		GetDlgItem(IDC_CRIT3, &hWndLastControl);
		::SendMessage(hWndLastControl, EM_SETSEL, 0, -1);
		return;
	}

	if(fabs(old - m_vSEV) > 1.e-9)
	{
		FillTableBig();
		ColoredGraphSEV();
	}
}

void CModelResultDlg::OnMaxMahChanged()
{
	double old = m_vMaxMah;

	if(!UpdateData())
	{
		GetDlgItem(IDC_MAXMAH)->SetFocus();
		HWND hWndLastControl;
		GetDlgItem(IDC_MAXMAH, &hWndLastControl);
		::SendMessage(hWndLastControl, EM_SETSEL, 0, -1);
		return;
	}

	if(fabs(old - m_vMaxMah) > 1.e-9)
	{
		ModData->SetMaxMah(m_vMaxMah);
		ColoredHistoCalib();
		ColoredHistoValid();
	}
}

void CModelResultDlg::OnExclude()
{
	if(m_vTab == C_MODRES_PAGE_CHEMLOAD)
		return;

	SInt PointsCopy;
	ModData->GetExcPoints(PointsCopy);

	VModSam CalibCopy, ValidCopy;
	ModData->GetCalibValid(CalibCopy, ValidCopy);

	CModelData NewTmpMod(ModData->GetParentHItem());
	NewTmpMod.Copy(*ModData);

	bool res = false;
	switch(m_vTab)
	{
	case C_MODRES_PAGE_SEC:
	case C_MODRES_PAGE_SECV:
	case C_MODRES_PAGE_SEV:
	case C_MODRES_PAGE_OUT:
	case C_MODRES_PAGE_BAD:
	case C_MODRES_PAGE_SCORES:
		res = ExcludeSpectra();
		break;
	case C_MODRES_PAGE_SPECLOAD:
    	res = ExcludePoints();
		break;
	}

	if(res)
	{
		if(ModData->CalculateModel())
		{
			ModTmp.push_back(NewTmpMod);
			GetDlgItem(IDC_BACK)->EnableWindow(true);
			GetDlgItem(IDC_BACK_ALL)->EnableWindow(true);

			if(m_vTab != C_MODRES_PAGE_SPECLOAD)
				ModData->CalcModParamLimits();
			SortAll();
			CreateAllGraphs();
			ShowPage();
		}
		else
		{
			if(m_vTab == C_MODRES_PAGE_SPECLOAD)
				ModData->SetExcPoints(PointsCopy);
			else
				ModData->SetCalibValid(CalibCopy, ValidCopy);
		}
	}
}

void CModelResultDlg::OnBack()
{
	if(ModTmp.size() <= 0)
		return;

	ModData->Copy(ModTmp.back());
	ModTmp.pop_back();
	GetDlgItem(IDC_BACK)->EnableWindow(ModTmp.size() > 0);
	GetDlgItem(IDC_BACK_ALL)->EnableWindow(ModTmp.size() > 0);

	ModelParam* pMP = ModData->GetModelParam(m_vParam);
	m_vCorrB = pMP->CorrB;
	m_vCorrA = pMP->CorrA;
	m_vMaxMah = ModData->GetMaxMah();
	m_vSEC = m_vSECV = m_vSEV = 2.;

	UpdateData(0);

	if(ModData->CalculateModel())
	{
		SortAll();
		CreateAllGraphs();
		ShowPage();
	}
}

void CModelResultDlg::OnBackAll()
{
	ModData->Copy(*StartModel);
	ModTmp.clear();
	GetDlgItem(IDC_BACK)->EnableWindow(false);
	GetDlgItem(IDC_BACK_ALL)->EnableWindow(false);

	ModelParam* pMP = ModData->GetModelParam(m_vParam);
	m_vCorrB = pMP->CorrB;
	m_vCorrA = pMP->CorrA;
	m_vMaxMah = ModData->GetMaxMah();
	m_vSEC = m_vSECV = m_vSEV = 2.;

	UpdateData(0);

	if(ModData->CalculateModel())
	{
		SortAll();
		CreateAllGraphs();
		ShowPage();
	}
}

void CModelResultDlg::OnRecalc()
{
	if(!UpdateData())
		return;

	CModelData NewTmpMod(ModData->GetParentHItem());
	NewTmpMod.Copy(*ModData);

	ModelParam* pMP = ModData->GetModelParam(m_vParam);
	pMP->CorrB = m_vCorrB;
	pMP->CorrA = m_vCorrA;

	if(ModData->CalculateModel())
	{
		ModTmp.push_back(NewTmpMod);
		GetDlgItem(IDC_BACK)->EnableWindow(true);
		GetDlgItem(IDC_BACK_ALL)->EnableWindow(true);

		SortAll();
		CreateGraphSEV();
		ShowPage();
	}
}

void CModelResultDlg::OnFromTrend()
{
	ParamSumResult *pPSR = ModData->ModelResult.GetPSResult(m_vParam);
	ModelParam* pMP = ModData->GetModelParam(m_vParam);

	if(fabs(pPSR->tSEVb) > 1.e-9)
	{
		m_vCorrA = (pMP->CorrA - pPSR->tSEVa) / pPSR->tSEVb;
		m_vCorrB = pMP->CorrB / pPSR->tSEVb;
	}

	UpdateData(0);
}

void CModelResultDlg::OnRefresh()
{
	switch(m_vTab)
	{
	case C_MODRES_PAGE_SEC:  OnSECChanged();  break;
	case C_MODRES_PAGE_SECV:  OnSECVChanged();  break;
	case C_MODRES_PAGE_SEV:  OnSEVChanged();  break;
	case C_MODRES_PAGE_OUT:  OnMaxMahChanged();  break;
	}

	ShowPage();
}

void CModelResultDlg::OnUserMenuPrev()
{
	pChart->GetWnd()->PostMessage(WM_COMMAND, LC_POPUP_GO_BACK, 0);
}

void CModelResultDlg::OnUserMenuNext()
{
	pChart->GetWnd()->PostMessage(WM_COMMAND, LC_POPUP_GO_FORWARD, 0);
}

void CModelResultDlg::OnUserMenuFull()
{
	pChart->GetWnd()->PostMessage(WM_COMMAND, LC_POPUP_FULL_SCREEN, 0);
}

void CModelResultDlg::OnUserMenuAuto()
{
	pChart->GetWnd()->PostMessage(WM_COMMAND, LC_POPUP_AUTOY, 0);
}

void CModelResultDlg::OnUserMenuLeft()
{
	pChart->GetWnd()->PostMessage(WM_COMMAND, LC_POPUP_XLEFT, 0);
}

void CModelResultDlg::OnUserMenuRight()
{
	pChart->GetWnd()->PostMessage(WM_COMMAND, LC_POPUP_XRIGHT, 0);
}

void CModelResultDlg::OnUserMenuLegend()
{
	show_legend = !show_legend;
	ShowPage();
}

void CModelResultDlg::OnUserMenuGrid()
{
	bool btmp, btmp2;
	pChart->IsGridEnabled(&btmp, &btmp2);
	pChart->ShowGrid(!btmp, !btmp);
}

void CModelResultDlg::OnUserMenuTrend()
{
	CLumChartDataSet* pTrend = NULL;
	switch(m_vTab)
	{
	case C_MODRES_PAGE_SEC:  pTrend = pGraphLineSEC;  break;
	case C_MODRES_PAGE_SECV:  pTrend = pGraphLineSECV;  break;
	case C_MODRES_PAGE_SEV:  pTrend = pGraphLineSEV;  break;
	}

	if(pTrend == NULL)
		return;

	bool btmp;
	pChart->IsGraphEnabled(pTrend->GetPos(), &btmp);
	pChart->ShowGraph(pTrend->GetPos(), !btmp);
	show_trend = !btmp;

	CreateLegends();
	pChart->GetUserLegend(posLegendSEC_V)->Enable(IsLegendEnable() && show_legend);

	pChart->RedrawWindow();
}

void CModelResultDlg::OnUserMenuTeor()
{
	if(pGraphLine0 == NULL)
		return;

	bool btmp;
	pChart->IsGraphEnabled(pGraphLine0->GetPos(), &btmp);
	pChart->ShowGraph(pGraphLine0->GetPos(), !btmp);
	show_teor = !btmp;

	CreateLegends();
	pChart->GetUserLegend(posLegendSEC_V)->Enable(IsLegendEnable() && show_legend);

	pChart->RedrawWindow();
}

void CModelResultDlg::OnUserMenuMDL()
{
	if(pHistoMDL == NULL)
		return;

	bool btmp;
	pChart->IsGraphEnabled(pHistoMDL->GetPos(), &btmp);
	pChart->ShowGraph(pHistoMDL->GetPos(), !btmp);
	show_mdl = !btmp;

	CreateLegends();
	pChart->GetUserLegend(posLegendOut)->Enable(show_mdl && show_legend);

	pChart->RedrawWindow();
}

void CModelResultDlg::OnUserMenuLimit()
{
	if(pHistoNorm == NULL)
		return;

	bool btmp;
	pChart->IsGraphEnabled(pHistoNorm->GetPos(), &btmp);
	pChart->ShowGraph(pHistoNorm->GetPos(), !btmp);
	show_limit = !btmp;

	CreateLegends();
	pChart->GetUserLegend(posLegendBad)->Enable(show_limit && show_legend);

	pChart->RedrawWindow();
}

int CModelResultDlg::GetCurTab()
{
	return m_vTab;
}

bool CModelResultDlg::GetCurCalibValid()
{
	return (m_vTab == C_MODRES_PAGE_SEV || (m_vTab == C_MODRES_PAGE_OUT && m_vCalibValid == C_MODRES_SMPL_VALID));
}

double CModelResultDlg::GetMaxMah()
{
	return ModData->GetMaxMah();
}

double CModelResultDlg::GetNorm()
{
	ParamSumResult *pPSR = ModData->ModelResult.GetPSResult(m_vParam);

	return pPSR->norm;
}

bool CModelResultDlg::IsResultBad(int item)
{
	bool isValid = GetCurCalibValid();

	ParamSumResult *pPSR = ModData->ModelResult.GetPSResult(m_vParam);
	SpectrumResult* pSpR = (!isValid) ? ModData->ModelResult.GetCalibResult(item) : NULL;
	SpectrumValResult* pSpVR = (isValid) ? ModData->ModelResult.GetValidResult(item) : NULL;
	SpectrumParamResult* pSpPR = (!isValid) ? &pSpR->SpecParamData[m_vParam] : NULL;
	SpectrumValParamResult* pSpVPR = (isValid) ? &pSpVR->SpecValParamData[m_vParam] : NULL;

	switch(m_vTab)
	{
	case C_MODRES_PAGE_SEC:
		if(fabs(pSpPR->SEC_RefPred) > pPSR->SEC * m_vSEC)
			return true;
		break;
	case C_MODRES_PAGE_SECV:
		if(fabs(pSpPR->SECV_RefPred) > pPSR->SECV * m_vSECV)
			return true;
		break;
	case C_MODRES_PAGE_SEV:
		if(fabs(pSpVPR->RefPred) > pPSR->SEV * m_vSEV)
			return true;
		break;
	case C_MODRES_PAGE_OUT:
		if(!isValid)
		{
			if(pSpR->Out_Mah > m_vMaxMah)
				return true;
		}
		else
		{
			if(pSpVR->Out_Mah > m_vMaxMah)
				return true;
		}
		break;
	case C_MODRES_PAGE_BAD:
		if(pSpPR->Bad_Remainder > pPSR->norm)
			return true;
		break;
	}

	return false;
}

void CModelResultDlg::ShowPage(int page/* = -1*/)
{
	if(page >= C_MODRES_PAGE_SEC && page <= C_MODRES_PAGE_CHEMLOAD)
		m_vTab = page;

	int graph;
	switch(m_vTab)
	{
	case C_MODRES_PAGE_SEC:  graph = C_PGRAPH_SEC;  break;
	case C_MODRES_PAGE_SECV:  graph = C_PGRAPH_SECV;  break;
	case C_MODRES_PAGE_SEV:  graph = C_PGRAPH_SEV;  break;
	case C_MODRES_PAGE_OUT:
		if(m_vCalibValid == C_MODRES_SMPL_CALIB)  graph = C_PGRAPH_MDCALIB;
		else  graph = C_PGRAPH_MDVALID;
		break;
	case C_MODRES_PAGE_BAD:  graph = C_PGRAPH_BAD;  break;
	case C_MODRES_PAGE_SCORES:  graph = C_PGRAPH_SCORES;  break;
	case C_MODRES_PAGE_SPECLOAD:  graph = C_PGRAPH_SPLOAD;  break;
	case C_MODRES_PAGE_CHEMLOAD:  graph = C_PGRAPH_CHLOAD;  break;
	}

	bool sec = (m_vTab == C_MODRES_PAGE_SEC);
	bool secv = (m_vTab == C_MODRES_PAGE_SECV);
	bool sev = (m_vTab == C_MODRES_PAGE_SEV);
	bool out = (m_vTab == C_MODRES_PAGE_OUT);
	bool bad = (m_vTab == C_MODRES_PAGE_BAD);
	bool scores = (m_vTab == C_MODRES_PAGE_SCORES);
	bool spload = (m_vTab == C_MODRES_PAGE_SPECLOAD);
	bool chload = (m_vTab == C_MODRES_PAGE_CHEMLOAD);

	int show_sec = (sec) ? SW_SHOW : SW_HIDE;
	int show_secv = (secv) ? SW_SHOW : SW_HIDE;
	int show_sev = (sev) ? SW_SHOW : SW_HIDE;
	int show_small = (sec || secv || sev) ? SW_SHOW : SW_HIDE;
	int show_corr = (sev) ? SW_SHOW : SW_HIDE;
	int show_out = (sec || secv || sev || out) ? SW_SHOW : SW_HIDE;
	int show_crit = (sec || secv || sev) ? SW_SHOW : SW_HIDE;
	int show_mah = (out) ? SW_SHOW : SW_HIDE;
	int show_smpl = (out) ? SW_SHOW : SW_HIDE;
	int show_par = (sec || secv || sev || bad) ? SW_SHOW : SW_HIDE;
	int show_edit = (!chload) ? SW_SHOW : SW_HIDE;
	int show_table = (m_vShow == C_MODRES_SHOW_TABLE) ? SW_SHOW : SW_HIDE;
	int show_axes = (scores || chload) ? SW_SHOW : SW_HIDE;
	int show_axe1 = (scores) ? SW_SHOW : SW_HIDE;
	int show_axe2 = (chload) ? SW_SHOW : SW_HIDE;
	int show_gk = (spload) ? SW_SHOW : SW_HIDE;

	RebuildTableBig();
	RebuildTableSmall();

	if(sec)  GetDlgItem(IDC_STATIC_5)->SetWindowText(ParcelLoadString(STRID_MODEL_RESULT_CRITSEC));
	if(secv)  GetDlgItem(IDC_STATIC_5)->SetWindowText(ParcelLoadString(STRID_MODEL_RESULT_CRITSECV));
	if(sev)  GetDlgItem(IDC_STATIC_5)->SetWindowText(ParcelLoadString(STRID_MODEL_RESULT_CRITSEV));

	GetDlgItem(IDC_MODEL_RESULT_TABLEBIG)->ShowWindow(show_table);

	pChart->GetWnd()->ShowWindow(!show_table);

	GetDlgItem(IDC_MODEL_RESULT_TABLESMALL)->ShowWindow(show_small);
	GetDlgItem(IDC_CORR_CALIB_B)->ShowWindow(show_corr);
	GetDlgItem(IDC_CORR_CALIB_A)->ShowWindow(show_corr);
	GetDlgItem(IDC_RECALC)->ShowWindow(show_corr);
	GetDlgItem(IDC_TREND)->ShowWindow(show_corr);
	GetDlgItem(IDC_STATIC_7)->ShowWindow(show_corr);
	GetDlgItem(IDC_STATIC_8)->ShowWindow(show_corr);
	GetDlgItem(IDC_STATIC_9)->ShowWindow(show_corr);
	GetDlgItem(IDC_STATIC_3)->ShowWindow(show_out);
	GetDlgItem(IDC_REFRESH)->ShowWindow(show_out);
	GetDlgItem(IDC_MAXMAH)->ShowWindow(show_mah);
	GetDlgItem(IDC_STATIC_10)->ShowWindow(show_mah);
	GetDlgItem(IDC_CRIT)->ShowWindow(show_sec);
	GetDlgItem(IDC_CRIT2)->ShowWindow(show_secv);
	GetDlgItem(IDC_CRIT3)->ShowWindow(show_sev);
	GetDlgItem(IDC_STATIC_4)->ShowWindow(show_crit);
	GetDlgItem(IDC_STATIC_5)->ShowWindow(show_crit);
	GetDlgItem(IDC_CALIBVALID)->ShowWindow(show_smpl);
	GetDlgItem(IDC_STATIC_13)->ShowWindow(show_smpl);
	GetDlgItem(IDC_STATIC_11)->ShowWindow(show_par);
	GetDlgItem(IDC_PARAM)->ShowWindow(show_par);
	GetDlgItem(IDC_STATIC_6)->ShowWindow(show_sev);
	GetDlgItem(IDC_EXCLUDE)->ShowWindow(show_edit);
	GetDlgItem(IDC_BACK)->ShowWindow(show_edit);
	GetDlgItem(IDC_BACK_ALL)->ShowWindow(show_edit);
	GetDlgItem(IDC_SETTINGS)->ShowWindow(show_gk);
	GetDlgItem(IDC_AXE_OX)->ShowWindow(show_axe1);
	GetDlgItem(IDC_AXE_OY)->ShowWindow(show_axe1);
	GetDlgItem(IDC_AXE_OX_2)->ShowWindow(show_axe2);
	GetDlgItem(IDC_AXE_OY_2)->ShowWindow(show_axe2);
	GetDlgItem(IDC_STATIC_14)->ShowWindow(show_axes);
	GetDlgItem(IDC_STATIC_15)->ShowWindow(show_axes);
	GetDlgItem(IDC_STATIC_16)->ShowWindow(show_axes);

	CString s;

	s = (spload) ? ParcelLoadString(STRID_MODEL_RESULT_EXCLUDEPNT) : ParcelLoadString(STRID_MODEL_RESULT_EXCLUDESPEC);
	GetDlgItem(IDC_EXCLUDE)->SetWindowText(s);

	m_Show.ResetContent();
	s = ParcelLoadString(STRID_MODEL_RESULT_SHOWASTAB);
	m_Show.AddString(s);
	if(out || bad)
		s = ParcelLoadString(STRID_MODEL_RESULT_SHOWASHIST);
	else
		s = ParcelLoadString(STRID_MODEL_RESULT_SHOWASPLOT);
	m_Show.AddString(s);
	m_Show.SetCurSel(m_vShow);

	if(!show_table)
		SelShowGraphs(graph);

	RedrawWindow();
}

void CModelResultDlg::RebuildTableBig()
{
	m_listBig.DeleteAllItems();

	CString s;
	int i;
	int nColumnCount = m_listBig.GetHeaderCtrl()->GetItemCount();
	for(i=1; i<nColumnCount; i++)
		m_listBig.DeleteColumn(1);

	if(m_vTab == C_MODRES_PAGE_SEC)
	{
		s = ParcelLoadString(STRID_MODEL_RESULT_SEC_COL_SPEC);
		m_listBig.InsertColumn(1, s, LVCFMT_LEFT, 0, 1);
		s = ParcelLoadString(STRID_MODEL_RESULT_SEC_COL_REF);
		m_listBig.InsertColumn(2, s, LVCFMT_RIGHT, 0, 2);
		s = ParcelLoadString(STRID_MODEL_RESULT_SEC_COL_PRED);
		m_listBig.InsertColumn(3, s, LVCFMT_RIGHT, 0, 3);
		s = ParcelLoadString(STRID_MODEL_RESULT_SEC_COL_DIFF);
		m_listBig.InsertColumn(4, s, LVCFMT_RIGHT, 0, 4);
		s = ParcelLoadString(STRID_MODEL_RESULT_SEC_COL_PROC);
		m_listBig.InsertColumn(5, s, LVCFMT_RIGHT, 0, 5);
	}
	if(m_vTab == C_MODRES_PAGE_SECV)
	{
		s = ParcelLoadString(STRID_MODEL_RESULT_SECV_COL_SPEC);
		m_listBig.InsertColumn(1, s, LVCFMT_LEFT, 0, 1);
		s = ParcelLoadString(STRID_MODEL_RESULT_SECV_COL_REF);
		m_listBig.InsertColumn(2, s, LVCFMT_RIGHT, 0, 2);
		s = ParcelLoadString(STRID_MODEL_RESULT_SECV_COL_PRED);
		m_listBig.InsertColumn(3, s, LVCFMT_RIGHT, 0, 3);
		s = ParcelLoadString(STRID_MODEL_RESULT_SECV_COL_DIFF);
		m_listBig.InsertColumn(4, s, LVCFMT_RIGHT, 0, 4);
		s = ParcelLoadString(STRID_MODEL_RESULT_SECV_COL_PROC);
		m_listBig.InsertColumn(5, s, LVCFMT_RIGHT, 0, 5);
		s = ParcelLoadString(STRID_MODEL_RESULT_SECV_COL_SEC);
		m_listBig.InsertColumn(6, s, LVCFMT_RIGHT, 0, 6);
	}
	if(m_vTab == C_MODRES_PAGE_SEV)
	{
		s = ParcelLoadString(STRID_MODEL_RESULT_SEV_COL_SPEC);
		m_listBig.InsertColumn(1, s, LVCFMT_LEFT, 0, 1);
		s = ParcelLoadString(STRID_MODEL_RESULT_SEV_COL_REF);
		m_listBig.InsertColumn(2, s, LVCFMT_RIGHT, 0, 2);
		s = ParcelLoadString(STRID_MODEL_RESULT_SEV_COL_PRED);
		m_listBig.InsertColumn(3, s, LVCFMT_RIGHT, 0, 3);
		s = ParcelLoadString(STRID_MODEL_RESULT_SEV_COL_DIFF);
		m_listBig.InsertColumn(4, s, LVCFMT_RIGHT, 0, 4);
		s = ParcelLoadString(STRID_MODEL_RESULT_SEV_COL_PROC);
		m_listBig.InsertColumn(5, s, LVCFMT_RIGHT, 0, 5);
		s = ParcelLoadString(STRID_MODEL_RESULT_SEV_COL_DELTA);
		m_listBig.InsertColumn(6, s, LVCFMT_RIGHT, 0, 6);
	}
	if(m_vTab == C_MODRES_PAGE_OUT)
	{
		s = ParcelLoadString(STRID_MODEL_RESULT_OUT_COL_SPEC);
		m_listBig.InsertColumn(1, s, LVCFMT_LEFT, 0, 1);
		s = ParcelLoadString(STRID_MODEL_RESULT_OUT_COL_MAH);
		m_listBig.InsertColumn(2, s, LVCFMT_RIGHT, 0, 2);
		s = ParcelLoadString(STRID_MODEL_RESULT_OUT_COL_EXCEED);
		m_listBig.InsertColumn(3, s, LVCFMT_RIGHT, 0, 3);
	}
	if(m_vTab == C_MODRES_PAGE_BAD)
	{
		s = ParcelLoadString(STRID_MODEL_RESULT_BAD_COL_SPEC);
		m_listBig.InsertColumn(1, s, LVCFMT_LEFT, 0, 1);
		s = ParcelLoadString(STRID_MODEL_RESULT_BAD_COL_REF);
		m_listBig.InsertColumn(2, s, LVCFMT_RIGHT, 0, 2);
		s = ParcelLoadString(STRID_MODEL_RESULT_BAD_COL_PRED);
		m_listBig.InsertColumn(3, s, LVCFMT_RIGHT, 0, 3);
		s = ParcelLoadString(STRID_MODEL_RESULT_BAD_COL_DIFF);
		m_listBig.InsertColumn(4, s, LVCFMT_RIGHT, 0, 4);
		s = ParcelLoadString(STRID_MODEL_RESULT_BAD_COL_PROC);
		m_listBig.InsertColumn(5, s, LVCFMT_RIGHT, 0, 5);
		s = ParcelLoadString(STRID_MODEL_RESULT_BAD_COL_REST);
		m_listBig.InsertColumn(6, s, LVCFMT_RIGHT, 0, 6);
	}
	if(m_vTab == C_MODRES_PAGE_SCORES)
	{
		s = ParcelLoadString(STRID_MODEL_RESULT_SCORES_COL_SPEC);
		m_listBig.InsertColumn(1, s, LVCFMT_LEFT, 0, 1);

		CString fmt = ParcelLoadString(STRID_MODEL_RESULT_SCORES_COL_GK);
		int NGK = ModData->GetNCompQnt();
		for(int m=0; m<NGK; m++)
		{
			s.Format(fmt, m + 1);
			m_listBig.InsertColumn(m + 2, s, LVCFMT_RIGHT, 0, 2);
		}
	}
	if(m_vTab == C_MODRES_PAGE_SPECLOAD)
	{
		s = ParcelLoadString(STRID_MODEL_RESULT_SPECLOAD_COL_WAVENUM);
		m_listBig.InsertColumn(1, s, LVCFMT_LEFT, 0, 1);

		CString fmt = ParcelLoadString(STRID_MODEL_RESULT_SPECLOAD_COL_GK);
		int NGK = ModData->GetNCompQnt();
		for(int m=0; m<NGK; m++)
		{
			s.Format(fmt, m + 1);
			m_listBig.InsertColumn(m + 2, s, LVCFMT_RIGHT, 0, 2);
		}
	}
	if(m_vTab == C_MODRES_PAGE_CHEMLOAD)
	{
		s = ParcelLoadString(STRID_MODEL_RESULT_CHEMLOAD_COL_PARAM);
		m_listBig.InsertColumn(1, s, LVCFMT_LEFT, 0, 1);

		CString fmt = ParcelLoadString(STRID_MODEL_RESULT_CHEMLOAD_COL_GK);
		int NGK = ModData->GetNCompQnt();
		for(int m=0; m<NGK; m++)
		{
			s.Format(fmt, m + 1);
			m_listBig.InsertColumn(m + 2, s, LVCFMT_RIGHT, 0, 2);
		}
	}

	FillTableBig();
}

void CModelResultDlg::RebuildTableSmall()
{
	m_listSmall.DeleteAllItems();

	CString s;
	int i;
	int nColumnCount = m_listSmall.GetHeaderCtrl()->GetItemCount();
	for(i=1; i<nColumnCount; i++)
		m_listSmall.DeleteColumn(1);

	if(m_vTab == C_MODRES_PAGE_SEC)
	{
		s = cGetCritName(C_CRIT_SEC);
		m_listSmall.InsertColumn(1, s, LVCFMT_RIGHT, 0, 1);
		s = cGetCritName(C_CRIT_R2SEC);
		m_listSmall.InsertColumn(2, s, LVCFMT_RIGHT, 0, 2);
		s = ParcelLoadString(STRID_MODEL_RESULT_COLTREND);
		m_listSmall.InsertColumn(3, s, LVCFMT_RIGHT, 0, 3);
	}
	if(m_vTab == C_MODRES_PAGE_SECV)
	{
		s = cGetCritName(C_CRIT_SECV);
		m_listSmall.InsertColumn(1, s, LVCFMT_RIGHT, 0, 1);
		s = cGetCritName(C_CRIT_R2SECV);
		m_listSmall.InsertColumn(2, s, LVCFMT_RIGHT, 0, 2);
		s = cGetCritName(C_CRIT_F);
		m_listSmall.InsertColumn(3, s, LVCFMT_RIGHT, 0, 3);
		s = ParcelLoadString(STRID_MODEL_RESULT_COLTREND);
		m_listSmall.InsertColumn(4, s, LVCFMT_RIGHT, 0, 4);
	}
	if(m_vTab == C_MODRES_PAGE_SEV)
	{
		s = cGetCritName(C_CRIT_SEV);
		m_listSmall.InsertColumn(1, s, LVCFMT_RIGHT, 0, 1);
		s = cGetCritName(C_CRIT_SDV);
		m_listSmall.InsertColumn(2, s, LVCFMT_RIGHT, 0, 2);
		s = cGetCritName(C_CRIT_R2SEV);
		m_listSmall.InsertColumn(3, s, LVCFMT_RIGHT, 0, 3);
		s = cGetCritName(C_CRIT_ERR);
		m_listSmall.InsertColumn(4, s, LVCFMT_RIGHT, 0, 4);
		s = ParcelLoadString(STRID_MODEL_RESULT_SEV_COL_T);
		m_listSmall.InsertColumn(5, s, LVCFMT_RIGHT, 0, 5);
		s = ParcelLoadString(STRID_MODEL_RESULT_COLTREND);
		m_listSmall.InsertColumn(6, s, LVCFMT_RIGHT, 0, 6);
	}

	FillTableSmall();
}

void CModelResultDlg::FillTableBig()
{
	m_listBig.DeleteAllItems();

	int i;
	CString s, s1;
	if(m_vTab == C_MODRES_PAGE_SECV && !ModData->IsUseSECV())
	{
		if(!IsAvr)
			s.Format(ParcelLoadString(STRID_MODEL_RESULT_ALLNUM), 0);
		else
			s.Format(ParcelLoadString(STRID_MODEL_RESULT_ALLNUM_AVR), 0);
		GetDlgItem(IDC_STATIC_1)->SetWindowText(s);

		SetSortIcons();
		for(i=0; i<m_listBig.GetHeaderCtrl()->GetItemCount(); i++)
			m_listBig.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);

		return;
	}

	CProjectData *ParentPrj = GetProjectByHItem(ModData->GetParentHItem());
	CString fmt = ParentPrj->GetParam(ModData->GetModelParam(m_vParam)->ParamName)->GetFormat();

	ParamSumResult *pPSR = ModData->ModelResult.GetPSResult(m_vParam);

	VInt sel;
	int N = 0;
	if(m_vTab != C_MODRES_PAGE_SPECLOAD && m_vTab != C_MODRES_PAGE_CHEMLOAD)
	{
		bool isValid = GetCurCalibValid();

		int iList = 0;
		if(!isValid)
		{
			ItStr itCp;
			for(itCp=selC.begin(); itCp!=selC.end(); ++itCp)
				sel.push_back(ModData->ModelResult.GetCalibIndByName(*itCp));

			N = ModData->ModelResult.GetNCalibResult();
			for(iList=0; iList<N; iList++)
			{
				SpectrumResult* pSpR = ModData->ModelResult.GetCalibResult(iList);
				SpectrumParamResult *pSpPR = &pSpR->SpecParamData[m_vParam];

				s.Format(cFmt1, iList+1);
				m_listBig.InsertItem(iList, s);
				m_listBig.SetItemText(iList, 1, pSpR->Name);

				if(m_vTab == C_MODRES_PAGE_SEC)
				{
					s.Format(fmt, pSpPR->Reference);
					m_listBig.SetItemText(iList, 2, s);
					s.Format(fmt, pSpPR->SEC_Prediction);
					m_listBig.SetItemText(iList, 3, s);
					s.Format(fmt, pSpPR->SEC_RefPred);
					if(fabs(pSpPR->SEC_RefPred) > pPSR->SEC * m_vSEC)
						s += cWarn;
					m_listBig.SetItemText(iList, 4, s);
					if(pSpPR->Reference > 1.e-9)
					{
						s.Format(cFmt31, pSpPR->SEC_Proc);
						m_listBig.SetItemText(iList, 5, s);
					}
					else
						m_listBig.SetItemText(iList, 5, cEmpty);
				}
				if(m_vTab == C_MODRES_PAGE_SECV)
				{
					s.Format(fmt, pSpPR->Reference);
					m_listBig.SetItemText(iList, 2, s);
					s.Format(fmt, pSpPR->SECV_Prediction);
					m_listBig.SetItemText(iList, 3, s);
					s.Format(fmt, pSpPR->SECV_RefPred);
					if(fabs(pSpPR->SECV_RefPred) > pPSR->SECV * m_vSECV)
						s += cWarn;
					m_listBig.SetItemText(iList, 4, s);
					s.Format(cFmt31, pSpPR->SECV_Proc);
					m_listBig.SetItemText(iList, 5, s);
					s.Format(cFmt86, pSpPR->SECV_SEC);
					m_listBig.SetItemText(iList, 6, s);
				}
				if(m_vTab == C_MODRES_PAGE_OUT)
				{
					s.Format(cFmt42, pSpR->Out_Mah);
					m_listBig.SetItemText(iList, 2, s);
					s.Format(cFmt42, m_vMaxMah - pSpR->Out_Mah);
					if(pSpR->Out_Mah > m_vMaxMah)
						s += cWarn;
					m_listBig.SetItemText(iList, 3, s);
				}
				if(m_vTab == C_MODRES_PAGE_BAD)
				{
					s.Format(fmt, pSpPR->Reference);
					m_listBig.SetItemText(iList, 2, s);
					s.Format(fmt, pSpPR->Bad_Prediction);
					m_listBig.SetItemText(iList, 3, s);
					s.Format(fmt, pSpPR->Bad_RefPred);
					m_listBig.SetItemText(iList, 4, s);
					if(pSpPR->Reference > 1.e-9)
					{
						s.Format(cFmt31, pSpPR->Bad_Proc);
						m_listBig.SetItemText(iList, 5, s);
					}
					else
						m_listBig.SetItemText(iList, 5, cEmpty);
					s.Format(cFmt42, pSpPR->Bad_Remainder);
					if(pSpPR->Bad_Remainder > pPSR->norm)
						s += cWarn;
					m_listBig.SetItemText(iList, 6, s);
				}
				if(m_vTab == C_MODRES_PAGE_SCORES)
				{
					int NGK = ModData->GetNCompQnt();
					for(int m=0; m<NGK; m++)
					{
						s.Format(cFmt86, pSpR->Scores_GK[m]);
						m_listBig.SetItemText(iList, m + 2, s);
					}
				}
			}
		}
		else
		{
			ItStr itCp;
			for(itCp=selV.begin(); itCp!=selV.end(); ++itCp)
				sel.push_back(ModData->ModelResult.GetValidIndByName(*itCp));

			N = ModData->ModelResult.GetNValidResult();
			for(iList=0; iList<N; iList++)
			{
				SpectrumValResult* pSpVR = ModData->ModelResult.GetValidResult(iList);
				SpectrumValParamResult *pSpVPR = &pSpVR->SpecValParamData[m_vParam];

				s.Format(cFmt1, iList+1);
				m_listBig.InsertItem(iList, s);
				m_listBig.SetItemText(iList, 1, pSpVR->Name);

				if(m_vTab == C_MODRES_PAGE_SEV)
				{
					s.Format(fmt, pSpVPR->Reference);
					m_listBig.SetItemText(iList, 2, s);
					s.Format(fmt, pSpVPR->Corrected);
					m_listBig.SetItemText(iList, 3, s);
					s.Format(fmt, pSpVPR->RefPred);
					if(fabs(pSpVPR->RefPred) > pPSR->SEV * m_vSEV)
						s += cWarn;
					m_listBig.SetItemText(iList, 4, s);
					s.Format(cFmt31, pSpVPR->Proc);
					m_listBig.SetItemText(iList, 5, s);
					s.Format(cFmt42, pSpVPR->Delta);
					m_listBig.SetItemText(iList, 6, s);
				}
				if(m_vTab == C_MODRES_PAGE_OUT)
				{
					s.Format(cFmt42, pSpVR->Out_Mah);
					m_listBig.SetItemText(iList, 2, s);
					s.Format(cFmt42, m_vMaxMah - pSpVR->Out_Mah);
					if(pSpVR->Out_Mah > m_vMaxMah)
						s += cWarn;
					m_listBig.SetItemText(iList, 3, s);
				}
			}
		}

		if(!IsAvr || m_vTab == C_MODRES_PAGE_SEV)
			s.Format(ParcelLoadString(STRID_MODEL_RESULT_ALLNUM), N);
		else
			s.Format(ParcelLoadString(STRID_MODEL_RESULT_ALLNUM_AVR), N);
		GetDlgItem(IDC_STATIC_1)->SetWindowText(s);
	}

	if(m_vTab == C_MODRES_PAGE_SPECLOAD)
	{
		N = ModData->ModelResult.GetNLoadingResult();
		for(int ispec=0; ispec<N; ispec++)
		{
			LoadingResult *pLR = ModData->ModelResult.GetLoadingResultByInd(ispec);
			s.Format(cFmt1, pLR->SpecPoint + 1);
			m_listBig.InsertItem(ispec, s);
			s.Format(cFmt86, ParentPrj->GetSpecPoint(pLR->SpecPoint));
			m_listBig.SetItemText(ispec, 1, s);

			int NGK = ModData->GetNCompQnt();
			for(int m=0; m<NGK; m++)
			{
				s.Format(cFmt86, pLR->Spec_GK[m]);
				m_listBig.SetItemText(ispec, m + 2, s);
			}

			s.Format(ParcelLoadString(STRID_MODEL_RESULT_SPECLOAD_ALLNUM), N);
			GetDlgItem(IDC_STATIC_1)->SetWindowText(s);
		}
	}
	if(m_vTab == C_MODRES_PAGE_CHEMLOAD)
	{
//		sel.push_back(0);
		N = ModData->ModelResult.GetNPSResult();
		for(int ipar=0; ipar<N; ipar++)
		{
			ParamSumResult *pPSR = ModData->ModelResult.GetPSResult(ipar);
			s.Format(cFmt1, ipar + 1);
			m_listBig.InsertItem(ipar, s);
			m_listBig.SetItemText(ipar, 1, pPSR->ParamName);

			int NGK = ModData->GetNCompQnt();
			for(int m=0; m<NGK; m++)
			{
				s.Format(cFmt86, pPSR->Chem_GK[m]);
				m_listBig.SetItemText(ipar, m + 2, s);
			}

			s.Format(ParcelLoadString(STRID_MODEL_RESULT_CHEMLOAD_ALLNUM), N);
			GetDlgItem(IDC_STATIC_1)->SetWindowText(s);
		}
	}

	SetSortIcons();
	for(i=0; i<m_listBig.GetHeaderCtrl()->GetItemCount(); i++)
		m_listBig.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);

	ItInt itS;
	for(itS=sel.begin(); itS!=sel.end(); ++itS)
	{
		if((*itS) >= 0 && (*itS) < N)
			m_listBig.SetItemState((*itS), LVIS_SELECTED, LVIS_SELECTED);
	}
}

void CModelResultDlg::FillTableSmall()
{
	int i;

	m_listSmall.DeleteAllItems();
	if(m_vTab == C_MODRES_PAGE_SECV && !ModData->IsUseSECV() ||
	   m_vTab == C_MODRES_PAGE_SEV && ModData->ModelResult.GetNValidResult() == 0)
	{
		for(i=0; i<m_listSmall.GetHeaderCtrl()->GetItemCount(); i++)
			m_listSmall.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);

		return;
	}

	int iList = 0;
	int nPSM = ModData->ModelResult.GetNPSResult();
	for(int k=0; k<nPSM; k++)
	{
		ParamSumResult *pPSR = ModData->ModelResult.GetPSResult(k);

		CString s = pPSR->ParamName;
		m_listSmall.InsertItem(iList, s);

		if(m_vTab == C_MODRES_PAGE_SEC)
		{
			s.Format(cFmt86, pPSR->SEC);
			m_listSmall.SetItemText(iList, 1, s);
			s.Format(cFmt86, pPSR->R2sec);
			m_listSmall.SetItemText(iList, 2, s);
			if(pPSR->tSECb >= 0)
				s.Format(ParcelLoadString(STRID_MODEL_RESULT_TRENDPLUS), pPSR->tSECa, fabs(pPSR->tSECb));
			else
				s.Format(ParcelLoadString(STRID_MODEL_RESULT_TRENDMINUS), pPSR->tSECa, fabs(pPSR->tSECb));
			m_listSmall.SetItemText(iList, 3, s);
		}
		if(m_vTab == C_MODRES_PAGE_SECV)
		{
			s.Format(cFmt86, pPSR->SECV);
			m_listSmall.SetItemText(iList, 1, s);
			s.Format(cFmt86, pPSR->R2secv);
			m_listSmall.SetItemText(iList, 2, s);
			s.Format(cFmt86, pPSR->F);
			m_listSmall.SetItemText(iList, 3, s);
			if(pPSR->tSECVb >= 0)
				s.Format(ParcelLoadString(STRID_MODEL_RESULT_TRENDPLUS), pPSR->tSECVa, fabs(pPSR->tSECVb));
			else
				s.Format(ParcelLoadString(STRID_MODEL_RESULT_TRENDMINUS), pPSR->tSECVa, fabs(pPSR->tSECVb));
			m_listSmall.SetItemText(iList, 4, s);
		}
		if(m_vTab == C_MODRES_PAGE_SEV)
		{
			s.Format(cFmt86, pPSR->SEV);
			m_listSmall.SetItemText(iList, 1, s);
			s.Format(cFmt86, pPSR->SDV);
			m_listSmall.SetItemText(iList, 2, s);
			if(ModData->ModelResult.GetNVal() > 1)
				s.Format(cFmt86, pPSR->R2sev);
			else
				s = ParcelLoadString(STRID_MODEL_RESULT_DASH);
			m_listSmall.SetItemText(iList, 3, s);
			s.Format(cFmt86, pPSR->e);
			m_listSmall.SetItemText(iList, 4, s);
			s.Format(_T("%4.2f (%4.2f)"), pPSR->t, pPSR->t0);
			m_listSmall.SetItemText(iList, 5, s);
			if(ModData->ModelResult.GetNVal() > 1)
			{
				if(pPSR->tSEVb >= 0)
					s.Format(ParcelLoadString(STRID_MODEL_RESULT_TRENDPLUS), pPSR->tSEVa, fabs(pPSR->tSEVb));
				else
					s.Format(ParcelLoadString(STRID_MODEL_RESULT_TRENDMINUS), pPSR->tSEVa, fabs(pPSR->tSEVb));
			}
			else
				s = ParcelLoadString(STRID_MODEL_RESULT_DASH);
			m_listSmall.SetItemText(iList, 6, s);
		}

		iList++;
	}

	for(i=0; i<m_listSmall.GetHeaderCtrl()->GetItemCount(); i++)
		m_listSmall.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);

	m_listSmall.SetItemState(0, LVIS_SELECTED, LVIS_SELECTED);
}

void CModelResultDlg::SortAll()
{
	int prop = ModData->ModelResult.SortCalibProp;
	bool dir = ModData->ModelResult.SortCalibDir;
	int ind = m_vParam;

	ModData->ModelResult.SortCalib(prop, dir, ind, m_vMaxMah);

	prop = ModData->ModelResult.SortValidProp;
	dir = ModData->ModelResult.SortValidDir;

	ModData->ModelResult.SortValid(prop, dir, ind, m_vMaxMah);
}

void CModelResultDlg::SetSortIcons()
{
	bool isValid = GetCurCalibValid();

	int prop = (!isValid) ? ModData->ModelResult.SortCalibProp : ModData->ModelResult.SortValidProp;
	bool dir = (!isValid) ? ModData->ModelResult.SortCalibDir : ModData->ModelResult.SortValidDir;

	int iCol = -1;
	switch(m_vTab)
	{
	case C_MODRES_PAGE_SEC:
		if(prop == C_MODRES_SORT_CALIB_NAME)  iCol = 1;
		if(prop == C_MODRES_SORT_CALIB_REF)  iCol = 2;
		if(prop == C_MODRES_SORT_CALIB_SEC_PRED)  iCol = 3;
		if(prop == C_MODRES_SORT_CALIB_SEC_DIFF)  iCol = 4;
		if(prop == C_MODRES_SORT_CALIB_SEC_PROC)  iCol = 5;
		break;
	case C_MODRES_PAGE_SECV:
		if(!ModData->IsUseSECV())
			break;
		if(prop == C_MODRES_SORT_CALIB_NAME)  iCol = 1;
		if(prop == C_MODRES_SORT_CALIB_REF)  iCol = 2;
		if(prop == C_MODRES_SORT_CALIB_SECV_PRED)  iCol = 3;
		if(prop == C_MODRES_SORT_CALIB_SECV_DIFF)  iCol = 4;
		if(prop == C_MODRES_SORT_CALIB_SECV_PROC)  iCol = 5;
		if(prop == C_MODRES_SORT_CALIB_SECV_SEC)  iCol = 6;
		break;
	case C_MODRES_PAGE_SEV:
		if(ModData->GetNSamplesForValid() <= 0)
			break;
		if(prop == C_MODRES_SORT_VALID_NAME)  iCol = 1;
		if(prop == C_MODRES_SORT_VALID_REF)  iCol = 2;
		if(prop == C_MODRES_SORT_VALID_PRED)  iCol = 3;
		if(prop == C_MODRES_SORT_VALID_DIFF)  iCol = 4;
		if(prop == C_MODRES_SORT_VALID_PROC)  iCol = 5;
		if(prop == C_MODRES_SORT_VALID_DELTA)  iCol = 6;
		break;
	case C_MODRES_PAGE_OUT:
		if(!isValid)
		{
			if(prop == C_MODRES_SORT_CALIB_NAME)  iCol = 1;
			if(prop == C_MODRES_SORT_CALIB_OUT_MD)  iCol = 2;
			if(prop == C_MODRES_SORT_CALIB_OUT_EXCEED)  iCol = 3;
		}
		else
		{
			if(ModData->GetNSamplesForValid() <= 0)
				break;
			if(prop == C_MODRES_SORT_VALID_NAME)  iCol = 1;
			if(prop == C_MODRES_SORT_VALID_MD)  iCol = 2;
			if(prop == C_MODRES_SORT_VALID_EXCEED)  iCol = 3;
		}
		break;
	case C_MODRES_PAGE_BAD:
		if(prop == C_MODRES_SORT_CALIB_NAME)  iCol = 1;
		if(prop == C_MODRES_SORT_CALIB_REF)  iCol = 2;
		if(prop == C_MODRES_SORT_CALIB_BAD_PRED)  iCol = 3;
		if(prop == C_MODRES_SORT_CALIB_BAD_DIFF)  iCol = 4;
		if(prop == C_MODRES_SORT_CALIB_BAD_PROC)  iCol = 5;
		if(prop == C_MODRES_SORT_CALIB_BAD_REST)  iCol = 6;
		break;
	case C_MODRES_PAGE_SCORES:
		if(prop == C_MODRES_SORT_CALIB_NAME)  iCol = 1;
		if(prop >= C_MODRES_SORT_CALIB_GK_0)  iCol = 2 + (prop - C_MODRES_SORT_CALIB_GK_0);
		break;
	}

	m_listBig.ShowSortIcons(iCol, dir);
}

bool CModelResultDlg::ExcludeSpectra()
{
	if(m_vTab == C_MODRES_PAGE_SPECLOAD || m_vTab == C_MODRES_PAGE_CHEMLOAD)
		return false;

	bool isValid = GetCurCalibValid();
	if(!isValid && int(selC.size()) >= ModData->ModelResult.GetNCalibResult())
	{
		CString s = ParcelLoadString(STRID_MODEL_QLT_RESULT_ERROR_NOCALIB);
		ParcelError(s);
		return false;
	}

	VStr* pSel = (isValid) ? &selV : &selC;
	ItStr it;
	for(it=pSel->begin(); it!=pSel->end(); ++it)
	{
		if(!isValid)
		{
			SpectrumResult* pSpR = ModData->ModelResult.GetCalibResult(*it);
			if(pSpR == NULL)
				continue;

			ModelSample* MSam = ModData->GetSampleForCalib(pSpR->SamName, pSpR->TransName);
			if(MSam == NULL)
				continue;

			if(!IsAvr)
				MSam->Spectra[pSpR->SpecNum] = false;

			if(IsAvr || MSam->GetNSpectraUsed() <= 0)
				ModData->DeleteSampleForCalib(pSpR->SamName, pSpR->TransName);
		}
		else
		{
			SpectrumValResult* pSpVR = ModData->ModelResult.GetValidResult(*it);
			if(pSpVR == NULL)
				continue;

			ModelSample* MSam = ModData->GetSampleForValid(pSpVR->SamName, pSpVR->TransName);
			if(MSam == NULL)
				continue;

			if(!IsAvr)
				MSam->Spectra[pSpVR->SpecNum] = false;

			if(IsAvr || MSam->GetNSpectraUsed() <= 0)
				ModData->DeleteSampleForValid(pSpVR->SamName, pSpVR->TransName);
		}
	}

	pSel->clear();

	return true;
}

bool CModelResultDlg::ExcludePoints()
{
	if(m_vTab != C_MODRES_PAGE_SPECLOAD)
		return false;

	int N = ModData->ModelResult.GetNLoadingResult();

	int Ind = -1, nsel = m_listBig.GetSelectedCount();
	if(nsel <= 0)
		return false;

	if(nsel >= N)
	{
		CString s = ParcelLoadString(STRID_MODEL_RESULT_ERROR_NOSPECPNT);
		ParcelError(s);
		return false;
	}

	SInt Points;
	ModData->GetExcPoints(Points);
	for(int i=0; i<nsel; i++)
	{
		Ind = m_listBig.GetNextItem(Ind, LVNI_SELECTED);
		if(Ind < 0 || Ind >= N)
			break;

		LoadingResult *pLR = ModData->ModelResult.GetLoadingResultByInd(Ind);
		if(pLR == NULL)
			return false;

		Points.insert(pLR->SpecPoint);
	}
	ModData->SetExcPoints(Points);

	return true;
}

void CModelResultDlg::CreateGraphSEC()
{
	if(pGraphSEC != NULL)
	{
		pChart->RemoveGraph(pGraphSEC->GetPos());
		delete pGraphSEC;
		pGraphSEC = NULL;
	}
	if(pMarkSEC != NULL)
	{
		delete[] pMarkSEC;
		pMarkSEC = NULL;
	}
	if(pGraphLineSEC != NULL)
	{
		pChart->RemoveGraph(pGraphLineSEC->GetPos());
		delete pGraphLineSEC;
		pGraphLineSEC = NULL;
	}

	ModData->ModelResult.CreateGraphSEC(m_vParam);
	pGISEC = ModData->ModelResult.GetGraphSEC();

	pGraphSEC = new CLumChartDataSet(pGISEC->N, pGISEC->x, pGISEC->y);
	pGraphSEC->SetInterpolation(LCF_CONNECT_NONE);
	pGraphSEC->SetColor(cColorBlack);
	pGraphSEC->SetMarkerColor(cColorGraphNorm);
	pGraphSEC->SetMarkerType(LCF_MARKERS_ROUND);
	pGraphSEC->SetMarkerSize(3);
	pGraphSEC->SetName(ParcelLoadString(STRID_MODEL_RESULT_GRAPH_SECNAME));

	pMarkSEC = new CLumChartDataMarker[pGISEC->N];
	for(int i=0; i<pGISEC->N; i++)
	{
		pMarkSEC[i].nColor = cColorGraphNorm;
		pMarkSEC[i].nSize = 3;
		pMarkSEC[i].nType = LCF_MARKERS_ROUND;
	}
	pGraphSEC->SetMarkerArray(pMarkSEC);
	pGraphSEC->EnableUserMarkers(true);
	pChart->AddGraph(pGraphSEC, false);

	double minSEC = min(pGISEC->xmin, pGISEC->ymin);
	double maxSEC = max(pGISEC->xmax, pGISEC->ymax);
	double plus = (maxSEC - minSEC) * 0.05;
	pGraphLineSEC = new CLumChartDataSet(minSEC - plus, maxSEC + plus, fGraphLineSEC);
	pGraphLineSEC->SetColor(cColorBlack);
	pGraphLineSEC->SetName(ParcelLoadString(STRID_MODEL_RESULT_GRAPH_SECTREND));
	pChart->AddGraph(pGraphLineSEC, false);

	CreateGraphTeor();

	ColoredGraphSEC();
}

void CModelResultDlg::CreateGraphSECV()
{
	if(pGraphSECV != NULL)
	{
		pChart->RemoveGraph(pGraphSECV->GetPos());
		delete pGraphSECV;
		pGraphSECV = NULL;
	}
	if(pMarkSECV != NULL)
	{
		delete[] pMarkSECV;
		pMarkSECV = NULL;
	}
	if(pGraphLineSECV != NULL)
	{
		pChart->RemoveGraph(pGraphLineSECV->GetPos());
		delete pGraphLineSECV;
		pGraphLineSECV = NULL;
	}

	if(!ModData->IsUseSECV())
	{
		CreateGraphTeor();
		return;
	}

	ModData->ModelResult.CreateGraphSECV(m_vParam);
	pGISECV = ModData->ModelResult.GetGraphSECV();

	pGraphSECV = new CLumChartDataSet(pGISECV->N, pGISECV->x, pGISECV->y);
	pGraphSECV->SetInterpolation(LCF_CONNECT_NONE);
	pGraphSECV->SetColor(cColorBlack);
	pGraphSECV->SetMarkerColor(cColorGraphNorm);
	pGraphSECV->SetMarkerType(LCF_MARKERS_ROUND);
	pGraphSECV->SetMarkerSize(3);
	pGraphSECV->SetName(ParcelLoadString(STRID_MODEL_RESULT_GRAPH_SECVNAME));

	pMarkSECV = new CLumChartDataMarker[pGISECV->N];
	for(int i=0; i<pGISECV->N; i++)
	{
		pMarkSECV[i].nColor = cColorGraphNorm;
		pMarkSECV[i].nSize = 3;
		pMarkSECV[i].nType = LCF_MARKERS_ROUND;
	}
	pGraphSECV->SetMarkerArray(pMarkSECV);
	pGraphSECV->EnableUserMarkers(true);
	pChart->AddGraph(pGraphSECV, false);

	double minSECV = min(pGISECV->xmin, pGISECV->ymin);
	double maxSECV = max(pGISECV->xmax, pGISECV->ymax);
	double plus = (maxSECV - minSECV) * 0.05;
	pGraphLineSECV = new CLumChartDataSet(minSECV - plus, maxSECV + plus, fGraphLineSECV);
	pGraphLineSECV->SetColor(cColorBlack);
	pGraphLineSECV->SetName(ParcelLoadString(STRID_MODEL_RESULT_GRAPH_SECVTREND));
	pChart->AddGraph(pGraphLineSECV, false);

	ColoredGraphSECV();
}

void CModelResultDlg::CreateGraphSEV()
{
	if(pGraphSEV != NULL)
	{
		pChart->RemoveGraph(pGraphSEV->GetPos());
		delete pGraphSEV;
		pGraphSEV = NULL;
	}
	if(pMarkSEV != NULL)
	{
		delete[] pMarkSEV;
		pMarkSEV = NULL;
	}
	if(pGraphLineSEV != NULL)
	{
		pChart->RemoveGraph(pGraphLineSEV->GetPos());
		delete pGraphLineSEV;
		pGraphLineSEV = NULL;
	}
	ModData->ModelResult.CreateGraphSEV(m_vParam);
	pGISEV = ModData->ModelResult.GetGraphSEV();

	if(pGISEV->N <= 0)
	{
		CreateGraphTeor();
		return;
	}

	pGraphSEV = new CLumChartDataSet(pGISEV->N, pGISEV->x, pGISEV->y);
	pGraphSEV->SetInterpolation(LCF_CONNECT_NONE);
	pGraphSEV->SetColor(cColorBlack);
	pGraphSEV->SetMarkerColor(cColorGraphNorm);
	pGraphSEV->SetMarkerType(LCF_MARKERS_ROUND);
	pGraphSEV->SetMarkerSize(3);
	pGraphSEV->SetName(ParcelLoadString(STRID_MODEL_RESULT_GRAPH_SEVNAME));

	pMarkSEV = new CLumChartDataMarker[pGISEV->N];
	for(int i=0; i<pGISEV->N; i++)
	{
		pMarkSEV[i].nColor = cColorGraphNorm;
		pMarkSEV[i].nSize = 3;
		pMarkSEV[i].nType = LCF_MARKERS_ROUND;
	}
	pGraphSEV->SetMarkerArray(pMarkSEV);
	pGraphSEV->EnableUserMarkers(true);
	pChart->AddGraph(pGraphSEV, false);

	double minSEV = min(pGISEV->xmin, pGISEV->ymin);
	double maxSEV = max(pGISEV->xmax, pGISEV->ymax);

	double plus = (maxSEV - minSEV) * 0.06;
	pGraphLineSEV = new CLumChartDataSet(minSEV - plus, maxSEV + plus, fGraphLineSEV);
	pGraphLineSEV->SetColor(cColorBlack);
	pGraphLineSEV->SetName(ParcelLoadString(STRID_MODEL_RESULT_GRAPH_SEVTREND));
	pChart->AddGraph(pGraphLineSEV, false);

	CreateGraphTeor();

	ColoredGraphSEV();
}

void CModelResultDlg::CreateGraphTeor()
{
	if(pGraphLine0 != NULL)
	{
		pChart->RemoveGraph(pGraphLine0->GetPos());
		delete pGraphLine0;
		pGraphLine0 = NULL;
	}

	pGISEC = ModData->ModelResult.GetGraphSEC();
	pGISECV = ModData->ModelResult.GetGraphSECV();
	pGISEV = ModData->ModelResult.GetGraphSEV();
	if(pGISEC == NULL)
		return;

	double Min = min(pGISEC->xmin, pGISEC->ymin);
	double Max = max(pGISEC->xmax, pGISEC->ymax);
	if(pGISECV != NULL)
	{
		Min = min(Min, min(pGISECV->xmin, pGISECV->ymin));
		Max = max(Max, max(pGISECV->xmax, pGISECV->ymax));
	}
	if(pGISEV != NULL)
	{
		Min = min(Min, min(pGISEV->xmin, pGISEV->ymin));
		Max = max(Max, max(pGISEV->xmax, pGISEV->ymax));
	}

	double plus = (Max - Min) * 0.05;
	pGraphLine0 = new CLumChartDataSet(Min - plus, Max + plus, fGraphLine0);
	pGraphLine0->SetColor(cColorGrey);
	pGraphLine0->SetStyle(PS_DOT);
	pGraphLine0->SetName(ParcelLoadString(STRID_MODEL_RESULT_GRAPH_TEORLINE));
	pChart->AddGraph(pGraphLine0, false);
}

void CModelResultDlg::CreateHistoCalib()
{
	if(pHistoCalib != NULL)
	{
		pChart->RemoveGraph(pHistoCalib->GetPos());
		delete pHistoCalib;
		pHistoCalib = NULL;
	}
    if(pMarkHCalib != NULL)
	{
		delete[] pMarkHCalib;
		pMarkHCalib = NULL;
	}

	ModData->ModelResult.CreateHistoCalib();
	pHICalib = ModData->ModelResult.GetHistoCalib();

	pHistoCalib = new CLumChartDataSet(pHICalib->N, 1, 1, pHICalib->y);
	pHistoCalib->SetInterpolation(LCF_CONNECT_HISTO);
	pHistoCalib->SetColor(cColorBlack);
	pHistoCalib->SetMarkerColor(cColorPass);
	pHistoCalib->SetName(ParcelLoadString(STRID_MODEL_RESULT_GRAPH_HISTOCALIBNAME));
	pMarkHCalib = new CLumChartDataMarker[pHICalib->N];
	for(int i=0; i<pHICalib->N; i++)
	{
		pMarkHCalib[i].nColor = cColorPass;
		pMarkHCalib[i].nSize = 3;
		pMarkHCalib[i].nType = LCF_MARKERS_ROUND;
	}
	pHistoCalib->SetMarkerArray(pMarkHCalib);
	pHistoCalib->EnableUserMarkers(true);
	pChart->AddGraph(pHistoCalib, false);

	ColoredHistoCalib();
}

void CModelResultDlg::CreateHistoValid()
{
	if(pHistoValid != NULL)
	{
		pChart->RemoveGraph(pHistoValid->GetPos());
		delete pHistoValid;
		pHistoValid = NULL;
	}
	if(pMarkHValid != NULL)
	{
		delete[] pMarkHValid;
		pMarkHValid = NULL;
	}

	ModData->ModelResult.CreateHistoValid();
	pHIValid = ModData->ModelResult.GetHistoValid();

	if(pHIValid->N <= 1)
		return;

	pHistoValid = new CLumChartDataSet(pHIValid->N, 1, 1, pHIValid->y);
	pHistoValid->SetInterpolation(LCF_CONNECT_HISTO);
	pHistoValid->SetColor(cColorBlack);
	pHistoValid->SetMarkerColor(cColorPass);
	pHistoValid->SetName(ParcelLoadString(STRID_MODEL_RESULT_GRAPH_HISTOVALIDNAME));
	pMarkHValid = new CLumChartDataMarker[pHIValid->N];
	for(int i=0; i<pHIValid->N; i++)
	{
		pMarkHValid[i].nColor = cColorPass;
		pMarkHValid[i].nSize = 3;
		pMarkHValid[i].nType = LCF_MARKERS_ROUND;
	}
	pHistoValid->SetMarkerArray(pMarkHValid);
	pHistoValid->EnableUserMarkers(true);
	pChart->AddGraph(pHistoValid, false);

	ColoredHistoValid();
}

void CModelResultDlg::CreateHistoSamCalib()
{
	if(pHistoSamCalib != NULL)
	{
		pChart->RemoveGraph(pHistoSamCalib->GetPos());
		delete pHistoSamCalib;
		pHistoSamCalib = NULL;
	}
	if(!ModData->ModelResult.CreateHistoSamCalib(m_vParam))
		return;

	pHISamCalib = ModData->ModelResult.GetHistoSamCalib();

//	pHistoSamCalib = new CLumChartDataSet(pHISamCalib->N, 1, 1, pHISamCalib->y);
	pHistoSamCalib = new CLumChartDataSet(pHISamCalib->N, pHISamCalib->x, pHISamCalib->y);
	pHistoSamCalib->SetInterpolation(LCF_CONNECT_HISTO);
	pHistoSamCalib->SetColor(cColorBlack);
	pHistoSamCalib->SetMarkerColor(cColorPass);
	pHistoSamCalib->SetName(ParcelLoadString(STRID_MODEL_RESULT_GRAPH_HISTOSAMCALIBNAME));
	pHistoSamCalib->SetFormat(cFmt31, _T("%2.0f"));
	pChart->AddGraph(pHistoSamCalib, false);
}

void CModelResultDlg::CreateHistoSamValid()
{
	if(pHistoSamValid != NULL)
	{
		pChart->RemoveGraph(pHistoSamValid->GetPos());
		delete pHistoSamValid;
		pHistoSamValid = NULL;
	}
	if(!ModData->ModelResult.CreateHistoSamValid(m_vParam))
		return;

	pHISamValid = ModData->ModelResult.GetHistoSamValid();

//	pHistoSamValid = new CLumChartDataSet(pHISamValid->N, 1, 1, pHISamValid->y);
	pHistoSamValid = new CLumChartDataSet(pHISamValid->N, pHISamValid->x, pHISamValid->y);
	pHistoSamValid->SetInterpolation(LCF_CONNECT_HISTO);
	pHistoSamValid->SetColor(cColorBlack);
	pHistoSamValid->SetMarkerColor(cColorPass);
	pHistoSamValid->SetName(ParcelLoadString(STRID_MODEL_RESULT_GRAPH_HISTOSAMVALIDNAME));
	pHistoSamValid->SetFormat(cFmt31, _T("%2.0f"));
	pChart->AddGraph(pHistoSamValid, false);
}

void CModelResultDlg::CreateHistoBad()
{
	if(pHistoBad != NULL)
	{
		pChart->RemoveGraph(pHistoBad->GetPos());
		delete pHistoBad;
		pHistoBad = NULL;
	}
	if(pMarkHBad != NULL)
	{
		delete[] pMarkHBad;
		pMarkHBad = NULL;
	}
	if(pHistoNorm != NULL)
	{
		pChart->RemoveGraph(pHistoNorm->GetPos());
		delete pHistoNorm;
		pHistoNorm = NULL;
	}

	ModData->ModelResult.CreateHistoBad(m_vParam);
	pHIBad = ModData->ModelResult.GetHistoBad();

	pHistoBad = new CLumChartDataSet(pHIBad->N, 1, 1, pHIBad->y);
	pHistoBad->SetInterpolation(LCF_CONNECT_HISTO);
	pHistoBad->SetColor(cColorBlack);
	pHistoBad->SetMarkerColor(cColorPass);
	pHistoBad->SetName(ParcelLoadString(STRID_MODEL_RESULT_GRAPH_HISTOBADNAME));
	pMarkHBad = new CLumChartDataMarker[pHIBad->N];
	for(int i=0; i<pHIBad->N; i++)
	{
		pMarkHBad[i].nColor = cColorPass;
		pMarkHBad[i].nSize = 3;
		pMarkHBad[i].nType = LCF_MARKERS_ROUND;
	}
	pHistoBad->SetMarkerArray(pMarkHBad);
	pHistoBad->EnableUserMarkers(true);
	pChart->AddGraph(pHistoBad, false);

	double N = double(pHIBad->N);
	pHistoNorm = new CLumChartDataSet(0., N, fHistoNorm);
	pHistoNorm->SetColor(cColorErr);
	pHistoNorm->SetName(ParcelLoadString(STRID_MODEL_RESULT_GRAPH_BADMAXNAME));
	pChart->AddGraph(pHistoNorm, false);

	ColoredHistoBad();
}

void CModelResultDlg::CreateGraphScores()
{
	if(pGraphScores != NULL)
	{
		pChart->RemoveGraph(pGraphScores->GetPos());
		delete pGraphScores;
		pGraphScores = NULL;
	}
	if(pMarkScores != NULL)
	{
		delete[] pMarkScores;
		pMarkScores = NULL;
	}

	ModData->ModelResult.CreateGraphScores(m_vOXsc, m_vOYsc);
	pGIScores = ModData->ModelResult.GetGraphScores();

	pGraphScores = new CLumChartDataSet(pGIScores->N, pGIScores->x, pGIScores->y);
	pGraphScores->SetInterpolation(LCF_CONNECT_NONE);
	pGraphScores->SetColor(cColorBlack);
	pGraphScores->SetMarkerColor(cColorGraphNorm);
	pGraphScores->SetMarkerType(LCF_MARKERS_ROUND);
	pGraphScores->SetMarkerSize(3);
	pGraphScores->SetName(ParcelLoadString(STRID_MODEL_RESULT_GRAPH_SCORESNAME));

	pMarkScores = new CLumChartDataMarker[pGIScores->N];
	for(int i=0; i<pGIScores->N; i++)
	{
		pMarkScores[i].nColor = cColorGraphNorm;
		pMarkScores[i].nSize = 3;
		pMarkScores[i].nType = LCF_MARKERS_ROUND;
	}
	pGraphScores->SetMarkerArray(pMarkScores);
	pGraphScores->EnableUserMarkers(true);

	pChart->AddGraph(pGraphScores, false);

	ColoredGraphScores();
}

void CModelResultDlg::CreateGraphsSpecLoad()
{
	vector<CLumChartDataSet>::iterator it;
	for(it=GraphsSpecLoad.begin(); it!=GraphsSpecLoad.end(); ++it)
		pChart->RemoveGraph(it->GetPos());
	GraphsSpecLoad.clear();

	vector<POSITION>::iterator it3;
	for(it3=MasksSLPos.begin(); it3!=MasksSLPos.end(); ++it3)
		pChart->RemoveMask(*it3);
	MasksSLPos.clear();

	ModData->ModelResult.CreateGraphsSpecLoad(ModData->GetParentHItem());

	int i;
	for(i=0; i<ModData->GetNCompQnt(); i++)
	{
		GraphInfo* pGI = ModData->ModelResult.GetGraphSpecLoad(i);

		CLumChartDataSet Graph(pGI->N, pGI->x, pGI->y);
		Graph.SetInterpolation(LCF_CONNECT_DIRECT);
		Graph.SetColor(cGetDefColor(i));
		Graph.SetMarkerType(LCF_MARKERS_NONE);

		CString s;
		s.Format(ParcelLoadString(STRID_MODEL_RESULT_GRAPH_SPLOADNAME), i + 1);
		Graph.SetName(s);

		pChart->AddGraph(&Graph, false);
		GraphsSpecLoad.push_back(Graph);
	}
}

void CModelResultDlg::CreateGraphChemLoad()
{
	if(pGraphChemLoad != NULL)
	{
		pChart->RemoveGraph(pGraphChemLoad->GetPos());
		delete pGraphChemLoad;
		pGraphChemLoad = NULL;
	}
	if(pMarkChemLoad != NULL)
	{
		delete[] pMarkChemLoad;
		pMarkChemLoad = NULL;
	}
	if(posLegendChem != NULL)
	{
		pChart->RemoveUserLegend(posLegendChem);
		posLegendChem = NULL;
	}

	ModData->ModelResult.CreateGraphChemLoad(m_vOXch, m_vOYch);
	pGIChemLoad = ModData->ModelResult.GetGraphChemLoad();

	pGraphChemLoad = new CLumChartDataSet(pGIChemLoad->N, pGIChemLoad->x, pGIChemLoad->y);
	pGraphChemLoad->SetInterpolation(LCF_CONNECT_NONE);
	pGraphChemLoad->SetColor(cColorBlack);
	pGraphChemLoad->SetMarkerColor(cColorSel);
	pGraphChemLoad->SetMarkerType(LCF_MARKERS_ROUND);
	pGraphChemLoad->SetMarkerSize(5);
	pGraphChemLoad->SetName(ParcelLoadString(STRID_MODEL_RESULT_GRAPH_CHLOADNAME));

	pMarkChemLoad = new CLumChartDataMarker[pGIChemLoad->N];
	vector<CLCULString> strings;
	for(int i=0; i<pGIChemLoad->N; i++)
	{
		pMarkChemLoad[i].nColor = cGetDefColor(i);
		pMarkChemLoad[i].nSize = 5;
		pMarkChemLoad[i].nType = LCF_MARKERS_ROUND;

		CLCULString NewString;
		NewString.SetText(ModData->ModelResult.GetPSResult(i)->ParamName);
		NewString.SetMarker(CLumChartDataMarker(LCF_MARKERS_ROUND, 4, cGetDefColor(i)));
		NewString.EnableMarker(true);

		strings.push_back(NewString);
	}
	pGraphChemLoad->SetMarkerArray(pMarkChemLoad);
	pGraphChemLoad->EnableUserMarkers(true);

	pChart->AddGraph(pGraphChemLoad, false);

	CRect RectGraph;
	GetDlgItem(IDC_MODEL_RESULT_TABLEBIG)->GetWindowRect(RectGraph);

	CLCUL legend;
	legend.MoveTo(RectGraph.Width() - 100, 0);
	legend.Resize(100, 20);
	legend.SetStrings(strings);
	legend.SetFillColor(cColorGraphNorm);
	legend.SetTextColor(cColorBlack);
	legend.Enable(false);
	legend.EnableMoveToFit(true);
	legend.EnableAutoWidth(true);
	legend.EnableAutoHeight(true);
	posLegendChem = pChart->AddUserLegend(legend);

	ColoredGraphChemLoad();
}

void CModelResultDlg::CreateHistoMDL()
{
	if(pHistoMDL != NULL)
	{
		pChart->RemoveGraph(pHistoMDL->GetPos());
		delete pHistoMDL;
		pHistoMDL = NULL;
	}

	double N = double(max(pHICalib->N, pHIValid->N));
	pHistoMDL = new CLumChartDataSet(0., N, fHistoMDL);
	pHistoMDL->SetColor(cColorErr);
	pHistoMDL->SetName(ParcelLoadString(STRID_MODEL_RESULT_GRAPH_LINEMAXNAME));
	pChart->AddGraph(pHistoMDL, false);
}

void CModelResultDlg::CreateLegends()
{
	if(posLegendSEC_V != NULL)
	{
		pChart->RemoveUserLegend(posLegendSEC_V);
		posLegendSEC_V = NULL;
	}
	if(posLegendOut != NULL)
	{
		pChart->RemoveUserLegend(posLegendOut);
		posLegendOut = NULL;
	}
	if(posLegendBad != NULL)
	{
		pChart->RemoveUserLegend(posLegendBad);
		posLegendBad = NULL;
	}

	CLCULString NewStringTrend;
	NewStringTrend.SetText(ParcelLoadString(STRID_MODEL_RESULT_GRAPH_SECTREND));
	NewStringTrend.SetLine(cColorBlack, PS_SOLID, 1);
	NewStringTrend.EnableLine(true);

	CLCULString NewStringTeor;
	NewStringTeor.SetText(ParcelLoadString(STRID_MODEL_RESULT_GRAPH_TEORLINE));
	NewStringTeor.SetLine(cColorGrey, PS_DOT, 1);
	NewStringTeor.EnableLine(true);

	CLCULString NewStringMDL;
	NewStringMDL.SetText(ParcelLoadString(STRID_MODEL_RESULT_GRAPH_LINEMAXNAME));
	NewStringMDL.SetLine(cColorErr, PS_SOLID, 1);
	NewStringMDL.EnableLine(true);

	CLCULString NewStringNorm;
	NewStringNorm.SetText(ParcelLoadString(STRID_MODEL_RESULT_GRAPH_BADMAXNAME));
	NewStringNorm.SetLine(cColorErr, PS_SOLID, 1);
	NewStringNorm.EnableLine(true);

	CRect RectGraph;
	GetDlgItem(IDC_MODEL_RESULT_TABLEBIG)->GetWindowRect(RectGraph);

	vector<CLCULString> strings;

	strings.clear();
	if(show_trend)  strings.push_back(NewStringTrend);
	if(show_teor)  strings.push_back(NewStringTeor);

	CLCUL legendSEC_V;
	legendSEC_V.MoveTo(RectGraph.Width() - 100, 0);
	legendSEC_V.Resize(100, 20);
	legendSEC_V.SetStrings(strings);
	legendSEC_V.SetFillColor(cColorGraphNorm);
	legendSEC_V.SetTextColor(cColorBlack);
	legendSEC_V.Enable(false);
	legendSEC_V.EnableMoveToFit(true);
	legendSEC_V.EnableAutoWidth(true);
	legendSEC_V.EnableAutoHeight(true);
	posLegendSEC_V = pChart->AddUserLegend(legendSEC_V);

	strings.clear();
	if(show_mdl)  strings.push_back(NewStringMDL);

	CLCUL legendOut;
	legendOut.MoveTo(RectGraph.Width() - 100, 0);
	legendOut.Resize(100, 20);
	legendOut.SetStrings(strings);
	legendOut.SetFillColor(cColorGraphNorm);
	legendOut.SetTextColor(cColorBlack);
	legendOut.Enable(false);
	legendOut.EnableMoveToFit(true);
	legendOut.EnableAutoWidth(true);
	legendOut.EnableAutoHeight(true);
	posLegendOut = pChart->AddUserLegend(legendOut);

	strings.clear();
	if(show_limit)  strings.push_back(NewStringNorm);

	CLCUL legendBad;
	legendBad.MoveTo(RectGraph.Width() - 100, 0);
	legendBad.Resize(100, 20);
	legendBad.SetStrings(strings);
	legendBad.SetFillColor(cColorGraphNorm);
	legendBad.SetTextColor(cColorBlack);
	legendBad.Enable(false);
	legendBad.EnableMoveToFit(true);
	legendBad.EnableAutoWidth(true);
	legendBad.EnableAutoHeight(true);
	posLegendBad = pChart->AddUserLegend(legendBad);
}

void CModelResultDlg::CreateLegendSpec()
{
	if(posLegendSpec != NULL)
	{
		pChart->RemoveUserLegend(posLegendSpec);
		posLegendSpec = NULL;
	}

	CRect RectGraph;
	GetDlgItem(IDC_MODEL_RESULT_TABLEBIG)->GetWindowRect(RectGraph);

	vector<CLCULString> strings;

	int i;
	for(i=0; i<ModData->GetNCompQnt(); i++)
	{
		GraphInfo* pGI = ModData->ModelResult.GetGraphSpecLoad(i);
		if(!pGI->sel)
			continue;

		CLCULString NewString;
		CString s;
		s.Format(ParcelLoadString(STRID_MODEL_RESULT_GRAPH_SPLOADNAME), i + 1);
		NewString.SetText(s);
		NewString.SetLine(cGetDefColor(i), PS_SOLID, 1);
		NewString.EnableLine(true);

		strings.push_back(NewString);
	}

	CLCUL legendSpec;
	legendSpec.MoveTo(RectGraph.Width() - 100, 0);
	legendSpec.Resize(100, 20);
	legendSpec.SetStrings(strings);
	legendSpec.SetFillColor(cColorGraphNorm);
	legendSpec.SetTextColor(cColorBlack);
	legendSpec.Enable(false);
	legendSpec.EnableMoveToFit(true);
	legendSpec.EnableAutoWidth(true);
	legendSpec.EnableAutoHeight(true);
	posLegendSpec = pChart->AddUserLegend(legendSpec);
}

void CModelResultDlg::CreateAllGraphs()
{
	CreateGraphSEC();
	CreateGraphSECV();
	CreateGraphSEV();
	CreateHistoCalib();
	CreateHistoValid();
	CreateHistoSamCalib();
	CreateHistoSamValid();
	CreateHistoBad();
	CreateHistoMDL();
	CreateGraphScores();
	CreateGraphsSpecLoad();
	CreateGraphChemLoad();
	CreateLegends();
	CreateLegendSpec();
}

void CModelResultDlg::CreateChart()
{
	CRect RectGraph;
	GetDlgItem(IDC_MODEL_RESULT_TABLEBIG)->GetWindowRect(RectGraph);

	pChart = new CLumChart(RectGraph, this, LCF_RECT_SCREEN);
	pChart->SetAxesNames(ParcelLoadString(STRID_MODEL_RESULT_GRAPH_AXISREF),
		ParcelLoadString(STRID_MODEL_RESULT_GRAPH_AXISPRED));

	CreateAllGraphs();

	COLORREF bg = GetSysColor(COLOR_3DFACE);
	pChart->SetBgOutsideColor(bg, cColorBlack);
	pChart->SetMargins(45, 30, 15, 50);
	pChart->SetSelFrameProps(RGB(0, 0, 255), 2);
	pChart->SetHighlightMode(false);

	pChart->SetSolidAxisHorzAt(0.);
	pChart->SetSolidAxisVertAt(0.);

	pChart->ShowLegend(false);
	pChart->EnableUpperLegend(false);
	pChart->EnableCoordLegend(false);

	pChart->SetUserCallback(LCF_CB_LBDOWN, LbClickCallback);
	pChart->SetUserCallback(LCF_CB_LBUP, LbUnClickCallback);
	pChart->SetUserCallback(LCF_CB_RBDOWN, RbClickCallback);
	pChart->SetUserCallback(LCF_CB_RBUP, RbUnClickCallback);
	pChart->EnableUserCallback(LCF_CB_LBDOWN, true);
	pChart->EnableUserCallback(LCF_CB_LBUP, true);
	pChart->EnableUserCallback(LCF_CB_RBDOWN, true);
	pChart->EnableUserCallback(LCF_CB_RBUP, true);

	BuildUserPopupMenu();
	pChart->SetUserPopupMenu(this, pMenu1);
	pChart->EnableUserPopupMenu(true);
}

void CModelResultDlg::ColoredGraphSEC()
{
	GraphInfo* pGI = ModData->ModelResult.GetGraphSEC();
	if(pGraphSEC == NULL || pMarkSEC == NULL || pGI == NULL)
		return;

	ParamSumResult *pPSR = ModData->ModelResult.GetPSResult(m_vParam);

	ItSpecRes it;
	for(it=ModData->ModelResult.CalibResult.begin(); it!=ModData->ModelResult.CalibResult.end(); ++it)
	{
		CString SpName = it->Name;
		SpectrumResult* pSpR = ModData->ModelResult.GetCalibResult(SpName);
		SpectrumParamResult* pSpPR = &pSpR->SpecParamData[m_vParam];

		int ind = pGI->GetIndByName(SpName);
		if(ind >= 0 && ind < pGI->N)
		{
			if(fabs(pSpPR->SEC_RefPred) > pPSR->SEC * m_vSEC)
				pMarkSEC[ind].nColor = cColorErr;
			else
				pMarkSEC[ind].nColor = cColorGraphNorm;

			if(find(selC.begin(), selC.end(), SpName) != selC.end())
				pMarkSEC[ind].nColor = cColorSel;
		}
	}
}

void CModelResultDlg::ColoredGraphSECV()
{
	GraphInfo* pGI = ModData->ModelResult.GetGraphSECV();
	if(pGraphSECV == NULL || pMarkSECV == NULL || pGI == NULL)
		return;

	ParamSumResult *pPSR = ModData->ModelResult.GetPSResult(m_vParam);

	ItSpecRes it;
	for(it=ModData->ModelResult.CalibResult.begin(); it!=ModData->ModelResult.CalibResult.end(); ++it)
	{
		CString SpName = it->Name;
		SpectrumResult* pSpR = ModData->ModelResult.GetCalibResult(SpName);
		SpectrumParamResult* pSpPR = &pSpR->SpecParamData[m_vParam];

		int ind = pGI->GetIndByName(SpName);
		if(ind >= 0 && ind < pGI->N)
		{
			if(fabs(pSpPR->SECV_RefPred) > pPSR->SECV * m_vSECV)
				pMarkSECV[ind].nColor = cColorErr;
			else
				pMarkSECV[ind].nColor = cColorGraphNorm;

			if(find(selC.begin(), selC.end(), SpName) != selC.end())
				pMarkSECV[ind].nColor = cColorSel;
		}
	}
}

void CModelResultDlg::ColoredGraphSEV()
{
	GraphInfo* pGI = ModData->ModelResult.GetGraphSEV();
	if(pGraphSEV == NULL || pMarkSEV == NULL || pGI == NULL)
		return;

	ParamSumResult *pPSR = ModData->ModelResult.GetPSResult(m_vParam);

	ItSpecValRes it;
	for(it=ModData->ModelResult.ValidResult.begin(); it!=ModData->ModelResult.ValidResult.end(); ++it)
	{
		CString SpName = it->Name;
		SpectrumValResult* pSpVR = ModData->ModelResult.GetValidResult(SpName);
		SpectrumValParamResult* pSpVPR = &pSpVR->SpecValParamData[m_vParam];

		int ind = ModData->ModelResult.GetGraphSEV()->GetIndByName(SpName);
		if(ind >= 0 && ind < ModData->ModelResult.GetGraphSEV()->N)
		{
			if(fabs(pSpVPR->RefPred) > pPSR->SEV * m_vSEV)
				pMarkSEV[ind].nColor = cColorErr;
			else
				pMarkSEV[ind].nColor = cColorGraphNorm;

			if(find(selV.begin(), selV.end(), SpName) != selV.end())
				pMarkSEV[ind].nColor = cColorSel;
		}
	}
}

void CModelResultDlg::ColoredHistoCalib()
{
	if(pHistoCalib == NULL || pMarkHCalib == NULL)
		return;

	ItSpecRes it;
	for(it=ModData->ModelResult.CalibResult.begin(); it!=ModData->ModelResult.CalibResult.end(); ++it)
	{
		CString SpName = it->Name;
		SpectrumResult* pSpR = ModData->ModelResult.GetCalibResult(SpName);

		int ind = ModData->ModelResult.GetHistoCalib()->GetIndByName(SpName);
		if(ind >= 0 && ind < ModData->ModelResult.GetHistoCalib()->N)
		{
			if(pSpR->Out_Mah > m_vMaxMah)
				pMarkHCalib[ind].nColor = cColorErr;
			else
				pMarkHCalib[ind].nColor = cColorPass;

			if(find(selC.begin(), selC.end(), SpName) != selC.end())
				pMarkHCalib[ind].nColor = cColorSel;
		}
	}
}

void CModelResultDlg::ColoredHistoValid()
{
	if(pHistoValid == NULL || pMarkHValid == NULL)
		return;

	ItSpecValRes it;
	for(it=ModData->ModelResult.ValidResult.begin(); it!=ModData->ModelResult.ValidResult.end(); ++it)
	{
		CString SpName = it->Name;
		SpectrumValResult* pSpVR = ModData->ModelResult.GetValidResult(SpName);

		int ind = ModData->ModelResult.GetHistoValid()->GetIndByName(SpName);
		if(ind >= 0 && ind < ModData->ModelResult.GetHistoValid()->N)
		{
			if(pSpVR->Out_Mah > m_vMaxMah)
				pMarkHValid[ind].nColor = cColorErr;
			else
				pMarkHValid[ind].nColor = cColorPass;

			if(find(selV.begin(), selV.end(), SpName) != selV.end())
				pMarkHValid[ind].nColor = cColorSel;
		}
	}
}

void CModelResultDlg::ColoredHistoBad()
{
	if(pHistoBad == NULL || pMarkHBad == NULL)
		return;

	ParamSumResult *pPSR = ModData->ModelResult.GetPSResult(m_vParam);

	ItSpecRes it;
	for(it=ModData->ModelResult.CalibResult.begin(); it!=ModData->ModelResult.CalibResult.end(); ++it)
	{
		CString SpName = it->Name;
		SpectrumResult* pSpR = ModData->ModelResult.GetCalibResult(SpName);
		SpectrumParamResult* pSpPR = &pSpR->SpecParamData[m_vParam];

		int ind = ModData->ModelResult.GetHistoBad()->GetIndByName(SpName);
		if(ind >= 0 && ind < ModData->ModelResult.GetHistoBad()->N)
		{
			if(pSpPR->Bad_Remainder > pPSR->norm)
				pMarkHBad[ind].nColor = cColorErr;
			else
				pMarkHBad[ind].nColor = cColorPass;

			if(find(selC.begin(), selC.end(), SpName) != selC.end())
				pMarkHBad[ind].nColor = cColorSel;
		}
	}
}

void CModelResultDlg::ColoredGraphScores()
{
	GraphInfo* pGI = ModData->ModelResult.GetGraphScores();
	if(pGraphScores == NULL || pMarkScores == NULL || pGI == NULL)
		return;

	ItSpecRes it;
	for(it=ModData->ModelResult.CalibResult.begin(); it!=ModData->ModelResult.CalibResult.end(); ++it)
	{
		CString SpName = it->Name;

		int ind = pGI->GetIndByName(SpName);
		if(ind >= 0 && ind < pGI->N)
		{
			if(find(selC.begin(), selC.end(), SpName) != selC.end())
				pMarkScores[ind].nColor = cColorSel;
			else
				pMarkScores[ind].nColor = cColorGraphNorm;
		}
	}
}

void CModelResultDlg::ColoredGraphChemLoad()
{
	GraphInfo* pGI = ModData->ModelResult.GetGraphChemLoad();
	if(pGraphChemLoad == NULL || pMarkChemLoad == NULL || pGI == NULL)
		return;

	int ind;
	for(ind=0; ind<pGI->N; ind++)
		pMarkChemLoad[ind].nColor = cGetDefColor(ind);
}

void CModelResultDlg::ColoredAllGraphs()
{
	ColoredGraphSEC();
	ColoredGraphSECV();
	ColoredGraphSEV();
	ColoredHistoCalib();
	ColoredHistoValid();
	ColoredHistoBad();
	ColoredGraphScores();
	ColoredGraphChemLoad();
}

void CModelResultDlg::SelShowGraphs(int graph)
{
	if(pHistoSamCalib)
	{
		pChart->ShowGraph(pHistoSamCalib->GetPos(), graph == C_PGRAPH_SAMCALIB);
		pChart->EnableGraphText(pHistoSamCalib->GetPos(), true);
	}

	if(pHistoSamValid)
	{
		pChart->ShowGraph(pHistoSamValid->GetPos(), graph == C_PGRAPH_SAMVALID);
		pChart->EnableGraphText(pHistoSamValid->GetPos(), true);
	}

	pChart->ShowGraph(pGraphSEC->GetPos(), graph == C_PGRAPH_SEC);
	pChart->ShowGraph(pGraphLineSEC->GetPos(), graph == C_PGRAPH_SEC && show_trend);

	if(pGraphSECV != NULL)
	{
		pChart->ShowGraph(pGraphSECV->GetPos(), graph == C_PGRAPH_SECV);
		pChart->ShowGraph(pGraphLineSECV->GetPos(), graph == C_PGRAPH_SECV && show_trend);
	}

	if(pGraphSEV != NULL)
	{
		pChart->ShowGraph(pGraphSEV->GetPos(), graph == C_PGRAPH_SEV);
		pChart->ShowGraph(pGraphLineSEV->GetPos(), graph == C_PGRAPH_SEV && show_trend && ModData->ModelResult.GetNVal() > 1);
	}

	pChart->ShowGraph(pGraphLine0->GetPos(), (graph == C_PGRAPH_SEC || graph == C_PGRAPH_SECV || graph == C_PGRAPH_SEV) && show_teor);
	if(posLegendSEC_V != NULL)
	{
		pChart->GetUserLegend(posLegendSEC_V)->Enable((graph == C_PGRAPH_SEC || graph == C_PGRAPH_SECV || graph == C_PGRAPH_SEV) && IsLegendEnable() && show_legend);
	}

	if(pHistoCalib)
	{
		pChart->ShowGraph(pHistoCalib->GetPos(), graph == C_PGRAPH_MDCALIB);
	}

	if(pHistoValid)
		pChart->ShowGraph(pHistoValid->GetPos(), graph == C_PGRAPH_MDVALID && pHIValid->N > 1);
	pChart->ShowGraph(pHistoMDL->GetPos(), (graph == C_PGRAPH_MDCALIB || graph == C_PGRAPH_MDVALID) && show_mdl);
	if(posLegendOut != NULL)
		pChart->GetUserLegend(posLegendOut)->Enable((graph == C_PGRAPH_MDCALIB || graph == C_PGRAPH_MDVALID)
			&& show_mdl && show_legend);

	pChart->ShowGraph(pHistoBad->GetPos(), graph == C_PGRAPH_BAD);
	pChart->ShowGraph(pHistoNorm->GetPos(), graph == C_PGRAPH_BAD && show_limit);
	if(posLegendBad != NULL)
		pChart->GetUserLegend(posLegendBad)->Enable(graph == C_PGRAPH_BAD && show_limit && show_legend);

	pChart->ShowGraph(pGraphScores->GetPos(), graph == C_PGRAPH_SCORES);

	pChart->ShowGraph(pGraphChemLoad->GetPos(), graph == C_PGRAPH_CHLOAD);
	if(posLegendChem != NULL)
		pChart->GetUserLegend(posLegendChem)->Enable(graph == C_PGRAPH_CHLOAD && IsLegendEnable() && show_legend);

	if(graph == C_PGRAPH_SPLOAD)
	{
		VDblDbl* Masks = ModData->ModelResult.GetAllMasksSpecLoad();
		ItVDblDbl it4;
		for(it4=Masks->begin(); it4!=Masks->end(); ++it4)
		{
			POSITION Pos = pChart->AddMask((*it4).first, (*it4).second, false, true, cColorPass);
			MasksSLPos.push_back(Pos);
		}
	}
	else
	{
		vector<POSITION>::iterator it3;
		for(it3=MasksSLPos.begin(); it3!=MasksSLPos.end(); ++it3)
			pChart->RemoveMask(*it3);
		MasksSLPos.clear();
	}

	int j;
	vector<CLumChartDataSet>::iterator it;
	for(j=0, it=GraphsSpecLoad.begin(); it!=GraphsSpecLoad.end(); ++it, j++)
	{
		GraphInfo* pGI = ModData->ModelResult.GetGraphSpecLoad(j);
		pChart->ShowGraph(it->GetPos(), (graph == C_PGRAPH_SPLOAD && pGI->sel));
	}
	vector<POSITION>::iterator it3;
	for(it3=MasksSLPos.begin(); it3!=MasksSLPos.end(); ++it3)
		pChart->ShowGraph((*it3), graph == C_PGRAPH_SPLOAD);
	if(posLegendSpec != NULL)
		pChart->GetUserLegend(posLegendSpec)->Enable(graph == C_PGRAPH_SPLOAD && IsLegendEnable() && show_legend);

	GraphInfo* pGI = NULL;
	switch(graph)
	{
	case C_PGRAPH_SAMCALIB:  
		pGI = ModData->ModelResult.GetHistoSamCalib();  
		break;
	case C_PGRAPH_SAMVALID:  
		pGI = ModData->ModelResult.GetHistoSamValid();  
		break;
	case C_PGRAPH_SEC:  
		pGI = ModData->ModelResult.GetGraphSEC();  
		break;
	case C_PGRAPH_SECV:  
		pGI = ModData->ModelResult.GetGraphSECV();  
		break;
	case C_PGRAPH_SEV:  
		pGI = ModData->ModelResult.GetGraphSEV();  
		break;
	case C_PGRAPH_MDCALIB:  
		pGI = ModData->ModelResult.GetHistoCalib();  
		break;
	case C_PGRAPH_MDVALID:
		if(pHIValid->N > 1)  pGI = ModData->ModelResult.GetHistoValid();
		break;
	case C_PGRAPH_BAD:  
		pGI = ModData->ModelResult.GetHistoBad();  
		break;
	case C_PGRAPH_SCORES:  
		pGI = ModData->ModelResult.GetGraphScores();  
		break;
	case C_PGRAPH_CHLOAD:  
		pGI = ModData->ModelResult.GetGraphChemLoad();  
		break;
	}

	if(pGI != NULL || graph == C_PGRAPH_SPLOAD)
	{
		double min, max, plus, Ymax, minX, minY, maxX, maxY, plusY;
		vector<CLumChartDataSet>::iterator it;
		switch(graph)
		{
		case C_PGRAPH_SEC:
		case C_PGRAPH_SECV:
		case C_PGRAPH_SEV:
		case C_PGRAPH_SCORES:
		case C_PGRAPH_CHLOAD:
			min = min(pGI->xmin, pGI->ymin);
			max = max(pGI->xmax, pGI->ymax);
			plus = (max - min) * 0.05;
			if(fabs(plus) < 1.e-9)  plus = max * 0.1;
			pChart->SetUserViewPort(min - plus, min - plus, max + plus, max + plus);
			break;

		case C_PGRAPH_SAMCALIB:
		case C_PGRAPH_SAMVALID:
			pChart->SetUserViewPort(pGI->xmin, 0., pGI->xmax, pGI->ymax);
			break;

		case C_PGRAPH_MDCALIB:
		case C_PGRAPH_MDVALID:
			Ymax = max(GetMaxMah(), pGI->ymax);
			pChart->SetUserViewPort(1, 0, pGI->N, Ymax * 1.1);
			break;
		case C_PGRAPH_BAD:
			Ymax = max(GetNorm(), pGI->ymax);
			pChart->SetUserViewPort(1, 0, pGI->N, Ymax * 1.1);
			break;
		case C_PGRAPH_SPLOAD:
			minX = 1.e9;  maxX = -1.e9;  minY = 1.e9;  maxY = -1.e9;
			for(j=0, it=GraphsSpecLoad.begin(); it!=GraphsSpecLoad.end(); ++it, j++)
			{
				pGI = ModData->ModelResult.GetGraphSpecLoad(j);
				if(!pGI->sel)
					continue;

				minX = min(minX, pGI->xmin);
				maxX = max(maxX, pGI->xmax);
				minY = min(minY, pGI->ymin);
				maxY = max(maxY, pGI->ymax);
			}
			plusY = (maxY - minY) * 0.05;
			if(fabs(plusY) < 1.e-9)  plusY = maxY * 0.1;

			pChart->SetUserViewPort(minX, minY - plusY, maxX, maxY + plusY);
		}
	}

	CString ox = cEmpty, oy = cEmpty;
	CString fmt = ParcelLoadString(STRID_MODEL_RESULT_GRAPH_AXISGK);
	switch(graph)
	{
	case C_PGRAPH_SAMCALIB:
	case C_PGRAPH_SAMVALID:
		ox = ParcelLoadString(STRID_MODEL_RESULT_GRAPH_AXISSAMREF);
		oy = ParcelLoadString(STRID_MODEL_RESULT_GRAPH_AXISNSAM);
		break;
	case C_PGRAPH_SEC:
	case C_PGRAPH_SECV:
	case C_PGRAPH_SEV:
		ox = ParcelLoadString(STRID_MODEL_RESULT_GRAPH_AXISREF);
		oy = ParcelLoadString(STRID_MODEL_RESULT_GRAPH_AXISPRED);
		break;
	case C_PGRAPH_MDCALIB:
	case C_PGRAPH_MDVALID:
		ox = ParcelLoadString(STRID_MODEL_RESULT_GRAPH_AXISSMPL);
		oy = ParcelLoadString(STRID_MODEL_RESULT_GRAPH_AXISMDL);
		break;
	case C_PGRAPH_BAD:
		ox = ParcelLoadString(STRID_MODEL_RESULT_GRAPH_AXISSMPL);
		oy = ParcelLoadString(STRID_MODEL_RESULT_GRAPH_AXISREST);
		break;
	case C_PGRAPH_SCORES:
		ox.Format(fmt, m_vOXsc + 1);
		oy.Format(fmt, m_vOYsc + 1);
		break;
	case C_PGRAPH_SPLOAD:
		ox = ParcelLoadString(STRID_MODEL_RESULT_GRAPH_AXISPNT);
		oy = ParcelLoadString(STRID_MODEL_RESULT_GRAPH_AXISSKIP);
		break;
	case C_PGRAPH_CHLOAD:
		ox.Format(fmt, m_vOXch + 1);
		oy.Format(fmt, m_vOYch + 1);
		break;
	}
	pChart->SetAxesNames(ox, oy);
	
	pChart->EnableSolidAxisHorz(graph == C_PGRAPH_SCORES || graph == C_PGRAPH_SPLOAD || graph == C_PGRAPH_CHLOAD);
	pChart->EnableSolidAxisVert(graph == C_PGRAPH_SCORES || graph == C_PGRAPH_CHLOAD);
	pChart->EmptyZoomList();
}

void CModelResultDlg::MakeGraphPicture(CString FileName, int graph)
{
	int graph_old;
	switch(m_vTab)
	{
	case C_MODRES_PAGE_SEC:  
		graph_old = C_PGRAPH_SEC;  
		break;
	case C_MODRES_PAGE_SECV:  
		graph_old = C_PGRAPH_SECV;  
		break;
	case C_MODRES_PAGE_SEV:  
		graph_old = C_PGRAPH_SEV;  
		break;
	case C_MODRES_PAGE_OUT:
		if(m_vCalibValid == C_MODRES_SMPL_CALIB)  
			graph_old = C_PGRAPH_MDCALIB;
		else  
			graph_old = C_PGRAPH_MDVALID;
		break;
	case C_MODRES_PAGE_BAD:  
		graph_old = C_PGRAPH_BAD;  
		break;
	case C_MODRES_PAGE_SCORES:  
		graph_old = C_PGRAPH_SCORES;  
		break;
	case C_MODRES_PAGE_SPECLOAD:  
		graph_old = C_PGRAPH_SPLOAD;  
		break;
	case C_MODRES_PAGE_CHEMLOAD:  
		graph_old = C_PGRAPH_CHLOAD;  
		break;
	}

	SelShowGraphs(graph);
	pChart->ExportPicture(FileName);

	SelShowGraphs(graph_old);
}

void CModelResultDlg::SelectByRect(CRect* rect, bool isFrame)
{
	if(!isFrame)
	{
		CPoint pnt(int((rect->left + rect->right) / 2), int((rect->top + rect->bottom) / 2));
		int resfind = -1;
		pChart->IsPointInSelRect(pnt, &resfind);
		if(resfind == 1)
			return;
	}

	bool isValid = GetCurCalibValid();
	GraphInfo* pGI = NULL;
	CLumChartDataSet* pGD = NULL;
	VStr* pSel = NULL;

	switch(m_vTab)
	{
	case C_MODRES_PAGE_SEC:
		pGI = ModData->ModelResult.GetGraphSEC();
		pGD = pGraphSEC;
		pSel = &selC;
		break;
	case C_MODRES_PAGE_SECV:
		pGI = ModData->ModelResult.GetGraphSECV();
		pGD = pGraphSECV;
		pSel = &selC;
		break;
	case C_MODRES_PAGE_SEV:
		pGI = ModData->ModelResult.GetGraphSEV();
		pGD = pGraphSEV;
		pSel = &selV;
		break;
	case C_MODRES_PAGE_OUT:
		if(m_vCalibValid == C_MODRES_SMPL_CALIB)
		{
			pGI = ModData->ModelResult.GetHistoCalib();
			pGD = pHistoCalib;
			pSel = &selC;
		}
		else
		{
			pGI = ModData->ModelResult.GetHistoValid();
			pGD = pHistoValid;
			pSel = &selV;
		}
		break;
	case C_MODRES_PAGE_BAD:
		pGI = ModData->ModelResult.GetHistoBad();
		pGD = pHistoBad;
		pSel = &selC;
		break;
	case C_MODRES_PAGE_SCORES:
		pGI = ModData->ModelResult.GetGraphScores();
		pGD = pGraphScores;
		pSel = &selC;
		break;
	case C_MODRES_PAGE_SPECLOAD:
		ShowGK(rect);
		return;
	case C_MODRES_PAGE_CHEMLOAD:
		pGI = ModData->ModelResult.GetGraphChemLoad();
		pGD = pGraphChemLoad;
		pSel = NULL;
		break;
	default:
		return;
	}

	if(pGD == NULL || pGI == NULL)
		return;

	int N = pChart->LocateGraphPoints(pGD->GetPos(), (*rect), 0);
	if(N > 0)
	{
		int* pidx = new int[N];
		pChart->LocateGraphPoints(pGD->GetPos(), (*rect), pidx);

		int i;
		if(pSel != NULL)
		{
			bool make_sel = false;
			for(i=0; i<N; i++)
			{
				CString SpName = pGI->GetNameByInd(pidx[i]);
				ItStr itf = find(pSel->begin(), pSel->end(), SpName);
				if(itf == pSel->end())
				{
					make_sel = true;
					break;
				}
			}
			for(i=0; i<N; i++)
			{
				CString SpName = pGI->GetNameByInd(pidx[i]);
				ItStr itf = find(pSel->begin(), pSel->end(), SpName);
				if(itf != pSel->end() && !make_sel)
					pSel->erase(itf);
				if(itf == pSel->end() && make_sel)
					pSel->push_back(SpName);
			}
		}

		if(!isFrame)
		{
			vector<CLCULString> strings;
			for(i=0; i<N; i++)
			{
				CLCULString NewString;
				CString tmpf;

				CString SpName = pGI->GetNameByInd(pidx[i]);

				ParamSumResult *pPSR = ModData->ModelResult.GetPSResult(SpName); // ��� ChemLoad

				CProjectData *ParentPrj = GetProjectByHItem(ModData->GetParentHItem());
				CString fmt = cEmpty, fmt2 = cEmpty, str = cEmpty;
				SpectrumResult* pSpR = (isValid || pSel == NULL) ? NULL : ModData->ModelResult.GetCalibResult(SpName);
				SpectrumParamResult *pSpPR = (isValid || pSel == NULL) ? NULL : &pSpR->SpecParamData[m_vParam];
				SpectrumValResult* pSpVR = (isValid && pSel != NULL) ? ModData->ModelResult.GetValidResult(SpName) : NULL;
				SpectrumValParamResult *pSpVPR = (isValid && pSel != NULL) ? &pSpVR->SpecValParamData[m_vParam] : NULL;
				switch(m_vTab)
				{
				case C_MODRES_PAGE_SEC:
					fmt = ParentPrj->GetParam(ModData->GetModelParam(m_vParam)->ParamName)->GetFormat();
					fmt2.Format(ParcelLoadString(STRID_MODEL_RESULT_GRAPH_INFO_XY), fmt, fmt);
					tmpf.Format(fmt2, pSpPR->SEC_Prediction, pSpPR->SEC_Reference);
					str = SpName + tmpf;
					break;
				case C_MODRES_PAGE_SECV:
					fmt = ParentPrj->GetParam(ModData->GetModelParam(m_vParam)->ParamName)->GetFormat();
					fmt2.Format(ParcelLoadString(STRID_MODEL_RESULT_GRAPH_INFO_XY), fmt, fmt);
					tmpf.Format(fmt2, pSpPR->SECV_Prediction, pSpPR->SECV_Reference);
					str = SpName + tmpf;
					break;
				case C_MODRES_PAGE_SEV:
					fmt = ParentPrj->GetParam(ModData->GetModelParam(m_vParam)->ParamName)->GetFormat();
					fmt2.Format(ParcelLoadString(STRID_MODEL_RESULT_GRAPH_INFO_XY), fmt, fmt);
					tmpf.Format(fmt2, pSpVPR->Corrected, pSpVPR->Reference);
					str = SpName + tmpf;
					break;
				case C_MODRES_PAGE_OUT:
					fmt = cFmt42;
					fmt2.Format(ParcelLoadString(STRID_MODEL_RESULT_GRAPH_INFO_Y), fmt);
					if(isValid)
						tmpf.Format(fmt2, pSpVR->Out_Mah);
					else
						tmpf.Format(fmt2, pSpR->Out_Mah);
					str = SpName + tmpf;
					break;
				case C_MODRES_PAGE_BAD:
					fmt = cFmt42;
					fmt2.Format(ParcelLoadString(STRID_MODEL_RESULT_GRAPH_INFO_Y), fmt);
					tmpf.Format(fmt2, pSpPR->Bad_Remainder);
					str = SpName + tmpf;
					break;
				case C_MODRES_PAGE_SCORES:
					fmt = cFmt86;
					fmt2.Format(ParcelLoadString(STRID_MODEL_RESULT_GRAPH_INFO_XY), fmt, fmt);
					tmpf.Format(fmt2, pSpR->Scores_GK[m_vOYsc], pSpR->Scores_GK[m_vOXsc]);
					str = SpName + tmpf;
					break;
				case C_MODRES_PAGE_CHEMLOAD:
					fmt = cFmt86;
					fmt2.Format(ParcelLoadString(STRID_MODEL_RESULT_GRAPH_INFO_XY), fmt, fmt);
					tmpf.Format(fmt2, pPSR->Chem_GK[m_vOYch], pPSR->Chem_GK[m_vOXch]);
					str = SpName + tmpf;
					NewString.SetMarker(CLumChartDataMarker(LCF_MARKERS_ROUND, 4, cGetDefColor(pidx[i])));
					NewString.EnableMarker(true);
					break;
				}

				NewString.SetText(str);
				strings.push_back(NewString);
			}

			CLCUL legend;
			legend.MoveTo(0, 0);
			legend.Resize(100, 20);
			legend.SetStrings(strings);

			legend.SetFillColor(cColorGraphNorm);
			legend.SetTextColor(cColorBlack);

			legend.Enable(true);
			legend.EnableMoveToFit(true);
			legend.EnableAutoWidth(true);
			legend.EnableAutoHeight(true);

			posInfo = pChart->AddUserLegend(legend);
		}

		ColoredAllGraphs();
		pChart->RedrawWindow();

		delete pidx;
	}
}

void CModelResultDlg::ShowGK(CRect* rect)
{
	vector<CLCULString> strings;

	int i;
	bool find = false;
	vector<CLumChartDataSet>::iterator it;
	for(i=0, it=GraphsSpecLoad.begin(); it!=GraphsSpecLoad.end(); ++it, i++)
	{
		bool res = false;
		pChart->IsGraphInRect(it->GetPos(), (*rect), &res);
		if(!res)
			continue;

		find = true;

		CString Name;
		Name.Format(ParcelLoadString(STRID_MODEL_RESULT_GRAPH_SPLOADNAME), i + 1);

		CLCULString NewString;
		NewString.SetText(Name);
		NewString.SetLine(cGetDefColor(i), PS_SOLID, 1);
		NewString.EnableLine(true);

		strings.push_back(NewString);
	}
	if(!find)
		return;

	CLCUL legend;
	legend.MoveTo(0, 0);
	legend.Resize(100, 20);
	legend.SetStrings(strings);

	legend.SetFillColor(cColorGraphNorm);
	legend.SetTextColor(cColorBlack);

	legend.Enable(true);
	legend.EnableMoveToFit(true);
	legend.EnableAutoWidth(true);
	legend.EnableAutoHeight(true);

	posInfo = pChart->AddUserLegend(legend);

	pChart->RedrawWindow();
}

void CModelResultDlg::ShowCoord(CPoint* pnt)
{
	if(m_vTab != C_MODRES_PAGE_SPECLOAD)
		return;

	vector<CLCULString> strings;

	CLCULString NewString;

	CString str;
	double X = 0, Y = 0;
	pChart->GetViewPortXY(*pnt, &X, &Y);
	str.Format(ParcelLoadString(STRID_MODEL_RESULT_GRAPH_INFO_COORD), Y, X);
	NewString.SetText(str);
	strings.push_back(NewString);

	CRect RectGraph;
	GetDlgItem(IDC_MODEL_RESULT_TABLEBIG)->GetWindowRect(RectGraph);

	CLCUL legend;
	legend.MoveTo(0, RectGraph.Height() - 20);
	legend.Resize(100, 20);
	legend.SetStrings(strings);

	legend.SetFillColor(cColorGraphNorm);
	legend.SetTextColor(cColorBlack);

	legend.Enable(true);
	legend.EnableMoveToFit(true);
	legend.EnableAutoWidth(true);
	legend.EnableAutoHeight(true);

	posCoord = pChart->AddUserLegend(legend);
}

void CModelResultDlg::RemoveInfo()
{
	if(posInfo != NULL)
	{
		pChart->RemoveUserLegend(posInfo);
		posInfo = NULL;
	}
	if(posCoord != NULL)
	{
		pChart->RemoveUserLegend(posCoord);
		posCoord = NULL;
	}
}

bool CModelResultDlg::IsLegendEnable()
{
	bool res = false;
	switch(m_vTab)
	{
	case C_MODRES_PAGE_SEC:
		res = (show_trend || show_teor);
		break;
	case C_MODRES_PAGE_SECV:
		res = ((show_trend || show_teor) && ModData->IsUseSECV());
		break;
	case C_MODRES_PAGE_SEV:
		pGISEV = ModData->ModelResult.GetGraphSEV();
		res = ((show_trend || show_teor) && pGISEV->N > 0);
		break;
	case C_MODRES_PAGE_OUT:
		res = (show_mdl);
		break;
	case C_MODRES_PAGE_BAD:
		res = (show_limit);
		break;
	case C_MODRES_PAGE_SCORES:
		res = false;
		break;
	case C_MODRES_PAGE_SPECLOAD:
		res = (GetNGKShow() > 0);
		break;
	case C_MODRES_PAGE_CHEMLOAD:
		res = true;
		break;
	}

	return res;
}

int CModelResultDlg::GetNGKShow()
{
	int j, N=0;
	vector<CLumChartDataSet>::iterator it;
	for(j=0, it=GraphsSpecLoad.begin(); it!=GraphsSpecLoad.end(); ++it, j++)
	{
		GraphInfo* pGI = ModData->ModelResult.GetGraphSpecLoad(j);
		if(pGI->sel)  N++;
	}

	return N;
}

void CModelResultDlg::BuildUserPopupMenu()
{
	pMenu1->AppendMenu(MF_STRING, C_MODRES_MENU_PREV, ParcelLoadString(STRID_MODEL_RESULT_GRAPH_MENU_PREV));
	pMenu1->AppendMenu(MF_STRING, C_MODRES_MENU_NEXT, ParcelLoadString(STRID_MODEL_RESULT_GRAPH_MENU_NEXT));
	pMenu1->AppendMenu(MF_SEPARATOR);
	pMenu1->AppendMenu(MF_STRING, C_MODRES_MENU_FULL, ParcelLoadString(STRID_MODEL_RESULT_GRAPH_MENU_FULL));
	pMenu1->AppendMenu(MF_STRING, C_MODRES_MENU_AUTO, ParcelLoadString(STRID_MODEL_RESULT_GRAPH_MENU_AUTO));
	pMenu1->AppendMenu(MF_SEPARATOR);
	pMenu1->AppendMenu(MF_STRING, C_MODRES_MENU_LEFT, ParcelLoadString(STRID_MODEL_RESULT_GRAPH_MENU_LEFT));
	pMenu1->AppendMenu(MF_STRING, C_MODRES_MENU_RIGHT, ParcelLoadString(STRID_MODEL_RESULT_GRAPH_MENU_RIGHT));
	pMenu1->AppendMenu(MF_SEPARATOR);
	pMenu1->AppendMenu(MF_STRING, C_MODRES_MENU_LEGEND, ParcelLoadString(STRID_MODEL_RESULT_GRAPH_MENU_LEGEND));
	pMenu1->AppendMenu(MF_STRING, C_MODRES_MENU_GRID, ParcelLoadString(STRID_MODEL_RESULT_GRAPH_MENU_GRID));
	pMenu1->AppendMenu(MF_SEPARATOR);
	pMenu1->AppendMenu(MF_STRING, C_MODRES_MENU_TREND, ParcelLoadString(STRID_MODEL_RESULT_GRAPH_MENU_TREND));
	pMenu1->AppendMenu(MF_STRING, C_MODRES_MENU_TEOR, ParcelLoadString(STRID_MODEL_RESULT_GRAPH_MENU_TEOR));
	pMenu1->AppendMenu(MF_STRING, C_MODRES_MENU_MDL, ParcelLoadString(STRID_MODEL_RESULT_GRAPH_MENU_MDL));
	pMenu1->AppendMenu(MF_STRING, C_MODRES_MENU_LIMIT, ParcelLoadString(STRID_MODEL_RESULT_GRAPH_MENU_LIMIT));
}

void CModelResultDlg::SetStatusMenu()
{
	UINT nFlagsE = MF_ENABLED | MF_BYCOMMAND;
	UINT nFlagsD = MF_DISABLED | MF_GRAYED | MF_BYCOMMAND;
	UINT nFlagsC = MF_CHECKED;
	UINT nFlagsU = MF_UNCHECKED;
	UINT nFlags;

	bool btmp = false, btmp2 = false;

	pChart->IsGoBackAvailable(&btmp);
	nFlags = (btmp) ? nFlagsE : nFlagsD;
	pMenu1->EnableMenuItem(C_MODRES_MENU_PREV, nFlags);

	pChart->IsGoForwardAvailable(&btmp);
	nFlags = (btmp) ? nFlagsE : nFlagsD;
	pMenu1->EnableMenuItem(C_MODRES_MENU_NEXT, nFlags);

	btmp2 = IsLegendEnable();
	nFlags = (show_legend && btmp2) ? nFlagsC : nFlagsU;
	pMenu1->CheckMenuItem(C_MODRES_MENU_LEGEND, nFlags);
	nFlags = (btmp2) ? nFlagsE : nFlagsD;
	pMenu1->EnableMenuItem(C_MODRES_MENU_LEGEND, nFlags);

	pChart->IsGridEnabled(&btmp, &btmp2);
	nFlags = (btmp) ? nFlagsC : nFlagsU;
	pMenu1->CheckMenuItem(C_MODRES_MENU_GRID, nFlags);

	pGISEV = ModData->ModelResult.GetGraphSEV();
	btmp2 = (m_vTab == C_MODRES_PAGE_SEC ||
		(m_vTab == C_MODRES_PAGE_SECV && ModData->IsUseSECV()) ||
		(m_vTab == C_MODRES_PAGE_SEV && pGISEV != NULL && pGISEV->N > 0 && ModData->ModelResult.GetNVal() > 1));
	nFlags = (show_trend && btmp2) ? nFlagsC : nFlagsU;
	pMenu1->CheckMenuItem(C_MODRES_MENU_TREND, nFlags);
	nFlags = (btmp2) ? nFlagsE : nFlagsD;
	pMenu1->EnableMenuItem(C_MODRES_MENU_TREND, nFlags);

	btmp2 = (m_vTab == C_MODRES_PAGE_SEC ||
		(m_vTab == C_MODRES_PAGE_SECV && ModData->IsUseSECV()) ||
		(m_vTab == C_MODRES_PAGE_SEV && pGISEV != NULL && pGISEV->N > 0));
	nFlags = (show_teor && btmp2) ? nFlagsC : nFlagsU;
	pMenu1->CheckMenuItem(C_MODRES_MENU_TEOR, nFlags);
	nFlags = (btmp2) ? nFlagsE : nFlagsD;
	pMenu1->EnableMenuItem(C_MODRES_MENU_TEOR, nFlags);

	btmp2 = (m_vTab == C_MODRES_PAGE_OUT);
	nFlags = (show_mdl) ? nFlagsC : nFlagsU;
	pMenu1->CheckMenuItem(C_MODRES_MENU_MDL, nFlags);
	nFlags = (btmp2) ? nFlagsE : nFlagsD;
	pMenu1->EnableMenuItem(C_MODRES_MENU_MDL, nFlags);

	btmp2 = (m_vTab == C_MODRES_PAGE_BAD);
	nFlags = (show_limit) ? nFlagsC : nFlagsU;
	pMenu1->CheckMenuItem(C_MODRES_MENU_LIMIT, nFlags);
	nFlags = (btmp2) ? nFlagsE : nFlagsD;
	pMenu1->EnableMenuItem(C_MODRES_MENU_LIMIT, nFlags);
}

void CModelResultDlg::OnHelp()
{
	int IdHelp = DLGID_MODEL_RESULT_QNT;

	pFrame->ShowHelp(IdHelp);
}
