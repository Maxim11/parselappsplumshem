// ParcelDoc.cpp : implementation of the CParcelDoc class
//

#include "stdafx.h"
#include "Parcel.h"

#include "ParcelDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CParcelDoc

IMPLEMENT_DYNCREATE(CParcelDoc, CDocument)

BEGIN_MESSAGE_MAP(CParcelDoc, CDocument)
END_MESSAGE_MAP()


// CParcelDoc construction/destruction

CParcelDoc::CParcelDoc()
{
	// TODO: add one-time construction code here

}

CParcelDoc::~CParcelDoc()
{
}

BOOL CParcelDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}




// CParcelDoc serialization

void CParcelDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}


// CParcelDoc diagnostics

#ifdef _DEBUG
void CParcelDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CParcelDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CParcelDoc commands
