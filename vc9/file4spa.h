////////////////////////////////////////////////////////////////////////////
//	Author      : Ignatyev Gennady 2008
//	Email       : i-gen@yandex.ru
//	Module      : file4spa.h
//	Description : ������������ ������������ ������ � ����
////////////////////////////////////////////////////////////////////////////
#ifndef _LUMEX_SPADATA_SERIALIZATION_INCLUDED_H__
#define _LUMEX_SPADATA_SERIALIZATION_INCLUDED_H__

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include <vector>

namespace file4spa
{


/// ������ ������������ ������	
typedef std::vector<double>	SpaData;


/** 
* ���������� ������������ ������ � ����
* ���������:
*	- path: ������ ���� � ����� ��� ��� �������� � ������ � ���� ������������ ������
*	- spadata: ������ ������������ ������, ������������ � ����
* ������������ ��������:
*	- true: ���� � ������� ������� ������
*	- false: ������ ��� �������� �����
*/
bool SaveSpaDataAtFile( LPCTSTR /*IN*/ path, const SpaData& /*IN*/ spadata );



/** 
* �������������� ������������ ������ �� �����
* ���������:
*	- path: ������ ���� � ����� ��� ������ �� ���� ������������ ������
*	- spadata: ������ ������������ ������ (��������������� ���������� ����� ��� ����������)
* ������������ ��������:
*	- true: ���� � ������� ������� �������� � ������ ���������� � spadata
*	- false: ������ ��� ������ �����
*/
bool ReadSpaDataFromFile( LPCTSTR /*IN*/ path, SpaData& /*OUT*/ spadata );



} // namespace file4spa

#endif // _LUMEX_SPADATA_SERIALIZATION_INCLUDED_H__
