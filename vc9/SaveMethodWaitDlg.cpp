#include "stdafx.h"

#include "SaveMethodWaitDlg.h"
#include "BranchInfo.h"

IMPLEMENT_DYNAMIC(CSaveMethodWaitDlg, CWaitingDlg)

CSaveMethodWaitDlg::CSaveMethodWaitDlg(CProjectData* prj, mtddb::PRMethodQnt* qnt, mtddb::PRMethodQlt* qlt, CString fname, CWnd* pParent)
	: CWaitingDlg(pParent)
{
	FileName = fname;
	PrjData = prj;
	pQNT = qnt;
	pQLT = qlt;

	Comment = ParcelLoadString(STRID_SAVE_METHOD_WAIT_COMMENT);
}

CSaveMethodWaitDlg::~CSaveMethodWaitDlg()
{
}

void CSaveMethodWaitDlg::DoDataExchange(CDataExchange* pDX)
{
	CWaitingDlg::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CSaveMethodWaitDlg, CWaitingDlg)
END_MESSAGE_MAP()


BOOL CSaveMethodWaitDlg::OnInitDialog()
{
	CWaitingDlg::OnInitDialog();

	return true;
}

void CSaveMethodWaitDlg::OK()
{
	CWaitingDlg::OK();
}

void CSaveMethodWaitDlg::Stop()
{
	CWaitingDlg::Stop();
}

void CSaveMethodWaitDlg::Work()
{
	ParcelWait(true);

	bool res = (pQNT == NULL) ? PrjData->SaveMethodToQltExportBase(FileName, pQLT)
		: PrjData->SaveMethodToQntExportBase(FileName, pQNT);

	ParcelWait(false);

	if(res)
		OK();
	else
		Stop();
}
