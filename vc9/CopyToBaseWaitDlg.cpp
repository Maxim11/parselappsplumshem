#include "stdafx.h"

#include "CopyToBaseWaitDlg.h"
#include "RootData.h"
#include "BranchInfo.h"

IMPLEMENT_DYNAMIC(CCopyToBaseWaitDlg, CWaitingDlg)

CCopyToBaseWaitDlg::CCopyToBaseWaitDlg(CString fname, CDeviceData* dev, CTreeCtrl* tree, CString prjname,
									   CWnd* pParent) : CWaitingDlg(pParent)
{
	FileName = fname;
	PrjName = prjname;
	DevData = dev;
	pTree = tree;

	Comment = (PrjName.IsEmpty()) ? ParcelLoadString(STRID_COPYTODB_WAIT_COMMENT_DEVICE) :
		ParcelLoadString(STRID_COPYTODB_WAIT_COMMENT_PROJECT);
}

CCopyToBaseWaitDlg::~CCopyToBaseWaitDlg()
{
}

void CCopyToBaseWaitDlg::DoDataExchange(CDataExchange* pDX)
{
	CWaitingDlg::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CCopyToBaseWaitDlg, CWaitingDlg)
END_MESSAGE_MAP()


BOOL CCopyToBaseWaitDlg::OnInitDialog()
{
	CWaitingDlg::OnInitDialog();

	return true;
}

void CCopyToBaseWaitDlg::OK()
{
	CWaitingDlg::OK();
}

void CCopyToBaseWaitDlg::Stop()
{
	CWaitingDlg::Stop();
}

void CCopyToBaseWaitDlg::Work()
{
	ParcelWait(true);
	
	bool res = pRoot->AddExistingDevice(FileName, DevData, pTree, PrjName);

	ParcelWait(false);

	if(res)
		OK();
	else
		Stop();
}
