#ifndef _PARCEL_MODEL_RESULT_DLG_H_
#define _PARCEL_MODEL_RESULT_DLG_H_

#pragma once

#include "ProjectData.h"
#include "ParcelListCtrl.h"
#include "Resource.h"


///////////////////////////////////////////////////////////////////////////////////////////////////
// ������ ������ ����������� ������� �������������� ������������� ������.

// ��������� ������ ������ �������

const int C_MODRESDLG_MODE_EDIT = 0;		// ����� � ����������� ��������������� ������
const int C_MODRESDLG_MODE_SHOW = 1;		// ����� � ����������� ��������������� ������

// �������� �������

const int C_MODRES_PAGE_SEC			= 0;	// �������� ������ SEC
const int C_MODRES_PAGE_SECV		= 1;	// �������� ������ SECV
const int C_MODRES_PAGE_SEV			= 2;	// �������� ������ SEV
const int C_MODRES_PAGE_OUT			= 3;	// �������� ������ ��������
const int C_MODRES_PAGE_BAD			= 4;	// �������� ������ ������� ����������� �������
const int C_MODRES_PAGE_SCORES		= 5;	// �������� ������ scores
const int C_MODRES_PAGE_SPECLOAD	= 6;	// �������� ������ ������������ ��������
const int C_MODRES_PAGE_CHEMLOAD	= 7;	// �������� ������ ���������� ��������

// � ����� ���� ����������

const int C_MODRES_SHOW_TABLE		= 0;		// ���������� � ���� �������
const int C_MODRES_SHOW_PLOT		= 1;		// ���������� � ���� ������� ���� �����������

// ����� ������� ����������

const int C_MODRES_SMPL_CALIB		= 0;		// ���������� �������������� �������
const int C_MODRES_SMPL_VALID		= 1;		// ���������� ������������� �������

// ������ ������������ ����

const int C_MODRES_MENU_PREV		= 11;
const int C_MODRES_MENU_NEXT		= 12;
const int C_MODRES_MENU_FULL		= 13;
const int C_MODRES_MENU_AUTO		= 14;
const int C_MODRES_MENU_LEFT		= 15;
const int C_MODRES_MENU_RIGHT		= 16;
const int C_MODRES_MENU_LEGEND		= 17;
const int C_MODRES_MENU_GRID		= 18;
const int C_MODRES_MENU_TREND		= 19;
const int C_MODRES_MENU_TEOR		= 20;
const int C_MODRES_MENU_MDL			= 21;
const int C_MODRES_MENU_LIMIT		= 22;

class CModelResultDlg : public CDialog
{
	DECLARE_DYNAMIC(CModelResultDlg)

public:

	CModelResultDlg(CModelData* Mod, int mode = C_MODRESDLG_MODE_EDIT, CWnd* pParent = NULL);	// �����������
	virtual ~CModelResultDlg();																	// ����������

	enum { IDD = IDD_MODEL_RESULT };							// ������������� �������

	CTabCtrl  m_Tab;			// �������� �� ��������
	CParcelListCtrl m_listBig;	// �������� ������� ������ �����������
	CListCtrl m_listSmall;		// �������������� ������� ������ �����������
	CComboBox m_Param;			// ������ ����������� ������
	CComboBox m_Show;			// ������ ��������� ����� �������������
	CComboBox m_CalibValid;		// ������ ��������� ������� ��������
	CComboBox m_OXsc;
	CComboBox m_OYsc;
	CComboBox m_OXch;
	CComboBox m_OYch;
	CEdit m_SEC;				// ���� ��� �������������� "�������� SEC"
	CEdit m_SECV;				// ���� ��� �������������� "�������� SECV"
	CEdit m_SEV;				// ���� ��� �������������� "�������� SEV"
	CEdit m_MaxMah;				// ���� ��� �������������� "������������� ���������� ������������"
	CEdit m_CorrB;				// ���� ��� �������������� "������������ ��������� b"
	CEdit m_CorrA;				// ���� ��� �������������� "������������ ��������� a"

	CMenu *pMenu1;

public:

	CModelData* ModData;		// ��������� �� �������������� ������
	LMod ModTmp;				// ���������� ������������� ��������

protected:

	CModelData* StartModel;		// ��������� �� ��������� �������������� ������

	int Mode;
	bool IsAvr;

	int m_vTab;					// ����� �������� ��������
	int m_vParam;				// ������ ���������� "����������"
	int m_vShow;				// ������ ���������� "���� �������������"
	int m_vCalibValid;			// ������ ���������� "������ ��������"
	double m_vSEC;				// ��������� �������� "�������� SEC"
	double m_vSECV;				// ��������� �������� "�������� SECV"
	double m_vSEV;				// ��������� �������� "�������� SEV"
	double m_vMaxMah;			// ��������� �������� "������������� ���������� ������������"
	double m_vCorrB;			// ��������� �������� "������������ ��������� b"
	double m_vCorrA;			// ��������� �������� "������������ ��������� a"
	int m_vOXsc;
	int m_vOYsc;
	int m_vOXch;
	int m_vOYch;

	VStr selC;					// ����� ���������� �������������� ��������
	VStr selV;					// ����� ���������� ������������� ��������

	CLumChart *pChart;

	CLumChartDataSet* pGraphSEC;
	CLumChartDataSet* pGraphSECV;
	CLumChartDataSet* pGraphSEV;
	CLumChartDataSet* pGraphLine0;
	CLumChartDataSet* pGraphLineSEC;
	CLumChartDataSet* pGraphLineSECV;
	CLumChartDataSet* pGraphLineSEV;
	CLumChartDataSet* pHistoCalib;
	CLumChartDataSet* pHistoValid;
	CLumChartDataSet* pHistoSamCalib;
	CLumChartDataSet* pHistoSamValid;
	CLumChartDataSet* pHistoBad;
	CLumChartDataSet* pHistoMDL;
	CLumChartDataSet* pHistoNorm;
	CLumChartDataSet* pGraphScores;
	vector<CLumChartDataSet> GraphsSpecLoad;
	vector<POSITION> MasksSLPos;
	CLumChartDataSet* pGraphChemLoad;

	CLumChartDataMarker* pMarkSEC;
	CLumChartDataMarker* pMarkSECV;
	CLumChartDataMarker* pMarkSEV;
	CLumChartDataMarker* pMarkHCalib;
	CLumChartDataMarker* pMarkHValid;
	CLumChartDataMarker* pMarkHBad;
	CLumChartDataMarker* pMarkScores;
	CLumChartDataMarker* pMarkChemLoad;

	POSITION posInfo;
	POSITION posCoord;
	POSITION posLegendSEC_V;
	POSITION posLegendOut;
	POSITION posLegendBad;
	POSITION posLegendSpec;
	POSITION posLegendChem;

	bool show_legend;
	bool show_trend;
	bool show_teor;
	bool show_mdl;
	bool show_limit;

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();						// ������������� �������

	virtual void OnOK()
	{};
	virtual void OnCancel();// ��������� ������� "�������� ���� � ����� �� �������"

	afx_msg void OnClose();
	afx_msg void OnAccept();			// ��������� ������� "������� ������ � ����� �� �������"
	afx_msg void OnTabSelchange(NMHDR* pNMHDR, LRESULT* pResult);			// ��������� ������� "������� �� ������ ��������"
	afx_msg void OnTableBigSelChange(NMHDR* /*pNMHDR*/, LRESULT *pResult);	// ��������� ������� "�������� �������� � �������� ������� �����������"
	afx_msg void OnTableBigColumnClick(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnChangeParam();		// ��������� ������� "�������� ����������"
	afx_msg void OnChangeView();		// ��������� ������� "�������� ��� �������������"
	afx_msg void OnChangeCalibValid();	// ��������� ������� "�������� ������ ��������"
	afx_msg void OnSECChanged();		// ��������� ������� "�������� �������� SEC"
	afx_msg void OnSECVChanged();		// ��������� ������� "�������� �������� SECV"
	afx_msg void OnSEVChanged();		// ��������� ������� "�������� �������� SEV"
	afx_msg void OnMaxMahChanged();		// ��������� ������� "�������� ������������ ���������� ������������"
	afx_msg void OnExclude();			// ��������� ������� "��������� ���������� �������"
	afx_msg void OnBack();				// ��������� ������� "�������� ��������� ���������"
	afx_msg void OnBackAll();			// ��������� ������� "�������� ��� ���������"
	afx_msg void OnRecalc();			// ��������� ������� "����������� ������"
	afx_msg void OnFromTrend();			// ��������� ������� "����������� ������������ ���������"
	afx_msg void OnRefresh();			// ��������� ������� "������������ �������"
	afx_msg void OnChangeAxeGKsc();
	afx_msg void OnChangeAxeGKch();
	afx_msg void OnSettings();
	afx_msg void OnSetTemplate();
	afx_msg void OnCreateProtocol();
	afx_msg void OnHelp();				// ��������� ������� "������� �������"

	void OnUserMenuPrev();
	void OnUserMenuNext();
	void OnUserMenuFull();
	void OnUserMenuAuto();
	void OnUserMenuLeft();
	void OnUserMenuRight();
	void OnUserMenuLegend();
	void OnUserMenuGrid();
	void OnUserMenuTrend();
	void OnUserMenuTeor();
	void OnUserMenuMDL();
	void OnUserMenuLimit();

	void ShowPage(int page = -1);		// ������� �� ����� ��������, ��������������� �������� ��������

	void RebuildTableBig();				// ����������� �������� ������� �����������
	void RebuildTableSmall();			// ��������� �������� ������� �����������

	void FillTableBig();				// ����������� ��������������� ������� �����������
	void FillTableSmall();				// ��������� ��������������� ������� �����������

	void SortAll();						// ����������� ���������� �������

	void SetSortIcons();

	bool ExcludeSpectra();
	bool ExcludePoints();

	void CreateGraphSEC();
	void CreateGraphSECV();
	void CreateGraphSEV();
	void CreateGraphTeor();
	void CreateHistoCalib();
	void CreateHistoValid();
	void CreateHistoSamCalib();
	void CreateHistoSamValid();
	void CreateHistoBad();
	void CreateHistoMDL();
	void CreateGraphScores();
	void CreateGraphsSpecLoad();
	void CreateGraphChemLoad();
	void CreateLegends();
	void CreateLegendSpec();
	void CreateAllGraphs();
	void CreateChart();

	void ColoredGraphSEC();
	void ColoredGraphSECV();
	void ColoredGraphSEV();
	void ColoredHistoCalib();
	void ColoredHistoValid();
	void ColoredHistoBad();
	void ColoredGraphScores();
	void ColoredGraphChemLoad();
	void ColoredAllGraphs();

	void BuildUserPopupMenu();

	bool IsLegendEnable();
	int GetNGKShow();

public:

	void SelShowGraphs(int graph);
	void MakeGraphPicture(CString FileName, int graph);

	void SelectByRect(CRect* rect, bool isFrame = true);
	void ShowGK(CRect* rect);
	void ShowCoord(CPoint* pnt);
	void RemoveInfo();

	int GetCurTab();
	bool GetCurCalibValid();
	double GetMaxMah();
	double GetNorm();
	bool IsResultBad(int item);

	void SetStatusMenu();

protected:

	DECLARE_MESSAGE_MAP()

};

#endif
