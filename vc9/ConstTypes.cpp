#include "stdafx.h"

#include "MainFrm.h"

#include <math.h>

CString cGetDeviceTypeName(int type)
{
	CString s = ParcelLoadString(STRID_LIST_DEVICE_TYPE);
	switch(type)
	{
	case C_DEV_TYPE_02:  
		s += _T("02");  break;
	case C_DEV_TYPE_10:  
		s += _T("10");  break;
	case C_DEV_TYPE_20:  
		s += _T("20");  break;
	case C_DEV_TYPE_40:  
		s += _T("40");  break;
	case C_DEV_TYPE_02M:  
		s += _T("02M");  break;
	case C_DEV_TYPE_10M:  
		s += _T("10M");  break;
	case C_DEV_TYPE_08:  
		s += _T("08");  break;
	case C_DEV_TYPE_12:  
		s += _T("12");  break;
	case C_DEV_TYPE_100:  
		s += _T("100");  break;
	case C_DEV_TYPE_IMIT:  
		s += _T("IMIT");  break;
	case C_DEV_TYPE_12M:  
		s += _T("12M");  break;
	}

	return s;
}

CString cGetMeasureTypeName(int type)
{
	CString str = cEmpty;
	switch(type)
	{
	case C_MEASURE_ONE:  str = ParcelLoadString(STRID_LIST_MEASURES_ONE);  break;
	case C_MEASURE_TWO:  str = ParcelLoadString(STRID_LIST_MEASURES_TWO);  break;
	case C_MEASURE_EVERY:  str = ParcelLoadString(STRID_LIST_MEASURES_EVERY);  break;
	default:  str = cEmpty;
	}

	return str;
}

CString cGetApodizeTypeName(int type)
{
	CString str = cEmpty;
	switch(type)
	{
	case C_APODIZE_RECTAN:  str = ParcelLoadString(STRID_LIST_CANALAPODIZE_RECTAN);  break;
	case C_APODIZE_NBWEAK:  str = ParcelLoadString(STRID_LIST_CANALAPODIZE_NBWEAK);  break;
	case C_APODIZE_NBMEAN:  str = ParcelLoadString(STRID_LIST_CANALAPODIZE_NBMEAN);  break;
	case C_APODIZE_NBSTRN:  str = ParcelLoadString(STRID_LIST_CANALAPODIZE_NBSTRN);  break;
	case C_APODIZE_BESSEL:  str = ParcelLoadString(STRID_LIST_CANALAPODIZE_BESSEL);  break;
	default:  str = cEmpty;
	}

	return str;
}

CString cGetAnalysisTypeName(int type)
{
	CString str = cEmpty;
	switch(type)
	{
	case C_ANALYSIS_QNT:  str = ParcelLoadString(STRID_LIST_ANTYPE_QNT);  break;
	case C_ANALYSIS_QLT:  str = ParcelLoadString(STRID_LIST_ANTYPE_QLT);  break;
	default:  str = cEmpty;
	}

	return str;
}

CString cGetTransTypeName(int type)
{
	CString str = cEmpty;
	switch(type)
	{
	case C_TRANS_TYPE_OWN:  str = ParcelLoadString(STRID_TRANSTYPE_OWN);  break;
	case C_TRANS_TYPE_NORMAL:  str = ParcelLoadString(STRID_TRANSTYPE_NORMAL);  break;
	case C_TRANS_TYPE_NOTCORR:  str = ParcelLoadString(STRID_TRANSTYPE_NOTCORR);  break;
	case C_TRANS_TYPE_SST:  str = ParcelLoadString(STRID_TRANSTYPE_SST);  break;
	default:  str = cEmpty;
	}

	return str;
}

CString cGetModelTypeName(int type)
{
	CString str = cEmpty;
	switch(type)
	{
	case C_MOD_MODEL_PLS:  str = ParcelLoadString(STRID_LIST_MODELTYPE_PLS);  break;
	case C_MOD_MODEL_PCR:  str = ParcelLoadString(STRID_LIST_MODELTYPE_PCR);  break;
	case C_MOD_MODEL_HSO:  str = ParcelLoadString(STRID_LIST_MODELTYPE_HSO);  break;
	case C_MOD_MODEL_PCA:  str = ParcelLoadString(STRID_LIST_MODELTYPE_PCA);  break;
	default:  str = cEmpty;
	}

	return str;
}

CString cGetModelTypeNameShort(int type)
{
	CString str = cEmpty;
	switch(type)
	{
	case C_MOD_MODEL_PLS:  str = ParcelLoadString(STRID_LIST_MODELTYPESHORT_PLS);  break;
	case C_MOD_MODEL_PCR:  str = ParcelLoadString(STRID_LIST_MODELTYPESHORT_PCR);  break;
	case C_MOD_MODEL_HSO:  str = ParcelLoadString(STRID_LIST_MODELTYPESHORT_HSO);  break;
	case C_MOD_MODEL_PCA:  str = ParcelLoadString(STRID_LIST_MODELTYPESHORT_PCA);  break;
	default:  str = cEmpty;
	}

	return str;
}

CString cGetModelHSOTypeName(int type)
{
	CString str = cEmpty;
	switch(type)
	{
	case C_MOD_MODEL_HSO_LPM:  str = ParcelLoadString(STRID_LIST_HSOTYPE_LPM);  break;
	case C_MOD_MODEL_HSO_LHM:  str = ParcelLoadString(STRID_LIST_HSOTYPE_LHM);  break;
	case C_MOD_MODEL_HSO_NLPM:  str = ParcelLoadString(STRID_LIST_HSOTYPE_NLPM);  break;
	case C_MOD_MODEL_HSO_NLHM:  str = ParcelLoadString(STRID_LIST_HSOTYPE_NLHM);  break;
	default:  str = cEmpty;
	}

	return str;
}

CString cGetSpectrumTypeName(int type)
{
	CString str = cEmpty;
	switch(type)
	{
	case C_MOD_SPECTRUM_TRA:  str = ParcelLoadString(STRID_LIST_SPTYPE_TRA);  break;
	case C_MOD_SPECTRUM_ABS:  str = ParcelLoadString(STRID_LIST_SPTYPE_ABS);  break;
	case C_MOD_SPECTRUM_BOTH:  str = ParcelLoadString(STRID_LIST_SPTYPESHORT_BOTH);  break;
	default:  str = cEmpty;
	}

	return str;
}

CString cGetSpectrumTypeNameShort(int type)
{
	CString str = cEmpty;
	switch(type)
	{
	case C_MOD_SPECTRUM_TRA:  str = ParcelLoadString(STRID_LIST_SPTYPESHORT_TRA);  break;
	case C_MOD_SPECTRUM_ABS:  str = ParcelLoadString(STRID_LIST_SPTYPESHORT_ABS);  break;
	case C_MOD_SPECTRUM_BOTH:  str = ParcelLoadString(STRID_LIST_SPTYPESHORT_BOTH);  break;
	default:  str = cEmpty;
	}

	return str;
}

CString cGetPreprocName(int prep)
{
	CString str = cEmpty;
	switch(prep)
	{
	case C_PREPROC_0:  str = ParcelLoadString(STRID_LIST_PREPROC_NO);  break;
	case C_PREPROC_A:  str = ParcelLoadString(STRID_LIST_PREPROC_A);  break;
	case C_PREPROC_D:  str = ParcelLoadString(STRID_LIST_PREPROC_D);  break;
	case C_PREPROC_M:  str = ParcelLoadString(STRID_LIST_PREPROC_M);  break;
	case C_PREPROC_C:  str = ParcelLoadString(STRID_LIST_PREPROC_C);  break;
	case C_PREPROC_N:  str = ParcelLoadString(STRID_LIST_PREPROC_N);  break;
	case C_PREPROC_B:  str = ParcelLoadString(STRID_LIST_PREPROC_B);  break;
	case C_PREPROC_1:  str = ParcelLoadString(STRID_LIST_PREPROC_1);  break;
	case C_PREPROC_2:  str = ParcelLoadString(STRID_LIST_PREPROC_2);  break;
	default:  str = ParcelLoadString(STRID_LIST_PREPROC_NO);
	}

	return str;
}

CString cGetCritName(int crit)
{
	CString str = cEmpty;

	switch(crit)
	{
	case C_CRIT_SEC:  str = ParcelLoadString(STRID_LIST_CRIT_SEC);  break;
	case C_CRIT_R2SEC:  str = ParcelLoadString(STRID_LIST_CRIT_R2SEC);  break;
	case C_CRIT_SECV:  str = ParcelLoadString(STRID_LIST_CRIT_SECV);  break;
	case C_CRIT_R2SECV:  str = ParcelLoadString(STRID_LIST_CRIT_R2SECV);  break;
	case C_CRIT_F:  str = ParcelLoadString(STRID_LIST_CRIT_F);  break;
	case C_CRIT_SEV:  str = ParcelLoadString(STRID_LIST_CRIT_SEV);  break;
	case C_CRIT_ERR:  str = ParcelLoadString(STRID_LIST_CRIT_ERR);  break;
	case C_CRIT_SDV:  str = ParcelLoadString(STRID_LIST_CRIT_SDV);  break;
	case C_CRIT_R2SEV:  str = ParcelLoadString(STRID_LIST_CRIT_R2SEV);  break;
	case C_CRIT_K1:  str.Format(ParcelLoadString(STRID_LIST_CRIT_K), 1);  break;
	case C_CRIT_K2:  str.Format(ParcelLoadString(STRID_LIST_CRIT_K), 2);  break;
	case C_CRIT_K3:  str.Format(ParcelLoadString(STRID_LIST_CRIT_K), 3);  break;
	case C_CRIT_K4:  str.Format(ParcelLoadString(STRID_LIST_CRIT_K), 4);  break;
	case C_CRIT_K5:  str.Format(ParcelLoadString(STRID_LIST_CRIT_K), 5);  break;
	case C_CRIT_K6:  str.Format(ParcelLoadString(STRID_LIST_CRIT_K), 6);  break;
	case C_CRIT_K7:  str.Format(ParcelLoadString(STRID_LIST_CRIT_K), 7);  break;
	case C_CRIT_SECSECV:  str = _T("SECV-SEC");  break;
	default:  str = cEmpty;
	}

	return str;
}

CString cGetOptimOperName(int oper)
{
	CString str = cEmpty;

	switch(oper)
	{
	case C_OPTIM_TEST_EXCLUDE:  str = ParcelLoadString(STRID_LIST_OPTIM_TEST_EXLUDE);  break;
	case C_OPTIM_TEST_OUTS:  str = ParcelLoadString(STRID_LIST_OPTIM_TEST_OUTS);  break;
	case C_OPTIM_TEST_HIPER:  str = ParcelLoadString(STRID_LIST_OPTIM_TEST_HIPER);  break;
	default:  str = cEmpty;
	}

	return str;
}

CString cGetOptimHQOHorPName(int type)
{
	CString str = cEmpty;

	switch(type)
	{
	case C_OPTIM_HQO_PNT:  str = ParcelLoadString(STRID_LIST_OPTIMHQO_PNT);  break;
	case C_OPTIM_HQO_HARM:  str = ParcelLoadString(STRID_LIST_OPTIMHQO_HARM);  break;
	case C_OPTIM_HQO_HALL:  str = ParcelLoadString(STRID_LIST_OPTIMHQO_HALL);  break;
	default:  str = cEmpty;
	}

	return str;
}

CString cGetOptimHQOLorNName(int type)
{
	CString str = cEmpty;

	switch(type)
	{
	case C_OPTIM_HQO_LIN:  str = ParcelLoadString(STRID_LIST_OPTIMHQO_LIN);  break;
	case C_OPTIM_HQO_NLN:  str = ParcelLoadString(STRID_LIST_OPTIMHQO_NLN);  break;
	case C_OPTIM_HQO_LALL:  str = ParcelLoadString(STRID_LIST_OPTIMHQO_LALL);  break;
	default:  str = cEmpty;
	}

	return str;
}

CString cGetPrjCopyName(int mode)
{
	CString str = cEmpty;

	switch(mode)
	{
	case C_PRJ_COPY_ONLYSETTINGS:  str = ParcelLoadString(STRID_LIST_PRJCOPY_ONLYSETTINGS);  break;
	case C_PRJ_COPY_WITHPARAMS:  str = ParcelLoadString(STRID_LIST_PRJCOPY_WITHPARAMS);  break;
	case C_PRJ_COPY_WITHSAMPLES:  str = ParcelLoadString(STRID_LIST_PRJCOPY_WITHSAMPLES);  break;
	case C_PRJ_COPY_WITHSPECTRA:  str = ParcelLoadString(STRID_LIST_PRJCOPY_WITHSPECTRA);  break;
	case C_PRJ_COPY_WITHTRANS:  str = ParcelLoadString(STRID_LIST_PRJCOPY_WITHTRANS);  break;
	default:  str = cEmpty;
	}

	return str;
}

CString cGetSpectraTypeName(int type)
{
	CString str = cEmpty;

	switch(type)
	{
	case C_SPECTRA_TYPE_ALL:  str = ParcelLoadString(STRID_LIST_MSTYPE_ALL);  break;
	case C_SPECTRA_TYPE_CALIB:  str = ParcelLoadString(STRID_LIST_MSTYPE_CALIB);  break;
	case C_SPECTRA_TYPE_VALID:  str = ParcelLoadString(STRID_LIST_MSTYPE_VALID);  break;
	default:  str = cEmpty;
	}

	return str;
}

CString cGetSortFmtUp(CString name)
{
	CString res;
	res.Format(ParcelLoadString(STRID_LIST_SORT_UP), name);

	return res;
}

CString cGetSortFmtDown(CString name)
{
	CString res;
	res.Format(ParcelLoadString(STRID_LIST_SORT_DOWN), name);

	return res;
}

int cGetPreprocIntByChar(TCHAR ch)
{
	int res = C_PREPROC_0;
	switch(ch)
	{
	case 'A':  res = C_PREPROC_A;  break;
	case 'M':  res = C_PREPROC_M;  break;
	case 'D':  res = C_PREPROC_D;  break;
	case 'C':  res = C_PREPROC_C;  break;
	case 'N':  res = C_PREPROC_N;  break;
	case 'B':  res = C_PREPROC_B;  break;
	case '1':  res = C_PREPROC_1;  break;
	case '2':  res = C_PREPROC_2;  break;
	default:  res = C_PREPROC_0;
	}

	return res;
}

TCHAR cGetPreprocCharByInt(int prep)
{
	if(prep < C_PREPROC_0 || prep > C_PREPROC_2)
		return ' ';

	TCHAR res = cPreprocs[prep];

	return res;
}

CString cGetPreprocAsStr(VInt* preps)
{
	CString res = cEmpty;
	ItInt it;
	for(it=preps->begin(); it!=preps->end(); ++it)
	{
		if((*it) == C_PREPROC_0)
			break;
		res += cGetPreprocCharByInt((*it));
	}

	return res;
}

void cGetPreprocFromStr(CString str, VInt& preps, int fill)
{
	preps.clear();

	int i, j, n = int(_tcslen(str));
	for(i=0; i<n; i++)
	{
		int prep = cGetPreprocIntByChar(str[i]);
		if(prep <= C_PREPROC_0 || prep > C_PREPROC_2)
			break;

		preps.push_back(prep);
	}

	for(j=i; j<fill; j++)
		preps.push_back(C_PREPROC_0);
}

bool cIsPrepInStr(CString str, int prep)
{
	int i, n = int(_tcslen(str));
	if(n == 0)
		return (prep == C_PREPROC_0);

	for(i=0; i<n; i++)
	{
		if(prep == cGetPreprocIntByChar(str[i]))
			return true;
	}

	return false;
}

bool cCheckPrepAlready(CString all, CString prep)
{
	VStr Adds;

	CString Str = all;
	Str.Trim();
	int i, N = int(_tcslen(Str));
	
	CString s = cEmpty;
	for(i=0; i<N; i++)
	{
		if(Str[i] == ';' || Str[i] == ',')
		{
			s.Trim();
			if(!s.Compare(prep))
				return true;
			s = cEmpty;
			continue;
		}
		else
			s += Str[i];
	}
	if(!s.IsEmpty())
		if(!s.Compare(prep))
			return true;

	return false;
}

bool cIsParamNameReserved(CString name)
{
	CString ParName1, ParName2, ParName3;
	ParName1.Format(ParcelLoadString(STRID_SSTNAMES_PARAM), 1);
	ParName2.Format(ParcelLoadString(STRID_SSTNAMES_PARAM), 2);
	ParName3.Format(ParcelLoadString(STRID_SSTNAMES_PARAM), 3);

	if(name.Compare(ParName1) && name.Compare(ParName2) && name.Compare(ParName3))
		return false;

	return true;
}

bool cIsBadSymbolInName(CString name)
{
	int i, len = name.GetLength();
	for(i=0; i<len; i++)
		if(name[i] == '"' || name[i] == ':' || name[i] == '*' || name[i] == '?' || name[i] == '|'
		   || name[i] == '<'|| name[i] == '>' || name[i] == '/' || name[i] == '\\')
			return false;

	return true;
}

bool cIsCritDerivated(int crit, int parent)
{
	if(crit == parent)
		return true;

	bool res = false;
	switch(parent)
	{
	case C_CRIT_SEC:
		res = (crit == C_CRIT_R2SEC || crit == C_CRIT_K1 || crit == C_CRIT_K2 || crit == C_CRIT_K3 ||
			crit == C_CRIT_K4 || crit == C_CRIT_K5 || crit == C_CRIT_K6 || crit == C_CRIT_K7);
		break;
	case C_CRIT_SECV:
		res = (crit == C_CRIT_R2SECV || crit == C_CRIT_F || crit == C_CRIT_K2 || crit == C_CRIT_K4 ||
			crit == C_CRIT_K5 || crit == C_CRIT_K7);
		break;
	case C_CRIT_SEV:
		res = (crit == C_CRIT_ERR || crit == C_CRIT_SDV || crit == C_CRIT_R2SEV || crit == C_CRIT_K1 ||
			crit == C_CRIT_K3 || crit == C_CRIT_K7);
		break;
	default:  res = false;
	}

	return res;
}

double cCalcCritK(int crit, double sec, double secv, double sev, double Ymin, double Ymax)
{
	bool bsec = (fabs(sec) > 1.e-9);

	double res = 0.;
	switch(crit)
	{
	case C_CRIT_K1:	res = sqrt(sec * sec + sev * sev) + fabs(sec - sev);  break;
	case C_CRIT_K2:	res = sqrt(sec * sec + secv * secv) + fabs(sec - secv);  break;
	case C_CRIT_K3:	res = sec * sec + sev * sev;  break;
	case C_CRIT_K4:	res = sec * sec + secv * secv;  break;
	case C_CRIT_K5:	res = (bsec) ? secv / sec : 0.;  break;
	case C_CRIT_K6:	res = (bsec) ? (Ymax - Ymin) / sec : 0.;  break;
	case C_CRIT_K7:	res = sqrt((sec * sec + secv * secv + sev * sev) / 3.);  break;
	}

	return res;
}

bool cCompareByCrit(int crit, double newval, double oldval)
{
	switch(crit)
	{
	case C_CRIT_SEC:  return newval < oldval;
	case C_CRIT_R2SEC:  return fabs(1. - newval) < fabs(1. - oldval);
	case C_CRIT_SECV:  return newval < oldval;
	case C_CRIT_R2SECV:  return fabs(1. - newval) < fabs(1. - oldval);
	case C_CRIT_SECSECV:  return newval < oldval;
	case C_CRIT_F:  return newval > oldval;
	case C_CRIT_SEV:  return newval < oldval;
	case C_CRIT_ERR:  return fabs(newval) < fabs(oldval);
	case C_CRIT_SDV:  return newval < oldval;
	case C_CRIT_R2SEV:  return fabs(1. - newval) < fabs(1. - oldval);
	case C_CRIT_K1:  return newval < oldval;
	case C_CRIT_K2:  return newval < oldval;
	case C_CRIT_K3:  return newval < oldval;
	case C_CRIT_K4:  return newval < oldval;
	case C_CRIT_K5:
		if(newval > 0. && (oldval < 0. || newval < oldval))
			return true;
		if(newval < 0. && oldval < 0. && newval > oldval)
			return true;
		return false;
	case C_CRIT_K6:  return newval > oldval;
	case C_CRIT_K7:  return newval < oldval;
	}

	return false;
}

CString cGetCritFormat(int crit)
{
	CString fmt = cFmt42;
	switch(crit)
	{
	case C_CRIT_SEC:
	case C_CRIT_SECV:
	case C_CRIT_SECSECV:
	case C_CRIT_SEV:
	case C_CRIT_ERR:
	case C_CRIT_SDV:
	case C_CRIT_K1:
	case C_CRIT_K2:
	case C_CRIT_K3:
	case C_CRIT_K4:
	case C_CRIT_K5:
	case C_CRIT_K6:
	case C_CRIT_K7:
		fmt = cFmt86;
		break;
	case C_CRIT_R2SEC:
	case C_CRIT_R2SECV:
	case C_CRIT_F:
	case C_CRIT_R2SEV:
		fmt = cFmt86;
		break;
	}

	return fmt;
}

int cGetDeviceID0(int type)
{
	int id0 = 0;
	switch(type)
	{
	case C_DEV_TYPE_02:  id0 = 1;  break;
	case C_DEV_TYPE_10:  id0 = 2;  break;
	case C_DEV_TYPE_20:  id0 = 4;  break;
	case C_DEV_TYPE_40:  id0 = 5;  break;
	case C_DEV_TYPE_02M:  id0 = 6;  break;
	case C_DEV_TYPE_10M:  id0 = 7;  break;
	case C_DEV_TYPE_08:  id0 = 8;  break;
	case C_DEV_TYPE_100:  id0 = 9;  break;
	case C_DEV_TYPE_12:  id0 = 10;  break;
	case C_DEV_TYPE_12M:  id0 = 11;  break;
	case C_DEV_TYPE_IMIT:  id0 = 111;  break;
	}

	return id0;
}

int cGetDeviceTypeByID0(int id0)
{
	int type = C_DEV_TYPE_02;
	switch(id0)
	{
	case 1:  type = C_DEV_TYPE_02;  break;
	case 2:  type = C_DEV_TYPE_10;  break;
	case 4:  type = C_DEV_TYPE_20;  break;
	case 5:  type = C_DEV_TYPE_40;  break;
	case 6:  type = C_DEV_TYPE_02M;  break;
	case 7:  type = C_DEV_TYPE_10M;  break;
	case 8:  type = C_DEV_TYPE_08;  break;
	case 9:  type = C_DEV_TYPE_100;  break;
	case 10:  type = C_DEV_TYPE_12;  break;
	case 11:  type = C_DEV_TYPE_12M;  break;
	case 111:  type = C_DEV_TYPE_IMIT;  break;
	}

	return type;
}

CString cGetFormat(int dig)
{
	CString s = cFmt20;
	switch(dig)
	{
	case 0:  s = cFmt20;  break;
	case 1:  s = cFmt31;  break;
	case 2:  s = cFmt42;  break;
	case 3:  s = cFmt53;  break;
	case 4:  s = cFmt64;  break;
	case 5:  s = cFmt75;  break;
	case 6:  s = cFmt86;  break;
	case 7:  s = cFmt97;  break;
	default:  s = cFmt20;
	}

	return s;
}

COLORREF cGetDefColor(int ind)
{
	int num = (ind % 52);

	COLORREF color = RGB(0, 0, 0);
	switch(num)
	{
	case 0:  color = RGB(102,102,255);  break;
	case 1:  color = RGB(51,204,51);  break;
	case 2:  color = RGB(255,102,102);  break;
	case 3:  color = RGB(51,204,204);  break;
	case 4:  color = RGB(204,51,204);  break;
	case 5:  color = RGB(204,204,51);  break;
	case 6:  color = RGB(153,153,153);  break;
	case 7:  color = RGB(51,153,255);  break;
	case 8:  color = RGB(153,255,51);  break;
	case 9:  color = RGB(255,51,153);  break;
	case 10:  color = RGB(153,51,255);  break;
	case 11:  color = RGB(51,255,153);  break;
	case 12:  color = RGB(255,153,51);  break;
	case 13:  color = RGB(51,51,204);  break;
	case 14:  color = RGB(102,255,102);  break;
	case 15:  color = RGB(204,51,51);  break;
	case 16:  color = RGB(0,153,153);  break;
	case 17:  color = RGB(153,0,153);  break;
	case 18:  color = RGB(153,153,0);  break;
	case 19:  color = RGB(102,102,102);  break;
	case 20:  color = RGB(0,102,204);  break;
	case 21:  color = RGB(102,204,0);  break;
	case 22:  color = RGB(204,0,102);  break;
	case 23:  color = RGB(102,0,204);  break;
	case 24:  color = RGB(0,204,102);  break;
	case 25:  color = RGB(204,102,0);  break;
	case 26:  color = RGB(153,153,255);  break;
	case 27:  color = RGB(153,255,153);  break;
	case 28:  color = RGB(255,153,153);  break;
	case 29:  color = RGB(51,255,255);  break;
	case 30:  color = RGB(255,51,255);  break;
	case 31:  color = RGB(255,255,51);  break;
	case 32:  color = RGB(204,204,204);  break;
	case 33:  color = RGB(102,204,255);  break;
	case 34:  color = RGB(204,255,102);  break;
	case 35:  color = RGB(255,102,204);  break;
	case 36:  color = RGB(204,102,255);  break;
	case 37:  color = RGB(102,255,204);  break;
	case 38:  color = RGB(255,204,102);  break;
	case 39:  color = RGB(0,0,204);  break;
	case 40:  color = RGB(0,204,0);  break;
	case 41:  color = RGB(204,0,0);  break;
	case 42:  color = RGB(0,102,102);  break;
	case 43:  color = RGB(102,0,102);  break;
	case 44:  color = RGB(102,102,0);  break;
	case 45:  color = RGB(53,53,53);  break;
	case 46:  color = RGB(0,51,153);  break;
	case 47:  color = RGB(51,153,0);  break;
	case 48:  color = RGB(153,0,51);  break;
	case 49:  color = RGB(51,0,153);  break;
	case 50:  color = RGB(0,153,51);  break;
	case 51:  color = RGB(153,51,0);  break;
	default:  color = RGB(0, 0, 0);
	}

	return color;
}

void cParcelDDX(CDataExchange* pDX, int nIDC, int& value)
{
	HWND hWndCtrl = pDX->PrepareEditCtrl(nIDC);
	ASSERT(hWndCtrl != NULL);

	CString str;
	TCHAR cstr[64];
	if(pDX->m_bSaveAndValidate)
	{
		::GetWindowText(hWndCtrl, cstr, sizeof(cstr));
		str = cstr;
		str.Trim();

		if(str.IsEmpty())
		{
			ParcelError(ParcelLoadString(STRID_DDXERROR_EMPTY));
			pDX->Fail();
			return;
		}

		int i = 0, len = str.GetLength();
		CString StrNorm;
		while(i < len && ((str.GetAt(i) >= '0' && str.GetAt(i) <= '9') ||
			  i == 0 && (str.GetAt(i) == '-' || str.GetAt(i) == '+')))
		{
			StrNorm += str.GetAt(i);
			i++;
		}
		if(StrNorm.IsEmpty() || i < len)
		{
			ParcelError(ParcelLoadString(STRID_DDXERROR_INT));
			pDX->Fail();
			return;
		}

		value = _tstoi(StrNorm);
	}
	else
	{
		str.Format(cFmt1, value);
		::SetWindowText(hWndCtrl, str);
	}
}

void cParcelDDX(CDataExchange* pDX, int nIDC, double& value)
{
	HWND hWndCtrl = pDX->PrepareEditCtrl(nIDC);
	ASSERT(hWndCtrl != NULL);

	CString str;
	TCHAR cstr[64];
	if(pDX->m_bSaveAndValidate)
	{
		::GetWindowText(hWndCtrl, cstr, sizeof(cstr));
		str = cstr;
		str.Trim();

		if(str.IsEmpty())
		{
			ParcelError(ParcelLoadString(STRID_DDXERROR_EMPTY));
			pDX->Fail();
			return;
		}

		bool minus = false;
		int i = 0, len = str.GetLength();
		CString StrNorm1, StrNorm2;
		while(i < len && ((str.GetAt(i) >= '0' && str.GetAt(i) <= '9') ||
			  i == 0 && (str.GetAt(i) == '-' || str.GetAt(i) == '+')))
		{
			if(i == 0 && str.GetAt(i) == '-')
				minus = true;
			else
				StrNorm1 += str.GetAt(i);
			i++;
		}
		if(StrNorm1.IsEmpty() || (i < len && str.GetAt(i) != '.' && str.GetAt(i) != ','))
		{
			ParcelError(ParcelLoadString(STRID_DDXERROR_FLOAT));
			pDX->Fail();
			return;
		}
		i++;
		while(i < len && str.GetAt(i) >= '0' && str.GetAt(i) <= '9')
		{
			StrNorm2 += str.GetAt(i);
			i++;
		}
		if(i < len)
		{
			ParcelError(ParcelLoadString(STRID_DDXERROR_FLOAT));
			pDX->Fail();
			return;
		}

		value = _tstof(StrNorm1);
		if(!StrNorm2.IsEmpty())
		{
			double val2 = _tstof(StrNorm2);
			int len2 = StrNorm2.GetLength();
			value += val2 / (pow(10., len2));
		}
		if(minus)
			value = - value;
	}
	else
	{
		str.Format(cFmt86, value);
		::SetWindowText(hWndCtrl, str);
	}
}

void cParcelDDX(CDataExchange* pDX, int nIDC, CString& value)
{
	HWND hWndCtrl = pDX->PrepareEditCtrl(nIDC);
	ASSERT(hWndCtrl != NULL);

	TCHAR cstr[255];
	if(pDX->m_bSaveAndValidate)
	{
		::GetWindowText(hWndCtrl, cstr, sizeof(cstr));
		value = cstr;
	}
	else
	{
		::SetWindowText(hWndCtrl, value);
	}
}

void cParcelDDV(CDataExchange* pDX, CString strerr, int value, int min, int max)
{
	if(value < min || value > max)
	{
		CString s;
		s.Format(strerr, min, max);
		ParcelError(s);
		pDX->Fail();
	}
}

void cParcelDDV(CDataExchange* pDX, CString strerr, double value, double min, double max)
{
	if(value < min - 1.e-10 || value > max + 1.e-10)
	{
		CString s;
		s.Format(strerr, min, max);
		ParcelError(s);
		pDX->Fail();
	}
}

CString cGetConvertError(int ind)
{
	CString s = cEmpty;
	switch(ind)
	{
	case C_CONVERT_ERROR_ZERONUM:  s = ParcelLoadString(STRID_ERROR_CONVERT_ZERONUM);  break;
	case C_CONVERT_ERROR_BADNUM:  s = ParcelLoadString(STRID_ERROR_CONVERT_BADNUM);  break;
	case C_CONVERT_ERROR_WAVE:  s = ParcelLoadString(STRID_ERROR_CONVERT_WAVE);  break;
	case C_CONVERT_ERROR_LOWLIM:  s = ParcelLoadString(STRID_ERROR_CONVERT_LOWLIM);  break;
	case C_CONVERT_ERROR_UPPERLIM:  s = ParcelLoadString(STRID_ERROR_CONVERT_UPPERLIM);  break;
	case C_CONVERT_ERROR_RESOL:  s = ParcelLoadString(STRID_ERROR_CONVERT_RESOL);  break;
	case C_CONVERT_ERROR_APODIZE:  s = ParcelLoadString(STRID_ERROR_CONVERT_APODIZE);  break;
	case C_CONVERT_ERROR_ZEROFILL:  s = ParcelLoadString(STRID_ERROR_CONVERT_ZEROFILL);  break;
	case C_CONVERT_ERROR_SAMPLE:  s = ParcelLoadString(STRID_ERROR_CONVERT_SAMPLE);  break;
	case C_CONVERT_ERROR_REFDATA:  s = ParcelLoadString(STRID_ERROR_CONVERT_REFDATA);  break;
	case C_CONVERT_ERROR_NOSPEC:  s = ParcelLoadString(STRID_ERROR_CONVERT_NOSPEC);  break;
	case C_CONVERT_ERROR_PARAM:  s = ParcelLoadString(STRID_ERROR_CONVERT_PARAM);  break;
	case C_CONVERT_ERROR_NOTOPEN:  s = ParcelLoadString(STRID_ERROR_CONVERT_NOTOPEN);  break;
	case C_CONVERT_ERROR_SPECPNT:  s = ParcelLoadString(STRID_ERROR_CONVERT_SPECPNT);  break;
	case C_CONVERT_ERROR_NOTALLDATA:  s = ParcelLoadString(STRID_ERROR_CONVERT_NOTALLDATA);  break;
	case C_CONVERT_ERROR_MODTRANS:  s = ParcelLoadString(STRID_ERROR_CONVERT_MODTRANS);  break;
	case C_CONVERT_ERROR_MODNOTQNT:  s = ParcelLoadString(STRID_ERROR_CONVERT_MODNOTQNT);  break;
	case C_CONVERT_ERROR_MODNOPARAMS:  s = ParcelLoadString(STRID_ERROR_CONVERT_MODNOPARAMS);  break;
	case C_CONVERT_ERROR_MODNOCALIB:  s = ParcelLoadString(STRID_ERROR_CONVERT_MODNOCALIB);  break;
	case C_CONVERT_ERROR_MODSAMPLE:  s = ParcelLoadString(STRID_ERROR_CONVERT_MODSAMPLE);  break;
	case C_CONVERT_ERROR_MTDNOTQNT:  s = ParcelLoadString(STRID_ERROR_CONVERT_MTDNOTQNT);  break;
	case C_CONVERT_ERROR_MTDNOTOPEN:  s = ParcelLoadString(STRID_ERROR_CONVERT_MTDNOTOPEN);  break;
	case C_CONVERT_ERROR_MTDNOPARAMS:  s = ParcelLoadString(STRID_ERROR_CONVERT_MTDNOPARAMS);  break;
	case C_CONVERT_ERROR_EMPTYUNIT:  s = ParcelLoadString(STRID_ERROR_CONVERT_EMPTYUNIT);  break;
	case C_CONVERT_ERROR_MODREF:  s = ParcelLoadString(STRID_ERROR_CONVERT_MODREF);  break;
	}

	return s;
}

CString cGetQltFormulaError(int err)
{
	CString str = cEmpty;
	switch(err)
	{
	case C_MTD_QLTFORM_ERR_OK:  str = ParcelLoadString(STRID_ERROR_FORMULA_OK);  break;
	case C_MTD_QLTFORM_ERR_EMPTY:  str = ParcelLoadString(STRID_ERROR_FORMULA_EMPTY);  break;
	case C_MTD_QLTFORM_ERR_NOMODELS:  str = ParcelLoadString(STRID_ERROR_FORMULA_NOMODELS);  break;
	case C_MTD_QLTFORM_ERR_NOTALLMODELS:  str = ParcelLoadString(STRID_ERROR_FORMULA_NOTALLMODELS);  break;
	case C_MTD_QLTFORM_ERR_MANYMODELS:  str = ParcelLoadString(STRID_ERROR_FORMULA_MANYMODELS);  break;
	case C_MTD_QLTFORM_ERR_DBLBR:  str = ParcelLoadString(STRID_ERROR_FORMULA_DBLBR);  break;
	case C_MTD_QLTFORM_ERR_DBLOPER:  str = ParcelLoadString(STRID_ERROR_FORMULA_DBLOPER);  break;
	case C_MTD_QLTFORM_ERR_BADEND:  str = ParcelLoadString(STRID_ERROR_FORMULA_BADEND);  break;
	}

	return str;
}

CString cGetHelpPageName(int IdDlg)
{
	CString s = cEmpty;
	switch(IdDlg)
	{
	case DLGID_MAIN:  s = _T("::/1_.htm");  break;
	case DLGID_DEVICE_CREATE:  s = _T("::/6.1_.htm");  break;
	case DLGID_DEVICE_CHANGE:  s = _T("::/6.2_.htm");  break;
	case DLGID_PROJECT_CREATE:  s = _T("::/7.1_.htm");  break;
	case DLGID_PROJECT_CHANGE:  s = _T("::/7.3_.htm");  break;
	case DLGID_PROJECT_COPY:  s = _T("::/7.2_.htm");  break;
	case DLGID_PROJECT_SST_CREATE:  s = _T("::/7.10_.htm");  break;
	case DLGID_PROJECT_CONVERT_ACCEPT:  s = _T("::/7.5.1_.htm");  break;
	case DLGID_PROJECT_IMPORT_SPECTRA:  s = _T("::/7.9_.htm");  break;
	case DLGID_PROJECT_TRANSFER: s = _T("::/7.11_.htm");  break;
	case DLGID_PARAM_EDIT_LIST:  s = _T("::/8.1_.htm");  break;
	case DLGID_PARAM_CREATE:  s = _T("::/8.1.1_.htm");  break;
	case DLGID_PARAM_CHANGE:  s = _T("::/8.1.2_.htm");  break;
	case DLGID_SAMPLE_EDIT_LIST:  s = _T("::/9.1.1_.htm");  break;
	case DLGID_SAMPLE_CREATE:  s = _T("::/9.1.2_.htm");  break;
	case DLGID_SAMPLE_CHANGE:  s = _T("::/9.1.3_.htm");  break;
	case DLGID_SAMPLE_CREATE_SST:  s = _T("::/9.4_.htm");  break;
	case DLGID_SAMPLE_SPECTRA:  s = _T("::/9.4.1_.htm");  break;
	case DLGID_SAMPLE_CREATE_SET:  s = _T("::/9.2.1_.htm");  break;
	case DLGID_SAMPLE_CHANGE_SET:  s = _T("::/9.2.2_.htm");  break;
	case DLGID_SHOW_SPECTRA:  s = _T("::/9.3_.htm");  break;
	case DLGID_SHOW_PLOT_SETTINGS:  s = _T("::/9.3.1_.htm");  break;
	case DLGID_SHOW_HISTO_CALIB:  s = _T("::/10.1.3.2_.htm");  break;
	case DLGID_SHOW_HISTO_VALID:  s = _T("::/10.1.3.2_.htm");  break;
	case DLGID_MODEL_CREATE:  s = _T("::/10.1_.htm");  break;
	case DLGID_MODEL_COPY:  s = _T("::/10.2_.htm");  break;
	case DLGID_MODEL_CHANGE:  s = _T("::/10.7_.htm");  break;
	case DLGID_MODEL_RESULT_QNT:  s = _T("::/10.1.9_.htm");  break;
	case DLGID_MODEL_RESULT_QLT:  s = _T("::/10.1.8_.htm");  break;
	case DLGID_MODEL_RESULT_CHOOSE_GK:  s = _T("::/10.1.10_.htm");  break;
	case DLGID_MODEL_RESULT_SET_TEMPLATE:  s = _T("::/15_.htm");  break;
	case DLGID_MODEL_OPTIM:  s = _T("::/11.1_.htm");  break;
	case DLGID_MODEL_OPTIM_BYTMPL:  s = _T("::/11.2_.htm");  break;
	case DLGID_MODEL_TEMPLATE_CREATE:  s = _T("::/11.3.1_.htm");  break;
	case DLGID_MODEL_TEMPLATE_CHANGE:  s = _T("::/11.3.2_.htm");  break;
	case DLGID_MODEL_TEMPLATE_MANAGER:  s = _T("::/11.3_.htm");  break;
	case DLGID_MODEL_TEMPLATE_NAME:  s = _T("::/11.7_.htm");  break;
	case DLGID_METHOD_CREATE_QLT:  s = _T("::/12.1_.htm");  break;
	case DLGID_METHOD_COPY_QLT:  s = _T("::/12.3_.htm");  break;
	case DLGID_METHOD_CHANGE_QLT:  s = _T("::/12.5_.htm");  break;
	case DLGID_METHOD_CREATE_QNT:  s = _T("::/12.2_.htm");  break;
	case DLGID_METHOD_COPY_QNT:  s = _T("::/12.4_.htm");  break;
	case DLGID_METHOD_CHANGE_QNT:  s = _T("::/12.6_.htm");  break;
	}

	return s;
}
