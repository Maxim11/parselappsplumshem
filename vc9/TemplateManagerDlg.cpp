#include "stdafx.h"

#include "BranchInfo.h"
#include "TemplateManagerDlg.h"
#include "OptimModelDlg2.h"
#include "MainFrm.h"


IMPLEMENT_DYNAMIC(CTemplateManagerDlg, CDialog)

CTemplateManagerDlg::CTemplateManagerDlg(CModelData* Mod, CWnd* pParent)
	: CDialog(CTemplateManagerDlg::IDD, pParent)
{
	Templates = GetOptimTemplates();
	CurModel = Mod;
}

CTemplateManagerDlg::~CTemplateManagerDlg()
{
}

void CTemplateManagerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_TEMPLATES_LIST, m_list);
}

BEGIN_MESSAGE_MAP(CTemplateManagerDlg, CDialog)

	ON_BN_CLICKED(IDHELP, OnHelp)
	ON_BN_CLICKED(IDC_LOAD, OnLoad)
	ON_BN_CLICKED(IDC_CREATE, OnCreate)
	ON_BN_CLICKED(IDC_ADD, OnAdd)
	ON_BN_CLICKED(IDC_EDIT, OnEdit)
	ON_BN_CLICKED(IDC_REMOVE, OnRemove)

END_MESSAGE_MAP()

BOOL CTemplateManagerDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	SetWindowText(ParcelLoadString(STRID_TEMPLATE_MANAGER_HDR));

	CString s;
	int i;

	s = ParcelLoadString(STRID_TEMPLATE_MANAGER_COLNUM);
	m_list.InsertColumn(0, s, LVCFMT_LEFT, 0, 0);
	s = ParcelLoadString(STRID_TEMPLATE_MANAGER_COLNAME);
	m_list.InsertColumn(1, s, LVCFMT_LEFT, 0, 1);
	s = ParcelLoadString(STRID_TEMPLATE_MANAGER_COLNOTE);
	m_list.InsertColumn(2, s, LVCFMT_LEFT, 0, 2);
	s = ParcelLoadString(STRID_TEMPLATE_MANAGER_COLNSTEP);
	m_list.InsertColumn(3, s, LVCFMT_LEFT, 0, 3);
	s = ParcelLoadString(STRID_TEMPLATE_MANAGER_COLDATA);
	m_list.InsertColumn(4, s, LVCFMT_LEFT, 0, 4);
	for(i=0; i<5; i++)
		m_list.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	m_list.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

	FillList();

	GetDlgItem(IDCANCEL)->SetWindowText(ParcelLoadString(STRID_TEMPLATE_MANAGER_EXIT));
	GetDlgItem(IDHELP)->SetWindowText(ParcelLoadString(STRID_TEMPLATE_MANAGER_HELP));
	GetDlgItem(IDC_LOAD)->SetWindowText(ParcelLoadString(STRID_TEMPLATE_MANAGER_LOAD));
	GetDlgItem(IDC_CREATE)->SetWindowText(ParcelLoadString(STRID_TEMPLATE_MANAGER_CREATE));
	GetDlgItem(IDC_ADD)->SetWindowText(ParcelLoadString(STRID_TEMPLATE_MANAGER_ADD));
	GetDlgItem(IDC_EDIT)->SetWindowText(ParcelLoadString(STRID_TEMPLATE_MANAGER_EDIT));
	GetDlgItem(IDC_REMOVE)->SetWindowText(ParcelLoadString(STRID_TEMPLATE_MANAGER_REMOVE));

	GetDlgItem(IDHELP)->EnableWindow(pFrame->IsExistHelp());

	return true;
}

void CTemplateManagerDlg::OnLoad()
{
	CString Title = ParcelLoadString(STRID_STDTITLE_TMPL_BASE_LOAD);
	CString Filter = LoadString(IDS_TMPLFILE_FILTER);
	CString DefExt = LoadString(IDS_TMPLFILE_DEFEXT);

	CString FileName = (Templates->IsLoaded()) ? Templates->GetBaseName() : Templates->GetBaseDefName();

	CFileDialog Dialog2(TRUE, DefExt, FileName,
					    OFN_EXPLORER | OFN_HIDEREADONLY | OFN_PATHMUSTEXIST | OFN_OVERWRITEPROMPT,
					    Filter, this);

	Dialog2.m_ofn.lpstrTitle = Title;

	if(Dialog2.DoModal() == IDOK)
	{
		FileName = Dialog2.GetPathName();
		if(FileName.IsEmpty())
		{
			ParcelError(ParcelLoadString(STRID_TEMPLATE_MANAGER_ERROR_FNEMPTY));
			return;
		}
		if(!PathFileExists(FileName))
		{
			ParcelError(ParcelLoadString(STRID_TEMPLATE_MANAGER_ERROR_FILENOEXIST));
			return;
		}

		if(!Templates->LoadBase(FileName, true))
			return;

		FillList(0);
	}
}

void CTemplateManagerDlg::OnCreate()
{
	CString Title = ParcelLoadString(STRID_STDTITLE_TMPL_BASE_NEW);
	CString Filter = LoadString(IDS_TMPLFILE_FILTER);
	CString DefExt = LoadString(IDS_TMPLFILE_DEFEXT);

	CString FileName = Templates->GetBaseDefName();

	CFileDialog Dialog(FALSE, DefExt, FileName,
					   OFN_EXPLORER | OFN_HIDEREADONLY |  OFN_PATHMUSTEXIST | OFN_OVERWRITEPROMPT,
					   Filter, this);

	Dialog.m_ofn.lpstrTitle = Title;

	if(Dialog.DoModal() == IDOK)
	{
		FileName = Dialog.GetPathName();
		if(FileName.IsEmpty())
		{
			ParcelError(ParcelLoadString(STRID_TEMPLATE_MANAGER_ERROR_FNEMPTY));
			return;
		}

		ParcelWait(true);

		Templates->Database.Close();

		if(PathFileExists(FileName))
			DeleteFile(FileName);

		if(!Templates->Database.CreateDatabase(FileName))
		{
			ParcelWait(false);
			ParcelError(ParcelLoadString(STRID_TEMPLATE_MANAGER_ERROR_DBNOCREATE));
			return;
		}
		if(!Templates->LoadDatabase())
		{
			ParcelWait(false);
			ParcelError(ParcelLoadString(STRID_TEMPLATE_MANAGER_ERROR_NOLOAD));
			return;
		}

		Templates->SetBaseName(FileName);

		ParcelWait(false);

		FillList(0);
	}
}

void CTemplateManagerDlg::OnAdd()
{
	if(!CurModel->LoadSpectraData(true, true))
		return;

	Template NewTmpl;

	COptimModelDlg2 OptDlg(CurModel, &NewTmpl, C_OPT_MODE_TMPLCREATE);
	int res = (OptDlg.DoModal() == IDOK);

	CurModel->FreeSpectraData();

	if(res)
	{
		Templates->AddTemplate(&NewTmpl);
		FillList(0);
	}
}

void CTemplateManagerDlg::OnEdit()
{
	int sel = m_list.GetNextItem(-1, LVNI_SELECTED);

	if(sel < 0 || sel >= Templates->GetNTemplates())
		return;

	if(CurModel->GetNSamplesForValid() == 0 && Templates->GetTemplate(sel)->IsUseSEV())
	{
		ParcelError(ParcelLoadString(STRID_TEMPLATE_MANAGER_ERROR_SEV));
		return;
	}

	CProjectData* Prj = GetProjectByHItem(CurModel->GetParentHItem());
	if(!Templates->GetTemplate(sel)->CheckPrjSettings(Prj))
	{
		ParcelError(ParcelLoadString(STRID_TEMPLATE_MANAGER_ERROR_PRJ));
		return;
	}

	if(!CurModel->LoadSpectraData(true, true))
		return;

	COptimModelDlg2 OptDlg(CurModel, Templates->GetTemplate(sel), C_OPT_MODE_TMPLEDIT);
	int res = (OptDlg.DoModal() == IDOK);

	CurModel->FreeSpectraData();

	if(res)
	{
		Templates->ChangeTemplate(sel);
		FillList(sel);
	}
}

void CTemplateManagerDlg::OnRemove()
{
	if(!ParcelConfirm(ParcelLoadString(STRID_TEMPLATE_MANAGER_ASK_DEL), false))
		return;

	int Ind = -1, nsel = m_list.GetSelectedCount();
	if(nsel <= 0)
		return;

	for(int i=0; i<nsel; i++)
	{
		Ind = m_list.GetNextItem(Ind, LVNI_SELECTED);
		if(Ind < 0 && Ind >= Templates->GetNTemplates())
			break;

		Templates->DeleteTemplate(Ind);
	}

	FillList(0);
}
/*
void CTemplateManagerDlg::OnTemplateSelChange(NMHDR*, LRESULT *pResult)
{
	int N = Templates->GetNTemplates();
	int sel = m_list.GetNextItem(-1, LVNI_SELECTED);

	if(sel < 0 || sel >= N)
		sel = 0;

	*pResult = 0;
}
*/
void CTemplateManagerDlg::FillList(int sel)
{
	m_list.DeleteAllItems();

	CString s;
	int i, N = Templates->GetNTemplates();

	for(i=0; i<N; i++)
	{
		Template* Tmpl = Templates->GetTemplate(i);
		s.Format(cFmt1, i+1);
		m_list.InsertItem(i, s);
		s = Tmpl->Name;
		m_list.SetItemText(i, 1, s);
		s = Tmpl->Note;
		m_list.SetItemText(i, 2, s);
		s.Format(cFmt1, int(Tmpl->Steps.size() - 1));
		m_list.SetItemText(i, 3, s);
		s = Tmpl->Date.Format(cFmtDate);
		m_list.SetItemText(i, 4, s);
	}
	for(i=0; i<5; i++)
		m_list.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);

	if(sel < 0 || sel >= N)
		sel = 0;
	m_list.SetItemState(sel, LVIS_SELECTED, LVIS_SELECTED);

	CString Name = Templates->IsLoaded() ? Templates->GetBaseNameShort() : ParcelLoadString(STRID_TEMPLATE_MANAGER_NO);
	s.Format(ParcelLoadString(STRID_TEMPLATE_MANAGER_CURBASE), Name, Templates->GetNTemplates());
	GetDlgItem(IDC_STATIC_1)->SetWindowText(s);

	Name = Templates->IsLoaded() ? Templates->GetBaseName() : ParcelLoadString(STRID_TEMPLATE_MANAGER_NO);
	s.Format(ParcelLoadString(STRID_TEMPLATE_MANAGER_CURPATH), Name);
	GetDlgItem(IDC_STATIC_2)->SetWindowText(s);

	GetDlgItem(IDC_ADD)->EnableWindow(Templates->IsLoaded());
	GetDlgItem(IDC_REMOVE)->EnableWindow(N > 0);
	GetDlgItem(IDC_EDIT)->EnableWindow(N > 0);
}

void CTemplateManagerDlg::OnHelp()
{
	int IdHelp = DLGID_MODEL_TEMPLATE_MANAGER;

	pFrame->ShowHelp(IdHelp);
}
