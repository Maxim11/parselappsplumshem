#ifndef _SAVE_METHOD_WAIT_DLG_H_
#define _SAVE_METHOD_WAIT_DLG_H_

#pragma once

#include "WaitingDlg.h"
#include "ProjectData.h"
//#include "PRDBMethod.h"

///////////////////////////////////////////////////////////////////////////////////////////////////
// ������ ��������� �������� ���������� ��������������� ������

class CSaveMethodWaitDlg : public CWaitingDlg
{
	DECLARE_DYNAMIC(CSaveMethodWaitDlg)

public:

	CSaveMethodWaitDlg(CProjectData* prj, mtddb::PRMethodQnt* qnt, mtddb::PRMethodQlt* qlt, CString fname, CWnd* pParent = NULL);	// �����������
	virtual ~CSaveMethodWaitDlg();																				// ����������

	enum { IDD = CWaitingDlg::IDD };	// ������������� �������

protected:

	CString FileName;
	CProjectData* PrjData;
	mtddb::PRMethodQnt* pQNT;
	mtddb::PRMethodQlt* pQLT;

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();						// ������������� �������

	virtual void OK();				// ��������� �������� ����� �� �������
	virtual void Stop();			// ��������� ����������� ����� �� �������
	virtual void Work();			// ������ �������� �������

	DECLARE_MESSAGE_MAP()
};

#endif
