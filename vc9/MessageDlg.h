#ifndef _MESSAGE_DLG_H_
#define _MESSAGE_DLG_H_

#pragma once

#include "Resource.h"


///////////////////////////////////////////////////////////////////////////////////////////////////
// ������ �������������� ��������� � ��������

const int C_MESSAGE_INFO		= 0;
const int C_MESSAGE_ERROR		= 1;
const int C_MESSAGE_CONFIRM_YES	= 2;
const int C_MESSAGE_CONFIRM_NO	= 3;

class CMessageDlg : public CDialog
{
	DECLARE_DYNAMIC(CMessageDlg)

public:

	CMessageDlg(int type, CString text, CWnd* pParent = NULL);	// �����������
	virtual ~CMessageDlg();										// ����������

	enum { IDD = IDD_MESSAGE };	// ������������� �������

	int Type;
	CString Comment;

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();						// ������������� �������

	virtual void OnOK();
	virtual void OnCancel();

	afx_msg void OnYes();
	afx_msg void OnNo();
	afx_msg void OnClose();

	DECLARE_MESSAGE_MAP()
};

#endif
