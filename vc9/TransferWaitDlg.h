#ifndef _TRANSFER_WAIT_DLG_H_
#define _TRANSFER_WAIT_DLG_H_

#pragma once

#include "WaitingDlg.h"
#include "TransferCalculate.h"


///////////////////////////////////////////////////////////////////////////////////////////////////
// ������ ��������� �������� ���������� ������������� ������

// ��������� ���� ��������

const int C_MODE_TRANSFER_NORMAL	= 0;	// ������� � ����������
const int C_MODE_TRANSFER_WITHOUT	= 1;	// ������� ��� ���������

class CTransferWaitDlg : public CWaitingDlg
{
	DECLARE_DYNAMIC(CTransferWaitDlg)

public:

	CTransferWaitDlg(CTransferCalculate* trans, int mode, CWnd* pParent = NULL);	// �����������
	virtual ~CTransferWaitDlg();													// ����������

	enum { IDD = CWaitingDlg::IDD };	// ������������� �������

protected:

	CTransferCalculate* TransCalc;	// ��������� �� ������, � ������� ������������ �����������

	int Mode;

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();						// ������������� �������

	virtual void OK();				// ��������� �������� ����� �� �������
	virtual void Stop();			// ��������� ����������� ����� �� �������
	virtual void Work();			// ������ �������� �������

	DECLARE_MESSAGE_MAP()
};

#endif
