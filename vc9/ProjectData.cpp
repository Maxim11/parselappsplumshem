#include "stdafx.h"

#include "RootData.h"
#include "BranchInfo.h"
#include "ProjectData.h"
#include "EditSampleListDlg.h"
#include "EditParamListDlg.h"
#include "CreateMethodDlg.h"
#include "CreateTransferDlg.h"
#include "CreateTratsferPrjDlg.h"
#include "CreateTransSSTDlg.h"
#include "CreateSSTWaitDlg.h"
#include "SaveMethodWaitDlg.h"
#include "EditTransferDlg.h"
#include "CreateModelDlg.h"
#include "ModelResultDlg.h"
#include "ModelQltResultDlg.h"
#include "SaveModelWaitDlg.h"
#include "LoadSpectraPBDlg.h"
#include "SpLumChem.h"

#include <math.h>


bool PSamUp(CSampleData& Sam1, CSampleData& Sam2)
{
	CProjectData *Prj = GetProjectByHItem(Sam1.GetParentHItem());
	if(Prj == NULL)
		return false;

	if(Prj->GetSamSortName() == cSortTime)
	{
		CTime t1 = Sam1.GetLastSpecDate();
		CTime t2 = Sam2.GetLastSpecDate();
		if(t1 == t2)
		{
			return (Sam1.GetName().Compare(Sam2.GetName()) < 0);
		}
		return t1 < t2;
	}
	else
	{
		ReferenceData *Ref1 = Sam1.GetReferenceData(Prj->GetSamSortName());
		ReferenceData *Ref2 = Sam2.GetReferenceData(Prj->GetSamSortName());

		if(Ref1 == NULL || Ref2 == NULL)
			return (Sam1.GetName().Compare(Sam2.GetName()) < 0);

		if(Ref1->bNoValue && !Ref2->bNoValue)
			return true;
		if(Ref2->bNoValue)
			return false;

		return Ref1->Value < Ref2->Value;
	}
}

bool PSamDown(CSampleData& Sam1, CSampleData& Sam2)
{
	CProjectData *Prj = GetProjectByHItem(Sam1.GetParentHItem());
	if(Prj == NULL)
		return false;

	if(Prj->GetSamSortName() == cSortTime)
	{
		CTime t1 = Sam1.GetLastSpecDate();
		CTime t2 = Sam2.GetLastSpecDate();
		if(t1 == t2)
		{
			return (Sam1.GetName().Compare(Sam2.GetName()) > 0);
		}
		return t1 > t2;
	}
	else
	{
		ReferenceData *Ref1 = Sam1.GetReferenceData(Prj->GetSamSortName());
		ReferenceData *Ref2 = Sam2.GetReferenceData(Prj->GetSamSortName());

		if(Ref1 == NULL || Ref2 == NULL)
			return (Sam1.GetName().Compare(Sam2.GetName()) > 0);

		if(Ref1->bNoValue)
			return false;

		if(Ref2->bNoValue)
			return true;

		return Ref1->Value > Ref2->Value;
	}
}

CProjectData::CProjectData(HTREEITEM hParent, bool sst)
{
	hItem = NULL;
	hParentItem = hParent;
	hParamSetItem = NULL;
	hSampleSetItem = NULL;
	hModelSetItem = NULL;
	hMethodSetItem = NULL;

	CDeviceData* ParDev = GetDeviceByHItem(hParentItem);

	DevNumber = ParDev->GetNumber();

	Name = ParcelLoadString(STRID_DEFNAME_PRJ);

	SST = sst;

	TransOwn = NULL;

	ZeroFilling = 0;
	RefCanalResolution = 1;
	SubCanalApodization = C_APODIZE_BESSEL;
	RefCanalApodization = C_APODIZE_BESSEL;

	BathLength = 18.;
	LowLimTemp = 5;
	UpperLimTemp = 40;
	MaxDiffTemp = 20;

	int FType = ParDev->GetType();
	switch(FType)
	{
	case C_DEV_TYPE_02:
	case C_DEV_TYPE_02M:
		LowLimSpecRange = 400;
		UpperLimSpecRange = 7500;
		SetSubCanalResolution(4);
		UseRefCanal = false;
		NScansSubsample = 240;
		NMeasuresSubsample = 1;
		NScansStandart = 240;
		NMeasuresStandart = C_MEASURE_ONE;
		break;
	case C_DEV_TYPE_10:
		LowLimSpecRange = 8000;
		UpperLimSpecRange = 14000;
		SetSubCanalResolution(2);
		UseRefCanal = false;
		NScansSubsample = (sst) ? 43 : 12;
		NMeasuresSubsample = (sst) ? 3 : 8;
		NScansStandart = (sst) ? 64 : 35;
		NMeasuresStandart = C_MEASURE_TWO;
		break;
	case C_DEV_TYPE_10M:
	case C_DEV_TYPE_12:
		LowLimSpecRange = 8000;
		UpperLimSpecRange = 14000;
		SetSubCanalResolution(2);
		UseRefCanal = true;
		NScansSubsample = (sst) ? 43 : 12;
		NMeasuresSubsample = (sst) ? 3 : 8;
		NScansStandart = (sst) ? 64 : 35;
		NMeasuresStandart = C_MEASURE_TWO;
		break;
	case C_DEV_TYPE_20:
		LowLimSpecRange = 4500;
		UpperLimSpecRange = 10000;
		SetSubCanalResolution(2);
		UseRefCanal = true;
		NScansSubsample = 240;
		NMeasuresSubsample = 1;
		NScansStandart = 240;
		NMeasuresStandart = C_MEASURE_ONE;
		break;
	case C_DEV_TYPE_40:
	case C_DEV_TYPE_12M:
		LowLimSpecRange = 8000;
		UpperLimSpecRange = 14000;
		SetSubCanalResolution(2);
		UseRefCanal = true;
		NScansSubsample = (sst) ? 128 : 8;
		NMeasuresSubsample = (sst) ? 1 : 10;
		NScansStandart = (sst) ? 64 : 40;
		NMeasuresStandart = C_MEASURE_TWO;
		break;
	case C_DEV_TYPE_08:
		LowLimSpecRange = 400;
		UpperLimSpecRange = 7800;
		SetSubCanalResolution(4);
		UseRefCanal = false;
		NScansSubsample = 240;
		NMeasuresSubsample = 1;
		NScansStandart = 240;
		NMeasuresStandart = C_MEASURE_ONE;
		break;
	case C_DEV_TYPE_100:
		LowLimSpecRange = 8000;
		UpperLimSpecRange = 14000;
		SetSubCanalResolution(2);
		UseRefCanal = false;
		NScansSubsample = 100;
		NMeasuresSubsample = 1;
		NScansStandart = 100;
		NMeasuresStandart = C_MEASURE_ONE;
		break;
	case C_DEV_TYPE_IMIT:
		LowLimSpecRange = 1;
		UpperLimSpecRange = 50;
		SetSubCanalResolution(2);
		UseRefCanal = false;
		NScansSubsample = 240;
		NMeasuresSubsample = 1;
		NScansStandart = 240;
		NMeasuresStandart = C_MEASURE_ONE;
	}

	SamSortName = cEmpty;
	SamSortDir = true;

	CopyMode = C_PRJ_COPY_ONLYSETTINGS;
	pCopies = NULL;
}

CProjectData::~CProjectData()
{
	SpecPoints.clear();

	FillingList.clear();

	Params.clear();
	Transfers.clear();
	Models.clear();
	Methods.clear();
}

void CProjectData::Copy(CProjectData& data)
{
	hItem = data.GetHItem();
	hParentItem = data.GetParentHItem();
	hParamSetItem = data.GetParamSetHItem();
	hSampleSetItem = data.GetSampleSetHItem();
	hModelSetItem = data.GetModelSetHItem();
	hMethodSetItem = data.GetMethodSetHItem();

	Name = data.GetName();
	SST = data.IsSST();
	NMeasuresStandart = data.GetNMeasuresStandart();
	SetSubCanalResolution(data.GetSubCanalResolution());
	SubCanalApodization = data.GetSubCanalApodization();
	ZeroFilling = data.GetZeroFillingInd();
	RefCanalResolution = data.GetRefCanalResolution();
	RefCanalApodization = data.GetRefCanalApodization();

	LowLimSpecRange = data.GetLowLimSpecRange();
	UpperLimSpecRange = data.GetUpperLimSpecRange();
	NScansSubsample = data.GetNScansSubsample();
	NMeasuresSubsample = data.GetNMeasuresSubsample();
	NScansStandart = data.GetNScansStandart();
	BathLength = data.GetBathLength();
	UseRefCanal = data.IsUseRefCanal();
	LowLimTemp = data.GetLowLimTemp();
	UpperLimTemp = data.GetUpperLimTemp();
	MaxDiffTemp = data.GetMaxDiffTemp();
	SamSortName = data.GetSamSortName();
	SamSortDir = data.GetSamSortDir();

	if(CopyMode == C_PRJ_COPY_FORCHANGE)
	{
		Transfers.clear();
		if(data.GetTransOwn() != NULL)
		{
			Transfers.push_back(*(data.GetTransOwn()));
			TransOwn = GetTransfer(cOwn);
		}
	}
}

void CProjectData::SortSamples(CString name, bool dir_up)
{
	SamSortName = name;
	SamSortDir = dir_up;

	SortAllSamples();
}

bool CProjectData::SortSamples(int ntype, bool bdir, int idxpar)
{
	if(idxpar >= GetNParams())
	{
		return false;
	}


	CString oldSSN = SamSortName;
	bool oldSSD = SamSortDir;

	switch(ntype)
	{
	case C_SORT_SAMPLE:
		SamSortName = cEmpty;
		break;
	case C_SORT_TIME:
		SamSortName = cSortTime;
		break;
	case C_SORT_PARAM:
		SamSortName = GetParam(idxpar)->GetName();
		break;
	default:
		return false;
	}
	SamSortDir = bdir;
	SortAllSamples();

	if(oldSSN.Compare(SamSortName) || oldSSD != SamSortDir)
	{
		int Number = GetDeviceByHItem(hParentItem)->GetNumber();

		prdb::PRAnalisisProject Obj;
		GetDataForBase(Obj);
		if(!GetDatabase()->ProjectAddEx(Number, Name, &Obj))
			return false;
	}

	return true;
}

void CProjectData::SortAllSamples()
{
	ItTrans itT;
	for(itT=Transfers.begin(); itT!=Transfers.end(); ++itT)
	{
		if(SamSortDir)
			itT->Samples.sort(&PSamUp);
		else
			itT->Samples.sort(&PSamDown);
	}
}

void CProjectData::SortSamples(CString transname)
{
	CTransferData* Trans = GetTransfer(transname);
	if(Trans == NULL)
		return;

	if(SamSortDir)
		Trans->Samples.sort(&PSamUp);
	else
		Trans->Samples.sort(&PSamDown);
}

HTREEITEM CProjectData::GetHItem()
{
	return hItem;
}

void CProjectData::SetHItem(HTREEITEM item)
{
	hItem = item;
}

HTREEITEM CProjectData::GetParentHItem()
{
	return hParentItem;
}

void CProjectData::SetParentHItem(HTREEITEM item)
{
	hParentItem = item;
}

int CProjectData::GetDevNumber()
{
	return DevNumber;
}

HTREEITEM CProjectData::GetParamSetHItem()
{
	return hParamSetItem;
}

void CProjectData::SetParamSetHItem(HTREEITEM item)
{
	hParamSetItem = item;
}

HTREEITEM CProjectData::GetSampleSetHItem()
{
	return hSampleSetItem;
}

void CProjectData::SetSampleSetHItem(HTREEITEM item)
{
	hSampleSetItem = item;
}

HTREEITEM CProjectData::GetSampleTransferHItem(CString name)
{
	CTransferData *Trans = GetTransfer(name);
	if(Trans == NULL)
		return NULL;

	return Trans->GetHItem();
}

bool CProjectData::SetSampleTransferHItem(CString name, HTREEITEM item)
{
	CTransferData *Trans = GetTransfer(name);
	if(Trans == NULL)
		return false;

	Trans->SetHItem(item);

	return true;
}

HTREEITEM CProjectData::GetModelSetHItem()
{
	return hModelSetItem;
}

void CProjectData::SetModelSetHItem(HTREEITEM item)
{
	hModelSetItem = item;
}

HTREEITEM CProjectData::GetMethodSetHItem()
{
	return hMethodSetItem;
}

void CProjectData::SetMethodSetHItem(HTREEITEM item)
{
	hMethodSetItem = item;
}

CString CProjectData::GetName()
{
	return Name;
}

void CProjectData::SetName(CString name)
{
	Name = name;
}

bool CProjectData::IsSST()
{
	return SST;
}

void CProjectData::SetSST(bool sst)
{
	SST = sst;
}

CTransferData* CProjectData::GetTransSST()
{
	ItTrans it;
	for(it=Transfers.begin(); it!=Transfers.end(); ++it)
		if(it->GetType() == C_TRANS_TYPE_SST)
			return &(*it);

	return NULL;
}

HTREEITEM CProjectData::GetTransSSTHItem()
{
	if(GetTransSST() == NULL)
		return 0;

	return GetTransSST()->GetHItem();
}

CTransferData* CProjectData::GetTransOwn()
{
	return TransOwn;
}

HTREEITEM CProjectData::GetTransOwnHItem()
{
	return TransOwn->GetHItem();
}

int CProjectData::GetLowLimSpecRange()
{
	return LowLimSpecRange;
}

void CProjectData::SetLowLimSpecRange(int lim)
{
	LowLimSpecRange = lim;
}

int CProjectData::GetUpperLimSpecRange()
{
	return UpperLimSpecRange;
}

void CProjectData::SetUpperLimSpecRange(int lim)
{
	UpperLimSpecRange = lim;
}

int CProjectData::GetNScansSubsample()
{
	return NScansSubsample;
}

void CProjectData::SetNScansSubsample(int num)
{
	NScansSubsample = num;
}

int CProjectData::GetNMeasuresSubsample()
{
	return NMeasuresSubsample;
}

void CProjectData::SetNMeasuresSubsample(int num)
{
	NMeasuresSubsample = num;
}

int CProjectData::GetNScansStandart()
{
	return NScansStandart;
}

void CProjectData::SetNScansStandart(int scans)
{
	NScansStandart = scans;
}

double CProjectData::GetBathLength()
{
	return BathLength;
}

void CProjectData::SetBathLength(double len)
{
	BathLength = len;
}

bool CProjectData::IsUseRefCanal()
{
	return UseRefCanal;
}

CString CProjectData::GetUseRefCanalStr()
{
	return (UseRefCanal) ? ParcelLoadString(STRID_FIELD_YES) : ParcelLoadString(STRID_FIELD_NO);
}

void CProjectData::SetUseRefCanal(bool use)
{
	UseRefCanal = use;
}

int CProjectData::GetLowLimTemp()
{
	return LowLimTemp;
}

void CProjectData::SetLowLimTemp(int lim)
{
	LowLimTemp = lim;
}

int CProjectData::GetUpperLimTemp()
{
	return UpperLimTemp;
}

void CProjectData::SetUpperLimTemp(int lim)
{
	UpperLimTemp = lim;
}

int CProjectData::GetMaxDiffTemp()
{
	return MaxDiffTemp;
}

void CProjectData::SetMaxDiffTemp(int diff)
{
	MaxDiffTemp = diff;
}

int CProjectData::GetNMeasuresStandart()
{
	return NMeasuresStandart;
}

bool CProjectData::SetNMeasuresStandart(int ind)
{
	if(ind < C_MEASURE_ONE || ind > C_MEASURE_EVERY)
		return false;

	NMeasuresStandart = ind;

	return true;
}

int CProjectData::GetSubCanalResolution()
{
	return SubCanalResolution;
}

double CProjectData::GetSubCanalResolutionMean()
{
	CDeviceData *Dev = GetDeviceByHItem(hParentItem);

	return Dev->GetResolutionFromList(SubCanalResolution);
}

bool CProjectData::SetSubCanalResolution(int ind)
{
	SubCanalResolution = ind;

	if(SubCanalResolution < RefCanalResolution)
		RefCanalResolution = SubCanalResolution;

	FillZeroFillingList();

	return true;
}

int CProjectData::GetSubCanalApodization()
{
	return SubCanalApodization;
}

bool CProjectData::SetSubCanalApodization(int ind)
{
	if(ind < C_APODIZE_RECTAN || ind > C_APODIZE_BESSEL)
		return false;

	SubCanalApodization = ind;

	return true;
}

int CProjectData::GetZeroFillingInd()
{
	return ZeroFilling;
}

int CProjectData::GetZeroFillingMean()
{
	return FillingList[ZeroFilling];
}

CString CProjectData::GetZeroFillingAsStr()
{
	CString s;
	s.Format(ParcelLoadString(STRID_LIST_ZEROFILL), GetZeroFillingMean());

	return s;
}

bool CProjectData::SetZeroFillingInd(int ind)
{
	if(ind < 0 || ind >= int(FillingList.size()))
		return false;

	ZeroFilling = ind;

	return true;
}

bool CProjectData::SetZeroFillingMean(int val)
{
	int i;
	ItInt it;
	for(i=0, it=FillingList.begin(); it!=FillingList.end(); ++it, i++)
		if((*it) == val)
		{
			ZeroFilling = i;
			return true;
		}

	return false;

}

bool CProjectData::IsRefCanalEnable()
{
	CDeviceData* ParDev = GetDeviceByHItem(hParentItem);
	if(ParDev == NULL)
		return false;

	int type = ParDev->GetType();

	return (type == C_DEV_TYPE_10M || type == C_DEV_TYPE_20 || type == C_DEV_TYPE_40 || type == C_DEV_TYPE_12 || type == C_DEV_TYPE_12M);
}

int CProjectData::GetRefCanalResolution()
{
	return RefCanalResolution;
}

double CProjectData::GetRefCanalResolutionMean()
{
	CDeviceData *Dev = GetDeviceByHItem(hParentItem);

	return Dev->GetResolutionFromList(RefCanalResolution);
}

bool CProjectData::SetRefCanalResolution(int ind)
{
	CDeviceData *Dev = GetDeviceByHItem(hParentItem);
	if(ind < 0 || ind >= Dev->GetNResolutions())
		return false;

	if(SubCanalResolution < RefCanalResolution)
		return false;

	RefCanalResolution = ind;

	return true;
}

int CProjectData::GetRefCanalApodization()
{
	return RefCanalApodization;
}

bool CProjectData::SetRefCanalApodization(int ind)
{
	if(ind < C_APODIZE_RECTAN || ind > C_APODIZE_BESSEL)
		return false;

	RefCanalApodization = ind;

	return true;
}

CString CProjectData::GetSamSortName()
{
	return SamSortName;
}

void CProjectData::SetSamSortName(CString name)
{
	SamSortName = name;
}

bool CProjectData::GetSamSortDir()
{
	return SamSortDir;
}

void CProjectData::SetSamSortDir(bool flag)
{
	SamSortDir = flag;
}

int CProjectData::GetNSpecPoints()
{
	return int(SpecPoints.size());
}

double CProjectData::GetSpecPoint(int indSP)
{
	if(indSP < 0 || indSP >= GetNSpecPoints())
		return 0.;

	return SpecPoints[indSP];
}

int CProjectData::GetSpecPointInd(double value)
{
	int i;
	ItDbl it;
	for(i=0, it=SpecPoints.begin(); it!=SpecPoints.end(); ++it, i++)
	{
		if(fabs(value - (*it)) < 1.e-9)
			return i;
	}

	return -1;
}

int CProjectData::GetNParams()
{
	return int(Params.size());
}

CParamData* CProjectData::GetParam(CString name)
{
	ItPar it;
	for(it=Params.begin(); it!=Params.end(); ++it)
		if(!(*it).GetName().CompareNoCase(name))
			return &(*it);

	return NULL;
}
/*
CParamData* CProjectData::GetParamWithoutReg(CString name)
{
	CString name_reg = cTmpMakeLowerRUS(name);

	ItPar it;
	for(it=Params.begin(); it!=Params.end(); ++it)
	{
		CString param_reg = cTmpMakeLowerRUS(it->GetName());
		param_reg.MakeLower();
		if(!param_reg.Compare(name_reg))
			return &(*it);
	}

	return NULL;
}
*/
CParamData* CProjectData::GetParam(int ind)
{
	if(ind < 0 || ind >= int(Params.size()))
		return NULL;

	int i;
	ItPar it;
	for(i=0, it=Params.begin(); i<ind; i++)
		++it;

	return &(*it);
}

CParamData* CProjectData::GetLastParam()
{
	return GetParam(GetNParams() - 1);
}

bool CProjectData::AddParam(CParamData* NewPar)
{
	if(!GetDatabase()->BeginTrans())
		return false;

	prdb::PRIndex Obj;
	NewPar->GetDataForBase(Obj);
	if(!GetDatabase()->IndexAddEx(DevNumber, Name, cEmpty, &Obj))
	{
		GetDatabase()->Rollback();
		return false;
	}

	ItTrans itT;
	for(itT=Transfers.begin(); itT!=Transfers.end(); ++itT)
	{
		ItSam it;
		for(it=itT->Samples.begin(); it!=itT->Samples.end(); ++it)
		{
			CString ParName = NewPar->GetName();
			it->AddReferenceData(ParName);

			prdb::PRRefData Obj;
			it->GetRefDataForBase(ParName, Obj);
			if(!GetDatabase()->RefDataAddEx(DevNumber, Name, itT->GetName(), it->GetName(), &Obj))
			{
				GetDatabase()->Rollback();
				return false;
			}
		}
	}

	GetDatabase()->CommitTrans();

	Params.push_back((*NewPar));
	GetLastParam()->hParentItem = this->hItem;

	return true;
}

bool CProjectData::ChangeParam(CParamData* OldPar, CParamData* NewPar)
{
	if(!GetDatabase()->BeginTrans())
		return false;

	CString OldName = OldPar->GetName(), NewName = NewPar->GetName();

	prdb::PRIndex Obj;
	NewPar->GetDataForBase(Obj);
	if(!GetDatabase()->IndexAddEx(DevNumber, Name, OldName, &Obj))
	{
		GetDatabase()->Rollback();
		return false;
	}

	bool flag = (OldName.Compare(NewName) != 0);

	ItTrans itT;
	for(itT=Transfers.begin(); itT!=Transfers.end(); ++itT)
	{
		ItSam it;
		for(it=itT->Samples.begin(); it!=itT->Samples.end(); ++it)
		{
			ReferenceData* Ref = it->GetReferenceData(OldName);
			if(flag)
			{
				Ref->Name = NewName;
				if(!GetDatabase()->RefDataDelete(DevNumber, Name, itT->GetName(), it->GetName(), OldName))
				{
					GetDatabase()->Rollback();
					return false;
				}
			}

			prdb::PRRefData Obj;
			it->GetRefDataForBase(NewName, Obj);
			if(!GetDatabase()->RefDataAddEx(DevNumber, Name, itT->GetName(), it->GetName(), &Obj))
			{
				GetDatabase()->Rollback();
				return false;
			}
		}
	}

	GetDatabase()->CommitTrans();

	OldPar->Copy(*NewPar);

	return true;
}

bool CProjectData::EditParamList()
{
	CEditParamListDlg dlg(this, &Params);
	dlg.DoModal();

	return true;
}

bool CProjectData::DeleteParam(int ind)
{
	if(ind < 0 || ind >= int(Params.size()))
		return false;
	if(GetParamKeepStatus(ind) != C_KEEP_FREE)
		return false;

	int i;
	ItPar it;
	for(it=Params.begin(), i=0; it!=Params.end(); ++it, i++)
		if(i == ind)
			return DeleteParam(it->GetName());

	return false;

}

bool CProjectData::DeleteParam(CString name)
{
	ItPar it1;
	for(it1=Params.begin(); it1!=Params.end(); ++it1)
		if(!it1->GetName().Compare(name))
			break;

	if(it1 == Params.end())
		return false;

	if(!GetDatabase()->IndexDelete(DevNumber, Name, name))
		return false;

	ItTrans itT;
	for(itT=Transfers.begin(); itT!=Transfers.end(); ++itT)
	{
		ItSam it;
		for(it=itT->Samples.begin(); it!=itT->Samples.end(); ++it)
			it->DeleteReferenceData(name);
	}

	Params.erase(it1);

	return true;
}

int CProjectData::GetParamKeepStatus(int ind)
{
	if(ind < 0 || ind >= int(Params.size()))
		return C_KEEP_NOEDIT;

	return GetParamKeepStatus(GetParam(ind)->GetName());
}

int CProjectData::GetParamKeepStatus(CString name)
{
	if(IsSST() && cIsParamNameReserved(name))
		return C_KEEP_NOEDIT;

	ItMod it;
	for(it=Models.begin(); it!=Models.end(); ++it)
	{
		if(it->IsModelParamExist(name))
			return C_KEEP_NOEDIT;
	}

	return C_KEEP_FREE;
}

bool CProjectData::IsParamMethodExist(int ind)
{
	if(ind < 0 || ind >= int(Params.size()))
		return false;

	ItMtd it;
	for(it=Methods.begin(); it!=Methods.end(); ++it)
	{
		if(it->IsMethodRuleExist(GetParam(ind)->GetName()))
			return true;
	}

	return false;
}

int CProjectData::GetNTransfers()
{
	return int(Transfers.size());
}

CTransferData* CProjectData::GetTransfer(HTREEITEM item)
{
	ItTrans it;
	for(it=Transfers.begin(); it!=Transfers.end(); ++it)
		if(it->GetHItem() == item)
			return &(*it);

	return NULL;
}

CTransferData* CProjectData::GetTransfer(int ind)
{
	if(ind < 0 || ind >= int(Transfers.size()))
		return NULL;

	int i;
	ItTrans it;
	for(i=0, it=Transfers.begin(); i<ind; i++)
		++it;

	return &(*it);
}

CTransferData* CProjectData::GetTransfer(CString name)
{
	ItTrans it;
	for(it=Transfers.begin(); it!=Transfers.end(); ++it)
		if(!it->GetName().CompareNoCase(name))
			return &(*it);

	return NULL;
}

CTransferData* CProjectData::GetLastTransfer()
{
	return GetTransfer(GetNTransfers() - 1);
}

HTREEITEM CProjectData::GetLastTransferHItem()
{
	CTransferData* Trans = GetLastTransfer();
	if(Trans)
		return Trans->GetHItem();

	return NULL;
}

bool CProjectData::AddOwnTrans()
{
	CTransferData NewTrans(hItem);

	NewTrans.SetName(cOwn);
	NewTrans.SetType(C_TRANS_TYPE_OWN);
	NewTrans.SetDate();
	NewTrans.SetLowLimSpec(LowLimSpecRange);
	NewTrans.SetUpperLimSpec(UpperLimSpecRange);

	CDeviceData* Dev = GetDeviceByHItem(hParentItem);
	NewTrans.SetMasterDevType(Dev->GetType());
	NewTrans.SetMasterDevNumber(Dev->GetNumber());

	if(!AddTransfer(&NewTrans))
		return false;

	TransOwn = GetTransfer(cOwn);
	TransOwn->GetSpecPoints();

	return true;
}

bool CProjectData::ChangeOwnTransfer()
{
	TransOwn->SetLowLimSpec(LowLimSpecRange);
	TransOwn->SetUpperLimSpec(UpperLimSpecRange);

	prdb::PRTrans Obj;
	TransOwn->GetDataForBase(Obj);
	if(!GetDatabase()->TransAddEx(DevNumber, Name, cOwn, &Obj))
		return false;

	return true;
}

bool CProjectData::AddTransfer(CTransferData* NewTrans)
{
	NewTrans->SetDate();

	prdb::PRTrans Obj;
	NewTrans->GetDataForBase(Obj);
	if(!GetDatabase()->TransAddEx(DevNumber, Name, cEmpty, &Obj))
		return false;

	Transfers.push_back((*NewTrans));

	GetLastTransfer()->SetParentHItem(hItem);

	return true;
}

bool CProjectData::ProjectTransfer()
{
	CCreateTratsferPrjDlg dlg(hItem);
	if(dlg.DoModal() != IDOK)
		return false;

	return true;
}

CTransferData* CProjectData::AddNewTransfer()
{
	CTransferData NewTrans(hItem);

	CCreateTransferDlg dlg(&NewTrans);
	if(dlg.DoModal() != IDOK)
		return NULL;

	return GetLastTransfer();
}

CTransferData* CProjectData::AddSSTTransfer(CTreeCtrl* curTree)
{
	int i, N;
	for(i=0, N=0; i<GetNSamples(); i++)
		if(GetSample(i)->GetNSpectraUsed() > 0)
			N++;
	if(N < 4)
	{
		ParcelError(ParcelLoadString(STRID_ERROR_CREATE_SST_NOSAM));
		return NULL;
	}

	VPlate Plates;
	for(i=0; i<4; i++)
	{
		PlateData NewPlate(i);
		Plates.push_back(NewPlate);
	}

	CCreateTransSSTDlg dlg(this, &Plates);
	if(dlg.DoModal() != IDOK)
		return NULL;

	pTree = curTree;

	CCreateSSTWaitDlg dlg2(this, &Plates);
	if(dlg2.DoModal() != IDOK)
		return NULL;

	return GetLastTransfer();
}

bool CProjectData::CreateSST(VPlate* pPlates)
{
	CTransferData NewTrans(hItem);
	NewTrans.SetType(C_TRANS_TYPE_SST);
	NewTrans.SetAllPreproc(cEmpty);
	NewTrans.SetName(cSST);
	NewTrans.Plates.clear();
	copy(pPlates->begin(), pPlates->end(), inserter(NewTrans.Plates, NewTrans.Plates.begin()));

	prdb::PRTrans Obj;
	NewTrans.GetDataForBase(Obj);
	if(!GetDatabase()->TransAddEx(DevNumber, Name, cEmpty, &Obj))
	{
		GetDatabase()->Rollback();
		return false;
	}

	HTREEITEM hParentItem = GetSampleSetHItem();
	HTREEITEM hLastItem = GetLastTransferHItem();

	Transfers.push_back(NewTrans);
	CTransferData* Trans = GetLastTransfer();
	Trans->SetParentHItem(hItem);

	HTREEITEM hItem = pTree->InsertItem(TVIF_TEXT, Trans->GetName(), 0, 0, 0, 0, 0, hParentItem, hLastItem);

	Trans->SetHItem(hItem);

	pTree->SetItemImage(hItem, 11, 11);

	pTree->Select(hItem, TVGN_CARET);
	pTree->Expand(hParentItem, TVE_EXPAND);
	pTree->SetItemImage(hParentItem, 7, 7);

	PlateData* Plate0 = Trans->GetPlate(0);
	PlateData* Plate1 = Trans->GetPlate(1);
	PlateData* Plate2 = Trans->GetPlate(2);
	PlateData* Plate3 = Trans->GetPlate(3);
	if(Plate0 == NULL || Plate1 == NULL || Plate2 == NULL || Plate3 == NULL)
		return false;
	CSampleData* SData0 = GetSample(Plate0->SamName);
	CSampleData* SData1 = GetSample(Plate1->SamName);
	CSampleData* SData2 = GetSample(Plate2->SamName);
	CSampleData* SData3 = GetSample(Plate3->SamName);
	if(SData0 == NULL || SData1 == NULL || SData2 == NULL || SData3 == NULL)
		return false;

	ItSpc itS;
	ItInt itNum;
	int i, n;
	int N = int(SpecPoints.size());

	VLSI PSam;
	for(i=0; i<4; i++)
	{
		PlateData* PData = Trans->GetPlate(i);
		CSampleData* SData = GetSample(PData->SamName);

		LoadSampleInfo NewLSI;
		NewLSI.pSam = SData;
		for(itNum=PData->Spectra.begin(); itNum!=PData->Spectra.end(); ++itNum)
				NewLSI.Nums.push_back(*itNum);

		PSam.push_back(NewLSI);
	}

	CLoadSpectraPBDlg dlg3(&PSam);
	if(dlg3.DoModal() != IDOK)
	{
		SData0->FreeSpectraData();
		SData1->FreeSpectraData();
		SData2->FreeSpectraData();
		SData3->FreeSpectraData();

		return false;
	}

	VDbl S0, S1, S2, S3;
	for(i=0; i<4; i++)
	{
		PlateData* PData = NewTrans.GetPlate(i);
		CSampleData* SData = GetSample(PData->SamName);

		VDbl S;
		int count = 0;
		for(itNum=PData->Spectra.begin(); itNum!=PData->Spectra.end(); ++itNum)
		{
			Spectrum *CurSpec = SData->GetSpectrumByNum(*itNum);
			if(count == 0)
				copy(CurSpec->Data.begin(), CurSpec->Data.end(), inserter(S, S.begin()));
			else
				for(n=0; n<N; n++)
					S[n] += CurSpec->Data[n];

			count++;
		}
		if(count == 0)
			return false;

		VDbl InS, OutS;
		for(n=0; n<N; n++)
		{
			InS.push_back(S[n] / count);
			OutS.push_back(0.);
		}
		TRA2ABS(&InS, &OutS);

		if(i == 0)	copy(OutS.begin(), OutS.end(), inserter(S0, S0.begin()));
		if(i == 1)	copy(OutS.begin(), OutS.end(), inserter(S1, S1.begin()));
		if(i == 2)	copy(OutS.begin(), OutS.end(), inserter(S2, S2.begin()));
		if(i == 3)	copy(OutS.begin(), OutS.end(), inserter(S3, S3.begin()));
	}

	SData0->FreeSpectraData();
	SData1->FreeSpectraData();
	SData2->FreeSpectraData();
	SData3->FreeSpectraData();


	VDbl S11, S12, S13, S14, S21, S22, S23, S24, S31, S32, S33, S34;
	double val;
	for(n=0; n<N; n++)
	{
		val = (S1[n] - S0[n] - Plate1->R) * Plate1->T1 / Plate1->T0 + Plate1->R;
		S11.push_back(val);
		val = (S1[n] - S0[n] - Plate1->R) * Plate1->T2 / Plate1->T0 + Plate1->R;
		S12.push_back(val);
		val = (S1[n] - S0[n] - Plate1->R) * Plate1->T3 / Plate1->T0 + Plate1->R;
		S13.push_back(val);
		val = (S1[n] - S0[n] - Plate1->R) * Plate1->T4 / Plate1->T0 + Plate1->R;
		S14.push_back(val);

		val = (S2[n] - S0[n] - Plate2->R) * Plate2->T1 / Plate2->T0 + Plate2->R;
		S21.push_back(val);
		val = (S2[n] - S0[n] - Plate2->R) * Plate2->T2 / Plate2->T0 + Plate2->R;
		S22.push_back(val);
		val = (S2[n] - S0[n] - Plate2->R) * Plate2->T3 / Plate2->T0 + Plate2->R;
		S23.push_back(val);
		val = (S2[n] - S0[n] - Plate2->R) * Plate2->T4 / Plate2->T0 + Plate2->R;
		S24.push_back(val);

		val = (S3[n] - S0[n] - Plate3->R) * Plate3->T1 / Plate3->T0 + Plate3->R;
		S31.push_back(val);
		val = (S3[n] - S0[n] - Plate3->R) * Plate3->T2 / Plate3->T0 + Plate3->R;
		S32.push_back(val);
		val = (S3[n] - S0[n] - Plate3->R) * Plate3->T3 / Plate3->T0 + Plate3->R;
		S33.push_back(val);
		val = (S3[n] - S0[n] - Plate3->R) * Plate3->T4 / Plate3->T0 + Plate3->R;
		S34.push_back(val);
	}

	MDbl Scalib1, Scalib2, Scalib3;
	Scalib1.push_back(S11);
	Scalib1.push_back(S12);
	Scalib1.push_back(S13);
	Scalib1.push_back(S14);
	Scalib2.push_back(S21);
	Scalib2.push_back(S22);
	Scalib2.push_back(S23);
	Scalib2.push_back(S24);
	Scalib3.push_back(S31);
	Scalib3.push_back(S32);
	Scalib3.push_back(S33);
	Scalib3.push_back(S34);

	CString ParName1, ParName2, ParName3;
	ParName1.Format(ParcelLoadString(STRID_SSTNAMES_PARAM), 1);
	ParName2.Format(ParcelLoadString(STRID_SSTNAMES_PARAM), 2);
	ParName3.Format(ParcelLoadString(STRID_SSTNAMES_PARAM), 3);

	int k, l, m;
	double ref1 = 0., ref2 = 0., ref3 = 0.;
	for(k=0; k<4; k++)
	{
		switch(k)
		{
		case 0:  ref1 = Plate1->T1;  break;
		case 1:  ref1 = Plate1->T2;  break;
		case 2:  ref1 = Plate1->T3;  break;
		case 3:  ref1 = Plate1->T4;  break;
		}
		for(l=0; l<4; l++)
		{
			switch(l)
			{
			case 0:  ref2 = Plate2->T1;  break;
			case 1:  ref2 = Plate2->T2;  break;
			case 2:  ref2 = Plate2->T3;  break;
			case 3:  ref2 = Plate2->T4;  break;
			}
			for(m=0; m<4; m++)
			{
				switch(m)
				{
				case 0:  ref3 = Plate3->T1;  break;
				case 1:  ref3 = Plate3->T2;  break;
				case 2:  ref3 = Plate3->T3;  break;
				case 3:  ref3 = Plate3->T4;  break;
				}

				CSampleData NewSample(hItem, Trans->GetHItem());

				CString SamName;
				SamName.Format(ParcelLoadString(STRID_SSTNAMES_SAMPLESST), k+1, l+1, m+1);
				NewSample.SetName(SamName);

				NewSample.AddReferenceData(ParName1);
				NewSample.SetReferenceValue(ParName1, ref1);
				NewSample.AddReferenceData(ParName2);
				NewSample.SetReferenceValue(ParName2, ref2);
				NewSample.AddReferenceData(ParName3);
				NewSample.SetReferenceValue(ParName3, ref3);

				Spectrum NewSpec;
				NewSpec.bUse = true;
				NewSpec.DateTime = CTime::GetCurrentTime();
				NewSpec.Num = 1;

				VDbl InS, OutS;
				for(n=0; n<N; n++)
				{
					double val = S0[n] + Scalib1[k][n] + Scalib2[l][n] + Scalib3[m][n];
					InS.push_back(val);
					OutS.push_back(0.);
				}
				ABS2TRA(&InS, &OutS);

				copy(OutS.begin(), OutS.end(), inserter(NewSpec.Data, NewSpec.Data.begin()));

				NewSample.SpecData.push_back(NewSpec);

				if(!Trans->AddSample(&NewSample))
					return false;

				Trans->GetSample(SamName)->FreeSpectraData();
			}
		}
	}

	Trans->GetSpecPoints();

	SortSamples(cSST);

	return true;
}

bool CProjectData::ChangeTransfer(HTREEITEM item)
{
	CTransferData* OldTrans = GetTransfer(item);
	if(OldTrans == NULL)
		return false;

	CTransferData NewTrans(hItem);
	NewTrans.Copy(*OldTrans);

	CEditTransferDlg dlg(&NewTrans);
	if(dlg.DoModal() != IDOK)
		return false;

	prdb::PRTrans Obj;
	NewTrans.GetDataForBase(Obj);
	if(!GetDatabase()->TransAddEx(DevNumber, Name, OldTrans->GetName(), &Obj))
		return false;

	OldTrans->Copy(NewTrans);

	return true;
}

bool CProjectData::DeleteTransfer(HTREEITEM item)
{
	if(GetTransferKeepStatus(item) != C_KEEP_FREE)
		return false;

	ItTrans it;
	for(it=Transfers.begin(); it!=Transfers.end(); ++it)
		if((*it).GetHItem() == item)
		{
			if(!GetDatabase()->TransDelete(DevNumber, Name, it->GetName()))
				return false;

			Transfers.erase(it);

			return true;
		}

	return false;
}

int CProjectData::GetTransferKeepStatus(HTREEITEM item)
{
	CTransferData *Trans = GetTransfer(item);
	if(Trans == NULL || Trans == TransOwn)
		return C_KEEP_NOEDIT;

	ItMod it;
	for(it=Models.begin(); it!=Models.end(); ++it)
		if(!it->GetTransName().Compare(Trans->GetName()))
			return C_KEEP_NOEDIT;

	return C_KEEP_FREE;
}

int CProjectData::GetNSamples()
{
	if(TransOwn == NULL)
		return 0;

	return TransOwn->GetNSamples();
}

int CProjectData::GetNAllSamples()
{
	int N = 0;

	ItTrans itT;
	for(itT=Transfers.begin(); itT!=Transfers.end(); ++itT)
		N += itT->GetNSamples();

	return N;
}

int CProjectData::GetNSpectra()
{
	if(TransOwn == NULL)
		return 0;

	return TransOwn->GetNSpectra();
}

int CProjectData::GetNAllSpectrum()
{
	int N = 0;

	ItTrans itT;
	for(itT=Transfers.begin(); itT!=Transfers.end(); ++itT)
		N += itT->GetNSpectra();

	return N;
}

int CProjectData::GetNSpectraUsed()
{
	if(TransOwn == NULL)
		return 0;

	return TransOwn->GetNSpectraUsed();
}

int CProjectData::GetNAllUsesSpectrum()
{
	int N = 0;

	ItTrans itT;
	for(itT=Transfers.begin(); itT!=Transfers.end(); ++itT)
		N += itT->GetNSpectraUsed();

	return N;
}

CSampleData* CProjectData::GetTransSample(CString transName, CString name)		// �������� ������� �� �����
{
	CTransferData *pTrans = GetTransfer(transName);
	if(pTrans == NULL) return NULL;
	return pTrans->GetSample(name);
}

CSampleData* CProjectData::GetSample(CString name)
{
	if(TransOwn == NULL) return NULL;

	return TransOwn->GetSample(name);
}

CSampleData* CProjectData::GetSample(int ind)
{
	if(TransOwn == NULL)
		return NULL;

	return TransOwn->GetSample(ind);
}

bool CProjectData::AddSample(CSampleData* NewSam)
{
	if(TransOwn == NULL)
		return NULL;

	return TransOwn->AddSample(NewSam);
}

bool CProjectData::ChangeSampleNoSpectra(CSampleData* OldSam, CSampleData* NewSam, CTransferData* pTrans)
{

	if(!GetDatabase()->BeginTrans()) return false;

	prdb::PRSample Obj;
	NewSam->GetDataForBase(Obj);
	if(!GetDatabase()->SampleAddEx(DevNumber, Name, pTrans->GetName(), OldSam->GetName(), &Obj))
	{
		GetDatabase()->Rollback();
		return false;
	}

	ItPar it;
	for(it=Params.begin(); it!=Params.end(); ++it)
	{
		prdb::PRRefData Obj2;
		NewSam->GetRefDataForBase(it->GetName(), Obj2);
		if(!GetDatabase()->RefDataAddEx(DevNumber, Name, pTrans->GetName(), NewSam->GetName(), &Obj2))
		{
			GetDatabase()->Rollback();
			return false;
		}
	}

	GetDatabase()->CommitTrans();

	OldSam->Copy(*NewSam);

	return true;
}

bool CProjectData::ChangeSample(CSampleData* OldSam, CSampleData* NewSam)
{
	// ������������� ������ ������� ������� ������!

	if(!GetDatabase()->BeginTrans())
		return false;

	prdb::PRSample Obj;
	NewSam->GetDataForBase(Obj);
	if(!GetDatabase()->SampleAddEx(DevNumber, Name, cOwn, OldSam->GetName(), &Obj))
	{
		GetDatabase()->Rollback();
		return false;
	}

	ItPar it;
	for(it=Params.begin(); it!=Params.end(); ++it)
	{
		prdb::PRRefData Obj2;
		NewSam->GetRefDataForBase(it->GetName(), Obj2);
		if(!GetDatabase()->RefDataAddEx(DevNumber, Name, cOwn, NewSam->GetName(), &Obj2))
		{
			GetDatabase()->Rollback();
			return false;
		}
	}

	int ind2;
	for(ind2=0; ind2<int(NewSam->SpecData.size()); ind2++)
	{
		Spectrum *Spec = NewSam->GetSpectrumByInd(ind2);
		prdb::PRSpectrum Obj3;
		Obj3.SpecNum = Spec->Num;
		NewSam->GetSpecDataForBase(Obj3);
		if(!GetDatabase()->SpectrumAddEx(DevNumber, Name, cOwn, NewSam->GetName(), &Obj3))
		{
			GetDatabase()->Rollback();
			return false;
		}
	}

	GetDatabase()->CommitTrans();

	OldSam->Copy(*NewSam);

	ItMod itm;
	for(itm=Models.begin(); itm!=Models.end(); ++itm)
	{
		ModelSample *MSam = itm->GetSampleForCalib(OldSam->GetName(), cOwn);
		if(MSam != NULL)
		{
			MSam->Rebuild(OldSam);
		}

		MSam = itm->GetSampleForValid(OldSam->GetName(), cOwn);
		if(MSam != NULL)
		{
			MSam->Rebuild(OldSam);
		}
	}

	return true;
}

bool CProjectData::EditSampleList(HTREEITEM item)
{
	CTransferData* Trans = GetTransfer(item);
	if(Trans == NULL) return false;

	CEditSampleListDlg dlg(this, &(Trans->Samples), item);
	dlg.DoModal();

	return true;
}

bool CProjectData::DeleteSample(int ind)
{
	if(TransOwn == NULL)
		return NULL;

	return TransOwn->DeleteSample(ind);
}

bool CProjectData::DeleteSample(CString name)
{
	if(TransOwn == NULL)
		return NULL;

	return TransOwn->DeleteSample(name);
}

int CProjectData::GetSampleKeepStatus(int ind)
{
	if(ind < 0 || ind >= GetNSamples())	return C_KEEP_NOTFOUND;

	return GetSampleKeepStatus(GetSample(ind)->GetName());
}

int CProjectData::GetSampleKeepStatus(CString name)
{
	if(GetSample(name) == NULL) return C_KEEP_NOTFOUND;

	ItMod it;
	for(it=Models.begin(); it!=Models.end(); ++it)
	{
		ModelSample *ModSam = it->GetSampleForCalib(name, cOwn);
		if(ModSam != NULL)
			return C_KEEP_NOEDIT;

		ModSam = it->GetSampleForValid(name, cOwn);
		if(ModSam != NULL)
			return C_KEEP_NOEDIT;
	}

	return C_KEEP_FREE;
}

int CProjectData::GetTransSampleKeepStatus(CString transName, CString name)		// �������� "���������" ������� �� �����
{
	CTransferData *pTrans = GetTransfer(transName);
	if(!pTrans) return C_KEEP_NOTFOUND;
	if(!pTrans->GetSample(name)) return C_KEEP_NOTFOUND;

	ItMod it;
	for(it=Models.begin(); it!=Models.end(); ++it)
	{
		ModelSample *ModSam = it->GetSampleForCalib(name, transName);
		if(ModSam != NULL) return C_KEEP_NOEDIT;

		ModSam = it->GetSampleForValid(name, transName);
		if(ModSam != NULL) return C_KEEP_NOEDIT;
	}

	return C_KEEP_FREE;
}

int CProjectData::GetSpectrumKeepStatus(CString samname, int ind)
{
	CSampleData *Sam = GetSample(samname);
	if(Sam == NULL) return C_KEEP_NOTFOUND;

	Spectrum *Spec = Sam->GetSpectrumByInd(ind);
	if(Spec == NULL) return C_KEEP_NOTFOUND;

	ItMod it;
	for(it=Models.begin(); it!=Models.end(); ++it)
	{
		ModelSample *ModSam = it->GetSampleForCalib(samname, cOwn);
		if(ModSam != NULL && ModSam->IsSpectrum(Spec->Num) && ModSam->Spectra[Spec->Num])
			return C_KEEP_NOEDIT;

		ModSam = it->GetSampleForValid(samname, cOwn);
		if(ModSam != NULL && ModSam->IsSpectrum(Spec->Num) && ModSam->Spectra[Spec->Num])
			return C_KEEP_NOEDIT;
	}

	return C_KEEP_FREE;
}

bool CProjectData::IsSampleRefKeep(CString namesam, CString nametrans, CString nameref)
{
	if(GetSample(namesam) == NULL) return false;

	int i;
	ItMod it;
	for(it=Models.begin(); it!=Models.end(); ++it)
	{
		if(!it->IsModelParamExist(nameref))	continue;

		for(i=0; i<it->GetNSamplesForCalib(); i++)
		{
			if(!it->GetSampleForCalib(i)->SampleName.Compare(namesam) && !it->GetSampleForCalib(i)->TransName.Compare(nametrans)) return true;
		}

		for(i=0; i<it->GetNSamplesForValid(); i++)
		{
			if(!it->GetSampleForValid(i)->SampleName.Compare(namesam) && it->GetSampleForValid(i)->TransName.Compare(nametrans)) return true;
		}
	}

	return false;
}

int CProjectData::GetNModels()
{
	return int(Models.size());
}

int CProjectData::GetNQntModels()
{
	int N = 0;

	ItMod it;
	for(it=Models.begin(); it!=Models.end(); ++it)
		if(it->GetAnalysisType() == C_ANALYSIS_QNT)
			N++;

	return N;
}

int CProjectData::GetNQltModels()
{
	int N = 0;

	ItMod it;
	for(it=Models.begin(); it!=Models.end(); ++it)
		if(it->GetAnalysisType() == C_ANALYSIS_QLT)
			N++;

	return N;
}

CModelData* CProjectData::GetModel(HTREEITEM item)
{
	ItMod it;
	for(it=Models.begin(); it!=Models.end(); ++it)
		if((*it).GetHItem() == item)
			return &(*it);

	return NULL;
}

CModelData* CProjectData::GetModel(CString name)
{
	ItMod it;
	for(it=Models.begin(); it!=Models.end(); ++it)
	{
		if(!(*it).GetName().CompareNoCase(name))
			return &(*it);
	}

	return NULL;
}

CModelData* CProjectData::GetModel(int ind)
{
	if(ind < 0 || ind >= int(Models.size()))
		return NULL;

	int i;
	ItMod it;
	for(i=0, it=Models.begin(); i<ind; i++)
		++it;

	return &(*it);
}

CModelData* CProjectData::GetQltModel(int ind)
{
	if(ind < 0 || ind >= GetNQltModels())
		return NULL;

	int i;
	ItMod it;
	for(i=-1, it=Models.begin(); it!= Models.end(); ++it)
	{
		if(it->GetAnalysisType() == C_ANALYSIS_QLT)
			i++;

		if(i == ind)
			return &(*it);
	}

	return NULL;
}

CModelData* CProjectData::GetLastModel()
{
	return GetModel(GetNModels() - 1);
}

HTREEITEM CProjectData::GetLastModelHItem()
{
	CModelData* ModData = GetLastModel();
	if(ModData)
		return ModData->GetHItem();

	return NULL;
}

CModelData* CProjectData::AddNewModel()
{
	CModelData NewMod(hItem);

	CCreateModelDlg ModDlg(&NewMod);
	if(ModDlg.DoModal() != IDOK)
	{
		return false;
	}
	NewMod.SetDate();

	CSaveModelWaitDlg dlg2(this, &NewMod, NULL);
	if(dlg2.DoModal() != IDOK)
		return false;

	return GetLastModel();
}

CModelData* CProjectData::AddExistingModel(CModelData &NewMod, CModelData* OldMod)
{
	NewMod.SetDate();

	prdb::PRModel Obj;
	NewMod.GetDataForBase(Obj);
	CString OldName = (OldMod != NULL) ? OldMod->GetName() : cEmpty;
	if(!GetDatabase()->ModelAddEx(DevNumber, Name, OldName, &Obj))
		return NULL;

	if(OldMod == NULL)
		Models.push_back(NewMod);
	else
		OldMod->Copy(NewMod);

	return GetLastModel();
}

CModelData* CProjectData::AddCopyModel(HTREEITEM item)
{
	CModelData* OldMod = GetModel(item);
	if(OldMod == NULL)
		return NULL;

	CModelData NewMod(hItem);
	NewMod.Copy(*OldMod);

	CCreateModelDlg ModDlg(&NewMod, C_MODDLG_MODE_COPY);
	if(ModDlg.DoModal() != IDOK)
	{
		return false;
	}
	NewMod.SetDate();

	CSaveModelWaitDlg dlg2(this, &NewMod, NULL);
	if(dlg2.DoModal() != IDOK)
		return false;

	return GetLastModel();
}

bool CProjectData::ChangeModelSettings(HTREEITEM item)
{
	CModelData* OldMod = GetModel(item);
	if(OldMod == NULL)
		return false;
	if(GetModelKeepStatus(item) != C_KEEP_FREE)
		return false;

	CModelData NewMod(hItem);
	NewMod.Copy(*OldMod);

	CCreateModelDlg ModDlg(&NewMod, C_MODDLG_MODE_CHANGE);
	if(ModDlg.DoModal() != IDOK)
	{
		return false;
	}
	NewMod.SetDate();

	CSaveModelWaitDlg dlg2(this, &NewMod, OldMod);
	if(dlg2.DoModal() != IDOK)
		return false;

	prdb::PRModel Obj;
	NewMod.GetDataForBase(Obj);
	if(!GetDatabase()->ModelAddEx(DevNumber, Name, OldMod->GetName(), &Obj))
		return false;

	OldMod->Copy(NewMod);

	return true;
}

bool CProjectData::DeleteModel(HTREEITEM item)
{
	if(GetModelKeepStatus(item) != C_KEEP_FREE)
		return false;

	ItMod it;
	for(it=Models.begin(); it!=Models.end(); ++it)
		if((*it).GetHItem() == item)
		{
			if(!GetDatabase()->ModelDelete(DevNumber, Name, it->GetName()))
				return false;

			Models.erase(it);

			return true;
		}

	return false;
}

bool CProjectData::ModelResult(HTREEITEM item)
{
	CModelData* Mod = GetModel(item);
	if(Mod == NULL)
		return false;

	if(Mod->CalculateModel())
	{
		int res;
		if(Mod->GetAnalysisType() == C_ANALYSIS_QNT)
		{
			CModelResultDlg ModResDlg(Mod, C_MODRESDLG_MODE_SHOW);
			res = (ModResDlg.DoModal() == IDOK);
		}
		else
		{
			CModelQltResultDlg ModQltResDlg(Mod, C_MODQLTRESDLG_MODE_SHOW);
			res = (ModQltResDlg.DoModal() == IDOK);
		}

		Mod->FreeSpectraData();

		if(!res)
			return false;
	}
	else
		Mod->FreeSpectraData();

	return true;
}

int CProjectData::GetModelKeepStatus(HTREEITEM item)
{
	if(GetModel(item) == NULL)
		return C_KEEP_NOEDIT;

	int i;
	CString name = GetModel(item)->GetName();

	ItMtd it;
	for(it=Methods.begin(); it!=Methods.end(); ++it)
	{
		if(it->GetAnalysisType() == C_MTD_ANALYSIS_QLT)
		{
			for(i=0; i<it->GetNModelQltNames(); i++)
				if(!it->GetModelQltName(i).Compare(name))
					return C_KEEP_NOEDIT;
		}
		else
		{
			for(i=0; i<it->GetNRules(); i++)
			{
				AnalyseRule *rule = it->GetRule(i);
				if(!rule->MainModel.Compare(name))
					return C_KEEP_NOEDIT;
				ItStr it2;
				for(it2=rule->AddModels.begin(); it2!=rule->AddModels.end(); ++it2)
					if(!name.Compare(*it2))
						return C_KEEP_NOEDIT;
			}
		}
	}

	return C_KEEP_FREE;
}

int CProjectData::GetNMethods()
{
	return int(Methods.size());
}

CMethodData* CProjectData::GetMethod(HTREEITEM item)
{
	ItMtd it;
	for(it=Methods.begin(); it!=Methods.end(); ++it)
		if((*it).GetHItem() == item)
			return &(*it);

	return NULL;
}

CMethodData* CProjectData::GetMethod(CString name)
{
	ItMtd it;
	for(it=Methods.begin(); it!=Methods.end(); ++it)
		if(!(*it).GetName().CompareNoCase(name))
			return &(*it);

	return NULL;
}

CMethodData* CProjectData::GetMethod(int ind)
{
	if(ind < 0 || ind >= int(Methods.size()))
		return NULL;

	int i;
	ItMtd it;
	for(i=0, it=Methods.begin(); i<ind; i++)
		++it;

	return &(*it);
}

CMethodData* CProjectData::GetLastMethod()
{
	return GetMethod(GetNMethods() - 1);
}

HTREEITEM CProjectData::GetLastMethodHItem()
{
	CMethodData* MtdData = GetLastMethod();
	if(MtdData)
		return MtdData->GetHItem();

	return NULL;
}

CMethodData* CProjectData::AddNewMethod()
{
	CMethodData NewMtd(hItem);

	CCreateMethodDlg dlg(&NewMtd);
	if(dlg.DoModal() != IDOK)
		return NULL;

	prdb::PRMethod Obj;
	NewMtd.GetDataForBase(Obj);
	if(!GetDatabase()->MethodAddEx(DevNumber, Name, cEmpty, &Obj))
		return NULL;

	Methods.push_back(NewMtd);

	return GetLastMethod();
}

CMethodData* CProjectData::AddNewMethod(CMethodData *pNewMtd)
{
	prdb::PRMethod Obj;
	pNewMtd->GetDataForBase(Obj);
	if(!GetDatabase()->MethodAddEx(DevNumber, Name, cEmpty, &Obj))
		return NULL;

	Methods.push_back(*pNewMtd);

	return GetLastMethod();
}

CMethodData* CProjectData::AddCopyMethod(HTREEITEM item)
{
	CMethodData* OldMtd = GetMethod(item);
	if(OldMtd == NULL)
		return NULL;

	CMethodData NewMtd(hItem);
	NewMtd.Copy(*OldMtd);

	CCreateMethodDlg dlg(&NewMtd, MTDDLG_MODE_COPY);
	if(dlg.DoModal() != IDOK)
		return NULL;

	prdb::PRMethod Obj;
	NewMtd.GetDataForBase(Obj);
	if(!GetDatabase()->MethodAddEx(DevNumber, Name, cEmpty, &Obj))
		return NULL;

	Methods.push_back(NewMtd);

	return GetLastMethod();
}

CMethodData* CProjectData::AddExistingMethod(CMethodData& Mtd)
{
	Mtd.SetDate();

	prdb::PRMethod Obj;
	Mtd.GetDataForBase(Obj);
	if(!GetDatabase()->MethodAddEx(DevNumber, Name, cEmpty, &Obj))
		return NULL;

	Methods.push_back(Mtd);

	return GetLastMethod();
}

bool CProjectData::ChangeMethodSettings(HTREEITEM item)
{
	CMethodData* OldMtd = GetMethod(item);
	if(OldMtd == NULL)
		return false;

	CMethodData NewMtd(hItem);
	NewMtd.Copy(*OldMtd);

	CCreateMethodDlg dlg(&NewMtd, MTDDLG_MODE_CHANGE);
	if(dlg.DoModal() != IDOK)
		return false;

	prdb::PRMethod Obj;
	NewMtd.GetDataForBase(Obj);
	if(!GetDatabase()->MethodAddEx(DevNumber, Name, OldMtd->GetName(), &Obj))
		return false;

	OldMtd->Copy(NewMtd);

	return true;
}

bool CProjectData::DeleteMethod(HTREEITEM item)
{
	ItMtd it;
	for(it=Methods.begin(); it!=Methods.end(); ++it)
		if((*it).GetHItem() == item)
		{
			if(!GetDatabase()->MethodDelete(DevNumber, Name, it->GetName()))
				return false;

			Methods.erase(it);

			return true;
		}

	return false;
}

bool CProjectData::ExportMethod(HTREEITEM item, CString fname)
{
	CMethodData* Mtd = GetMethod(item);
	if(Mtd == NULL)
		return false;

	if(Mtd->GetAnalysisType() == C_ANALYSIS_QNT)
	{
		mtddb::PRMethodQnt Obj;
		if(!Mtd->GetDataForExportQnt(Obj))
			return false;

		CSaveMethodWaitDlg dlg2(this, &Obj, NULL, fname);
		if(dlg2.DoModal() != IDOK)
			return false;

		return SaveMethodToQntExportBase(fname, &Obj);
	}
	else
	{
		mtddb::PRMethodQlt Obj;
		if(!Mtd->GetDataForExportQlt(Obj))
			return false;

		CSaveMethodWaitDlg dlg2(this, NULL, &Obj, fname);
		if(dlg2.DoModal() != IDOK)
			return false;
	}

	return true;
}

bool CProjectData::SaveMethodToQntExportBase(CString fname, mtddb::PRMethodQnt* Obj)
{
	if(PathFileExists(fname))
		DeleteFile(fname);

	CPRDBMethodQnt Database;

	if(!Database.CreateDatabase(fname))
	{
		ParcelError(ParcelLoadString(STRID_STDTITLE_MTDDBNOOPEN));
		return false;
	}
	if(!Database.MethodAddEx(cEmpty, Obj))
	{
		ParcelError(ParcelLoadString(STRID_STDTITLE_MTDNOSAVE));
		return false;
	}

	Database.Close();

	return true;
}

bool CProjectData::SaveMethodToQltExportBase(CString fname, mtddb::PRMethodQlt* Obj)
{
	if(PathFileExists(fname))
		DeleteFile(fname);

	CPRDBMethodQlt Database;

	if(!Database.CreateDatabase(fname))
	{
		ParcelError(ParcelLoadString(STRID_STDTITLE_MTDDBNOOPEN));
		return false;
	}
	if(!Database.MethodAddEx(cEmpty, Obj))
	{
		ParcelError(ParcelLoadString(STRID_STDTITLE_MTDNOSAVE));
		return false;
	}

	Database.Close();

	return true;
}

bool CProjectData::ExportSettings(CString fname)
{
	apjdb::PRApjProject Obj;
	if(!GetDataForExportSettings(Obj))
		return false;

	CPRDBSpectra Database;

	if(!Database.CreateDatabase(fname))
	{
		ParcelError(ParcelLoadString(STRID_STDTITLE_APJDBNOOPEN));
		return false;
	}
	if(!Database.ApjProjectAddEx(cEmpty, &Obj))
	{
		ParcelError(ParcelLoadString(STRID_STDTITLE_APJNOSAVE));
		return false;
	}

	Database.Close();

	return true;
}

int CProjectData::GetNFillings()
{
	return int(FillingList.size());
}

int CProjectData::GetFillingFromList(int ind)
{
	if(ind < 0 || ind >= int(FillingList.size()))
		return 0;

	return FillingList[ind];
}

void CProjectData::CalcSpecPoints()
{
	CDeviceData *Dev = GetDeviceByHItem(hParentItem);
	bool IsImit = (Dev->GetType() == C_DEV_TYPE_IMIT);
	double Step, MinFreq, MaxFreq;

	if(IsImit)
	{
		Step = 1.;
		MinFreq = LowLimSpecRange;
		MaxFreq = UpperLimSpecRange;
	}
	else
	{
		Step = Dev->GetResolutionFromList(SubCanalResolution);
		Step /= (16384 * C_LASER_WAVE);
		MinFreq = int(LowLimSpecRange / Step) * Step;
		if(LowLimSpecRange - MinFreq > Step / 2.)
			MinFreq += Step;
		MaxFreq = int(UpperLimSpecRange / Step) * Step;
		if(UpperLimSpecRange - MaxFreq > Step / 2.)
			MaxFreq += Step;
	}
	int NAllPoints = int((MaxFreq - MinFreq) / Step + 1.1);

	SpecPoints.clear();
	for(int i=0; i<NAllPoints; i++)
		SpecPoints.push_back(MinFreq + i * Step);
}

void CProjectData::ExtractSpecPoints(int low, int upper, VDbl& points)
{
	points.clear();
	if(int(SpecPoints.size()) <= 0)
		return;

	if(low < LowLimSpecRange)  low = LowLimSpecRange;
	if(upper > UpperLimSpecRange)  upper = UpperLimSpecRange;

	CDeviceData *Dev = GetDeviceByHItem(hParentItem);
	bool IsImit = (Dev->GetType() == C_DEV_TYPE_IMIT);
	double Step, LowFreq, UpperFreq;

	if(IsImit)
	{
		Step = 1.;
		LowFreq = low;
		UpperFreq = upper;
	}
	else
	{
		Step = Dev->GetResolutionFromList(SubCanalResolution);
		Step /= (16384 * C_LASER_WAVE);
		LowFreq = int(low / Step) * Step;
		if(low - LowFreq > Step / 2.)
			LowFreq += Step;
		UpperFreq = int(upper / Step) * Step;
		if(upper - UpperFreq > Step / 2.)
			UpperFreq += Step;
	}

	int ilow = GetSpecPointInd(LowFreq);
	int iupper = GetSpecPointInd(UpperFreq);

	copy(SpecPoints.begin() + ilow, SpecPoints.begin() + (iupper + 1), inserter(points, points.begin()));
}

void CProjectData::FillZeroFillingList()
{
	FillingList.clear();

	for(int i=SubCanalResolution, mean=1; i<9; i++, mean*=2)
		FillingList.push_back(mean);

	if(ZeroFilling >= GetNFillings())
		ZeroFilling = GetNFillings() - 1;
}

bool CProjectData::IsReadyForTrans(CProjectData *SPrj)
{
	if(LowLimSpecRange > SPrj->GetUpperLimSpecRange() ||
	   UpperLimSpecRange < SPrj->GetLowLimSpecRange() ||
	   fabs(GetSubCanalResolutionMean() - SPrj->GetSubCanalResolutionMean()) > 1.e-3 ||
	   SubCanalApodization != SPrj->GetSubCanalApodization() ||
	   ZeroFilling != SPrj->GetZeroFillingInd())
		return false;
/*
	ItMod it;
	for(it=Models.begin(); it!=Models.end(); ++it)
		if(it->IsReadyForTrans())
			return true;
*/
	return true;
}

bool CProjectData::IsExistMaster()
{
	ItDev itD;
	for(itD=pRoot->Devices.begin(); itD!=pRoot->Devices.end(); ++itD)
	{
		if(itD->GetHItem() == hParentItem)
			continue;

		if(itD->IsReadyForTrans(this))
			return true;
	}

	return false;
}

void CProjectData::GetDataForBase(prdb::PRAnalisisProject& Obj)
{
	Obj.APName = Name;
	Obj.SST = SST;
	Obj.SpRangeDown = LowLimSpecRange;
	Obj.SpRangeUp = UpperLimSpecRange;
	Obj.SampleScanCount = NScansSubsample;
	Obj.SampleMeasCount = NMeasuresSubsample;
	Obj.StandardScanCount = NScansStandart;
	Obj.StandardMeasCount = NMeasuresStandart;
	Obj.CuvetteLength = BathLength;
	Obj.SubChannelPermit = SubCanalResolution;
	Obj.SubChannelApodization = SubCanalApodization;
	Obj.ZeroFilling = ZeroFilling;
	Obj.UseRefChannel = UseRefCanal;
	Obj.RefChannelPermit = RefCanalResolution;
	Obj.RefChannelApodization = RefCanalApodization;
	Obj.TempRangeDown = LowLimTemp;
	Obj.TempRangeUp = UpperLimTemp;
	Obj.TempDiff = MaxDiffTemp;
	Obj.SamSortName = SamSortName;
	Obj.SamSortDir = SamSortDir;
}

void CProjectData::SetDataFromBase(prdb::PRAnalisisProject& Obj)
{
	Name = Obj.APName;
	SST = Obj.SST;
	LowLimSpecRange = Obj.SpRangeDown;
	UpperLimSpecRange = Obj.SpRangeUp;
	NScansSubsample = Obj.SampleScanCount;
	NMeasuresSubsample = Obj.SampleMeasCount;
	NScansStandart = Obj.StandardScanCount;
	NMeasuresStandart = Obj.StandardMeasCount;
	BathLength = Obj.CuvetteLength;
	SubCanalResolution = Obj.SubChannelPermit;
	SubCanalApodization = Obj.SubChannelApodization;
	ZeroFilling = Obj.ZeroFilling;
	UseRefCanal = Obj.UseRefChannel;
	RefCanalResolution = Obj.RefChannelPermit;
	RefCanalApodization = Obj.RefChannelApodization;
	LowLimTemp = Obj.TempRangeDown;
	UpperLimTemp = Obj.TempRangeUp;
	MaxDiffTemp = Obj.TempDiff;
	SamSortName = Obj.SamSortName;
	SamSortDir = Obj.SamSortDir;

	CalcSpecPoints();
}

bool CProjectData::LoadDatabase(CTreeCtrl* pTree)
{
	LPar NewParams;

	if(!GetDatabase()->IndicesRead(DevNumber, Name))
		return false;

	while(!GetDatabase()->IsEOF())
	{
		prdb::PRIndex Obj;
		if(!GetDatabase()->IndexGet(&Obj))
			break;

		CParamData Par(hItem);
		Par.SetDataFromBase(Obj);
		NewParams.push_back(Par);
		GetDatabase()->MoveNext();
	}
	GetDatabase()->Free();

	Params.clear();
	copy(NewParams.begin(), NewParams.end(), inserter(Params, Params.begin()));


	ItTrans it;
	LTrans NewTransfers;

	if(!GetDatabase()->TransRead(DevNumber, Name))
		return false;

	HTREEITEM hLastItem = NULL;
	while(!GetDatabase()->IsEOF())
	{
		prdb::PRTrans Obj;
		if(!GetDatabase()->TransGet(&Obj))
			break;

		CTransferData Trans(hItem);
		Trans.SetDataFromBase(Obj);

		CString s = (Trans.GetName().Compare(cOwn)) ? Trans.GetName() : ParcelLoadString(STRID_TREE_SAMOWN);

		HTREEITEM hNewItem = pTree->InsertItem(TVIF_TEXT, s, 0, 0, 0, 0, 0,
											   hSampleSetItem, hLastItem);
		Trans.SetHItem(hNewItem);
		pTree->SetItemImage(hNewItem, 11, 11);
		hLastItem = hNewItem;

		NewTransfers.push_back(Trans);

		GetDatabase()->MoveNext();
	}
	GetDatabase()->Free();

	Transfers.clear();
	copy(NewTransfers.begin(), NewTransfers.end(), inserter(Transfers, Transfers.begin()));

	TransOwn = GetTransfer(cOwn);

	for(it=Transfers.begin(); it!=Transfers.end(); ++it)
		it->LoadDatabase();

	SortSamples(SamSortName, SamSortDir);

	LMod NewModels;

	if(!GetDatabase()->ModelRead(DevNumber, Name))
		return false;

	hLastItem = NULL;
	while(!GetDatabase()->IsEOF())
	{
		prdb::PRModel Obj;
		if(!GetDatabase()->ModelGet(&Obj))
			break;

		CModelData Mod(hItem);
		Mod.SetDataFromBase(Obj);

		HTREEITEM hNewItem = pTree->InsertItem(TVIF_TEXT, Mod.GetName(), 0, 0, 0, 0, 0,
											   hModelSetItem, hLastItem);
		Mod.SetHItem(hNewItem);
		pTree->SetItemImage(hNewItem, 11, 11);
		hLastItem = hNewItem;

		NewModels.push_back(Mod);

		GetDatabase()->MoveNext();
	}
	GetDatabase()->Free();

	Models.clear();
	copy(NewModels.begin(), NewModels.end(), inserter(Models, Models.begin()));

	pTree->Expand(hModelSetItem, TVE_COLLAPSE);
	if(Models.size() > 0)
		pTree->SetItemImage(hModelSetItem, 6, 6);
	else
		pTree->SetItemImage(hModelSetItem, 10, 10);


	LMtd NewMethods;

	if(!GetDatabase()->MethodRead(DevNumber, Name))
		return false;

	hLastItem = NULL;
	while(!GetDatabase()->IsEOF())
	{
		prdb::PRMethod Obj;
		if(!GetDatabase()->MethodGet(&Obj))
			break;

		CMethodData Mtd(hItem);
		Mtd.SetDataFromBase(Obj);

		HTREEITEM hNewItem = pTree->InsertItem(TVIF_TEXT, Mtd.GetName(), 0, 0, 0, 0, 0,
											   hMethodSetItem, hLastItem);
		Mtd.SetHItem(hNewItem);
		pTree->SetItemImage(hNewItem, 11, 11);
		hLastItem = hNewItem;

		NewMethods.push_back(Mtd);

		GetDatabase()->MoveNext();
	}
	GetDatabase()->Free();

	Methods.clear();
	copy(NewMethods.begin(), NewMethods.end(), inserter(Methods, Methods.begin()));

	pTree->Expand(hMethodSetItem, TVE_COLLAPSE);
	if(Methods.size() > 0)
		pTree->SetItemImage(hMethodSetItem, 6, 6);
	else
		pTree->SetItemImage(hMethodSetItem, 10, 10);

	return true;
}

bool CProjectData::GetDataForExportSettings(apjdb::PRApjProject& Obj)
{
	CDeviceData *ParentDev = GetDeviceByHItem(GetParentHItem());

	Obj.ProjectName = Name;
	Obj.DateCreated = CTime::GetCurrentTime();
	Obj.DevNum = ParentDev->GetNumber();
	Obj.DevType = cGetDeviceID0(ParentDev->GetType());
	Obj.SpecRangeLow = GetLowLimSpecRange();
	Obj.SpecRangeUpper = GetUpperLimSpecRange();
	Obj.ResSub = GetSubCanalResolution();
	Obj.ApodSub = GetSubCanalApodization();
	Obj.ZeroFill = GetZeroFillingInd();
	Obj.UseRef = IsUseRefCanal();
	Obj.ResRef = GetRefCanalResolution();
	Obj.ApodRef = GetRefCanalApodization();
	Obj.SmplScan = GetNScansSubsample();
	Obj.SmplMeas = GetNMeasuresSubsample();
	Obj.StdScan = GetNScansStandart();
	Obj.StdMeas = GetNMeasuresStandart();
	Obj.Xtransform = true;
	Obj.IsSST = IsSST();
	Obj.UseBath = !((ParentDev->GetType() == C_DEV_TYPE_40 || ParentDev->GetType() == C_DEV_TYPE_12M) && IsSST());
	Obj.BathLength = GetBathLength();
	Obj.TempLow = GetLowLimTemp();
	Obj.TempUpper = GetUpperLimTemp();
	Obj.TempDiff = MaxDiffTemp;

	Obj.Params.clear();
	Obj.Samples.clear();

	ItPar itP;
	for(itP=Params.begin(); itP!=Params.end(); ++itP)
	{
		apjdb::PRApjParam Obj2;

		Obj2.ParamName = itP->GetName();
		Obj2.Unit = itP->GetUnit();
		Obj2.Format = itP->GetFormat();
		Obj2.StdMois = itP->GetStdMoisture();
		Obj2.UseStdMois = itP->IsUseStdMoisture();

		Obj.Params.push_back(Obj2);
	}

	ItSam itS;
	for(itS=TransOwn->Samples.begin(); itS!=TransOwn->Samples.end(); ++itS)
	{
		apjdb::PRApjSample Obj3;

		Obj3.SampleName = itS->GetName();
		Obj3.Iden2 = itS->GetIden2();
		Obj3.Iden3 = itS->GetIden3();

		ItRef itR;
		for(itR=itS->RefData.begin(); itR!=itS->RefData.end(); ++itR)
		{
			apjdb::PRApjRefData Obj4;

			Obj4.ParamName = itR->Name;
			Obj4.RefData = itR->Value;
			Obj4.IsDataExists = !itR->bNoValue;

			Obj3.RefData.push_back(Obj4);
		}

		Obj.Samples.push_back(Obj3);
	}

	return true;
}
