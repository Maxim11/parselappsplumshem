#pragma once

#ifndef _EDITLIST_H_
#define _EDITLIST_H_

#include "ListCtrlStyled.h"

typedef int (* fGetCellType) (int, int);
typedef int (* fCellCallbackType) (int, int, int);

/////////////////////////////////////////////////////////////////////////////
// CEditList window
class CEditList : public CListCtrlStyled
{
	// Construction
public:
	CEditList();

// Attributes
public:
	enum eType{
		eText,
		eEdit,
		eCombo,
		eLast	
	};

	fGetCellType m_fGetCellType;

	fCellCallbackType m_fCellCallback;

	CStringList m_strList;
// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEditList)
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
public:
	void SetCellType(fGetCellType func) { m_fGetCellType = func;}
	void SetCellCallback(fCellCallbackType func) { m_fCellCallback = func;}
	virtual ~CEditList();

	// Generated message map functions
protected:

	CComboBox * ComboItem( int nItem,  int nSubItem);
	CEdit * EditItem( int nItem, int nSubItem);
	//{{AFX_MSG(CEditList)
	afx_msg void OnEndlabeledit(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnClick(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnKeydown(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);

//	afx_msg void OnCustomdraw(NMHDR* pNMHDR, LRESULT* pResult);
//	afx_msg void OnDestroy();

	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EDITLIST_H__834127E6_9297_11D5_8AFA_DAC6E5A1C54B__INCLUDED_)
