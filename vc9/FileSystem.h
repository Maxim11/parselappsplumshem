////////////////////////////////////////////////////////////////////////////
//	Author      : Ignatyev Gena 2006
//	Email       : gena.ignatev@paloma.spbu.ru
//	Module      : FileSystem.h
//	Description : 
////////////////////////////////////////////////////////////////////////////
#ifndef _FILE_DIR_HELPER_H__
#define _FILE_DIR_HELPER_H__

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include <string>
#include <sstream>



namespace filesystem
{

#ifdef _UNICODE
	typedef wchar_t					tchar;
	typedef std::wstring			tstring;
	typedef std::wistringstream		tistringstream;
	typedef std::wostringstream		tostringstream;
	//typedef std::wofstream			tofstream;
	//typedef std::wifstream			tifstream;
#else
	typedef char					tchar;
	typedef std::string				tstring;
	typedef std::istringstream		tistringstream;
	typedef std::ostringstream		tostringstream;
	//typedef std::ofstream			tofstream;
	//typedef std::ifstream			tifstream;
#endif


//-------------------------------------------------------------------------
template < typename StringT >
tstring toCorrectStdString( StringT str );

template <> inline tstring toCorrectStdString<tstring>( tstring str ){ return str; }
template <> inline tstring toCorrectStdString<const tstring&>( const tstring& str ){ return str; }
template <> inline tstring toCorrectStdString<CString>( CString str ){ return (LPCTSTR)str; }
template <> inline tstring toCorrectStdString<const CString&>( const CString& str ){ return (LPCTSTR)str; }
template <> inline tstring toCorrectStdString<LPTSTR>( LPTSTR str ){ return (str != NULL ? str : _T("")); }
template <> inline tstring toCorrectStdString<LPCTSTR>( LPCTSTR str ){ return (str != NULL ? str : _T("")); }
//-------------------------------------------------------------------------
template < typename StringT >
CString toCorrectCString( StringT str );

template <> inline CString toCorrectCString<tstring>( tstring str )	{ return str.c_str(); }
template <> inline CString toCorrectCString<const tstring&>( const tstring& str ){ return str.c_str(); }
template <> inline CString toCorrectCString<CString>( CString str ){ return str; }
template <> inline CString toCorrectCString<const CString&>( const CString& str ){ return str; }
template <> inline CString toCorrectCString<LPTSTR>( LPTSTR str ){ return (str != NULL ? str : _T("")); }
template <> inline CString toCorrectCString<LPCTSTR>( LPCTSTR str ){ return (str != NULL ? str : _T("")); }
//-------------------------------------------------------------------------



/**********************************************************************
*                        -= SetCurrDir =-
*	Description : ����� ���������� � ������������ � �������������� � �����������
**********************************************************************/
class SetCurrDir
{
private:
	CString m_strPrevDir;

public:
	// .ctor
	// ���� �������� NULL - �� ������ ������ �� ����������, �� � ����������� ����� ���������������
	SetCurrDir( LPCTSTR lpszNewDir = NULL )
	{
		::GetCurrentDirectory( MAX_PATH, m_strPrevDir.GetBuffer(MAX_PATH+1) );
		m_strPrevDir.ReleaseBuffer();
		if( lpszNewDir ) ::SetCurrentDirectory( lpszNewDir );
	}

	// .dtor
	~SetCurrDir()
	{
		::SetCurrentDirectory( m_strPrevDir );
	}

};
//-----------------------------------------------------------------------
inline
CString GetCurrDir()
{
	CString strDir;
	::GetCurrentDirectory( _MAX_PATH, strDir.GetBuffer(_MAX_PATH+1) );
	strDir.ReleaseBuffer();
	return strDir;
}
//-------------------------------------------------------------------------
inline
CString GetWindowsDir()
{
	CString strDir;
	::GetWindowsDirectory( strDir.GetBuffer(_MAX_PATH+1), _MAX_PATH );
	strDir.ReleaseBuffer();
	return strDir;
}
//-------------------------------------------------------------------------




/**********************************************************************
*                      -= ������� ��� ������ =-
**********************************************************************/
inline
bool IsExistFile( LPCTSTR lpszFile )
{
	CFileStatus status;
	return ( TRUE == CFile::GetStatus( lpszFile, status ) ) 
		&& ( 0 == (status.m_attribute & CFile::directory) )
		&& ( 0 == (status.m_attribute & CFile::volume) );
}
//-------------------------------------------------------------------------
inline
bool IsNotExistFile( LPCTSTR lpszFile )
{
	CFileStatus status;
	return ( TRUE != CFile::GetStatus( lpszFile, status ) ) 
		|| ( status.m_attribute & CFile::directory )
		|| ( status.m_attribute & CFile::volume );
}
//-------------------------------------------------------------------------
inline
bool IsFile( LPCTSTR lpszFile )
{
	CFileStatus status;
	return ( TRUE == CFile::GetStatus( lpszFile, status ) ) 
		&& ( 0 == (status.m_attribute & CFile::directory) )
		&& ( 0 == (status.m_attribute & CFile::volume) );
}
//-------------------------------------------------------------------------
inline
LONG GetFileSize( LPCTSTR lpszFile )
{
	CFileStatus status;
	if(( TRUE == CFile::GetStatus( lpszFile, status ) ) 
	&& ( 0 == (status.m_attribute & CFile::directory) )
	&& ( 0 == (status.m_attribute & CFile::volume) ) )
	{
		return LONG(status.m_size);
	}
	return 0L;
}
//-------------------------------------------------------------------------


/**********************************************************************
*                      -= ������� ��� ���������� =-
**********************************************************************/
inline
CString RemoveLastSeparator( CString strPath )
{
	const int len = strPath.GetLength();
	if( len && strPath[len-1] == _T('\\') ) return strPath.Left(len-1);
	return strPath;
}
//-------------------------------------------------------------------------
inline
CString RemovePreSeparator( CString strPath )
{
	if( !strPath.IsEmpty() && strPath[0] == _T('\\') ) return strPath.Mid(1);
	return strPath;
}
//-------------------------------------------------------------------------
inline
bool IsExistDirectory( LPCTSTR lpszDir )
{
	CFileFind finder;
	CString strDir = RemoveLastSeparator( lpszDir );
	if( !finder.FindFile( strDir ) ) return false;

	finder.FindNextFile();
	bool isExist = (TRUE == finder.IsDirectory());
	finder.Close();
	return isExist;
}
//-------------------------------------------------------------------------
inline
bool IsNotExistDirectory( LPCTSTR lpszDir )
{
	return !IsExistDirectory( lpszDir );	
}
//-------------------------------------------------------------------------
inline
bool IsExistFolder( LPCTSTR lpszDir ) // same as IsExistDirectory
{
	return IsExistDirectory( lpszDir );	
}
//-------------------------------------------------------------------------
inline
bool IsNotExistFolder( LPCTSTR lpszDir ) // same as IsNotExistDirectory
{
	return !IsExistDirectory( lpszDir );
}
//-------------------------------------------------------------------------
inline
bool EnsureDirectory( LPCTSTR lpszDir )
{
	return ( TRUE == ::CreateDirectory( lpszDir, NULL ) ) 
		|| ( ERROR_ALREADY_EXISTS == ::GetLastError() );
}
//-------------------------------------------------------------------------
inline
bool EnsureFolder( LPCTSTR lpszFolder ) // same as EnsureDirectory
{
	return EnsureDirectory( lpszFolder );
}
//-------------------------------------------------------------------------





/**********************************************************************
*                      -= ������� ��� ����� =-
**********************************************************************/
inline
bool IsExistPath( LPCTSTR lpszPath )
{
	CFileFind finder;
	CString	strPath = RemoveLastSeparator( lpszPath );
	bool isExist = (TRUE == finder.FindFile( strPath ) );
	finder.Close();
	return isExist;
}
//-------------------------------------------------------------------------
inline
bool IsNotExistPath( LPCTSTR lpszPath )
{
	return !IsExistPath( lpszPath );
}
//-------------------------------------------------------------------------
template < typename StringT1, typename StringT2 >
CString MakePath( StringT1 str1, StringT2 str2 )
{
	CString s1 = toCorrectCString( str1 );
	CString s2 = toCorrectCString( str2 );

	bool isPrevHas	= !s1.IsEmpty() && s1[ s1.GetLength()-1 ] == _T('\\');
	bool isLastHas	= !s2.IsEmpty() && s2[ 0 ] == _T('\\');

	if( (isPrevHas && !isLastHas) || (!isPrevHas && isLastHas) ) return s1 + s2;
	else if( isPrevHas && isLastHas ) return s1 + s2.Mid(1);

	return s1 + _T("\\") + s2;
}
//-------------------------------------------------------------------------
template < typename StringT1, typename StringT2, typename StringT3 >
CString MakePath( StringT1 str1, StringT2 str2, StringT3 str3 )
{
	return MakePath( str1, MakePath( str2, str3 ) );
}
//-------------------------------------------------------------------------
template < typename StringT1, typename StringT2 >
bool IsEqualPaths( StringT1 str1, StringT2 str2 )
{
	CString s1 = toCorrectCString( str1 );
	CString s2 = toCorrectCString( str2 );
	s1.MakeUpper();
	s2.MakeUpper();
	return( s1 == s2 );
}
//-------------------------------------------------------------------------
template < typename StringT >
CString ExtractFileName( StringT path )
{
	CString s = toCorrectCString( path );
	int pos = s.ReverseFind( _T('\\') );
	if( pos > -1 ) return s.Mid( pos+1 );
	return s;
}
//-------------------------------------------------------------------------
template < typename StringT >
CString ExtractFileNameWithoutExt( StringT path )
{
	CString s = toCorrectCString( path );
	int pos = s.ReverseFind( _T('\\') );
	if( pos > -1 ) s = s.Mid( pos+1 );
	pos = s.ReverseFind( _T('.') );
	if( pos > -1 ) s = s.Left( pos );
	return s;
}
//-------------------------------------------------------------------------
template < typename StringT >
CString ExtractDirPath( StringT path )
{
	CString s = toCorrectCString( path );
	int pos = s.ReverseFind( _T('\\') );
	if( pos > -1 ) return s.Mid( 0, pos );
	return s;
}
//-------------------------------------------------------------------------
template < typename StringT >
CString ExtractFileExt( StringT path )
{
	CString s = toCorrectCString( path );
	int pos = s.Find( _T('.') );
	if( pos > -1 ) return s.Mid( pos+1 );
	return _T("");
}
//-------------------------------------------------------------------------



template < typename StringT, typename ContainerT >
inline 
void SplitPath( StringT path, ContainerT& out )
{
	tstring s = toCorrectStdString( path );
	tchar name[ _MAX_PATH ];
	size_t pos = 0;
	name[pos] = _T('\0');
	const tstring::size_type len = s.length();
	for( tstring::size_type i = 0; i < len; ++i )
	{
		if( s[i] == _T('\\') )
		{
			name[pos] = _T('\0');
			if( pos ) out.push_back( name );
			pos = 0;
		}
		else name[pos++] = s[i];
	}
	if( pos ) { name[pos] = _T('\0'); out.push_back( name ); }
}
//-------------------------------------------------------------------------
template < typename ContainerT >
inline
CString JoinPath( ContainerT const& in )
{
	CString str = _T("");
	for( ContainerT::const_iterator i = in.begin(); i != in.end(); ++i )
	{
		if( !str.IsEmpty() ) str += _T("\\");
		str += *i;
	}
	return str;
}
//-------------------------------------------------------------------------



/**********************************************************************
*               -= ��������� ������ � ���������� =-
**********************************************************************/
inline
void DeleteDirectoryWithAllContent( LPCTSTR lpszDir )
{
	if(IsNotExistDirectory( lpszDir ))
		return;

	{
		SetCurrDir curdir( lpszDir );

		CFileFind finder;
		BOOL bWorking = finder.FindFile( _T("*.*") );

		while( bWorking )
		{
			bWorking = finder.FindNextFile();

			// skip . and .. files; otherwise, we'd
			// recur infinitely!
			if( finder.IsDots() ) continue;

			// if it's a directory, recursively search it
			if( finder.IsDirectory() )
				DeleteDirectoryWithAllContent( finder.GetFilePath() );
			else 
				::DeleteFile( finder.GetFilePath() ); // this is a file
		}

		finder.Close();
	}	
	::RemoveDirectory( lpszDir );
}

// N.Y.Kuzmin

inline
void DeleteAllContentFromDirectory( LPCTSTR lpszDir )
{
	if(IsNotExistDirectory( lpszDir ))
		return;
	{
		SetCurrDir curdir( lpszDir );

		CFileFind finder;
		BOOL bWorking = finder.FindFile( _T("*.*") );

		while( bWorking )
		{
			bWorking = finder.FindNextFile();

			// skip . and .. files; otherwise, we'd
			// recur infinitely!
			if( finder.IsDots() ) continue;

			// if it's a directory, recursively search it
			if( finder.IsDirectory() )
				DeleteDirectoryWithAllContent( finder.GetFilePath() );
			else 
				::DeleteFile( finder.GetFilePath() ); // this is a file
		}

		finder.Close();
	}	
}

inline
bool DeleteFileByPath( LPCTSTR lpszFile )
{
	return(TRUE == ::DeleteFile( lpszFile ));
}

//-------------------------------------------------------------------------
// � �������� ����������� ����� �������� ���������� ������� ��� ��������� � ����������:
// void fnc( LPCTSTR lpszFile )
//		��� �������������� ������:
// struct Functor
// {
//		void operator()( LPCTSTR lpszFile )
//		{
//			 .... process file or folder
//		}
// };
template < typename HandlerT >
void ProcessEachFile( LPCTSTR lpszDir, HandlerT& handler, LPCTSTR lpszMask = _T("*.*"), bool isRecursively = true )
{
	tstring mask = toCorrectStdString( lpszMask );
	SetCurrDir curdir( lpszDir );

	// ������� ������ �� ���� ������ �����:
	CFileFind finder;
	BOOL bWorking = finder.FindFile( mask.c_str() );
	while( bWorking )
	{
		bWorking = finder.FindNextFile();
		if( !finder.IsDots() && !finder.IsDirectory() )
			handler( finder.GetFilePath() );
	}
	finder.Close();

	if( !isRecursively ) return;

	// ������ ������ �� ���� �����������:
	bWorking = finder.FindFile( _T("*.*") );
	while( bWorking )
	{
		bWorking = finder.FindNextFile();
		if( !finder.IsDots() && finder.IsDirectory() )
			ProcessEachFile( finder.GetFilePath(), handler, lpszMask, isRecursively );
	}
	finder.Close();
}
//-------------------------------------------------------------------------
template < typename HandlerT >
void ProcessEachDir( LPCTSTR lpszDir, HandlerT& handler, LPCTSTR lpszMask = _T("*.*"), bool isRecursively = false )
{
	tstring mask = toCorrectStdString( lpszMask );
	SetCurrDir curdir( lpszDir );

	// �������� �� ��������� ����������:
	CFileFind finder;
	BOOL bWorking = finder.FindFile( mask.c_str() );
	while( bWorking )
	{
		bWorking = finder.FindNextFile();
		if( !finder.IsDots() && finder.IsDirectory() )
		{
			handler( finder.GetFilePath() );
			if( isRecursively )
				ProcessEachDir( finder.GetFilePath(), handler, lpszMask, isRecursively );
		}
	}
	finder.Close();
}
//-------------------------------------------------------------------------

// ��������������� �������� ��� ����������� (������������) ����������
namespace aux
{
	/// ���������� ��������� ���������� (������������� ����) � ����� ������
	struct make_dir_list
	{
		/// .ctor
		make_dir_list
			( std::list<CString>&	outDirList		// ������� ������
			, int					srcLen			// ����� ���������� �������, ������� ���� �������� �� �����������, ����� �������� �������������
			) 
			: m_dirs(outDirList)
			, m_srclen(srcLen)
			{}

		/// exec
		void operator()( LPCTSTR lpszDir )
		{
			m_dirs.push_back( CString(lpszDir).Mid(m_srclen) );
		}

		std::list<CString>&		m_dirs;		///< ������� ������
		int						m_srclen;	///< ����� ������� ���� ���������
	};

	/// ����������� �����
	struct copy_file
	{
		/// .ctor
		copy_file
			( const CString&	strTrgDir		// ������� �����
			, int				srcLen			// ����� ���������� �������, ������� ���� �������� �� �����������, ����� �������� �������������
			)
			: m_strTrgDir(strTrgDir)
			, m_srcLen(srcLen)
			{}

		/// exec
		void operator()( LPCTSTR lpszFile )
		{
			if( !::CopyFile( lpszFile, MakePath( m_strTrgDir, CString(lpszFile).Mid(m_srcLen) ), FALSE ) )
				throw std::exception();
		}

		CString	m_strTrgDir;
		int		m_srcLen;
	};

} // namespace aux

//-------------------------------------------------------------------------
/// ����������� ����������: ��� ���������� (�� �����) ������ lpszSrcDir ����� ����������� ������ ����� lpszTrgDir
/// 

inline
bool CopyDirectory
	( LPCTSTR lpszSrcDir				///< �����-�������� 
	, LPCTSTR lpszTrgDir				///< �����, � ������� ����������
	, LPCTSTR lpszMask = _T("*.*")		///< ����� ������ ������
	, bool isRecursively = true			///< ���� �� ������������ ����������
	, bool deleteDirOnError = true		///< ���� �� ������� ����� lpszTrgDir ��� �������
	)
{
	if( !IsExistDirectory(lpszSrcDir) || !EnsureDirectory(lpszTrgDir) )
		return false;

	try
	{
		CString src = lpszSrcDir;
		CString trg	= lpszTrgDir;
		int		len	= src.GetLength();

		typedef std::list<CString>	DirList;

		// ��������� ������ ���� ���������� �����
		DirList dirs;
		aux::make_dir_list dirhandler(dirs, len);
		ProcessEachDir( lpszSrcDir, dirhandler, _T("*.*"), isRecursively );

		// ������� ������ ��������:
		for( DirList::iterator it = dirs.begin(); it != dirs.end(); ++it )
			if( !EnsureDirectory( MakePath(trg, *it) ) ) throw 1;

		// �������� �� ������ � �������� ��
		aux::copy_file filehandler(trg, len);
		ProcessEachFile( lpszSrcDir, filehandler, lpszMask, isRecursively );
	}
	catch( ... )
	{
		if( deleteDirOnError ) DeleteDirectoryWithAllContent( lpszTrgDir );
		return false;
	}

	return true;
}

//-------------------------------------------------------------------------




}	//namespace filesystem


#endif // _FILE_DIR_HELPER_H__
