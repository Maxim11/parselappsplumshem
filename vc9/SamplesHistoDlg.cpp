#include "stdafx.h"

#include "SamplesHistoDlg.h"
#include "MainFrm.h"

#include <math.h>


static CSamplesHistoDlg* pSamH = NULL;
static bool iscancel = false;

static int LbClickCallback(UINT /*nFlags*/, CPoint point, void *pData)
{
	CRect rect;
	rect.left = point.x - 1;
	rect.right = point.x + 1;
	rect.top = point.y - 1;
	rect.bottom = point.y + 1;

	if(pSamH != NULL)
		pSamH->ShowInfo(&rect);

	return 0;
}

static int LbUnClickCallback(UINT /*nFlags*/, CPoint /*point*/, void *pData)
{
	if(pSamH != NULL)
		pSamH->RemoveInfo();

	return 1;
}

static int RbClickCallback(UINT /*nFlags*/, CPoint /*point*/, void *pData)
{
	return 1;
}

static int RbUnClickCallback(UINT /*nFlags*/, CPoint /*point*/, void *pData)
{
	if(pSamH != NULL)
		pSamH->SetStatusMenu();

	return 1;
}

IMPLEMENT_DYNAMIC(CSamplesHistoDlg, CDialog)

CSamplesHistoDlg::CSamplesHistoDlg(VPSam *data, VModPar* params, CProjectData* prj, int mode, CWnd* pParent)
	: CDialog(CSamplesHistoDlg::IDD, pParent)
{
	pSamH = this;

	Data = data;
	Params = params;
	ParentPrj = prj;
	Mode = mode;

	m_vParam = 0;
	m_vNPart = CalcDefault();

	pGraph = NULL;
	pMenu1 = new CMenu;
	pMenu1->CreatePopupMenu();

	posInfo = NULL;
}

CSamplesHistoDlg::~CSamplesHistoDlg()
{
	if(pGraph != NULL)  delete pGraph;

	pMenu1->DestroyMenu();
	delete pMenu1;
}

void CSamplesHistoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_PARAM, m_Param);
	DDX_Control(pDX, IDC_NPART, m_NPart);
	DDX_Control(pDX, IDC_SPIN_LETTER, m_SpinLetter);

	DDX_CBIndex(pDX, IDC_PARAM, m_vParam);

	cParcelDDX(pDX, IDC_NPART, m_vNPart);
	cParcelDDV(pDX, ParcelLoadString(STRID_SAMPLES_HISTO_DDV_NPART), m_vNPart, 2, 30);
}

BEGIN_MESSAGE_MAP(CSamplesHistoDlg, CDialog)

	ON_COMMAND(C_SAMPLESH_MENU_GRID, OnUserMenuGrid)

	ON_CBN_SELCHANGE(IDC_PARAM, OnChangeParam)

	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_LETTER, OnSpinLetterDeltaPos)

	ON_BN_CLICKED(IDHELP, OnHelp)
	ON_BN_CLICKED(IDC_SET_DEFAULT, OnDefault)

	ON_EN_CHANGE(IDC_NPART, OnNPartChange)

END_MESSAGE_MAP()

BOOL CSamplesHistoDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	iscancel = false;

	CString s;

	m_Param.ResetContent();
	ItModPar it;
	for(it=Params->begin(); it!=Params->end(); ++it)
	{
		s = it->ParamName;
		m_Param.AddString(s);
	}
	m_Param.SetCurSel(m_vParam);

	CWnd* pWindow = GetDlgItem(IDC_NPART);
	if(pWindow != 0)
	{
		m_SpinLetter.SetBuddy(pWindow);
	    m_SpinLetter.SetRange(2, 30);
		m_SpinLetter.SetPos(m_vNPart);
	}

	CreateChart();

	CString Hdr;
	switch(Mode)
	{
	case C_SAMPLESH_CALIB:  
		Hdr = ParcelLoadString(STRID_SAMPLES_HISTO_HDR_CALIB);  
		break;
	case C_SAMPLESH_VALID:  
		Hdr = ParcelLoadString(STRID_SAMPLES_HISTO_HDR_VALID);  
		break;
	}
	SetWindowText(Hdr);

	GetDlgItem(IDCANCEL)->SetWindowText(ParcelLoadString(STRID_SAMPLES_HISTO_EXIT));
	GetDlgItem(IDHELP)->SetWindowText(ParcelLoadString(STRID_SAMPLES_HISTO_HELP));
	GetDlgItem(IDC_SET_DEFAULT)->SetWindowText(ParcelLoadString(STRID_SAMPLES_HISTO_DEFAULT));
	GetDlgItem(IDC_STATIC_1)->SetWindowText(ParcelLoadString(STRID_SAMPLES_HISTO_PARTS));
	GetDlgItem(IDC_STATIC_3)->SetWindowText(ParcelLoadString(STRID_SAMPLES_HISTO_PARAM));

	GetDlgItem(IDHELP)->EnableWindow(pFrame->IsExistHelp());

	return true;
}

void CSamplesHistoDlg::OnCancel()
{
	iscancel = true;

	CDialog::OnCancel();
}

void CSamplesHistoDlg::OnDefault()
{
	m_vNPart = CalcDefault();

	UpdateData(0);

	CreateGraph();
}

void CSamplesHistoDlg::OnChangeParam()
{
	m_vParam = m_Param.GetCurSel();

	UpdateData(0);

	CreateGraph();
}

void CSamplesHistoDlg::OnNPartChange()
{
	if(iscancel)
		return;
	if(GetFocus() == GetDlgItem(IDCANCEL))
	{
		OnCancel();
		return;
	}

	int old = m_vNPart;
	if(!UpdateData())
		return;

	if(old != m_vNPart)
		CreateGraph();
}

void CSamplesHistoDlg::OnSpinLetterDeltaPos(NMHDR *pNMHDR, LRESULT *pResult)
{
	if(pNMHDR == 0 || pResult == 0) return;

	NMUPDOWN *pNMUpDown = (NMUPDOWN *) pNMHDR;

	pNMUpDown->iDelta *= 1;
}
/*
void CSamplesHistoDlg::OnUserMenuPrev()
{
	pChart->GetWnd()->PostMessage(WM_COMMAND, LC_POPUP_GO_BACK, 0);
}

void CSamplesHistoDlg::OnUserMenuNext()
{
	pChart->GetWnd()->PostMessage(WM_COMMAND, LC_POPUP_GO_FORWARD, 0);
}

void CSamplesHistoDlg::OnUserMenuFull()
{
	pChart->GetWnd()->PostMessage(WM_COMMAND, LC_POPUP_FULL_SCREEN, 0);
}

void CSamplesHistoDlg::OnUserMenuAuto()
{
	pChart->GetWnd()->PostMessage(WM_COMMAND, LC_POPUP_AUTOY, 0);
}

void CSamplesHistoDlg::OnUserMenuLeft()
{
	pChart->GetWnd()->PostMessage(WM_COMMAND, LC_POPUP_XLEFT, 0);
}

void CSamplesHistoDlg::OnUserMenuRight()
{
	pChart->GetWnd()->PostMessage(WM_COMMAND, LC_POPUP_XRIGHT, 0);
}

void CSamplesHistoDlg::OnUserMenuLegend()
{
	bool btmp;
	pChart->IsLegendEnabled(&btmp);
	pChart->ShowLegend(!btmp);
}
*/
void CSamplesHistoDlg::OnUserMenuGrid()
{
	bool btmp, btmp2;
	pChart->IsGridEnabled(&btmp, &btmp2);
	pChart->ShowGrid(!btmp, !btmp);
}

int CSamplesHistoDlg::CalcDefault()
{
	double N = double(Data->size());
	return int(1 + 3.322 * log10(N));
}

void CSamplesHistoDlg::CreateGraph()
{
	if(pGraph != NULL)
	{
		pChart->RemoveGraph(pGraph->GetPos());
		delete pGraph;
		pGraph = NULL;
	}

	int i;

	y.clear();
	x.clear();

	double min = 1.e9, max = -1.e9;
	ItVPSam it;
	for(it=Data->begin(); it!=Data->end(); ++it)
	{
		double Ref = (*it)->GetReferenceData((*Params)[m_vParam].ParamName)->Value;
		if(min > Ref)  min = Ref;
		if(max < Ref)  max = Ref;
	}
	double sh = (max - min) / m_vNPart;

	for(i=0; i<m_vNPart + 1; i++)
	{
		x.push_back(min + i * sh);
		y.push_back(0.);
	}

	double Nmax = 0.;
	for(it=Data->begin(); it!=Data->end(); ++it)
	{
		double Ref = (*it)->GetReferenceData((*Params)[m_vParam].ParamName)->Value;
		int ind = (sh > 1.e-9) ? int((Ref - min) / sh) : 0;
		if(ind < 0 || ind >= m_vNPart)  ind = m_vNPart - 1;

		y[ind] += 1.;
		if(Nmax < y[ind])
			Nmax += 1.;
	}
	y[m_vNPart] = y[m_vNPart - 1];

	pGraph = new CLumChartDataSet(m_vNPart + 1, x, y);
	pGraph->SetInterpolation(LCF_CONNECT_HISTO);
	pGraph->SetColor(cColorBlack);
	pGraph->SetMarkerColor(cColorPass);
	CString s = (Mode == C_SAMPLESH_CALIB) ? ParcelLoadString(STRID_SAMPLES_HISTO_GRAPH_CALIBNAME)
		: ParcelLoadString(STRID_SAMPLES_HISTO_GRAPH_VALIDNAME);
	pGraph->SetName(s);
	pGraph->SetFormat(cFmt31, _T("%2.0f"));

	pChart->AddGraph(pGraph, true);

	pChart->SetUserViewPort(min, 0., max, Nmax + 1.);

	pChart->EnableGraphText(pGraph->GetPos(), true);

	pChart->RedrawWindow();
}

void CSamplesHistoDlg::CreateChart()
{
	CRect RectGraph;
	GetDlgItem(IDC_STATIC_2)->GetWindowRect(RectGraph);

	pChart = new CLumChart(RectGraph, this, LCF_RECT_SCREEN);
	pChart->SetAxesNames(ParcelLoadString(STRID_SAMPLES_HISTO_GRAPH_AXISREF),
		ParcelLoadString(STRID_SAMPLES_HISTO_GRAPH_AXISNUM));

	CreateGraph();

	COLORREF bg = GetSysColor(COLOR_3DFACE);
	pChart->SetBgOutsideColor(bg, cColorBlack);
	pChart->SetMargins(45, 30, 15, 50);

	pChart->ShowLegend(false);
	pChart->EnableUpperLegend(false);
	pChart->EnableCoordLegend(false);

	pChart->SetUserCallback(LCF_CB_LBDOWN, LbClickCallback);
	pChart->SetUserCallback(LCF_CB_LBUP, LbUnClickCallback);
	pChart->SetUserCallback(LCF_CB_RBDOWN, RbClickCallback);
	pChart->SetUserCallback(LCF_CB_RBUP, RbUnClickCallback);
	pChart->EnableUserCallback(LCF_CB_LBDOWN, true);
	pChart->EnableUserCallback(LCF_CB_LBUP, true);
	pChart->EnableUserCallback(LCF_CB_RBDOWN, true);
	pChart->EnableUserCallback(LCF_CB_RBUP, true);

	pChart->SetSelFrameProps(RGB(0, 0, 255), 2);

	BuildUserPopupMenu();
	pChart->SetUserPopupMenu(this, pMenu1);
	pChart->EnableUserPopupMenu(true);
}

void CSamplesHistoDlg::ShowInfo(CRect* rect)
{
	int i, N = pChart->LocateGraphPoints(pGraph->GetPos(), (*rect), 0);
	if(N > 0)
	{
		int* pidx = new int[N];
		pChart->LocateGraphPoints(pGraph->GetPos(), (*rect), pidx);

		vector<CLCULString> strings;
		for(i=0; i<N; i++)
		{
			int ind = pidx[i];
			if(ind < 0 || ind >= m_vNPart)
				continue;
			CParamData* pParam = ParentPrj->GetParam((*Params)[m_vParam].ParamName);
			if(pParam == NULL)
				continue;

			CLCULString NewString;

			CString str, s2, s3, fmt = pParam->GetFormat();
			s2.Format(fmt, x[ind]);
			s3.Format(fmt, x[ind+1]);
			str.Format(ParcelLoadString(STRID_SAMPLES_HISTO_GRAPH_INFO), int(y[ind]), s2, s3);
			NewString.SetText(str);
			strings.push_back(NewString);
		}

		CLCUL legend;
		legend.MoveTo(0, 0);
		legend.Resize(100, 20);
		legend.SetStrings(strings);

		legend.SetFillColor(cColorGraphNorm);
		legend.SetTextColor(cColorBlack);

		legend.Enable(true);
		legend.EnableMoveToFit(true);
		legend.EnableAutoWidth(true);
		legend.EnableAutoHeight(true);

		posInfo = pChart->AddUserLegend(legend);
	}

	pChart->RedrawWindow();
}

void CSamplesHistoDlg::RemoveInfo()
{
	if(posInfo != NULL)
	{
		pChart->RemoveUserLegend(posInfo);
		posInfo = NULL;

		pChart->RedrawWindow();
	}
}

void CSamplesHistoDlg::BuildUserPopupMenu()
{
	pMenu1->AppendMenu(MF_STRING, C_SAMPLESH_MENU_GRID, ParcelLoadString(STRID_SAMPLES_HISTO_GRAPH_MENU_GRID));
}

void CSamplesHistoDlg::SetStatusMenu()
{
	UINT nFlags;
	bool btmp = false, btmp2 = false;

	pChart->IsGridEnabled(&btmp, &btmp2);
	nFlags = (btmp) ? MF_CHECKED : MF_UNCHECKED;
	pMenu1->CheckMenuItem(C_SAMPLESH_MENU_GRID, nFlags);
}

void CSamplesHistoDlg::OnHelp()
{
	int IdHelp = DLGID_MAIN;
	switch(Mode)
	{
	case C_SAMPLESH_CALIB:  IdHelp = DLGID_SHOW_HISTO_CALIB;  break;
	case C_SAMPLESH_VALID:  IdHelp = DLGID_SHOW_HISTO_VALID;  break;
	}

	pFrame->ShowHelp(IdHelp);
}
