#include "stdafx.h"

#include "BranchInfo.h"
#include "TransferData.h"
#include "lltimer.h"

#include <math.h>

PlateData::PlateData(int num):Num(num)
{
	CString fmtS = ParcelLoadString(STRID_SSTNAMES_SAMPLE) + cFmt1;
	switch(Num)
	{
	case 0:
		SamName.Format(fmtS, 0);
		R = 0.;
		T0 = 0.;
		T1 = 0.;
		T2 = 0.;
		T3 = 0.;
		T4 = 0.;
		break;
	case 1:
		SamName.Format(fmtS, 1);
		R = 0.037;
		T0 = 0.;
		T1 = 1.203;
		T2 = 1.352;
		T3 = 1.578;
		T4 = 1.778;
		break;
	case 2:
		SamName.Format(fmtS, 2);
		R = 0.037;
		T0 = 0.;
		T1 = 0.961;
		T2 = 1.171;
		T3 = 1.374;
		T4 = 1.565;
		break;
	case 3:
		SamName.Format(fmtS, 3);
		R = 0.038;
		T0 = 0.;
		T1 = 0.965;
		T2 = 1.163;
		T3 = 1.379;
		T4 = 1.568;
		break;
	}
}

CTransferData::CTransferData(HTREEITEM hParent)
{
	Name = cEmpty;
	Type = C_TRANS_TYPE_OWN;

	hItem = NULL;
	hParentItem = hParent;

	CProjectData* Prj = GetProjectByHItem(hParentItem);

	MasterDevType = C_DEV_TYPE_10;
	MasterDevNumber = 1;
	Date = CTime::GetCurrentTime();
	LowLimSpec = Prj->GetLowLimSpecRange();
	UpperLimSpec = Prj->GetUpperLimSpecRange();

	FrameName = cEmpty;
}

CTransferData::~CTransferData()
{
	Preproc.clear();
	Samples.clear();
	ArtSamples.clear();
};

void CTransferData::Copy(CTransferData& data)
{
	int i;

	Name = data.GetName();
	Type = data.GetType();
	hItem = data.GetHItem();
	hParentItem = data.GetParentHItem();
	MasterDevType = data.GetMasterDevType();
	MasterDevNumber = data.GetMasterDevNumber();
	Date = data.GetDate();
	LowLimSpec = data.GetLowLimSpec();
	UpperLimSpec = data.GetUpperLimSpec();

	Preproc.clear();
	copy(data.Preproc.begin(), data.Preproc.end(), inserter(Preproc, Preproc.begin()));

	Samples.clear();
	for(i=0; i<data.GetNSamples(); i++)
		Samples.push_back(*(data.GetSample(i)));

	ArtSamples.clear();
	CSampleData *pArt = data.GetArtSample();
	if(pArt != NULL)
	{
		CSampleData ArtSam(hParentItem, hItem);
		ArtSam.Copy(*(pArt));
		ArtSamples.push_back(ArtSam);
	}
}

HTREEITEM CTransferData::GetHItem()
{
	return hItem;
}

void CTransferData::SetHItem(HTREEITEM item)
{
	hItem = item;
	ItSam it;
	for(it=Samples.begin(); it!=Samples.end(); ++it)
		it->SetHTransItem(item);

	if(int(ArtSamples.size()) > 0)
		ArtSamples[0].SetHTransItem(item);
}

void CTransferData::SetParentHItem(HTREEITEM item)
{
	hParentItem = item;

	ItSam it;
	for(it=Samples.begin(); it!=Samples.end(); ++it)
		it->SetParentHItem(item);

	if(int(ArtSamples.size()) > 0)
		ArtSamples[0].SetParentHItem(item);
}

HTREEITEM CTransferData::GetParentHItem()
{
	return hParentItem;
}

CString CTransferData::GetName()
{
	return Name;
}

void CTransferData::SetName(CString name)
{
	Name = name;
}

int CTransferData::GetType()
{
	return Type;
}

void CTransferData::SetType(int type)
{
	Type = type;
}

CTime CTransferData::GetDate()
{
	return Date;
}

void CTransferData::SetDate()
{
	Date = CTime::GetCurrentTime();
}

int CTransferData::GetMasterDevType()
{
	return MasterDevType;
}

void CTransferData::SetMasterDevType(int type)
{
	MasterDevType = type;
}

int CTransferData::GetMasterDevNumber()
{
	return MasterDevNumber;
}

void CTransferData::SetMasterDevNumber(int num)
{
	MasterDevNumber = num;
}

CString CTransferData::GetMasterDevName()
{
	CString s, s1;

	s1.Format(cFmt08, MasterDevNumber);

	s += cGetDeviceTypeName(MasterDevType) + ParcelLoadString(STRID_FIELD_NUM) + s1;

	return s;
}

int CTransferData::GetLowLimSpec()
{
	return LowLimSpec;
}

void CTransferData::SetLowLimSpec(int lim)
{
	LowLimSpec = lim;
}

int CTransferData::GetUpperLimSpec()
{
	return UpperLimSpec;
}

void CTransferData::SetUpperLimSpec(int lim)
{
	UpperLimSpec = lim;
}

int CTransferData::GetNPreproc()
{
	int N;
	ItInt it;
	for(N = 0, it=Preproc.begin(); it!= Preproc.end(); ++it, N++)
		if((*it) == C_PREPROC_0)
            break;

	return N;
}

int CTransferData::GetPreprocInd(int num)
{
	if(num < 0 || num >= GetNPreproc())
		return C_PREPROC_0;

	return Preproc[num];
}

CString CTransferData::GetAllPreprocAsStr()
{
	return cGetPreprocAsStr(&Preproc);
}

void CTransferData::GetAllPreproc(VInt& vect)
{
	vect.clear();
	copy(Preproc.begin(), Preproc.end(), inserter(vect, vect.begin()));
}

void CTransferData::SetAllPreproc(VInt* preps)
{
	Preproc.clear();
	copy(preps->begin(), preps->end(), inserter(Preproc, Preproc.begin()));
}

void CTransferData::SetAllPreproc(CString prepstr)
{
	Preproc.clear();
	cGetPreprocFromStr(prepstr, Preproc, 7);
}

PlateData* CTransferData::GetPlate(int num)
{
	ItPlate it;
	for(it=Plates.begin(); it!=Plates.end(); ++it)
		if(it->Num == num)
			return &(*it);

	return NULL;
}

int CTransferData::GetNSamples()
{
	return int(Samples.size());
}

int CTransferData::GetNSpectra()
{
	int nAll = 0;
	ItSam it;
	for(it=Samples.begin(); it!=Samples.end(); ++it)
		nAll += it->GetNSpectra();

	return nAll;
}

int CTransferData::GetNSpectraUsed()
{
	int nAll = 0;
	ItSam it;
	for(it=Samples.begin(); it!=Samples.end(); ++it)
		nAll += it->GetNSpectraUsed();

	return nAll;
}

CSampleData* CTransferData::GetSample(CString name)
{
	ItSam it;
	for(it=Samples.begin(); it!=Samples.end(); ++it)
	{
		if(!(*it).GetName().CompareNoCase(name))
		{
			return &(*it);
		}
	}

	return NULL;
}

CSampleData* CTransferData::GetSample(int ind)
{
	if(ind < 0 || ind >= int(Samples.size()))
	{
		return NULL;
	}

	int i;
	ItSam it;
	for(i=0, it=Samples.begin(); i<ind; i++)
	{
		++it;
	}

	return &(*it);
}

CSampleData* CTransferData::GetLastSample()
{
	return GetSample(GetNSamples() - 1);
}

bool CTransferData::AddSample(CSampleData* NewSam)
{
	if(!GetDatabase()->BeginTrans())
		return false;

	CProjectData *Prj = GetProjectByHItem(hParentItem);

	prdb::PRSample Obj;
	NewSam->GetDataForBase(Obj);
	if(!GetDatabase()->SampleAddEx(Prj->GetDevNumber(), Prj->GetName(), Name, cEmpty, &Obj))
	{
		GetDatabase()->Rollback();
		return false;
	}

	ItPar it;
	for(it=Prj->Params.begin(); it!=Prj->Params.end(); ++it)
	{
		prdb::PRRefData Obj;
		NewSam->GetRefDataForBase(it->GetName(), Obj);
		if(!GetDatabase()->RefDataAddEx(Prj->GetDevNumber(), Prj->GetName(), Name, NewSam->GetName(), &Obj))
		{
			GetDatabase()->Rollback();
			return false;
		}
	}

	int ind2;
	for(ind2=0; ind2<int(NewSam->SpecData.size()); ind2++)
	{
		Spectrum *Spec = NewSam->GetSpectrumByInd(ind2);
		prdb::PRSpectrum Obj2;
		Obj2.SpecNum = Spec->Num;
		NewSam->GetSpecDataForBase(Obj2);
		if(!GetDatabase()->SpectrumAddEx(Prj->GetDevNumber(), Prj->GetName(), Name, NewSam->GetName(), &Obj2))
		{
			GetDatabase()->Rollback();
			return false;
		}
	}

	GetDatabase()->CommitTrans();

	if(NewSam->GetName().Compare(cArtSam))
	{
		Samples.push_back((*NewSam));
		GetLastSample()->SetParentHItem(hParentItem);
	}
	else
	{
		ArtSamples.clear();
		ArtSamples.push_back((*NewSam));
		ArtSamples[0].SetParentHItem(hParentItem);
	}

	Prj->SortSamples(Name);

	return true;
}

bool CTransferData::DeleteSample(int ind)
{
	CProjectData *Prj = GetProjectByHItem(hParentItem);
	if(Prj->GetSampleKeepStatus(ind) != C_KEEP_FREE)
		return false;

	int i;
	ItSam it;
	for(it=Samples.begin(), i=0; it!=Samples.end(); ++it, i++)
		if(i == ind)
		{
			if(!GetDatabase()->SampleDelete(Prj->GetDevNumber(), Prj->GetName(), Name, it->GetName()))
				return false;

			Samples.erase(it);

			return true;
		}

	return false;
}

bool CTransferData::DeleteSample(CString name)
{
	CProjectData *Prj = GetProjectByHItem(hParentItem);
	if(Prj->GetSampleKeepStatus(name) != C_KEEP_FREE)
		return false;

	ItSam it;
	for(it=Samples.begin(); it!=Samples.end(); ++it)
		if(!it->GetName().CompareNoCase(name))
		{
			if(!GetDatabase()->SampleDelete(Prj->GetDevNumber(), Prj->GetName(), Name, name))
				return false;

			Samples.erase(it);

			return true;
		}

	return false;
}

CSampleData* CTransferData::GetArtSample()
{
	if(int(ArtSamples.size()) > 0)
		return &ArtSamples[0];

	return NULL;
}

bool CTransferData::DeleteArtSample()
{
	CProjectData *Prj = GetProjectByHItem(hParentItem);
	if(!GetDatabase()->SampleDelete(Prj->GetDevNumber(), Prj->GetName(), Name, cArtSam))
		return false;

	ArtSamples.clear();

	return true;
}

CString CTransferData::GetFrameName()
{
	return FrameName;
}

void CTransferData::SetFrameName(CString name)
{
	FrameName = name;
}

int CTransferData::GetNSpecPoints()
{
	return int(SpecPoints.size());
}

double CTransferData::GetSpecPoint(int indSP)
{
	if(indSP < 0 || indSP >= GetNSpecPoints())
		return 0.;

	return SpecPoints[indSP];
}

int CTransferData::GetSpecPointInd(double value)
{
	int i;
	ItDbl it;
	for(i=0, it=SpecPoints.begin(); it!=SpecPoints.end(); ++it, i++)
	{
		if(fabs(value - (*it)) < 1.e-9)
			return i;
	}

	return -1;
}

void CTransferData::GetSpecPoints()
{
	CProjectData *Prj = GetProjectByHItem(hParentItem);

	Prj->ExtractSpecPoints(LowLimSpec, UpperLimSpec, SpecPoints);
}

void CTransferData::GetMeanSpec(VDbl& Mean)
{
	Mean.clear();
	copy(MeanSpec.begin(), MeanSpec.end(), inserter(Mean, Mean.begin()));
}

void CTransferData::SetMeanSpec(VDbl& mean)
{
	MeanSpec.clear();
	copy(mean.begin(), mean.end(), inserter(MeanSpec, MeanSpec.begin()));
}

void CTransferData::GetMeanSpecMSC(VDbl& Mean)
{
	Mean.clear();
	copy(MeanSpecMSC.begin(), MeanSpecMSC.end(), inserter(Mean, Mean.begin()));
}

void CTransferData::SetMeanSpecMSC(VDbl& mean)
{
	MeanSpecMSC.clear();
	copy(mean.begin(), mean.end(), inserter(MeanSpecMSC, MeanSpecMSC.begin()));
}

void CTransferData::GetMeanSpecStd(VDbl& Mean)
{
	Mean.clear();
	copy(MeanSpecStd.begin(), MeanSpecStd.end(), inserter(Mean, Mean.begin()));
}

void CTransferData::SetMeanSpecStd(VDbl& mean)
{
	MeanSpecStd.clear();
	copy(mean.begin(), mean.end(), inserter(MeanSpecStd, MeanSpecStd.begin()));
}

void CTransferData::GetDataForBase(prdb::PRTrans& Obj)
{
	Obj.TransName = Name;
	Obj.Type = Type;
	Obj.MasterDevType = MasterDevType;
	Obj.MasterDevNumber = MasterDevNumber;
	Obj.DateCreated = Date;
	Obj.LowLim = LowLimSpec;
	Obj.UpperLim = UpperLimSpec;
	Obj.Preproc = cGetPreprocAsStr(&Preproc);

	ItPlate it;
	Obj.Plates.clear();
	for(it=Plates.begin(); it!=Plates.end(); ++it)
	{
		PRPlate NewPlate;
		NewPlate.PlateNum = it->Num;
		NewPlate.SamName = it->SamName;
		NewPlate.R = it->R;
		NewPlate.T0 = it->T0;
		NewPlate.T1 = it->T1;
		NewPlate.T2 = it->T2;
		NewPlate.T3 = it->T3;
		NewPlate.T4 = it->T4;

		Obj.Plates.push_back(NewPlate);
	}
}

void CTransferData::SetDataFromBase(prdb::PRTrans& Obj)
{
	Name = Obj.TransName;
	Type = Obj.Type;
	MasterDevType = Obj.MasterDevType;
	MasterDevNumber = Obj.MasterDevNumber;
	Date = Obj.DateCreated;
	LowLimSpec = Obj.LowLim;
	UpperLimSpec = Obj.UpperLim;
	cGetPreprocFromStr(Obj.Preproc, Preproc, 7);

	vector<PRPlate>::iterator it;
	Plates.clear();
	for(it=Obj.Plates.begin(); it!=Obj.Plates.end(); ++it)
	{
		PlateData NewPlate(it->PlateNum);

		NewPlate.SamName = it->SamName;
		NewPlate.R = it->R;
		NewPlate.T0 = it->T0;
		NewPlate.T1 = it->T1;
		NewPlate.T2 = it->T2;
		NewPlate.T3 = it->T3;
		NewPlate.T4 = it->T4;

		Plates.push_back(NewPlate);
	}

	GetSpecPoints();
}

bool CTransferData::LoadDatabase()
{
	ItSam it;
	LSam NewSamples;

	CProjectData *Prj = GetProjectByHItem(hParentItem);

	if(!GetDatabase()->SamplesRead(Prj->GetDevNumber(), Prj->GetName(), Name))
		return false;

	while(!GetDatabase()->IsEOF())
	{
		prdb::PRSample Obj;
		if(!GetDatabase()->SampleGet(&Obj))
			break;

		CSampleData Sam(hParentItem, hItem);
		Sam.SetDataFromBase(Obj);

		if(Sam.GetName().Compare(cArtSam))
			NewSamples.push_back(Sam);
		else
		{
			ArtSamples.clear();
			ArtSamples.push_back(Sam);
		}

		GetDatabase()->MoveNext();
	}
	GetDatabase()->Free();

	Samples.clear();
	copy(NewSamples.begin(), NewSamples.end(), inserter(Samples, Samples.begin()));

	for(it=Samples.begin(); it!=Samples.end(); ++it)
	{
		it->LoadDatabase();
	}
	if(int(ArtSamples.size()) > 0)
		ArtSamples[0].LoadDatabase();

	return true;
}
