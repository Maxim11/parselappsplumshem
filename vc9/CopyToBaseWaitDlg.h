#ifndef _COPY_TOBASE_WAIT_DLG_H_
#define _COPY_TOBASE_WAIT_DLG_H_

#pragma once

#include "WaitingDlg.h"
#include "DeviceData.h"


///////////////////////////////////////////////////////////////////////////////////////////////////
// ������ ��������� �������� ����������� �������/������� � ����

class CCopyToBaseWaitDlg : public CWaitingDlg
{
	DECLARE_DYNAMIC(CCopyToBaseWaitDlg)

public:

	CCopyToBaseWaitDlg(CString fname, CDeviceData* dev, CTreeCtrl* tree, CString prjname,
		CWnd* pParent = NULL);																// �����������
	virtual ~CCopyToBaseWaitDlg();															// ����������

	enum { IDD = CWaitingDlg::IDD };	// ������������� �������

protected:

	CString FileName;
	CDeviceData* DevData;
	CString PrjName;
	CTreeCtrl *pTree;

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();						// ������������� �������

	virtual void OK();				// ��������� �������� ����� �� �������
	virtual void Stop();			// ��������� ����������� ����� �� �������
	virtual void Work();			// ������ �������� �������

	DECLARE_MESSAGE_MAP()
};

#endif
