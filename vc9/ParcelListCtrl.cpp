#include "stdafx.h"
#include "ParcelListCtrl.h"


CParcelListCtrl::CParcelListCtrl()
{
	pSortIcons = NULL;

	ResetUserCallbacks();

	CurTxtColor = CLR_DEFAULT;
	CurBGColor = CLR_DEFAULT;
}

CParcelListCtrl::~CParcelListCtrl()
{
	if(pSortIcons != NULL)  delete pSortIcons;
}

BEGIN_MESSAGE_MAP(CParcelListCtrl, CListCtrlWithCustomDraw)
END_MESSAGE_MAP()

bool CParcelListCtrl::IsDraw()
{
	return true;
}

bool CParcelListCtrl::OnDraw(CDC* /*pDC*/, const CRect& /*rc*/)
{
	return false;
}

bool CParcelListCtrl::IsNotifyItemDraw()
{
	return true;
}

bool CParcelListCtrl::IsNotifySubItemDraw(int /*nItem*/, UINT /*nState*/, LPARAM /*lParam*/)
{
	return true;
}

COLORREF CParcelListCtrl::TextColorForSubItem(int nItem, int nSubItem, UINT nState, LPARAM lParam)
{
	COLORREF res = CListCtrlWithCustomDraw::TextColorForSubItem(nItem, nSubItem, nState, lParam);

	UINT nEvent = LCF_CB_SI_TXTCOLOR;
	if(Callbacks.nFlags & (1<<nEvent))
	{
		LC_SI_CALLBACK *pFunc = Callbacks.pFunc[nEvent];
		if(pFunc != NULL)
		{
			void *pCallbackData = 0;
			if(pFunc(nItem, nSubItem, pCallbackData))
				res = CurTxtColor;
		}
	}

	return res;
}

COLORREF CParcelListCtrl::BkColorForSubItem(int nItem, int nSubItem, UINT nState, LPARAM lParam)
{
	COLORREF res = CListCtrlWithCustomDraw::BkColorForSubItem(nItem, nSubItem, nState, lParam);

	UINT nEvent = LCF_CB_SI_BGCOLOR;
	if(Callbacks.nFlags & (1<<nEvent))
	{
		LC_SI_CALLBACK *pFunc = Callbacks.pFunc[nEvent];
		if(pFunc != NULL)
		{
			void *pCallbackData = 0;
			if(pFunc(nItem, nSubItem, pCallbackData))
				res = CurBGColor;
		}
	}

	return res;
}

void CParcelListCtrl::SetIconList()
{
	if(pSortIcons != NULL)  delete pSortIcons;

	pSortIcons = new CImageList();
	ASSERT(pSortIcons != NULL);   
	pSortIcons->Create(16, 16, ILC_MASK, 2, 2);

	HICON hIcon[2];
	hIcon[0] = AfxGetApp()->LoadIcon(IDI_ICON_SORTUP);
	hIcon[1] = AfxGetApp()->LoadIcon(IDI_ICON_SORTDOWN);

	pSortIcons->Add(hIcon[0]);
	pSortIcons->Add(hIcon[1]);

	CHeaderCtrl* pHdrCtrl = GetHeaderCtrl();
	pHdrCtrl->SetImageList(pSortIcons);
}

void CParcelListCtrl::ShowSortIcons(int iCol, bool isUp)
{
	HDITEM curItem;
	CHeaderCtrl* pHdrCtrl = GetHeaderCtrl();

	int nCol = pHdrCtrl->GetItemCount();

	for(int iPos=0; iPos<nCol; iPos++)
	{
		memset(&curItem, 0, sizeof(curItem));
		pHdrCtrl->GetItem(iPos, &curItem);

		curItem.mask = HDI_IMAGE | HDI_FORMAT;
		if(iPos == iCol)
		{
			curItem.fmt = HDF_LEFT | HDF_IMAGE | HDF_STRING | HDF_BITMAP_ON_RIGHT;
			curItem.iImage = (isUp) ? 0 : 1;
		}
		else
			curItem.fmt = HDF_LEFT | HDF_STRING;

		pHdrCtrl->SetItem(iPos, &curItem);
	}
}

void CParcelListCtrl::SetUserCallback(UINT nEvent, LC_SI_CALLBACK *pCallback)
{
	if(nEvent < LCF_CB_SI_NUM)
		Callbacks.pFunc[nEvent] = pCallback;
}

int CParcelListCtrl::EnableUserCallback(UINT nEvent, bool bEnable)
{
	if(nEvent >= LCF_CB_SI_NUM)
		return LCF_SI_ERR;
	if(Callbacks.pFunc[nEvent] == NULL)
		return LCF_SI_ERR;

	if(bEnable)
		Callbacks.nFlags |= 1<<nEvent;
	else
		Callbacks.nFlags &= ~(1<<nEvent);

	return LCF_SI_OK;
}

void CParcelListCtrl::ResetUserCallbacks()
{
	Callbacks.nFlags = 0;
	for(int i=0; i<LCF_CB_SI_NUM; i++)
	{
		Callbacks.pFunc[i] = NULL;
	}
}
