#ifndef _PARCEL_CREATE_TRANSFER_ALONE_V2_H_
#define _PARCEL_CREATE_TRANSFER_ALONE_V2_H_

#pragma once

#include "TransferCalculate.h"

///////////////////////////////////////////////////////////////////////////////////////////////////
class CCreateTransferAloneV2
{

	bool AcceptData();				// ������� ��������� ������

public:

	CCreateTransferAloneV2(CTransferData* trans, CDeviceData* pDev, CProjectData* pPrj, int iTrans);	// �����������
	bool Create();					

protected:

	CProjectData* PrjData;			// ��������� �� ������������ ������ �������
	CTransferCalculate CalcInfo;	// ����� ������� ������ ������������ ��������

	CDeviceData* MDev;				// ��������� �� ������-������
	CProjectData* MPrj;				// ��������� �� ������ ������-�������
	int iTrans;						// ������ ������ � ������� ������-�������

};

#endif
