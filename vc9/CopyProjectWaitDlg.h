#ifndef _COPY_PROJECT_WAIT_DLG_H_
#define _COPY_PROJECT_WAIT_DLG_H_

#pragma once

#include "WaitingDlg.h"
#include "DeviceData.h"


///////////////////////////////////////////////////////////////////////////////////////////////////
// ������ ��������� �������� ���������� �������������� �������

class CCopyProjectWaitDlg : public CWaitingDlg
{
	DECLARE_DYNAMIC(CCopyProjectWaitDlg)

public:

	CCopyProjectWaitDlg(CDeviceData* dev, CWnd* pParent = NULL);	// �����������
	virtual ~CCopyProjectWaitDlg();									// ����������

	enum { IDD = CWaitingDlg::IDD };	// ������������� �������

protected:

	CDeviceData* DevData;	// ��������� �� ������, � ������� ������������ �����������

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();						// ������������� �������

	virtual void OK();				// ��������� �������� ����� �� �������
	virtual void Stop();			// ��������� ����������� ����� �� �������
	virtual void Work();			// ������ �������� �������

	DECLARE_MESSAGE_MAP()
};

#endif
