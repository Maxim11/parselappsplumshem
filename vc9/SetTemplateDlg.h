#ifndef _SET_TEMPLATE_DLG_H_
#define _SET_TEMPLATE_DLG_H_

#pragma once

#include "Resource.h"

///////////////////////////////////////////////////////////////////////////////////////////////////
// ������ ������ ������� ���������

class CSetTemplateDlg : public CDialog
{
	DECLARE_DYNAMIC(CSetTemplateDlg)

public:

	CSetTemplateDlg(int mode, CWnd* pParent = NULL);	// �����������
	virtual ~CSetTemplateDlg();							// ����������

	enum { IDD = IDD_SET_TEMPLATE };					// ������������� �������

	CEdit m_Name;			// ���� ��� ����� "����� �������"

protected:

	int Mode;				// ����� ������ �������

	CString m_vName;		// ��������� �������� "����� �������"
	CString Lang;

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();						// ������������� �������

	virtual void OnOK()
	{};
	virtual void OnCancel();		// ��������� ������� "�������� ���� ������ � ����� �� �������"

	afx_msg void OnAccept();		// ��������� ������� "������� ������ � ����� �� �������"
	afx_msg void OnHelp();			// ��������� ������� "������� �������"
	afx_msg void OnEdit();
	afx_msg void OnCreate();
	afx_msg void OnOpen();

	CString MakeEditorCommandLine(LPCTSTR tmplPath, LPCTSTR palettePath, LPCTSTR paletteID, LPCTSTR tmplDir);
	void RunEditor(CString& cmdline);

	DECLARE_MESSAGE_MAP()
};

#endif
