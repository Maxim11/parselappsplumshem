#include "stdafx.h"

#include "BranchInfo.h"
#include "ImitDeviceDlg.h"
#include "DeleteDeviceWaitDlg.h"
#include "LoadDBWaitDlg.h"
#include "FileSystem.h"

using namespace filesystem;

CRootData *pRoot;

CRootData::CRootData()
{
	hItem = NULL;

	TCHAR buf[PATH_LENGTH];
	GetCurrentDirectory(PATH_LENGTH, buf);
	CString s(buf);

	WorkFolder = s;

	BaseDefName = _T("defname");
	BaseFileDefName = MakePath(WorkFolder, _T("\\Base\\defname.prd"));
	SetBaseFileName(BaseFileDefName);

	SetTemplateName(_T("Template.wtl"), C_TEMPLATE_QNT);
	SetTemplateName(_T("Template.wtl"), C_TEMPLATE_QLT);

	prdb::PRUser User;
	User.FirstName = _T("Nickolay");
	User.UserNIC = _T("Nickolay");
	User.Role.Type = 0;
	User.Role.Description = _T("Admin");

	Database.SetOptions(User, prdbHistoryActivity::PRDB_HISTORY_ACTIVITY_ONE_PER_OPEN, false, false);
}

CRootData::~CRootData()
{
	Devices.clear();
}

HTREEITEM CRootData::GetHItem()
{
	return hItem;
}

void CRootData::SetHItem(HTREEITEM item)
{
	hItem = item;
}

CString CRootData::GetBaseFileName()
{
	return BaseFileName;
}

CString CRootData::GetBaseName()
{
	return BaseName;
}

CString CRootData::GetBaseDefFileName()
{
	return BaseFileDefName;
}

void CRootData::SetBaseFileName(CString name)
{
	BaseFileName = name;
	BaseName = ExtractFileNameWithoutExt(name);
	BaseFolder = ExtractDirPath(name);
}

void CRootData::SetBaseName(CString name)
{
	BaseName = name;
}

CString CRootData::GetBaseDefName()
{
	return BaseDefName;
}

CString CRootData::GetWorkFolder()
{
	return WorkFolder;
}

CString CRootData::GetBaseFolder()
{
	return BaseFolder;
}

CString CRootData::GetPalettePath()
{
	return MakePath(WorkFolder, _T("\\Palette"));
}

CString CRootData::GetPaletteName()
{
	return _T("Calibration.ptl");
}

CString CRootData::GetPaletteFullName()
{
	return MakePath(GetPalettePath(), GetPaletteName());
}

CString CRootData::GetTemplateFolder(int tmpl)
{
	switch(tmpl)
	{
	case C_TEMPLATE_QNT:  return MakePath(WorkFolder, _T("\\Templates\\Qnt"));
	case C_TEMPLATE_QLT:  return MakePath(WorkFolder, _T("\\Templates\\Qlt"));
	}

	return cEmpty;
}

CString CRootData::GetTemplateName(int tmpl)
{
	CString s;
	switch(tmpl)
	{
	case C_TEMPLATE_QNT:  s = TemplateQntName;  break;
	case C_TEMPLATE_QLT:  s = TemplateQltName;  break;
	}
	if(s.IsEmpty())  s = _T("Template.wtl");

	return s;
}

void CRootData::SetTemplateName(CString name, int tmpl)
{
	switch(tmpl)
	{
	case C_TEMPLATE_QNT:  TemplateQntName = name;  break;
	case C_TEMPLATE_QLT:  TemplateQltName = name;  break;
	}
}

CString CRootData::GetReportPath(int rep)
{
	CString s = cEmpty;
	switch(rep)
	{
	case C_TEMPLATE_QNT:  s = MakePath(WorkFolder, _T("\\Reports\\report_qnt.html"));  break;
	case C_TEMPLATE_QLT:  s = MakePath(WorkFolder, _T("\\Reports\\report_qlt.html"));  break;
	}

	return s;
}

CString CRootData::GetReportPictureFolder(int rep)
{
	CString s = cEmpty;
	switch(rep)
	{
	case C_TEMPLATE_QNT:  s = MakePath(WorkFolder, _T("\\Reports\\report_qnt"));  break;
	case C_TEMPLATE_QLT:  s = MakePath(WorkFolder, _T("\\Reports\\report_qlt"));  break;
	}

	return s;
}

void CRootData::SetBaseFolder(CString name)
{
	BaseFolder = name;
}

int CRootData::GetNDevices()
{
	return int(Devices.size());
}

CDeviceData* CRootData::GetDevice(HTREEITEM item)
{
	ItDev it;
	for(it=Devices.begin(); it!=Devices.end(); ++it)
		if((*it).GetHItem() == item)
			return &(*it);

	return NULL;
}

CDeviceData* CRootData::GetDevice(CString name)
{
	ItDev it;
	for(it=Devices.begin(); it!=Devices.end(); ++it)
		if(!(*it).GetName().CompareNoCase(name))
			return &(*it);

	return NULL;
}

CDeviceData* CRootData::GetDeviceByNum(int num)
{
	ItDev it;
	for(it=Devices.begin(); it!=Devices.end(); ++it)
		if((*it).GetNumber() == num)
			return &(*it);

	return NULL;
}

CDeviceData* CRootData::GetDevice(int ind)
{
	if(ind < 0 || ind >= int(Devices.size()))
		return NULL;

	int i;
	ItDev it;
	for(i=0, it=Devices.begin(); i<ind; i++)
		++it;

	return &(*it);
}

CDeviceData* CRootData::GetLastDevice()
{
	return GetDevice(GetNDevices() - 1);
}

HTREEITEM CRootData::GetLastDeviceHItem()
{
	CDeviceData* DevData = GetLastDevice();
	if(DevData)
		return DevData->GetHItem();

	return NULL;
}

CDeviceData* CRootData::AddNewDevice()
{
	CDeviceData NewDev;

	CImitDeviceDlg dlg(&NewDev, IDEVDLG_MODE_CREATE);
	if(dlg.DoModal() != IDOK)
		return NULL;

	prdb::PRAppliance Obj;
	NewDev.GetDataForBase(Obj);
	if(!Database.ApplianceAddEx(0, &Obj))
		return NULL;

	Devices.push_back(NewDev);

	return GetLastDevice();
}

bool CRootData::AddExistingDevice(CString FileName, CDeviceData* CopyDev, CTreeCtrl* pTree, CString PrjName)
{
	CString OldFileName = GetBaseFileName();

	Database.Close();
	if(PathFileExists(FileName))
	{
		if(!Database.Open(FileName))
		{
			ParcelError(ParcelLoadString(STRID_STDERR_DBNOOPEN));
			Database.Open(OldFileName);
			LoadDatabase(pTree);
			return false;
		}
		if(!Database.CheckDatabase())
		{
			ParcelError(ParcelLoadString(STRID_STDERR_DBINCORRVERS));
			Database.Close();
			Database.Open(OldFileName);
			LoadDatabase(pTree);
			return false;
		}
	}
	else
	{
		if(!Database.CreateDatabase(FileName))
		{
			ParcelError(ParcelLoadString(STRID_STDERR_DBNOCREATE));
			return false;
		}
		CreateSpectraDataDirectory(FileName);
	}

	SetBaseFileName(FileName);
	
	if(!LoadDatabase(pTree))
	{
		SetBaseFileName(OldFileName);
		ParcelError(ParcelLoadString(STRID_STDERR_DBNOLOAD));
		Database.Close();
		Database.Open(OldFileName);
		LoadDatabase(pTree);
		return false;
	}

	ItPrj itP;
	CString sErr = cEmpty, sOK = cEmpty;
	CString sN = cEmpty, sY = cEmpty;

	CDeviceData* pOldDev = GetDeviceByNum(CopyDev->GetNumber());
	for(itP=CopyDev->Projects.begin(); itP!=CopyDev->Projects.end(); ++itP)
	{
		if(pOldDev != NULL && pOldDev->GetProject(itP->GetName()) != NULL)
		{
			if(!sN.IsEmpty())
				sN += _T(", ");
			sN += itP->GetName();
		}
		else
		{
			if(!sY.IsEmpty())
				sY += _T(", ");
			sY += itP->GetName();
		}
	}
	if(!sN.IsEmpty())
		sErr.Format(ParcelLoadString(STRID_STDERR_COPYTOBASE), sN, pOldDev->GetName(), BaseName);
	if(!sY.IsEmpty())
		sOK.Format(ParcelLoadString(STRID_STDMSG_COPYTOBASE), sY);

	CDeviceData NewDev;
	NewDev.Copy(*CopyDev);

	pCopiedDev = NULL;
	if(pOldDev == NULL)
	{
		prdb::PRAppliance Obj;
		NewDev.GetDataForBase(Obj);
		if(!GetDatabase()->ApplianceAddEx(0, &Obj))
			return false;

		HTREEITEM hLastItem = GetLastDeviceHItem();

		HTREEITEM hDevItem = pTree->InsertItem(TVIF_TEXT, NewDev.GetName(),
			0, 0, 0, 0, 0, hItem, hLastItem);
		NewDev.SetHItem(hDevItem);
		pTree->SetItemImage(hDevItem, 9, 9);
		pTree->Select(hDevItem, TVGN_CARET);
		pTree->Expand(hItem, TVE_EXPAND);
		pTree->SetItemImage(hItem, 1, 1);

		Devices.push_back(NewDev);
		pCopiedDev = GetLastDevice();
	}
	else
		pCopiedDev = pOldDev;

	pCopiedDev->pTree = pTree;

	for(itP=CopyDev->Projects.begin(); itP!=CopyDev->Projects.end(); ++itP)
	{
		if(pCopiedDev->GetProject(itP->GetName()) != NULL)
			continue;

		CProjectData Prj(pCopiedDev->GetHItem());
		Prj.Copy(*itP);
		Prj.pCopies = &(*itP);
		Prj.CopyMode = C_PRJ_COPY_WITHALL;

		pCopiedDev->pNewCopyPrj = &Prj;

		if(!pCopiedDev->AcceptCopyPrj())
			continue;
	}

	pCopiedDev->FreeAllSpectraData();

	CString sAll;
	if(!sOK.IsEmpty())
	{
		sAll = sOK;
		if(!sErr.IsEmpty())
			sAll += _T("\n") + sErr;
	}
	else
		sAll = sErr;

	ParcelInformation(sAll);

	return true;
}

bool CRootData::ChangeDeviceSettings(HTREEITEM item)
{
	CDeviceData* OldDev = GetDevice(item);
	if(OldDev == NULL)
		return false;

	CDeviceData NewDev;
	NewDev.Copy(*OldDev);

	int mode = (OldDev->GetNProjects() > 0) ? IDEVDLG_MODE_CHANGENUM : IDEVDLG_MODE_CHANGE;
	CImitDeviceDlg dlg(&NewDev, mode);
	if(dlg.DoModal() != IDOK)
		return false;

	prdb::PRAppliance Obj;
	NewDev.GetDataForBase(Obj);
	if(!Database.ApplianceAddEx(OldDev->GetNumber(), &Obj))
		return false;

	OldDev->Copy(NewDev);

	return true;
}

bool CRootData::DeleteDevice(HTREEITEM item)
{
	ItDev it;
	for(it=Devices.begin(); it!=Devices.end(); ++it)
		if((*it).GetHItem() == item)
		{
			if(!DeleteDevice(it->GetNumber()))
				return false;

			Devices.erase(it);
			return true;
		}

	return false;
}

bool CRootData::DeleteDevice(int number)
{
	CDeleteDeviceWaitDlg dlg(number);
	if(dlg.DoModal() != IDOK)
		return false;

	return true;
}

int CRootData::GetNProjects()
{
	int N = 0;
	ItDev it;
	for(it=Devices.begin(); it!=Devices.end(); ++it)
		N += it->GetNProjects();

	return N;
}

bool CRootData::LoadDatabase(CTreeCtrl* pTree)
{
	ItDev it;
	for(it=Devices.begin(); it!=Devices.end(); ++it)
		pTree->DeleteItem(it->GetHItem());

	LDev NewDevices;

	prdb::PRAppliance Obj;

	if(!Database.ApplianceRead())
		return false;

	HTREEITEM hLastItem = NULL;
	while(!Database.IsEOF())
	{
		if(!Database.ApplianceGet(&Obj))
			break;

		CDeviceData Dev;

		Dev.SetDataFromBase(Obj);

		HTREEITEM hNewItem = pTree->InsertItem(TVIF_TEXT, Dev.GetName(), 0, 0, 0, 0, 0, hItem, hLastItem);
		Dev.SetHItem(hNewItem);
		pTree->SetItemImage(hNewItem, 9, 9);
		hLastItem = hNewItem;

		NewDevices.push_back(Dev);

		Database.MoveNext();
	}
	Database.Free();

	Devices.clear();
	copy(NewDevices.begin(), NewDevices.end(), inserter(Devices, Devices.begin()));

	for(it=Devices.begin(); it!=Devices.end(); ++it)
		it->LoadDatabase(pTree);

	if(Devices.size() > 0)
	{
		pTree->SetItemImage(hItem, 1, 1);
		pTree->Expand(hItem, TVE_EXPAND);
	}
	else
	{
		pTree->SetItemImage(hItem, 8, 8);
		pTree->Expand(hItem, TVE_COLLAPSE);
	}

	pTree->SetItemText(pTree->GetRootItem(), GetBaseName());
	pTree->Select(hItem, TVGN_CARET);

	return true;
}

bool CRootData::CloseDatabase(CTreeCtrl* pTree)
{
	Database.Close();

	ItDev it;
	for(it=Devices.begin(); it!=Devices.end(); ++it)
		pTree->DeleteItem(it->GetHItem());

	Devices.clear();

	pTree->SetItemImage(hItem, 8, 8);
	pTree->Expand(hItem, TVE_COLLAPSE);

	pTree->SetItemText(pTree->GetRootItem(), cEmpty);
	pTree->Select(hItem, TVGN_CARET);

	return true;
}

bool CRootData::DeleteDatabase(CTreeCtrl* pTree)
{
	if(!CloseDatabase(pTree))
		return false;

	DeleteBase(BaseFileName);

	return true;
}

void CRootData::DeleteBase(CString fname)
{
	if(PathFileExists(fname))
	{
		DeleteFile(fname);

		CString Path = ExtractDirPath(fname);
		CString Base = ExtractFileNameWithoutExt(fname);
		CString Dir = _T("PRD_") + Base;
		CString Full = MakePath(Path, Dir);

		DeleteDirectoryWithAllContent(Full);
	}
}

bool CRootData::CopyBase(CString fname)
{
	DeleteBase(fname);

	if(!CreateSpectraDataDirectory(fname))
		return false;

	if(!CopyFile(BaseFileName, fname, false))
		return false;

	CString PathOld = ExtractDirPath(BaseFileName);
	CString PathNew = ExtractDirPath(fname);
	CString BaseNew = ExtractFileNameWithoutExt(fname);
	CString DirOld = _T("PRD_") + BaseName;
	CString DirNew = _T("PRD_") + BaseNew;
	CString Name = _T("*.dat");
	CString filemask = MakePath(PathOld, DirOld, Name);

	WIN32_FIND_DATA FindData;
	HANDLE hFindFile = FindFirstFile(filemask, &FindData);
	if(hFindFile == INVALID_HANDLE_VALUE)
		return true;
	do
	{
		CString File = CString(FindData.cFileName);

		CString OldPlace = MakePath(PathOld, DirOld, File);
		CString NewPlace = MakePath(PathNew, DirNew, File);

		if(!CopyFile(OldPlace, NewPlace, false))
			return false;
	}
	while(FindNextFile(hFindFile, &FindData));

	return true;
}

bool CRootData::CreateSpectraDataDirectory(CString fname)
{
	CString Path = ExtractDirPath(fname);
	CString Base = ExtractFileNameWithoutExt(fname);
	CString Dir = _T("PRD_") + Base;
	CString Full = MakePath(Path, Dir);

	if(!EnsureDirectory(Full))
		return false;

	return true;
}

void CRootData::DeleteBaseApjx(CString fname)
{
	if(PathFileExists(fname))
	{
		DeleteFile(fname);

		CString Path = ExtractDirPath(fname);
		CString Base = ExtractFileNameWithoutExt(fname);
		CString Dir = _T("APJX_") + Base;
		CString Full = MakePath(Path, Dir);

		DeleteDirectoryWithAllContent(Full);
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// ���������� �������


int GetBranchInfo(HTREEITEM item, BranchInfo &Info)
{
	Info.Clear();
	if(pRoot->GetHItem() == item)
		return TIM_ROOT;

	ItDev itd;
	for(itd=pRoot->Devices.begin(); itd!=pRoot->Devices.end(); ++itd)
	{
		CDeviceData *Dev = &(*itd);
		if(Dev->GetHItem() == item)
		{
			Info.Device = Dev;
			Info.hDevice = Dev->GetHItem();
			return TIM_DEVICE;
		}

		ItPrj itp;
		for(itp=Dev->Projects.begin(); itp!=Dev->Projects.end(); ++itp)
		{
			CProjectData *Prj = &(*itp);
			int find = TIM_NONE;
			if(Prj->GetHItem() == item)
				find = TIM_PROJECT;
			if(Prj->GetParamSetHItem() == item)
				find = TIM_PARAMSET;
			if(Prj->GetSampleSetHItem() == item)
				find = TIM_SAMPLESET;

			ItTrans itsp;
			for(itsp=Prj->Transfers.begin(); itsp!=Prj->Transfers.end(); ++itsp)
			{
				CTransferData *Trans = &(*itsp);
				if(itsp->GetHItem() == item)
				{
					Info.Trans = Trans;
					Info.hSampleTrans = itsp->GetHItem();
					find = (Prj->GetTransOwn()->GetHItem() == item) ? TIM_SAMPLEOWN : TIM_SAMPLETRANS;
					break;
				}
			}

			if(Prj->GetModelSetHItem() == item)
				find = TIM_MODELSET;
			if(Prj->GetMethodSetHItem() == item)
				find = TIM_METHODSET;

			if(find != TIM_NONE)
			{
				Info.Device = Dev;
				Info.Project = Prj;
				Info.hDevice = Dev->GetHItem();
				Info.hProject = Prj->GetHItem();
				Info.hParamSet = Prj->GetParamSetHItem();
				Info.hSampleSet = Prj->GetSampleSetHItem();
				Info.hModelSet = Prj->GetModelSetHItem();
				Info.hMethodSet = Prj->GetMethodSetHItem();
				return find;
			}

			ItMod itmod;
			for(itmod=Prj->Models.begin(); itmod!=Prj->Models.end(); ++itmod)
			{
				CModelData *Mod = &(*itmod);
				if(Mod->GetHItem() == item)
				{
					Info.Device = Dev;
					Info.Project = Prj;
					Info.Model = Mod;
					Info.hDevice = Dev->GetHItem();
					Info.hProject = Prj->GetHItem();
					Info.hParamSet = Prj->GetParamSetHItem();
					Info.hSampleSet = Prj->GetSampleSetHItem();
					Info.hModelSet = Prj->GetModelSetHItem();
					Info.hMethodSet = Prj->GetMethodSetHItem();
					Info.hModel = Mod->GetHItem();
					return TIM_MODEL;
				}
			}

			ItMtd itmtd;
			for(itmtd=Prj->Methods.begin(); itmtd!=Prj->Methods.end(); ++itmtd)
			{
				CMethodData *Mtd = &(*itmtd);
				if(Mtd->GetHItem() == item)
				{
					Info.Device = Dev;
					Info.Project = Prj;
					Info.Method = Mtd;
					Info.hDevice = Dev->GetHItem();
					Info.hProject = Prj->GetHItem();
					Info.hParamSet = Prj->GetParamSetHItem();
					Info.hSampleSet = Prj->GetSampleSetHItem();
					Info.hModelSet = Prj->GetModelSetHItem();
					Info.hMethodSet = Prj->GetMethodSetHItem();
					Info.hMethod = Mtd->GetHItem();
					return TIM_METHOD;
				}
			}
		}
	}

	return TIM_NONE;
}

CDeviceData* GetDeviceByHItem(HTREEITEM item)
{
	BranchInfo Info;
	GetBranchInfo(item, Info);

	return Info.Device;
}

CProjectData* GetProjectByHItem(HTREEITEM item)
{
	BranchInfo Info;
	GetBranchInfo(item, Info);

	return Info.Project;
}

CTransferData* GetTransferByHItem(HTREEITEM item)
{
	BranchInfo Info;
	GetBranchInfo(item, Info);

	return Info.Trans;
}

COptimTemplate* GetOptimTemplates()
{
	return &pRoot->Templates;
}

CPRDBProject* GetDatabase()
{
	return &pRoot->Database;
}
