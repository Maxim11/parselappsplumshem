/*
*  Copyright (C)
*
*  Project: LumexDB
*
*  File: ILumexDB.h
*  Creation: 04.07.2007, Conductor, JarilloLabs Ltd., Co.
*
*  Description: ���� ���������� ����������� � ������������ ���� ��� ������ � �������� ��� ODBC API.
*
*  Contents: LumexDB - ������������ ����, ���������� � ���� ������������ � smart-pointer ��� ������ � �������� ��� ODBC API;
*    		 ILumexDBDataSet - ��������� ��� ������ � �������������� ���������� �����;
*			 ILumexDB - ��������� ��� ������ � ����� ������;
*			 ILumexDBManager - ��������� ��������� ��� ������.
*/


#ifndef _ILUMEXDB_2007_07_04
#define _ILUMEXDB_2007_07_04

#pragma once

/// ������������ ����, ���������� � ���� ������������ � smart-pointer ��� ������ � �������� ��� ODBC API
namespace LumexDB
{
	/// ������������ ����������� ���������� ��������
	enum EnOperationResult
	{
		/// �������
		enSuccess = 0,
		/// ������ �� ���������������
		enNotInitialized,
		/// ��� ���������� � �����
		enNotConnected,
		/// ���������� ��� ������
		enTransactionIsAlreadyBegun,
		/// ���������� ��� �� ������
		enTransactionIsNotBegun,
		/// ������ ODBC
		enODBCError,
		/// ���������� ���������
		enNotValidParams,
		/// ���������� ���������� ��������
		enStillExecuting,
		/// ������ �����������
		enNoData,
		/// ����� ����
		enFail
	};

	/// ��������� ������������
	enum 
	{
		/// ����� ������ � ������ ������������� ����
		enBufferLength = 4096,
		/// ������������ ����� ������/������ � ������ ������������� ����
		enMaxUIDAndPWDLength = 256
	};

	/// ������������ ��������� ��� ������
	enum EnDataBaseDrivers
	{
		/// ��� ���������� ����
		enFirstDriver = 0,

		/// ������� MS Access
		enMSAccessDriver = 0,

		/// ��� ���������� ����
		enLastDriver
	};

	/// ��������� ������������
	enum
	{
		/// ������������ ����� ����� ������� � ������ ������������� ����
		enMaxColumnNameLength = 256
	};	

	/// ��������� ����� smart-pointer'�
	template<class T>
	struct CDataPtr
	{
		/// ��������� �� ����������� ������
		T *m_pData;

		/// ����������� �� ���������
		CDataPtr() : m_pData(NULL) {};
		/// �����������, ����������� � �������� ��������� ��������� �� ����������� ������
		CDataPtr(T* pData) : m_pData(pData) {};
		/// ����������� �����������
		CDataPtr(const CDataPtr& DP)
		{
			if (DP.m_pData == NULL)
			{
				m_pData = NULL;
				return;
			}
			m_pData = T::Creator();
			*m_pData = *DP.m_pData;
		}
		/// �������� �����
		const CDataPtr& operator = (const CDataPtr& DP)
		{
			if (&DP == this)
			{
				return *this;
			}

			if (m_pData != NULL)
			{
				m_pData->Release();
				m_pData = NULL;
			}

			if (DP.m_pData == NULL)
			{
				m_pData = NULL;				
			}
			else
			{	
				m_pData = T::Creator();
				*m_pData = *DP.m_pData;
			}

			return *this;
		}
		/// ����������
		~CDataPtr()
		{
			if (m_pData)
			{
				m_pData->Release();
				m_pData = NULL;
			}
		}

		/// �������� ->
		T* operator -> ()
		{
			return m_pData;
		}

		/// �������� ->
		const T* operator -> () const
		{
			return m_pData;
		}

		/// �������� *
		operator T* () const
		{
			return m_pData;
		}		
	};
}

/// ����� ���������� ��� ������ � �������������� ���������� �����
struct ILumexDBDataSet
{
	/// ������������ �������������� ����� ������
	enum EnDataTypes
	{
		/// ��� ���������� ����
		enFirstDataTypes = 0,

		/// ������
		enBool = 0,
		/// �����
		enInt,
		/// ������� ������� ��������
		enDouble,
		/// ������
		enString,
		/// ����-�����
		enDataTime,
		/// ������� ������
		enNullData,
		/// ���������� ������
		enNoTotal,

		/// ��� ���������� ����
		enLastDataTypes
	};

	/// ��������� ��� ��������� ���������� �������
	struct LUMEXDB_API TParams
	{
		/// ��� �������
		LPTSTR m_lpColName;
		/// ��� ������
		EnDataTypes m_DataType;
		/// ������������ ������ ������ ������� � ������
		unsigned long m_udwColSize;		
		
		/// �����������
		TParams();
		/// ����������
		~TParams();
		/// �����������, ����������� � �������� ��������� ��� �������
		TParams(LPCTSTR lpColName);
		/// ����������� �����������
		TParams(const TParams &P);
		/// �������� �����
		const TParams& operator = (const TParams& P);

		/// ����������� ����� ������������ ��� �������� ������ ������� ���������.
		/// ������ ����������:
		///      �����������.
		/// ������������ ��������:
		///      ��������� �� ��������� ������
		static TParams* Creator();

		/// ����� ������������ ��� ���������� �������.
		/// ������ ����������:
		///      �����������.
		/// ������������ ��������:
		///      �� ������������
		void Release();
	};

	/// ��������� ��� ��������� ������ �������
	struct LUMEXDB_API TColumnData
	{
		/// ��� ������
		EnDataTypes m_Type;
		/// ������ ������
		size_t m_Size;
		/// ��������� �� ������
		LPSTR m_pData;

		/// �����������
		TColumnData();
		/// ����������
		~TColumnData();
		/// ����������� �����������
		TColumnData(const TColumnData& CD);		
		/// �������� �����
		const TColumnData& operator = (const TColumnData& CD);

		/// ����������� ����� ������������ ��� �������� ������ ������� ���������.
		/// ������ ����������:
		///      �����������.
		/// ������������ ��������:
		///      ��������� �� ��������� ������
		static TColumnData* Creator();

		/// ����� ������������ ��� ���������� �������.
		/// ������ ����������:
		///      �����������.
		/// ������������ ��������:
		///      �� ������������
		void Release();

		/// ����� ������������ ��� ��������� ������ ��� ������
		/// ������ ����������:
		///      [in] dwSize - ��������� ����� ������
		/// ������������ ��������:
		///      true - � ������ ������
		bool AllocateMemoryForData(DWORD dwSize);		

		/// ��������� ����� ������������ ��� ��������� ������
		/// ������ ����������:
		///      [in/out] val - ��������� �� ������������ ������
		///      [in] ValSize - ������������ ������ ������������ ������
		///      [in] bSimpleData - ������� ������������� ����� ������ (true - ��� ���� ������, ����� �����)
		/// ������������ ��������:
		///      true - � ������ ������
		template <class T>
		LumexDB::EnOperationResult GetData(T* val, size_t ValSize, bool bSimpleData = true)
		{
			// ���������� � ������ ��������� ������ ������������� ������� ���������� �� �������������. ������, ��-�� ����, ��� � ����������
			// ���� ���������� �� ��, ��� � ������ ���������� ������ ������� ��������� ����� �������������������, ������������� ���� �������.
			// ����������� ������������� ��������� �������� � ����� ���������� �������� ���������� ������ � ������� ��������.

			if (bSimpleData)
			{
				if (((m_Type == enNullData || m_Type == enNoTotal) ? false : (sizeof(T) != m_Size || ValSize != m_Size)) || val == NULL)
				{
					return LumexDB::enNotValidParams;
				}

				if (m_Type == enNullData || m_Type == enNoTotal)
				{
					::memset(val, 0, ValSize);
					return LumexDB::enNoData;
				}

				::memcpy(val, m_pData, m_Size);
				return LumexDB::enSuccess;
			}
			else
			{
				size_t nNeededLen = m_Size + sizeof(TCHAR);
				if (((m_Type == enNullData || m_Type == enNoTotal) ? false : (ValSize < nNeededLen)) || val == NULL)
				{				
					return LumexDB::enNotValidParams;
				}				

				if (m_Type == enNullData || m_Type == enNoTotal)
				{
					::memset(val, 0, ValSize);
					return LumexDB::enNoData;
				}

				::memset(val, 0, nNeededLen);
				::memcpy(val, m_pData, m_Size);
				return LumexDB::enSuccess;
			}
		}
	};

	/// ����� ������������ ��� ���������� �������.
	/// ������ ����������:
	///      �����������.
	/// ������������ ��������:
	///      �� ������������
	virtual void Release() = 0;

	/// ����� ������������ ��� ��������� ���������� ��������.
	/// ������ ����������:
	///      �����������.
	/// ������������ ��������:
	///      ���������� ��������
	virtual UINT GetColumnsCount() const = 0;

	/// ����� ������������ ��� ��������� ���������� �������.
	/// ������ ����������:
	///      [in] nColInd - ������ �������
	///      [out] Params - ��������� �������
	/// ������������ ��������:
	///      ��������� ��������.
	virtual LumexDB::EnOperationResult GetColumnParams(const int nColInd, TParams &Params) const = 0;

	/// ����� ���������� ��������� ��������������� ��������� �� ������ ������
	/// ������ ����������:
	///      �����������
	/// ������������ ��������:
	///      ��������� �������� (LumexDB::enNoData - ���� � ��������� ��� �����).
	virtual LumexDB::EnOperationResult FirstRow() = 0;

	/// ����� ���������� ��������� ��������������� ��������� �� ��������� ������
	/// ������ ����������:
	///      �����������
	/// ������������ ��������:
	///      ��������� �������� (LumexDB::enNoData - ���� � ��������� ��� �����).
	virtual LumexDB::EnOperationResult LastRow() = 0;

	/// ����� ���������� ��������� ��������������� ��������� �� ��������� ������
	/// ������ ����������:
	///      �����������
	/// ������������ ��������:
	///      ��������� �������� (LumexDB::enNoData - ���� � ��������� ��� ����� ��� ������� ������ - ���������).
	virtual LumexDB::EnOperationResult NextRow() = 0;

	/// ����� ���������� ��������� ��������������� ��������� �� ���������� ������
	/// ������ ����������:
	///      �����������
	/// ������������ ��������:
	///      ��������� �������� (LumexDB::enNoData - ���� � ��������� ��� ����� ��� ������� ������ - ������).
	virtual LumexDB::EnOperationResult PrevRow() = 0;

	/// ����� ���������� ��������� ��������������� ��������� �� �������� ���������� ����� ������������ �������
	/// ������ ����������:
	///      [in] nRowCount - ������������� �������� (��������)
	/// ������������ ��������:
	///      ��������� �������� (LumexDB::enNoData - ���� � ��������� ��� ����� ��� ������������� �������� ��������� �� �������������� ������).
	virtual LumexDB::EnOperationResult MoveRelative(int nRowCount) = 0;

	/// ����� ������������ ��� �������� ����� ������.
	/// ������ ����������:
	///      �����������
	/// ������������ ��������:
	///      true - ���� ��������� ����� ������.
	virtual bool IsEOF() const = 0;

	/// ����� ������������ ��� ��������� ������ �������.
	/// ������ ����������:
	///      [in] nColInd - ������ �������	
	/// ������������ ��������:
	///      ������ �������. NULL - � ������ ������.
	virtual TColumnData* GetColumn(const int nColInd) const = 0;	
};

/// ����� ���������� ��� ������ � ����� ������
struct ILumexDB
{
	/// ����� ������������ ��� ���������� �������.
	/// ������ ����������:
	///      �����������.
	/// ������������ ��������:
	///      �� ������������
	virtual void Release() = 0;

	/// ����� ������������ ��� ������ ����������.
	/// ������ ����������:
	///      �����������
	/// ������������ ��������:
	///      ��������� ��������.
	virtual LumexDB::EnOperationResult BeginTransaction() = 0;

	/// ����� ������������ ��� ���������� ����������.
	/// ������ ����������:
	///      �����������
	/// ������������ ��������:
	///      ��������� ��������.
	virtual LumexDB::EnOperationResult EndTransaction(bool bCommit) = 0;

	/// ����� ������������ ��� ��������� ���� ������ ��������� ��������.
	/// ������ ����������:
	///      �����������
	/// ������������ ��������:
	///      ��� ������.
	virtual short GetLastSQLErrorCode() const = 0;

	/// ����� ������������ ��� ���������� SQL-�������.
	/// ������ ����������:
	///      [in] lpSQL - ����� SQL-�������
	///      [in/out] pDataSet - �������������� ��������� �����, ���� ��� �� �������� - NULL
	/// ������������ ��������:
	///      ��������� ��������.
	virtual LumexDB::EnOperationResult ExecuteSQL(LPCTSTR lpSQL, ILumexDBDataSet** pDataSet = NULL) = 0;

	/// ����� ������������ ��� ������� ������ ���� ������.
	/// ������ ����������:
	///      [in] lpcOldPassword - ������ ������ (NULL - ���� ������ ������������)
	///      [in] lpcNewPassword - ����� ������
	///      [in] Driver - ������� ���� ������
	/// ������������ ��������:
	///      ��������� ��������.
	virtual LumexDB::EnOperationResult SetDBPassword(LPCTSTR lpcOldPassword, LPCTSTR lpcNewPassword, LumexDB::EnDataBaseDrivers Driver = LumexDB::enMSAccessDriver) = 0;

	/// ����� ������������ ��� ��������� �������� ������� ���������� � ����� ������.
	/// ������ ����������:
	///      �����������
	/// ������������ ��������:
	///      ������� ������� ���������� � ����� ������.
	virtual bool IsConnected() const = 0;
};

/// ����� ���������� ��� ������ � ���������� ��� ������
struct ILumexDBManager
{
	/// ����� ������������ ��� �������� ���� ������.
	/// ������ ����������:
	///		[in] lpFileName - ���� � ����
	///		[in] bCreateAlways - ���� true - ��������� ������, ���� ���� ���������� - ������������ ���; ���� false - ������� ������ � ��� ������, ���� ���� �� ����������
	///      [in] Driver - ������� ���� ������
	///      [in] lpUserName - ��� ������������
	///      [in] lpPassword - ������
	/// ������������ ��������:
	///      ��������� �� ��������� ILumexDB, NULL ���� ������� �� �������.
	virtual ILumexDB* CreateDB(LPCTSTR lpFileName, bool bCreateAlways = true, LumexDB::EnDataBaseDrivers Driver = LumexDB::enMSAccessDriver, LPCTSTR lpUserName = NULL, LPCTSTR lpPassword = NULL) = 0;

	/// ����� ������������ ��� �������� ���� ������.
	/// ������ ����������:
	///		[in] lpFileName - ���� � ����
	///      [in] Driver - ������� ���� ������
	///      [in] lpUserName - ��� ������������
	///      [in] lpPassword - ������
	/// ������������ ��������:
	///      ��������� �� ��������� ILumexDB, NULL ���� ������� �� �������.
	virtual ILumexDB* OpenDB(LPCTSTR lpFileName, LumexDB::EnDataBaseDrivers Driver = LumexDB::enMSAccessDriver, LPCTSTR lpUserName = NULL, LPCTSTR lpPassword = NULL) = 0;

	/// ����� ������������ ��� ������ ���� ������.
	/// ������ ����������:
	///		[in] lpFileName - ���� � ����
	///      [in] Driver - ������� ���� ������
	///      [in] lpUserName - ��� ������������
	///      [in] lpPassword - ������
	/// ������������ ��������:
	///      ��������� ��������.
	virtual LumexDB::EnOperationResult CompactDB(LPCTSTR lpFileName, LumexDB::EnDataBaseDrivers Driver = LumexDB::enMSAccessDriver, LPCTSTR lpUserName = NULL, LPCTSTR lpPassword = NULL) = 0;	
};

#endif