// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once


#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>

#define _CRT_SECURE_NO_DEPRECATE
#define _CRT_NON_CONFORMING_SWPRINTFS

// TODO: reference additional headers your program requires here
#include <assert.h>
#include <tchar.h>
#include <odbcinst.h>
#include <sqlext.h>
#include <sqltypes.h>
#include <mmsystem.h>
#include "../lltimer.h"
