/*
*  Copyright (C)
*
*  Project: LumexDB
*
*  File: ILumexDB.cpp
*  Creation: 04.07.2007, Conductor, JarilloLabs Ltd., Co.
*
*  Description: ���� ���������� ����������� � ������� ��� ������ � �������� ��� ODBC API.
*
*  Contents: ILumexDBDataSetImpl - ���������� ���������� ��� ������ � �������������� ���������� �����;
*			 ILumexDBImpl - ���������� ���������� ��� ������ � ����� ������;
*			 ILumexDBManagerImpl - ���������� ���������� ��������� ��� ������;
*			 GetLumexDBManager() - ������� ��� ��������� ���������� ��������� ��� ������.
*/


#include "stdafx.h"

#include <vector>

#include "LumexDB.h"
#include "ILumexDB.h"

/// ����� ���������� ���������� ��� ������ � �������������� ���������� �����
class ILumexDBDataSetImpl : public ILumexDBDataSet
{
public:
	// from ILumexDBDataSet //
	virtual void Release() {delete this;};
	virtual UINT GetColumnsCount() const;
	virtual LumexDB::EnOperationResult GetColumnParams(const int nColInd, TParams &Params) const;	
	virtual LumexDB::EnOperationResult FirstRow();
	virtual LumexDB::EnOperationResult LastRow();
	virtual LumexDB::EnOperationResult NextRow();
	virtual LumexDB::EnOperationResult PrevRow();
	virtual LumexDB::EnOperationResult MoveRelative(int nRowCount);
	virtual bool IsEOF() const
	{
		return m_bEOF;
	}
	virtual TColumnData* GetColumn(const int nColInd) const;
	//////////////////////////////////////////////////////////////////////////
	
	
	ILumexDBDataSetImpl();	
	ILumexDBDataSetImpl(SQLHSTMT hStmt);
	~ILumexDBDataSetImpl();

private:

	/// ������������ ����������� ����������� �� ��������������� ��������� �����
	enum EnDirections
	{
		/// ������ ������
		enFirst = 0,
		/// ���������� ������
		enPrev,
		/// ��������� ������
		enNext,
		/// ��������� ������
		enLast,
		/// ������������� �����������
		enRelative,

		/// ��� ���������� ����
		enUnknown
	};		

	/// ���������� ��������������� ��������� �����
	SQLHSTMT m_hStmt;
	/// ������, ���������� �������� ��������	
	std::vector<TParams> m_vecColumns;
	/// ������� ����� ������
	bool m_bEOF;
	/// ����� ��� ����������� ��������� ���������� ���������� ������
	LumexDB::EnOperationResult VerifyState() const;
	/// ����� ��� ����������� �� ��������������� ���������
	LumexDB::EnOperationResult FetchRow(EnDirections Direction, int nRow = 1);
	/// ����� �������������
	void Initialize();
	/// ����� ��� ����������� ���������� ���� ������
	inline EnDataTypes DefType(int nDataType) const;
	/// ����� ��� ����������� �������� ���� ������
	inline SQLSMALLINT GetTargetType(const EnDataTypes DataType) const;

#ifdef  _DEBUG
	/// ����� ��� ������ ���������� �� ������
	inline void _TraceError() const;
#endif

};

/// ����� ���������� ���������� ��� ������ � ����� ������
class ILumexDBImpl : public ILumexDB
{
public:
	// from ILumexDB //
	virtual void Release();
	virtual LumexDB::EnOperationResult BeginTransaction();
	virtual LumexDB::EnOperationResult EndTransaction(bool bCommit);
	virtual short GetLastSQLErrorCode() const
	{
		return m_SQLRet;
	}
	virtual LumexDB::EnOperationResult ExecuteSQL(LPCTSTR lpSQL, ILumexDBDataSet** pDataSet);
	virtual LumexDB::EnOperationResult SetDBPassword(LPCTSTR lpcOldPassword, LPCTSTR lpcNewPassword, LumexDB::EnDataBaseDrivers Driver);
	virtual bool IsConnected() const
	{
		return m_bConnect;
	}
	//////////////////////////////////////////////////////////////////////////
	
	ILumexDBImpl();
	ILumexDBImpl(SQLHENV hEnv, SQLHDBC hDbc, bool bConnect = true, LPCTSTR lpFilePath = NULL, LPCTSTR lpUID = NULL, LPCTSTR lpPWD = NULL, LumexDB::EnDataBaseDrivers Driver = LumexDB::enLastDriver);
	~ILumexDBImpl();

private:
	/// ����������� ���� ������
	SQLHDBC m_hDbc;
	/// ����������� ��������� ����������
	SQLHENV m_hEnv;
	/// ������� ������� ������������
	bool m_bConnect;
	/// ������� ���������� ����������
	bool m_bTransaction;
	/// ��� ������ ��������� ��������
	SQLRETURN m_SQLRet;
	/// ��� ������������
	LPTSTR m_lpUID;
	/// ������
	LPTSTR m_lpPWD;
	/// ���� � ����
	LPTSTR m_lpFilePath;
	/// ������� ���� ������
	LumexDB::EnDataBaseDrivers m_Driver;
	/// ����� ��� ����������� ��������� ���������� ���������� ������
	LumexDB::EnOperationResult VerifyState() const;

#ifdef  _DEBUG
	/// ����� ��� ������ ���������� �� ������
	inline void _TraceError() const;
#endif
	
};

#ifdef  _DEBUG
	#define TraceError _TraceError()
#else
	#define TraceError
#endif

/// ����� ���������� ���������� ��������� ��� ������
class ILumexDBManagerImpl : public ILumexDBManager
{
public:
	// from ILumexDBManager //
	virtual ILumexDB* CreateDB(LPCTSTR lpFileName, bool bCreateAlways, LumexDB::EnDataBaseDrivers Driver, LPCTSTR lpUserName, LPCTSTR lpPassword);
	virtual ILumexDB* OpenDB(LPCTSTR lpFileName, LumexDB::EnDataBaseDrivers Driver, LPCTSTR lpUserName, LPCTSTR lpPassword);
	virtual LumexDB::EnOperationResult CompactDB(LPCTSTR lpFileName, LumexDB::EnDataBaseDrivers Driver, LPCTSTR lpUserName, LPCTSTR lpPassword);
	//////////////////////////////////////////////////////////////////////////
	
	ILumexDBManagerImpl() {}
};

static LPCTSTR s_lpDriverNames[LumexDB::enLastDriver] = 
{
	_T("MICROSOFT ACCESS DRIVER (*.MDB)")
};

static int s_anDataTypes[ILumexDBDataSet::enLastDataTypes] = 
{
	SQL_BIT,
	SQL_INTEGER,
	SQL_DOUBLE,
	SQL_WVARCHAR,		
	SQL_TYPE_TIMESTAMP
};

// Definitions

// ILumexDBDataSetImpl

// TParams
ILumexDBDataSet::TParams::TParams() : 
	m_lpColName(NULL),
	m_DataType(enLastDataTypes),
	m_udwColSize(0)
{
}

ILumexDBDataSet::TParams::~TParams()
{
	if (m_lpColName != NULL)
	{
		delete []m_lpColName;
	}
}

ILumexDBDataSet::TParams::TParams(LPCTSTR lpColName) : 
	m_DataType(enLastDataTypes),
	m_udwColSize(0)
{
	if (lpColName == NULL)
	{
		m_lpColName = NULL;
		return;
	}

	int nLen = ::lstrlen(lpColName);
	m_lpColName = new TCHAR[nLen + 1];
	::lstrcpy(m_lpColName, lpColName);
}

ILumexDBDataSet::TParams::TParams(const TParams &P) :
	m_lpColName(P.m_lpColName),			
	m_DataType(P.m_DataType),
	m_udwColSize(P.m_udwColSize)
{
	if (P.m_lpColName != NULL)
	{
		int nLen = ::lstrlen(P.m_lpColName);
		m_lpColName = new TCHAR[nLen + 1];
		::lstrcpy(m_lpColName, P.m_lpColName);	
	}
	else
	{
		m_lpColName = NULL;
	}
}

const ILumexDBDataSet::TParams& ILumexDBDataSet::TParams::operator = (const TParams& P)
{
	if (&P == this)
		return *this;

	if (m_lpColName != NULL)
	{
		delete []m_lpColName;
	}

	if (P.m_lpColName != NULL)
	{
		int nLen = ::lstrlen(P.m_lpColName);
		m_lpColName = new TCHAR[nLen + 1];
		::lstrcpy(m_lpColName, P.m_lpColName);	
	}
	else
	{
		m_lpColName = NULL;
	}

	m_DataType = P.m_DataType;
	m_udwColSize = P.m_udwColSize;

	return *this;
}

ILumexDBDataSet::TParams* ILumexDBDataSet::TParams::Creator()
{
	return new ILumexDBDataSet::TParams;
}

void ILumexDBDataSet::TParams::Release()
{
	delete this;
}

// TColumnData
ILumexDBDataSet::TColumnData::TColumnData() : 
	m_Type(enLastDataTypes),
	m_Size(0),
	m_pData(NULL)
{	
}

ILumexDBDataSet::TColumnData::~TColumnData()
{
	if (m_pData)
	{
		delete []m_pData;
		m_pData = NULL;
	}
}

ILumexDBDataSet::TColumnData::TColumnData(const TColumnData& CD)
{
	m_Type = CD.m_Type;
	m_Size = CD.m_Size;
	if (CD.m_pData == NULL)
	{
		m_pData = NULL;
	}
	else
	{
		m_pData = new char[m_Size];
		::memcpy(m_pData, CD.m_pData, m_Size);
	}
}

const ILumexDBDataSet::TColumnData& ILumexDBDataSet::TColumnData::operator = (const TColumnData& CD)
{
	if (&CD == this)
	{
		return *this;
	}

	m_Type = CD.m_Type;			
	m_Size = CD.m_Size;

	if (m_pData)
	{
		delete []m_pData;
		m_pData = NULL;
	}

	if (CD.m_pData != NULL)	
	{
		m_pData = new char[m_Size];
		::memcpy(m_pData, CD.m_pData, m_Size);
	}

	return *this;
}

ILumexDBDataSet::TColumnData* ILumexDBDataSet::TColumnData::Creator()
{
	return new ILumexDBDataSet::TColumnData;
}

void ILumexDBDataSet::TColumnData::Release()
{
	delete this;
}

bool ILumexDBDataSet::TColumnData::AllocateMemoryForData(DWORD dwSize)
{
	if (m_pData != NULL)
	{
		delete []m_pData;
	}

	m_pData = new char[dwSize];

	return m_pData != NULL ? true : false;
}	

// ILumexDBDataSetImpl

ILumexDBDataSet::EnDataTypes ILumexDBDataSetImpl::DefType(int nDataType) const
{
	for (int i = 0; i < ILumexDBDataSet::enLastDataTypes; i++)
	{
		if (s_anDataTypes[i] == nDataType)
		{
			return (ILumexDBDataSet::EnDataTypes)i;
		}
	}

	return ILumexDBDataSet::enLastDataTypes;
}

ILumexDBDataSetImpl::ILumexDBDataSetImpl() :
	m_hStmt(SQL_NULL_HANDLE),
	m_bEOF(false)
{
	Initialize();
}

ILumexDBDataSetImpl::ILumexDBDataSetImpl(SQLHSTMT hStmt) :
	m_hStmt(hStmt),
	m_bEOF(false)
{
	Initialize();
}

ILumexDBDataSetImpl::~ILumexDBDataSetImpl()
{
	if (m_hStmt != SQL_NULL_HANDLE)
	{
		// ����������� ���������� ��������������� ��������� �����
		::SQLFreeHandle(SQL_HANDLE_STMT, m_hStmt);
	}
}

void ILumexDBDataSetImpl::Initialize()
{
	if (VerifyState() != LumexDB::enSuccess)
	{
		return;
	}

	UINT nColCount = GetColumnsCount();

	m_vecColumns.clear();
	for (UINT i = 0; i < nColCount; i++)
	{
		TParams params;
		if (GetColumnParams(i, params) != LumexDB::enSuccess)
		{
			m_vecColumns.clear();
			return;
		}

		m_vecColumns.push_back(params);
	}

	FirstRow();
}

LumexDB::EnOperationResult ILumexDBDataSetImpl::VerifyState() const
{
	if (m_hStmt == SQL_NULL_HANDLE)
	{
		return LumexDB::enNotInitialized;
	}

	return LumexDB::enSuccess;
}

UINT ILumexDBDataSetImpl::GetColumnsCount() const
{
	LumexDB::EnOperationResult res = VerifyState();
	if (res != LumexDB::enSuccess)
	{
		return 0;
	}

	SQLSMALLINT ColCount;

	// �������� ��� ��������������� ��������� ����� ���������� ��������
	SQLRETURN ret = ::SQLNumResultCols(m_hStmt, &ColCount);
	if (ret != SQL_SUCCESS && SQL_SUCCESS_WITH_INFO)
	{
		assert(false);
		return 0;
	}

	return ColCount;
}

LumexDB::EnOperationResult ILumexDBDataSetImpl::GetColumnParams(const int nColInd , TParams &Params) const
{
	LumexDB::EnOperationResult res = VerifyState();
	if (res != LumexDB::enSuccess)
	{
		return res;
	}

	TCHAR stBuf[LumexDB::enMaxColumnNameLength];
	SQLSMALLINT OutLen, DataType, DecimalDigit, Nullable;
	SQLUINTEGER ColumnSize;
	// �������� �������� �������. ���������: ���������� ��������������� ��������� �����; ������ ������� (��������� ���������� � 1);
	// ����� ��� ����� �������; ������������ ����� ������; ����� �����; ��� ������; ������ �������; ����� ���������� ������ �������; ����������� ��������� �������� ��� ������� (NULL � ���������  SQL).
	SQLRETURN ret = ::SQLDescribeCol(m_hStmt, nColInd + 1, stBuf, LumexDB::enMaxColumnNameLength, &OutLen, &DataType, &ColumnSize, &DecimalDigit, &Nullable);

	if (ret != SQL_SUCCESS && ret != SQL_SUCCESS_WITH_INFO)
	{
		assert(false);
		TraceError;

		return LumexDB::enODBCError;
	}

	Params = TParams(stBuf);
	Params.m_DataType = DefType(DataType);
	Params.m_udwColSize = Params.m_DataType == enString ? (ColumnSize + 1) * sizeof(TCHAR) : ColumnSize;

	return LumexDB::enSuccess;
}

LumexDB::EnOperationResult ILumexDBDataSetImpl::FetchRow(EnDirections Direction, int nRow)
{
	LumexDB::EnOperationResult res = VerifyState();
	if (res != LumexDB::enSuccess)
	{
		return res;
	}

	SQLSMALLINT FetchOrientation;

	if (Direction == enFirst)
	{
		FetchOrientation = SQL_FETCH_FIRST;
	}
	else if (Direction == enLast)
	{
		FetchOrientation = SQL_FETCH_LAST;
	}
	else if (Direction == enNext)
	{
		FetchOrientation = SQL_FETCH_NEXT;
	}
	else if (Direction == enPrev)
	{
		FetchOrientation = SQL_FETCH_PREV;
	}
	else if (Direction = enRelative)
	{
		FetchOrientation = SQL_FETCH_RELATIVE;
	}
	else
	{
		assert(false);
		return LumexDB::enNotValidParams;
	}

	// ������������ �� ��������� � ������ ����������� �� ������ ����� �����
	SQLRETURN ret = ::SQLFetchScroll(m_hStmt, FetchOrientation, nRow);

	if (ret == SQL_ERROR || ret == SQL_INVALID_HANDLE)
	{
		TraceError;
		return LumexDB::enODBCError;
	}

	if (ret == SQL_NO_DATA)
	{
		if (Direction == enLast || Direction == enFirst || Direction == enNext || (Direction == enRelative && nRow >= 0))
		{
			m_bEOF = true;
		}

		return LumexDB::enNoData;
	}

	if (ret == SQL_STILL_EXECUTING)
	{
		return LumexDB::enStillExecuting;
	}

	m_bEOF = false;

	return LumexDB::enSuccess;
}

LumexDB::EnOperationResult ILumexDBDataSetImpl::FirstRow()
{
	return FetchRow(enFirst);
}

LumexDB::EnOperationResult ILumexDBDataSetImpl::LastRow()
{
	return FetchRow(enLast);
}

LumexDB::EnOperationResult ILumexDBDataSetImpl::NextRow()
{
	return FetchRow(enNext);
}

LumexDB::EnOperationResult ILumexDBDataSetImpl::PrevRow()
{
	return FetchRow(enPrev);
}

LumexDB::EnOperationResult ILumexDBDataSetImpl::MoveRelative(int nRowCount)
{
	return FetchRow(enRelative, nRowCount);
}

SQLSMALLINT ILumexDBDataSetImpl::GetTargetType(const EnDataTypes DataType) const
{
	if (DataType == enBool)
	{
		return SQL_C_BIT;
	}
	else if (DataType == enInt)
	{
		return SQL_C_LONG;
	}
	else if (DataType == enDouble)
	{
		return SQL_C_DOUBLE;
	}
	else if (DataType == enString)
	{
		return SQL_C_TCHAR;
	}
	else if (DataType == enDataTime)
	{
		return SQL_C_TYPE_TIMESTAMP;
	}
	
	return SQL_C_DEFAULT;
}

ILumexDBDataSet::TColumnData* ILumexDBDataSetImpl::GetColumn(const int nColInd) const
{
	if (nColInd < 0 || (UINT)nColInd >= m_vecColumns.size() || VerifyState() != LumexDB::enSuccess)	
	{
		return NULL;
	}

	TColumnData *pData = new TColumnData();
	UINT uSize = m_vecColumns[nColInd].m_udwColSize;
	pData->AllocateMemoryForData(uSize);

	SQLINTEGER DataLen;
	// �������� ������ �������. ���������: ���������� ��������������� ��������� �����; ������ ������� (��������� ���������� � 1);
	// ������� ��� ������; ����� ��� ��������� ������; ������ ������; �������� ����� ������.
	SQLRETURN ret = ::SQLGetData(m_hStmt, nColInd + 1, GetTargetType(m_vecColumns[nColInd].m_DataType), pData->m_pData, uSize, &DataLen);

	if (ret != SQL_SUCCESS && ret != SQL_SUCCESS_WITH_INFO)
	{
		delete pData;
		return NULL;
	}

	if (DataLen == SQL_NULL_DATA || DataLen == SQL_NO_TOTAL)
	{
		delete []pData->m_pData;		
		pData->m_pData = NULL;

		if (DataLen == SQL_NULL_DATA)
		{
			pData->m_Type = enNullData;
		}
		else
		{
			pData->m_Type = enNoTotal;
		}

		pData->m_Size = 0;
	}
	else
	{
		pData->m_Type = m_vecColumns[nColInd].m_DataType;
		pData->m_Size = DataLen;
	}	

	return pData;
}

#ifdef  _DEBUG
void ILumexDBDataSetImpl::_TraceError() const
{
	SQLTCHAR      SqlState[6], Msg[SQL_MAX_MESSAGE_LENGTH];
	SQLINTEGER    NativeError;
	SQLSMALLINT   i = 1, MsgLen;
	SQLRETURN     rc2;

	// �������� ���������� �� ������. ���������: ��� �����������; ���������� ��������������� ��������� �����; ����� ��������� �� ������ (���������� � 1);
	// ����� ��� ��������� SQL-���������; ��� ������; ����� ��� ���������; ������������ ����� ������; �������� �����.
	rc2 = ::SQLGetDiagRec(SQL_HANDLE_STMT, m_hStmt, i, SqlState, &NativeError, Msg, SQL_MAX_MESSAGE_LENGTH, &MsgLen);
	// ���� ���� ���������
	// 	   ��������� �����
	//while (rc2 != SQL_NO_DATA)
	//{
	//	TCHAR stBuf[4 * SQL_MAX_MESSAGE_LENGTH];
	//	if (MsgLen < SQL_MAX_MESSAGE_LENGTH)
	//	{
	//		Msg[MsgLen] = _T('\0');
	//	}
	//	else
	//	{
	//		Msg[SQL_MAX_MESSAGE_LENGTH - 1] = _T('\0');
	//	}

	//	_stprintf_s(stBuf, _T("SQLERROR: Num = %d, SqlState = %s, NativeError = %d, Msg = %s"), i, SqlState, NativeError, Msg);

	//	::OutputDebugStr(stBuf);

	//	i++;
	//	// �������� ���������� �� ������. ���������: ��� �����������; ���������� ��������������� ��������� �����; ����� ��������� �� ������ (���������� � 1);
	//	// ����� ��� ��������� SQL-���������; ��� ������; ����� ��� ���������; ������������ ����� ������; �������� �����.
	//	rc2 = ::SQLGetDiagRec(SQL_HANDLE_STMT, m_hStmt, i, SqlState, &NativeError, Msg, SQL_MAX_MESSAGE_LENGTH, &MsgLen);		
	//}
}
#endif

// ILumexDBImpl

ILumexDBImpl::ILumexDBImpl() :
	m_hEnv(SQL_NULL_HANDLE),
	m_hDbc(SQL_NULL_HANDLE),
	m_bConnect(false),
	m_bTransaction(false),
	m_SQLRet(SQL_SUCCESS),
	m_lpFilePath(NULL),
	m_lpPWD(NULL),
	m_lpUID(NULL),
	m_Driver(LumexDB::enLastDriver)
{	
}

ILumexDBImpl::ILumexDBImpl(SQLHENV hEnv, SQLHDBC hDbc, bool bConnect, LPCTSTR lpFilePath, LPCTSTR lpUID, LPCTSTR lpPWD, LumexDB::EnDataBaseDrivers Driver) :
	m_hEnv(hEnv),
	m_hDbc(hDbc),
	m_bConnect(bConnect),
	m_bTransaction(false),
	m_SQLRet(SQL_SUCCESS),
	m_lpFilePath(NULL),
	m_lpPWD(NULL),
	m_lpUID(NULL),
	m_Driver(Driver)
{	

	if (lpFilePath != NULL)
	{
		m_lpFilePath = new TCHAR[LumexDB::enBufferLength];
		::lstrcpyn(m_lpFilePath, lpFilePath, LumexDB::enBufferLength);
	}

	if (lpUID != NULL)
	{
		m_lpUID = new TCHAR[LumexDB::enMaxUIDAndPWDLength];
		::lstrcpyn(m_lpUID, lpUID, LumexDB::enMaxUIDAndPWDLength);
	}

	if (lpPWD != NULL)
	{
		m_lpPWD = new TCHAR[LumexDB::enMaxUIDAndPWDLength];
		::lstrcpyn(m_lpPWD, lpPWD, LumexDB::enMaxUIDAndPWDLength);
	}
}

ILumexDBImpl::~ILumexDBImpl()
{
	SQLRETURN ret;

	bool bConnect = m_bConnect;

	if (m_bConnect)
	{
		if (m_bTransaction)
		{
			EndTransaction(false);
		}

		if (m_hDbc != SQL_NULL_HANDLE)
		{
			// ��������� ����������
			ret = ::SQLDisconnect(m_hDbc);
			m_bConnect = false;

			if (ret != SQL_SUCCESS && ret != SQL_SUCCESS_WITH_INFO)
			{
				TraceError;
				assert(false);
			}
		}
		else
		{
			assert(false);
		}
	}	

	if (m_hDbc != SQL_NULL_HANDLE)
	{
		// ����������� ���������� ����
		ret = ::SQLFreeHandle(SQL_HANDLE_DBC, m_hDbc);
		if (ret != SQL_SUCCESS && ret != SQL_SUCCESS_WITH_INFO)
		{
			TraceError;
			assert(false);		
		}		
	}

	if (m_hEnv != SQL_NULL_HANDLE)
	{
		// ����������� ���������� ���������
		ret = ::SQLFreeHandle(SQL_HANDLE_ENV, m_hEnv);
		if (ret != SQL_SUCCESS && ret != SQL_SUCCESS_WITH_INFO)
		{
			TraceError;
			assert(false);		
		}
	}
	
#ifndef DLL4PRO
	if (bConnect && m_lpFilePath != NULL)
	{
		LumexDB::EnOperationResult res = ::GetLumexDBManager()->CompactDB(m_lpFilePath, m_Driver, m_lpUID, m_lpPWD);
		assert(res == LumexDB::enSuccess);
	}
#endif
	

	if (m_lpFilePath != NULL)
	{
		delete []m_lpFilePath;
	}

	if (m_lpUID != NULL)
	{
		delete []m_lpUID;
	}

	if (m_lpPWD != NULL)
	{
		delete []m_lpPWD;
	}
}

void ILumexDBImpl::Release()
{
	delete this;
}

LumexDB::EnOperationResult ILumexDBImpl::VerifyState() const
{
	if (m_hDbc == SQL_NULL_HANDLE || m_hEnv == SQL_NULL_HANDLE)
	{
		return LumexDB::enNotInitialized;
	}

	if (!m_bConnect)
	{
		return LumexDB::enNotConnected;
	}

	return LumexDB::enSuccess;
}

LumexDB::EnOperationResult ILumexDBImpl::BeginTransaction()
{
	LumexDB::EnOperationResult res = VerifyState();
	if (res != LumexDB::enSuccess)
	{
		assert(false);
		return res;
	}

	if (m_bTransaction)
	{
		assert(false);
		return LumexDB::enTransactionIsAlreadyBegun;
	}
	
	// ���������� ������� ����, ��� ���� ����� ��������� ������������ - ��������� �������������� ���������� ���������� ����� ���������� ��������.
	// ���������: ���������� ����; ��� ���������; �������� ���������; �� ������������.
	m_SQLRet = ::SQLSetConnectAttr (m_hDbc, SQL_ATTR_AUTOCOMMIT, (SQLPOINTER)SQL_AUTOCOMMIT_OFF, 0);
	if (m_SQLRet != SQL_SUCCESS && m_SQLRet != SQL_SUCCESS_WITH_INFO)
	{
		TraceError;
		return LumexDB::enODBCError;
	}

	m_bTransaction = true;

	return LumexDB::enSuccess;
}

LumexDB::EnOperationResult ILumexDBImpl::EndTransaction(bool bCommit)
{
	LumexDB::EnOperationResult res = VerifyState();
	if (res != LumexDB::enSuccess)
	{
		assert(false);
		return res;
	}

	if (!m_bTransaction)
	{
		assert(false);
		return LumexDB::enTransactionIsNotBegun;
	}

	// ��������� ����������. ��������: ��� �����������; ���������� ����; ��� ������������ ����������, ��� ������ �����.
	m_SQLRet = ::SQLEndTran(SQL_HANDLE_DBC, m_hDbc, bCommit ? SQL_COMMIT : SQL_ROLLBACK);

	if (m_SQLRet != SQL_SUCCESS && m_SQLRet != SQL_SUCCESS_WITH_INFO)
	{
		TraceError;
		// ���������� ������� ����, ��� ��������� ���� ��������� ������������ - �������� �������������� ���������� ���������� ����� ���������� ��������.
		// ���������: ���������� ����; ��� ���������; �������� ���������; �� ������������.
		SQLRETURN ret = ::SQLSetConnectAttr (m_hDbc, SQL_ATTR_AUTOCOMMIT, (SQLPOINTER)SQL_AUTOCOMMIT_ON, 0);
		if (ret != SQL_SUCCESS && ret != SQL_SUCCESS_WITH_INFO)
		{
			TraceError;
		}
		return LumexDB::enODBCError;
	}

	// ���������� ������� ����, ��� ��������� ���� ��������� ������������ - �������� �������������� ���������� ���������� ����� ���������� ��������.
	// ���������: ���������� ����; ��� ���������; �������� ���������; �� ������������.
	m_SQLRet = ::SQLSetConnectAttr (m_hDbc, SQL_ATTR_AUTOCOMMIT, (SQLPOINTER)SQL_AUTOCOMMIT_ON, 0);
	if (m_SQLRet != SQL_SUCCESS && m_SQLRet != SQL_SUCCESS_WITH_INFO)
	{
		TraceError;
		return LumexDB::enODBCError;
	}

	m_bTransaction = false;

	return LumexDB::enSuccess;
}

LumexDB::EnOperationResult ILumexDBImpl::ExecuteSQL(LPCTSTR lpSQL, ILumexDBDataSet** pDataSet)
{	
//	InitLLTimer();
//	StartLLTimer();

	if (lpSQL == NULL || ::lstrlen(lpSQL) <= 0)
	{
		assert(false);
		return LumexDB::enNotValidParams;
	}

	if (pDataSet != NULL)
	{
		*pDataSet = NULL;
	}

	LumexDB::EnOperationResult res = VerifyState();
	if (res != LumexDB::enSuccess)
	{
		assert(false);
		return res;
	}

	SQLHSTMT hStmt;
	// �������� ���������� ��������� �����. ���������: ��� �����������; ���������� ����; ���������� ��������� �����.
	m_SQLRet = ::SQLAllocHandle(SQL_HANDLE_STMT, m_hDbc, &hStmt);

	if (m_SQLRet != SQL_SUCCESS && m_SQLRet != SQL_SUCCESS_WITH_INFO)
	{
		TraceError;
		return LumexDB::enODBCError;
	}
	
	if (pDataSet != NULL)
	{
		// ������������� ��������� ����������� ��������� �����. ���������: ���������� ��������� �����; ��� ���������; �������� (����������� ������); ��� �������� ���������.
		SQLRETURN ret = ::SQLSetStmtAttr(hStmt, SQL_ATTR_CURSOR_TYPE, (SQLPOINTER)SQL_CURSOR_STATIC, SQL_IS_UINTEGER);
		assert(ret == SQL_SUCCESS || ret == SQL_SUCCESS_WITH_INFO);
		if (ret != SQL_SUCCESS && ret != SQL_SUCCESS_WITH_INFO)
		{
			// ����������� ����������
			::SQLFreeHandle(SQL_HANDLE_STMT, hStmt);
			return LumexDB::enODBCError;			
		}		
	}	

	// ��������� SQL-������. ���������: ���������� ��������������� ��������� �����; ����� �������; ������� ������, ��������������� �����.
#ifdef _UNICODE 
	m_SQLRet = ::SQLExecDirect(hStmt, (SQLTCHAR*)lpSQL, SQL_NTSL);
//	TRACE1("%f\n", dtll);
#else
	m_SQLRet = ::SQLExecDirect(hStmt, (SQLTCHAR*)lpSQL, SQL_NTS);
#endif
	
	// ���������� ���������� ������ ��������������� ��� ��������� ��������, �� � ���������� ���� ���� � ������ ���������� ������ ���� ������ �
	// �������������� ����������. ��� ������ 2 ������ �������, ������ ��� � ������ ���������� ������ ������ ����� ���� � ������ ����������. ������,
	// ���� �������������� ��������� ��������, ����� ������� ������, ��� ������ �����������, ��� �������������� �������.
	if (m_SQLRet != SQL_SUCCESS && m_SQLRet != SQL_SUCCESS_WITH_INFO && m_SQLRet != SQL_NO_DATA)
	{
		TraceError;
		// ����������� ����������
		SQLRETURN ret = ::SQLFreeHandle(SQL_HANDLE_STMT, hStmt);
		if (ret != SQL_SUCCESS && ret != SQL_SUCCESS_WITH_INFO)
		{
			TraceError;
		}
		return LumexDB::enODBCError;
	}

	if (pDataSet == NULL)
	{
		// ����������� ����������
		m_SQLRet = ::SQLFreeHandle(SQL_HANDLE_STMT, hStmt);
	}
	else
	{
		*pDataSet = new ILumexDBDataSetImpl(hStmt);
	}

	if (m_SQLRet != SQL_SUCCESS && m_SQLRet != SQL_SUCCESS_WITH_INFO && m_SQLRet != SQL_NO_DATA)
	{
		TraceError;
		return LumexDB::enODBCError;
	}

	return LumexDB::enSuccess;
}

#ifdef  _DEBUG

void ILumexDBImpl::_TraceError() const
{
	SQLTCHAR      SqlState[6], Msg[SQL_MAX_MESSAGE_LENGTH];
	SQLINTEGER    NativeError;
	SQLSMALLINT   i = 1, MsgLen;
	SQLRETURN     rc2;
	
	// �������� ���������� �� ������. ���������: ��� �����������; ���������� ����; ����� ��������� �� ������ (���������� � 1);
	// ����� ��� ��������� SQL-���������; ��� ������; ����� ��� ���������; ������������ ����� ������; �������� �����.
	rc2 = ::SQLGetDiagRec(SQL_HANDLE_DBC, m_hDbc, i, SqlState, &NativeError, Msg, SQL_MAX_MESSAGE_LENGTH, &MsgLen);
	// ��������� ����� 2
	//while (rc2 != SQL_NO_DATA)
	//{
	//	TCHAR stBuf[4 * SQL_MAX_MESSAGE_LENGTH];
	//	if (MsgLen < SQL_MAX_MESSAGE_LENGTH)
	//	{
	//		Msg[MsgLen] = _T('\0');
	//	}
	//	else
	//	{
	//		Msg[SQL_MAX_MESSAGE_LENGTH - 1] = _T('\0');
	//	}

	//	_stprintf_s(stBuf, _T("SQLERROR: Num = %d, SqlState = %s, NativeError = %d, Msg = %s"), i, SqlState, NativeError, Msg);

	//	::OutputDebugStr(stBuf);

	//	i++;
	//	// �������� ���������� �� ������. ���������: ��� �����������; ���������� ����; ����� ��������� �� ������ (���������� � 1);
	//	// ����� ��� ��������� SQL-���������; ��� ������; ����� ��� ���������; ������������ ����� ������; �������� �����.
	//	rc2 = ::SQLGetDiagRec(SQL_HANDLE_DBC, m_hDbc, i, SqlState, &NativeError, Msg, SQL_MAX_MESSAGE_LENGTH, &MsgLen);		
	//}
}

#endif

LumexDB::EnOperationResult ILumexDBImpl::SetDBPassword(LPCTSTR lpcOldPassword, LPCTSTR lpcNewPassword, LumexDB::EnDataBaseDrivers Driver)
{
	LumexDB::EnOperationResult res = VerifyState();
	if (res != LumexDB::enSuccess)
	{
		return res;
	}

	if (lpcNewPassword == NULL || Driver < LumexDB::enFirstDriver || Driver >= LumexDB::enLastDriver)
	{
		return LumexDB::enNotValidParams;
	}

	TCHAR stBuf[LumexDB::enBufferLength];

	LPCTSTR pFormat = lpcOldPassword == NULL ? _T("ALTER DATABASE PASSWORD %s NULL") : _T("ALTER DATABASE PASSWORD %s %s");
	
	_stprintf_s(stBuf, pFormat, lpcNewPassword, lpcOldPassword);

    return ExecuteSQL(stBuf, NULL);
}


// ILumexDBManagerImpl

ILumexDB* ILumexDBManagerImpl::CreateDB(LPCTSTR lpFileName, bool bCreateAlways, LumexDB::EnDataBaseDrivers Driver, LPCTSTR lpUserName, LPCTSTR lpPassword)
{	
	if (lpFileName == NULL || ::lstrlen(lpFileName) == 0 || Driver < LumexDB::enFirstDriver || Driver >= LumexDB::enLastDriver)
	{
		assert(false);
		return NULL;
	}	

	HANDLE hDBFile = ::CreateFile(lpFileName, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (hDBFile != INVALID_HANDLE_VALUE)
	{
		::CloseHandle(hDBFile);
		if (!bCreateAlways || ::DeleteFile(lpFileName) == FALSE)
		{			
			return NULL;
		}
	}

	TCHAR stBuffer[LumexDB::enBufferLength];
	if (LumexDB::enMSAccessDriver == Driver)
	{		
		if (lpUserName != NULL && lpPassword != NULL)
		{
			// ������ ������: ������� ���� ���� ������; ��� ���������� ���������� ����� ������� ��������; ��� ������������; ������.
			_stprintf_s(stBuffer, _T("CREATE_DB=\"%s\" RUSSIAN ENCRYPT UID=%s PWD=%s"), lpFileName, lpUserName, lpPassword);
		}
		else if (lpUserName != NULL && lpPassword == NULL)
		{
			_stprintf_s(stBuffer, _T("CREATE_DB=\"%s\" RUSSIAN ENCRYPT UID=%s"), lpFileName, lpUserName);
		}
		else
		{
			_stprintf_s(stBuffer, _T("CREATE_DB=\"%s\" RUSSIAN ENCRYPT"), lpFileName);
		}
	}
	
	// ������� ����-�������� ������. ���������: ���������� ������������� ����; ������� ���������� ���������; ������������ ��������; ������ ������.
	if (::SQLConfigDataSource( NULL, ODBC_ADD_DSN, s_lpDriverNames[Driver], stBuffer) == FALSE)
	{
		assert(false);
		return NULL;
	}

	return OpenDB(lpFileName, Driver, lpUserName, lpPassword);
}

LumexDB::EnOperationResult ILumexDBManagerImpl::CompactDB(LPCTSTR lpFileName, LumexDB::EnDataBaseDrivers Driver, LPCTSTR lpUserName, LPCTSTR lpPassword)
{
	if (lpFileName == NULL || ::lstrlen(lpFileName) == 0 || Driver < LumexDB::enFirstDriver || Driver >= LumexDB::enLastDriver)
	{
		assert(false);
		return LumexDB::enFail;
	}

	TCHAR stBuffer[LumexDB::enBufferLength];

	::memset(stBuffer, 0, LumexDB::enBufferLength * sizeof(TCHAR));
	
	if (Driver == LumexDB::enMSAccessDriver)
	{
		if (lpUserName == NULL && lpPassword == NULL)
		{
			_stprintf_s(stBuffer, _T("REPAIR_DB=\"%s\""), lpFileName);
		}
		else if (lpPassword == NULL)
		{
			_stprintf_s(stBuffer, _T("REPAIR_DB=\"%s\" UID=%s"), lpFileName, lpUserName);
		}
		else
		{
			// ������ ������: ��������������� ���� ���� ������; ��� ������������; ������.
			_stprintf_s(stBuffer, _T("REPAIR_DB=\"%s\" UID=%s PWD=%s"), lpFileName, lpUserName, lpPassword);
		}
	}

	// �������� �������� ������. ���������: ���������� ������������� ����; ������� ��������� ���������; ������������ ��������; ������ ������.
	if (::SQLConfigDataSource( NULL, ODBC_CONFIG_DSN, s_lpDriverNames[Driver], stBuffer) == FALSE)
	{/*
		for(int i = 1; i < 9; i++)
         {
          DWORD dwErrorCode;
          const WORD cbMsgMax = SQL_MAX_MESSAGE_LENGTH - 1;
          WORD cbMsg;
          TCHAR stBuf[cbMsgMax];
          if (::SQLInstallerError(i, &dwErrorCode, stBuf, cbMsgMax, &cbMsg) == SQL_NO_DATA)
          {
               break;
          }
          ::MessageBox(NULL, stBuf, _T("Error"), MB_ICONWARNING | MB_OK);
          }
*/
          return LumexDB::enODBCError;
	}

	::memset(stBuffer, 0, LumexDB::enBufferLength * sizeof(TCHAR));

	if (Driver == LumexDB::enMSAccessDriver)
	{
		if (lpUserName == NULL && lpPassword == NULL)
		{
			_stprintf_s(stBuffer, _T("COMPACT_DB=\"%s\" \"%s\" Russian ENCRYPT"), lpFileName, lpFileName);
		}
		else if (lpPassword == NULL)
		{
			_stprintf_s(stBuffer, _T("COMPACT_DB=\"%s\" \"%s\" Russian ENCRYPT UID=%s"), lpFileName, lpFileName, lpUserName);
		}
		else
		{
			// ������ ������: ������� ���� ���� ������; ��� ���������� ���������� ����� ������� ��������; ��� ������������; ������.
			_stprintf_s(stBuffer, _T("COMPACT_DB=\"%s\" \"%s\" Russian ENCRYPT UID=%s PWD=%s"), lpFileName, lpFileName, lpUserName, lpPassword);
		}
	}

	// �������� �������� ������. ���������: ���������� ������������� ����; ������� ��������� ���������; ������������ ��������; ������ ������.
	if (::SQLConfigDataSource( NULL, ODBC_CONFIG_DSN, s_lpDriverNames[Driver], stBuffer) == FALSE)
	{
		return LumexDB::enODBCError;
	}

	return LumexDB::enSuccess;
}

ILumexDB* ILumexDBManagerImpl::OpenDB(LPCTSTR lpFileName, LumexDB::EnDataBaseDrivers Driver, LPCTSTR lpUserName, LPCTSTR lpPassword)
{
//	CompactDB(lpFileName, Driver, lpUserName, lpPassword);

	if (lpFileName == NULL || ::lstrlen(lpFileName) == 0)
	{
		assert(false);
		return NULL;
	}

	HANDLE hDBFile = ::CreateFile(lpFileName, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (hDBFile == INVALID_HANDLE_VALUE)
	{
		return NULL;
	}
	::CloseHandle(hDBFile);

	SQLHDBC hDbc  = SQL_NULL_HANDLE;
	SQLHENV hEnvironment = SQL_NULL_HANDLE;	

	// �������� ���������� ���������. ���������: ��� �����������; ������� ����������; ���������� ���������.
	SQLRETURN ret = ::SQLAllocHandle(SQL_HANDLE_ENV, SQL_NULL_HANDLE, &hEnvironment);
	if (ret != SQL_SUCCESS && ret != SQL_SUCCESS_WITH_INFO)
	{
		assert(false);
		return NULL;
	}

	// ������������� ��������� ���������. ���������: ���������� ���������; ��� ��������� (������ ODBC); �������� (������ 3); ��� �������� ���������.
	ret = ::SQLSetEnvAttr(hEnvironment, SQL_ATTR_ODBC_VERSION, (SQLPOINTER)SQL_OV_ODBC3, SQL_IS_INTEGER);
	if (ret != SQL_SUCCESS && ret != SQL_SUCCESS_WITH_INFO)
	{
		// ����������� ����������
		::SQLFreeHandle(SQL_HANDLE_ENV, hEnvironment);
		assert(false);
		return NULL;
	}

	// �������� ���������� ����. ���������: ��� �����������; ���������� ���������; ���������� ����.
	ret = ::SQLAllocHandle(SQL_HANDLE_DBC, hEnvironment, &hDbc);
	if (ret != SQL_SUCCESS && ret != SQL_SUCCESS_WITH_INFO)
	{
		// ����������� ����������
		::SQLFreeHandle(SQL_HANDLE_ENV, hEnvironment);
		assert(false);
		return NULL;
	}
	
	TCHAR stBuffer[LumexDB::enBufferLength];
	TCHAR stOutBuffer[LumexDB::enBufferLength];
	if (LumexDB::enMSAccessDriver == Driver)
	{
		if (lpUserName == NULL)
		{
			_stprintf_s(stBuffer, _T("DRIVER={%s};DBQ=%s;SafeTransactions=1;Exclusive=1;ExtendedANSISQL=1"), s_lpDriverNames[Driver], lpFileName);
		}
		else
		{
			if (lpPassword == NULL)
			{
				_stprintf_s(stBuffer, _T("DRIVER={%s};DBQ=%s;UID=%s;SafeTransactions=1;Exclusive=1;ExtendedANSISQL=1"), s_lpDriverNames[Driver], lpFileName, lpUserName);
			}
			else
			{
				// ������ ����������: ������� ����; ���� � ����; ��� ������������; ������; ���������� ���������� ����������; ����������� ������; ���������� ����������� ����� ������ SQL.
				_stprintf_s(stBuffer, _T("DRIVER={%s};DBQ=%s;UID=%s;PWD=%s;SafeTransactions=1;Exclusive=1;ExtendedANSISQL=1"), s_lpDriverNames[Driver], lpFileName, lpUserName, lpPassword);
			}
		}
	}

	SQLSMALLINT LenOut;
	// ������������� ���������� � �����. ���������: ���������� ����; ���������� ������������� ����; ������� ������ ����������; ����� ������� ������; ����� ��� �������� ������; ������������ ����� ������; �������� �����; ��������� (�� ������� ������ ����������).
	ret = ::SQLDriverConnect(hDbc, NULL, (SQLTCHAR*)stBuffer, (SQLSMALLINT)::lstrlen(stBuffer) * sizeof(TCHAR), (SQLTCHAR*)stOutBuffer, LumexDB::enBufferLength, &LenOut, SQL_DRIVER_NOPROMPT);

	if (ret != SQL_SUCCESS && ret != SQL_SUCCESS_WITH_INFO)
	{
		// ����������� ����������
		::SQLFreeHandle(SQL_HANDLE_DBC, hDbc);
		// ����������� ����������
		::SQLFreeHandle(SQL_HANDLE_ENV, hEnvironment);
		assert(false);
		return NULL;
	}	

	ILumexDBImpl *pDB = new ILumexDBImpl(hEnvironment, hDbc, true, lpFileName, lpUserName, lpPassword, Driver);		

	return pDB;
}

/// ������� ��� ��������� ���������� ��������� ��� ������
ILumexDBManager* GetLumexDBManager()
{
	static ILumexDBManagerImpl manager;
	return &manager;
}
