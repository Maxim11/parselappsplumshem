#ifndef _PARCEL_CREATE_MODEL_DLG_H_
#define _PARCEL_CREATE_MODEL_DLG_H_

#pragma once

#include "ModelCalculate.h"
#include "ParcelListCtrl.h"
#include "Resource.h"


///////////////////////////////////////////////////////////////////////////////////////////////////
// ������ �������� � �������������� �������������� ������.

// ��������� ������ ������ �������

const int C_MODDLG_MODE_CREATE	= 0;		// ����� �������� ����� ������
const int C_MODDLG_MODE_CHANGE	= 1;		// ����� �������������� ������
const int C_MODDLG_MODE_COPY	= 2;		// ����� ����������� ������

// �������� �������

const int C_MODDLG_PAGE_PRM = 0;		// �������� ������ �����������
const int C_MODDLG_PAGE_SMP = 1;		// �������� ������ ��������
const int C_MODDLG_PAGE_PRE = 2;		// �������� ������ ������������ ����������
const int C_MODDLG_PAGE_SET = 3;		// �������� ������ ���������� �������

class CCreateModelDlg : public CDialog
{
	DECLARE_DYNAMIC(CCreateModelDlg)

public:

	CCreateModelDlg(CModelData* Mod, int mode = C_MODDLG_MODE_CREATE, CWnd* pParent = NULL);	// �����������
	virtual ~CCreateModelDlg();																	// ����������

	enum { IDD = IDD_CREATE_MODEL };								// ������������� �������

	CTabCtrl m_Tab;					// �������� �� ��������

	CParcelListCtrl m_listParamAll;	// �������, ���������� ������ ���� ���� �����������
	CListBox m_listParamSel;		// �������, ���������� ������ ���� ��������� �����������
	CParcelListCtrl m_listTransAll;	// �������, ���������� ������ ���� ��������� ������� ��������
	CListBox m_listTransSel;		// �������, ���������� ������ ���� ��������� ������� ��������
	CComboBox m_AnType;				// ������ ��� ������ "���� �������"
	CEdit m_Name;					// ���� ��� �������������� "����� ������"

	CParcelListCtrl m_listSmpl;		// �������, ���������� ������ ���� ���� �������� ��������
	CParcelListCtrl m_listCalib;	// �������, ���������� ������ ���� ��������, ��������� ��� ����������
	CParcelListCtrl m_listValid;	// �������, ���������� ������ ���� ��������, ��������� ��� ���������
	int m_nListParStart;
	int m_nListTimeCol;

	CComboBox m_Template;			// ������ ��� ������ ������� �����������
	CButton m_UseTemplate;			// ��������� "�������� ������������� �������"
	CEdit m_ProcValid;				// ���� ��� �������������� "������� ��� ���������"

	CListCtrl m_listParamC;		// �������, ���������� ������ ����������� � �� ��������
	CEdit m_LowSpec;			// ���� ��� �������������� "������ ������� ������������� ���������"
	CEdit m_UpperSpec;			// ���� ��� �������������� "������� ������� ������������� ���������"
	CEdit m_MSD;				// ���� ��� �������������� "����������� ��� �����������"
	CEdit m_LowCalib;			// ���� ��� �������������� "������� �������� ��������� �����������"
	CEdit m_UpperCalib;			// ���� ��� �������������� "�������� �������� ��������� �����������"
	CEdit m_CorrA;				// ���� ��� �������������� "�������� ����������� a"
	CEdit m_CorrB;				// ���� ��� �������������� "�������� ����������� b"
	CComboBox m_SpType;			// ������ ��� ������ "���� �������"
	CComboBox m_Preproc0;		// ������ ��� ������ "1-� ������������"
	CComboBox m_Preproc1;		// ������ ��� ������ "2-� ������������"
	CComboBox m_Preproc2;		// ������ ��� ������ "3-� ������������"
	CComboBox m_Preproc3;		// ������ ��� ������ "4-� ������������"
	CComboBox m_Preproc4;		// ������ ��� ������ "5-� ������������"
	CComboBox m_Preproc5;		// ������ ��� ������ "6-� ������������"
	CComboBox m_Preproc6;		// ������ ��� ������ "7-� ������������"

	CParcelListCtrl m_listAll;	// �������, ���������� ������ ���� ������������ �����
	CListCtrl m_listExc;		// �������, ���������� ������ ����������� ������������ �����
	CEdit m_NComp;				// ���� ��� �������������� "����� ������� ���������"
	CEdit m_DimHiper;			// ���� ��� �������������� "������� ���������"
	CEdit m_NHarm;				// ���� ��� �������������� "����� ��������"
	CEdit m_Kfnl;				// ���� ��� �������������� "������������ ������������"
	CEdit m_MaxMah;				// ���� ��� �������������� "����������� ���������� ������������" ��� qlt
	CEdit m_MaxMah2;			// ���� ��� �������������� "����������� ���������� ������������" ��� qnt
	CEdit m_MaxMSD;				// ���� ��� �������������� "����������� ��� ����������� ��� ������������� �������"
	CComboBox m_ModType;		// ������ ��� ������ "���� ������" ��� �������������� ������
	CComboBox m_ModQltType;		// ������ ��� ������ "���� ������" ��� ������������ ������
	CComboBox m_HSOType;		// ������ ��� ������ "���� HSO-������"
	CButton m_UseSECV;			// ��������� "�������� ������������ �� SECV"

protected:

	CModelData *ModData;		// ��������� �� ������������� ������������� ������

	int Mode;					// ����� ������ �������

	int m_vTab;					// ����� �������� ��������

	int NAllParams;				// ����� ����� �����������
	CString m_vName;			// ��������� �������� "��� ������"
	int m_vAnType;				// ������ ���������� "���� �������"
	VModPar Params;				// ������ ��������� �����������
	VStr TransesAll;			// ������ ��������� ������� ��������
	VStr TransesSel;			// ������ ��������� ������� ��������


	VStr SamSelAll;				// ������ ��������, ���������� � ����� ������
	int GetSamSelAll();

	VStr SamSelClb;				// ������ ��������, ���������� � ������ ��� ����������
	int GetSamSelClb();

	VStr SamSelVal;				// ������ ��������, ���������� � ������ ��� ���������
	int GetSamSelVal();

	int m_vTemplate;			// ������ ���������� "������� �����������"
	BOOL m_vUseTemplate;		// ������������� �������� "�������� ������������� �������"
	VModSam NamesSmpl;			// ������ ���� �������� ��������
	VModSam NamesCalib;			// ������ ���� ��������, ��������� ��� ����������
	VModSam NamesValid;			// ������ ���� ��������, ��������� ��� ���������
	int m_vProcValid;			// ������� ��� ���������

	int m_vLowSpec;				// ��������� �������� "������ ������� ������������� ���������"
	int m_vUpperSpec;			// ��������� �������� "������� ������� ������������� ���������"
	int m_vSpType;				// ������ ���������� "���� �������"
	VInt m_vPreproc;			// ������ ��������� �������������
	double m_vMSD;				// ��������� �������� "���������� ��� �����������"
	double m_vLowCalib;			// ��������� �������� "������ �������� ��������� �����������"
	double m_vUpperCalib;		// ��������� �������� "������� �������� ��������� �����������"
	double m_vCorrB;			// ��������� �������� "�������� ����������� b"
	double m_vCorrA;			// ��������� �������� "�������� ����������� a"

	ItModPar CurParam;			// ��������� �� ������� ����������
	int LimSpecRangeMin;		// ������ ������ ������������� ���������
	int LimSpecRangeMax;		// ������� ������ ��� ������������� ���������
	double MinValue;			// ������ ������ ��������� �����������
	double MaxValue;			// ������� ������ ��������� �����������

	int m_vNComp;				// ��������� �������� "����� ������� ���������"
	double m_vDimHiper;			// ��������� �������� "������ ���������"
	int m_vNHarm;				// ��������� �������� "����� ��������"
	double m_vKfnl;				// ��������� �������� "����������� ������������"
	double m_vMaxMah;			// ��������� �������� "���������� ���������� ������������" ��� qlt
	double m_vMaxMah2;			// ��������� �������� "���������� ���������� ������������" ��� qnt
	double m_vMaxMSD;			// ��������� �������� "���������� ��� �����������"
	int m_vModType;				// ������ ���������� "���� ������"
	int m_vHSOType;				// ������ ���������� "���� HSO-������"
	BOOL m_vUseSECV;			// ������������� �������� "�������� ������������ �� SECV"

	VDbl PointsAll;				// ������ ���� ������������ �����
	SInt PointsExc;				// ������ ����������� ������������ �����
	int NAllPoints;				// ����� ����� ������������ �����
	int NMaxComp;				// ����������� ���������� ����� ������� ���������

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();						// ������������� �������
	virtual void OnOK()
	{};
	virtual void OnCancel();							// ��������� ������� "�������� ���� � ����� �� �������"

	void OnAccept();												// ������� ������ � ����� �� �������
	afx_msg void OnTabSelchange(NMHDR* pNMHDR, LRESULT* pResult);	// ��������� ������� "������� �� ������ ��������"

	afx_msg void OnAnTypeChange();		// ��������� ������� "�������� ��� �������"
	afx_msg void OnParamAdd();			// ��������� ������� "�������� ����������"
	afx_msg void OnParamAddAll();		// ��������� ������� "�������� ��� ����������"
	afx_msg void OnParamRemove();		// ��������� ������� "������� ����������"
	afx_msg void OnParamRemoveAll();	// ��������� ������� "������� ��� ����������"
	afx_msg void OnTransAdd();			// ��������� ������� "�������� �����"
	afx_msg void OnTransRemove();		// ��������� ������� "������� �����"
	afx_msg void OnTransRemoveAll();	// ��������� ������� "������� ��� ������"

	afx_msg void OnAddCalib();			// ��������� ������� "�������� ������� � ������ ��� �����������"
	afx_msg void OnAddAllCalib();		// ��������� ������� "�������� ��� ������� � ������ ��� �����������"
	afx_msg void OnRemoveCalib();		// ��������� ������� "������� ������� ��� ������ ��� �����������"
	afx_msg void OnRemoveAllCalib();	// ��������� ������� "������� ��� ������� �� ������ ��� �����������"
	afx_msg void OnShowSpecCalib();		// ��������� ������� "�������� �������"
	afx_msg void OnPlotCalib();			// ��������� ������� "�������� �����������"
	afx_msg void OnAddValid();			// ��������� ������� "�������� ������� � ������ ��� ���������"
	afx_msg void OnAddAllValid();		// ��������� ������� "�������� ��� ������� � ������ ��� ���������"
	afx_msg void OnRemoveValid();		// ��������� ������� "������� ������� ��� ������ ��� ���������"
	afx_msg void OnRemoveAllValid();	// ��������� ������� "������� ��� ������� �� ������ ��� �"
	afx_msg void OnShowSpecValid();		// ��������� ������� "�������� �������"
	afx_msg void OnPlotValid();			// ��������� ������� "�������� �����������"
	afx_msg void OnSamplesListsColumnClick(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnOptim();				// ��������� ������� "��������� ������� ����������� ������"
	afx_msg void OnTemplate();			// ��������� ������� "������������� ������� �����������"
	afx_msg void OnUseTemplate();		// ��������� ������� "���������� ������� ������������� �������"
	afx_msg void OnTemplateChange();
	afx_msg void OnShowSpectra();
	afx_msg void OnAutoChoose();

	afx_msg void OnChangeLowSpecRange();	// ��������� ������� "�������� ������ ������� ������������� ���������"
	afx_msg void OnChangeUpperSpecRange();	// ��������� ������� "�������� ������� ������� ������������� ���������"
	afx_msg void OnPreproc0();			// ��������� ������� "�������� 1-� �������������"
	afx_msg void OnPreproc1();			// ��������� ������� "�������� 2-� �������������"
	afx_msg void OnPreproc2();			// ��������� ������� "�������� 3-� �������������"
	afx_msg void OnPreproc3();			// ��������� ������� "�������� 4-� �������������"
	afx_msg void OnPreproc4();			// ��������� ������� "�������� 5-� �������������"
	afx_msg void OnPreproc5();			// ��������� ������� "�������� 6-� �������������"
	afx_msg void OnPreproc6();			// ��������� ������� "�������� 7-� �������������"
	afx_msg void OnParamsCListSelChange(NMHDR* /*pNMHDR*/, LRESULT *pResult); // ��������� ������� "�������� �������� � ������� ������� �����������"
	afx_msg	void OnMaxMSDCalibChange();
	afx_msg	void OnLowLimCalibChange();
	afx_msg	void OnUpperLimCalibChange();
	afx_msg	void OnCorrCalibBChange();
	afx_msg	void OnCorrCalibAChange();

	afx_msg void OnModelTypeChange();	// ��������� ������� "�������� ��� ������"
	afx_msg void OnModelHSOTypeChange();// ��������� ������� "�������� ��� HSO-������"
	afx_msg void OnAddExc();			// ��������� ������� "��������� ������������ �����"
	afx_msg void OnAddAllExc();			// ��������� ������� "��������� ��� ������������ �����"
	afx_msg void OnRemoveExc();			// ��������� ������� "������� ������������ ����� �� ������ �����������"
	afx_msg void OnRemoveAllExc();		// ��������� ������� "������� ��� ������������ ����� �� ������ �����������"
	afx_msg void OnCalculate();			// ��������� ������� "���������� ������"

	afx_msg void OnHelp();				// ��������� ������� "������� �������"

	void ChangeCalibSettings();

	void FillListParamSel(int newsel = -1);	// ��������� ������� �� ������� ���� ��������� �����������

	void GetTransValList();					// �������� ������ ��������� �������
	CString GetSampleTransName(ModelSample *pMSam);	// �������� ��� ������� � ������ ���������
	void FillListTransAll(int newsel = -1);	// ��������� ������� �� ������� ���� ��������� �������
	void FillListTransSel(int newsel = -1);	// ��������� ������� �� ������� ���� ��������� �������

	void CCreateModelDlg::SetBasicTrans(CString Name);
	void RecreateSmplLists(bool resetpar = true);	// ��������� ������ �������� ��������
	void CreateSmplAll();					// ������� ������ �������� ��������
	void RecreateSmplCalibValid();			// ����������� ������ �������� ��� ����������� � ���������
	void FillListSmpl(int newsel, bool bFromScratch = true);			// ��������� ������� �� ������� �������� ��������
	void FillListCalib(int newsel, bool bFromScratch = true);			// ��������� ������� �� ������� �������� ��� �����������
	void FillListValid(int newsel, bool bFromScratch = true);			// ��������� ������� �� ������� �������� ��� ���������
	void FillTemplates();					// ��������� ������ �������� �����������

	void SmplSetSortIcons();
	void CalibSetSortIcons();
	void ValidSetSortIcons();

	void FillListsPreproc(int iCb);				// ��������� ������ ��������� �������� �������������
	void RefillParamCList(bool reset = true);	// ������������� ������� ������� �����������
	void FillParamCFields();					// ��������� ���� ����� ������� �����������

	void RecreateSpecPoints();					// ����������� ������ ������������ �����
	void GetSpecPoints();						// ���������� ������������ ����� ������
	void FillSpecAllList();						// ��������� ������� �� ������� ������������ �����
	void FillListExc(int newsel = -1);			// ��������� ������� �� ������� ����������� ������������ �����
	void FillHSOFields();						// ��������� ���� ��� �������������� ���������� HSO-������

	bool IsSampleAvailable(CSampleData* data);	// ��������� ���������� �������
	int GetNUsesSpectrumAll();					// ���������� ����� ����� �������� ��� �������� ��������
	int GetNUsesSpectrumCalib();				// ���������� ����� ����� �������� ��� �������������� ��������
	int GetNUsesSpectrumValid();				// ���������� ����� ����� �������� ��� ������������� ��������
	int GetNSamplesForCalib();					// ���������� ����� ����� �������������� ��������
	int GetNSpectraForCalib();						// ���������� ����� ����� �������� ��������
	int GetMaxSpectraInNamesCalib();			// ���������� ���������� ����� �������� � ����� �������������� �������
	void PrepareSort(ModelSample& ModSam1);
	void SortNamesSmpl();						// ����������� ������ �������� ��������
	void SortNamesCalib();						// ����������� ������ �������������� ��������
	void SortNamesValid();						// ����������� ������ �������� ��� ���������

	void OnPreproc(int icb, int sel);	// �������� �������������
	int FindPreproc(int val);			// �������� ���������� ����� �������������
	int GetSelByVal(int icb, int val);	// �������� ��� ������������� ������ � ������ ��������� ��������
	int GetValBySel(int icb, int sel);	// �������� ������������� �� ������� � ������ ��������� ��������
	void CalcMinMaxCalibValue();		// ���������� ���������� � ����������� ���������� ������� ����������� ������ ��� �������� ����������
	void RecalcMaxComp();				// ����������� ����������� ���������� ����� ������� ���������

	int GetNUseSpecPoints();			// ���������� ����� ���������� ������������ �����
	int GetNRealExcPoints();			// ���������� ����� ����������� ����� ������ �������� ���������
	int GetNFactor();					// ���������� ����� ��������

	void ShowFields(int act = -1);		// ������� �� ����� ��������, ��������������� �������� ��������
	void EnableFields();				// ���������� �����������/������������� ��������� ����� ������

	void FillAllPages();				// ��������� ��� ��������
	void ReinitData();					// �������� ��������������� ������

	void InitData();					// ������������� ������
	bool CheckData();					// ��������� ��������� ������
	bool AcceptData();					// ������� ��������� ������

public:

	bool IsPointExc(int ind);
	bool IsSampleUse(int ind);
	int GetTransNum(ModelSample *pMSam);
	bool IsParamUse(int ind);
	bool IsTransNotVal(int ind);
	bool IsAnTypeQlt();

protected:

	DECLARE_MESSAGE_MAP()
};

#endif
