#ifndef _LOAD_DB_WAIT_DLG_H_
#define _LOAD_DB_WAIT_DLG_H_

#pragma once

#include "WaitingDlg.h"


///////////////////////////////////////////////////////////////////////////////////////////////////
// ������ ��������� �������� ��������/�������� ���� ������

// ���������� ������ ������ �������

const int C_MODE_LOADDB_LOAD	= 0;
const int C_MODE_LOADDB_CLOSE	= 1;

class CLoadDBWaitDlg : public CWaitingDlg
{
	DECLARE_DYNAMIC(CLoadDBWaitDlg)

public:

	CLoadDBWaitDlg(CTreeCtrl* tree, int mode = C_MODE_LOADDB_LOAD, CWnd* pParent = NULL);	// �����������
	virtual ~CLoadDBWaitDlg();																// ����������

	enum { IDD = CWaitingDlg::IDD };	// ������������� �������

protected:

	CTreeCtrl* pTree;

	int Mode;

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();						// ������������� �������

	virtual void OK();				// ��������� �������� ����� �� �������
	virtual void Stop();			// ��������� ����������� ����� �� �������
	virtual void Work();			// ������ �������� �������

	DECLARE_MESSAGE_MAP()
};

#endif
