#include "stdafx.h"

#include "ShowSpectraDlg.h"
#include "MainFrm.h"


static CShowSpectraDlg* pShow = NULL;

static int LbClickCallback(UINT /*nFlags*/, CPoint point, void *pData)
{
	CRect rect;
	rect.left = point.x - 2;
	rect.right = point.x + 2;
	rect.top = point.y - 2;
	rect.bottom = point.y + 2;

	if(pShow != NULL)
	{
		pShow->ShowSpecInfo(&rect, false);
		pShow->ShowCoord(&point);
	}

	return 1;
}

static int LbUnClickCallback(UINT /*nFlags*/, CPoint /*point*/, void *pData)
{
	if(pShow != NULL)
		pShow->RemoveInfo();

	return 1;
}

static int RbClickCallback(UINT /*nFlags*/, CPoint /*point*/, void *pData)
{
	return 1;
}

static int RbUnClickCallback(UINT /*nFlags*/, CPoint /*point*/, void *pData)
{
	if(pShow != NULL)
		pShow->SetStatusMenu();

	return 1;
}

IMPLEMENT_DYNAMIC(CShowSpectraDlg, CDialog)

CShowSpectraDlg::CShowSpectraDlg(CShowSpectra *data, int mode, CWnd* pParent)
	: CDialog(CShowSpectraDlg::IDD, pParent)
{
	pShow = this;

	Data = data;
	Mode = mode;

	m_bShowMarkers = false;

	if(int(Data->ShowData.size()) > 0)
		selS.push_back(Data->ShowData[0].Name);

	posInfo = NULL;
	posCoord = NULL;

	pMenu1 = new CMenu;
	pMenu1->CreatePopupMenu();
}

CShowSpectraDlg::~CShowSpectraDlg()
{
	Graphs.clear();

	pMenu1->DestroyMenu();
	delete pMenu1;
}

void CShowSpectraDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_LIST_SEL, m_list);
	DDX_Control(pDX, IDC_SHOW_MARKERS, m_checkShowMarkers);
}

BEGIN_MESSAGE_MAP(CShowSpectraDlg, CDialog)

	ON_COMMAND(C_SHOWSPEC_MENU_PREV, OnUserMenuPrev)
	ON_COMMAND(C_SHOWSPEC_MENU_NEXT, OnUserMenuNext)
	ON_COMMAND(C_SHOWSPEC_MENU_FULL, OnUserMenuFull)
	ON_COMMAND(C_SHOWSPEC_MENU_AUTO, OnUserMenuAuto)
	ON_COMMAND(C_SHOWSPEC_MENU_LEFT, OnUserMenuLeft)
	ON_COMMAND(C_SHOWSPEC_MENU_RIGHT, OnUserMenuRight)
	ON_COMMAND(C_SHOWSPEC_MENU_GRID, OnUserMenuGrid)

	ON_CBN_SELCHANGE(IDC_LIST_SEL, OnSpectraListSelChange)

	ON_BN_CLICKED(IDHELP, OnHelp)
	ON_BN_CLICKED(IDC_SETTINGS, OnSettings)

	ON_BN_CLICKED(IDC_SHOW_MARKERS, &CShowSpectraDlg::OnBnClickedShowMarkers)
END_MESSAGE_MAP()

BOOL CShowSpectraDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_checkShowMarkers.SetCheck(m_bShowMarkers);
	m_checkShowMarkers.SetWindowText(ParcelLoadString(STRID_SHOW_SPECTRAL_POINTS));

	CreateChart();
	FillList();

	CString Hdr = ParcelLoadString(STRID_SHOW_SPECTRA_HDR);
	SetWindowText(Hdr);

	GetDlgItem(IDCANCEL)->SetWindowText(ParcelLoadString(STRID_SHOW_SPECTRA_EXIT));
	GetDlgItem(IDHELP)->SetWindowText(ParcelLoadString(STRID_SHOW_SPECTRA_HELP));
	GetDlgItem(IDC_SETTINGS)->SetWindowText(ParcelLoadString(STRID_SHOW_SPECTRA_SETTINGS));

	GetDlgItem(IDC_STATIC_2)->ShowWindow(SW_HIDE);

	GetDlgItem(IDHELP)->EnableWindow(pFrame->IsExistHelp());

	return true;
}

void CShowSpectraDlg::OnSpectraListSelChange()
{
	int sel = m_list.GetCurSel();
	if(sel < 0 || sel >= int(Data->ShowData.size()))
		return;

	int nCount = m_list.GetSelCount();
	CArray<int,int> arySel;
	arySel.SetSize(nCount);
	m_list.GetSelItems(nCount, arySel.GetData());

	int Ind = -1, i, nsel = m_list.GetSelCount();
	selS.clear();
	for(i=0; i<nsel; i++)
	{
		Ind = arySel.GetAt(i);
		selS.push_back(Data->ShowData[Ind].Name);
	}

	ShowSelection();
}

void CShowSpectraDlg::OnSettings()
{
	CPlotSettingsDlg dlg(Data, Mode, this);
	if(dlg.DoModal() != IDOK)
		return;

	FillList();
}

void CShowSpectraDlg::OnUserMenuPrev()
{
	pChart->GetWnd()->PostMessage(WM_COMMAND, LC_POPUP_GO_BACK, 0);
}

void CShowSpectraDlg::OnUserMenuNext()
{
	pChart->GetWnd()->PostMessage(WM_COMMAND, LC_POPUP_GO_FORWARD, 0);
}

void CShowSpectraDlg::OnUserMenuFull()
{
	pChart->GetWnd()->PostMessage(WM_COMMAND, LC_POPUP_FULL_SCREEN, 0);
}

void CShowSpectraDlg::OnUserMenuAuto()
{
	pChart->GetWnd()->PostMessage(WM_COMMAND, LC_POPUP_AUTOY, 0);
}

void CShowSpectraDlg::OnUserMenuLeft()
{
	pChart->GetWnd()->PostMessage(WM_COMMAND, LC_POPUP_XLEFT, 0);
}

void CShowSpectraDlg::OnUserMenuRight()
{
	pChart->GetWnd()->PostMessage(WM_COMMAND, LC_POPUP_XRIGHT, 0);
}
/*
void CShowSpectraDlg::OnUserMenuLegend()
{
	bool btmp;
	pChart->IsLegendEnabled(&btmp);
	pChart->ShowLegend(!btmp);
}
*/
void CShowSpectraDlg::OnUserMenuGrid()
{
	bool btmp, btmp2;
	pChart->IsGridEnabled(&btmp, &btmp2);
	pChart->ShowGrid(!btmp, !btmp);
}

void CShowSpectraDlg::FillList()
{
	Data->CreateShowData();

	m_list.ResetContent();

	ItSSD it;
	for(it=Data->ShowData.begin(); it!=Data->ShowData.end(); ++it)
		m_list.AddString(it->Name);

	CString s;
	s.Format(ParcelLoadString(STRID_SHOW_SPECTRA_NUMSPECTRA), int(Data->ShowData.size()));
	GetDlgItem(IDC_STATIC_1)->SetWindowText(s);

	CreateGraphs();

	ItStr itS;
	for(itS=selS.begin(); itS!=selS.end(); ++itS)
	{
		ItSSD it;
		int j;
		for(j=0, it=Data->ShowData.begin(); it!=Data->ShowData.end(); ++it, j++)
			if(!it->Name.Compare(*itS))
			{
				m_list.SetSel(j, true);
				break;
			}
	}

	ShowSelection();
}

void CShowSpectraDlg::CreateGraphs()
{
	vector<CLumChartDataSet>::iterator it2;
	for(it2=Graphs.begin(); it2!=Graphs.end(); ++it2)
		pChart->RemoveGraph(it2->GetPos());
	Graphs.clear();

	int k = 0;
	ItSSD it;
	int N = Data->GetNPnt();
	for(it=Data->ShowData.begin(); it!=Data->ShowData.end(); ++it)
	{
		CLumChartDataSet Graph(Data->GetNPnt(), it->x, it->y);
		Graph.SetName(it->Name);
		Graph.SetInterpolation(LCF_CONNECT_SPLINE);
		Graph.SetColor(it->Color);
		Graph.SetMarkerColor(it->Color);

		int iMT = m_bShowMarkers ? (k%5)+1 : LCF_MARKERS_NONE;
		Graph.SetMarkerType(iMT);


		pChart->AddGraph(&Graph, true);
		Graphs.push_back(Graph);
		k++;
	}

	ShowSelection();
}

void CShowSpectraDlg::ShowSelection()
{
	int i;
	ItSSD it;
	for(i=0, it=Data->ShowData.begin(); it!=Data->ShowData.end(); ++it, i++)
	{
		CLumChartDataSet* pGraph = &Graphs[i];
		ItStr itf = find(selS.begin(), selS.end(), it->Name);
		if(itf != selS.end())
		{
			pGraph->SetWidth(2);
			m_list.SetSel(i, true);
		}
		else
		{
			pGraph->SetWidth(1);
			m_list.SetSel(i, false);
		}

		pChart->UpdateGraph(pGraph->GetPos(), pGraph, false);
	}

	pChart->RedrawWindow();
}

void CShowSpectraDlg::CreateChart()
{
	CRect RectGraph;
	GetDlgItem(IDC_STATIC_2)->GetWindowRect(RectGraph);

	pChart = new CLumChart(RectGraph, this, LCF_RECT_SCREEN);
	pChart->SetAxesNames(ParcelLoadString(STRID_SHOW_SPECTRA_GRAPH_AXISPNT),
		ParcelLoadString(STRID_SHOW_SPECTRA_GRAPH_AXISSKIP));

	CreateGraphs();

	COLORREF bg = GetSysColor(COLOR_3DFACE);
	pChart->SetBgOutsideColor(bg, cColorBlack);
	pChart->SetMargins(45, 30, 15, 50);
	pChart->SetSelFrameProps(RGB(0, 0, 255), 2);
	pChart->SetHighlightMode(false);

	pChart->SetSolidAxisHorzAt(0.);
	pChart->EnableSolidAxisHorz(true);

	pChart->ShowLegend(false);
	pChart->EnableUpperLegend(false);
	pChart->EnableCoordLegend(false);

	pChart->SetUserCallback(LCF_CB_LBDOWN, LbClickCallback);
	pChart->SetUserCallback(LCF_CB_LBUP, LbUnClickCallback);
	pChart->SetUserCallback(LCF_CB_RBDOWN, RbClickCallback);
	pChart->SetUserCallback(LCF_CB_RBUP, RbUnClickCallback);
	pChart->EnableUserCallback(LCF_CB_LBDOWN, true);
	pChart->EnableUserCallback(LCF_CB_LBUP, true);
	pChart->EnableUserCallback(LCF_CB_RBDOWN, true);
	pChart->EnableUserCallback(LCF_CB_RBUP, true);

	pChart->SetSelFrameProps(RGB(0, 0, 255), 2);

	BuildUserPopupMenu();
	pChart->SetUserPopupMenu(this, pMenu1);
	pChart->EnableUserPopupMenu(true);
}

void CShowSpectraDlg::ShowSpecInfo(CRect* rect, bool isFrame)
{
	if(!isFrame)
	{
		CPoint pnt(int((rect->left + rect->right) / 2), int((rect->top + rect->bottom) / 2));
		int resfind = -1;
		pChart->IsPointInSelRect(pnt, &resfind);
		if(resfind == 1)
			return;
	}

	vector<CLCULString> strings;

	int i;
	bool fnd = false;
	bool make_sel = false;
	VStr sel_names;
	vector<CLumChartDataSet>::iterator it;
	for(i=0, it=Graphs.begin(); it!=Graphs.end(); ++it, i++)
	{
		bool res = false;
		pChart->IsGraphInRect(it->GetPos(), (*rect), &res);
		if(!res)
			continue;

		fnd = true;

		CString Name = Data->ShowData[i].Name;

		sel_names.push_back(Name);
		if(!make_sel)
		{
			ItStr itf = find(selS.begin(), selS.end(), Name);
			if(itf == selS.end())
				make_sel = true;
		}

		if(!isFrame)
		{

			CLCULString NewString;
			NewString.SetText(Name);
			NewString.SetLine(Data->ShowData[i].Color, PS_SOLID, 1);
			NewString.EnableLine(true);

			strings.push_back(NewString);
		}
	}
	if(!fnd)
		return;

	ItStr it2;
	for(it2=sel_names.begin(); it2!=sel_names.end(); ++it2)
	{
		ItStr itf = find(selS.begin(), selS.end(), (*it2));
		if(itf != selS.end() && !make_sel)
			selS.erase(itf);
		if(itf == selS.end() && make_sel)
			selS.push_back((*it2));
	}

	if(!isFrame)
	{
		CLCUL legend;
		legend.MoveTo(0, 0);
		legend.Resize(100, 20);
		legend.SetStrings(strings);

		legend.SetFillColor(cColorGraphNorm);
		legend.SetTextColor(cColorBlack);

		legend.Enable(true);
		legend.EnableMoveToFit(true);
		legend.EnableAutoWidth(true);
		legend.EnableAutoHeight(true);

		posInfo = pChart->AddUserLegend(legend);
	}

	ShowSelection();
}

void CShowSpectraDlg::ShowCoord(CPoint* pnt)
{
	vector<CLCULString> strings;

	CLCULString NewString;

	CString str;
	double X = 0, Y = 0;
	pChart->GetViewPortXY(*pnt, &X, &Y);
	str.Format(ParcelLoadString(STRID_SHOW_SPECTRA_GRAPH_INFO_COORD), Y, X);
	NewString.SetText(str);
	strings.push_back(NewString);

	CRect RectGraph;
	GetDlgItem(IDC_STATIC_2)->GetWindowRect(RectGraph);

	CLCUL legend;
	legend.MoveTo(0, RectGraph.Height() - 20);
	legend.Resize(100, 20);
	legend.SetStrings(strings);

	legend.SetFillColor(cColorGraphNorm);
	legend.SetTextColor(cColorBlack);

	legend.Enable(true);
	legend.EnableMoveToFit(true);
	legend.EnableAutoWidth(true);
	legend.EnableAutoHeight(true);

	posCoord = pChart->AddUserLegend(legend);
}

void CShowSpectraDlg::RemoveInfo()
{
	if(posInfo != NULL)
	{
		pChart->RemoveUserLegend(posInfo);
		posInfo = NULL;
	}
	if(posCoord != NULL)
	{
		pChart->RemoveUserLegend(posCoord);
		posCoord = NULL;
	}
}

void CShowSpectraDlg::BuildUserPopupMenu()
{
	pMenu1->AppendMenu(MF_STRING, C_SHOWSPEC_MENU_PREV, ParcelLoadString(STRID_SHOW_SPECTRA_GRAPH_MENU_PREV));
	pMenu1->AppendMenu(MF_STRING, C_SHOWSPEC_MENU_NEXT, ParcelLoadString(STRID_SHOW_SPECTRA_GRAPH_MENU_NEXT));
	pMenu1->AppendMenu(MF_SEPARATOR);
	pMenu1->AppendMenu(MF_STRING, C_SHOWSPEC_MENU_FULL, ParcelLoadString(STRID_SHOW_SPECTRA_GRAPH_MENU_FULL));
	pMenu1->AppendMenu(MF_STRING, C_SHOWSPEC_MENU_AUTO, ParcelLoadString(STRID_SHOW_SPECTRA_GRAPH_MENU_AUTO));
	pMenu1->AppendMenu(MF_SEPARATOR);
	pMenu1->AppendMenu(MF_STRING, C_SHOWSPEC_MENU_LEFT, ParcelLoadString(STRID_SHOW_SPECTRA_GRAPH_MENU_LEFT));
	pMenu1->AppendMenu(MF_STRING, C_SHOWSPEC_MENU_RIGHT, ParcelLoadString(STRID_SHOW_SPECTRA_GRAPH_MENU_RIGHT));
	pMenu1->AppendMenu(MF_SEPARATOR);
	pMenu1->AppendMenu(MF_STRING, C_SHOWSPEC_MENU_GRID, ParcelLoadString(STRID_SHOW_SPECTRA_GRAPH_MENU_GRID));
}

void CShowSpectraDlg::SetStatusMenu()
{
	UINT nFlags;
	bool btmp = false, btmp2 = false;

	pChart->IsGoBackAvailable(&btmp);
	nFlags = (btmp ? MF_ENABLED : (MF_DISABLED|MF_GRAYED)) | MF_BYCOMMAND;
	pMenu1->EnableMenuItem(C_SHOWSPEC_MENU_PREV, nFlags);

	pChart->IsGoForwardAvailable(&btmp);
	nFlags = (btmp ? MF_ENABLED : (MF_DISABLED|MF_GRAYED)) | MF_BYCOMMAND;
	pMenu1->EnableMenuItem(C_SHOWSPEC_MENU_NEXT, nFlags);

	pChart->IsGridEnabled(&btmp, &btmp2);
	nFlags = (btmp) ? MF_CHECKED : MF_UNCHECKED;
	pMenu1->CheckMenuItem(C_SHOWSPEC_MENU_GRID, nFlags);
}

void CShowSpectraDlg::OnHelp()
{
	int IdHelp = DLGID_SHOW_SPECTRA;

	pFrame->ShowHelp(IdHelp);
}

void CShowSpectraDlg::OnBnClickedShowMarkers()
{
	m_bShowMarkers = !m_bShowMarkers;
	m_checkShowMarkers.SetCheck(m_bShowMarkers);
	FillList();
}
