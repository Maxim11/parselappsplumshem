#ifndef _PARCEL_CONST_TYPES_H_
#define _PARCEL_CONST_TYPES_H_

#pragma once

// ����� ������

//const int C_ABOUT_V1	= 1;
//const int C_ABOUT_V2	= 2;
//const int C_ABOUT_V3	= 139;

// ����� ��������� ������

const CString cFini = _T("Parcel.ini");
const CString cFhelpRU = _T("helpParcel_RU.chm");
const CString cFhelpEN= _T("helpParcel_EN.chm");

// ��������� ���� �������

const int C_DEN_NTYPES		= 11;	// ����� ����� ��������
const int C_DEV_TYPE_10		= 0;	// �������� ��-10
const int C_DEV_TYPE_10M	= 1;	// �������� ��-10�
const int C_DEV_TYPE_40		= 2;	// �������� ��-40
const int C_DEV_TYPE_02		= 3;	// �������� ��-02
const int C_DEV_TYPE_02M	= 4;	// �������� ��-02�
const int C_DEV_TYPE_08		= 5;	// �������� ��-08
const int C_DEV_TYPE_20		= 6;	// �������� ��-20
const int C_DEV_TYPE_12		= 7;	// �������� ��-12
const int C_DEV_TYPE_100	= 8;	// �������� ��-100
const int C_DEV_TYPE_IMIT	= 9;	// ����� ������ (��������)
const int C_DEV_TYPE_12M	= 10;	// �������� ��-12�

// ��������� ���� ����� ��������� �������

const int C_MEASURE_ONE		= 0;	// ���� ��� �� ������
const int C_MEASURE_TWO		= 1;	// ������ �� ������
const int C_MEASURE_EVERY	= 2;	// ���� ��� �� �����

// ��������� ���� ����������

const int C_APODIZE_RECTAN = 0;		// �������������
const int C_APODIZE_NBWEAK = 1;		// �-� ������
const int C_APODIZE_NBMEAN = 2;		// �-� �������
const int C_APODIZE_NBSTRN = 3;		// �-� �������
const int C_APODIZE_BESSEL = 4;		// ���������

// ��������� ���� �������

const int C_ANALYSIS_QNT = 0;		// �������������� ������
const int C_ANALYSIS_QLT = 1;		// ������������ ������

// ��������� ���� �������

const int C_TRANS_TYPE_OWN		= 0;
const int C_TRANS_TYPE_NORMAL	= 1;
const int C_TRANS_TYPE_NOTCORR	= 2;
const int C_TRANS_TYPE_SST		= 3;

// ��������� ���� ������

const int C_MOD_MODEL_PLS = 0;			// PLS-������
const int C_MOD_MODEL_PCR = 1;			// PCR-������
const int C_MOD_MODEL_HSO = 2;			// HSO-������
const int C_MOD_MODEL_PCA = 3;			// PCA-������

// ������������� HSO-������

const int C_MOD_MODEL_HSO_LPM = 0;		// �������� �������� ������
const int C_MOD_MODEL_HSO_LHM = 1;		// �������� ������������� ������
const int C_MOD_MODEL_HSO_NLPM = 2;		// ���������� �������� ������
const int C_MOD_MODEL_HSO_NLHM = 3;		// ���������� ������������� ������

// ��������� ���� �������

const int C_MOD_SPECTRUM_TRA = 0;		// �����������
const int C_MOD_SPECTRUM_ABS = 1;		// ����������
const int C_MOD_SPECTRUM_BOTH = 2;		// ��� �����������: ������� ����� ����� �������

// ��������� ���� �������������

const int C_PREPROC_0 = 0;				// ��� �������������
const int C_PREPROC_A = 1;				// ���������� �� �������
const int C_PREPROC_M = 2;				// ���������������� ��������
const int C_PREPROC_D = 3;				// �������������� ����������
const int C_PREPROC_C = 4;				// ����������������� ���������
const int C_PREPROC_N = 5;				// ���������� �� ���
const int C_PREPROC_B = 6;				// ������������ ������� �����
const int C_PREPROC_1 = 7;				// 1-� �����������
const int C_PREPROC_2 = 8;				// 2-� �����������

const TCHAR cPreprocs[9] = { ' ', 'A', 'M', 'D', 'C', 'N', 'B', '1', '2' };

// ��������� ���� ��������� ��� ��������

const int C_REGRESSION_LIN = 0;

// ��������� �������� ������ ����������� ������� ������

const int C_CRIT_SEC		= 0;
const int C_CRIT_R2SEC		= 1;
const int C_CRIT_SECV		= 2;
const int C_CRIT_R2SECV		= 3;
const int C_CRIT_F			= 4;
const int C_CRIT_SEV		= 5;
const int C_CRIT_ERR		= 6;
const int C_CRIT_SDV		= 7;
const int C_CRIT_R2SEV		= 8;
const int C_CRIT_K1			= 9;
const int C_CRIT_K2			= 10;
const int C_CRIT_K3			= 11;
const int C_CRIT_K4			= 12;
const int C_CRIT_K5			= 13;
const int C_CRIT_K6			= 14;
const int C_CRIT_K7			= 15;
const int C_CRIT_SECSECV	= 16;

// ��������� ��������������� ��������

const int C_OPTIM_TEST_EXCLUDE	= 0;
const int C_OPTIM_TEST_OUTS		= 1;
const int C_OPTIM_TEST_HIPER	= 2;

// ��������� ���� ����������

const int C_SORT_SAMPLE		= 0;			//	sample name
const int C_SORT_TIME		= 1;			//	last spec time
const int C_SORT_PARAM		= 2;			//	param 

// ��������� ����������� ����������

const int C_SORT_DIR_UP		= 0;
const int C_SORT_DIR_DOWN	= 1;

// ��������� ���� ����������� �������

const int C_PRJ_COPY_ONLYSETTINGS	= 0;
const int C_PRJ_COPY_WITHPARAMS		= 1;
const int C_PRJ_COPY_WITHSAMPLES	= 2;
const int C_PRJ_COPY_WITHSPECTRA	= 3;
const int C_PRJ_COPY_WITHTRANS		= 4;
const int C_PRJ_COPY_WITHALL		= 5;
const int C_PRJ_COPY_FORCHANGE		= 6;

// ��������� ���� �������� HQO-������� ��� �����������

const int C_OPTIM_HQO_PNT	= 0;
const int C_OPTIM_HQO_HARM	= 1;
const int C_OPTIM_HQO_HALL	= 2;

const int C_OPTIM_HQO_LIN	= 0;
const int C_OPTIM_HQO_NLN	= 1;
const int C_OPTIM_HQO_LALL	= 2;

// ���� ������� �������� ��� ���������

const int C_TRANS_SPECNUM_MEAN		= 1;
const int C_TRANS_SPECNUM_MEANMSC	= 2;
const int C_TRANS_SPECNUM_MEANSTD	= 3;

// ��������� ���� �������� ��� �����������

const int C_SPECTRA_TYPE_SIMPLE	= 0x01;
const int C_SPECTRA_TYPE_CALIB	= 0x02;
const int C_SPECTRA_TYPE_VALID	= 0x04;
const int C_SPECTRA_TYPE_MODEL	= C_SPECTRA_TYPE_CALIB | C_SPECTRA_TYPE_VALID;
const int C_SPECTRA_TYPE_ALL	= C_SPECTRA_TYPE_SIMPLE | C_SPECTRA_TYPE_CALIB | C_SPECTRA_TYPE_VALID;

// ��������� ���� ����� ������ ������� ������������� �������

const int C_MTD_QLTFORM_MEAN	= 0;
const int C_MTD_QLTFORM_AND		= 1;
const int C_MTD_QLTFORM_OR		= 2;
const int C_MTD_QLTFORM_OPEN	= 3;
const int C_MTD_QLTFORM_CLOSE	= 4;

// ��������� ������ � �������� ������� ������������� �������

const int C_MTD_QLTFORM_ERR_OK				= 0;
const int C_MTD_QLTFORM_ERR_EMPTY			= 1;
const int C_MTD_QLTFORM_ERR_NOMODELS		= 2;
const int C_MTD_QLTFORM_ERR_NOTALLMODELS	= 3;
const int C_MTD_QLTFORM_ERR_MANYMODELS		= 4;
const int C_MTD_QLTFORM_ERR_DBLBR			= 5;
const int C_MTD_QLTFORM_ERR_DBLOPER			= 6;
const int C_MTD_QLTFORM_ERR_BADEND			= 7;

// ��������� ������ ��� �����������

const int C_CONVERT_ERROR_ZERONUM		= 1;
const int C_CONVERT_ERROR_BADNUM		= 2;
const int C_CONVERT_ERROR_WAVE			= 3;
const int C_CONVERT_ERROR_LOWLIM		= 4;
const int C_CONVERT_ERROR_UPPERLIM		= 5;
const int C_CONVERT_ERROR_RESOL			= 6;
const int C_CONVERT_ERROR_APODIZE		= 7;
const int C_CONVERT_ERROR_ZEROFILL		= 8;
const int C_CONVERT_ERROR_SAMPLE		= 9;
const int C_CONVERT_ERROR_REFDATA		= 10;
const int C_CONVERT_ERROR_NOSPEC		= 11;
const int C_CONVERT_ERROR_PARAM			= 12;
const int C_CONVERT_ERROR_NOTOPEN		= 13;
const int C_CONVERT_ERROR_SPECPNT		= 14;
const int C_CONVERT_ERROR_NOTALLDATA	= 15;
const int C_CONVERT_ERROR_MODTRANS		= 16;
const int C_CONVERT_ERROR_MODNOTQNT		= 17;
const int C_CONVERT_ERROR_MODNOPARAMS	= 18;
const int C_CONVERT_ERROR_MODNOCALIB	= 19;
const int C_CONVERT_ERROR_MODSAMPLE		= 20;
const int C_CONVERT_ERROR_MTDNOTQNT		= 21;
const int C_CONVERT_ERROR_MTDNOTOPEN	= 22;
const int C_CONVERT_ERROR_MTDNOPARAMS	= 23;
const int C_CONVERT_ERROR_EMPTYUNIT		= 24;
const int C_CONVERT_ERROR_MODREF		= 25;

const int C_TEMPLATE_QNT				= 0;
const int C_TEMPLATE_QLT				= 1;

const double C_LASER_WAVE	= 0.00006328;  // ������������� ����� ����� ������

const CString cEmpty	= _T("");
const CString cOwn		= _T("OWN");
const CString cSST		= _T("SST");
const CString cSortTime	= _T("SORT_TIME");
const CString cArtSam	= _T("Artificial sample: mean spectra");
const CString cWarn		= _T(" (!)");
const CString cStatStd	= _T("For Help, press F1");
const CString cFmt1		= _T("%1d");
const CString cFmt02	= _T("%02d");
const CString cFmt08	= _T("%08d");
const CString cFmt20	= _T("%2.0f");
const CString cFmt31	= _T("%3.1f");
const CString cFmt42	= _T("%4.2f");
const CString cFmt53	= _T("%5.3f");
const CString cFmt64	= _T("%6.4f");
const CString cFmt75	= _T("%7.5f");
const CString cFmt86	= _T("%8.6f");
const CString cFmt97	= _T("%9.7f");
const CString cFmtDate	= _T("%Y/%m/%d %H:%M:%S");
const CString cFmtDate2	= _T("%Y-%m-%d %H:%M");
const CString cFmt1_1	= _T("%1d(%1d)");
const CString cFmtAstStr = _T("*%s");
const CString cFmtS_S	= _T("%s, %s");

CString cGetDeviceTypeName(int type);
CString cGetMeasureTypeName(int type);
CString cGetApodizeTypeName(int type);
CString cGetAnalysisTypeName(int type);
CString cGetTransTypeName(int type);
CString cGetModelTypeName(int type);
CString cGetModelTypeNameShort(int type);
CString cGetModelHSOTypeName(int type);
CString cGetSpectrumTypeName(int type);
CString cGetSpectrumTypeNameShort(int type);
CString cGetPreprocName(int prep);
CString cGetCritName(int crit);
CString cGetOptimOperName(int oper);
CString cGetOptimHQOHorPName(int type); 
CString cGetOptimHQOLorNName(int type);
CString cGetPrjCopyName(int mode);
CString cGetSpectraTypeName(int type);

CString cGetSortFmtUp(CString name);
CString cGetSortFmtDown(CString name);

int cGetPreprocIntByChar(TCHAR ch);
TCHAR cGetPreprocCharByInt(int prep);
CString cGetPreprocAsStr(VInt* preps);
void cGetPreprocFromStr(CString str, VInt& preps, int fill = 0);
bool cIsPrepInStr(CString str, int prep);
bool cCheckPrepAlready(CString all, CString prep);

bool cIsParamNameReserved(CString name);
bool cIsBadSymbolInName(CString name);

bool cIsCritDerivated(int crit, int parent);
double cCalcCritK(int crit, double sec, double secv, double sev, double Ymin, double Ymax);
bool cCompareByCrit(int crit, double newval, double oldval); 
CString cGetCritFormat(int crit);

int cGetDeviceID0(int type);
int cGetDeviceTypeByID0(int id0);

CString cGetFormat(int dig);

const COLORREF cColorGraphNorm = RGB(255, 255, 239);
const COLORREF cColorSel = RGB(0, 0, 255);
const COLORREF cColorErr = RGB(255, 0, 0);
const COLORREF cColorPass = RGB(192, 192, 192);
const COLORREF cColorTest = RGB(0, 192, 192);
const COLORREF cColorBlack = RGB(0, 0, 0);
const COLORREF cColorGrey = RGB(127, 127, 127);

COLORREF cGetDefColor(int ind);

void cParcelDDX(CDataExchange* pDX, int nIDC, int& value);
void cParcelDDX(CDataExchange* pDX, int nIDC, double& value);
void cParcelDDX(CDataExchange* pDX, int nIDC, CString& value);
void cParcelDDV(CDataExchange* pDX, CString strerr, int value, int min, int max);
void cParcelDDV(CDataExchange* pDX, CString strerr, double value, double min, double max);

CString cGetConvertError(int ind);
CString cGetQltFormulaError(int err);

CString cGetHelpPageName(int IdDlg);

//CString cTmpMakeLowerRUS(CString str);

#endif
