#include "stdafx.h"

#include "ModelOptimization2.h"

#include <math.h>

int SortByCrit = C_CRIT_SEC;

bool SortOptimModels(OptimModel& OptMod1, OptimModel& OptMod2)
{
	return cCompareByCrit(SortByCrit, OptMod1.GetValue(SortByCrit), OptMod2.GetValue(SortByCrit));
}

bool SortOptimModelNums(PIntDbl& OptNum1, PIntDbl& OptNum2)
{
	return cCompareByCrit(SortByCrit, OptNum1.second, OptNum2.second);
}

OptimSpectrum2::OptimSpectrum2(CString sam, CString trans, int num, double ref, bool use, int indT)
{
	SampleName = sam;
	TransName = trans;
	Num = num;
	Ref = ref;
	Use = use;

	CString Tmp = SampleName;
	CString fmt;
	for(int i=0; i<indT; i++)
		fmt += '*';
	fmt += _T("%s-%02d");
	Name.Format(fmt, SampleName, Num); 
}

void OptimSpectrum2::Copy(OptimSpectrum2& data)
{
	SampleName = data.SampleName;
	TransName = data.TransName;
	Num = data.Num;
	Name = data.Name;
	Ref = data.Ref;
	Use = data.Use;
}

bool OptimSpectrum2::IsSample(CString sam, CString trans)
{
	return (!SampleName.Compare(sam) && !TransName.Compare(trans));
}

void OptimPreprocSet::Copy(OptimPreprocSet& data)
{
	pA = data.pA;
	pB = data.pB;
	pM = data.pM;
	pD = data.pD;
	pC = data.pC;
	pN = data.pN;
	p1 = data.p1;
	p2 = data.p2;
	pNo = data.pNo;
	MaxLen = data.MaxLen;
	UseBest = data.UseBest;
	NumBest = data.NumBest;

	Adds.clear();
	copy(data.Adds.begin(), data.Adds.end(), inserter(Adds, Adds.begin()));
}

CString OptimPreprocSet::GetAddsAsStr()
{
	if(Adds.size() <= 0)
		return cEmpty;

	CString str = cEmpty;

	ItStr it;
	for(it=Adds.begin(); it!=Adds.end(); ++it)
	{
		if((*it).IsEmpty())
			continue;

		if(!str.IsEmpty())
			str += _T("; ");
		str += (*it);
	}

	return str;
}

void OptimPreprocSet::SetAddsFromStr(CString str)
{
	Adds.clear();

	CString Str = str;
	Str.Trim();
	int i, N = int(_tcslen(Str));
	
	CString s = cEmpty;
	for(i=0; i<N; i++)
	{
		if(Str[i] == ';' || Str[i] == ',')
		{
			s.Trim();
			Adds.push_back(s);
			s = cEmpty;
			continue;
		}
		else
			s += Str[i];
	}
	if(!s.IsEmpty())
		Adds.push_back(s);
}

void OptimCritSet::Copy(OptimCritSet& data)
{
	bSEC = data.bSEC;
	bSECV = data.bSECV;
	bSECSECV = data.bSECSECV;
	bSEV = data.bSEV;
	bR2sec = data.bR2sec;
	bR2secv = data.bR2secv;
	bF = data.bF;
	bErr = data.bErr;
	bSDV = data.bSDV;
	bR2sev = data.bR2sev;
	bK1 = data.bK1;
	bK2 = data.bK2;
	bK3 = data.bK3;
	bK4 = data.bK4;
	bK5 = data.bK5;
	bK6 = data.bK6;
	bK7 = data.bK7;
}

void OptimCritSet::Empty()
{
	bSEC = bSECV = bSECSECV = bSEV = bR2sec = bR2secv = bF = bErr = bSDV = bR2sev = bK1 = bK2 = bK3 = bK4 = bK5 = bK6 = bK7 = false;
}

void OptimCritSet::SetK(int crit)
{
	switch(crit)
	{
	case C_CRIT_SEC:  bSEC = true;  break;
	case C_CRIT_R2SEC:  bSEC = bR2sec = true;  break;
	case C_CRIT_SECV:  bSECV = true;  break;
	case C_CRIT_R2SECV:  bSECV = bR2secv = true;  break;
	case C_CRIT_SECSECV:  bSECSECV = true;  break;
	case C_CRIT_F:  bSECV = bF = true;  break;
	case C_CRIT_SEV:  bSEV = true;  break;
	case C_CRIT_ERR:  bSEV = bErr = true;  break;
	case C_CRIT_SDV:  bSEV = bSDV = true;  break;
	case C_CRIT_R2SEV:  bSEV = bR2sev = true;  break;
	case C_CRIT_K1:  bSEC = bSEV = bK1 = true;  break;
	case C_CRIT_K2:  bSEC = bSECV = bK2 = true;  break;
	case C_CRIT_K3:  bSEC = bSEV = bK3 = true;  break;
	case C_CRIT_K4:  bSEC = bSECV = bK4 = true;  break;
	case C_CRIT_K5:  bSEC = bSECV = bK5 = true;  break;
	case C_CRIT_K6:  bSEC = bK6 = true;  break;
	case C_CRIT_K7:  bSEC = bSECV = bK7 = true;  break;
	default:  return;
	}
}

void OutsSet::Copy(OutsSet& data)
{
	chMah = data.chMah;
	chSEC = data.chSEC;
	chSECV = data.chSECV;
	chBad = data.chBad;
	chBound = data.chBound;
	limMah = data.limMah;
	limSEC = data.limSEC;
	limSECV = data.limSECV;
	limBound = data.limBound;
}

void OptimStepSettings::Copy(OptimStepSettings& data)
{
	ModelType = data.ModelType;
	SpectrumType = data.SpectrumType;
	NCompMean = data.NCompMean;
	NCompPlus = data.NCompPlus;
	NCompStep = data.NCompStep;
	HiperMean = data.HiperMean;
	HiperPlus = data.HiperPlus;
	HiperStep = data.HiperStep;
	NHarmMean = data.NHarmMean;
	NHarmPlus = data.NHarmPlus;
	NHarmStep = data.NHarmStep;
	NHarmType = data.NHarmType;
	KfnlMean = data.KfnlMean;
	KfnlPlus = data.KfnlPlus;
	KfnlStep = data.KfnlStep;
	KfnlType = data.KfnlType;
	LeftMean = data.LeftMean;
	LeftPlus = data.LeftPlus;
	LeftStep = data.LeftStep;
	RightMean = data.RightMean;
	RightPlus = data.RightPlus;
	RightStep = data.RightStep;
	IsOperation = data.IsOperation;
	Operation = data.Operation;
	MainCrit = data.MainCrit;
	SortCrit = data.SortCrit;
	MaxModels = data.MaxModels;

	data.GetExcPoints(ExcPoints);

	Calib.clear();
	copy(data.Calib.begin(), data.Calib.end(), inserter(Calib, Calib.begin()));
	Valid.clear();
	copy(data.Valid.begin(), data.Valid.end(), inserter(Valid, Valid.begin()));

	Preps.Copy(data.Preps);
	Crits.Copy(data.Crits);
	Outs.Copy(data.Outs);
}

void OptimStepSettings::GetExcPoints(SInt& points)
{
	points.clear();
	copy(ExcPoints.begin(), ExcPoints.end(), inserter(points, points.begin()));
}

void OptimStepSettings::GetCalibValid(VOptSpec2& calib, VOptSpec2& valid)
{
	calib.clear();
	copy(Calib.begin(), Calib.end(), inserter(calib, calib.begin()));

	valid.clear();
	copy(Valid.begin(), Valid.end(), inserter(valid, valid.begin()));
}

int OptimStepSettings::GetNCalibUse()
{
	int N = 0;
	ItOptSpec2 it;
	for(it=Calib.begin(); it!=Calib.end(); ++it)
		if(it->Use)
			N++;

	return N;
}

int OptimStepSettings::GetNValidUse()
{
	int N = 0;
	ItOptSpec2 it;
	for(it=Valid.begin(); it!=Valid.end(); ++it)
		if(it->Use)
			N++;

	return N;
}

OptimSpectrum2* OptimStepSettings::GetCalib(CString name, CString transname, int num)
{
	int i;
	ItOptSpec2 it;
	for(i=0, it=Calib.begin(); it!=Calib.end(); ++it, i++)
		if(!it->SampleName.Compare(name) && !it->TransName.Compare(transname) && it->Num == num)
			return &(*it);

	return NULL;
}

void OptimStepSettings::ExcAllBadSpectra(VBOSp2* badsp)
{
	ItBOSp2 itB;
	for(itB=badsp->begin(); itB!=badsp->end(); itB++)
	{
		OptimSpectrum2* Calib = GetCalib(itB->SampleName, itB->TransName, itB->Num);
		if(Calib == NULL)
			continue;

		Calib->Use = false;
	}
}

OptimModel::OptimModel()
{
	Num = 0;
	ModelType = C_MOD_MODEL_PLS;
	SpectrumType = 0;
	RangeLeft = 0;
	RangeRight = 0;
	NComp = 3;
	Hiper = 0.0001;
    NHarm = 0;
	Kfnl = 0.;
	Preproc = cEmpty;
}

void OptimModel::SetCrits(VInt* crits)
{
	CritValues.clear();

	ItInt it;
	for(it=crits->begin(); it!=crits->end(); ++it)
	{
		PIntDbl val((*it), 0.);
		CritValues.insert(val);
	}
}

void OptimModel::Copy(OptimModel& data)
{
	ModelType = data.ModelType;
	SpectrumType = data.SpectrumType;
	RangeLeft = data.RangeLeft;
	RangeRight = data.RangeRight;
	NComp = data.NComp;
	Hiper = data.Hiper;
	NHarm = data.NHarm;
	Kfnl = data.Kfnl;
	Preproc = data.Preproc;

	ExcPoints.clear();
	copy(data.ExcPoints.begin(), data.ExcPoints.end(), inserter(ExcPoints, ExcPoints.begin()));

	CritValues.clear();
	copy(data.CritValues.begin(), data.CritValues.end(), inserter(CritValues, CritValues.begin()));
}

void OptimModel::GetExcPoints(SInt& points)
{
	points.clear();
	copy(ExcPoints.begin(), ExcPoints.end(), inserter(points, points.begin()));
}

bool OptimModel::SetValue(int crit, double val)
{
	ItIntDbl it;
	for(it=CritValues.begin(); it!=CritValues.end(); ++it)
		if(it->first == crit)
		{
			it->second = val;
			return true;
		}

	return false;
}

double OptimModel::GetValue(int crit)
{
	ItIntDbl it;
	for(it=CritValues.begin(); it!=CritValues.end(); ++it)
	{
		if(it->first == crit)
			return it->second;
	}

	return 0.;
}

int OptimModel::GetNExcPoints()
{
	return int(ExcPoints.size());
}

void OptimModel::SetRange(int left, int right, int low, int upper)
{
	RangeLeft = left;
	RangeRight = right;

	SInt ExcNew;
	ItSInt it;
	for(it=ExcPoints.begin(); it!=ExcPoints.end(); ++it)
	{
		if((*it) < low)  continue;
		if((*it) > upper)  break;

		ExcNew.insert(*it);
	}
	ExcPoints.clear();
	copy(ExcNew.begin(), ExcNew.end(), inserter(ExcPoints, ExcPoints.begin()));
}

OptimResults::OptimResults(OptimCritSet* critset, int max)
{
	if(critset->bSEC)  Crits.push_back(C_CRIT_SEC);
	if(critset->bR2sec)  Crits.push_back(C_CRIT_R2SEC);
	if(critset->bSECV)  Crits.push_back(C_CRIT_SECV);
	if(critset->bR2secv)  Crits.push_back(C_CRIT_R2SECV);
	if(critset->bSECSECV)  Crits.push_back(C_CRIT_SECSECV);
	if(critset->bF)  Crits.push_back(C_CRIT_F);
	if(critset->bSEV)  Crits.push_back(C_CRIT_SEV);
	if(critset->bErr)  Crits.push_back(C_CRIT_ERR);
	if(critset->bSDV)  Crits.push_back(C_CRIT_SDV);
	if(critset->bR2sev)  Crits.push_back(C_CRIT_R2SEV);
	if(critset->bK1)  Crits.push_back(C_CRIT_K1);
	if(critset->bK2)  Crits.push_back(C_CRIT_K2);
	if(critset->bK3)  Crits.push_back(C_CRIT_K3);
	if(critset->bK4)  Crits.push_back(C_CRIT_K4);
	if(critset->bK5)  Crits.push_back(C_CRIT_K5);
	if(critset->bK6)  Crits.push_back(C_CRIT_K6);
	if(critset->bK7)  Crits.push_back(C_CRIT_K7);

	VIntDbl Nums;
	ItInt it;
	for(it=Crits.begin();  it!=Crits.end(); ++it)
		Sorts.push_back(Nums);

	MaxModels = max;
	NCalcModels = 0;
}

int OptimResults::GetPosByNum(int num)
{
	ItOptMod it;
	int i;
	for(i=0, it=OptModels.begin(); it!=OptModels.end(); ++it, i++)
		if(num == it->Num)
			return i;

	return -1;
}

bool OptimResults::DeleteByNum(int num)
{
	ItOptMod it;
	int i;
	for(i=0, it=OptModels.begin(); it!=OptModels.end(); ++it, i++)
		if(num == it->Num)
		{
			OptModels.erase(it);
			return true;
		}

	return false;
}

bool OptimResults::AddOptimModel(OptimModel& optmod)
{
	if(GetPosByNum(optmod.Num) >= 0)
		return false;

	NCalcModels++;

	OptModels.push_back(optmod);

	MapIntBool Bads;
	ItMIntDbl it;
	int i;
	for(i=0, it=Sorts.begin(); it!=Sorts.end(); ++it, i++)
	{
		PIntDbl newNum(optmod.Num, optmod.GetValue(Crits[i]));
		it->push_back(newNum);

		SortByCrit = Crits[i];
		sort(it->begin(), it->end(), SortOptimModelNums);

		if(int(it->size()) > MaxModels)
		{
			PIntBool newBad((*it)[MaxModels].first, true);
			Bads.insert(newBad);

			ItVIntDbl itID = it->begin() + MaxModels;
			it->erase(itID);
		}
	}
	if(Bads.size() <= 0)
		return true;

	for(it=Sorts.begin(); it!=Sorts.end(); ++it)
	{
		ItVIntDbl itID;
		for(itID=it->begin(); itID!=it->end(); ++itID)
		{
			ItIntBool itIB;
			for(itIB=Bads.begin(); itIB!=Bads.end(); ++itIB)
				if(itIB->first == itID->first)
					itIB->second = false;
		}
	}

	ItIntBool itIB;
	for(itIB=Bads.begin(); itIB!=Bads.end(); ++itIB)
		if(itIB->second)
			DeleteByNum(itIB->first);


	return true;
}

void OptimResults::EndAdding()
{
	Sorts.clear();
}

void OptimResults::Sort(int crit)
{
	SortByCrit = crit;
	sort(OptModels.begin(), OptModels.end(), SortOptimModels);
}

int OptimResults::GetNCrits()
{
	return int(Crits.size());
}

int OptimResults::GetNModels()
{
	return int(OptModels.size());
}

bool OptimResults::IsHQO()
{
	if(GetNModels() > 0)
		return (OptModels[0].ModelType == C_MOD_MODEL_HSO);

	return false;
}

bool OptimResults::IsPrepUse(int prep, int maxmod)
{
	int N = min(maxmod, GetNModels());
	if(N < 1)
		return false;

	ItOptMod it, itS = OptModels.begin(), itF = OptModels.begin() + N;
	for(it=itS; it!=itF; ++it)
	{
		if(cIsPrepInStr(it->Preproc, prep))
			return true;
	}

	return false;
}

void OptimResults::ClearResults()
{
	OptModels.clear();
	NCalcModels = 0;
}

int OptimResults::FindCritInd(int crit)
{
	int i;
	ItInt it;
	for(i=0, it=Crits.begin(); it!=Crits.end(); ++it, i++)
		if(crit == (*it))
			return i;

	return -1;
}
