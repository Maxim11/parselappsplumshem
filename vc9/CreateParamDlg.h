#ifndef _CREATE_PARAM_DLG_H_
#define _CREATE_PARAM_DLG_H_

#pragma once

#include "Resource.h"
#include "ParamData.h"

// ��������� ������ ������ �������

const int PARDLG_MODE_CREATE = 0;		// ����� �������� ������ ����������
const int PARDLG_MODE_CHANGE = 1;		// ����� �������� ������ ����������

///////////////////////////////////////////////////////////////////////////////////////////////////
// ������ ��������/�������������� ����������

class CCreateParamDlg : public CDialog
{
	DECLARE_DYNAMIC(CCreateParamDlg)

public:

	CCreateParamDlg(CParamData* data, int mode = PARDLG_MODE_CREATE, CWnd* pParent = NULL);		// �����������
	virtual ~CCreateParamDlg();																	// ����������

	enum { IDD = IDD_CREATE_PARAM };	// ������������� �������

	CEdit m_Name;				// ���� ��� �������������� "����� ����������"
	CEdit m_StdMois;			// ���� ��� �������������� "����������� ���������"
	CEdit m_Unit;				// ���� ��� �������������� "������� ���������"
	CEdit m_Format;				// ���� ��� �������������� "����� ������ ����� �������"
	CButton	m_Use;				// ��������� "�������� ������������� ����������� ���������"
	CSpinButtonCtrl m_SpinLetter;	// ����������/���������� "����� ������ ����� �������"

protected:

	CParamData* ParData;		// ��������� �� ������������� ����������
	CProjectData* ParentPrj;	// ��������� �� ������, ������������ ��� ����������

	int Mode;					// ����� ������

	CString m_vName;			// ��������� �������� "����� ����������"
	BOOL m_vUse;				// ������������� �������� "�������� ������������� ����������� ���������"
	double m_vStdMois;			// ��������� �������� "����������� ���������"
	CString m_vUnit;			// ��������� �������� "������� ���������"
	int m_vFormat;				// ��������� �������� "����� ������ ����� �������"

	bool IsBtnClk;

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();						// ������������� �������

	virtual void OnOK()
	{};
	virtual void OnCancel();			// ��������� ������� "�������� ���� � ����� �� �������"

	afx_msg void OnAccept();			// ��������� ������� "������� ������ � ����� �� �������"
	afx_msg void OnSetUse();			// ��������� ������� "�������� ������� ���. ����������� ���������"
	afx_msg void OnHelp();				// ��������� ������� "������� �������"

	void OnSpinLetterDeltaPos(NMHDR *pNMHDR, LRESULT *pResult);	// ��������� ������� "���������/��������� ����� ������ ����� �������"

	bool IsChanged();

	DECLARE_MESSAGE_MAP()
};

#endif
