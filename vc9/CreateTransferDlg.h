#ifndef _PARCEL_CREATE_TRANSFER_DLG_H_
#define _PARCEL_CREATE_TRANSFER_DLG_H_

#pragma once

#include "TransferCalculate.h"
#include "ParcelListCtrl.h"
#include "Resource.h"

///////////////////////////////////////////////////////////////////////////////////////////////////
// ������ �������� ������ ������������ ��������.

// �������� �������

const int C_TRANSDLG_PAGE_SMPL = 0;		// �������� ������ ��������
const int C_TRANSDLG_PAGE_CORR = 1;		// �������� ������ �������������

// ��������� ���� ���������� �������������

const int C_SORT_PREP_AVER	= 0;	// ��� ���������� ������������� �� �������� ��������
const int C_SORT_PREP_DIFF	= 1;	// ��� ���������� ������������� �� ������� �/� ���������� � ���������� ��������

class CCreateTransferDlg : public CDialog
{
	DECLARE_DYNAMIC(CCreateTransferDlg)

public:

	CCreateTransferDlg(CTransferData* trans, CWnd* pParent = NULL);					// �����������
	virtual ~CCreateTransferDlg();													// ����������

	enum { IDD = IDD_CREATE_TRANSFER };							// ������������� �������

	CTabCtrl  m_Tab;				// �������� �� ��������
	CParcelListCtrl m_listMaster;	// ������� �� ������� �������� ������-�������
	int m_nParStart;
	int m_nTimeCol;
	int GetSamSel();
	VStr SamSel;
	int GetPrepSel();
	VStr PrepSel;

	CParcelListCtrl m_listSlave;	// ������� �� ������� �������� �������� �������
	CListCtrl m_listSpec;			// ������� �� ������� �������� ����������� ������� �������� �������
	CEdit m_Name;					// ���� ��� �������������� "����� ������"
	CComboBox m_Device;				// ������ �������� ��� �������� ��������
	CComboBox m_Project;			// ������ �������� ��� �������� �������� �������
	CComboBox m_Model;				// ������ �������� ��� �������� �������
	CButton m_NoCorr;				// ������� "�������� ��� ���������"

	CParcelListCtrl m_listPrep;		// ������ �������������
	CListCtrl m_listDist;			// ������ ����������
	CEdit m_AddPrep;				// ���� ��� �������������� "�������������� �������������"
	CEdit m_MaxLetter;				// ���� ��� �������������� "������������� ����� �������������"
	CEdit m_LowLim;					// ���� ��� �������������� "������� ������� ������������� ���������"
	CEdit m_UpperLim;				// ���� ��� �������������� "�������� ������� ������������� ���������"
	CComboBox m_Sort2;				// ������ ����� ����������
	CButton	m_PrepA;				// ��������� �������� "������������ ������������� A"
	CButton	m_PrepB;				// ��������� �������� "������������ ������������� B"
	CButton	m_PrepM;				// ��������� �������� "������������ ������������� M"
	CButton	m_PrepD;				// ��������� �������� "������������ ������������� D"
	CButton	m_PrepC;				// ��������� �������� "������������ ������������� C"
	CButton	m_PrepN;				// ��������� �������� "������������ ������������� N"
	CButton	m_Prep1;				// ��������� �������� "������������ ������������� 1"
	CButton	m_Prep2;				// ��������� �������� "������������ ������������� 2"
	CButton	m_PrepNo;				// ��������� "�������� ������������� ������ �������������"
	CSpinButtonCtrl m_SpinLetter;	// ����������/���������� "������������� ����� �������������"

protected:

	CProjectData* PrjData;			// ��������� �� ������������ ������ �������
	CTransferCalculate CalcInfo;	// ����� ������� ������ ������������ ��������

	int m_vTab;						// ����� �������� ��������

	CDeviceData* MDev;				// ��������� �� ������-������
	CProjectData* MPrj;				// ��������� �� ������ ������-�������
	CModelData* MMod;				// ��������� �� ������ ������-�������
	CString m_vName;				// ��������� �������� "����� ������"
	int m_vDevice;					// ������ ���������� ������-�������
	int m_vProject;					// ������ ���������� ������� ������-�������
	int m_vModel;					// ������ ��������� ������ ������-�������
	BOOL m_vNoCorr;
	int nDev;						// ����� �������� ��� �������� ��������
	int nPrj;						// ����� �������� ��� �������� ��������
	int nMod;						// ����� �������� ��� �������� �������
	VStr MDevNames;					// ������ ���� �������� ��� �������� ��������
	VStr MPrjNames;					// ������ ���� �������� ��� �������� ��������
	VStr MModNames;					// ������ ���� �������� ��� �������� �������
	VModSam SamNames;				// ������ ��������� ��������
	VStr MSamNames;					// ������ ��������, ��������� ��� ��������
	VStr MParNames;					// ������ ����������� ��� ��������

	int m_vSort2;					// ������ ���������� ���� ����������
	BOOL m_vPrepA;					// ������������� �������� �������� "������������ ������������� A"
	BOOL m_vPrepB;					// ������������� �������� �������� "������������ ������������� B"
	BOOL m_vPrepM;					// ������������� �������� �������� "������������ ������������� M"
	BOOL m_vPrepD;					// ������������� �������� �������� "������������ ������������� D"
	BOOL m_vPrepC;					// ������������� �������� �������� "������������ ������������� C"
	BOOL m_vPrepN;					// ������������� �������� �������� "������������ ������������� N"
	BOOL m_vPrep1;					// ������������� �������� �������� "������������ ������������� 1"
	BOOL m_vPrep2;					// ������������� �������� �������� "������������ ������������� 2"
	BOOL m_vPrepNo;					// ������������� �������� �������� "������������� ������ �������������"
	CString m_vAddPrep;				// ��������� �������� "�������������� �������������"
	int m_vMaxLetter;				// ��������� �������� "������������� ����� �������������"
	int m_vLowLim;					// ��������� �������� "������� ������� ������������� ���������"
	int m_vUpperLim;				// ��������� �������� "�������� ������� ������������� ���������"
	int MinLimit;					// ����������-���������� �������� ��� ������ ������������ ���������
	int MaxLimit;					// �����������-���������� �������� ��� ������ ������������ ���������
	CString curprep;				// ������� �������������
	int PreprocMaxLen;				// ���������� ����� ������������� �� ���������
	VInt PreprocAval;				// ������ ����������� �������������
	MInt PreprocList;				// ������ ��������� �������������
	MInt PreprocListAdd;			// ������ �������������� �������������
	bool IsAddPreprocValid;

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();						// ������������� �������

	virtual void OnOK()
	{};
	virtual void OnCancel();			// ��������� ������� "�������� ���� � ����� �� �������"

	afx_msg void OnAccept();										// ��������� ������� "������� ������ � ����� �� �������"
	afx_msg void OnTabSelchange(NMHDR* pNMHDR, LRESULT* pResult);	// ��������� ������� "������� �� ������ ��������"

	afx_msg void OnSelect();		// ��������� ������� "������� ������� ��� ��������"
	afx_msg void OnRemoveM();		// ��������� ������� "������� ����� ��������, ��������� ��� ��������"
	afx_msg void OnRemove();		// ��������� ������� "������� ��������� �������"
	afx_msg void OnAdd();			// ��������� ������� "���������������� ������ ��� ���������� �������"
	afx_msg void OnExclude();		// ��������� ������� "��������/�������� ������"
	afx_msg void OnPlotMaster();	// ��������� ������� "�������� ������� �������� ������-�������"
	afx_msg void OnPlotSlave();		// ��������� ������� "�������� ������� �������� �������� �������"
	afx_msg void OnSelDevice();		// ��������� ������� "�������� �������� � ������ ��������"
	afx_msg void OnSelProject();	// ��������� ������� "�������� �������� � ������ ��������"
	afx_msg void OnSelModel();		// ��������� ������� "�������� �������� � ������ �������"
	afx_msg void OnSlaveListSelChange(NMHDR* /*pNMHDR*/, LRESULT *pResult); // ��������� ������� "�������� �������� � ������ ��������� ��������"
	afx_msg void OnSpecListSelChange(NMHDR* /*pNMHDR*/, LRESULT *pResult);	// ��������� ������� "�������� �������� � ������ ��������"
	afx_msg void OnSortSamples();	// ��������� ������� "����������� ������ ��������"
	afx_msg	void OnSetEnablingNoConv();

	afx_msg void OnSort2();					// ��������� ������� "����������� ������ �������������"
	afx_msg void OnMake();					// ��������� ������� "���������� �������������"
	afx_msg void OnCorrect();				// ��������� ������� "�������� ��������� ��������"
	afx_msg	void OnCreatePreprocList();		// ��������� ������� "�������� ������ �������������"
	afx_msg	void OnMaxLetterChange();		// ��������� ������� "�������� ����������� ����� �������������"
	afx_msg	void OnAddPrepChange();			// ��������� ������� "�������� �������������� �������������"
	afx_msg void OnPrepListSelChange(NMHDR* /*pNMHDR*/, LRESULT *pResult); // ��������� ������� "�������� �������� � ������ �������������"
	afx_msg	void OnLowLimChange();			// ��������� ������� "�������� ������ ������� ������������� ���������"
	afx_msg	void OnUpperLimChange();		// ��������� ������� "�������� ������� ������� ������������� ���������"
	afx_msg void OnHelp();				// ��������� ������� "������� �������"

	afx_msg void OnAnyTableColumnClick(NMHDR* pNMHDR, LRESULT* pResult);

	void SortCorrSamples();			// ��������� ���������� ��������
	void FillListDevice();			// ��������� ������ ��������
	void FillListProject();			// ��������� ������ ��������
	void FillListModel();			// ��������� ������ �������
	void FillTableMaster(bool bFromScratch);			// ��������� ������� � ��������� ������, ��������� ��� ��������
	void FillTableSlave(int sel);	// ��������� ������� � ���������� ���������
	void FillTableSpec();			// ��������� ������� �� ������� ������� ���������� �������
	void ClearTableSpec();			// �������� ������� �� ������� ������� ���������� �������
	void GetMSamPar();				// ���������� ����� �������� � ����������� ��� ��������
	void FindSamples();				// ����� ������� � ����������� �������

	void OnSpinLetterDeltaPos(NMHDR *pNMHDR, LRESULT *pResult);	// ��������� ������� "���������/��������� ������������ ����� �������������"
	void FillTablePrep(bool bFromScratch);						// ��������� ������� �� ������� �������������
	void FillTableDist(int sel);				// ��������� ������� �� ������� ����������
	void CreatePreprocList(VInt* Old);			// ������� ������ �������������
	void CreatePreprocListAdd(CString str);		// ������� ������ �������������� �������������
	CString GetPreprocLine(VInt* prepline);					// �������� ������������� ��� ������
	TCHAR GetPreprocCharByMean(int prep);					// �������� ���������� ��������� �������������
	bool IsPreprocAddAvailable(VInt* prepline, int prep);	// ����������, ������� �� �������������
	void CreateCorrFields();

	void MasterSetSortIcons();
	void SlaveSetSortIcons();
	void PrepSetSortIcons();

	void ShowFields(int page = -1);		// ������� �� ����� ��������, ��������������� �������� ��������
	void EnableFields();

	bool CheckData();				// ��������� ��������� ������
	bool AcceptData();				// ������� ��������� ������


//	test--
	void OnCorrectAlone();
	bool AcceptDataAlone();
//	--test

protected:

	DECLARE_MESSAGE_MAP()

};

#endif
