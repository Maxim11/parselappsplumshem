#include "stdafx.h"

#include "CreateTransSSTDlg.h"
#include "MainFrm.h"
#include "ExcludeSpectraDlg.h"

static bool need_check = false;

IMPLEMENT_DYNAMIC(CCreateTransSSTDlg, CDialog)

CCreateTransSSTDlg::CCreateTransSSTDlg(CProjectData* prj, VPlate* plates, CWnd* pParent)
	: CDialog(CCreateTransSSTDlg::IDD, pParent)
{
	ParentPrj = prj;
	Plates = plates;

	need_check = false;

	m_vTab = C_TRANSSSTDLG_PAGE_FRAME;

	CreateSamData();
	FillStartData();
}

CCreateTransSSTDlg::~CCreateTransSSTDlg()
{
}

void CCreateTransSSTDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_TAB_1, m_Tab);

	DDX_Control(pDX, IDC_SAMPLE_0, m_Frame);
	DDX_Control(pDX, IDC_SAMPLE_1, m_Plate1);
	DDX_Control(pDX, IDC_SAMPLE_2, m_Plate2);
	DDX_Control(pDX, IDC_SAMPLE_3, m_Plate3);

	DDX_CBIndex(pDX, IDC_SAMPLE_0, m_vFrame);
	DDX_CBIndex(pDX, IDC_SAMPLE_1, m_vPlate1);
	DDX_CBIndex(pDX, IDC_SAMPLE_2, m_vPlate2);
	DDX_CBIndex(pDX, IDC_SAMPLE_3, m_vPlate3);

	cParcelDDX(pDX, IDC_KOEFF_1, m_vKoeff1);
	cParcelDDV(pDX, ParcelLoadString(STRID_CREATE_TRANS_SST_DDV_KOEFF), m_vKoeff1, 0.000001, 1.);
	cParcelDDX(pDX, IDC_KOEFF_2, m_vKoeff2);
	cParcelDDV(pDX, ParcelLoadString(STRID_CREATE_TRANS_SST_DDV_KOEFF), m_vKoeff2, 0.000001, 1.);
	cParcelDDX(pDX, IDC_KOEFF_3, m_vKoeff3);
	cParcelDDV(pDX, ParcelLoadString(STRID_CREATE_TRANS_SST_DDV_KOEFF), m_vKoeff3, 0.000001, 1.);
	cParcelDDX(pDX, IDC_WIDTH_1, m_vWidth1);
	cParcelDDX(pDX, IDC_WIDTH_2, m_vWidth2);
	cParcelDDX(pDX, IDC_WIDTH_3, m_vWidth3);
	if(need_check)
	{
		cParcelDDV(pDX, ParcelLoadString(STRID_CREATE_TRANS_SST_DDV_WIDTH), m_vWidth1, 0.000001, 100000.);
		cParcelDDV(pDX, ParcelLoadString(STRID_CREATE_TRANS_SST_DDV_WIDTH), m_vWidth2, 0.000001, 100000.);
		cParcelDDV(pDX, ParcelLoadString(STRID_CREATE_TRANS_SST_DDV_WIDTH), m_vWidth3, 0.000001, 100000.);
	}
	cParcelDDX(pDX, IDC_ADDWIDTH1_1, m_vAddWidth11);
	cParcelDDV(pDX, ParcelLoadString(STRID_CREATE_TRANS_SST_DDV_ADDWIDTH), m_vAddWidth11, 0.000001, 100000.);
	cParcelDDX(pDX, IDC_ADDWIDTH1_2, m_vAddWidth12);
	cParcelDDV(pDX, ParcelLoadString(STRID_CREATE_TRANS_SST_DDV_ADDWIDTH), m_vAddWidth12, 0.000001, 100000.);
	cParcelDDX(pDX, IDC_ADDWIDTH1_3, m_vAddWidth13);
	cParcelDDV(pDX, ParcelLoadString(STRID_CREATE_TRANS_SST_DDV_ADDWIDTH), m_vAddWidth13, 0.000001, 100000.);
	cParcelDDX(pDX, IDC_ADDWIDTH2_1, m_vAddWidth21);
	cParcelDDV(pDX, ParcelLoadString(STRID_CREATE_TRANS_SST_DDV_ADDWIDTH), m_vAddWidth21, 0.000001, 100000.);
	cParcelDDX(pDX, IDC_ADDWIDTH2_2, m_vAddWidth22);
	cParcelDDV(pDX, ParcelLoadString(STRID_CREATE_TRANS_SST_DDV_ADDWIDTH), m_vAddWidth22, 0.000001, 100000.);
	cParcelDDX(pDX, IDC_ADDWIDTH2_3, m_vAddWidth23);
	cParcelDDV(pDX, ParcelLoadString(STRID_CREATE_TRANS_SST_DDV_ADDWIDTH), m_vAddWidth23, 0.000001, 100000.);
	cParcelDDX(pDX, IDC_ADDWIDTH3_1, m_vAddWidth31);
	cParcelDDV(pDX, ParcelLoadString(STRID_CREATE_TRANS_SST_DDV_ADDWIDTH), m_vAddWidth31, 0.000001, 100000.);
	cParcelDDX(pDX, IDC_ADDWIDTH3_2, m_vAddWidth32);
	cParcelDDV(pDX, ParcelLoadString(STRID_CREATE_TRANS_SST_DDV_ADDWIDTH), m_vAddWidth32, 0.000001, 100000.);
	cParcelDDX(pDX, IDC_ADDWIDTH3_3, m_vAddWidth33);
	cParcelDDV(pDX, ParcelLoadString(STRID_CREATE_TRANS_SST_DDV_ADDWIDTH), m_vAddWidth33, 0.000001, 100000.);
	cParcelDDX(pDX, IDC_ADDWIDTH4_1, m_vAddWidth41);
	cParcelDDV(pDX, ParcelLoadString(STRID_CREATE_TRANS_SST_DDV_ADDWIDTH), m_vAddWidth41, 0.000001, 100000.);
	cParcelDDX(pDX, IDC_ADDWIDTH4_2, m_vAddWidth42);
	cParcelDDV(pDX, ParcelLoadString(STRID_CREATE_TRANS_SST_DDV_ADDWIDTH), m_vAddWidth42, 0.000001, 100000.);
	cParcelDDX(pDX, IDC_ADDWIDTH4_3, m_vAddWidth43);
	cParcelDDV(pDX, ParcelLoadString(STRID_CREATE_TRANS_SST_DDV_ADDWIDTH), m_vAddWidth43, 0.000001, 100000.);
}


BEGIN_MESSAGE_MAP(CCreateTransSSTDlg, CDialog)

	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB_1, OnTabSelchange)

	ON_BN_CLICKED(IDC_ACCEPT, OnAccept)
	ON_BN_CLICKED(IDHELP, OnHelp)
	ON_BN_CLICKED(IDC_SHOW, OnSelect)

END_MESSAGE_MAP()


BOOL CCreateTransSSTDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	USES_CONVERSION;

	CString s;

	TC_ITEM TabCtrlItem;
	TabCtrlItem.mask = TCIF_TEXT;
	TabCtrlItem.pszText = (LPWSTR)GETSTRUI(STRID_CREATE_TRANS_SST_FRAME);
	m_Tab.InsertItem(C_TRANSSSTDLG_PAGE_FRAME, &TabCtrlItem);
	TabCtrlItem.pszText = (LPWSTR)GETSTRUI(STRID_CREATE_TRANS_SST_PLATE1);
	m_Tab.InsertItem(C_TRANSSSTDLG_PAGE_PLATE1, &TabCtrlItem);
	TabCtrlItem.pszText = (LPWSTR)GETSTRUI(STRID_CREATE_TRANS_SST_PLATE2);
	m_Tab.InsertItem(C_TRANSSSTDLG_PAGE_PLATE2, &TabCtrlItem);
	TabCtrlItem.pszText = (LPWSTR)GETSTRUI(STRID_CREATE_TRANS_SST_PLATE3);
	m_Tab.InsertItem(C_TRANSSSTDLG_PAGE_PLATE3, &TabCtrlItem);

	CString Hdr = ParcelLoadString(STRID_CREATE_TRANS_SST_HDR);
	SetWindowText(Hdr);

	m_Frame.ResetContent();
	m_Plate1.ResetContent();
	m_Plate2.ResetContent();
	m_Plate3.ResetContent();

	ItModSam it;
	for(it=SamNames.begin(); it!=SamNames.end(); ++it)
	{
		m_Frame.AddString(it->SampleName);
		m_Plate1.AddString(it->SampleName);
		m_Plate2.AddString(it->SampleName);
		m_Plate3.AddString(it->SampleName);
	}
	m_Frame.SetCurSel(m_vFrame);
	m_Plate1.SetCurSel(m_vPlate1);
	m_Plate2.SetCurSel(m_vPlate2);
	m_Plate3.SetCurSel(m_vPlate3);

	GetDlgItem(IDC_ACCEPT)->SetWindowText(ParcelLoadString(STRID_CREATE_TRANS_SST_ACCEPT));
	GetDlgItem(IDCANCEL)->SetWindowText(ParcelLoadString(STRID_CREATE_TRANS_SST_DECLINE));
	GetDlgItem(IDHELP)->SetWindowText(ParcelLoadString(STRID_CREATE_TRANS_SST_HELP));
	GetDlgItem(IDC_SHOW)->SetWindowText(ParcelLoadString(STRID_CREATE_TRANS_SST_SPECTRA));
	GetDlgItem(IDC_STATIC_1)->SetWindowText(ParcelLoadString(STRID_CREATE_TRANS_SST_SETTINGS));
	GetDlgItem(IDC_STATIC_2)->SetWindowText(ParcelLoadString(STRID_CREATE_TRANS_SST_ADDINGS));
	GetDlgItem(IDC_STATIC_3)->SetWindowText(ParcelLoadString(STRID_CREATE_TRANS_SST_SAMPLE));
	GetDlgItem(IDC_STATIC_4)->SetWindowText(ParcelLoadString(STRID_CREATE_TRANS_SST_KOEFF));
	GetDlgItem(IDC_STATIC_5)->SetWindowText(ParcelLoadString(STRID_CREATE_TRANS_SST_WIDTH0));
	GetDlgItem(IDC_STATIC_6)->SetWindowText(ParcelLoadString(STRID_CREATE_TRANS_SST_WIDTH1));
	GetDlgItem(IDC_STATIC_7)->SetWindowText(ParcelLoadString(STRID_CREATE_TRANS_SST_WIDTH2));
	GetDlgItem(IDC_STATIC_8)->SetWindowText(ParcelLoadString(STRID_CREATE_TRANS_SST_WIDTH3));
	GetDlgItem(IDC_STATIC_9)->SetWindowText(ParcelLoadString(STRID_CREATE_TRANS_SST_WIDTH4));

	ShowFields(C_TRANSSSTDLG_PAGE_FRAME);

	GetDlgItem(IDHELP)->EnableWindow(pFrame->IsExistHelp());

	return true;
}

void CCreateTransSSTDlg::OnAccept()
{
	need_check = true;

	if(!UpdateData())
		return;

	if(m_vFrame == m_vPlate1 || m_vFrame == m_vPlate2 || m_vFrame == m_vPlate3 ||
	   m_vPlate1 == m_vPlate2 || m_vPlate1 == m_vPlate3 || m_vPlate2 == m_vPlate3)
	{
		ParcelError(ParcelLoadString(STRID_CREATE_TRANS_SST_ERR_NAMES));
		return;
	}

	ItIntBool it4;
	ItPlate it;
	for(it=Plates->begin(); it!=Plates->end(); ++it)
	{
		CString s;
		int Nuse = 0;
		switch(it->Num)
		{
		case 0:
			it->SamName = SamNames[m_vFrame].SampleName;
			it->Spectra.clear();
			for(it4=SamNames[m_vFrame].Spectra.begin(); it4!=SamNames[m_vFrame].Spectra.end(); ++it4)
				if(it4->second)
				{
					it->Spectra.push_back(it4->first);
					Nuse++;
				}
			break;
		case 1:
			it->SamName = SamNames[m_vPlate1].SampleName;
			it->Spectra.clear();
			for(it4=SamNames[m_vPlate1].Spectra.begin(); it4!=SamNames[m_vPlate1].Spectra.end(); ++it4)
				if(it4->second)
				{
					it->Spectra.push_back(it4->first);
					Nuse++;
				}
			it->R = m_vKoeff1;
			it->T0 = m_vWidth1;
			it->T1 = m_vAddWidth11;
			it->T2 = m_vAddWidth21;
			it->T3 = m_vAddWidth31;
			it->T4 = m_vAddWidth41;
			break;
		case 2:
			it->SamName = SamNames[m_vPlate2].SampleName;
			it->Spectra.clear();
			for(it4=SamNames[m_vPlate2].Spectra.begin(); it4!=SamNames[m_vPlate2].Spectra.end(); ++it4)
				if(it4->second)
				{
					it->Spectra.push_back(it4->first);
					Nuse++;
				}
			it->R = m_vKoeff2;
			it->T0 = m_vWidth2;
			it->T1 = m_vAddWidth12;
			it->T2 = m_vAddWidth22;
			it->T3 = m_vAddWidth32;
			it->T4 = m_vAddWidth42;
			break;
		case 3:
			it->SamName = SamNames[m_vPlate3].SampleName;
			it->Spectra.clear();
			for(it4=SamNames[m_vPlate3].Spectra.begin(); it4!=SamNames[m_vPlate3].Spectra.end(); ++it4)
				if(it4->second)
				{
					it->Spectra.push_back(it4->first);
					Nuse++;
				}
			it->R = m_vKoeff3;
			it->T0 = m_vWidth3;
			it->T1 = m_vAddWidth13;
			it->T2 = m_vAddWidth23;
			it->T3 = m_vAddWidth33;
			it->T4 = m_vAddWidth43;
			break;
		}

		if(Nuse <= 0)
		{
			s.Format(ParcelLoadString(STRID_CREATE_TRANS_SST_ERR_NOSPEC), it->SamName);
			ParcelError(s);
			return;
		}
	}

	CDialog::OnOK();
}

void CCreateTransSSTDlg::OnCancel()
{
	CDialog::OnCancel();
}

void CCreateTransSSTDlg::OnTabSelchange(NMHDR* /*pNMHDR*/, LRESULT* pResult)
{
	m_vTab = m_Tab.GetCurSel();

	ShowFields();

	*pResult = 0;
}

void CCreateTransSSTDlg::OnSelect()
{
	int Ind = 0;
	switch(m_vTab)
	{
	case C_TRANSSSTDLG_PAGE_FRAME:  Ind = m_Frame.GetCurSel();  break;
	case C_TRANSSSTDLG_PAGE_PLATE1:  Ind = m_Plate1.GetCurSel();  break;
	case C_TRANSSSTDLG_PAGE_PLATE2:  Ind = m_Plate2.GetCurSel();  break;
	case C_TRANSSSTDLG_PAGE_PLATE3:  Ind = m_Plate3.GetCurSel();  break;
	}

	ModelSample *MSam = &SamNames[Ind];
	CTransferData *Trans = ParentPrj->GetTransOwn();
	ModelSample ModSam(Trans->GetSample(MSam->SampleName));
	ModSam.Copy(MSam);

	CExcludeSpectraDlg Dlg(ParentPrj, &ModSam);
	if(Dlg.DoModal() != IDOK)
		return;

	SamNames[Ind].Copy(&ModSam);
}

void CCreateTransSSTDlg::ShowFields(int page/* = -1*/)
{
	if(page >= C_TRANSSSTDLG_PAGE_FRAME && page <= C_TRANSSSTDLG_PAGE_PLATE3)
		m_vTab = page;

	int show1 = (m_vTab == C_TRANSSSTDLG_PAGE_PLATE1) ? SW_SHOW : SW_HIDE;
	int show2 = (m_vTab == C_TRANSSSTDLG_PAGE_PLATE2) ? SW_SHOW : SW_HIDE;
	int show3 = (m_vTab == C_TRANSSSTDLG_PAGE_PLATE3) ? SW_SHOW : SW_HIDE;
	int show123 = (m_vTab != C_TRANSSSTDLG_PAGE_FRAME) ? SW_SHOW : SW_HIDE;

	GetDlgItem(IDC_STATIC_1)->ShowWindow(show123);
	GetDlgItem(IDC_STATIC_2)->ShowWindow(show123);
	GetDlgItem(IDC_STATIC_4)->ShowWindow(show123);
	GetDlgItem(IDC_STATIC_5)->ShowWindow(show123);
	GetDlgItem(IDC_STATIC_6)->ShowWindow(show123);
	GetDlgItem(IDC_STATIC_7)->ShowWindow(show123);
	GetDlgItem(IDC_STATIC_8)->ShowWindow(show123);
	GetDlgItem(IDC_STATIC_9)->ShowWindow(show123);

	GetDlgItem(IDC_SAMPLE_0)->ShowWindow(!show123);

	GetDlgItem(IDC_SAMPLE_1)->ShowWindow(show1);
	GetDlgItem(IDC_KOEFF_1)->ShowWindow(show1);
	GetDlgItem(IDC_WIDTH_1)->ShowWindow(show1);
	GetDlgItem(IDC_ADDWIDTH1_1)->ShowWindow(show1);
	GetDlgItem(IDC_ADDWIDTH2_1)->ShowWindow(show1);
	GetDlgItem(IDC_ADDWIDTH3_1)->ShowWindow(show1);
	GetDlgItem(IDC_ADDWIDTH4_1)->ShowWindow(show1);

	GetDlgItem(IDC_SAMPLE_2)->ShowWindow(show2);
	GetDlgItem(IDC_KOEFF_2)->ShowWindow(show2);
	GetDlgItem(IDC_WIDTH_2)->ShowWindow(show2);
	GetDlgItem(IDC_ADDWIDTH1_2)->ShowWindow(show2);
	GetDlgItem(IDC_ADDWIDTH2_2)->ShowWindow(show2);
	GetDlgItem(IDC_ADDWIDTH3_2)->ShowWindow(show2);
	GetDlgItem(IDC_ADDWIDTH4_2)->ShowWindow(show2);

	GetDlgItem(IDC_SAMPLE_3)->ShowWindow(show3);
	GetDlgItem(IDC_KOEFF_3)->ShowWindow(show3);
	GetDlgItem(IDC_WIDTH_3)->ShowWindow(show3);
	GetDlgItem(IDC_ADDWIDTH1_3)->ShowWindow(show3);
	GetDlgItem(IDC_ADDWIDTH2_3)->ShowWindow(show3);
	GetDlgItem(IDC_ADDWIDTH3_3)->ShowWindow(show3);
	GetDlgItem(IDC_ADDWIDTH4_3)->ShowWindow(show3);
}

void CCreateTransSSTDlg::CreateSamData()
{
	int i;
	SamNames.clear();
	for(i=0; i<ParentPrj->GetNSamples(); i++)
	{
		CSampleData* Sam = ParentPrj->GetSample(i);
		if(Sam->GetNSpectraUsed() > 0)
		{
			ModelSample MSam(Sam);
			SamNames.push_back(MSam);
		}
	}
}

void CCreateTransSSTDlg::FillStartData()
{
	m_vFrame = m_vPlate1 = m_vPlate2 = m_vPlate3 = 0;
	ItModSam itN;
	int i;
	ItPlate it;
	for(it=Plates->begin(); it!=Plates->end(); ++it)
	{
		switch(it->Num)
		{
		case 0:
			for(i=0, itN=SamNames.begin(); itN!=SamNames.end(); ++itN, i++)
				if(!itN->SampleName.Compare(it->SamName))
				{
					m_vFrame = i;
					break;
				}
			break;
		case 1:
			for(i=0, itN=SamNames.begin(); itN!=SamNames.end(); ++itN, i++)
				if(!itN->SampleName.Compare(it->SamName))
				{
					m_vPlate1 = i;
					break;
				}
			m_vKoeff1 = it->R;
			m_vWidth1 = it->T0;
			m_vAddWidth11 = it->T1;
			m_vAddWidth21 = it->T2;
			m_vAddWidth31 = it->T3;
			m_vAddWidth41 = it->T4;
			break;
		case 2:
			for(i=0, itN=SamNames.begin(); itN!=SamNames.end(); ++itN, i++)
				if(!itN->SampleName.Compare(it->SamName))
				{
					m_vPlate2 = i;
					break;
				}
			m_vKoeff2 = it->R;
			m_vWidth2 = it->T0;
			m_vAddWidth12 = it->T1;
			m_vAddWidth22 = it->T2;
			m_vAddWidth32 = it->T3;
			m_vAddWidth42 = it->T4;
			break;
		case 3:
			for(i=0, itN=SamNames.begin(); itN!=SamNames.end(); ++itN, i++)
				if(!itN->SampleName.Compare(it->SamName))
				{
					m_vPlate3 = i;
					break;
				}
			m_vKoeff3 = it->R;
			m_vWidth3 = it->T0;
			m_vAddWidth13 = it->T1;
			m_vAddWidth23 = it->T2;
			m_vAddWidth33 = it->T3;
			m_vAddWidth43 = it->T4;
			break;
		}
	}
}

void CCreateTransSSTDlg::OnHelp()
{
	int IdHelp = DLGID_SAMPLE_CREATE_SST;

	pFrame->ShowHelp(IdHelp);
}
