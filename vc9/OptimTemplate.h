#ifndef _PARCEL_OPTIM_TEMPLATE_H_
#define _PARCEL_OPTIM_TEMPLATE_H_

#pragma once

#include "ModelOptimization2.h"
#include "ProjectData.h"
#include "PRDBTemplate.h"

struct TemplateStep
{
	int Num;

	int ModelType;
	int SpType;

	int NCompPlus;
	int NCompStep;
	double HiperPlus;
	double HiperStep;
	int NHarmPlus;
	int NHarmStep;
	int NHarmType;
	double KfnlPlus;
	double KfnlStep;
	int KfnlType;

	int SpecLeftPlus;
	int SpecLeftStep;
	int SpecRightPlus;
	int SpecRightStep;

	int MaxModels;

	int MainCrit;
	int SortCrit;
	bool IsTest;
	int Operation;

	OptimPreprocSet prepsets;
	OutsSet outs;
};

typedef vector<TemplateStep> VStep;
typedef vector<TemplateStep>::iterator ItVStep;

struct Template
{
	CString Name;
	CString Note;
	CTime Date;

	int NCompMean;
	double HiperMean;
	int NHarmMean;
	double KfnlMean;
	int SpecLeftMean;
	int SpecRightMean;

	int PrjSpecMin;
	int PrjSpecMax;
	int PrjResol;
	int PrjZeroFilling;

	int LastCrit;

	SInt ExcPoints;

	VStep Steps;

	Template();
	~Template();

	void Copy(Template& data);
	void SetPrjSettings(CProjectData* prj);
	bool IsUseSEV();
	bool CheckPrjSettings(CProjectData* Prj);
	void GetExcPoints(SInt& points);
};

typedef vector<Template> VTmpl;
typedef vector<Template>::iterator ItTmpl;

class COptimTemplate
{
public:

	COptimTemplate();
	~COptimTemplate();

protected:

	CString BaseDefName;
	CString BaseName;

	VTmpl Templates;

public:

	CPRDBTemplate Database;

	CString GetBaseDefName();
	CString GetBaseName();
	void SetBaseName(CString name);

	CString GetBaseNameShort();

	bool IsLoaded();

	int GetNTemplates();
	Template* GetTemplate(int ind);
	Template* GetTemplate(CString name);
	bool AddTemplate(Template* tmpl);
	bool ChangeTemplate(int ind);
	bool DeleteTemplate(int ind);
	bool DeleteTemplate(CString name);
	void DeleteAllTemplates();

	void GetDataForBase(Template& Tmpl, tmpldb::PRTemplate& Obj);
	void SetDataFromBase(Template& Tmpl, tmpldb::PRTemplate& Obj);
	bool LoadDatabase();

	bool LoadBase(CString fname, bool IsMsg);
};

#endif
