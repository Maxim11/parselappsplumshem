#include "stdafx.h"

#include "ProgressBarDlg.h"
#include "wm.h"


IMPLEMENT_DYNAMIC(CProgressBarDlg, CDialog)

CProgressBarDlg::CProgressBarDlg(CWnd* pParent) : CDialog(CProgressBarDlg::IDD, pParent)
{
	pParentWnd = pParent;
	Hdr = ParcelLoadString(STRID_PROGRESS_BAR_DEFHDR);
	Comment = ParcelLoadString(STRID_PROGRESS_BAR_DEFCOMMENT);
	Status = ParcelLoadString(STRID_PROGRESS_BAR_DEFSTATUS);
	Start = 0;
	Finish = 100;
	Step = 1;
	Position = 0;
	Elapsed = 0;
	Estimated = 0;
	IsStopped = false;
	IsPause = false;
}

CProgressBarDlg::~CProgressBarDlg()
{
}

void CProgressBarDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_PROGRESS1, m_Progress);
}


BEGIN_MESSAGE_MAP(CProgressBarDlg, CDialog)

	ON_WM_TIMER()

	ON_BN_CLICKED(IDC_STOP, OnStop)
	ON_BN_CLICKED(IDC_PAUSE, OnPause)

END_MESSAGE_MAP()


BOOL CProgressBarDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	SetHeader(Hdr);
	SetComment(Comment);
	SetStatus(Status);
	SetRangeStep(Start, Finish, Step);
	SetPosition(Position);
	SetElapsed(Elapsed);
	SetEstimated(Estimated);

	GetDlgItem(IDC_STOP)->SetWindowText(ParcelLoadString(STRID_PROGRESS_BAR_STOP));
	GetDlgItem(IDC_PAUSE)->SetWindowText(ParcelLoadString(STRID_PROGRESS_BAR_PAUSA));
	GetDlgItem(IDC_STATIC_3)->SetWindowText(ParcelLoadString(STRID_PROGRESS_BAR_ELAPSED));
	GetDlgItem(IDC_STATIC_4)->SetWindowText(ParcelLoadString(STRID_PROGRESS_BAR_ESTIMATED));

	SetTimer(PARCEL_WM_PROGRESS_TIMER, 100, 0);

	return true;
}

void CProgressBarDlg::OK()
{
	CDialog::OnOK();
}

void CProgressBarDlg::Stop()
{
	CDialog::OnCancel();
}

void CProgressBarDlg::Work()
{
	OK();
}

void CProgressBarDlg::ResumeWork()
{
	OK();
}

void CProgressBarDlg::OnStop()
{
	IsStopped = true;
	if(IsPause)
		Stop();
}

void CProgressBarDlg::OnPause()
{
	IsPause = !IsPause;

	CString s = (IsPause) ? ParcelLoadString(STRID_PROGRESS_BAR_CONTINUE) : ParcelLoadString(STRID_PROGRESS_BAR_PAUSA);
	GetDlgItem(IDC_PAUSE)->SetWindowText(s);

	if(!IsPause)
		ResumeWork();
}

void CProgressBarDlg::OnTimer(UINT Id)
{
	if(Id == PARCEL_WM_PROGRESS_TIMER)
	{
		KillTimer(Id);
		Work();
	}
	if(Id == PARCEL_WM_PAUSE_TIMER)
	{
		KillTimer(Id);
		IsPause = false;
		ResumeWork();
	}
}

void CProgressBarDlg::SetHeader(CString str)
{
	Hdr = str;

	CString s;
	s.Format(Hdr, 0);
	SetWindowText(s);
}

void CProgressBarDlg::SetComment(CString str)
{
	Comment = str;
	GetDlgItem(IDC_STATIC_1)->SetWindowText(Comment);
}

void CProgressBarDlg::SetStatus(CString str)
{
	Status = str;
	ShowCurStatus();
}

void CProgressBarDlg::SetRangeStep(int start, int finish, int step)
{
	Start = start;
	Finish = finish;
	Step = step;
	Position = 0;

	m_Progress.SetRange32(Start, Finish);
	m_Progress.SetStep(Step);
	SetPosition(0);
}

void CProgressBarDlg::SetPosition(int pos)
{
	Position = pos;
	m_Progress.SetPos(Position);
	ShowCurStatus();
}

void CProgressBarDlg::SetElapsed(int sec)
{
	Elapsed = sec;
	GetDlgItem(IDC_STATIC_5)->SetWindowText(ConvertTimeToStr(Elapsed));
}

void CProgressBarDlg::SetEstimated(int sec)
{
	Estimated = sec;
	GetDlgItem(IDC_STATIC_6)->SetWindowText(ConvertTimeToStr(Estimated));
}

void CProgressBarDlg::ShowCurStatus()
{
	CString s;
	s.Format(Status, Position - Start, Finish - Start);
	GetDlgItem(IDC_STATIC_2)->SetWindowText(s);

	int num = (Finish > Start) ? int(100 * (Position - Start) / (Finish - Start)) : 0;
	s.Format(Hdr, num);
	s += _T("%");
	SetWindowText(s);
}

CString CProgressBarDlg::ConvertTimeToStr(int time)
{
	CString s = cEmpty;
	int hour = int(time / 3600);
	int min = int(time / 60) - 60 * hour;
	int sec = time - 3600 * hour - 60 * min;

	if(time < 60)
		s.Format(ParcelLoadString(STRID_PROGRESS_BAR_TIME_S), sec);
	else if(time < 3600)
		s.Format(ParcelLoadString(STRID_PROGRESS_BAR_TIME_MS), min, sec);
	else
		s.Format(ParcelLoadString(STRID_PROGRESS_BAR_TIME_HMS), hour, min, sec);

	return s;
}

bool CProgressBarDlg::CheckStopPressed()
{
	MSG msg;

	ParcelProcess(false);
	while(::PeekMessage(&msg, GetDlgItem(IDC_STOP)->m_hWnd, 0, 0, PM_REMOVE))
	{
		GetDlgItem(IDC_STOP)->SendMessage(msg.message, msg.wParam, msg.lParam);
	}
	GetDlgItem(IDC_STOP)->UpdateWindow();
	ParcelProcess(true);

	return IsStopped;
}

bool CProgressBarDlg::CheckPausePressed()
{
	MSG msg;

	ParcelProcess(false);
	while(::PeekMessage(&msg, GetDlgItem(IDC_PAUSE)->m_hWnd, 0, 0, PM_REMOVE))
	{
		GetDlgItem(IDC_PAUSE)->SendMessage(msg.message, msg.wParam, msg.lParam);
	}
	GetDlgItem(IDC_PAUSE)->UpdateWindow();
	ParcelProcess(true);

	return IsPause;
}

void CProgressBarDlg::UpdateAllWindows()
{
	if(pParentWnd != NULL)
		pParentWnd->UpdateWindow();

	UpdateWindow();
}
