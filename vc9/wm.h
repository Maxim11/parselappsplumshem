#ifndef _WM_H_
#define _WM_H_

#define PARCEL_WM_FIRST					WM_USER + 5900

#define PARCEL_WM_SET_STATUS_BAR		PARCEL_WM_FIRST	+ 1

#define PARCEL_WM_OPEN_PROGRESS_BAR		PARCEL_WM_FIRST	+ 2
#define PARCEL_WM_CLOSE_PROGRESS_BAR	PARCEL_WM_FIRST	+ 3
#define PARCEL_WM_SET_PROGRESS_BAR		PARCEL_WM_FIRST	+ 4

#define PARCEL_WM_PROGRESS_TIMER		PARCEL_WM_FIRST + 5
#define PARCEL_WM_EXIT_TIMER			PARCEL_WM_FIRST + 6
#define PARCEL_WM_PAUSE_TIMER			PARCEL_WM_FIRST + 7
#define PARCEL_WM_OPEN_TIMER			PARCEL_WM_FIRST + 8

LRESULT ParcelSendMDIFrame(UINT Message, WPARAM wParam = 0, LPARAM lParam = 0);

void ParcelSetStatusBar(int Index, CString pText);

void ParcelOpenProgressBar(int min, int max, int step);
void ParcelCloseProgressBar();
void ParcelSetProgressBar(int pos);
void ParcelSetProgressBar();


#endif
