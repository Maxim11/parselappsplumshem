#ifndef _PARCEL_PARAM_DATA_H_
#define _PARCEL_PARAM_DATA_H_

#pragma once

#include "PRDBProject.h"


///////////////////////////////////////////////////////////////////////////////////////////////////
// ����� ��� �������� ����������

class CParamData
{
public:

	CParamData(HTREEITEM hParent);	// �����������
	~CParamData();					// ����������

	void Copy(CParamData& data);	// ����������� ������

	HTREEITEM hParentItem;			// ������ ���� � ������, ���������������� ������������� �������

protected:

	CString Name;					// ��� ����������

	bool UseStdMoisture;			// ������� ������������� ����������� ���������
	double StdMoisture;				// ����������� ���������
	CString Unit;					// ������� ���������
	int IFormat;					// ������ ������ (����� ������ ����� �������)

public:

	HTREEITEM GetParentHItem();			// �������� ������ ���� � ������, ��������������� �������
	void SetParentHItem(HTREEITEM item);

	CString GetName();					// �������� ��� ����������
	void SetName(CString name);			// ���������� ��� ����������

	bool IsUseStdMoisture();			// �������� ������� ������������� ����������� ���������
	void SetUseStdMoisture(bool flag);	// ���������� ������� ������������� ����������� ���������

	double GetStdMoisture();			// �������� ����������� ���������
	void SetStdMoisture(double val);	// ���������� ����������� ���������
	CString GetStdMoistureStr();		// �������� ����������� ��������� ��� ������

	CString GetUnit();					// �������� ������� ���������
	void SetUnit(CString str);			// ���������� ������� ���������

	int GetIFormat();					// �������� ������ ������ � ���� ����� ������ ����� �������
	CString GetFormat();				// �������� ������ ������ � ���� ������
	void SetFormat(int dig);			// ���������� ������ ������
	void SetFormat(CString fmt);

	void GetDataForBase(prdb::PRIndex& Obj);
	void SetDataFromBase(prdb::PRIndex& Obj);
};

#endif
