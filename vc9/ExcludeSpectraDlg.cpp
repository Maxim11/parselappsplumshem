#include "stdafx.h"

#include "BranchInfo.h"
#include "ExcludeSpectraDlg.h"
#include "ShowSpectraDlg.h"
#include "LoadSpectraPBDlg.h"
#include "MainFrm.h"


IMPLEMENT_DYNAMIC(CExcludeSpectraDlg, CDialog)

CExcludeSpectraDlg::CExcludeSpectraDlg(CProjectData* PrjData, ModelSample* ModSample, int indT, CWnd* pParent)
	: CDialog(CExcludeSpectraDlg::IDD, pParent)
{
	ParentPrj = PrjData;
	ModSam = ModSample;
	indTrans = indT;

	CurSpec = ModSam->Spectra.begin();

	IsBtnClk = false;
}

CExcludeSpectraDlg::~CExcludeSpectraDlg()
{
}

void CExcludeSpectraDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_SPECTRUM_LIST, m_list);
}


BEGIN_MESSAGE_MAP(CExcludeSpectraDlg, CDialog)

	ON_BN_CLICKED(IDC_ACCEPT, OnAccept)
	ON_BN_CLICKED(IDHELP, OnHelp)
	ON_BN_CLICKED(IDC_EXCLUDE, OnExclude)
	ON_BN_CLICKED(IDC_DETAIL, OnShow)

	ON_NOTIFY(LVN_ITEMCHANGED, IDC_SPECTRUM_LIST, OnSpecListSelChange)

END_MESSAGE_MAP()


BOOL CExcludeSpectraDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	int i;
	CString s, s1 = cEmpty;
	for(i=0; i<indTrans; i++)
		s1 += '*';
	s1 += ModSam->SampleName;
	s.Format(ParcelLoadString(STRID_EXCLUDE_SPECTRA_HDR), s1);
	SetWindowText(s);

	s = ParcelLoadString(STRID_EXCLUDE_SPECTRA_COLSPNUM);
	m_list.InsertColumn(0, s, LVCFMT_LEFT, 0, 0);
	s = ParcelLoadString(STRID_EXCLUDE_SPECTRA_COLSPUSE);
	m_list.InsertColumn(1, s, LVCFMT_RIGHT, 0, 2);
	s = ParcelLoadString(STRID_EXCLUDE_SPECTRA_COLSPDATE);
	m_list.InsertColumn(2, s, LVCFMT_RIGHT, 0, 1);
	for(i=0; i<3; i++)
		m_list.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
	m_list.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

	FillList();

	GetDlgItem(IDC_ACCEPT)->SetWindowText(ParcelLoadString(STRID_EXCLUDE_SPECTRA_ACCEPT));
	GetDlgItem(IDCANCEL)->SetWindowText(ParcelLoadString(STRID_EXCLUDE_SPECTRA_DECLINE));
	GetDlgItem(IDHELP)->SetWindowText(ParcelLoadString(STRID_EXCLUDE_SPECTRA_HELP));
	GetDlgItem(IDC_DETAIL)->SetWindowText(ParcelLoadString(STRID_EXCLUDE_SPECTRA_PLOT));

	GetDlgItem(IDC_DETAIL)->EnableWindow(!ModSam->IsTransN());

	GetDlgItem(IDHELP)->EnableWindow(pFrame->IsExistHelp());

	return true;
}

void CExcludeSpectraDlg::OnAccept()
{
	CDialog::OnOK();
}

void CExcludeSpectraDlg::OnCancel()
{
	if(IsChanged())
		if(!ParcelConfirm(ParcelLoadString(STRID_EXCLUDE_SPECTRA_ASK_DOCANCEL), false))
			return;

	CDialog::OnCancel();
}

void CExcludeSpectraDlg::OnExclude()
{
	if(CurSpec == ModSam->Spectra.end())
		return;

	CurSpec->second = !CurSpec->second;

	CString s = (CurSpec->second) ? ParcelLoadString(STRID_EXCLUDE_SPECTRA_EXCLUDE)
		: ParcelLoadString(STRID_EXCLUDE_SPECTRA_INCLUDE);
	GetDlgItem(IDC_EXCLUDE)->SetWindowText(s);

	int sel = m_list.GetNextItem(-1, LVNI_SELECTED);
	if(sel < 0 || sel >= int(ModSam->Spectra.size()))
		return;

	s = (CurSpec->second) ? ParcelLoadString(STRID_EXCLUDE_SPECTRA_YES) : ParcelLoadString(STRID_EXCLUDE_SPECTRA_NO);
	m_list.SetItemText(sel, 1, s);

	IsBtnClk = true;
}

void CExcludeSpectraDlg::OnShow()
{
	CTransferData* pTrans = ParentPrj->GetTransfer(ModSam->TransName);
	if(pTrans == NULL || ModSam->IsTransN())
		return;
	CSampleData* Sam = pTrans->GetSample(ModSam->SampleName);
	if(Sam == NULL)
		return;

	VLSI PSam;
	LoadSampleInfo NewLSI;
	NewLSI.pSam = Sam;
	ItIntBool it;
	for(it=ModSam->Spectra.begin(); it!=ModSam->Spectra.end(); ++it)
		NewLSI.Nums.push_back(it->first);

	PSam.push_back(NewLSI);

	CLoadSpectraPBDlg dlg2(&PSam, this);
	if(dlg2.DoModal() != IDOK)
	{
		Sam->FreeSpectraData();
		return;
	}

	CShowSpectra ShowSp(&pTrans->SpecPoints);

	int i;
	for(i=0, it=ModSam->Spectra.begin(); it!=ModSam->Spectra.end(); ++it, i++)
	{
		Spectrum* Spec = Sam->GetSpectrumByNum(it->first);
		if(Spec == NULL)
			continue;
		bool issel = (m_list.GetItemState(i, LVIS_SELECTED) == LVIS_SELECTED);

		SpectraShowSettings* NewSS = ShowSp.AddSpectraShow(ModSam->SampleName, it->first, indTrans, 0, issel);
		if(NewSS == NULL)
			continue;

		copy(Spec->Data.begin(), Spec->Data.end(), inserter(NewSS->SpecData, NewSS->SpecData.begin()));
	}

	CShowSpectraDlg dlg(&ShowSp, C_PLOT_SET_MODE_SAMPLE, this);
	dlg.DoModal();

	Sam->FreeSpectraData();
}

void CExcludeSpectraDlg::OnSpecListSelChange(NMHDR* /*pNMHDR*/, LRESULT *pResult)
{
	int sel = m_list.GetNextItem(-1, LVNI_SELECTED);
	if(sel < 0 || sel >= int(ModSam->Spectra.size()))
		return;

	CurSpec = ModSam->Spectra.begin();
	for(int i=0; i<sel; i++)
		++CurSpec;

	CString s = (CurSpec->second) ? ParcelLoadString(STRID_EXCLUDE_SPECTRA_EXCLUDE)
		: ParcelLoadString(STRID_EXCLUDE_SPECTRA_INCLUDE);
	GetDlgItem(IDC_EXCLUDE)->SetWindowText(s);

	*pResult = 0;
}

void CExcludeSpectraDlg::FillList()
{
	m_list.DeleteAllItems();

	CString s;
	ItIntBool it;
	int i;
	for(i=0, it=ModSam->Spectra.begin(); it!=ModSam->Spectra.end(); ++it, i++)
	{
		CTransferData *Trans = ParentPrj->GetTransfer(ModSam->TransName);
		CSampleData *Sam = Trans->GetSample(ModSam->SampleName);
		if(Sam == NULL)
			return;
		Spectrum *Spec = Sam->GetSpectrumByNum(it->first);

		s.Format(cFmt02, Spec->Num);
		m_list.InsertItem(i, s);

		s = (it->second) ? ParcelLoadString(STRID_EXCLUDE_SPECTRA_YES) : ParcelLoadString(STRID_EXCLUDE_SPECTRA_NO);
		m_list.SetItemText(i, 1, s);
		s = Spec->DateTime.Format(cFmtDate);
		m_list.SetItemText(i, 2, s);
	}
	for(i=0; i<3; i++)
		m_list.SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);

	m_list.SetItemState(0, LVIS_SELECTED, LVIS_SELECTED);
}

bool CExcludeSpectraDlg::IsChanged()
{
	return IsBtnClk;
}

void CExcludeSpectraDlg::OnHelp()
{
	int IdHelp = DLGID_SAMPLE_SPECTRA;

	pFrame->ShowHelp(IdHelp);
}
