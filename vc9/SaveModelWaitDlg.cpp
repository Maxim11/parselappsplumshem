#include "stdafx.h"

#include "SaveModelWaitDlg.h"
#include "BranchInfo.h"

IMPLEMENT_DYNAMIC(CSaveModelWaitDlg, CWaitingDlg)

CSaveModelWaitDlg::CSaveModelWaitDlg(CProjectData* prj, CModelData* mod, CModelData* old, CWnd* pParent) : CWaitingDlg(pParent)
{
	PrjData = prj;
	ModData = mod;
	OldMod = old;

	Comment = ParcelLoadString(STRID_SAVE_MODEL_WAIT_COMMENT);
}

CSaveModelWaitDlg::~CSaveModelWaitDlg()
{
}

void CSaveModelWaitDlg::DoDataExchange(CDataExchange* pDX)
{
	CWaitingDlg::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CSaveModelWaitDlg, CWaitingDlg)
END_MESSAGE_MAP()


BOOL CSaveModelWaitDlg::OnInitDialog()
{
	CWaitingDlg::OnInitDialog();

	return true;
}

void CSaveModelWaitDlg::OK()
{
	CWaitingDlg::OK();
}

void CSaveModelWaitDlg::Stop()
{
	CWaitingDlg::Stop();
}

void CSaveModelWaitDlg::Work()
{
	ParcelWait(true);
	
	bool res = (PrjData->AddExistingModel(*ModData, OldMod) != NULL);

	ParcelWait(false);

	if(res)
		OK();
	else
		Stop();
}
