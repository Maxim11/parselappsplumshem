#ifndef _PARCEL_CREATE_TRANSFER_ALONE_H_
#define _PARCEL_CREATE_TRANSFER_ALONE_H_

#pragma once

#include "TransferCalculate.h"

///////////////////////////////////////////////////////////////////////////////////////////////////
class CCreateTransferAlone
{

	bool AcceptData(CString strName);				// ������� ��������� ������

public:

	CCreateTransferAlone(CTransferData* trans, CDeviceData* pDev, CProjectData* pPrj);	// �����������
	bool Create(CString strName);					

protected:

	CProjectData* PrjData;			// ��������� �� ������������ ������ �������
	CTransferCalculate CalcInfo;	// ����� ������� ������ ������������ ��������

	CDeviceData* MDev;				// ��������� �� ������-������
	CProjectData* MPrj;				// ��������� �� ������ ������-�������

};

#endif
