#include "stdafx.h"

#include "RootData.h"
#include "DeviceData.h"
#include "CreateProjectDlg.h"
#include "ConvertAcceptDlg.h"
#include "ConvertWaitDlg.h"
#include "ConvertReadWaitDlg.h"
#include "CopyProjectWaitDlg.h"
#include "ImportSpectraDlg.h"
#include "ReadApjxWaitDlg.h"
#include "DeleteProjectWaitDlg.h"
#include "LoadSpectraPBDlg.h"
#include "BranchInfo.h"

#include <math.h>

static int num0 = 1001;

CDeviceData::CDeviceData()
{
	hItem = NULL;

	SetType(C_DEV_TYPE_10M);
	SetNumber(0);
}

CDeviceData::~CDeviceData()
{
	Projects.clear();
	ResolList.clear();
}

void CDeviceData::Copy(CDeviceData& data)
{
	int i;

	ResolList.clear();
	for(i=0; i<data.GetNResolutions(); i++)
		ResolList.push_back(data.GetResolutionFromList(i));

	hItem = data.GetHItem();

	Type = data.GetType();
	Number = data.GetNumber();
}

HTREEITEM CDeviceData::GetHItem()
{
	return hItem;
}

void CDeviceData::SetHItem(HTREEITEM item)
{
	hItem = item;
}

int CDeviceData::GetType()
{
	return Type;
}

void CDeviceData::SetType(int ind)
{
	Type = ind;
	SetResolList();
}

CString CDeviceData::GetTypeStr()
{
	return cGetDeviceTypeName(Type);
}

int CDeviceData::GetNumber()
{
	return Number;
}

void CDeviceData::SetNumber(int num)
{
	Number = num;
}

CString CDeviceData::GetName()
{
	CString s, s1;

	s1.Format(cFmt08, Number);

	s += GetTypeStr() + ParcelLoadString(STRID_FIELD_NUM) + s1;

	return s;
}

int CDeviceData::GetNProjects()
{
	return int(Projects.size());
}

CProjectData* CDeviceData::GetProject(HTREEITEM item)
{
	ItPrj it;
	for(it=Projects.begin(); it!=Projects.end(); ++it)
		if((*it).GetHItem() == item)
			return &(*it);

	return NULL;
}

CProjectData* CDeviceData::GetProject(CString name)
{
	ItPrj it;
	for(it=Projects.begin(); it!=Projects.end(); ++it)
		if(!(*it).GetName().CompareNoCase(name))
			return &(*it);

	return NULL;
}

CProjectData* CDeviceData::GetProject(int ind)
{
	if(ind < 0 || ind >= int(Projects.size()))
		return NULL;

	int i;
	ItPrj it;
	for(i=0, it=Projects.begin(); i<ind; i++)
		++it;

	return &(*it);
}

CProjectData* CDeviceData::GetLastProject()
{
	return GetProject(GetNProjects() - 1);
}

HTREEITEM CDeviceData::GetLastProjectHItem()
{
	CProjectData* PrjData = GetLastProject();
	if(PrjData)
		return PrjData->GetHItem();

	return NULL;
}

CProjectData* CDeviceData::AddNewProject(CTreeCtrl* curTree)
{
	CProjectData NewPrj(hItem);

	CCreateProjectDlg dlg(&NewPrj, PRJDLG_MODE_CREATE);
	if(dlg.DoModal() != IDOK)
		return NULL;

	pTree = curTree;
	pNewCopyPrj = &NewPrj;

	CCopyProjectWaitDlg dlg2(this);
	if(dlg2.DoModal() != IDOK)
		return NULL;

	return GetLastProject();
}

CProjectData* CDeviceData::CreateSSTProject(CTreeCtrl* curTree)
{
	int i;
	CString s, s1;

	CProjectData NewPrj(hItem, true);

	NewPrj.SetName(GetSSTName());

	CCreateProjectDlg dlg(&NewPrj, PRJDLG_MODE_CREATE_SST);
	if(dlg.DoModal() != IDOK)
		return NULL;

	pTree = curTree;
	pNewCopyPrj = &NewPrj;

	if(!GetDatabase()->BeginTrans())
		return false;

	prdb::PRAnalisisProject Obj;
	pNewCopyPrj->GetDataForBase(Obj);
	if(!GetDatabase()->ProjectAddEx(Number, cEmpty, &Obj))
	{
		GetDatabase()->Rollback();
		return false;
	}

	GetDatabase()->CommitTrans();

	HTREEITEM hLastItem = (GetLastProject() != NULL) ? GetLastProject()->GetHItem() : 0;

	Projects.push_back(*pNewCopyPrj);
	CProjectData *Prj = GetLastProject();

	CString NameParam = ParcelLoadString(STRID_TREE_PARSET);
	CString NameSmpl = ParcelLoadString(STRID_TREE_SAMSET);
	CString NameMod = ParcelLoadString(STRID_TREE_MODSET);
	CString NameMtd = ParcelLoadString(STRID_TREE_MTDSET);
	CString NameSmplOwn = ParcelLoadString(STRID_TREE_SAMOWN);

	HTREEITEM hPrjItem = pTree->InsertItem(TVIF_TEXT, Prj->GetName(), 0, 0, 0, 0, 0,
		hItem, hLastItem);
	HTREEITEM hParamItem = pTree->InsertItem(TVIF_TEXT, NameParam, 0, 0, 0, 0, 0, hPrjItem, NULL);
	HTREEITEM hSmplItem = pTree->InsertItem(TVIF_TEXT, NameSmpl, 0, 0, 0, 0, 0, hPrjItem, hParamItem);
	HTREEITEM hModItem = pTree->InsertItem(TVIF_TEXT, NameMod, 0, 0, 0, 0, 0, hPrjItem, hSmplItem);
	HTREEITEM hMtdItem = pTree->InsertItem(TVIF_TEXT, NameMtd, 0, 0, 0, 0, 0, hPrjItem, hModItem);
	HTREEITEM hSmplOwnItem = pTree->InsertItem(TVIF_TEXT, NameSmplOwn, 0, 0, 0, 0, 0, hSmplItem, NULL);

	Prj->SetHItem(hPrjItem);
	Prj->SetParentHItem(hItem);
	Prj->SetParamSetHItem(hParamItem);
	Prj->SetSampleSetHItem(hSmplItem);
	Prj->SetModelSetHItem(hModItem);
	Prj->SetMethodSetHItem(hMtdItem);

	pTree->SetItemImage(hPrjItem, 4, 4);
	pTree->SetItemImage(hParamItem, 10, 10);
	pTree->SetItemImage(hSmplItem, 6, 6);
	pTree->SetItemImage(hModItem, 10, 10);
	pTree->SetItemImage(hMtdItem, 10, 10);
	pTree->SetItemImage(hSmplOwnItem, 11, 11);

	pTree->Select(hPrjItem, TVGN_CARET);
	pTree->Expand(hItem, TVE_EXPAND);
	pTree->SetItemImage(hItem, 3, 3);

	Prj->CalcSpecPoints();

	Prj->AddOwnTrans();
	Prj->GetTransOwn()->SetHItem(hSmplOwnItem);

	for(i=0; i<3; i++)
	{
		CParamData NewPar(hPrjItem);

		s.Format(ParcelLoadString(STRID_SSTNAMES_PARAM), i + 1);
		NewPar.SetName(s);
		NewPar.SetFormat(3);
		NewPar.SetUnit(ParcelLoadString(STRID_SSTNAMES_UNIT));
		
		Prj->AddParam(&NewPar);
	}

	CTransferData* pTransOwn = Prj->GetTransOwn();
	for(i=0; i<5; i++)
	{
		CSampleData NewSam(hPrjItem, pTransOwn->GetHItem());

		CString fmtS = ParcelLoadString(STRID_SSTNAMES_SAMPLE) + cFmt1;
		CString fmtS1 = ParcelLoadString(STRID_SSTNAMES_SAMPLE) + cFmt08;
		switch(i)
		{
		case 0:
			s.Format(fmtS, 1);
			s1 = ParcelLoadString(STRID_SSTNAMES_COMMENT1);
			break;
		case 1:
			s.Format(fmtS, 2);
			s1 = ParcelLoadString(STRID_SSTNAMES_COMMENT2);
			break;
		case 2:
			s.Format(fmtS, 3);
			s1 = ParcelLoadString(STRID_SSTNAMES_COMMENT3);
			break;
		case 3:
			s.Format(fmtS, 0);
			s1 = ParcelLoadString(STRID_SSTNAMES_COMMENT0);
			break;
		case 4:
			s.Format(fmtS, Number);
			s1 = cEmpty;
			break;
		}
		NewSam.SetName(s);
		NewSam.SetIden2(s1);

		pTransOwn->AddSample(&NewSam);
	}

	return GetLastProject();
}

bool CDeviceData::AcceptCopyPrj() 
{
	if(pNewCopyPrj == NULL)
		return false;

	int i, j;

	if(!GetDatabase()->BeginTrans())
		return false;

	prdb::PRAnalisisProject Obj;
	pNewCopyPrj->GetDataForBase(Obj);
	if(!GetDatabase()->ProjectAddEx(Number, cEmpty, &Obj))
	{
		GetDatabase()->Rollback();
		return false;
	}

	GetDatabase()->CommitTrans();

	HTREEITEM hLastItem = (GetLastProject() != NULL) ? GetLastProject()->GetHItem() : 0;

	Projects.push_back(*pNewCopyPrj);
	CProjectData *Prj = GetLastProject();

	CString NameParam = ParcelLoadString(STRID_TREE_PARSET);
	CString NameSmpl = ParcelLoadString(STRID_TREE_SAMSET);
	CString NameMod = ParcelLoadString(STRID_TREE_MODSET);
	CString NameMtd = ParcelLoadString(STRID_TREE_MTDSET);
	CString NameSmplOwn = ParcelLoadString(STRID_TREE_SAMOWN);

	HTREEITEM hPrjItem = pTree->InsertItem(TVIF_TEXT, Prj->GetName(), 0, 0, 0, 0, 0,
		hItem, hLastItem);
	HTREEITEM hParamItem = pTree->InsertItem(TVIF_TEXT, NameParam, 0, 0, 0, 0, 0, hPrjItem, NULL);
	HTREEITEM hSmplItem = pTree->InsertItem(TVIF_TEXT, NameSmpl, 0, 0, 0, 0, 0, hPrjItem, hParamItem);
	HTREEITEM hModItem = pTree->InsertItem(TVIF_TEXT, NameMod, 0, 0, 0, 0, 0, hPrjItem, hSmplItem);
	HTREEITEM hMtdItem = pTree->InsertItem(TVIF_TEXT, NameMtd, 0, 0, 0, 0, 0, hPrjItem, hModItem);
	HTREEITEM hSmplOwnItem = pTree->InsertItem(TVIF_TEXT, NameSmplOwn, 0, 0, 0, 0, 0, hSmplItem, NULL);

	Prj->SetHItem(hPrjItem);
	Prj->SetParentHItem(hItem);
	Prj->SetParamSetHItem(hParamItem);
	Prj->SetSampleSetHItem(hSmplItem);
	Prj->SetModelSetHItem(hModItem);
	Prj->SetMethodSetHItem(hMtdItem);

	pTree->SetItemImage(hPrjItem, 4, 4);
	pTree->SetItemImage(hParamItem, 10, 10);
	pTree->SetItemImage(hSmplItem, 6, 6);
	pTree->SetItemImage(hModItem, 10, 10);
	pTree->SetItemImage(hMtdItem, 10, 10);
	pTree->SetItemImage(hSmplOwnItem, 11, 11);

	pTree->Select(hPrjItem, TVGN_CARET);
	pTree->Expand(hItem, TVE_EXPAND);
	pTree->SetItemImage(hItem, 3, 3);

	Prj->CalcSpecPoints();

	Prj->AddOwnTrans();
	Prj->GetTransOwn()->SetHItem(hSmplOwnItem);

	if(Prj->CopyMode >= C_PRJ_COPY_WITHPARAMS)
	{
		ItPar itP;
		for(itP=Prj->pCopies->Params.begin(); itP!=Prj->pCopies->Params.end(); ++itP)
			Prj->AddParam(&(*itP));

		if(Prj->CopyMode >= C_PRJ_COPY_WITHSAMPLES)
		{
			bool withoutspec = (Prj->CopyMode == C_PRJ_COPY_WITHSAMPLES);

			for(i=0; i<Prj->pCopies->GetNTransfers(); i++)
			{
				CTransferData* pTrans = Prj->pCopies->GetTransfer(i);
				if(Prj->CopyMode < C_PRJ_COPY_WITHTRANS && pTrans->GetType() != C_TRANS_TYPE_OWN)
					continue;

				CTransferData *pNewTrans = NULL;
				if(pTrans->GetType() != C_TRANS_TYPE_OWN)
				{
					HTREEITEM hLastTransItem = Prj->GetLastTransferHItem();

					Prj->AddTransfer(pTrans);
					pNewTrans = Prj->GetLastTransfer();
					pNewTrans->Samples.clear();

					HTREEITEM hNewTransItem = pTree->InsertItem(TVIF_TEXT, pNewTrans->GetName(), 0, 0, 0, 0, 0,
						hSmplItem, hLastTransItem);

					pNewTrans->SetHItem(hNewTransItem);
					pNewTrans->SetParentHItem(Prj->GetHItem());

					pTree->SetItemImage(hNewTransItem, 11, 11);

					pTree->Select(hNewTransItem, TVGN_CARET);
					pTree->Expand(hSmplItem, TVE_EXPAND);
					pTree->SetItemImage(hSmplItem, 7, 7);
				}
				else
					pNewTrans = Prj->GetLastTransfer();

				for(j=0; j<pTrans->GetNSamples(); j++)
				{
					CSampleData* pSam = pTrans->GetSample(j);
					if(!withoutspec && Prj->CopyMode != C_PRJ_COPY_WITHALL)
						pSam->LoadSpectraData();

					CSampleData NewSam(Prj->GetHItem(), pNewTrans->GetHItem());
					NewSam.Copy((*pSam), withoutspec);

					pNewTrans->AddSample(&NewSam);

					pSam->FreeSpectraData();
				}

				CSampleData* pArt = pTrans->GetArtSample();
				if(pArt != NULL)
				{
					if(!withoutspec && Prj->CopyMode != C_PRJ_COPY_WITHALL)
						pArt->LoadSpectraData();

					CSampleData NewSam(Prj->GetHItem(), pNewTrans->GetHItem());
					NewSam.Copy((*pArt), withoutspec);

					pNewTrans->AddSample(&NewSam);

					pArt->FreeSpectraData();
				}
			}

			if(Prj->CopyMode == C_PRJ_COPY_WITHALL)
			{
				ItMod itM;
				for(itM=Prj->pCopies->Models.begin(); itM!=Prj->pCopies->Models.end(); ++itM)
				{
					HTREEITEM hParentItem = Prj->GetModelSetHItem();
					hLastItem = Prj->GetLastModelHItem();
					CModelData* Model = Prj->AddExistingModel(*itM);
					if(Model == NULL)
						return false;

					HTREEITEM hModelItem = pTree->InsertItem(TVIF_TEXT, Model->GetName(),
						0, 0, 0, 0, 0, hParentItem, hLastItem);

					Model->SetHItem(hModelItem);
					Model->SetParentHItem(Prj->GetHItem());
					pTree->SetItemImage(hModelItem, 11, 11);
					pTree->SetItemImage(hModItem, 6, 6);
				}

				ItMtd itMt;
				for(itMt=Prj->pCopies->Methods.begin(); itMt!=Prj->pCopies->Methods.end(); ++itMt)
				{
					HTREEITEM hParentItem = Prj->GetMethodSetHItem();
					hLastItem = Prj->GetLastMethodHItem();
					CMethodData* Method = Prj->AddExistingMethod(*itMt);
					if(Method == NULL)
						return false;

					HTREEITEM hMethodItem = pTree->InsertItem(TVIF_TEXT, Method->GetName(),
						0, 0, 0, 0, 0, hParentItem, hLastItem);

					Method->SetHItem(hMethodItem);
					Method->SetParentHItem(Prj->GetHItem());
					pTree->SetItemImage(hMethodItem, 11, 11);
					pTree->SetItemImage(hMtdItem, 6, 6);
				}
			}
		}
	}

	return true;
}

bool CDeviceData::ChangeProjectSettings(HTREEITEM item)
{
	CProjectData* OldPrj = GetProject(item);
	if(OldPrj == NULL)
		return false;

	CProjectData NewPrj(hItem);
	NewPrj.CopyMode = C_PRJ_COPY_FORCHANGE;
	NewPrj.Copy(*OldPrj);

	int mode = (OldPrj->IsSST()) ? PRJDLG_MODE_CHANGE_SST : PRJDLG_MODE_CHANGE;
	CCreateProjectDlg dlg(&NewPrj, mode);
	if(dlg.DoModal() != IDOK)
		return false;

	if(!GetDatabase()->BeginTrans())
		return NULL;

	prdb::PRAnalisisProject Obj;
	NewPrj.GetDataForBase(Obj);
	if(!GetDatabase()->ProjectAddEx(Number, OldPrj->GetName(), &Obj))
	{
		GetDatabase()->Rollback();
		return false;
	}
	if(!NewPrj.ChangeOwnTransfer())
	{
		GetDatabase()->Rollback();
		return false;
	}

	GetDatabase()->CommitTrans();

	OldPrj->Copy(NewPrj);

	return true;
}

bool CDeviceData::DeleteProject(HTREEITEM item)
{
	ItPrj it;
	for(it=Projects.begin(); it!=Projects.end(); ++it)
		if((*it).GetHItem() == item)
		{
			if(!DeleteProject(it->GetName()))
				return false;

			Projects.erase(it);
			return true;
		}

	return false;
}

bool CDeviceData::DeleteProject(CString name)
{
	CDeleteProjectWaitDlg dlg(this, name);
	if(dlg.DoModal() != IDOK)
		return false;

	return true;
}

CProjectData* CDeviceData::AddOldApj(CString FileName, CTreeCtrl* curTree, bool IsSST)
{
	CLoadOldDatas CLoad;
	ProjectOldData OldApj;
	OldApj.DevNum = Number;

	CConvertReadWaitDlg dlg3(&CLoad, &OldApj, FileName);
	if(dlg3.DoModal() != IDOK)
		return NULL;

	CConvertAcceptDlg dlg(&CLoad);
	if(dlg.DoModal() != IDOK)
		return NULL;

	if(CLoad.Checks.EmptyUnit == C_CONVERT_EMPTYUNIT_EXIST)
	{
		vector<ParamOldData>::iterator it;
		for(it=OldApj.Params.begin(); it!=OldApj.Params.end(); ++it)
			if(it->Unit.IsEmpty())
				it->Unit = CLoad.Checks.ReplaceUnit;
	}

	without = (CLoad.Checks.WithoutNumber == C_CONVERT_WITHOUT_DECLINE);
	check_wave = (CLoad.Checks.BadWave == C_CONVERT_BADWAVE_DECLINE);
	Apj = &OldApj;
	pTree = curTree;

	CConvertWaitDlg dlg2(this, IsSST);
	if(dlg2.DoModal() != IDOK)
		return NULL;

	return GetLastProject();
}

bool CDeviceData::AcceptOldApj(bool IsSST)
{
	CString s;

	CProjectData NewPrj(hItem, IsSST);

	int indres = GetResolutionIndByMean(Apj->RealRes);
	int shift = indres - Apj->ResCode;

	CString NewPrjName = (IsSST) ? GetSSTName() : Apj->Name;
	NewPrj.SetName(NewPrjName);
	NewPrj.SetLowLimSpecRange(Apj->LowSpec);
	NewPrj.SetUpperLimSpecRange(Apj->UpperSpec);
	NewPrj.SetSubCanalResolution(Apj->ResCode + shift);
	NewPrj.SetSubCanalApodization(Apj->Apodize);
	NewPrj.SetUseRefCanal(Apj->UseRef);
	NewPrj.SetRefCanalResolution(Apj->ResCodeRef + shift);
	NewPrj.SetRefCanalApodization(Apj->ApodizeRef);
	NewPrj.SetZeroFillingInd(Apj->ZeroFilling);
	NewPrj.SetNScansSubsample(Apj->NScanSample);
	NewPrj.SetNMeasuresSubsample(Apj->NMeasSample);
	NewPrj.SetNScansStandart(Apj->NScanStd);
	NewPrj.SetNMeasuresStandart(Apj->NMeasStd);
	if(Type == C_DEV_TYPE_40 || Type == C_DEV_TYPE_12M)
	{
		if(Apj->BathLength >= 6 && Apj->BathLength <= 35)
			NewPrj.SetBathLength(Apj->BathLength);
		NewPrj.SetLowLimTemp(Apj->MinTemp);
		NewPrj.SetUpperLimTemp(Apj->MaxTemp);
		NewPrj.SetMaxDiffTemp(Apj->DiffTemp);
	}

	if(!GetDatabase()->BeginTrans())
		return false;

	prdb::PRAnalisisProject Obj;
	NewPrj.GetDataForBase(Obj);
	if(!GetDatabase()->ProjectAddEx(Number, cEmpty, &Obj))
	{
		GetDatabase()->Rollback();
		return false;
	}

	GetDatabase()->CommitTrans();


	HTREEITEM hLastItem = (GetLastProject() != NULL) ? GetLastProject()->GetHItem() : 0;

	Projects.push_back(NewPrj);
	CProjectData *Prj = GetLastProject();

	CString NameParam = ParcelLoadString(STRID_TREE_PARSET);
	CString NameSmpl = ParcelLoadString(STRID_TREE_SAMSET);
	CString NameMod = ParcelLoadString(STRID_TREE_MODSET);
	CString NameMtd = ParcelLoadString(STRID_TREE_MTDSET);
	CString NameSmplOwn = ParcelLoadString(STRID_TREE_SAMOWN);

	HTREEITEM hPrjItem = pTree->InsertItem(TVIF_TEXT, NewPrj.GetName(), 0, 0, 0, 0, 0,
		hItem, hLastItem);
	HTREEITEM hParamItem = pTree->InsertItem(TVIF_TEXT, NameParam, 0, 0, 0, 0, 0, hPrjItem, NULL);
	HTREEITEM hSmplItem = pTree->InsertItem(TVIF_TEXT, NameSmpl, 0, 0, 0, 0, 0, hPrjItem, hParamItem);
	HTREEITEM hModItem = pTree->InsertItem(TVIF_TEXT, NameMod, 0, 0, 0, 0, 0, hPrjItem, hSmplItem);
	HTREEITEM hMtdItem = pTree->InsertItem(TVIF_TEXT, NameMtd, 0, 0, 0, 0, 0, hPrjItem, hModItem);
	HTREEITEM hSmplOwnItem = pTree->InsertItem(TVIF_TEXT, NameSmplOwn, 0, 0, 0, 0, 0, hSmplItem, NULL);

	Prj->SetHItem(hPrjItem);
	Prj->SetParamSetHItem(hParamItem);
	Prj->SetSampleSetHItem(hSmplItem);
	Prj->SetModelSetHItem(hModItem);
	Prj->SetMethodSetHItem(hMtdItem);

	pTree->SetItemImage(hPrjItem, 4, 4);
	pTree->SetItemImage(hParamItem, 10, 10);
	pTree->SetItemImage(hSmplItem, 6, 6);
	pTree->SetItemImage(hModItem, 10, 10);
	pTree->SetItemImage(hMtdItem, 10, 10);
	pTree->SetItemImage(hSmplOwnItem, 11, 11);

	pTree->Select(hPrjItem, TVGN_CARET);
	pTree->Expand(hItem, TVE_EXPAND);
	pTree->SetItemImage(hItem, 3, 3);

	Prj->CalcSpecPoints();

	Prj->AddOwnTrans();
	Prj->GetTransOwn()->SetHItem(hSmplOwnItem);

	vector<ParamOldData>::iterator it;
	for(it=Apj->Params.begin(); it!=Apj->Params.end(); ++it)
	{
		CParamData NewPar(Prj->GetHItem());
		NewPar.SetName(it->Name);
		NewPar.SetUnit(it->Unit);
		NewPar.SetFormat(it->IFormat);
		NewPar.SetStdMoisture(it->HumidRef);
		if(it->HumidRef < 1.e-6)  NewPar.SetUseStdMoisture(false);
		else  NewPar.SetUseStdMoisture(true);

		Prj->AddParam(&NewPar);
	}

	if(IsSST)
	{
		int i;
		for(i=0; i<3; i++)
		{
			s.Format(ParcelLoadString(STRID_SSTNAMES_PARAM), i + 1);
			if(Prj->GetParam(s) == NULL)
			{
				CParamData NewPar(Prj->GetHItem());
				NewPar.SetName(s);
				NewPar.SetFormat(3);
				NewPar.SetUnit(ParcelLoadString(STRID_SSTNAMES_UNIT));

				Prj->AddParam(&NewPar);
			}
		}
	}

	vector<SampleOldDataFull>::iterator itS;
	for(itS=Apj->Samples.begin(); itS!=Apj->Samples.end(); ++itS)
	{
		CSampleData NewSample(Prj->GetHItem(), Prj->GetTransOwnHItem());

		NewSample.SetName(itS->OldSam.Name);
		NewSample.SetIden2(itS->OldSam.Iden2);
		NewSample.SetIden3(cEmpty);

		ItROD itR;
		for(itR=itS->RefData.begin(); itR!=itS->RefData.end(); ++itR)
		{
			if(Prj->GetParam(itR->Name) == NULL)
				continue;

			NewSample.AddReferenceData(itR->Name);
			if(itR->Value < 0.)
				NewSample.SetReferenceAsNoValue(itR->Name);
			else
				NewSample.SetReferenceValue(itR->Name, itR->Value);
		}

		ItSOD itSp;
		for(itSp=itS->SpecData.begin(); itSp!=itS->SpecData.end(); ++itSp)
		{
			if(Prj->GetLowLimSpecRange() != itSp->PrjLowSpec ||
			   Prj->GetUpperLimSpecRange() != itSp->PrjUpperSpec)
			    continue;
			if(Type != C_DEV_TYPE_IMIT)
			{
				if((GetNumber() != itSp->DevNumber && (itSp->DevNumber > 0 || without)) ||
				   (check_wave && fabs(itSp->WaveLength - C_LASER_WAVE) > 1.e-9) ||
				   fabs(Prj->GetSubCanalResolutionMean() - itSp->PrjResolution) > 1.e-3 ||
				   Prj->GetSubCanalApodization() != itSp->PrjApodize ||
				   Prj->GetZeroFillingInd() != itSp->PrjZeroFilling)
					continue;
			}

			Spectrum NewSpec;
			NewSpec.bUse = true;
			NewSpec.DateTime = itSp->DateTime;
			NewSpec.Num = itSp->num;

			copy(itSp->SpecData.begin(), itSp->SpecData.end(), inserter(NewSpec.Data, NewSpec.Data.begin()));

			NewSample.SpecData.push_back(NewSpec);
		}

		if(NewSample.SpecData.size() <= 0)
			continue;

		if(!Prj->AddSample(&NewSample))
			return false;

		Prj->GetSample(NewSample.GetName())->FreeSpectraData();
	}

	vector<ModelOldData>::iterator itM;
	for(itM=Apj->Models.begin(); itM!=Apj->Models.end(); ++itM)
	{
		CModelData NewModel(Prj->GetHItem());

		NewModel.SetName(itM->ModelName);
		if(itM->AnType == 1)  NewModel.SetAnalysisType(C_ANALYSIS_QNT);
		if(itM->AnType == 2)
			continue;
		NewModel.SetTransNameByInd(0);

		ItMPOD itMP;
		for(itMP=itM->Params.begin(); itMP!=itM->Params.end(); ++itMP)
		{
			if(Prj->GetParam(itMP->MPName) == NULL)
				continue;

			ModelParam NewMPar(itMP->MPName);

			NewMPar.LimMSD = itMP->MPSKO;
			NewMPar.LowValue = itMP->MinRange;
			NewMPar.UpperValue = itMP->MaxRange;
			NewMPar.CorrA = itMP->Corr;
			NewMPar.CorrB = 1.;

			NewModel.AddModelParam(&NewMPar);
		}
		if(NewModel.GetNModelParams() <= 0)
			continue;

		NewModel.SetSamSortName(cEmpty);
		NewModel.SetSamSortDir(true);

		bool Error = false;
		ItMSOD itMS;
		for(itMS=itM->Calib.begin(); itMS!=itM->Calib.end(); ++itMS)
		{
			CSampleData* Sam = Prj->GetSample(itMS->Name);
			if(Sam == NULL)
			{
				Error = true;
				break;
			}

			ModelSample NewMSam(Sam);

			NewMSam.SetAll(false);
			for(int ii=0; ii<itMS->nSpec; ii++)
				NewMSam.Spectra[itMS->SpecNums[ii]] = true;

			NewModel.AddSampleForCalib(&NewMSam);
		}
		if(Error || NewModel.GetNSamplesForCalib() <= 0)
			continue;

		for(itMS=itM->Valid.begin(); itMS!=itM->Valid.end(); ++itMS)
		{
			CSampleData* Sam = Prj->GetSample(itMS->Name);
			if(Sam == NULL)
			{
				Error = true;
				break;
			}

			ModelSample NewMSam(Sam);

			NewMSam.SetAll(false);
			for(int ii=0; ii<itMS->nSpec; ii++)
				NewMSam.Spectra[itMS->SpecNums[ii]] = true;

			NewModel.AddSampleForValid(&NewMSam);
		}
		if(Error)
			continue;

		NewModel.CalcModParamLimits();

		if(itM->SpecType == 0)  NewModel.SetSpectrumType(C_MOD_SPECTRUM_TRA);
		if(itM->SpecType == 1)  NewModel.SetSpectrumType(C_MOD_SPECTRUM_ABS);

		CString preproc;
		if(itM->IsAvr)  preproc = cPreprocs[1];
		preproc += itM->Preproc;
		NewModel.SetAllPreproc(preproc);

		NewModel.SetLowSpecRange(itM->LowSpecRange);
		NewModel.SetUpperSpecRange(itM->UpperSpecRange);
		NewModel.GetSpecPoints();

		if(itM->ModelType == 0)  NewModel.SetModelType(C_MOD_MODEL_PLS);
		if(itM->ModelType == 1)  NewModel.SetModelType(C_MOD_MODEL_PCR);

		if(NewModel.GetAnalysisType() == C_ANALYSIS_QNT)
			NewModel.SetNCompQnt(itM->NComp);
		NewModel.SetMaxMah(itM->MaxMah);

		NewModel.SetUseSECV(true);

		HTREEITEM hParentItem = Prj->GetModelSetHItem();
		hLastItem = Prj->GetLastModelHItem();
		CModelData* Model = Prj->AddExistingModel(NewModel);
		if(Model == NULL)
		{
			return false;
		}

		Model->FillModInfo();

		HTREEITEM hModelItem = pTree->InsertItem(TVIF_TEXT, Model->GetName(),
			0, 0, 0, 0, 0, hParentItem, hLastItem);

		Model->SetHItem(hModelItem);
		pTree->SetItemImage(hModelItem, 11, 11);
		pTree->SetItemImage(hModItem, 6, 6);
	}

	vector<MethodOldData>::iterator itMt;
	for(itMt=Apj->Methods.begin(); itMt!=Apj->Methods.end(); ++itMt)
	{
		CMethodData NewMethod(Prj->GetHItem());

		ItRlOD itRl;
		for(itRl=itMt->Rules.begin(); itRl!=itMt->Rules.end(); ++itRl)
		{
			if(Prj->GetParam(itRl->ParamName) == NULL)
				continue;

			AnalyseRule Rule(itRl->ParamName);

			Rule.MainModel = itRl->DefModel;
			if(itRl->StdMois < 0)
			{
				Rule.Moisture = 0.;
				Rule.UseStdMoisture = false;
			}
			else
			{
				Rule.Moisture = itRl->StdMois;
				Rule.UseStdMoisture = true;
			}

			ItStr itstr;
			for(itstr=itMt->Models.begin(); itstr!=itMt->Models.end(); ++itstr)
			{
				for(itM=Apj->Models.begin(); itM!=Apj->Models.end(); ++itM)
					if(!itM->FileName.Compare(*itstr))
						break;
				if(itM == Apj->Models.end())
					continue;
				CModelData *Mod = Prj->GetModel(itM->ModelName);
				if(Mod == NULL || !Mod->IsModelParamExist(itRl->ParamName))
					continue;

				Rule.AddModels.push_back(itM->ModelName);
			}

			NewMethod.AddRule(Rule);
		}

		NewMethod.SetName(itMt->MethodName);
		if(itMt->AnType == 1)  NewMethod.SetAnalysisType(C_MTD_ANALYSIS_QNT);
		if(itMt->AnType == 2)
			continue;

		if(itMt->IndMois < 0 || itMt->IndMois >= int(NewMethod.Rules.size()))
			NewMethod.SetMoisParam(cEmpty);
		else
			NewMethod.SetMoisParam(NewMethod.Rules[itMt->IndMois].Name);

		if(GetType() == C_DEV_TYPE_40 || GetType() == C_DEV_TYPE_12M)
		{
			NewMethod.SetLowTemp(itMt->MinTemp);
			NewMethod.SetUpperTemp(itMt->MaxTemp);
			NewMethod.SetMaxDiffTemp(itMt->DiffTemp);
		}

		HTREEITEM hParentItem = Prj->GetMethodSetHItem();
		hLastItem = Prj->GetLastMethodHItem();
		CMethodData* Method = Prj->AddExistingMethod(NewMethod);
		if(Method == NULL)
			return false;

		HTREEITEM hMethodItem = pTree->InsertItem(TVIF_TEXT, Method->GetName(),
			0, 0, 0, 0, 0, hParentItem, hLastItem);

		Method->SetHItem(hMethodItem);
		pTree->SetItemImage(hMethodItem, 11, 11);
		pTree->SetItemImage(hMtdItem, 6, 6);
	}

	return true;
}

bool CDeviceData::GetDeviceData(CDeviceData& copydev, CString PrjName)
{
	bool all = (PrjName.IsEmpty());

	VLSI PSam;
	ItPrj itP;
	for(itP=Projects.begin(); itP!=Projects.end(); ++itP)
	{
		if(!all && itP->GetName().Compare(PrjName))
			continue;

		ItTrans itT;
		for(itT=itP->Transfers.begin(); itT!=itP->Transfers.end(); ++itT)
		{
			ItSam itS;
			for(itS=itT->Samples.begin(); itS!=itT->Samples.end(); ++itS)
			{
				LoadSampleInfo NewLSI;
				NewLSI.pSam = &(*itS);

				ItSpc itSp;
				for(itSp=itS->SpecData.begin(); itSp!=itS->SpecData.end(); ++itSp)
					NewLSI.Nums.push_back(itSp->Num);

				PSam.push_back(NewLSI);
			}

			CSampleData *pArt = itT->GetArtSample();
			if(pArt != NULL)
			{
				LoadSampleInfo NewLSI;
				NewLSI.pSam = pArt;

				ItSpc itSp;
				for(itSp=pArt->SpecData.begin(); itSp!=pArt->SpecData.end(); ++itSp)
					NewLSI.Nums.push_back(itSp->Num);

				PSam.push_back(NewLSI);
			}
		}
	}

	if(int(PSam.size() > 0))
	{
		CLoadSpectraPBDlg dlg(&PSam, NULL);
		if(dlg.DoModal() != IDOK)
		{
			FreeAllSpectraData();
			return false;
		}
	}

	copydev.Copy((*this));
	copydev.Projects.clear();
	for(itP=Projects.begin(); itP!=Projects.end(); ++itP)
	{
		if(!all && itP->GetName().Compare(PrjName))
			continue;

		copydev.Projects.push_back(*itP);
	}

	FreeAllSpectraData();
	return true;
}

CProjectData* CDeviceData::ImportSpectra(CString FileName, CTreeCtrl* curTree)
{
	apjdb::PRApjProject Data;

	CReadApjxWaitDlg dlg3(this, &Data, FileName);
	if(dlg3.DoModal() != IDOK)
		return NULL;

	if(Number != Data.DevNum)
	{
		CString s, s1, s2;
		s1.Format(cFmt08, Data.DevNum);
		s2 = cGetDeviceTypeName(cGetDeviceTypeByID0(Data.DevType)) + ParcelLoadString(STRID_FIELD_NUM) + s1;
		s.Format(ParcelLoadString(STRID_ERROR_IMPORT_DEVNUM), FileName, s2);
		ParcelError(s);
		return NULL;
	}

	pTree = curTree;

	CProjectData* pPrj = NULL;
	CImportSpectraDlg dlg(this, &Data, pPrj);
	if(dlg.DoModal() != IDOK)
		return NULL;

	return GetLastProject();
}

bool CDeviceData::ReadApjx(CString FileName, apjdb::PRApjProject& apjxData)
{
	CPRDBSpectra Database2;
	if(!Database2.Open(FileName))
	{
		ParcelError(ParcelLoadString(STRID_STDERR_DBNOOPEN));
		return false;
	}
	if(!Database2.CheckDatabase())
	{
		ParcelError(ParcelLoadString(STRID_STDERR_DBINCORRVERS));
		Database2.Close();
		return false;
	}

	if(!Database2.ApjProjectsRead())
	{
		ParcelError(ParcelLoadString(STRID_STDERR_DBNOREAD));
		Database2.Close();
		return false;
	}
	while(!Database2.IsEOF())
	{
		if(!Database2.ApjProjectGetWithSpecData(&apjxData))
		{
			ParcelError(ParcelLoadString(STRID_STDERR_DBNOGETDATA));
			Database2.Close();
			return false;
		}

		Database2.MoveNext();
	}
	Database2.Free();

	return true;
}

bool CDeviceData::AcceptImportSpectra(CString PrjName, apjdb::PRApjProject* Apjx, VImpSam* ISamples, bool repRef)
{
	CProjectData* pPrj = GetProject(PrjName);

	if(pPrj == NULL)
	{
		if(!GetDatabase()->BeginTrans())
			return false;

		CProjectData NewPrj(hItem, Apjx->IsSST);

		NewPrj.SetName(PrjName);
		NewPrj.SetLowLimSpecRange(Apjx->SpecRangeLow);
		NewPrj.SetUpperLimSpecRange(Apjx->SpecRangeUpper);
		NewPrj.SetSubCanalResolution(Apjx->ResSub);
		NewPrj.SetSubCanalApodization(Apjx->ApodSub);
		NewPrj.SetUseRefCanal(Apjx->UseRef);
		NewPrj.SetRefCanalResolution(Apjx->ResRef);
		NewPrj.SetRefCanalApodization(Apjx->ApodRef);
		NewPrj.SetZeroFillingInd(Apjx->ZeroFill);
		NewPrj.SetNScansSubsample(Apjx->SmplScan);
		NewPrj.SetNMeasuresSubsample(Apjx->SmplMeas);
		NewPrj.SetNScansStandart(Apjx->StdScan);
		NewPrj.SetNMeasuresStandart(Apjx->StdMeas);
		if(Type == C_DEV_TYPE_40 || Type == C_DEV_TYPE_12M)
		{
			NewPrj.SetBathLength(Apjx->BathLength);
			NewPrj.SetLowLimTemp(Apjx->TempLow);
			NewPrj.SetUpperLimTemp(Apjx->TempUpper);
			NewPrj.SetMaxDiffTemp(Apjx->TempDiff);
		}

		prdb::PRAnalisisProject Obj;
		NewPrj.GetDataForBase(Obj);
		if(!GetDatabase()->ProjectAddEx(Number, cEmpty, &Obj))
		{
			GetDatabase()->Rollback();
			return false;
		}

		GetDatabase()->CommitTrans();

		HTREEITEM hLastItem = (GetLastProject() != NULL) ? GetLastProject()->GetHItem() : 0;

		Projects.push_back(NewPrj);
		pPrj = GetLastProject();

		CString NameParam = ParcelLoadString(STRID_TREE_PARSET);
		CString NameSmpl = ParcelLoadString(STRID_TREE_SAMSET);
		CString NameMod = ParcelLoadString(STRID_TREE_MODSET);
		CString NameMtd = ParcelLoadString(STRID_TREE_MTDSET);
		CString NameSmplOwn = ParcelLoadString(STRID_TREE_SAMOWN);

		HTREEITEM hPrjItem = pTree->InsertItem(TVIF_TEXT, pPrj->GetName(), 0, 0, 0, 0, 0, hItem, hLastItem);
		HTREEITEM hParamItem = pTree->InsertItem(TVIF_TEXT, NameParam, 0, 0, 0, 0, 0, hPrjItem, NULL);
		HTREEITEM hSmplItem = pTree->InsertItem(TVIF_TEXT, NameSmpl, 0, 0, 0, 0, 0, hPrjItem, hParamItem);
		HTREEITEM hModItem = pTree->InsertItem(TVIF_TEXT, NameMod, 0, 0, 0, 0, 0, hPrjItem, hSmplItem);
		HTREEITEM hMtdItem = pTree->InsertItem(TVIF_TEXT, NameMtd, 0, 0, 0, 0, 0, hPrjItem, hModItem);
		HTREEITEM hSmplOwnItem = pTree->InsertItem(TVIF_TEXT, NameSmplOwn, 0, 0, 0, 0, 0, hSmplItem, NULL);

		pPrj->SetHItem(hPrjItem);
		pPrj->SetParentHItem(hItem);
		pPrj->SetParamSetHItem(hParamItem);
		pPrj->SetSampleSetHItem(hSmplItem);
		pPrj->SetModelSetHItem(hModItem);
		pPrj->SetMethodSetHItem(hMtdItem);

		pTree->SetItemImage(hPrjItem, 4, 4);
		pTree->SetItemImage(hParamItem, 10, 10);
		pTree->SetItemImage(hSmplItem, 6, 6);
		pTree->SetItemImage(hModItem, 10, 10);
		pTree->SetItemImage(hMtdItem, 10, 10);
		pTree->SetItemImage(hSmplOwnItem, 11, 11);

		pTree->Select(hPrjItem, TVGN_CARET);
		pTree->Expand(hItem, TVE_EXPAND);
		pTree->SetItemImage(hItem, 3, 3);

		pPrj->CalcSpecPoints();

		pPrj->AddOwnTrans();
		pPrj->GetTransOwn()->SetHItem(hSmplOwnItem);
	}

	vector<PRApjParam>::iterator itP;
	for(itP=Apjx->Params.begin(); itP!=Apjx->Params.end(); ++itP)
	{
		if(pPrj->GetParam(itP->ParamName) != NULL)
			continue;

		CParamData NewPar(pPrj->GetHItem());
		NewPar.SetName(itP->ParamName);
		NewPar.SetUnit(itP->Unit);
		NewPar.SetFormat(itP->Format);
		NewPar.SetStdMoisture(itP->StdMois);
		NewPar.SetUseStdMoisture(itP->UseStdMois);

		pPrj->AddParam(&NewPar);
	}

	ItImpSam itS;
	for(itS=ISamples->begin(); itS!=ISamples->end(); ++itS)
	{
		if(!itS->Include)
			continue;

		CString SamName = itS->SamName;

		vector<PRApjSample>::iterator itAS;
		for(itAS=Apjx->Samples.begin(); itAS!=Apjx->Samples.end(); ++itAS)
			if(!itAS->SampleName.Compare(SamName))
				break;

		if(itAS == Apjx->Samples.end())
			continue;

		CSampleData* pSam = pPrj->GetSample(SamName);
		if(pSam == NULL)
		{
			CSampleData NewSam(pPrj->GetHItem(), pPrj->GetTransOwnHItem());

			NewSam.SetName(SamName);
			NewSam.SetIden2(itAS->Iden2);
			NewSam.SetIden3(itAS->Iden3);

			vector<PRApjRefData>::iterator itR;
			for(itR=itAS->RefData.begin(); itR!=itAS->RefData.end(); ++itR)
			{
				CString ParName = itR->ParamName;
				if(pPrj->GetParam(ParName) == NULL)
					continue;

				NewSam.AddReferenceData(ParName);
				if(itR->IsDataExists)  NewSam.SetReferenceValue(ParName, itR->RefData);
				else  NewSam.SetReferenceAsNoValue(ParName);
			}

			if(!pPrj->AddSample(&NewSam))
				continue;

			pSam = pPrj->GetSample(SamName);
		}
		else
		{
			if(!GetDatabase()->BeginTrans())
				return false;

			if(pSam->GetIden2().Compare(itAS->Iden2) || pSam->GetIden3().Compare(itAS->Iden3))
			{
				pSam->SetIden2(itAS->Iden2);
				pSam->SetIden3(itAS->Iden3);

				prdb::PRSample Obj;
				pSam->GetDataForBase(Obj);
				if(!GetDatabase()->SampleAddEx(Number, PrjName, cOwn, pSam->GetName(), &Obj))
				{
					GetDatabase()->Rollback();
					return false;
				}
			}

			vector<PRApjRefData>::iterator itR;
			for(itR=itAS->RefData.begin(); itR!=itAS->RefData.end(); ++itR)
			{
				if(!itR->IsDataExists)
					continue;
				ReferenceData* pRef = pSam->GetReferenceData(itR->ParamName);
				if(pRef == NULL)
					continue;
				if(pRef->bNoValue || (repRef && fabs(pRef->Value - itR->RefData) > 1.e-9))
				{
					pRef->Value = itR->RefData;
					pRef->bNoValue = false;

					prdb::PRRefData Obj2;
					pSam->GetRefDataForBase(itR->ParamName, Obj2);
					if(!GetDatabase()->RefDataAddEx(Number, PrjName, cOwn, SamName, &Obj2))
					{
						GetDatabase()->Rollback();
						return false;
					}
				}
			}

			GetDatabase()->CommitTrans();
		}

		ItIntBool itSp;
		for(itSp=itS->Spectra.begin(); itSp!=itS->Spectra.end(); ++itSp)
		{
			if(!itSp->second)
				continue;

			vector<PRApjSpectrum>::iterator itASp;
			for(itASp=itAS->Spectra.begin(); itASp!=itAS->Spectra.end(); ++itASp)
				if(itASp->SpecNum == itSp->first)
					break;

			if(itASp == itAS->Spectra.end())
				continue;

			Spectrum NewSpec;
			NewSpec.bUse = true;
			NewSpec.DateTime = itASp->DateCreated;
			NewSpec.Num = pSam->GetSpecNumMax() + 1;
			copy(itASp->Data.begin(), itASp->Data.end(), inserter(NewSpec.Data, NewSpec.Data.begin()));

			pSam->AddSpectrum(&NewSpec);
		}

		pSam->FreeSpectraData();
	}

	return true;
}

int CDeviceData::GetNResolutions()
{
	return int(ResolList.size());
}

double CDeviceData::GetResolutionFromList(int ind)
{
	if(ind < 0 || ind >= int(ResolList.size()))
		return 0.;

	return ResolList[ind];
}

int CDeviceData::GetResolutionIndByMean(double mean)
{
	int i;
	ItDbl it;
	for(i=0, it=ResolList.begin(); it!=ResolList.end(); ++it, i++)
		if(fabs(mean - (*it)) < 1.e-6)
			return i;

	return -1;
}

bool CDeviceData::SetResolList()
{
	ResolList.clear();
	ResolList.push_back(64.);
	ResolList.push_back(32.);
	ResolList.push_back(16.);
	ResolList.push_back(8.);

	if(Type == C_DEV_TYPE_02 || Type == C_DEV_TYPE_02M || Type == C_DEV_TYPE_10 || Type == C_DEV_TYPE_20
		|| Type == C_DEV_TYPE_08)
	{
		ResolList.push_back(4.);
		ResolList.push_back(2.);
		ResolList.push_back(1.);
	}
	if(Type == C_DEV_TYPE_02 || Type == C_DEV_TYPE_02M || Type == C_DEV_TYPE_08)
	{
		ResolList.push_back(0.5);
	}

	return true;
}

bool CDeviceData::IsReadyForTrans(CProjectData *SPrj)
{
	ItPrj it;
	for(it=Projects.begin(); it!=Projects.end(); ++it)
		if(it->IsReadyForTrans(SPrj))
			return true;

	return false;
}

bool CDeviceData::IsSpectraExist()
{
	ItPrj it;
	for(it=Projects.begin(); it!=Projects.end(); ++it)
		if(it->GetNAllSpectrum() > 0)
			return true;

	return false;
}

CString CDeviceData::GetSSTName()
{
	CString s;
	s.Format(ParcelLoadString(STRID_SSTNAMES_PRJ), Number);

	return s;
}

bool CDeviceData::IsSSTExist()
{
	ItPrj itP;
	for(itP=Projects.begin(); itP!=Projects.end(); ++itP)
		if(itP->IsSST())
			return true;

	return false;
}

void CDeviceData::FreeAllSpectraData()
{
	ItPrj itP;
	for(itP=Projects.begin(); itP!=Projects.end(); ++itP)
	{
		ItTrans itT;
		for(itT=itP->Transfers.begin(); itT!=itP->Transfers.end(); ++itT)
		{
			ItSam itS;
			for(itS=itT->Samples.begin(); itS!=itT->Samples.end(); ++itS)
				itS->FreeSpectraData();

			CSampleData* pArt = itT->GetArtSample();
			if(pArt != NULL)
				pArt->FreeSpectraData();
		}
	}
}

void CDeviceData::GetDataForBase(prdb::PRAppliance& Obj)
{
	Obj.ApplianceType = Type;
	Obj.ApplianceSN = Number;
}

void CDeviceData::SetDataFromBase(prdb::PRAppliance& Obj)
{
	SetType(Obj.ApplianceType);
	SetNumber(Obj.ApplianceSN);
}

bool CDeviceData::LoadDatabase(CTreeCtrl* pTree)
{
	ItPrj it;
	LPrj NewProjects;

	if(!GetDatabase()->ProjectRead(Number))
		return false;

	HTREEITEM hLastItem = NULL;
	while(!GetDatabase()->IsEOF())
	{
		prdb::PRAnalisisProject Obj;

		if(!GetDatabase()->ProjectGet(&Obj))
			break;

		CProjectData Prj(hItem);

		Prj.SetDataFromBase(Obj);

		CString NameParam = ParcelLoadString(STRID_TREE_PARSET);
		CString NameSmpl = ParcelLoadString(STRID_TREE_SAMSET);
		CString NameMod = ParcelLoadString(STRID_TREE_MODSET);
		CString NameMtd = ParcelLoadString(STRID_TREE_MTDSET);

		HTREEITEM hNewItem = pTree->InsertItem(TVIF_TEXT, Prj.GetName(), 0, 0, 0, 0, 0,
											   hItem, hLastItem);
		HTREEITEM hParamItem = pTree->InsertItem(TVIF_TEXT, NameParam, 0, 0, 0, 0, 0, hNewItem, NULL);
		HTREEITEM hSmplItem = pTree->InsertItem(TVIF_TEXT, NameSmpl, 0, 0, 0, 0, 0, hNewItem, hParamItem);
		HTREEITEM hModItem = pTree->InsertItem(TVIF_TEXT, NameMod, 0, 0, 0, 0, 0, hNewItem, hSmplItem);
		HTREEITEM hMtdItem = pTree->InsertItem(TVIF_TEXT, NameMtd, 0, 0, 0, 0, 0, hNewItem, hModItem);

		Prj.SetHItem(hNewItem);
		Prj.SetParamSetHItem(hParamItem);
		Prj.SetSampleSetHItem(hSmplItem);
		Prj.SetModelSetHItem(hModItem);
		Prj.SetMethodSetHItem(hMtdItem);

		pTree->SetItemImage(hNewItem, 4, 4);
		pTree->SetItemImage(hParamItem, 10, 10);
		pTree->SetItemImage(hSmplItem, 6, 6);
		pTree->SetItemImage(hModItem, 10, 10);
		pTree->SetItemImage(hMtdItem, 10, 10);

		hLastItem = hNewItem;

		NewProjects.push_back(Prj);

		GetDatabase()->MoveNext();
	}
	GetDatabase()->Free();

	Projects.clear();
	copy(NewProjects.begin(), NewProjects.end(), inserter(Projects, Projects.begin()));

	for(it=Projects.begin(); it!=Projects.end(); ++it)
		it->LoadDatabase(pTree);

	pTree->Expand(hItem, TVE_COLLAPSE);
	if(Projects.size() > 0)
		pTree->SetItemImage(hItem, 2, 2);
	else
		pTree->SetItemImage(hItem, 9, 9);

	return true;
}
