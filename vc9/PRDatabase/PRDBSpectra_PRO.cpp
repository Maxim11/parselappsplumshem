#include "stdafx.h"

#include "PRDBSpectra_PRO.h"
#include "..\file4spa.h"
#include "..\FileSystem.h"

using namespace filesystem;
using namespace file4spa;

#define VerifySuccess if (res != LumexDB::enSuccess) { assert(false); return false; }
#define VerifySuccessAndNoData if (res != LumexDB::enSuccess && res != LumexDB::enNoData) { assert(false); return false; }
#define VerifyDataPointer if (pData == NULL) {	assert(false); return false; }
#define StringLen (int)(pData->m_Size / sizeof(TCHAR) + 1)
#define BufferLen (pData->m_Size + sizeof(TCHAR))

CPRDBSpectra_PRO::CPRDBSpectra_PRO() : CLDatabase()
{
}

CPRDBSpectra_PRO::~CPRDBSpectra_PRO()
{
}

bool CPRDBSpectra_PRO::IsApjSampleExists(CString SampleName, long lApj)
{
	m_sSQL.Format(_T(APJDB_SQL_ISEXISTS_APJ_SAMPLE), replaceChar(SampleName), lApj);
	return isDataExists() > 0;
}

bool CPRDBSpectra_PRO::IsApjSpectrumExists(int SpecNum, long lSam)
{
	m_sSQL.Format(_T(APJDB_SQL_ISEXISTS_APJ_SPECTRA), SpecNum, lSam);
	return isDataExists() > 0;
}

bool CPRDBSpectra_PRO::ApjSampleAddEx(CString ApjProjectName, CString SampleName_OLD, const PRApjSample *const pSam)
{
	m_sSQL.Format(_T(APJDB_SQL_GET_ID_APJ), replaceChar(ApjProjectName));
	long lApj = isDataExists();
	if(lApj <= 0)
		return false;

	if(IsApjSampleExists(SampleName_OLD, lApj))
		return sampleUpdate(SampleName_OLD, lApj, pSam);
	else
		return sampleAdd(lApj, pSam);
}

bool CPRDBSpectra_PRO::ApjSampleDelete(CString ApjProjectName, CString SampleName)
{
	vector<int> SpecIDs;
	if(!GetSampleSpectraID(ApjProjectName, SampleName, SpecIDs))
	{
		assert(false);
		return false;
	}
	if(!DeleteSpectraFiles(&SpecIDs))
	{
		assert(false);
		return false;
	}

	m_sSQL.Format(_T(APJDB_SQL_DELETE_APJ_SAMPLE), SampleName, ApjProjectName);
	return executeSQL();
}

bool CPRDBSpectra_PRO::SetRefData(CString ApjProjectName, CString SampleName, CString ParamName, double Value, bool IsValue)
{
	m_sSQL.Format(_T(APJDB_SQL_GET_ID_APJ), replaceChar(ApjProjectName));
	long lApj = isDataExists();
	if(lApj <= 0)
		return false;

	m_sSQL.Format(_T(APJDB_SQL_GET_ID_APJ_SAMPLE), replaceChar(SampleName), lApj);
	long lSam = isDataExists();
	if(lSam <= 0)
		return false;

	m_sSQL.Format(_T(APJDB_SQL_GET_ID_APJ_PARAM), replaceChar(ParamName), lApj);
	long lPar = isDataExists();
	if(lPar <= 0)
		return false;


	m_sSQL.Format(_T(APJDB_SQL_SET_DATA_APJ_REFDATA), floatWithPoint(Value), IsValue, lPar, lSam);
	if(!executeSQL())
		return false;

	return true;
}

bool CPRDBSpectra_PRO::ApjSpectrumAdd(CString ApjProjectName, CString SampleName, const PRApjSpectrum *const pSpec)
{
	m_sSQL.Format(_T(APJDB_SQL_GET_ID_APJ), replaceChar(ApjProjectName));
	long lApj = isDataExists();
	if(lApj <= 0)
		return false;

	m_sSQL.Format(_T(APJDB_SQL_GET_ID_APJ_SAMPLE), replaceChar(SampleName), lApj);
	long lSam = isDataExists();
	if(lSam <= 0)
		return false;

	if(IsApjSpectrumExists(pSpec->SpecNum, lSam))
		return false;
	else
		return spectrumAdd(lSam, pSpec);
}

bool CPRDBSpectra_PRO::ApjSpectrumDelete(CString ApjProjectName, CString SampleName, int SpecNum)
{
	vector<int> SpecIDs;
	m_sSQL.Format(_T(APJDB_SQL_GET_ID_APJ_SPECTRA), SpecNum, replaceChar(SampleName), replaceChar(ApjProjectName));
	long lSpecID = isDataExists();
	if(lSpecID <= 0)
		return false;

	SpecIDs.push_back(lSpecID);
	if(!DeleteSpectraFiles(&SpecIDs))
		return false;

	m_sSQL.Format(_T(APJDB_SQL_DELETE_APJ_SPECTRA), SpecNum, replaceChar(SampleName), replaceChar(ApjProjectName));
	return executeSQL();
}

int CPRDBSpectra_PRO::GetSpectrumNumMax(CString ApjProjectName, CString SampleName)
{
	m_sSQL.Format(_T(APJDB_SQL_GET_NUM_MAX_APJ_SPECTRA), replaceChar(SampleName), replaceChar(ApjProjectName));
	long res = isDataExists();

	return (int(res));
}

bool CPRDBSpectra_PRO::ApjProjectsRead()
{
	m_sSQL.Format(_T(APJDB_SQL_READ_APJ));
	return selectSQL();
}

bool CPRDBSpectra_PRO::ApjProjectGet(PRApjProject *const pApj)
{
	if(m_pDataSet == NULL)
	{
		assert(false);
		return false;
	}

	LumexDB::CDataPtr<ILumexDBDataSet::TColumnData> pData = m_pDataSet->GetColumn(0);
	VerifyDataPointer;	
	long lApj;
	LumexDB::EnOperationResult res = pData->GetData(&lApj, sizeof(lApj));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(1);
	VerifyDataPointer;	
	LPTSTR lpBuf = pApj->ProjectName.GetBuffer(StringLen);	
	res = pData->GetData(lpBuf, BufferLen, false);
	pApj->ProjectName.ReleaseBuffer();
	VerifySuccess;
	returnChar(pApj->ProjectName);

	pData = m_pDataSet->GetColumn(2);
	VerifyDataPointer;
	TIMESTAMP_STRUCT date;
	res = pData->GetData(&date, sizeof(date));
	VerifySuccess;		
	pApj->DateCreated = CTime(date.year, date.month, date.day, date.hour, date.minute, date.second);

	pData = m_pDataSet->GetColumn(3);
	VerifyDataPointer;		
	res = pData->GetData(&pApj->DevNum, sizeof(pApj->DevNum));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(4);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->DevType, sizeof(pApj->DevType));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(5);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->SpecRangeLow, sizeof(pApj->SpecRangeLow));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(6);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->SpecRangeUpper, sizeof(pApj->SpecRangeUpper));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(7);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->ResSub, sizeof(pApj->ResSub));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(8);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->ApodSub, sizeof(pApj->ApodSub));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(9);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->ZeroFill, sizeof(pApj->ZeroFill));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(10);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->UseRef, sizeof(pApj->UseRef));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(11);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->ResRef, sizeof(pApj->ResRef));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(12);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->ApodRef, sizeof(pApj->ApodRef));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(13);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->SmplScan, sizeof(pApj->SmplScan));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(14);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->SmplMeas, sizeof(pApj->SmplMeas));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(15);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->StdScan, sizeof(pApj->StdScan));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(16);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->StdMeas, sizeof(pApj->StdMeas));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(17);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->Xtransform, sizeof(pApj->Xtransform));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(18);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->IsSST, sizeof(pApj->IsSST));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(19);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->UseBath, sizeof(pApj->UseBath));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(20);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->BathLength, sizeof(pApj->BathLength));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(21);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->TempLow, sizeof(pApj->TempLow));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(22);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->TempUpper, sizeof(pApj->TempUpper));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(23);
	VerifyDataPointer;		
	res = pData->GetData(&pApj->TempDiff, sizeof(pApj->TempDiff));
	VerifySuccess;


	CString sql;
	ILumexDBDataSet *pDataSet;

	// ��������� �����������
	{
		sql.Format(_T(APJDB_SQL_READ_APJ_PARAM), lApj);
		
		res = m_pDB->ExecuteSQL(sql, &pDataSet);
		if (res != LumexDB::enSuccess || pDataSet == NULL)
		{
			assert(false);
			return false;
		}

		pApj->Params.clear();

		while(!pDataSet->IsEOF())
		{
			PRApjParam newParam;

			pData = pDataSet->GetColumn(0);
			VerifyDataPointer;	
			lpBuf = newParam.ParamName.GetBuffer(StringLen);	
			res = pData->GetData(lpBuf, BufferLen, false);
			newParam.ParamName.ReleaseBuffer();
			VerifySuccess;
			returnChar(newParam.ParamName);

			pData = pDataSet->GetColumn(1);
			VerifyDataPointer;	
			lpBuf = newParam.Unit.GetBuffer(StringLen);	
			res = pData->GetData(lpBuf, BufferLen, false);
			newParam.Unit.ReleaseBuffer();
			VerifySuccess;
			returnChar(newParam.Unit);

			pData = pDataSet->GetColumn(2);
			VerifyDataPointer;	
			lpBuf = newParam.Format.GetBuffer(StringLen);	
			res = pData->GetData(lpBuf, BufferLen, false);
			newParam.Format.ReleaseBuffer();
			VerifySuccess;
			returnChar(newParam.Format);

			pData = pDataSet->GetColumn(3);
			VerifyDataPointer;		
			res = pData->GetData(&newParam.StdMois, sizeof(newParam.StdMois));
			VerifySuccess;

			pData = pDataSet->GetColumn(4);
			VerifyDataPointer;		
			res = pData->GetData(&newParam.UseStdMois, sizeof(newParam.UseStdMois));
			VerifySuccess;

			pApj->Params.push_back(newParam);

			res = pDataSet->NextRow();
			VerifySuccessAndNoData;
		}
		pDataSet->Release();
	}

	// ��������� ��������
	{
		sql.Format(_T(APJDB_SQL_READ_APJ_SAMPLE), lApj);
		
		res = m_pDB->ExecuteSQL(sql, &pDataSet);
		if (res != LumexDB::enSuccess || pDataSet == NULL)
		{
			assert(false);
			return false;
		}

		pApj->Samples.clear();

		while(!pDataSet->IsEOF())
		{
			PRApjSample newSample;

			int lSam;
			pData = pDataSet->GetColumn(0);
			VerifyDataPointer;		
			res = pData->GetData(&lSam, sizeof(lSam));
			VerifySuccess;

			pData = pDataSet->GetColumn(1);
			VerifyDataPointer;	
			lpBuf = newSample.SampleName.GetBuffer(StringLen);	
			res = pData->GetData(lpBuf, BufferLen, false);
			newSample.SampleName.ReleaseBuffer();
			VerifySuccess;
			returnChar(newSample.SampleName);

			pData = pDataSet->GetColumn(2);
			VerifyDataPointer;	
			lpBuf = newSample.Iden2.GetBuffer(StringLen);	
			res = pData->GetData(lpBuf, BufferLen, false);
			newSample.Iden2.ReleaseBuffer();
			VerifySuccess;
			returnChar(newSample.Iden2);

			pData = pDataSet->GetColumn(3);
			VerifyDataPointer;	
			lpBuf = newSample.Iden3.GetBuffer(StringLen);	
			res = pData->GetData(lpBuf, BufferLen, false);
			newSample.Iden3.ReleaseBuffer();
			VerifySuccess;
			returnChar(newSample.Iden3);


			CString sql2;
			ILumexDBDataSet *pDataSet2;

			// ��������� ����������� ������
			{
				sql2.Format(_T(APJDB_SQL_READ_APJ_REFDATA), lSam);
				
				res = m_pDB->ExecuteSQL(sql2, &pDataSet2);
				if(res != LumexDB::enSuccess || pDataSet2 == NULL)
				{
					assert(false);
					return false;
				}

				while(!pDataSet2->IsEOF())
				{
					PRApjRefData newRef;

					pData = pDataSet2->GetColumn(0);
					VerifyDataPointer;	
					lpBuf = newRef.ParamName.GetBuffer(StringLen);	
					res = pData->GetData(lpBuf, BufferLen, false);
					newRef.ParamName.ReleaseBuffer();
					VerifySuccess;
					returnChar(newRef.ParamName);

					pData = pDataSet2->GetColumn(1);
					VerifyDataPointer;		
					res = pData->GetData(&newRef.RefData, sizeof(newRef.RefData));
					VerifySuccess;

					pData = pDataSet2->GetColumn(2);
					VerifyDataPointer;		
					res = pData->GetData(&newRef.IsDataExists, sizeof(newRef.IsDataExists));
					VerifySuccess;

					newSample.RefData.push_back(newRef);

					res = pDataSet2->NextRow();
					VerifySuccessAndNoData;
				}
				pDataSet2->Release();
			}

			// ��������� ��������
			{
				sql2.Format(_T(APJDB_SQL_READ_APJ_SPECTRA), lSam);
				
				res = m_pDB->ExecuteSQL(sql2, &pDataSet2);
				if(res != LumexDB::enSuccess || pDataSet2 == NULL)
				{
					assert(false);
					return false;
				}

				while(!pDataSet2->IsEOF())
				{
					PRApjSpectrum newSpec;

					pData = pDataSet2->GetColumn(0);
					VerifyDataPointer;		
					res = pData->GetData(&newSpec.SpecNum, sizeof(newSpec.SpecNum));
					VerifySuccess;

					pData = pDataSet2->GetColumn(1);
					VerifyDataPointer;
					TIMESTAMP_STRUCT date;
					res = pData->GetData(&date, sizeof(date));
					VerifySuccess;		
					newSpec.DateCreated = CTime(date.year, date.month, date.day, date.hour, date.minute, date.second);

					newSample.Spectra.push_back(newSpec);

					res = pDataSet2->NextRow();
					VerifySuccessAndNoData;
				}
				pDataSet2->Release();
			}

			pApj->Samples.push_back(newSample);

			res = pDataSet->NextRow();
			VerifySuccessAndNoData;
		}
		pDataSet->Release();
	}

	return true;
}

bool CPRDBSpectra_PRO::ApjSpectrumDataGet(CString ApjProjectName, CString SampleName, PRApjSpectrum *const pSpec)
{
	m_sSQL.Format(_T(APJDB_SQL_GET_ID_APJ_SPECTRA), pSpec->SpecNum, replaceChar(SampleName), replaceChar(ApjProjectName));
	long lSpec = isDataExists();
	if(lSpec <= 0)
		return false;

	CString Path = GetPathToSpec(lSpec);
	if(!IsExistFile(Path))
	{
		assert(false);
		return false;
	}

	pSpec->Data.clear();
	bool res = ReadSpaDataFromFile(Path, pSpec->Data);
	if(!res)
	{
		assert(false);
		return false;
	}

	return true;
}

bool CPRDBSpectra_PRO::sampleAdd(long lApj, const PRApjSample *const pSam)
{
	m_sSQL = _T(APJDB_SQL_GET_ID_MAX_APJ_SAMPLE);
	long lSam = 1 + isDataExists();
	if(lSam <= 0)
		return false;

	m_sSQL.Format(_T(APJDB_SQL_INSERT_APJ_SAMPLE), lSam, lApj, replaceChar(pSam->SampleName),
		replaceChar(pSam->Iden2), replaceChar(pSam->Iden3));
	if(!executeSQL())
		return false;

	return sampleAddVect(lApj, lSam, pSam);
}

bool CPRDBSpectra_PRO::sampleUpdate(CString SampleName_OLD, long lApj, const PRApjSample *const pSam)
{
	m_sSQL.Format(_T(APJDB_SQL_GET_ID_APJ_SAMPLE), replaceChar(SampleName_OLD), lApj);
	long lSam = isDataExists();
	if(lSam <= 0)
		return false;

	m_sSQL.Format(_T(APJDB_SQL_UPDATE_APJ_SAMPLE), replaceChar(pSam->SampleName), replaceChar(pSam->Iden2),
		replaceChar(pSam->Iden3), lSam, lApj);
	if(!executeSQL())
		return false;

	m_sSQL.Format(_T(APJDB_SQL_DELETE_ALL_APJ_REFDATA), lSam);
	if(!executeSQL())
		return false;

	m_sSQL.Format(_T(APJDB_SQL_DELETE_ALL_APJ_SPECTRA), lSam);
	if(!executeSQL())
		return false;

	return sampleAddVect(lApj, lSam, pSam);
}

bool CPRDBSpectra_PRO::sampleAddVect(long lApj, long lSam, const PRApjSample *const pSam)
{
	for(std::vector<PRApjRefData>::const_iterator itR=pSam->RefData.begin(); itR!=pSam->RefData.end(); ++itR) 
	{
		m_sSQL = _T(APJDB_SQL_GET_ID_MAX_APJ_REFDATA);
		long lRef = 1 + isDataExists();
		if(lRef <= 0)
			return false;

		m_sSQL.Format(_T(APJDB_SQL_GET_ID_APJ_PARAM), replaceChar(itR->ParamName), lApj);
		long lPar = isDataExists();
		if(lPar <= 0)
			return false;

		m_sSQL.Format(_T(APJDB_SQL_INSERT_APJ_REFDATA), lRef, lPar, lSam, floatWithPoint(itR->RefData),
			itR->IsDataExists);
		if(!executeSQL())
			return false;
	}

	return true;
}

bool CPRDBSpectra_PRO::spectrumAdd(long lSam, const PRApjSpectrum *const pSpec)
{
	m_sSQL = _T(APJDB_SQL_GET_ID_MAX_APJ_SPECTRA);
	long lSpec = 1 + isDataExists();
	if(lSpec <= 0)
		return false;

	m_sSQL.Format(_T(APJDB_SQL_INSERT_APJ_SPECTRA), lSpec, lSam, pSpec->SpecNum,
		pSpec->DateCreated.Format(m_pszFormatCTime));
	if(!executeSQL())
		return false;

	bool res = CheckDirectory();
	if(res)
	{
		CString Path = GetPathToSpec(lSpec);
		if(IsExistFile(Path))
			DeleteFileByPath(Path);

		res = SaveSpaDataAtFile(Path, pSpec->Data);
	}

	return res;
}

bool CPRDBSpectra_PRO::GetSampleSpectraID(CString ApjProjectName, CString SampleName, std::vector<int>& SpecIDs)
{
	CString sql;
	ILumexDBDataSet *pDataSet;

	sql.Format(_T(APJDB_SQL_GET_SAMSPECTRA_ID_APJ_SPECTRA), replaceChar(SampleName), replaceChar(ApjProjectName));
	LumexDB::EnOperationResult res = m_pDB->ExecuteSQL(sql, &pDataSet);
	if (res != LumexDB::enSuccess || pDataSet == NULL)
	{
		assert(false);
		return false;
	}

	while(!pDataSet->IsEOF())
	{
		LumexDB::CDataPtr<ILumexDBDataSet::TColumnData> pData = pDataSet->GetColumn(0);
		VerifyDataPointer;
		int lSpec;
		LumexDB::EnOperationResult res = pData->GetData(&lSpec, sizeof(lSpec));
		VerifySuccess;

		SpecIDs.push_back(lSpec);

		res = pDataSet->NextRow();
		VerifySuccessAndNoData;
	}
	pDataSet->Release();

	return true;
}

bool CPRDBSpectra_PRO::DeleteSpectraFiles(const std::vector<int> *const SpecIDs)
{
	for(std::vector<int>::const_iterator it=SpecIDs->begin(); it!=SpecIDs->end(); ++it)
	{
		CString Path = GetPathToSpec(*it);
		if(IsExistFile(Path))
			if(!DeleteFileByPath(Path))
				return false;
	}

	return true;
}

bool CPRDBSpectra_PRO::CheckDirectory()
{
	CString Path = ExtractDirPath(m_sDB);
	CString Base = ExtractFileNameWithoutExt(m_sDB);
	CString Dir = _T("APJX_") + Base;
	CString Full = MakePath(Path, Dir);

	if(IsExistDirectory(Full))
		return true;

	return EnsureDirectory(Full);
}

CString CPRDBSpectra_PRO::GetPathToSpec(int iSp)
{
	CString Path = ExtractDirPath(m_sDB);
	CString Base = ExtractFileNameWithoutExt(m_sDB);
	CString Dir = _T("APJX_") + Base;
	CString Name;
	Name.Format(_T("%1d.dat"), iSp);
	CString Full = MakePath(Path, Dir, Name);

	return Full;
}

long CPRDBSpectra_PRO::getCurrentVersion()
{
	return APJDB_CURRENT_VERSION;
}

CString CPRDBSpectra_PRO::getCurrentDBName()
{
	return _T("APJPROJECT_DATABASE_LUMEX_NIOKR");
}
