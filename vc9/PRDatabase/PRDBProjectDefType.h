#ifndef _PARCEL_PRDB_PROJECT_DEFTYPE_H_
#define _PARCEL_PRDB_PROJECT_DEFTYPE_H_

#pragma once

#include <vector>

namespace prdbTypeCount
{
	const int PRDBSYSRulesDes		= 255;
	const int PRDBSYSUserNIC		= 20;
	const int PRDBSYSUserFN			= 25;
	const int PRDBSYSUserLN			= 25;
	const int PRDBSYSUserMN			= 25;

	const int PRDBPDTAPTitle		= 50;
	const int PRDBPDTIIdxName		= 50;
	const int PRDBPDTIIdxUnits		= 20;
	const int PRDBPDTITransName		= 50;
	const int PRDBPDTPreproc		= 7;
	const int PRDBPDTSSample1		= 40;
	const int PRDBPDTSSample2		= 40;
	const int PRDBPDTSSample3		= 255;
	const int PRDBPDTSTransName		= 50;
	const int PRDBPDTMModelName		= 50;
	const int PRDBPDTMMethodName	= 50;
}

namespace prdbHistoryType
{
	const int PRDB_HISTORY_TYPE_UNTYPED				= 0;
	const int PRDB_HISTORY_TYPE_OPEN				= 1;
	const int PRDB_HISTORY_TYPE_CLOSE				= 1 << 2;
	const int PRDB_HISTORY_TYPE_CREATE				= 1 << 3;
	const int PRDB_HISTORY_TYPE_INSERT				= 1 << 4;
	const int PRDB_HISTORY_TYPE_UPDATE				= 1 << 5;
	const int PRDB_HISTORY_TYPE_READ				= 1 << 6;
	const int PRDB_HISTORY_TYPE_DELETE				= 1 << 7;
}

namespace prdbHistoryActivity
{
	const int PRDB_HISTORY_ACTIVITY_NONE			= 0;
	const int PRDB_HISTORY_ACTIVITY_ONE_PER_OPEN	= 1;
	const int PRDB_HISTORY_ACTIVITY_FULL			= 2;
}

namespace prdbHistoryTarget
{
	const int PRDB_HISTORY_TRG_UNTYPED				= 0;
	const int PRDB_HISTORY_TRG_APPLIANCES			= 1;
	const int PRDB_HISTORY_TRG_APROJECTS			= 1 << 1;
	const int PRDB_HISTORY_TRG_INDICES				= 1 << 2;
	const int PRDB_HISTORY_TRG_SAMPLES				= 1 << 3;
	const int PRDB_HISTORY_TRG_REFDATAS				= 1 << 4;
	const int PRDB_HISTORY_TRG_SPECTRUMS			= 1 << 5;
	const int PRDB_HISTORY_TRG_MODELS				= 1 << 6;
	const int PRDB_HISTORY_TRG_METHODS				= 1 << 7;
	const int PRDB_HISTORY_TRG_TRANS				= 1 << 8;
}

namespace prdb
{
	const long PRDB_CURRENT_VERSION				= 153;
	const long PRDB_STORE_CURRENT_VERSION		= 102;

	const int PRDB_MTD_QLTFORM_MEAN					= 0;
	const int PRDB_MTD_QLTFORM_AND					= 1;
	const int PRDB_MTD_QLTFORM_OR					= 2;
	const int PRDB_MTD_QLTFORM_OPEN					= 3;
	const int PRDB_MTD_QLTFORM_CLOSE				= 4;

	struct PRRoles
	{
		int			Type;						// ����
		CString		Description;				// �������� (255 ��������)
	};

	struct PRUser
	{
		CString		UserNIC;					// nickname (20 ��������)
		CString		FirstName;					// �� ������������ ���� (25 ��������)
		CString		LastName;					// �� ������������ ���� (25 ��������)
		CString		MiddleName;					// �� ������������ ���� (25 ��������)

		PRRoles Role;							// ����
	};

	struct PRHistory
	{
		PRUser		User;						// ������������
		CTime		HistoryDate;				// ���� �������
		int			EventType;					// ��� ������� (��. prdbHistoryType)
		int			HistoryTarget;				// ������ (��. prdbHistoryTarget)
	};

	struct PRAppliance
	{
		int			ApplianceType;				// ��� �������
		int			ApplianceSN;				// ����� �������
	};

	struct PRAnalisisProject
	{
		CString		APName;						// ��� ������� �������
		bool		SST;						// ������� ������������ �������
		int			SpRangeDown;				// ������ ������� ������������� ���������
		int			SpRangeUp;					// ������� ������� ������������� ���������
		int			SampleScanCount;			// ����� ������ ����������
		int			SampleMeasCount;			// ����� ��������� ����������
		int			StandardScanCount;			// ����� ������ �������
		int			StandardMeasCount;			// ����� ��������� �������
		double		CuvetteLength;				// ����� ������
		int			SubChannelPermit;			// ���������� ����������� ������
		int			SubChannelApodization;		// ���������� ����������� ������
		int			ZeroFilling;				// ���������� ������
		bool		UseRefChannel;				// ������������� �������� ������
		int			RefChannelPermit;			// ���������� �������� ������
		int			RefChannelApodization;		// ���������� �������� ������
		int			TempRangeDown;				// ������ ������ ����������� �������
		int			TempRangeUp;				// ������� ������ ����������� �������
		int			TempDiff;					// ���������� ������� ����������� ������� �� ����������� ���������� �����
		CString		SamSortName;				// ���-������� ��� ���������� ��������
		bool		SamSortDir;					// ����������� ���������� ��������
	};

	struct PRIndex
	{
		CString		IdxName;					// ��� ����������
		CString		IdxUnits;					// ������� ���������
		int			IdxFormat;					// ������ ������
		double		StHumidity;					// ����������� ���������
		bool		IsUseStHumidity;			// ������� ������������� ����������� ���������
	};

	struct PRPlate
	{
		int PlateNum;							// ����� ��������
		CString SamName;						// ��� �������-��������
		double R;								// ����������� ��������� ��������
		double T0;								// ������� ��������
		double T1;								// 1-� ���. ������� ��������
		double T2;								// 2-� ���. ������� ��������
		double T3;								// 3-� ���. ������� ��������
		double T4;								// 4-� ���. ������� ��������
	};

	struct PRTrans
	{
		CString TransName;						// ��� ������
		int Type;								// ��� ������
		int MasterDevType;						// ��� ������-�������
		int MasterDevNumber;						// ����� ������-�������
		CTime DateCreated;						// ���� ��������
		int LowLim;								// ������ ������� ������������� ���������
		int UpperLim;							// ������� ������� ������������� ���������
		CString Preproc;						// �������������

		CString FrameName;						// ��� �������-������
		vector<PRPlate> Plates;					// ��������� ������� ��� SST
	};

	struct PRSample
	{
		CString		Sample1;					// ������������� 1
		CString		Sample2;					// ������������� 2
		CString		Sample3;					// ������������� 3
	};

	struct PRRefData
	{
		CString		IdxName;					// ��� ����������
		bool		IsDataExists;				// ��� ������?
		double		RefData;					// �������� ����������
	};

	struct PRSpectrumLite
	{
		int			SpecNum;					// ����� ������� � �������
		CTime		DateCreated;				// ���� / ����� ����������� �������
		bool		IsUse;						// ������ ���������?
	};

	struct PRSpectrum
	{
		int			SpecNum;					// ����� ������� � �������
		CTime		DateCreated;				// ���� / ����� ����������� �������
		bool		IsUse;						// ������ ���������?
		vector<double> Vc;						// ������������ ������ 
	};

	struct PRModel
	{		
		CString		ModelName;					// ��� ������
		CString		TransName;					// ��� ������ ������������ ��������
		CTime		DateCreated;				// ����� �������� ������
		int			AnalisisType;				// ��� �������
		int			ModelType;					// ��� ������
		int			NumComponentQNT;			// ����� ������� ��������� ��� ��������������� �������
		int			NumComponentQLT;			// ����� ������� ��������� ��� ������������� �������
		double		HypercubeVolume;			// ������ ���������
		int			HarmonyCount;				// ����� ��������
		double		Nonlinearity;				// ����������� ������������
		double		PredH;						// ���������� ���������� ������������
		double		PredCKO;					// ���������� CKO
		bool		UseSECV;					// ������������ �� SECV
		int			SpectrumType;				// ��� �������
		int			SpRangeRealDown;			// ������ ������� ������������� ��������� ��� �����������
		int			SpRangeRealUp;				// ������� ������� ������������� ��������� ��� �����������
		CString		Preproc;					// �������������

		struct PRMDParam
		{
			CString	IndexName;					// ������ ����������� ��� �����������
			double	PredCKOIX;					// ����. ��� ��� ���. �������
			double	CalibDown;					// ������ �������� ��������� ����������� ��� ������� ����������
			double	CalibUp;					// ������� �������� ��������� ����������� ��� ������� ����������
			double  CorrB;						// ��������� ����������� b ��� ������� ����������
			double  CorrA;						// ��������� ����������� a ��� ������� ����������
		};

		struct PRMDSpectrum
		{
			int SpNum;
			CString SampleName;
			CString TransName;
			bool IsCalib;
			bool IsValid;
		};

		vector<PRModel::PRMDParam> Indcies;			// ���. � �����������
		vector<int>	 SpDataExclude;					// ������ ����������� ������������ �����
		vector<PRModel::PRMDSpectrum> Spectra;		// ������ ��������
	};

	struct PRMethod
	{
		CString		MethodName;					// ��� ������
		CTime		DateCreated;				// ����� �������� ������
		int			AnalisisType;				// ��� �������
		CString		IndexAsHum;					// ���������� ��� ���������
		int			TempRangeRealDown;			// ������ ������ ����������� �������
		int			TempRangeRealUp;			// ������� ������ ����������� �������
		int			TempDiff;					// ���������� ������� ����������� ������� �� ����������� ���������� �����

		struct PRMTParam
		{
			CString IndexName;					// ��� ����������
			CString MainModel;					// ������ �� ���������			
			double Moisture;					// ����������� ���������
			bool UseStdMoisture;				// ����������� ���������
			vector<CString> AddModels;			// ������ �������������� �������
		};

		struct PRMTElement
		{
			int Type;
			int ModelInd;
		};

		enum PRMethodError { PRME_NONE = 0, PRME_UPDATE_EXPORTED, PRME_SMTH_ELSE };

		vector<PRMethod::PRMTParam> Index;

		map<int, CString> QltModels;			// ������ ������� QLT
		vector<PRMTElement> Formula;

	};

}

#endif