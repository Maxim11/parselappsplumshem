#ifndef _PRDB_METHOD_PRO_H_
#define _PRDB_METHOD_PRO_H_

#pragma once

#include "LDatabase/LDatabase.h"
#include "PRDBMethodSQL_Pro.h"
#include "PRDBMethodDefType.h"

using namespace mtddb;

// �����, �������������� ������ � ����� ������� ��������������� ������� �� PRO
class CPRDBMethodQntPRO : public CLDatabase
{
public:

	CPRDBMethodQntPRO(void);
	virtual ~CPRDBMethodQntPRO( void );

	// ������ ��������� ������
	bool MethodsRead();
	bool MethodGet(PRMethodQnt *const);

	// ��������� ����� �������� ������������� ���������
	bool SetMtdModelCorr(CString MethodName, CString ParamName, CString ModelName, double CorrB, double CorrA);

protected:

	// �������� ������ ���� ������, � PRO �� ������������
	virtual void createTables(std::vector<CString>*)
	{};
	// �������� ������ ���� ������
	virtual long getCurrentVersion();
	// �������� ����� ���� ������
	virtual CString getCurrentDBName();	

};

// �����, �������������� ������ � ����� ������� ������������� ������� �� PRO
class CPRDBMethodQltPRO : public CLDatabase
{
public:

	CPRDBMethodQltPRO(void);
	virtual ~CPRDBMethodQltPRO( void );

	// ������ ��������� ������
	bool MethodsRead();
	bool MethodGet(PRMethodQlt *const);

protected:

	// �������� ������ ���� ������, � PRO �� ������������
	virtual void createTables(std::vector<CString>*)
	{};
	// �������� ������ ���� ������
	virtual long getCurrentVersion();
	// �������� ����� ���� ������
	virtual CString getCurrentDBName();	

};

// ������ ������������� ������� ������
/*-----

	CPRDBMethodQntPRO Database2;
	if(!Database2.Open(fname))
	{
		ParcelWait(false);
		ParcelError(_T("���� �� ���������..."));
		return false;
	}
	if(!Database2.CheckDatabase())
	{
		ParcelWait(false);
		ParcelError(_T("������ ���� �� �������..."));
		Database2.Close();
		return false;
	}

	if(!Database2.MethodsRead())
	{
		ParcelError(_T("���� �� ��������..."));
		Database2.Close();
		return false;
	}

	mtddb::PRMethodQnt Obj2;
	while(!Database2.IsEOF())
	{
		if(!Database2.MethodGet(&Obj2))
		{
			ParcelError(_T("����� �� ��� �������..."));
			break;
		}

		Database2.MoveNext();
	}

	if(!Database2.SetMtdModelCorr(_T("��� ������"), _T("��� ����������"), _T("��� ������"), 1., 1.))
	{
		ParcelError(_T("������������ �� ����������..."));
	}

	Database2.Free();
	Database2.Close();

----*/

#endif _PRDB_METHOD_PRO_H_
