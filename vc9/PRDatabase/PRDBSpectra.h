#ifndef _PARCEL_PRDB_SPECTRA_H_
#define _PARCEL_PRDB_SPECTRA_H_

#pragma once

#include "LDatabase/LDatabase.h"
#include "PRDBSpectraSQL.h"
#include "PRDBSpectraDefType.h"

using namespace apjdb;

class CPRDBSpectra : public CLDatabase
{
public:

	CPRDBSpectra( void );
	virtual ~CPRDBSpectra( void );

	bool ApjProjectAddEx(CString ApjProjectName_OLD, const PRApjProject *const );
	bool ApjProjectDelete(CString ApjProjectName);
	bool ApjProjectsRead();
	bool ApjProjectGet(PRApjProject *const);
	bool ApjProjectGetWithSpecData(PRApjProject *const);

	bool ApjSampleAddEx(CString ApjProjectName, CString SampleName_OLD, const PRApjSample *const);
	bool ApjSampleDelete(CString ApjProjectName, CString SampleName);
	bool SetRefData(CString ApjProjectName, CString SampleName, CString ParamName, double Value, bool IsValue);
	bool ApjSpectrumAdd(CString ApjProjectName, CString SampleName, const PRApjSpectrum *const);
	bool ApjSpectrumDelete(CString ApjProjectName, CString SampleName, int SpecNum);
	int GetSpectrumNumMax(CString ApjProjectName, CString SampleName);
	bool ApjSpectrumDataGet(CString ApjProjectName, CString SampleName, PRApjSpectrum *const pSpec);

protected:

	bool IsApjProjectExists(CString ApjProjectName);
	bool IsApjSampleExists(CString SampleName, long lApj);
	bool IsApjSpectrumExists(int SpecNum, long lSam);

	bool projectAdd(const PRApjProject *const);
	bool projectUpdate(CString ApjProjectName_OLD, const PRApjProject *const );
	bool projectAddVect(long lApj, const PRApjProject *const pApj);

	bool sampleAdd(long lApj, const PRApjSample *const pSam);
	bool sampleUpdate(CString SampleName_OLD, long lApj, const PRApjSample *const pSam);
	bool sampleAddVect(long lApj, long lSam, const PRApjSample *const pSam);

	bool spectrumAdd(long lSam, const PRApjSpectrum *const pSpec);

	virtual void createTables(std::vector<CString>*);
	virtual long getCurrentVersion();
	virtual CString getCurrentDBName();	

	bool GetApjProjectSpectraID(CString ApjProjectName, std::vector<int>& SpecIDs);
	bool GetSampleSpectraID(CString ApjProjectName, CString SampleName, std::vector<int>& SpecIDs);
	bool DeleteSpectraFiles(const std::vector<int> *const SpecIDs);
	bool CheckDirectory();
	CString GetPathToSpec(int iSp);
};

#endif
