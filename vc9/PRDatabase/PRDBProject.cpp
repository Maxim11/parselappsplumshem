#include "../stdafx.h"

#include "PRDBProject.h"
#include "..\file4spa.h"
#include "..\FileSystem.h"

using namespace filesystem;
using namespace file4spa;

#define VerifySuccess if (res != LumexDB::enSuccess) { assert(false); return false; }
#define VerifySuccessAndNoData if (res != LumexDB::enSuccess && res != LumexDB::enNoData) { assert(false); return false; }
#define VerifyDataPointer if (pData == NULL) {	assert(false); return false; }
#define StringLen (int)(pData->m_Size / sizeof(TCHAR) + 1)
#define BufferLen (pData->m_Size + sizeof(TCHAR))

CPRDBProject::CPRDBProject() 
	: CLDatabase()
	, m_bSetIdentity(false)
	, m_bFailIfHistoryFolse(false)
	, m_history_one_per_one(0)
{
}

CPRDBProject::~CPRDBProject()
{
	CPRDBProject::Close();
}

void CPRDBProject::Close()
{
	addHistoryEvent( true, prdbHistoryType::PRDB_HISTORY_TYPE_CLOSE, prdbHistoryTarget::PRDB_HISTORY_TRG_UNTYPED );
	CLDatabase::Close();
}

bool CPRDBProject::SetOptions(PRUser &userIdentity, int iHistoryActivity, bool fFailIfHistoryFolse, bool bIsOpen )
{
	ASSERT( iHistoryActivity >= 0 && iHistoryActivity <= 2 );
		
	m_history_activity		= iHistoryActivity;	
	m_bFailIfHistoryFolse	= fFailIfHistoryFolse;
	m_user = userIdentity;
	m_bSetIdentity = true;
	return ( bIsOpen ? setUserIdentity( &userIdentity ) : true );
}

bool CPRDBProject::Open( LPCTSTR lpszFilePath, bool IsNeedCheck /* = false  */)
{
	ASSERT( m_bSetIdentity );
	bool bRes = CLDatabase::Open( lpszFilePath, IsNeedCheck );
	if ( !bRes )
		return false;
	
	bRes = setUserIdentity( &m_user );
	if ( !bRes )
		return m_bFailIfHistoryFolse ? false : true;

	return addHistoryEvent( bRes, prdbHistoryType::PRDB_HISTORY_TYPE_OPEN, prdbHistoryTarget::PRDB_HISTORY_TRG_UNTYPED );
}

bool CPRDBProject::CreateDatabase( LPCTSTR lpszPath )
{
	ASSERT( m_bSetIdentity );
	bool bRes = CLDatabase::CreateDatabase( lpszPath );
	if ( !bRes )
		return false;

	bRes = setUserIdentity( &m_user );
	if ( !bRes )
		return m_bFailIfHistoryFolse ? false : true;

	return addHistoryEvent( bRes, prdbHistoryType::PRDB_HISTORY_TYPE_CREATE, prdbHistoryTarget::PRDB_HISTORY_TRG_UNTYPED );
}

bool CPRDBProject::GetHistory(PRUser * pUserIdentity, vector<PRHistory> *pVHistory, bool bJustLast )
{
	m_sSQL.Format( bJustLast ? _T(PRDB_SQL_READ_HIS_USER_LAST) : _T(PRDB_SQL_READ_HIS_USER), replaceChar( pUserIdentity->UserNIC ) );
	if ( !selectSQL() )
		return false;

	if (m_pDataSet == NULL)
	{
		assert(false);
		return false;
	}

	PRHistory history;
	history.User = *pUserIdentity;

	pVHistory->clear();

	while (!m_pDataSet->IsEOF())
	{
		LumexDB::CDataPtr<ILumexDBDataSet::TColumnData> pData = m_pDataSet->GetColumn(0);
		VerifyDataPointer;
		TIMESTAMP_STRUCT date;
		LumexDB::EnOperationResult res = pData->GetData(&date, sizeof(date));
		VerifySuccess;		
		history.HistoryDate = CTime(date.year, date.month, date.day, date.hour, date.minute, date.second);

		pData = m_pDataSet->GetColumn(1);
		VerifyDataPointer;		
		res = pData->GetData(&history.EventType, sizeof(history.EventType));
		VerifySuccess;
		
		pData = m_pDataSet->GetColumn(2);
		VerifyDataPointer;		
		res = pData->GetData(&history.HistoryTarget, sizeof(history.HistoryTarget));
		VerifySuccess;		

		pVHistory->push_back( history );

		res = m_pDataSet->NextRow();
		VerifySuccessAndNoData;
	}

	return true;
}

bool CPRDBProject::GetHistory( vector<PRHistory> *pVHistory, bool bJustLast )
{
	m_sSQL = bJustLast ? _T(PRDB_SQL_READ_HIS_USER_LAST) : _T(PRDB_SQL_READ_HIS);
	if ( !selectSQL() )
		return false;

	if (m_pDataSet == NULL)
	{
		assert(false);
		return false;
	}

	PRHistory history;

	pVHistory->clear();

	while (!m_pDataSet->IsEOF())
	{
		LumexDB::CDataPtr<ILumexDBDataSet::TColumnData> pData = m_pDataSet->GetColumn(0);
		VerifyDataPointer;
		TIMESTAMP_STRUCT date;
		LumexDB::EnOperationResult res = pData->GetData(&date, sizeof(date));
		VerifySuccess;		
		history.HistoryDate = CTime(date.year, date.month, date.day, date.hour, date.minute, date.second);

		pData = m_pDataSet->GetColumn(1);
		VerifyDataPointer;		
		res = pData->GetData(&history.EventType, sizeof(history.EventType));
		VerifySuccess;

		pData = m_pDataSet->GetColumn(2);
		VerifyDataPointer;		
		res = pData->GetData(&history.HistoryTarget, sizeof(history.HistoryTarget));
		VerifySuccess;

		pData = m_pDataSet->GetColumn(3);
		VerifyDataPointer;
		LPTSTR lpBuf = history.User.UserNIC.GetBuffer(StringLen);
		res = pData->GetData(lpBuf, BufferLen, false);
		history.User.UserNIC.ReleaseBuffer();
		VerifySuccess;
		returnChar( history.User.UserNIC );

		pData = m_pDataSet->GetColumn(4);
		VerifyDataPointer;
		lpBuf = history.User.FirstName.GetBuffer(StringLen);
		res = pData->GetData(lpBuf, BufferLen, false);
		history.User.FirstName.ReleaseBuffer();
		VerifySuccess;
		returnChar( history.User.FirstName );

		pData = m_pDataSet->GetColumn(5);
		VerifyDataPointer;
		lpBuf = history.User.LastName.GetBuffer(StringLen);
		res = pData->GetData(lpBuf, BufferLen, false);
		history.User.LastName.ReleaseBuffer();
		VerifySuccess;
		returnChar( history.User.LastName );

		pData = m_pDataSet->GetColumn(6);
		VerifyDataPointer;
		lpBuf = history.User.MiddleName.GetBuffer(StringLen);
		res = pData->GetData(lpBuf, BufferLen, false);
		history.User.MiddleName.ReleaseBuffer();
		VerifySuccess;
		returnChar( history.User.MiddleName );

		pData = m_pDataSet->GetColumn(7);
		VerifyDataPointer;		
		res = pData->GetData(&history.User.Role.Type, sizeof(history.User.Role.Type));
		VerifySuccess;

		pData = m_pDataSet->GetColumn(8);
		VerifyDataPointer;
		lpBuf = history.User.Role.Description.GetBuffer(StringLen);
		res = pData->GetData(lpBuf, BufferLen, false);
		history.User.Role.Description.ReleaseBuffer();
		VerifySuccess;
		returnChar( history.User.Role.Description );

		pVHistory->push_back( history );

		res = m_pDataSet->NextRow();
		VerifySuccessAndNoData;		
	}

	return true;
}

bool CPRDBProject::addHistoryEvent( bool bPrevRes, int hType, int hTarget )
{
	ASSERT(m_bSetIdentity);
	if ( !bPrevRes )
		return false;

	if (m_pDB == NULL || !m_pDB->IsConnected())
	{
		return false;
	}

	if ( m_history_activity ==  prdbHistoryActivity::PRDB_HISTORY_ACTIVITY_NONE )
	{
		return true;
	}
	else if ( m_history_activity == prdbHistoryActivity::PRDB_HISTORY_ACTIVITY_ONE_PER_OPEN )
	{
		if ( !( m_history_one_per_one & hType ) )
			m_history_one_per_one |= hType;
		
		if ( hType != prdbHistoryType::PRDB_HISTORY_TYPE_CLOSE )
			return true;

        hType = m_history_one_per_one;
		hTarget = prdbHistoryType::PRDB_HISTORY_TYPE_UNTYPED;
	}

	CTime currTime = CTime::GetCurrentTime();
	m_sSQL.Format( _T(PRDB_SQL_INSERT_HISTORY), m_user_identity, currTime.Format( m_pszFormatCTime ), hType, hTarget );
	bool bRes = executeSQL();
	return ( m_bFailIfHistoryFolse ? bRes : true );
}

bool CPRDBProject::setUserIdentity(PRUser * pUserIdentity )
{
	m_user_identity = checkUser( pUserIdentity );
	if ( m_user_identity <= 0 )
		return false;

	m_bSetIdentity	= true;
	return true;
}

long CPRDBProject::checkRole(PRRoles *pRole )
{
	m_sSQL.Format( _T(PRDB_SQL_IS_EXITS_ROLE), pRole->Type );
	long lRes = isDataExist();
	if ( lRes <= 0 )
	{
		m_sSQL = _T(PRDB_SQL_MAX_ROLE);
		lRes = 1 + isDataExist();

		m_sSQL.Format( _T(PRDB_SQL_INSERT_ROLE), lRes, pRole->Type, replaceChar( pRole->Description ) );
		return ( executeSQL() ? lRes : -1 );
	}	
	return lRes;
}

long CPRDBProject::checkUser(PRUser *pUserIdentity )
{
	long lRole = checkRole( &pUserIdentity->Role );
	if ( lRole <= 0 )
		return -1;

	m_sSQL.Format( _T(PRDB_SQL_IS_EXITS_USER), replaceChar( pUserIdentity->UserNIC ) );
	long lRes = isDataExist();
	if ( lRes <= 0 )
	{
		m_sSQL = _T(PRDB_SQL_MAX_USER);
		lRes = 1 + isDataExist();

		m_sSQL.Format( _T(PRDB_SQL_INSERT_USER), lRes, lRole, 
			replaceChar( pUserIdentity->UserNIC ), replaceChar( pUserIdentity->FirstName ), replaceChar( pUserIdentity->LastName ), replaceChar( pUserIdentity->MiddleName ) );
		return ( executeSQL() ? lRes : -1 );
	}
	return lRes;
}

////////////////////////////////////////
//				������				  //
////////////////////////////////////////
bool CPRDBProject::ApplianceRead()
{
	m_sSQL = _T(PRDB_SQL_READ_APPLIENCE);
	return addHistoryEvent( selectSQL(), prdbHistoryType::PRDB_HISTORY_TYPE_READ, prdbHistoryTarget::PRDB_HISTORY_TRG_APPLIANCES );
}

bool CPRDBProject::ApplianceGet(PRAppliance *pA )
{
	if (m_pDataSet == NULL)
	{
		assert(false);
		return false;
	}

	LumexDB::CDataPtr<ILumexDBDataSet::TColumnData> pData = m_pDataSet->GetColumn(0);
	VerifyDataPointer;		
	LumexDB::EnOperationResult res = pData->GetData(&pA->ApplianceType, sizeof(pA->ApplianceType));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(1);
	VerifyDataPointer;		
	res = pData->GetData(&pA->ApplianceSN, sizeof(pA->ApplianceSN));
	VerifySuccess;

	return true;
}

bool CPRDBProject::ApplianceAddEx( int ApplianceSN_OLD, const PRAppliance *const pA )
{	
	if( IsApplianceExist( ApplianceSN_OLD ) ) 
		return addHistoryEvent( applianceUpdate( ApplianceSN_OLD, pA ), prdbHistoryType::PRDB_HISTORY_TYPE_UPDATE, prdbHistoryTarget::PRDB_HISTORY_TRG_APPLIANCES ); 
	else
		return addHistoryEvent( applianceAdd( pA ), prdbHistoryType::PRDB_HISTORY_TYPE_INSERT, prdbHistoryTarget::PRDB_HISTORY_TRG_APPLIANCES ); 
}

bool CPRDBProject::IsApplianceExist( int ApplianceSN )
{
	m_sSQL.Format(_T(PRDB_SQL_ISEXISTS_APPLIENCE), ApplianceSN);
    return isDataExist() > 0;
}

bool CPRDBProject::ApplianceDelete( int ApplianceSN )
{
	vector<int> SpecIDs;
	if(!GetApplianceSpectraID(ApplianceSN, SpecIDs))
	{
		assert(false);
		return false;
	}

	if(!DeleteSpectraFiles(&SpecIDs))
	{
		assert(false);
		return false;
	}

	m_sSQL.Format( _T(PRDB_SQL_DELETE_APPLIENCE), ApplianceSN );
	return addHistoryEvent( executeSQL(), prdbHistoryType::PRDB_HISTORY_TYPE_DELETE, prdbHistoryTarget::PRDB_HISTORY_TRG_APPLIANCES );
}

////////////////////////////////////////
//				������				  //
////////////////////////////////////////
bool CPRDBProject::ProjectAddEx( int ApplianceSN, CString ProjectName_OLD, const PRAnalisisProject *const pAP )
{
	if ( !IsApplianceExist( ApplianceSN ) )
		return false;
	if ( IsProjectExist( ApplianceSN, ProjectName_OLD ) )
		return addHistoryEvent( projectUpdate( ApplianceSN, ProjectName_OLD, pAP ), prdbHistoryType::PRDB_HISTORY_TYPE_UPDATE, prdbHistoryTarget::PRDB_HISTORY_TRG_APROJECTS );
	else
		return addHistoryEvent( projectAdd( ApplianceSN, pAP ), prdbHistoryType::PRDB_HISTORY_TYPE_INSERT, prdbHistoryTarget::PRDB_HISTORY_TRG_APROJECTS );
}


bool CPRDBProject::ProjectDelete( int ApplianceSN, CString ProjectName )
{
	vector<int> SpecIDs;
	if(!GetProjectSpectraID(ApplianceSN, ProjectName, SpecIDs))
	{
		assert(false);
		return false;
	}

	if(!DeleteSpectraFiles(&SpecIDs))
	{
		assert(false);
		return false;
	}

	m_sSQL.Format( _T(PRDB_SQL_DELETE_APROJ), ProjectName, ApplianceSN );
	return addHistoryEvent( executeSQL(), prdbHistoryType::PRDB_HISTORY_TYPE_DELETE, prdbHistoryTarget::PRDB_HISTORY_TRG_APROJECTS );
}

bool CPRDBProject::ProjectRead( int ApplianceSN )
{
	m_sSQL.Format( _T(PRDB_SQL_READ_APROJECT), ApplianceSN );
	return addHistoryEvent( selectSQL(), prdbHistoryType::PRDB_HISTORY_TYPE_READ, prdbHistoryTarget::PRDB_HISTORY_TRG_APROJECTS );
}

bool CPRDBProject::ProjectGet(PRAnalisisProject *const pAP)
{
	if (m_pDataSet == NULL)
	{
		assert(false);
		return false;
	}

	LumexDB::CDataPtr<ILumexDBDataSet::TColumnData> pData = m_pDataSet->GetColumn(0);
	VerifyDataPointer;	
	LPTSTR lpBuf = pAP->APName.GetBuffer(StringLen);	
	LumexDB::EnOperationResult res = pData->GetData(lpBuf, BufferLen, false);
	pAP->APName.ReleaseBuffer();
	VerifySuccess;
	returnChar( pAP->APName );

	pData = m_pDataSet->GetColumn(1);
	VerifyDataPointer;		
	res = pData->GetData(&pAP->SST, sizeof(pAP->SST));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(2);
	VerifyDataPointer;		
	res = pData->GetData(&pAP->SpRangeDown, sizeof(pAP->SpRangeDown));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(3);
	VerifyDataPointer;		
	res = pData->GetData(&pAP->SpRangeUp, sizeof(pAP->SpRangeUp));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(4);
	VerifyDataPointer;		
	res = pData->GetData(&pAP->SampleScanCount, sizeof(pAP->SampleScanCount));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(5);
	VerifyDataPointer;		
	res = pData->GetData(&pAP->SampleMeasCount, sizeof(pAP->SampleMeasCount));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(6);
	VerifyDataPointer;		
	res = pData->GetData(&pAP->StandardScanCount, sizeof(pAP->StandardScanCount));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(7);
	VerifyDataPointer;		
	res = pData->GetData(&pAP->StandardMeasCount, sizeof(pAP->StandardMeasCount));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(8);
	VerifyDataPointer;		
	res = pData->GetData(&pAP->CuvetteLength, sizeof(pAP->CuvetteLength));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(9);
	VerifyDataPointer;		
	res = pData->GetData(&pAP->SubChannelPermit, sizeof(pAP->SubChannelPermit));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(10);
	VerifyDataPointer;		
	res = pData->GetData(&pAP->SubChannelApodization, sizeof(pAP->SubChannelApodization));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(11);
	VerifyDataPointer;		
	res = pData->GetData(&pAP->ZeroFilling, sizeof(pAP->ZeroFilling));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(12);
	VerifyDataPointer;		
	res = pData->GetData(&pAP->UseRefChannel, sizeof(pAP->UseRefChannel));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(13);
	VerifyDataPointer;		
	res = pData->GetData(&pAP->RefChannelPermit, sizeof(pAP->RefChannelPermit));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(14);
	VerifyDataPointer;		
	res = pData->GetData(&pAP->RefChannelApodization, sizeof(pAP->RefChannelApodization));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(15);
	VerifyDataPointer;		
	res = pData->GetData(&pAP->TempRangeDown, sizeof(pAP->TempRangeDown));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(16);
	VerifyDataPointer;		
	res = pData->GetData(&pAP->TempRangeUp, sizeof(pAP->TempRangeUp));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(17);
	VerifyDataPointer;		
	res = pData->GetData(&pAP->TempDiff, sizeof(pAP->TempDiff));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(18);
	VerifyDataPointer;
	lpBuf = pAP->SamSortName.GetBuffer(StringLen);
	res = pData->GetData(lpBuf, BufferLen, false);
	pAP->SamSortName.ReleaseBuffer();
	VerifySuccess;
	returnChar( pAP->SamSortName );

	pData = m_pDataSet->GetColumn(19);
	VerifyDataPointer;		
	res = pData->GetData(&pAP->SamSortDir, sizeof(pAP->SamSortDir));
	VerifySuccess;	

	return true;
}

bool CPRDBProject::IsProjectExist( int ApplianceSN, CString ProjectName )
{
	m_sSQL.Format( _T(PRDB_SQL_ISEXISTS_APROJ), replaceChar( ProjectName ), ApplianceSN );
	return isDataExist() > 0;
}

////////////////////////////////////////
//			  ����������			  //
////////////////////////////////////////
bool CPRDBProject::IndexAddEx( int ApplianceSN, CString ProjectName, CString IndexName_OLD, const PRIndex *const pI)
{
	if ( !IsProjectExist( ApplianceSN, ProjectName ) )
		return false;
	if( IsIndexExist( ApplianceSN, ProjectName, IndexName_OLD ) )
		return addHistoryEvent( indexUpdate( ApplianceSN, ProjectName, IndexName_OLD, pI ), prdbHistoryType::PRDB_HISTORY_TYPE_UPDATE, prdbHistoryTarget::PRDB_HISTORY_TRG_INDICES );
	else
		return addHistoryEvent( indexAdd( ApplianceSN, ProjectName, pI ), prdbHistoryType::PRDB_HISTORY_TYPE_INSERT, prdbHistoryTarget::PRDB_HISTORY_TRG_INDICES );
}

bool CPRDBProject::IndexDelete( int ApplianceSN, CString ProjectName, CString IndexName )
{
	m_sSQL.Format( _T(PRDB_SQL_DELETE_IDX), replaceChar( IndexName ), replaceChar( ProjectName ), ApplianceSN );
	return addHistoryEvent( executeSQL(), prdbHistoryType::PRDB_HISTORY_TYPE_DELETE, prdbHistoryTarget::PRDB_HISTORY_TRG_INDICES );
}

bool CPRDBProject::IsIndexExist( int ApplianceSN, CString ProjectName, CString IndexName )
{
	m_sSQL.Format( _T(PRDB_SQL_ISEXISTS_IDX), replaceChar( IndexName ), replaceChar( ProjectName ), ApplianceSN );
	return isDataExist() > 0;
}

bool CPRDBProject::IndicesRead( int ApplianceSN, CString ProjectName )
{
	m_sSQL.Format( _T(PRDB_SQL_READ_IDX), replaceChar( ProjectName ), ApplianceSN );
	return addHistoryEvent( selectSQL(), prdbHistoryType::PRDB_HISTORY_TYPE_READ, prdbHistoryTarget::PRDB_HISTORY_TRG_INDICES );
}

bool CPRDBProject::IndexGet(PRIndex *const pI )
{
	if (m_pDataSet == NULL)
	{
		assert(false);
		return false;
	}

	LumexDB::CDataPtr<ILumexDBDataSet::TColumnData> pData = m_pDataSet->GetColumn(0);
	VerifyDataPointer;
	LPTSTR lpBuf = pI->IdxName.GetBuffer(StringLen);
	LumexDB::EnOperationResult res = pData->GetData(lpBuf, BufferLen, false);
	pI->IdxName.ReleaseBuffer();
	VerifySuccess;
	returnChar( pI->IdxName );

	pData = m_pDataSet->GetColumn(1);
	VerifyDataPointer;
	lpBuf = pI->IdxUnits.GetBuffer(StringLen);
	res = pData->GetData(lpBuf, BufferLen, false);
	pI->IdxUnits.ReleaseBuffer();
	VerifySuccess;
	returnChar( pI->IdxUnits );

	pData = m_pDataSet->GetColumn(2);
	VerifyDataPointer;		
	res = pData->GetData(&pI->IdxFormat, sizeof(pI->IdxFormat));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(3);
	VerifyDataPointer;		
	res = pData->GetData(&pI->StHumidity, sizeof(pI->StHumidity));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(4);
	VerifyDataPointer;		
	res = pData->GetData(&pI->IsUseStHumidity, sizeof(pI->IsUseStHumidity));
	VerifySuccess;

	return true;
}

////////////////////////////////////////
//	  ������ ������������ ��������	  //
////////////////////////////////////////
bool CPRDBProject::TransAddEx( int ApplianceSN, CString ProjectName, CString TransName_OLD, const PRTrans *const pI)
{
	if ( !IsProjectExist( ApplianceSN, ProjectName ) )
		return false;
	if( IsTransExist( ApplianceSN, ProjectName, TransName_OLD ) )
		return addHistoryEvent( transUpdate( ApplianceSN, ProjectName, TransName_OLD, pI ), prdbHistoryType::PRDB_HISTORY_TYPE_UPDATE, prdbHistoryTarget::PRDB_HISTORY_TRG_TRANS );
	else
		return addHistoryEvent( transAdd( ApplianceSN, ProjectName, pI ), prdbHistoryType::PRDB_HISTORY_TYPE_INSERT, prdbHistoryTarget::PRDB_HISTORY_TRG_TRANS );
}

bool CPRDBProject::TransDelete( int ApplianceSN, CString ProjectName, CString TransName )
{
	vector<int> SpecIDs;
	if(!GetTransSpectraID(ApplianceSN, ProjectName, TransName, SpecIDs))
	{
		assert(false);
		return false;
	}

	if(!DeleteSpectraFiles(&SpecIDs))
	{
		assert(false);
		return false;
	}

	m_sSQL.Format( _T(PRDB_SQL_DELETE_TRANS), replaceChar( TransName ), replaceChar( ProjectName ), ApplianceSN );
	return addHistoryEvent( executeSQL(), prdbHistoryType::PRDB_HISTORY_TYPE_DELETE, prdbHistoryTarget::PRDB_HISTORY_TRG_TRANS );
}

bool CPRDBProject::IsTransExist( int ApplianceSN, CString ProjectName, CString TransName )
{
	m_sSQL.Format( _T(PRDB_SQL_ISEXISTS_TRANS), replaceChar( TransName ), replaceChar( ProjectName ), ApplianceSN );
	return isDataExist() > 0;
}

bool CPRDBProject::TransRead( int ApplianceSN, CString ProjectName )
{
	m_sSQL.Format( _T(PRDB_SQL_READ_TRANS), replaceChar( ProjectName ), ApplianceSN );
	return addHistoryEvent( selectSQL(), prdbHistoryType::PRDB_HISTORY_TYPE_READ, prdbHistoryTarget::PRDB_HISTORY_TRG_TRANS );
}

bool CPRDBProject::TransGet(PRTrans *const pI )
{
	if (m_pDataSet == NULL)
	{
		assert(false);
		return false;
	}

	LumexDB::CDataPtr<ILumexDBDataSet::TColumnData> pData = m_pDataSet->GetColumn(0);
	VerifyDataPointer;
	LPTSTR lpBuf = pI->TransName.GetBuffer(StringLen);
	LumexDB::EnOperationResult res = pData->GetData(lpBuf, BufferLen, false);
	pI->TransName.ReleaseBuffer();
	VerifySuccess;
	returnChar( pI->TransName );

	pData = m_pDataSet->GetColumn(1);
	VerifyDataPointer;		
	res = pData->GetData(&pI->Type, sizeof(pI->Type));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(2);
	VerifyDataPointer;		
	res = pData->GetData(&pI->MasterDevType, sizeof(pI->MasterDevType));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(3);
	VerifyDataPointer;		
	res = pData->GetData(&pI->MasterDevNumber, sizeof(pI->MasterDevNumber));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(4);
	VerifyDataPointer;
	TIMESTAMP_STRUCT date;
	res = pData->GetData(&date, sizeof(date));
	VerifySuccess;		
	pI->DateCreated = CTime(date.year, date.month, date.day, date.hour, date.minute, date.second);

	pData = m_pDataSet->GetColumn(5);
	VerifyDataPointer;		
	res = pData->GetData(&pI->LowLim, sizeof(pI->LowLim));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(6);
	VerifyDataPointer;		
	res = pData->GetData(&pI->UpperLim, sizeof(pI->UpperLim));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(7);
	VerifyDataPointer;
	lpBuf = pI->Preproc.GetBuffer(StringLen);
	res = pData->GetData(lpBuf, BufferLen, false);
	pI->Preproc.ReleaseBuffer();
	VerifySuccess;
	returnChar( pI->Preproc );

	pData = m_pDataSet->GetColumn(8);
	VerifyDataPointer;
	lpBuf = pI->FrameName.GetBuffer(StringLen);
	res = pData->GetData(lpBuf, BufferLen, false);
	pI->FrameName.ReleaseBuffer();
	VerifySuccess;
	returnChar( pI->FrameName );
	/**/

	return true;
}

////////////////////////////////////////
//			  �������				  //
////////////////////////////////////////
bool CPRDBProject::SampleAddEx( int ApplianceSN, CString ProjectName, CString TransName, CString Sample1_OLD, const PRSample *const pS)
{
	if ( !IsTransExist( ApplianceSN, ProjectName, TransName ) )
		return false;
	if ( IsSampleExist( ApplianceSN, ProjectName, TransName, Sample1_OLD ) )
		return addHistoryEvent( sampleUpdate( ApplianceSN, ProjectName, TransName, Sample1_OLD, pS ), prdbHistoryType::PRDB_HISTORY_TYPE_UPDATE, prdbHistoryTarget::PRDB_HISTORY_TRG_SAMPLES );
	else
		return addHistoryEvent( sampleAdd( ApplianceSN, ProjectName, TransName, pS ), prdbHistoryType::PRDB_HISTORY_TYPE_INSERT, prdbHistoryTarget::PRDB_HISTORY_TRG_SAMPLES );
}

bool CPRDBProject::SampleDelete(int ApplianceSN, CString ProjectName, CString TransName, CString Sample1)
{
	vector<int> SpecIDs;
	if(!GetSampleSpectraID(ApplianceSN, ProjectName, TransName, Sample1, SpecIDs))
	{
		assert(false);
		return false;
	}

	if(!DeleteSpectraFiles(&SpecIDs))
	{
		assert(false);
		return false;
	}

	m_sSQL.Format( _T(PRDB_SQL_DELETE_SMPL), replaceChar( Sample1 ), replaceChar( TransName ), replaceChar( ProjectName ), ApplianceSN );
	return addHistoryEvent( executeSQL(), prdbHistoryType::PRDB_HISTORY_TYPE_DELETE, prdbHistoryTarget::PRDB_HISTORY_TRG_SAMPLES );
}

bool CPRDBProject::IsSampleExist( int ApplianceSN, CString ProjectName, CString TransName, CString Sample1 )
{
	m_sSQL.Format( _T(PRDB_SQL_ISEXISTS_SMPL), replaceChar( Sample1 ), replaceChar( TransName ), replaceChar( ProjectName ), ApplianceSN );
	return isDataExist() > 0;
}

		// ID
bool CPRDBProject::SampleGetID( int ApplianceSN, CString ProjectName, CString TransName, CString Sample1, int *pID )
{
	return sampleGetID(ApplianceSN, ProjectName, TransName, Sample1, pID); 
}



bool CPRDBProject::SamplesRead( int ApplianceSN, CString ProjectName, CString TransName )
{
	m_sSQL.Format( _T(PRDB_SQL_READ_SMPL), replaceChar( TransName ), replaceChar( ProjectName ), ApplianceSN );
	return addHistoryEvent( selectSQL(), prdbHistoryType::PRDB_HISTORY_TYPE_READ, prdbHistoryTarget::PRDB_HISTORY_TRG_SAMPLES );
}

bool CPRDBProject::SampleGet(PRSample *const pS)
{
	if (m_pDataSet == NULL)
	{
		assert(false);
		return false;
	}

	LumexDB::CDataPtr<ILumexDBDataSet::TColumnData> pData = m_pDataSet->GetColumn(0);
	VerifyDataPointer;
	LPTSTR lpBuf = pS->Sample1.GetBuffer(StringLen);
	LumexDB::EnOperationResult res = pData->GetData(lpBuf, BufferLen, false);
	pS->Sample1.ReleaseBuffer();
	VerifySuccess;
	returnChar( pS->Sample1 );

	pData = m_pDataSet->GetColumn(1);
	VerifyDataPointer;
	lpBuf = pS->Sample2.GetBuffer(StringLen);
	res = pData->GetData(lpBuf, BufferLen, false);
	pS->Sample2.ReleaseBuffer();
	VerifySuccess;
	returnChar( pS->Sample2 );

	pData = m_pDataSet->GetColumn(2);
	VerifyDataPointer;
	lpBuf = pS->Sample3.GetBuffer(StringLen);
	res = pData->GetData(lpBuf, BufferLen, false);
	pS->Sample3.ReleaseBuffer();
	VerifySuccess;
	returnChar( pS->Sample3 );	

	return true;
}
////////////////////////////////////////
//		  ����������� ������		  //
////////////////////////////////////////
bool CPRDBProject::RefDataAddEx( int ApplianceSN, CString ProjectName, CString TransName, CString Sample1, const PRRefData *const pRF )
{
	if ( !IsIndexExist( ApplianceSN, ProjectName, pRF->IdxName ) || !IsSampleExist( ApplianceSN, ProjectName, TransName, Sample1 ) )
		return false;
	if ( IsRefDataExist( ApplianceSN, ProjectName, TransName, Sample1, pRF->IdxName ) )
		return addHistoryEvent( refDataUpdate( ApplianceSN, ProjectName, TransName, Sample1, pRF ), prdbHistoryType::PRDB_HISTORY_TYPE_UPDATE, prdbHistoryTarget::PRDB_HISTORY_TRG_REFDATAS );
	else
		return addHistoryEvent( refDataAdd( ApplianceSN, ProjectName, TransName, Sample1, pRF ), prdbHistoryType::PRDB_HISTORY_TYPE_INSERT, prdbHistoryTarget::PRDB_HISTORY_TRG_REFDATAS );
}

bool CPRDBProject::IsRefDataExist( int ApplianceSN, CString ProjectName, CString TransName, CString Sample1, CString IndexName )
{
	m_sSQL.Format( _T(PRDB_SQL_ISEXISTS_RFDT), replaceChar( IndexName ), replaceChar( ProjectName ), ApplianceSN, replaceChar( Sample1 ), replaceChar( TransName ), replaceChar( ProjectName ), ApplianceSN );
	return isDataExist() > 0;
}

bool CPRDBProject::RefDataDelete( int ApplianceSN, CString ProjectName, CString TransName, CString Sample1, CString IndexName )
{
	m_sSQL.Format( _T(PRDB_SQL_DELETE_RFDT), replaceChar( IndexName ), replaceChar( ProjectName ), ApplianceSN, replaceChar( Sample1 ), replaceChar( TransName ), replaceChar( ProjectName ), ApplianceSN );
	return addHistoryEvent( executeSQL(), prdbHistoryType::PRDB_HISTORY_TYPE_DELETE, prdbHistoryTarget::PRDB_HISTORY_TRG_REFDATAS );
}

bool CPRDBProject::RefDataRead( int ApplianceSN, CString ProjectName, CString TransName, CString Sample1 )
{
	m_sSQL.Format( _T(PRDB_SQL_READ_RFDT), replaceChar( Sample1 ), replaceChar( TransName ), replaceChar( ProjectName ), ApplianceSN );
	return addHistoryEvent( selectSQL(), prdbHistoryType::PRDB_HISTORY_TYPE_READ, prdbHistoryTarget::PRDB_HISTORY_TRG_REFDATAS );
}

bool CPRDBProject::RefDataReadBySampleID( int SampleID )
{
	m_sSQL.Format( _T(PRDB_SQL_READ_RFDT_BYID), SampleID);
	return addHistoryEvent( selectSQL(), prdbHistoryType::PRDB_HISTORY_TYPE_READ, prdbHistoryTarget::PRDB_HISTORY_TRG_REFDATAS );
}


bool CPRDBProject::RefDataGet(PRRefData *const pRF)
{
	if (m_pDataSet == NULL)
	{
		assert(false);
		return false;
	}

	LumexDB::CDataPtr<ILumexDBDataSet::TColumnData> pData = m_pDataSet->GetColumn(0);
	VerifyDataPointer;
	LPTSTR lpBuf = pRF->IdxName.GetBuffer(StringLen);
	LumexDB::EnOperationResult res = pData->GetData(lpBuf, BufferLen, false);
	pRF->IdxName.ReleaseBuffer();
	VerifySuccess;
	returnChar( pRF->IdxName );

	pData = m_pDataSet->GetColumn(1);
	VerifyDataPointer;		
	res = pData->GetData(&pRF->IsDataExists, sizeof(pRF->IsDataExists));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(2);
	VerifyDataPointer;		
	res = pData->GetData(&pRF->RefData, sizeof(pRF->RefData));
	VerifySuccess;

	return true;
}
////////////////////////////////////////
//				������				  //
////////////////////////////////////////

bool CPRDBProject::SpectrumAddEx( int ApplianceSN, CString ProjectName, CString TransName, CString Sample1, const PRSpectrum *const pSp )
{
	if ( !IsSampleExist( ApplianceSN, ProjectName, TransName, Sample1 ) )
		return false;

	if(!IsSpectrumExist( ApplianceSN, ProjectName, TransName, Sample1, pSp->SpecNum ))
		return spectrumAdd( ApplianceSN, ProjectName, TransName, Sample1, pSp );
	else
		return spectrumUpdateUse( ApplianceSN, ProjectName, TransName, Sample1, pSp );
}

bool CPRDBProject::SpectrumDelete(int ApplianceSN, CString ProjectName, CString TransName, CString Sample1, int Num)
{
	vector<int> SpecIDs;
	m_sSQL.Format(_T(PRDB_SQL_SELECT_SPECTRUM_ID), Num, replaceChar(Sample1), replaceChar(TransName), replaceChar(ProjectName), ApplianceSN);
	long lSpecID = isDataExist();
	if(lSpecID <= 0)
		return false;

	SpecIDs.push_back(lSpecID);
	if(!DeleteSpectraFiles(&SpecIDs))
		return false;

	m_sSQL.Format( _T(PRDB_SQL_DELETE_SPRM), Num, replaceChar(Sample1), replaceChar(TransName), replaceChar(ProjectName), ApplianceSN);
	return addHistoryEvent( executeSQL(), prdbHistoryType::PRDB_HISTORY_TYPE_DELETE, prdbHistoryTarget::PRDB_HISTORY_TRG_REFDATAS );
}

bool CPRDBProject::SpectrumLinkGet( int ApplianceSN, CString ProjectName, CString TransName, CString Sample1, vector<int> *const pSpL )
{
	m_sSQL.Format( _T(PRDB_SQL_READ_SPRM_LINK), replaceChar( Sample1 ), replaceChar( TransName ), replaceChar( ProjectName ), ApplianceSN );

//	double tll1 = GetLLTimerSec();
	if ( !selectSQL() )
		return false;
//	double dtll = GetLLTimerSec()-tll1;
//	TRACE1("%f\n",dtll);

	if (m_pDataSet == NULL)
	{
		assert(false);
		return false;
	}

	pSpL->clear();

	while (!m_pDataSet->IsEOF())
	{
		LumexDB::CDataPtr<ILumexDBDataSet::TColumnData> pData = m_pDataSet->GetColumn(0);
		VerifyDataPointer;
		int nData;
		LumexDB::EnOperationResult res = pData->GetData(&nData, sizeof(nData));
		VerifySuccess;

		pSpL->push_back( nData );

		res = m_pDataSet->NextRow();
		VerifySuccessAndNoData;		
	}

	return true;
}
bool CPRDBProject::IsSpectrumExist( int ApplianceSN, CString ProjectName, CString TransName, CString Sample1, int spNum )
{
	m_sSQL.Format( _T(PRDB_SQL_ISEXISTS_SPRM), spNum, replaceChar( Sample1 ), replaceChar( TransName ), replaceChar( ProjectName ), ApplianceSN );
	return isDataExist() > 0;
}
bool CPRDBProject::SpectrumGet( int ApplianceSN, CString ProjectName, CString TransName, CString Sample1, PRSpectrum *const pSpL )
{
	return spectrumGet( ApplianceSN, ProjectName, TransName, Sample1, true, pSpL, NULL );
}

bool CPRDBProject::SpectrumGet( int ApplianceSN, CString ProjectName, CString TransName, CString Sample1, PRSpectrumLite *const pSpLL )
{
	return spectrumGet( ApplianceSN, ProjectName, TransName, Sample1, false, NULL, pSpLL );
}

bool CPRDBProject::SpectrumGetByID( int nSpecID, PRSpectrum *const pSpL)
{
	return spectrumGetByID( nSpecID, true, pSpL, NULL );
}

bool CPRDBProject::SpectrumGetByID( int nSpecID, PRSpectrumLite *const  pSpLL)
{
	return spectrumGetByID( nSpecID, false, NULL, pSpLL );
}


////////////////////////////////////////
//				������				  //
////////////////////////////////////////
bool CPRDBProject::ModelAddEx( int ApplianceSN, CString ProjectName, CString ModelName_OLD, const PRModel *const pM )
{
	return IsModelExist( ApplianceSN, ProjectName, ModelName_OLD ) ? modelUpdate( ApplianceSN, ProjectName, ModelName_OLD, pM ) : modelAdd( ApplianceSN, ProjectName, pM );
}


bool CPRDBProject::IsModelExist( int ApplianceSN, CString ProjectName, CString ModelName )
{
	m_sSQL.Format( _T(PRDB_SQL_ISEXISTS_MDL), replaceChar( ModelName ), replaceChar( ProjectName ), ApplianceSN );
	return isDataExist() > 0;
}
bool CPRDBProject::ModelDelete( int ApplianceSN, CString ProjectName, CString ModelName )
{
	m_sSQL.Format( _T(PRDB_SQL_DELETE_MDL), replaceChar( ModelName ), replaceChar( ProjectName ), ApplianceSN );
	return addHistoryEvent( executeSQL(), prdbHistoryType::PRDB_HISTORY_TYPE_DELETE, prdbHistoryTarget::PRDB_HISTORY_TRG_MODELS );
}

bool CPRDBProject::ModelRead( int ApplianceSN, CString ProjectName )
{
	m_sSQL.Format( _T(PRDB_SQL_READ_MDL), replaceChar( ProjectName ), ApplianceSN );
	return addHistoryEvent( selectSQL(), prdbHistoryType::PRDB_HISTORY_TYPE_READ, prdbHistoryTarget::PRDB_HISTORY_TRG_MODELS );
}

bool CPRDBProject::ModelGet(PRModel *const pM )
{
	if (m_pDataSet == NULL)
	{
		assert(false);
		return false;
	}

	LumexDB::CDataPtr<ILumexDBDataSet::TColumnData> pData = m_pDataSet->GetColumn(0);
	VerifyDataPointer;	
	long lM;
	LumexDB::EnOperationResult res = pData->GetData(&lM, sizeof(lM));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(1);
	VerifyDataPointer;
	LPTSTR lpBuf = pM->ModelName.GetBuffer(StringLen);
	res = pData->GetData(lpBuf, BufferLen, false);
	pM->ModelName.ReleaseBuffer();
	VerifySuccess;
	returnChar( pM->ModelName );

	pData = m_pDataSet->GetColumn(2);
	VerifyDataPointer;
	lpBuf = pM->TransName.GetBuffer(StringLen);
	res = pData->GetData(lpBuf, BufferLen, false);
	pM->TransName.ReleaseBuffer();
	VerifySuccess;
	returnChar( pM->TransName );

	pData = m_pDataSet->GetColumn(3);
	VerifyDataPointer;
	TIMESTAMP_STRUCT date;
	res = pData->GetData(&date, sizeof(date));
	VerifySuccess;		
	pM->DateCreated = CTime(date.year, date.month, date.day, date.hour, date.minute, date.second);
	
	pData = m_pDataSet->GetColumn(4);
	VerifyDataPointer;	
	res = pData->GetData(&pM->AnalisisType, sizeof(pM->AnalisisType));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(5);
	VerifyDataPointer;	
	res = pData->GetData(&pM->ModelType, sizeof(pM->ModelType));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(6);
	VerifyDataPointer;	
	res = pData->GetData(&pM->NumComponentQNT, sizeof(pM->NumComponentQNT));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(7);
	VerifyDataPointer;	
	res = pData->GetData(&pM->NumComponentQLT, sizeof(pM->NumComponentQLT));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(8);
	VerifyDataPointer;	
	res = pData->GetData(&pM->HypercubeVolume, sizeof(pM->HypercubeVolume));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(9);
	VerifyDataPointer;	
	res = pData->GetData(&pM->HarmonyCount, sizeof(pM->HarmonyCount));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(10);
	VerifyDataPointer;	
	res = pData->GetData(&pM->Nonlinearity, sizeof(pM->Nonlinearity));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(11);
	VerifyDataPointer;	
	res = pData->GetData(&pM->PredH, sizeof(pM->PredH));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(12);
	VerifyDataPointer;	
	res = pData->GetData(&pM->PredCKO, sizeof(pM->PredCKO));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(13);
	VerifyDataPointer;		
	res = pData->GetData(&pM->UseSECV, sizeof(pM->UseSECV));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(14);
	VerifyDataPointer;	
	res = pData->GetData(&pM->SpectrumType, sizeof(pM->SpectrumType));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(15);
	VerifyDataPointer;	
	res = pData->GetData(&pM->SpRangeRealDown, sizeof(pM->SpRangeRealDown));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(16);
	VerifyDataPointer;	
	res = pData->GetData(&pM->SpRangeRealUp, sizeof(pM->SpRangeRealUp));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(17);
	VerifyDataPointer;
	lpBuf = pM->Preproc.GetBuffer(StringLen);
	res = pData->GetData(lpBuf, BufferLen, false);
	pM->Preproc.ReleaseBuffer();
	VerifySuccess;
	returnChar( pM->Preproc );	
	
	if (m_pDB == NULL || !m_pDB->IsConnected())
	{
		assert(false);
		return false;
	}

	CString		sql;
	// ���. � �����������
	{
		sql.Format( _T(PRDB_SQL_READ_MDL_IX), lM );
		
		ILumexDBDataSet *pDataSet;
		LumexDB::EnOperationResult res = m_pDB->ExecuteSQL(sql, &pDataSet);
		if (res != LumexDB::enSuccess || pDataSet == NULL)
		{
			assert(false);
			return false;
		}
		
		pM->Indcies.clear();

		PRModel::PRMDParam prm;
		while ( !pDataSet->IsEOF() )
		{
			pData = pDataSet->GetColumn(0);
			VerifyDataPointer;
			lpBuf = prm.IndexName.GetBuffer(StringLen);
			res = pData->GetData(lpBuf, BufferLen, false);
			prm.IndexName.ReleaseBuffer();
			VerifySuccess;
			returnChar( prm.IndexName );	

			pData = pDataSet->GetColumn(1);
			VerifyDataPointer;	
			res = pData->GetData(&prm.PredCKOIX, sizeof(prm.PredCKOIX));
			VerifySuccess;

			pData = pDataSet->GetColumn(2);
			VerifyDataPointer;	
			res = pData->GetData(&prm.CalibDown, sizeof(prm.CalibDown));
			VerifySuccess;

			pData = pDataSet->GetColumn(3);
			VerifyDataPointer;	
			res = pData->GetData(&prm.CalibUp, sizeof(prm.CalibUp));
			VerifySuccess;

			pData = pDataSet->GetColumn(4);
			VerifyDataPointer;	
			res = pData->GetData(&prm.CorrB, sizeof(prm.CorrB));
			VerifySuccess;

			pData = pDataSet->GetColumn(5);
			VerifyDataPointer;	
			res = pData->GetData(&prm.CorrA, sizeof(prm.CorrA));
			VerifySuccess;

			pM->Indcies.push_back( prm );
			
			res = pDataSet->NextRow();
			VerifySuccessAndNoData;
		}

		pDataSet->Release();
	}

	// ������ ����������� ������������ �����
	{
		sql.Format( _T(PRDB_SQL_READ_MDL_SE), lM );
		
		ILumexDBDataSet *pDataSet;
		LumexDB::EnOperationResult res = m_pDB->ExecuteSQL(sql, &pDataSet);
		if (res != LumexDB::enSuccess || pDataSet == NULL)
		{
			assert(false);
			return false;
		}

		pM->SpDataExclude.clear();

		while ( !pDataSet->IsEOF() )
		{
			int nData;
			pData = pDataSet->GetColumn(0);
			VerifyDataPointer;	
			res = pData->GetData(&nData, sizeof(nData));
			VerifySuccess;
			
			pM->SpDataExclude.push_back( nData );
			
			res = pDataSet->NextRow();
			VerifySuccessAndNoData;
		}
		pDataSet->Release();
	}

	// ������� ��� ����������� � ���������

	CString tmpSampleName;
	CString tmpTransName;

	sql.Format( _T(PRDB_SQL_READ_MDL_SP), lM );

	ILumexDBDataSet *pDataSet1;
	LumexDB::EnOperationResult res1 = m_pDB->ExecuteSQL(sql, &pDataSet1);
	if (res1 != LumexDB::enSuccess || pDataSet1 == NULL)
	{
		assert(false);
		return false;
	}

	pM->Spectra.clear();	
	while ( !pDataSet1->IsEOF() )
	{
		PRModel::PRMDSpectrum spec;

		pData = pDataSet1->GetColumn(0);
		VerifyDataPointer;	
		long lSpecID;
		res = pData->GetData(&lSpecID, sizeof(lSpecID));
		VerifySuccess;

		pData = pDataSet1->GetColumn(1);
		VerifyDataPointer;	
		res = pData->GetData(&spec.IsCalib, sizeof(spec.IsCalib));
		VerifySuccess;

		pData = pDataSet1->GetColumn(2);
		VerifyDataPointer;	
		res = pData->GetData(&spec.IsValid, sizeof(spec.IsValid));
		VerifySuccess;			

		sql.Format( _T(PRDB_SQL_SELECT_SPRM_SPECNUM), lSpecID );
		ILumexDBDataSet *pDataSet2;
		LumexDB::EnOperationResult res = m_pDB->ExecuteSQL(sql, &pDataSet2);
		if (res != LumexDB::enSuccess || pDataSet2 == NULL)
		{
			assert(false);
			return false;
		}

		pData = pDataSet2->GetColumn(0);
		VerifyDataPointer;
		res = pData->GetData(&spec.SpNum, sizeof(spec.SpNum));
		VerifySuccess;

		pDataSet2->Release();

		sql.Format( _T(PRDB_SQL_SELECT_SPRM_SMPL), lSpecID );
		ILumexDBDataSet *pDataSet3;
		LumexDB::EnOperationResult res3 = m_pDB->ExecuteSQL(sql, &pDataSet3);
		if (res3 != LumexDB::enSuccess || pDataSet3 == NULL)
		{
			assert(false);
			return false;
		}

		pData = pDataSet3->GetColumn(0);
		VerifyDataPointer;
		lpBuf = tmpSampleName.GetBuffer(StringLen);
		res = pData->GetData(lpBuf, BufferLen, false);
		tmpSampleName.ReleaseBuffer();
		VerifySuccess;
		returnChar( tmpSampleName );
		spec.SampleName = tmpSampleName;

		pData = pDataSet3->GetColumn(1);
		VerifyDataPointer;	
		long lSampleID;
		res = pData->GetData(&lSampleID, sizeof(lSampleID));
		VerifySuccess;

		pData = pDataSet3->GetColumn(2);
		VerifyDataPointer;	
		long lTransID;
		res = pData->GetData(&lTransID, sizeof(lTransID));
		VerifySuccess;

		pDataSet3->Release();

		sql.Format( _T(PRBD_SQL_GETNAME_TRANS), lTransID );
		ILumexDBDataSet *pDataSet4;
		LumexDB::EnOperationResult res4 = m_pDB->ExecuteSQL(sql, &pDataSet4);
		if (res4 != LumexDB::enSuccess || pDataSet4 == NULL)
		{
			assert(false);
			return false;
		}

		pData = pDataSet4->GetColumn(0);
		VerifyDataPointer;
		lpBuf = tmpTransName.GetBuffer(StringLen);
		res = pData->GetData(lpBuf, BufferLen, false);
		tmpTransName.ReleaseBuffer();
		VerifySuccess;
		returnChar( tmpTransName );
		spec.TransName = tmpTransName;

		pDataSet4->Release();

		pM->Spectra.push_back(spec);

		res = pDataSet1->NextRow();
		VerifySuccessAndNoData;
	}

	pDataSet1->Release();

	return true;
}

////////////////////////////////////////
//				�����				  //
////////////////////////////////////////

bool CPRDBProject::MethodAddEx( int ApplianceSN, CString ProjectName, CString MethodName_OLD, const PRMethod *const pM )
{
	if ( IsMethodExist( ApplianceSN, ProjectName, MethodName_OLD  ) )
	{
		return methodUpdate( ApplianceSN, ProjectName, MethodName_OLD, pM );
	}

	if ( !this->BeginTrans() ) return false;

	if ( !methodAdd( ApplianceSN, ProjectName, pM ) )
	{
		this->Rollback();
		return false;
	}	
	return this->CommitTrans();
}

bool CPRDBProject::IsMethodExist( int ApplianceSN, CString ProjectName, CString MethodName )
{
	m_sSQL.Format( _T(PRDB_SQL_IS_EXISTS_MHD_EX), replaceChar( MethodName ), replaceChar( ProjectName ), ApplianceSN );
	return isDataExist() > 0;
}

bool CPRDBProject::MethodDelete( int ApplianceSN, CString ProjectName, CString MethodName )
{
	m_sSQL.Format( _T(PRDB_SQL_DELETE_MHD_EX), replaceChar( MethodName ), replaceChar( ProjectName ), ApplianceSN );
	return addHistoryEvent( executeSQL(), prdbHistoryType::PRDB_HISTORY_TYPE_DELETE, prdbHistoryTarget::PRDB_HISTORY_TRG_METHODS );
}
bool CPRDBProject::MethodRead( int ApplianceSN, CString ProjectName )
{
	m_sSQL.Format( _T(PRDB_SQL_READ_MHD), replaceChar( ProjectName ), ApplianceSN );
	return addHistoryEvent( selectSQL(), prdbHistoryType::PRDB_HISTORY_TYPE_READ, prdbHistoryTarget::PRDB_HISTORY_TRG_METHODS );
}

bool CPRDBProject::MethodGet(PRMethod *const pM )
{
	if (m_pDataSet == NULL || m_pDB == NULL || !m_pDB->IsConnected())
	{
		assert(false);
		return false;
	}

	LumexDB::CDataPtr<ILumexDBDataSet::TColumnData> pData = m_pDataSet->GetColumn(0);
	VerifyDataPointer;
	long lM;
	LumexDB::EnOperationResult res = pData->GetData(&lM, sizeof(lM));
	VerifySuccess;			

	pData = m_pDataSet->GetColumn(1);
	VerifyDataPointer;
	LPTSTR lpBuf = pM->MethodName.GetBuffer(StringLen);
	res = pData->GetData(lpBuf, BufferLen, false);
	pM->MethodName.ReleaseBuffer();
	VerifySuccess;
	returnChar( pM->MethodName );

	pData = m_pDataSet->GetColumn(2);
	VerifyDataPointer;
	TIMESTAMP_STRUCT date;
	res = pData->GetData(&date, sizeof(date));
	VerifySuccess;		
	pM->DateCreated = CTime(date.year, date.month, date.day, date.hour, date.minute, date.second);

	pData = m_pDataSet->GetColumn(3);
	VerifyDataPointer;	
	res = pData->GetData(&pM->AnalisisType, sizeof(pM->AnalisisType));
	VerifySuccess;			

	pData = m_pDataSet->GetColumn(4);
	VerifyDataPointer;
	lpBuf = pM->IndexAsHum.GetBuffer(StringLen);
	res = pData->GetData(lpBuf, BufferLen, false);
	pM->IndexAsHum.ReleaseBuffer();
	VerifySuccess;
	returnChar( pM->IndexAsHum );

	pData = m_pDataSet->GetColumn(5);
	VerifyDataPointer;	
	res = pData->GetData(&pM->TempRangeRealDown, sizeof(pM->TempRangeRealDown));
	VerifySuccess;			

	pData = m_pDataSet->GetColumn(6);
	VerifyDataPointer;	
	res = pData->GetData(&pM->TempRangeRealUp, sizeof(pM->TempRangeRealUp));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(7);
	VerifyDataPointer;	
	res = pData->GetData(&pM->TempDiff, sizeof(pM->TempDiff));
	VerifySuccess;

	CString sql;
	CString tmp = cEmpty;

	// ������ ������� QLT
	{
		sql.Format( _T(PRDB_SQL_READ_MHD_MQLT), lM );

		ILumexDBDataSet *pDataSet;
		LumexDB::EnOperationResult res = m_pDB->ExecuteSQL(sql, &pDataSet);
		if (res != LumexDB::enSuccess || pDataSet == NULL)
		{
			assert(false);
			return false;
		}
		
		pM->QltModels.clear();
		while ( !pDataSet->IsEOF() )
		{
			int lMtdQltMod;
			pData = pDataSet->GetColumn(0);
			VerifyDataPointer;	
			res = pData->GetData(&lMtdQltMod, sizeof(lMtdQltMod));
			VerifySuccess;			

			int QInd;
			pData = pDataSet->GetColumn(1);
			VerifyDataPointer;	
			res = pData->GetData(&QInd, sizeof(QInd));
			VerifySuccess;			

			int lMod;
			pData = pDataSet->GetColumn(2);
			VerifyDataPointer;	
			res = pData->GetData(&lMod, sizeof(lMod));
			VerifySuccess;			

			CString sql2;
			ILumexDBDataSet *pDataSet2;
			sql2.Format(_T(PRDB_SQL_GET_MDL_NAME), lMod);
				
			LumexDB::EnOperationResult res2 = m_pDB->ExecuteSQL(sql2, &pDataSet2);
			if(res2 != LumexDB::enSuccess || pDataSet2 == NULL)
			{
				assert(false);
				return false;
			}

			while(!pDataSet2->IsEOF())
			{
				pData = pDataSet2->GetColumn(0);
				VerifyDataPointer;
				lpBuf = tmp.GetBuffer(StringLen);
				res2 = pData->GetData(lpBuf, BufferLen, false);
				tmp.ReleaseBuffer();
				VerifySuccess;
				returnChar(tmp);
				
				res2 = pDataSet2->NextRow();
				VerifySuccessAndNoData;
			}
			pDataSet2->Release();
/*
			pData = m_pDataSet->GetColumn(2);
			VerifyDataPointer;
			lpBuf = tmp.GetBuffer(StringLen);
			res = pData->GetData(lpBuf, BufferLen, false);
			tmp.ReleaseBuffer();
			VerifySuccess;
			returnChar( tmp );
*/
			pM->QltModels[QInd] = tmp;
			
			res2 = pDataSet->NextRow();
			VerifySuccessAndNoData;
		}

		pDataSet->Release();
	}

	// ������ ������������� �������

	{
		sql.Format( _T(PRDB_SQL_READ_MHD_QLT_FORM), lM );

		ILumexDBDataSet *pDataSet;
		LumexDB::EnOperationResult res = m_pDB->ExecuteSQL(sql, &pDataSet);
		if (res != LumexDB::enSuccess || pDataSet == NULL)
		{
			assert(false);
			return false;
		}

		pM->Formula.clear();
		while ( !pDataSet->IsEOF() )
		{
			PRMethod::PRMTElement NewElem;

			pData = pDataSet->GetColumn(0);
			VerifyDataPointer;	
			res = pData->GetData(&NewElem.Type, sizeof(NewElem.Type));
			VerifySuccess;			

			pData = pDataSet->GetColumn(1);
			VerifyDataPointer;	
			res = pData->GetData(&NewElem.ModelInd, sizeof(NewElem.ModelInd));
			VerifySuccess;			

			pM->Formula.push_back(NewElem);
			
			res = pDataSet->NextRow();
			VerifySuccessAndNoData;
		}

		pDataSet->Release();
	}

	// IX
	{
		sql.Format( _T(PRDB_SQL_READ_MHD_MIX), lM );
		
		ILumexDBDataSet *pDataSet;
		LumexDB::EnOperationResult res = m_pDB->ExecuteSQL(sql, &pDataSet);
		if (res != LumexDB::enSuccess || pDataSet == NULL)
		{
			assert(false);
			return false;
		}

		pM->Index.clear();

		while ( !pDataSet->IsEOF() )
		{
			PRMethod::PRMTParam prm;

			pData = pDataSet->GetColumn(0);
			VerifyDataPointer;
			long lIX;
			res = pData->GetData(&lIX, sizeof(lIX));
			VerifySuccess;

			pData = pDataSet->GetColumn(1);
			VerifyDataPointer;
			lpBuf = prm.IndexName.GetBuffer(StringLen);
			res = pData->GetData(lpBuf, BufferLen, false);
			prm.IndexName.ReleaseBuffer();
			VerifySuccess;
			returnChar( prm.IndexName );

			pData = pDataSet->GetColumn(2);
			VerifyDataPointer;
			lpBuf = prm.MainModel.GetBuffer(StringLen);
			res = pData->GetData(lpBuf, BufferLen, false);
			prm.MainModel.ReleaseBuffer();
			VerifySuccess;
			returnChar( prm.MainModel );

			pData = pDataSet->GetColumn(3);
			VerifyDataPointer;			
			res = pData->GetData(&prm.Moisture, sizeof(prm.Moisture));
			VerifySuccess;

			pData = pDataSet->GetColumn(4);
			VerifyDataPointer;		
			res = pData->GetData(&prm.UseStdMoisture, sizeof(prm.UseStdMoisture));
			VerifySuccess;

			// IX_IX
			sql.Format( _T(PRDB_SQL_READ_MHD_MIX_IX), lIX );

			ILumexDBDataSet *pDataSet1;
			LumexDB::EnOperationResult res = m_pDB->ExecuteSQL(sql, &pDataSet1);
			if (res != LumexDB::enSuccess || pDataSet1 == NULL)
			{
				assert(false);
				return false;
			}

			while ( !pDataSet1->IsEOF() )
			{
				pData = pDataSet1->GetColumn(0);
				VerifyDataPointer;
				lpBuf = tmp.GetBuffer(StringLen);
				res = pData->GetData(lpBuf, BufferLen, false);
				tmp.ReleaseBuffer();
				VerifySuccess;
				returnChar( tmp );				

				prm.AddModels.push_back( tmp );

				res = pDataSet1->NextRow();
				VerifySuccessAndNoData;
			}
			pDataSet1->Release();
			
			pM->Index.push_back( prm );
			
			res = pDataSet->NextRow();
			VerifySuccessAndNoData;
		}
		pDataSet->Release();
	}

	return true;
}

////////////////////////////////////////
//			PROTECTED				  //
////////////////////////////////////////
bool CPRDBProject::spectrumGetByID( int nSpecID, bool bFull, PRSpectrum *const pS, PRSpectrumLite *const pL)
{
	m_sSQL.Format( _T(PRDB_SQL_READ_SPRM_BYID), nSpecID );
	if ( !selectSQL() )
		return false;

	if (m_pDataSet == NULL)
	{
		assert(false);
		return false;
	}

	LumexDB::CDataPtr<ILumexDBDataSet::TColumnData> pData = m_pDataSet->GetColumn(0);
	VerifyDataPointer;
	TIMESTAMP_STRUCT date;
	LumexDB::EnOperationResult res = pData->GetData(&date, sizeof(date));
	VerifySuccess;	
	( bFull ? pS->DateCreated : pL->DateCreated ) = CTime(date.year, date.month, date.day, date.hour, date.minute, date.second);

	pData = m_pDataSet->GetColumn(1);
	VerifyDataPointer;
	bool bVal;
	res = pData->GetData(&bVal, sizeof(bVal));
	VerifySuccess;
	( bFull ? pS->IsUse : pL->IsUse ) = bVal;

	pData = m_pDataSet->GetColumn(2);
	VerifyDataPointer;
	int lVal;
	res = pData->GetData(&lVal, sizeof(lVal));
	VerifySuccess;		
	( bFull ? pS->SpecNum : pL->SpecNum ) = lVal;

	CString Path = GetPathToSpec(nSpecID);
	if(!IsExistFile(Path))
	{
		assert(false);
		return false;
	}

	if(bFull)
	{
		pS->Vc.clear();

		bool res = ReadSpaDataFromFile(Path, pS->Vc);
		if(!res)
		{
			assert(false);
			return false;
		}
	}

	return addHistoryEvent( true, prdbHistoryType::PRDB_HISTORY_TYPE_READ, prdbHistoryTarget::PRDB_HISTORY_TRG_SPECTRUMS );
}


bool CPRDBProject::sampleGetID( int ApplianceSN, CString ProjectName, CString TransName, CString Sample1, int *pID)
{
	m_sSQL.Format( _T(PRDB_SQL_SELECT_SAMPLE_ID), replaceChar( Sample1 ), replaceChar( TransName ), replaceChar( ProjectName ), ApplianceSN );
	if ( !selectSQL() )
		return false;

	if (m_pDataSet == NULL)
	{
		assert(false);
		return false;
	}

	LumexDB::CDataPtr<ILumexDBDataSet::TColumnData> pData = m_pDataSet->GetColumn(0);
	VerifyDataPointer;
	int lVal;
	LumexDB::EnOperationResult res = pData->GetData(&lVal, sizeof(lVal));
	VerifySuccess;
	*pID = lVal;

	return addHistoryEvent( true, prdbHistoryType::PRDB_HISTORY_TYPE_READ, prdbHistoryTarget::PRDB_HISTORY_TRG_SPECTRUMS );
}

bool CPRDBProject::spectrumGet( int ApplianceSN, CString ProjectName, CString TransName, CString Sample1, bool bFull, PRSpectrum *const pS, PRSpectrumLite *const pL )
{
	m_sSQL.Format( _T(PRDB_SQL_READ_SPRM), ( bFull ? pS->SpecNum : pL->SpecNum ), replaceChar( Sample1 ), replaceChar( TransName ), replaceChar( ProjectName ), ApplianceSN );
	if ( !selectSQL() )
		return false;

	if (m_pDataSet == NULL)
	{
		assert(false);
		return false;
	}

	LumexDB::CDataPtr<ILumexDBDataSet::TColumnData> pData = m_pDataSet->GetColumn(0);
	VerifyDataPointer;
	TIMESTAMP_STRUCT date;
	LumexDB::EnOperationResult res = pData->GetData(&date, sizeof(date));
	VerifySuccess;	
	( bFull ? pS->DateCreated : pL->DateCreated ) = CTime(date.year, date.month, date.day, date.hour, date.minute, date.second);

	pData = m_pDataSet->GetColumn(1);
	VerifyDataPointer;
	bool bVal;
	res = pData->GetData(&bVal, sizeof(bVal));
	VerifySuccess;
	( bFull ? pS->IsUse : pL->IsUse ) = bVal;

	pData = m_pDataSet->GetColumn(2);
	VerifyDataPointer;
	int lVal;
	res = pData->GetData(&lVal, sizeof(lVal));
	VerifySuccess;		

	CString Path = GetPathToSpec(lVal);
	if(!IsExistFile(Path))
	{
		assert(false);
		return false;
	}

	if(bFull)
	{
		pS->Vc.clear();

		bool res = ReadSpaDataFromFile(Path, pS->Vc);
		if(!res)
		{
			assert(false);
			return false;
		}

/* ������ � ��������� ������������ ������ � ����

		m_sSQL.Format( _T(PRDB_SQL_READ_SPRMDATA), lVal );
		if ( !selectSQL() )
			return false;

		if (m_pDataSet == NULL)
		{
			assert(false);
			return false;
		}

		pS->Vc.clear();

		while ( !m_pDataSet->IsEOF() )
		{			
			pData = m_pDataSet->GetColumn(0);
			VerifyDataPointer;
			double dVal;
			res = pData->GetData(&dVal, sizeof(dVal));
			VerifySuccess;				
			pS->Vc.push_back( dVal );
			
			res = m_pDataSet->NextRow();
			VerifySuccessAndNoData;
		}

 ����� ������� ���� */

	}

	return addHistoryEvent( true, prdbHistoryType::PRDB_HISTORY_TYPE_READ, prdbHistoryTarget::PRDB_HISTORY_TRG_SPECTRUMS );
}

bool CPRDBProject::applianceAdd( const PRAppliance *const pA )
{
	m_sSQL.Format(_T(PRDB_SQL_INSERT_APPLIENCE), pA->ApplianceType, pA->ApplianceSN);

	return executeSQL();
}

bool CPRDBProject::applianceUpdate( int ApplianceSN, const PRAppliance *const pA )
{
	m_sSQL.Format(_T(PRDB_SQL_UPDATE_APPLIENCE), pA->ApplianceType, pA->ApplianceSN, ApplianceSN);

	return executeSQL();
}

bool CPRDBProject::projectAdd( int ApplianceSN, const PRAnalisisProject *const pAP )
{
	m_sSQL.Format( _T(PRDB_SQL_INSERT_APROJ), replaceChar( pAP->APName ), pAP->SST, pAP->SpRangeDown, pAP->SpRangeUp,
		pAP->SampleScanCount, pAP->SampleMeasCount, pAP->StandardScanCount, pAP->StandardMeasCount,
		floatWithPoint( pAP->CuvetteLength ), pAP->SubChannelPermit, pAP->SubChannelApodization, pAP->ZeroFilling,
		pAP->UseRefChannel, pAP->RefChannelPermit, pAP->RefChannelApodization, pAP->TempRangeDown, pAP->TempRangeUp,
		pAP->TempDiff, replaceChar( pAP->SamSortName ), pAP->SamSortDir, ApplianceSN );
	return executeSQL();
}

bool CPRDBProject::projectUpdate( int ApplianceSN, CString ProjectName, const PRAnalisisProject *const pAP )
{
	m_sSQL.Format( _T(PRDB_SQL_UPDATE_APROJ), replaceChar( pAP->APName ), pAP->SST, pAP->SpRangeDown, pAP->SpRangeUp,
		pAP->SampleScanCount, pAP->SampleMeasCount, pAP->StandardScanCount, pAP->StandardMeasCount,
		floatWithPoint( pAP->CuvetteLength ), pAP->SubChannelPermit, pAP->SubChannelApodization, pAP->ZeroFilling,
		pAP->UseRefChannel, pAP->RefChannelPermit, pAP->RefChannelApodization, pAP->TempRangeDown, pAP->TempRangeUp,
		pAP->TempDiff, replaceChar( pAP->SamSortName ), pAP->SamSortDir, replaceChar( ProjectName ),
		ApplianceSN );
	return executeSQL();
}

bool CPRDBProject::indexAdd( int ApplianceSN, CString ProjectName, const PRIndex *const pI )
{
	m_sSQL.Format(_T(PRDB_SQL_INSERT_IDX), replaceChar(pI->IdxName), replaceChar(pI->IdxUnits), pI->IdxFormat, floatWithPoint(pI->StHumidity), pI->IsUseStHumidity, replaceChar( ProjectName ), ApplianceSN );
	return executeSQL();
}

bool CPRDBProject::indexUpdate( int ApplianceSN, CString ProjectName, CString IndexName, const PRIndex *const pI )
{
	m_sSQL.Format( _T(PRDB_SQL_UPDATE_IDX), replaceChar(pI->IdxName), replaceChar(pI->IdxUnits), pI->IdxFormat, floatWithPoint(pI->StHumidity), pI->IsUseStHumidity, replaceChar( IndexName ), replaceChar( ProjectName ) ,ApplianceSN );
	return executeSQL();
}

bool CPRDBProject::transAdd(int ApplianceSN, CString ProjectName, const PRTrans *const pT)
{
	m_sSQL = _T(PRDB_SQL_GET_TRANS_ID_MAX);
	long lT = 1 + isDataExist();
	if ( lT <= 0 )
		return false;

	m_sSQL.Format(_T(PRDB_SQL_INSERT_TRANS), lT, replaceChar(pT->TransName), pT->Type, pT->MasterDevType, pT->MasterDevNumber,
		pT->DateCreated.Format(m_pszFormatCTime), pT->LowLim, pT->UpperLim, replaceChar(pT->Preproc), replaceChar(pT->FrameName),
		replaceChar(ProjectName), ApplianceSN);
	if(!executeSQL())
		return false;

	return transAddVect(lT, pT);	
}

bool CPRDBProject::transUpdate( int ApplianceSN, CString ProjectName, CString TransName, const PRTrans *const pT )
{
	m_sSQL.Format( _T(PRDB_SQL_GET_TRANS_ID), replaceChar( TransName ), replaceChar( ProjectName ), ApplianceSN );
	long lT = isDataExist();
	if(lT <= 0)
		return false;

	m_sSQL.Format( _T(PRDB_SQL_UPDATE_TRANS), replaceChar(pT->TransName), pT->Type, pT->MasterDevType, pT->MasterDevNumber,
		pT->DateCreated.Format(m_pszFormatCTime), pT->LowLim, pT->UpperLim, replaceChar(pT->Preproc), replaceChar(pT->FrameName),
		lT);
	if(!executeSQL())
		return false;

	m_sSQL.Format(_T(PRDB_SQL_DELETE_PLATE), lT);
	if(!executeSQL())
		return false;

	return transAddVect(lT, pT);	
}

bool CPRDBProject::transAddVect(long lT, const PRTrans *const pT)
{
	if(pT->Plates.size() != NULL)
	{
		for(vector<PRPlate>::const_iterator it=pT->Plates.begin(); it!=pT->Plates.end(); ++it)
		{
			m_sSQL.Format( _T(PRDB_SQL_INSERT_PLATE), lT, it->PlateNum, replaceChar(it->SamName), 
				floatWithPoint(it->R), floatWithPoint(it->T0), floatWithPoint(it->T1), floatWithPoint(it->T2),
				floatWithPoint(it->T3), floatWithPoint(it->T4));
			if ( !executeSQL() )
				return false;
		}
	}

	return true;
}

bool CPRDBProject::sampleAdd( int ApplianceSN, CString ProjectName, CString TransName, const PRSample *const pS )
{
	m_sSQL.Format( _T(PRDB_SQL_INSERT_SMPL), replaceChar( pS->Sample1 ), replaceChar( pS->Sample2 ), replaceChar( pS->Sample3 ), replaceChar( TransName ), replaceChar( ProjectName ), ApplianceSN );
	return executeSQL();
}


bool CPRDBProject::sampleUpdate( int ApplianceSN, CString ProjectName, CString TransName, CString Sample1, const PRSample *const pS )
{
	m_sSQL.Format( _T(PRDB_SQL_UPDATE_SMPL), replaceChar( pS->Sample1 ), replaceChar( pS->Sample2 ), replaceChar( pS->Sample3 ), /*replaceChar( TransName ),*/ replaceChar( Sample1 ), replaceChar( TransName ), replaceChar( ProjectName ), ApplianceSN );
	return executeSQL();
}

bool CPRDBProject::refDataAdd( int ApplianceSN, CString ProjectName, CString TransName, CString Sample1, const PRRefData *const pRF )
{
	m_sSQL.Format( _T(PRDB_SQL_INSERT_RFDT), pRF->IsDataExists, floatWithPoint( pRF->RefData ), replaceChar( pRF->IdxName ), replaceChar( Sample1 ), replaceChar( TransName ), replaceChar( ProjectName ), ApplianceSN );
	return executeSQL();
}

bool CPRDBProject::refDataUpdate( int ApplianceSN, CString ProjectName, CString TransName, CString Sample1, const PRRefData *const pRF )
{
	m_sSQL.Format( _T(PRDB_SQL_UPDATE_RFDT), pRF->IsDataExists, floatWithPoint( pRF->RefData ), replaceChar( pRF->IdxName ), replaceChar( ProjectName ), ApplianceSN, replaceChar( Sample1 ), replaceChar( TransName ), replaceChar( ProjectName ), ApplianceSN );
	return executeSQL();
}

bool CPRDBProject::spectrumAdd( int ApplianceSN, CString ProjectName, CString TransName, CString Sample1, const PRSpectrum *const pSp )
{
	m_sSQL = _T(PRDB_SQL_GET_SPRM_ID_MAX);
	long lSpID = 1 + isDataExist();
	if ( lSpID <= 0 )
		return false;

	m_sSQL.Format( _T(PRDB_SQL_INSERT_SPRM), lSpID, pSp->SpecNum, pSp->DateCreated.Format( m_pszFormatCTime ), pSp->IsUse, replaceChar( Sample1 ), replaceChar( TransName ), replaceChar( ProjectName ), ApplianceSN );
	if ( !executeSQL() )
		return false;

	bool res = CheckDirectory();
	if(res)
	{
		CString Path = GetPathToSpec(lSpID);
		if(IsExistFile(Path))
			DeleteFileByPath(Path);

		res = SaveSpaDataAtFile(Path, pSp->Vc);
	}
	if(!res)
		return false;

/* ������ � ��������� ������������ ������ � ����

	CString tmp;
	tmp.Format( _T(PRDB_SQL_INSERT_SPRMDATA), lSpID, _T("%s") );

	for ( vector<double>::const_iterator it = pSp->Vc.begin(); it != pSp->Vc.end(); it++ )
	{
		m_sSQL.Format( tmp, floatWithPoint(*it) );
		if ( !executeSQL() )
			return false;
	}

����� ������� ���� */

	return addHistoryEvent( true, prdbHistoryType::PRDB_HISTORY_TYPE_INSERT, prdbHistoryTarget::PRDB_HISTORY_TRG_SPECTRUMS );
}

bool CPRDBProject::spectrumUpdateUse( int ApplianceSN, CString ProjectName, CString TransName, CString Sample1, const PRSpectrum *const pSp )
{
	m_sSQL = _T(PRDB_SQL_GET_SPRM_ID_MAX);
	long lSpID = 1 + isDataExist();
	if ( lSpID <= 0 )
		return false;

	m_sSQL.Format( _T(PRDB_SQL_UPDATE_USE_SPRM), pSp->IsUse, pSp->SpecNum, replaceChar( Sample1 ), replaceChar( TransName ), replaceChar( ProjectName ), ApplianceSN );
	return executeSQL();
}

bool CPRDBProject::modelUpdate( int ApplianceSN, CString ProjectName, CString ModelName_OLD, const PRModel *const pM )
{
	m_sSQL.Format( _T(PRDB_SQL_GET_MDL_ID), replaceChar( ModelName_OLD ), replaceChar( ProjectName ), ApplianceSN );
	long lM = isDataExist();
	if ( lM <= 0 )
		return false;

	m_sSQL.Format( _T(PRDB_SQL_UPDATE_MDL), replaceChar( pM->ModelName ), replaceChar( pM->TransName ),
		pM->DateCreated.Format( m_pszFormatCTime ), pM->AnalisisType, pM->ModelType, pM->NumComponentQNT,
		pM->NumComponentQLT, floatWithPoint( pM->HypercubeVolume ), pM->HarmonyCount,
		floatWithPoint( pM->Nonlinearity ), floatWithPoint( pM->PredH ), floatWithPoint( pM->PredCKO ),
		pM->UseSECV, pM->SpectrumType, pM->SpRangeRealDown, pM->SpRangeRealUp, replaceChar( pM->Preproc ),
		lM );
	if ( !executeSQL() )
		return false;
	
	m_sSQL.Format( _T(PRDB_SQL_DELETE_MDL_IX), lM );
	if ( !executeSQL() )
		return false;
	

	m_sSQL.Format( _T(PRDB_SQL_DELETE_MDL_SE), lM );
	if ( !executeSQL() )
		return false;

	m_sSQL.Format( _T(PRDB_SQL_DELETE_MDL_SP), lM );
	if ( !executeSQL() )
		return false;

	return addHistoryEvent( modelAddVect( ApplianceSN, ProjectName, lM, pM ), prdbHistoryType::PRDB_HISTORY_TYPE_UPDATE, prdbHistoryTarget::PRDB_HISTORY_TRG_MODELS );
}

bool CPRDBProject::modelAddVect( int ApplianceSN, CString ProjectName, long lM, const PRModel *const pM )
{
	// ���. � �����������
	if ( pM->Indcies.size() != NULL )
	{
		for ( vector<PRModel::PRMDParam>::const_iterator it = pM->Indcies.begin(); it != pM->Indcies.end(); it++ )
		{
			m_sSQL.Format( _T(PRDB_SQL_INSERT_MDL_IX), lM, floatWithPoint( (*it).PredCKOIX ),
				floatWithPoint( (*it).CalibDown ), floatWithPoint( (*it).CalibUp ), floatWithPoint( (*it).CorrB ),
				floatWithPoint( (*it).CorrA ), replaceChar( (*it).IndexName ),	replaceChar( ProjectName ), ApplianceSN );
			if ( !executeSQL() )
				return false;
		}
	}

	// ������ ����������� ������������ �����
	if ( pM->SpDataExclude.size() != NULL )
	{
		for ( vector<int>::const_iterator it = pM->SpDataExclude.begin(); it != pM->SpDataExclude.end(); it++ )
		{
			m_sSQL.Format( _T(PRDB_SQL_INSERT_MDL_SE), lM, *it );
			if ( !executeSQL() )
				return false;
		}
	}

	// ������ ������� ��� ����������� � ���������

	m_sSQL = _T(PRDB_SQL_MAX_MDL_SP);
	long lSP = 1 + isDataExist();
	if ( lSP <= 0 )
		return false;

	if(pM->Spectra.size() != NULL )
	{
		for ( vector<PRModel::PRMDSpectrum>::const_iterator it = pM->Spectra.begin(); it != pM->Spectra.end(); it++ )
		{
			m_sSQL.Format( _T(PRDB_SQL_SELECT_SPECTRUM_ID), it->SpNum, replaceChar( it->SampleName ), replaceChar( it->TransName ), replaceChar( ProjectName ), ApplianceSN );
			long lSpecID = isDataExist();
			if ( lSpecID <= 0 )
				return false;

			m_sSQL.Format( _T(PRDB_SQL_INSERT_MDL_SP), lSP, lM, lSpecID, it->IsCalib, it->IsValid );
			if ( !executeSQL() )
				return false;

			lSP++;
		}		
	}


	return true;
}

bool CPRDBProject::modelAdd( int ApplianceSN, CString ProjectName, const PRModel *const pM )
{
	m_sSQL = _T(PRDB_SQL_GET_MDL_ID_MAX);
	long lM = 1 + isDataExist();
	if ( lM <= 0 )
		return false;

		// ������
	m_sSQL.Format( _T(PRDB_SQL_INSERT_MDL), lM, replaceChar( pM->ModelName ), replaceChar( pM->TransName ),
		pM->DateCreated.Format( m_pszFormatCTime ), pM->AnalisisType, pM->ModelType, pM->NumComponentQNT,
		pM->NumComponentQLT, floatWithPoint( pM->HypercubeVolume ), pM->HarmonyCount,
		floatWithPoint( pM->Nonlinearity ), floatWithPoint( pM->PredH ), floatWithPoint( pM->PredCKO ),
		pM->UseSECV, pM->SpectrumType, pM->SpRangeRealDown, pM->SpRangeRealUp, replaceChar( pM->Preproc ),
		replaceChar( ProjectName ), ApplianceSN );
	if ( !executeSQL() )
		return false;

	return addHistoryEvent( modelAddVect( ApplianceSN, ProjectName, lM, pM ), prdbHistoryType::PRDB_HISTORY_TYPE_INSERT, prdbHistoryTarget::PRDB_HISTORY_TRG_MODELS );	
}

bool CPRDBProject::methodAdd( int ApplianceSN, CString ProjectName, const PRMethod *const pM )
{
	m_sSQL = _T(PRDB_SQL_GET_MHD_ID_MAX);
	long lM = 1 + isDataExist();
	if ( lM <= 0 )
		return false;

		// �����
	m_sSQL.Format( _T(PRDB_SQL_INSERT_MHD), lM, replaceChar( pM->MethodName ), pM->DateCreated.Format( m_pszFormatCTime ), pM->AnalisisType, replaceChar( pM->IndexAsHum ),
		pM->TempRangeRealDown, pM->TempRangeRealUp, pM->TempDiff, replaceChar( ProjectName ), ApplianceSN );
	if ( !executeSQL() ) 
		return false;
		
	return addHistoryEvent( methodAddVect( ApplianceSN, ProjectName, lM, pM ), prdbHistoryType::PRDB_HISTORY_TYPE_INSERT, prdbHistoryTarget::PRDB_HISTORY_TRG_METHODS );
}

bool CPRDBProject::methodAddVect( int ApplianceSN, CString ProjectName, long lM, const PRMethod *const pM )
{
	// ������ ������� QLT
	if ( pM->QltModels.size() != NULL )
	{
		for ( std::map<int, CString>::const_iterator it = pM->QltModels.begin(); it != pM->QltModels.end(); it++ )
		{
			m_sSQL.Format( _T(PRDB_SQL_INSERT_MHD_MQLT), lM, it->first, replaceChar( it->second ), replaceChar( ProjectName ), ApplianceSN );
			if ( !executeSQL() )
				return false;
		}
	}

	if(pM->Formula.size() != NULL)
	{
		for(vector<PRMethod::PRMTElement>::const_iterator it=pM->Formula.begin(); it!=pM->Formula.end(); it++)
		{
			m_sSQL.Format(_T(PRDB_SQL_INSERT_MHD_QLT_FORM), lM, it->Type, it->ModelInd);
			if(!executeSQL())
				return false;
		}
	}

	// IX
	if ( pM->Index.size() != NULL )
	{
		m_sSQL = _T(PRDB_SQL_GET_MHD_MIX_ID_MAX);
		long lIX = 1 + isDataExist();
		if ( lIX <= 0 )
			return false;

		for ( vector<PRMethod::PRMTParam>::const_iterator it = pM->Index.begin(); it != pM->Index.end(); it++ )
		{
			m_sSQL.Format( _T(PRDB_SQL_INSERT_MHD_MIX), lIX, lM, replaceChar( (*it).MainModel ), floatWithPoint( (*it).Moisture ), (*it).UseStdMoisture, replaceChar( (*it).IndexName ), replaceChar( ProjectName ), ApplianceSN );
			if ( !executeSQL() )
				return false;

			// IX_IX

			for ( vector<CString>::const_iterator itM = (*it).AddModels.begin(); itM != (*it).AddModels.end(); itM++ )
			{
				m_sSQL.Format( _T(PRDB_SQL_INSERT_MHD_MIX_IX), lIX, replaceChar( *itM ), replaceChar( ProjectName ), ApplianceSN );
				if ( !executeSQL() )
					return false;
			}
			lIX++;
		}
	}
	return true;
}

bool CPRDBProject::methodUpdate( int ApplianceSN, CString ProjectName, CString MethodName_OLD, const PRMethod *const pM )
{
	m_sSQL.Format( _T(PRDB_SQL_GET_MHD_ID), replaceChar( MethodName_OLD ), replaceChar( ProjectName ), ApplianceSN );
	long lM = isDataExist();
	if ( lM <= 0 )
		return false;

	m_sSQL.Format( _T(PRDB_SQL_UPDATE_MHD), replaceChar( pM->MethodName ), pM->DateCreated.Format( m_pszFormatCTime ), pM->AnalisisType, replaceChar( pM->IndexAsHum ),
		pM->TempRangeRealDown, pM->TempRangeRealUp, pM->TempDiff, lM );
	if ( !executeSQL() )
		return false;

	m_sSQL.Format(_T(PRDB_SQL_DELETE_MHD_MQLT_ALL), lM );
	if ( !executeSQL() )
		return false;

	m_sSQL.Format(_T(PRDB_SQL_DELETE_MHD_QLT_FORM_ALL), lM);
	if(!executeSQL())
		return false;

	m_sSQL.Format( _T(PRDB_SQL_DELETE_MHD_MIX), lM );
	if ( !executeSQL() )
		return false;

	return addHistoryEvent( methodAddVect( ApplianceSN, ProjectName, lM, pM ), prdbHistoryType::PRDB_HISTORY_TYPE_UPDATE, prdbHistoryTarget::PRDB_HISTORY_TRG_METHODS );
}

bool CPRDBProject::GetApplianceSpectraID(int ApplianceSN, vector<int>& SpecIDs)
{
	CString sql;
	sql.Format(_T(PRDB_SQL_GET_DEVPROJECTS_NAME), ApplianceSN);
	ILumexDBDataSet *pDataSet;
	LumexDB::EnOperationResult res = m_pDB->ExecuteSQL(sql, &pDataSet);
	if (res != LumexDB::enSuccess || pDataSet == NULL)
	{
		assert(false);
		return false;
	}

	while(!pDataSet->IsEOF())
	{
		CString PrjName;
		LumexDB::CDataPtr<ILumexDBDataSet::TColumnData> pData = pDataSet->GetColumn(0);
		LPTSTR lpBuf = PrjName.GetBuffer(StringLen);	
		LumexDB::EnOperationResult res = pData->GetData(lpBuf, BufferLen, false);
		PrjName.ReleaseBuffer();
		VerifySuccess;
		returnChar(PrjName);

		GetProjectSpectraID(ApplianceSN, PrjName, SpecIDs);

		res = pDataSet->NextRow();
		VerifySuccessAndNoData;
	}
	pDataSet->Release();

	return true;
}

bool CPRDBProject::GetProjectSpectraID(int ApplianceSN, CString ProjectName, vector<int>& SpecIDs)
{
	CString sql;
	sql.Format(_T(PRDB_SQL_GET_PRJTRANSES_NAME), replaceChar(ProjectName), ApplianceSN);
	ILumexDBDataSet *pDataSet;
	LumexDB::EnOperationResult res = m_pDB->ExecuteSQL(sql, &pDataSet);
	if (res != LumexDB::enSuccess || pDataSet == NULL)
	{
		assert(false);
		return false;
	}

	while(!pDataSet->IsEOF())
	{
		CString TransName;
		LumexDB::CDataPtr<ILumexDBDataSet::TColumnData> pData = pDataSet->GetColumn(0);
		LPTSTR lpBuf = TransName.GetBuffer(StringLen);	
		LumexDB::EnOperationResult res = pData->GetData(lpBuf, BufferLen, false);
		TransName.ReleaseBuffer();
		VerifySuccess;
		returnChar(TransName);

		GetTransSpectraID(ApplianceSN, ProjectName, TransName, SpecIDs);

		res = pDataSet->NextRow();
		VerifySuccessAndNoData;
	}
	pDataSet->Release();

	return true;
}

bool CPRDBProject::GetTransSpectraID(int ApplianceSN, CString ProjectName, CString TransName, vector<int>& SpecIDs)
{
	CString sql;
	sql.Format(_T(PRDB_SQL_GET_TRANSSAMPLES_NAME), replaceChar(TransName), replaceChar(ProjectName), ApplianceSN);
	ILumexDBDataSet *pDataSet;
	LumexDB::EnOperationResult res = m_pDB->ExecuteSQL(sql, &pDataSet);
	if (res != LumexDB::enSuccess || pDataSet == NULL)
	{
		assert(false);
		return false;
	}

	while(!pDataSet->IsEOF())
	{
		CString SamName;
		LumexDB::CDataPtr<ILumexDBDataSet::TColumnData> pData = pDataSet->GetColumn(0);
		LPTSTR lpBuf = SamName.GetBuffer(StringLen);	
		LumexDB::EnOperationResult res = pData->GetData(lpBuf, BufferLen, false);
		SamName.ReleaseBuffer();
		VerifySuccess;
		returnChar(SamName);

		GetSampleSpectraID(ApplianceSN, ProjectName, TransName, SamName, SpecIDs);

		res = pDataSet->NextRow();
		VerifySuccessAndNoData;
	}
	pDataSet->Release();

	return true;
}

bool CPRDBProject::GetSampleSpectraID(int ApplianceSN, CString ProjectName, CString TransName, CString SampleName, vector<int>& SpecIDs)
{
	CString sql;
	sql.Format(_T(PRDB_SQL_GET_SAMSPECTRA_ID), replaceChar(SampleName), replaceChar(TransName), replaceChar(ProjectName), ApplianceSN);
	ILumexDBDataSet *pDataSet;
	LumexDB::EnOperationResult res = m_pDB->ExecuteSQL(sql, &pDataSet);
	if (res != LumexDB::enSuccess || pDataSet == NULL)
	{
		assert(false);
		return false;
	}

	while(!pDataSet->IsEOF())
	{
		LumexDB::CDataPtr<ILumexDBDataSet::TColumnData> pData = pDataSet->GetColumn(0);
		VerifyDataPointer;
		int lSpec;
		LumexDB::EnOperationResult res = pData->GetData(&lSpec, sizeof(lSpec));
		VerifySuccess;

		SpecIDs.push_back(lSpec);

		res = pDataSet->NextRow();
		VerifySuccessAndNoData;
	}
	pDataSet->Release();

	return true;
}

bool CPRDBProject::GetSampleSpectraID(int nSampleID, vector<int>& SpecIDs)
{
	CString sql;
	sql.Format(_T(PRDB_SQL_GET_SAMSPECTRA_ID2), nSampleID);
	ILumexDBDataSet *pDataSet;
	LumexDB::EnOperationResult res = m_pDB->ExecuteSQL(sql, &pDataSet);
	if (res != LumexDB::enSuccess || pDataSet == NULL)
	{
		assert(false);
		return false;
	}

	while(!pDataSet->IsEOF())
	{
		LumexDB::CDataPtr<ILumexDBDataSet::TColumnData> pData = pDataSet->GetColumn(0);
		VerifyDataPointer;
		int lSpec;
		LumexDB::EnOperationResult res = pData->GetData(&lSpec, sizeof(lSpec));
		VerifySuccess;

		SpecIDs.push_back(lSpec);

		res = pDataSet->NextRow();
		VerifySuccessAndNoData;
	}
	pDataSet->Release();

	return true;
}

bool CPRDBProject::DeleteSpectraFiles(const vector<int> *const SpecIDs)
{
	for(vector<int>::const_iterator it=SpecIDs->begin(); it!=SpecIDs->end(); ++it)
	{
		CString Path = GetPathToSpec(*it);
		if(IsExistFile(Path))
			if(!DeleteFileByPath(Path))
				return false;
	}

	return true;
}

bool CPRDBProject::CheckDirectory()
{
	CString Path = ExtractDirPath(m_sDB);
	CString Base = ExtractFileNameWithoutExt(m_sDB);
	CString Dir = _T("PRD_") + Base;
	CString Full = MakePath(Path, Dir);

	if(IsExistDirectory(Full))
		return true;

	return EnsureDirectory(Full);
}

CString CPRDBProject::GetPathToSpec(int iSp)
{
	CString Path = ExtractDirPath(m_sDB);
	CString Base = ExtractFileNameWithoutExt(m_sDB);
	CString Dir = _T("PRD_") + Base;
	CString Name;
	Name.Format(_T("%1d.dat"), iSp);
	CString Full = MakePath(Path, Dir, Name);

	return Full;
}

void CPRDBProject::createTables( vector<CString> * pV )
{
	CString tmp;
		// SYS TABLES
	tmp.Format( _T(PRDB_SQL_CREATE_TAB_PR_ROLES), prdbTypeCount::PRDBSYSRulesDes );
	pV->push_back( tmp );
	tmp.Format( _T(PRDB_SQL_CREATE_TAB_PR_USERS), prdbTypeCount::PRDBSYSUserNIC, prdbTypeCount::PRDBSYSUserFN, prdbTypeCount::PRDBSYSUserLN, prdbTypeCount::PRDBSYSUserMN );
	pV->push_back( tmp );
	pV->push_back( _T(PRDB_SQL_CREATE_TAB_PR_HIS) );

		// OTHER TABLES
	pV->push_back( _T(PRDB_SQL_CREATE_TAB_PR_APP) );
	tmp.Format( _T(PRDB_SQL_CREATE_TAB_PR_APROJ), prdbTypeCount::PRDBPDTAPTitle, prdbTypeCount::PRDBPDTIIdxName );
	pV->push_back( tmp );
	tmp.Format( _T(PRDB_SQL_CREATE_TAB_PR_IDX), prdbTypeCount::PRDBPDTIIdxName, prdbTypeCount::PRDBPDTIIdxUnits );
	pV->push_back( tmp );
	tmp.Format( _T(PRDB_SQL_CREATE_TAB_PR_TRANS), prdbTypeCount::PRDBPDTITransName, prdbTypeCount::PRDBPDTPreproc, prdbTypeCount::PRDBPDTSSample1 );
	pV->push_back( tmp );
//	tmp.Format( _T(PRDB_SQL_CREATE_TAB_PR_PLATE), prdbTypeCount::PRDBPDTSSample1 );
	tmp.Format( _T(PRDB_SQL_CREATE_TAB_PR_PLATE), prdbTypeCount::PRDBPDTSSample1 );
	pV->push_back( tmp );
	tmp.Format( _T(PRDB_SQL_CREATE_TAB_PR_SMPL), prdbTypeCount::PRDBPDTSSample1, prdbTypeCount::PRDBPDTSSample2, prdbTypeCount::PRDBPDTSSample3 );
	pV->push_back( tmp );
	pV->push_back( _T(PRDB_SQL_CREATE_TAB_PR_RFDT) );
	pV->push_back( _T(PRDB_SQL_CREATE_TAB_PR_SPRM) );
/* ������ � ��������� ������������ ������ � ����
		pV->push_back( _T(PRDB_SQL_CREATE_TAB_PR_SPRMDATA) );
����� ������� ����*/
	tmp.Format( _T(PRDB_SQL_CREATE_TAB_PR_MDL), prdbTypeCount::PRDBPDTMModelName, prdbTypeCount::PRDBPDTITransName, prdbTypeCount::PRDBPDTPreproc );
	pV->push_back( tmp );
	pV->push_back( _T(PRDB_SQL_CREATE_TAB_PR_MDL_SE) );
	pV->push_back( _T(PRDB_SQL_CREATE_TAB_PR_MDL_IX) );
	pV->push_back( _T(PRDB_SQL_CREATE_TAB_MDL_SP) );
	tmp.Format( _T(PRDB_SQL_CREATE_TAB_PR_MHD), prdbTypeCount::PRDBPDTMMethodName, prdbTypeCount::PRDBPDTIIdxName );
	pV->push_back( tmp );
	tmp.Format( _T(PRDB_SQL_CREATE_TAB_PR_MHD_MIX), prdbTypeCount::PRDBPDTMModelName );
	pV->push_back(tmp);
	pV->push_back( _T(PRDB_SQL_CREATE_TAB_PR_MHD_MIX_IX) );
	pV->push_back( _T(PRDB_SQL_CREATE_TAB_PR_MHD_MQLT) );
	pV->push_back( _T(PRDB_SQL_CREATE_TAB_PR_MHD_QLT_FORM) );
}

long CPRDBProject::getCurrentVersion()
{
	return PRDB_CURRENT_VERSION;
}
CString CPRDBProject::getCurrentDBName()
{
	return _T("PARSEL_PROJECT_DATABASE_LUMEX_NIOKR");
}
