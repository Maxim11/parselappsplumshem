#include "stdafx.h"

#include "PRDBMethod_PRO.h"

#define VerifySuccess if (res != LumexDB::enSuccess) { assert(false); return false; }
#define VerifySuccessAndNoData if (res != LumexDB::enSuccess && res != LumexDB::enNoData) { assert(false); return false; }
#define VerifyDataPointer if (pData == NULL) {	assert(false); return false; }
#define StringLen (int)(pData->m_Size / sizeof(TCHAR) + 1)
#define BufferLen (pData->m_Size + sizeof(TCHAR))

CPRDBMethodQntPRO::CPRDBMethodQntPRO() : CLDatabase()
{
}

CPRDBMethodQntPRO::~CPRDBMethodQntPRO()
{
}

bool CPRDBMethodQntPRO::SetMtdModelCorr(CString MethodName, CString ParamName, CString ModelName,
								  double CorrB, double CorrA)
{
	// �������� ������ ������ � ���� �� �����
	m_sSQL.Format(_T(MTDDB_SQL_GET_MTD_ID), replaceChar(MethodName));
	long lMtd = isDataExists();
	if(lMtd <= 0)
		return false;

	// �������� ������ ���������� ������ � ���� �� �����
	m_sSQL.Format(_T(MTDDB_SQL_GET_MTDPARAM_ID), lMtd, replaceChar(ParamName));
	long lMtdPar = isDataExists();
	if(lMtdPar <= 0)
		return false;

	// �������� ������ ������ ������ � ���� �� �����
	m_sSQL.Format(_T(MTDDB_SQL_GET_MTDMODEL_ID), lMtd, replaceChar(ModelName));
	long lMtdMod = isDataExists();
	if(lMtdMod <= 0)
		return false;

	// ������������� ����� ������������ ���������
	m_sSQL.Format(_T(MTDDB_SQL_SET_CORR_PARMODEL), floatWithPoint(CorrB), floatWithPoint(CorrA), lMtdPar, lMtdMod);
	if(!executeSQL())
		return false;

	return true;
}

bool CPRDBMethodQntPRO::MethodsRead()
{
	m_sSQL.Format(_T(MTDDB_SQL_READ_MTD));
	return selectSQL();
}

bool CPRDBMethodQntPRO::MethodGet(PRMethodQnt *const pMtd)
{
	if(m_pDataSet == NULL)
	{
		assert(false);
		return false;
	}

	// ������ ������ � ������

	LumexDB::CDataPtr<ILumexDBDataSet::TColumnData> pData = m_pDataSet->GetColumn(0);
	VerifyDataPointer;	
	long lMtd;
	LumexDB::EnOperationResult res = pData->GetData(&lMtd, sizeof(lMtd));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(1);
	VerifyDataPointer;	
	LPTSTR lpBuf = pMtd->MethodName.GetBuffer(StringLen);	
	res = pData->GetData(lpBuf, BufferLen, false);
	pMtd->MethodName.ReleaseBuffer();
	VerifySuccess;
	returnChar(pMtd->MethodName);

	pData = m_pDataSet->GetColumn(2);
	VerifyDataPointer;
	TIMESTAMP_STRUCT date;
	res = pData->GetData(&date, sizeof(date));
	VerifySuccess;		
	pMtd->DateCreated = CTime(date.year, date.month, date.day, date.hour, date.minute, date.second);

	pData = m_pDataSet->GetColumn(3);
	VerifyDataPointer;	
	lpBuf = pMtd->MoisParam.GetBuffer(StringLen);	
	res = pData->GetData(lpBuf, BufferLen, false);
	pMtd->MoisParam.ReleaseBuffer();
	VerifySuccess;
	returnChar(pMtd->MoisParam);

	pData = m_pDataSet->GetColumn(4);
	VerifyDataPointer;		
	res = pData->GetData(&pMtd->DevNum, sizeof(pMtd->DevNum));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(5);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->DevType, sizeof(pMtd->DevType));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(6);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->SpecRangeLow, sizeof(pMtd->SpecRangeLow));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(7);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->SpecRangeUpper, sizeof(pMtd->SpecRangeUpper));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(8);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->ResSub, sizeof(pMtd->ResSub));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(9);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->ApodSub, sizeof(pMtd->ApodSub));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(10);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->ZeroFill, sizeof(pMtd->ZeroFill));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(11);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->UseRef, sizeof(pMtd->UseRef));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(12);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->ResRef, sizeof(pMtd->ResRef));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(13);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->ApodRef, sizeof(pMtd->ApodRef));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(14);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->SmplScan, sizeof(pMtd->SmplScan));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(15);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->SmplMeas, sizeof(pMtd->SmplMeas));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(16);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->StdScan, sizeof(pMtd->StdScan));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(17);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->StdMeas, sizeof(pMtd->StdMeas));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(18);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->Xtransform, sizeof(pMtd->Xtransform));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(19);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->UseBath, sizeof(pMtd->UseBath));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(20);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->BathLength, sizeof(pMtd->BathLength));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(21);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->IsSST, sizeof(pMtd->IsSST));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(22);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->TempLow, sizeof(pMtd->TempLow));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(23);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->TempUpper, sizeof(pMtd->TempUpper));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(24);
	VerifyDataPointer;		
	res = pData->GetData(&pMtd->TempDiff, sizeof(pMtd->TempDiff));
	VerifySuccess;

	CString sql;
	ILumexDBDataSet *pDataSet;

	// ������ ������ � ������� ������
	{
		sql.Format(_T(MTDDB_SQL_READ_MTDMODEL), lMtd);
		
		LumexDB::EnOperationResult res = m_pDB->ExecuteSQL(sql, &pDataSet);
		if (res != LumexDB::enSuccess || pDataSet == NULL)
		{
			assert(false);
			return false;
		}

		pMtd->MtdParams.clear();

		while(!pDataSet->IsEOF())
		{
			PRMtdModel newMtdMod;

			int lMtdMod;
			pData = pDataSet->GetColumn(0);
			VerifyDataPointer;	
			res = pData->GetData(&lMtdMod, sizeof(lMtdMod));
			VerifySuccess;

			pData = pDataSet->GetColumn(1);
			VerifyDataPointer;	
			lpBuf = newMtdMod.ModelName.GetBuffer(StringLen);	
			res = pData->GetData(lpBuf, BufferLen, false);
			newMtdMod.ModelName.ReleaseBuffer();
			VerifySuccess;
			returnChar(newMtdMod.ModelName);

			pData = pDataSet->GetColumn(2);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.ModelType, sizeof(newMtdMod.ModelType));
			VerifySuccess;

			pData = pDataSet->GetColumn(3);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.SpecRangeLow, sizeof(newMtdMod.SpecRangeLow));
			VerifySuccess;

			pData = pDataSet->GetColumn(4);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.SpecRangeUpper, sizeof(newMtdMod.SpecRangeUpper));
			VerifySuccess;

			pData = pDataSet->GetColumn(5);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.FreqMin, sizeof(newMtdMod.FreqMin));
			VerifySuccess;

			pData = pDataSet->GetColumn(6);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.FreqStep, sizeof(newMtdMod.FreqStep));
			VerifySuccess;

			pData = pDataSet->GetColumn(7);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.NFreq, sizeof(newMtdMod.NFreq));
			VerifySuccess;

			pData = pDataSet->GetColumn(8);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.SpType, sizeof(newMtdMod.SpType));
			VerifySuccess;

			pData = pDataSet->GetColumn(9);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.NSpec, sizeof(newMtdMod.NSpec));
			VerifySuccess;

			pData = pDataSet->GetColumn(10);
			VerifyDataPointer;	
			lpBuf = newMtdMod.Preproc.GetBuffer(StringLen);	
			res = pData->GetData(lpBuf, BufferLen, false);
			newMtdMod.Preproc.ReleaseBuffer();
			VerifySuccess;
			returnChar(newMtdMod.Preproc);

			pData = pDataSet->GetColumn(11);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.MaxMah, sizeof(newMtdMod.MaxMah));
			VerifySuccess;

			pData = pDataSet->GetColumn(12);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.MeanMah, sizeof(newMtdMod.MeanMah));
			VerifySuccess;

			pData = pDataSet->GetColumn(13);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.IsTrans, sizeof(newMtdMod.IsTrans));
			VerifySuccess;

			pData = pDataSet->GetColumn(14);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.FreqMinTrans, sizeof(newMtdMod.FreqMinTrans));
			VerifySuccess;

			pData = pDataSet->GetColumn(15);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.FreqStepTrans, sizeof(newMtdMod.FreqStepTrans));
			VerifySuccess;

			pData = pDataSet->GetColumn(16);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.NFreqTrans, sizeof(newMtdMod.NFreqTrans));
			VerifySuccess;

			pData = pDataSet->GetColumn(17);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.SpTypeTrans, sizeof(newMtdMod.SpTypeTrans));
			VerifySuccess;

			pData = pDataSet->GetColumn(18);
			VerifyDataPointer;	
			lpBuf = newMtdMod.PreprocTrans.GetBuffer(StringLen);	
			res = pData->GetData(lpBuf, BufferLen, false);
			newMtdMod.PreprocTrans.ReleaseBuffer();
			VerifySuccess;
			returnChar(newMtdMod.PreprocTrans);

			pData = pDataSet->GetColumn(19);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.T0, sizeof(newMtdMod.T0));
			VerifySuccess;

			pData = pDataSet->GetColumn(20);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.FMax, sizeof(newMtdMod.FMax));
			VerifySuccess;

			// ������ ������ ����������� ������������ �����

			CString sql2;
			ILumexDBDataSet *pDataSet2;
			{
				sql2.Format(_T(MTDDB_SQL_READ_EXCPNT), lMtdMod);
				
				LumexDB::EnOperationResult res = m_pDB->ExecuteSQL(sql2, &pDataSet2);
				if(res != LumexDB::enSuccess || pDataSet2 == NULL)
				{
					assert(false);
					return false;
				}

				newMtdMod.ExcPoints.clear();

				while(!pDataSet2->IsEOF())
				{
					int nData;
					pData = pDataSet2->GetColumn(0);
					VerifyDataPointer;	
					res = pData->GetData(&nData, sizeof(nData));
					VerifySuccess;
					
					newMtdMod.ExcPoints.push_back(nData);
					
					res = pDataSet2->NextRow();
					VerifySuccessAndNoData;
				}
				pDataSet2->Release();
			}

			// ������ ������� ������� (���, ��� ��� ����)

			CString sql3;
			ILumexDBDataSet *pDataSet3;
			{
				sql3.Format(_T(MTDDB_SQL_READ_MEANSPEC), lMtdMod);
				
				LumexDB::EnOperationResult res = m_pDB->ExecuteSQL(sql3, &pDataSet3);
				if(res != LumexDB::enSuccess || pDataSet3 == NULL)
				{
					assert(false);
					return false;
				}

				while(!pDataSet3->IsEOF())
				{
					int lMeanSpec;
					pData = pDataSet3->GetColumn(0);
					VerifyDataPointer;	
					res = pData->GetData(&lMeanSpec, sizeof(lMeanSpec));
					VerifySuccess;

					int Type;
					pData = pDataSet3->GetColumn(1);
					VerifyDataPointer;	
					res = pData->GetData(&Type, sizeof(Type));
					VerifySuccess;
					
					// ������ ������� ������

					CString sql4;
					ILumexDBDataSet *pDataSet4;
					{
						sql4.Format(_T(MTDDB_SQL_READ_MEANSPEC_DATA), lMeanSpec);
						
						LumexDB::EnOperationResult res = m_pDB->ExecuteSQL(sql4, &pDataSet4);
						if(res != LumexDB::enSuccess || pDataSet4 == NULL)
						{
							assert(false);
							return false;
						}

						if(Type != MTDDBVectorTypeCalibrQlt && Type != MTDDBVectorTypeFactors && Type != MTDDBVectorTypeScores)
						{
							while(!pDataSet4->IsEOF())
							{
								double fData;
								pData = pDataSet4->GetColumn(0);
								VerifyDataPointer;	
								res = pData->GetData(&fData, sizeof(fData));
								VerifySuccess;

								switch(Type)
								{
								case MTDDBVectorTypeMean:  newMtdMod.MeanSpec.push_back(fData);  break;
								case MTDDBVectorTypeMeanMSC:  newMtdMod.MeanSpecMSC.push_back(fData);  break;
								case MTDDBVectorTypeMeanStd:  newMtdMod.MeanSpecStd.push_back(fData);  break;
								case MTDDBVectorTypeMeanTrans:  newMtdMod.MeanSpecTrans.push_back(fData);  break;
								case MTDDBVectorTypeMeanMSCTrans:  newMtdMod.MeanSpecMSCTrans.push_back(fData);  break;
								case MTDDBVectorTypeMeanStdTrans:  newMtdMod.MeanSpecStdTrans.push_back(fData);  break;
								case MTDDBVectorTypeMeanHQO:  newMtdMod.MeanSpecHQO.push_back(fData);  break;
								case MTDDBVectorTypeMeanMSCHQO:  newMtdMod.MeanSpecMSCHQO.push_back(fData);  break;
								case MTDDBVectorTypeMeanStdHQO:  newMtdMod.MeanSpecStdHQO.push_back(fData);  break;
								}
								
								res = pDataSet4->NextRow();
								VerifySuccessAndNoData;
							}
						}
						else if(Type == MTDDBVectorTypeFactors)
						{
							int count = 0;
							int Nf = newMtdMod.NFreq - int(newMtdMod.ExcPoints.size());
							vector<double> NewVect;
							while(!pDataSet4->IsEOF())
							{
								double fData;
								pData = pDataSet4->GetColumn(0);
								VerifyDataPointer;	
								res = pData->GetData(&fData, sizeof(fData));
								VerifySuccess;

								NewVect.push_back(fData);

								count++;
								if(count == Nf)
								{
									newMtdMod.Factors.push_back(NewVect);
									NewVect.clear();
									count = 0;
								}

								res = pDataSet4->NextRow();
								VerifySuccessAndNoData;
							}
						}
						else if(Type == MTDDBVectorTypeScores)
						{
							int count = 0;
							int Ns = newMtdMod.NSpec;
							vector<double> NewVect;
							while(!pDataSet4->IsEOF())
							{
								double fData;
								pData = pDataSet4->GetColumn(0);
								VerifyDataPointer;	
								res = pData->GetData(&fData, sizeof(fData));
								VerifySuccess;

								NewVect.push_back(fData);

								count++;
								if(count == Ns)
								{
									newMtdMod.Scores.push_back(NewVect);
									NewVect.clear();
									count = 0;
								}

								res = pDataSet4->NextRow();
								VerifySuccessAndNoData;
							}
						}

						pDataSet4->Release();
					}

					res = pDataSet3->NextRow();
					VerifySuccessAndNoData;
				}
				pDataSet3->Release();
			}


			pMtd->MtdModels.push_back(newMtdMod);
			
			res = pDataSet->NextRow();
			VerifySuccessAndNoData;
		}
		pDataSet->Release();
	}

	// ������ ������ � ����������� ������
	{
		sql.Format(_T(MTDDB_SQL_READ_MTDPARAM), lMtd);
		
		LumexDB::EnOperationResult res = m_pDB->ExecuteSQL(sql, &pDataSet);
		if (res != LumexDB::enSuccess || pDataSet == NULL)
		{
			assert(false);
			return false;
		}

		pMtd->MtdParams.clear();

		while(!pDataSet->IsEOF())
		{
			PRMtdParam newMtdPar;

			int lMtdPar;
			pData = pDataSet->GetColumn(0);
			VerifyDataPointer;	
			res = pData->GetData(&lMtdPar, sizeof(lMtdPar));
			VerifySuccess;

			pData = pDataSet->GetColumn(1);
			VerifyDataPointer;	
			lpBuf = newMtdPar.ParamName.GetBuffer(StringLen);	
			res = pData->GetData(lpBuf, BufferLen, false);
			newMtdPar.ParamName.ReleaseBuffer();
			VerifySuccess;
			returnChar(newMtdPar.ParamName);

			pData = pDataSet->GetColumn(2);
			VerifyDataPointer;	
			lpBuf = newMtdPar.Unit.GetBuffer(StringLen);	
			res = pData->GetData(lpBuf, BufferLen, false);
			newMtdPar.Unit.ReleaseBuffer();
			VerifySuccess;
			returnChar(newMtdPar.Unit);

			pData = pDataSet->GetColumn(3);
			VerifyDataPointer;	
			lpBuf = newMtdPar.Format.GetBuffer(StringLen);	
			res = pData->GetData(lpBuf, BufferLen, false);
			newMtdPar.Format.ReleaseBuffer();
			VerifySuccess;
			returnChar(newMtdPar.Format);

			pData = pDataSet->GetColumn(4);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdPar.UseDefModel, sizeof(newMtdPar.UseDefModel));
			VerifySuccess;

			pData = pDataSet->GetColumn(5);
			VerifyDataPointer;	
			lpBuf = newMtdPar.DefModel.GetBuffer(StringLen);	
			res = pData->GetData(lpBuf, BufferLen, false);
			newMtdPar.DefModel.ReleaseBuffer();
			VerifySuccess;
			returnChar(newMtdPar.DefModel);

			pData = pDataSet->GetColumn(6);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdPar.UseRefMois, sizeof(newMtdPar.UseRefMois));
			VerifySuccess;

			pData = pDataSet->GetColumn(7);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdPar.RefMois, sizeof(newMtdPar.RefMois));
			VerifySuccess;

			pData = pDataSet->GetColumn(8);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdPar.UseStdMois, sizeof(newMtdPar.UseStdMois));
			VerifySuccess;

			pData = pDataSet->GetColumn(9);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdPar.StdMois, sizeof(newMtdPar.StdMois));
			VerifySuccess;

			// ������ ������ � ���������� ���������� ������ ������

			CString sql2;
			ILumexDBDataSet *pDataSet2;
			{
				sql2.Format(_T(MTDDB_SQL_READ_PARMODEL), lMtdPar);
				
				LumexDB::EnOperationResult res = m_pDB->ExecuteSQL(sql2, &pDataSet2);
				if(res != LumexDB::enSuccess || pDataSet2 == NULL)
				{
					assert(false);
					return false;
				}

				while(!pDataSet2->IsEOF())
				{
					PRParModel newParMod;

					int lParMod;
					pData = pDataSet2->GetColumn(0);
					VerifyDataPointer;	
					res = pData->GetData(&lParMod, sizeof(lParMod));
					VerifySuccess;

					pData = pDataSet2->GetColumn(1);
					VerifyDataPointer;	
					lpBuf = newParMod.ModelName.GetBuffer(StringLen);	
					res = pData->GetData(lpBuf, BufferLen, false);
					newParMod.ModelName.ReleaseBuffer();
					VerifySuccess;
					returnChar(newParMod.ModelName);

					pData = pDataSet2->GetColumn(2);
					VerifyDataPointer;		
					res = pData->GetData(&newParMod.LowCalibRange, sizeof(newParMod.LowCalibRange));
					VerifySuccess;

					pData = pDataSet2->GetColumn(3);
					VerifyDataPointer;		
					res = pData->GetData(&newParMod.UpperCalibRange, sizeof(newParMod.UpperCalibRange));
					VerifySuccess;

					pData = pDataSet2->GetColumn(4);
					VerifyDataPointer;		
					res = pData->GetData(&newParMod.MaxSKO, sizeof(newParMod.MaxSKO));
					VerifySuccess;

					pData = pDataSet2->GetColumn(5);
					VerifyDataPointer;		
					res = pData->GetData(&newParMod.CorrB, sizeof(newParMod.CorrB));
					VerifySuccess;

					pData = pDataSet2->GetColumn(6);
					VerifyDataPointer;		
					res = pData->GetData(&newParMod.CorrA, sizeof(newParMod.CorrA));
					VerifySuccess;

					pData = pDataSet2->GetColumn(7);
					VerifyDataPointer;		
					res = pData->GetData(&newParMod.SEC, sizeof(newParMod.SEC));
					VerifySuccess;

					pData = pDataSet2->GetColumn(8);
					VerifyDataPointer;		
					res = pData->GetData(&newParMod.MeanComp, sizeof(newParMod.MeanComp));
					VerifySuccess;

					pData = pDataSet2->GetColumn(9);
					VerifyDataPointer;		
					res = pData->GetData(&newParMod.MeanCompTrans, sizeof(newParMod.MeanCompTrans));
					VerifySuccess;

					pData = pDataSet2->GetColumn(10);
					VerifyDataPointer;		
					res = pData->GetData(&newParMod.MeanCompStd, sizeof(newParMod.MeanCompStd));
					VerifySuccess;

					// ������ ������� �� �������������� ������� (���, ��� ��� ����)

					CString sql3;
					ILumexDBDataSet *pDataSet3;
					{
						sql3.Format(_T(MTDDB_SQL_READ_CALIBVECTOR), lParMod);
						
						LumexDB::EnOperationResult res = m_pDB->ExecuteSQL(sql3, &pDataSet3);
						if(res != LumexDB::enSuccess || pDataSet3 == NULL)
						{
							assert(false);
							return false;
						}

						VDbl Types;
						while(!pDataSet3->IsEOF())
						{
							int lCalibVect;
							pData = pDataSet3->GetColumn(0);
							VerifyDataPointer;	
							res = pData->GetData(&lCalibVect, sizeof(lCalibVect));
							VerifySuccess;

							int Type;
							pData = pDataSet3->GetColumn(1);
							VerifyDataPointer;	
							res = pData->GetData(&Type, sizeof(Type));
							VerifySuccess;
							
							// ������ ������ �� �������������� �������

							CString sql4;
							ILumexDBDataSet *pDataSet4;
							{
								sql4.Format(_T(MTDDB_SQL_READ_CALIBVECTOR_DATA), lCalibVect);
								
								LumexDB::EnOperationResult res = m_pDB->ExecuteSQL(sql4, &pDataSet4);
								if(res != LumexDB::enSuccess || pDataSet4 == NULL)
								{
									assert(false);
									return false;
								}

								while(!pDataSet4->IsEOF())
								{
									double fData;
									pData = pDataSet4->GetColumn(0);
									VerifyDataPointer;	
									res = pData->GetData(&fData, sizeof(fData));
									VerifySuccess;

									switch(Type)
									{
									case MTDDBMatrixTypeCalibrLin:  newParMod.CalibrLin.push_back(fData);  break;
									case MTDDBMatrixTypeCalibrNonLin:  newParMod.CalibrNonLin.push_back(fData);  break;
									}
									
									res = pDataSet4->NextRow();
									VerifySuccessAndNoData;
								}
								pDataSet4->Release();
							}
							
							res = pDataSet3->NextRow();
							VerifySuccessAndNoData;
						}
						pDataSet3->Release();
					}

					newMtdPar.ParModels.push_back(newParMod);
					
					res = pDataSet2->NextRow();
					VerifySuccessAndNoData;
				}
				pDataSet2->Release();
			}

			pMtd->MtdParams.push_back(newMtdPar);

			res = pDataSet->NextRow();
			VerifySuccessAndNoData;
		}
		pDataSet->Release();
	}

	return true;
}

long CPRDBMethodQntPRO::getCurrentVersion()
{
	return PRDB_METHOD_QNT_CURRENT_VERSION;
}

CString CPRDBMethodQntPRO::getCurrentDBName()
{
	return _T("METHOD_QNT_DATABASE_LUMEX_NIOKR");
}



CPRDBMethodQltPRO::CPRDBMethodQltPRO() : CLDatabase()
{
}

CPRDBMethodQltPRO::~CPRDBMethodQltPRO()
{
}

bool CPRDBMethodQltPRO::MethodsRead()
{
	m_sSQL.Format(_T(MTDDB_SQL_READ_MTD_QLT));
	return selectSQL();
}

bool CPRDBMethodQltPRO::MethodGet(PRMethodQlt *const pMtd)
{
	if(m_pDataSet == NULL)
	{
		assert(false);
		return false;
	}

	LumexDB::CDataPtr<ILumexDBDataSet::TColumnData> pData = m_pDataSet->GetColumn(0);
	VerifyDataPointer;	
	long lMtd;
	LumexDB::EnOperationResult res = pData->GetData(&lMtd, sizeof(lMtd));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(1);
	VerifyDataPointer;	
	LPTSTR lpBuf = pMtd->MethodName.GetBuffer(StringLen);	
	res = pData->GetData(lpBuf, BufferLen, false);
	pMtd->MethodName.ReleaseBuffer();
	VerifySuccess;
	returnChar(pMtd->MethodName);

	pData = m_pDataSet->GetColumn(2);
	VerifyDataPointer;
	TIMESTAMP_STRUCT date;
	res = pData->GetData(&date, sizeof(date));
	VerifySuccess;		
	pMtd->DateCreated = CTime(date.year, date.month, date.day, date.hour, date.minute, date.second);

	pData = m_pDataSet->GetColumn(3);
	VerifyDataPointer;		
	res = pData->GetData(&pMtd->DevNum, sizeof(pMtd->DevNum));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(4);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->DevType, sizeof(pMtd->DevType));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(5);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->SpecRangeLow, sizeof(pMtd->SpecRangeLow));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(6);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->SpecRangeUpper, sizeof(pMtd->SpecRangeUpper));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(7);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->ResSub, sizeof(pMtd->ResSub));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(8);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->ApodSub, sizeof(pMtd->ApodSub));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(9);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->ZeroFill, sizeof(pMtd->ZeroFill));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(10);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->UseRef, sizeof(pMtd->UseRef));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(11);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->ResRef, sizeof(pMtd->ResRef));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(12);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->ApodRef, sizeof(pMtd->ApodRef));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(13);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->SmplScan, sizeof(pMtd->SmplScan));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(14);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->SmplMeas, sizeof(pMtd->SmplMeas));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(15);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->StdScan, sizeof(pMtd->StdScan));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(16);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->StdMeas, sizeof(pMtd->StdMeas));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(17);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->Xtransform, sizeof(pMtd->Xtransform));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(18);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->UseBath, sizeof(pMtd->UseBath));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(19);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->BathLength, sizeof(pMtd->BathLength));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(20);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->TempLow, sizeof(pMtd->TempLow));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(21);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->TempUpper, sizeof(pMtd->TempUpper));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(22);
	VerifyDataPointer;		
	res = pData->GetData(&pMtd->TempDiff, sizeof(pMtd->TempDiff));
	VerifySuccess;

	CString sql;
	ILumexDBDataSet *pDataSet;

	// ��������� ������� ������
	{
		sql.Format(_T(MTDDB_SQL_READ_MTD_QLTMODEL), lMtd);
		
		LumexDB::EnOperationResult res = m_pDB->ExecuteSQL(sql, &pDataSet);
		if (res != LumexDB::enSuccess || pDataSet == NULL)
		{
			assert(false);
			return false;
		}

		pMtd->MtdQltModels.clear();

		while(!pDataSet->IsEOF())
		{
			PRMtdQltModel newMtdMod;

			int lMtdMod;
			pData = pDataSet->GetColumn(0);
			VerifyDataPointer;	
			res = pData->GetData(&lMtdMod, sizeof(lMtdMod));
			VerifySuccess;

			pData = pDataSet->GetColumn(1);
			VerifyDataPointer;	
			lpBuf = newMtdMod.ModelName.GetBuffer(StringLen);	
			res = pData->GetData(lpBuf, BufferLen, false);
			newMtdMod.ModelName.ReleaseBuffer();
			VerifySuccess;
			returnChar(newMtdMod.ModelName);

			pData = pDataSet->GetColumn(2);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.ModelInd, sizeof(newMtdMod.ModelInd));
			VerifySuccess;

			pData = pDataSet->GetColumn(3);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.SpecRangeLow, sizeof(newMtdMod.SpecRangeLow));
			VerifySuccess;

			pData = pDataSet->GetColumn(4);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.SpecRangeUpper, sizeof(newMtdMod.SpecRangeUpper));
			VerifySuccess;

			pData = pDataSet->GetColumn(5);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.FreqMin, sizeof(newMtdMod.FreqMin));
			VerifySuccess;

			pData = pDataSet->GetColumn(6);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.FreqStep, sizeof(newMtdMod.FreqStep));
			VerifySuccess;

			pData = pDataSet->GetColumn(7);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.NFreq, sizeof(newMtdMod.NFreq));
			VerifySuccess;

			pData = pDataSet->GetColumn(8);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.SpType, sizeof(newMtdMod.SpType));
			VerifySuccess;

			pData = pDataSet->GetColumn(9);
			VerifyDataPointer;	
			lpBuf = newMtdMod.Preproc.GetBuffer(StringLen);	
			res = pData->GetData(lpBuf, BufferLen, false);
			newMtdMod.Preproc.ReleaseBuffer();
			VerifySuccess;
			returnChar(newMtdMod.Preproc);

			pData = pDataSet->GetColumn(10);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.MaxMah, sizeof(newMtdMod.MaxMah));
			VerifySuccess;

			pData = pDataSet->GetColumn(11);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.MaxSKO, sizeof(newMtdMod.MaxSKO));
			VerifySuccess;

			pData = pDataSet->GetColumn(12);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.MeanMah, sizeof(newMtdMod.MeanMah));
			VerifySuccess;

			pData = pDataSet->GetColumn(13);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.IsTrans, sizeof(newMtdMod.IsTrans));
			VerifySuccess;

			pData = pDataSet->GetColumn(14);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.FreqMinTrans, sizeof(newMtdMod.FreqMinTrans));
			VerifySuccess;

			pData = pDataSet->GetColumn(15);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.FreqStepTrans, sizeof(newMtdMod.FreqStepTrans));
			VerifySuccess;

			pData = pDataSet->GetColumn(16);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.NFreqTrans, sizeof(newMtdMod.NFreqTrans));
			VerifySuccess;

			pData = pDataSet->GetColumn(17);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.SpTypeTrans, sizeof(newMtdMod.SpTypeTrans));
			VerifySuccess;

			pData = pDataSet->GetColumn(18);
			VerifyDataPointer;	
			lpBuf = newMtdMod.PreprocTrans.GetBuffer(StringLen);	
			res = pData->GetData(lpBuf, BufferLen, false);
			newMtdMod.PreprocTrans.ReleaseBuffer();
			VerifySuccess;
			returnChar(newMtdMod.PreprocTrans);

			// ��������� ������ ����������� ������������ �����

			CString sql2;
			ILumexDBDataSet *pDataSet2;
			{
				sql2.Format(_T(MTDDB_SQL_READ_QLT_EXCPNT), lMtdMod);
				
				LumexDB::EnOperationResult res = m_pDB->ExecuteSQL(sql2, &pDataSet2);
				if(res != LumexDB::enSuccess || pDataSet2 == NULL)
				{
					assert(false);
					return false;
				}

				newMtdMod.ExcPoints.clear();

				while(!pDataSet2->IsEOF())
				{
					int nData;
					pData = pDataSet2->GetColumn(0);
					VerifyDataPointer;	
					res = pData->GetData(&nData, sizeof(nData));
					VerifySuccess;
					
					newMtdMod.ExcPoints.push_back(nData);
					
					res = pDataSet2->NextRow();
					VerifySuccessAndNoData;
				}
				pDataSet2->Release();
			}

			// ��������� ������� �������� � �������������� �������
			CString sql3;
			ILumexDBDataSet *pDataSet3;
			{
				sql3.Format(_T(MTDDB_SQL_READ_QLT_MEANSPEC), lMtdMod);
				
				LumexDB::EnOperationResult res = m_pDB->ExecuteSQL(sql3, &pDataSet3);
				if(res != LumexDB::enSuccess || pDataSet3 == NULL)
				{
					assert(false);
					return false;
				}

				while(!pDataSet3->IsEOF())
				{
					int lMeanSpec;
					pData = pDataSet3->GetColumn(0);
					VerifyDataPointer;	
					res = pData->GetData(&lMeanSpec, sizeof(lMeanSpec));
					VerifySuccess;

					int Type;
					pData = pDataSet3->GetColumn(1);
					VerifyDataPointer;	
					res = pData->GetData(&Type, sizeof(Type));
					VerifySuccess;
					
					// ��������� ������ �� ������� �������� � �������������� �������

					CString sql4;
					ILumexDBDataSet *pDataSet4;
					{
						sql4.Format(_T(MTDDB_SQL_READ_QLT_MEANSPEC_DATA), lMeanSpec);
						
						LumexDB::EnOperationResult res = m_pDB->ExecuteSQL(sql4, &pDataSet4);
						if(res != LumexDB::enSuccess || pDataSet4 == NULL)
						{
							assert(false);
							return false;
						}

						if(Type != MTDDBVectorTypeCalibrQlt && Type != MTDDBVectorTypeFactors)
						{
							while(!pDataSet4->IsEOF())
							{
								double fData;
								pData = pDataSet4->GetColumn(0);
								VerifyDataPointer;	
								res = pData->GetData(&fData, sizeof(fData));
								VerifySuccess;

								switch(Type)
								{
								case MTDDBVectorTypeMean:  newMtdMod.MeanSpec.push_back(fData);  break;
								case MTDDBVectorTypeMeanMSC:  newMtdMod.MeanSpecMSC.push_back(fData);  break;
								case MTDDBVectorTypeMeanStd:  newMtdMod.MeanSpecStd.push_back(fData);  break;
								case MTDDBVectorTypeMeanTrans:  newMtdMod.MeanSpecTrans.push_back(fData);  break;
								case MTDDBVectorTypeMeanMSCTrans:  newMtdMod.MeanSpecMSCTrans.push_back(fData);  break;
								case MTDDBVectorTypeMeanStdTrans:  newMtdMod.MeanSpecStdTrans.push_back(fData);  break;
								}
								
								res = pDataSet4->NextRow();
								VerifySuccessAndNoData;
							}
						}
						else if(Type != MTDDBVectorTypeFactors)
						{
							int count = 0;
							int Nf = newMtdMod.NFreq - int(newMtdMod.ExcPoints.size());
							vector<double> NewVect;
							while(!pDataSet4->IsEOF())
							{
								double fData;
								pData = pDataSet4->GetColumn(0);
								VerifyDataPointer;	
								res = pData->GetData(&fData, sizeof(fData));
								VerifySuccess;

								NewVect.push_back(fData);

								count++;
								if(count == Nf)
								{
									newMtdMod.Factors.push_back(NewVect);
									NewVect.clear();
									count = 0;
								}

								res = pDataSet4->NextRow();
								VerifySuccessAndNoData;
							}
						}

						pDataSet4->Release();
					}

					res = pDataSet3->NextRow();
					VerifySuccessAndNoData;
				}
				pDataSet3->Release();
			}

			pMtd->MtdQltModels.push_back(newMtdMod);
			
			res = pDataSet->NextRow();
			VerifySuccessAndNoData;
		}
		pDataSet->Release();
	}

	// ��������� ������ ������������� �������

	{
		sql.Format(_T(MTDDB_SQL_READ_QLT_FORM), lMtd);

		ILumexDBDataSet *pDataSet;
		LumexDB::EnOperationResult res = m_pDB->ExecuteSQL(sql, &pDataSet);
		if (res != LumexDB::enSuccess || pDataSet == NULL)
		{
			assert(false);
			return false;
		}

		pMtd->Formula.clear();
		while (!pDataSet->IsEOF())
		{
			PRMtdQltElement NewElem;

			pData = pDataSet->GetColumn(0);
			VerifyDataPointer;	
			res = pData->GetData(&NewElem.Type, sizeof(NewElem.Type));
			VerifySuccess;			

			pData = pDataSet->GetColumn(1);
			VerifyDataPointer;	
			res = pData->GetData(&NewElem.ModelInd, sizeof(NewElem.ModelInd));
			VerifySuccess;			

			pMtd->Formula.push_back(NewElem);
			
			res = pDataSet->NextRow();
			VerifySuccessAndNoData;
		}
		pDataSet->Release();
	}

	return true;
}

long CPRDBMethodQltPRO::getCurrentVersion()
{
	return PRDB_METHOD_QLT_CURRENT_VERSION;
}

CString CPRDBMethodQltPRO::getCurrentDBName()
{
	return _T("METHOD_QLT_DATABASE_LUMEX_NIOKR");
}
