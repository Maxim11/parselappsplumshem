#ifndef _PARCEL_PRDB_SPECTRA_SQL_H_
#define _PARCEL_PRDB_SPECTRA_SQL_H_

#pragma once

//////////////////////////////////////////////////////////
// ��������� ������������

#define APJDB_SQL_CREATE_TAB_APJ \
"CREATE TABLE ApjProjects(\n\
ApjProjectID	COUNTER		PRIMARY KEY,\n\
ApjProjectName	TEXT(%d)	NOT NULL,\n\
DateCreated		DATETIME	NOT NULL,\n\
DevNum			INT			NOT NULL,\n\
DevType			INT			NOT NULL,\n\
SpecRangeLow	INT			NOT NULL,\n\
SpecRangeUpper	INT			NOT NULL,\n\
ResSub			INT			NOT NULL,\n\
ApodSub			INT			NOT NULL,\n\
ZeroFill		INT			NOT NULL,\n\
UseRef			BIT			NOT NULL,\n\
ResRef			INT			NOT NULL,\n\
ApodRef			INT			NOT NULL,\n\
SmplScan		INT			NOT NULL,\n\
SmplMeas		INT			NOT NULL,\n\
StdScan			INT			NOT NULL,\n\
StdMeas			INT			NOT NULL,\n\
Xtransform		BIT			NOT NULL,\n\
IsSST			BIT			NOT NULL,\n\
UseBath			BIT			NOT NULL,\n\
BathLength		FLOAT		NOT NULL,\n\
TempLow			INT			NOT NULL,\n\
TempUpper		INT			NOT NULL,\n\
TempDiff		INT			NOT NULL,\n\
CONSTRAINT PRAPJ UNIQUE (ApjProjectName))"

#define APJDB_SQL_INSERT_APJ \
"INSERT INTO ApjProjects (\n\
ApjProjectID,\n\
ApjProjectName,\n\
DateCreated,\n\
DevNum,\n\
DevType,\n\
SpecRangeLow,\n\
SpecRangeUpper,\n\
ResSub,\n\
ApodSub,\n\
ZeroFill,\n\
UseRef,\n\
ResRef,\n\
ApodRef,\n\
SmplScan,\n\
SmplMeas,\n\
StdScan,\n\
StdMeas,\n\
Xtransform,\n\
IsSST,\n\
UseBath,\n\
BathLength,\n\
TempLow,\n\
TempUpper,\n\
TempDiff)\n\
VALUES (%d, '%s', #%s#, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %s, %d, %d, %d)"

#define APJDB_SQL_UPDATE_APJ \
"UPDATE ApjProjects\n\
SET ApjProjectName = '%s',\n\
DateCreated = #%s#,\n\
DevNum = %d,\n\
DevType = %d,\n\
SpecRangeLow = %d,\n\
SpecRangeUpper = %d,\n\
ResSub = %d,\n\
ApodSub = %d,\n\
ZeroFill = %d,\n\
UseRef = %d,\n\
ResRef = %d,\n\
ApodRef = %d,\n\
SmplScan = %d,\n\
SmplMeas = %d,\n\
StdScan = %d,\n\
StdMeas = %d,\n\
Xtransform = %d,\n\
IsSST = %d,\n\
UseBath = %d,\n\
BathLength = %s,\n\
TempLow = %d,\n\
TempUpper = %s,\n\
TempDiff = %s\n\
WHERE ApjProjectName = '%s'"

#define APJDB_SQL_DELETE_APJ \
"DELETE FROM ApjProjects WHERE ApjProjectName = '%s'"

#define APJDB_SQL_ISEXISTS_APJ \
"SELECT COUNT(*)\n\
FROM ApjProjects\n\
WHERE ApjProjectName = '%s'"

#define APJDB_SQL_GET_ID_MAX_APJ \
"SELECT MAX(ApjProjectID)\n\
FROM ApjProjects"

#define APJDB_SQL_GET_ID_APJ \
"SELECT ApjProjectID\n\
FROM ApjProjects\n\
WHERE ApjProjectName = '%s'"

#define APJDB_SQL_READ_APJ \
"SELECT ApjProjectID,\n\
ApjProjectName,\n\
DateCreated,\n\
DevNum,\n\
DevType,\n\
SpecRangeLow,\n\
SpecRangeUpper,\n\
ResSub,\n\
ApodSub,\n\
ZeroFill,\n\
UseRef,\n\
ResRef,\n\
ApodRef,\n\
SmplScan,\n\
SmplMeas,\n\
StdScan,\n\
StdMeas,\n\
Xtransform,\n\
IsSST,\n\
UseBath,\n\
BathLength,\n\
TempLow,\n\
TempUpper,\n\
TempDiff\n\
FROM ApjProjects"

//////////////////////////////////////////////////////////
// ����������

#define APJDB_SQL_CREATE_TAB_APJ_PARAM \
"CREATE TABLE Params (\n\
ParamID			COUNTER		PRIMARY KEY,\n\
ApjProjectID	INT			NOT NULL REFERENCES ApjProjects(ApjProjectID) ON UPDATE CASCADE ON DELETE CASCADE,\n\
ParamName		TEXT(%d)	NOT NULL,\n\
Unit			TEXT(%d)	NOT NULL,\n\
Format			TEXT(%d)	NOT NULL,\n\
StdMois			FLOAT		NOT NULL,\n\
UseStdMois		BIT			NOT NULL,\n\
CONSTRAINT PRAPJPARAM UNIQUE (ApjProjectID, ParamName))"

#define APJDB_SQL_INSERT_APJ_PARAM \
"INSERT INTO Params(\n\
ParamID,\n\
ApjProjectID,\n\
ParamName,\n\
Unit,\n\
Format,\n\
StdMois,\n\
UseStdMois)\n\
VALUES (%d, %d, '%s', '%s', '%s', %s, %d)"

#define APJDB_SQL_DELETE_APJ_PARAM \
"DELETE FROM Params\n\
WHERE ParamName = '%s' AND ApjProjectID = (SELECT ApjProjectID FROM ApjProjects WHERE ApjProjectName = '%s')"

#define APJDB_SQL_DELETE_ALL_APJ_PARAM \
"DELETE FROM Params\n\
WHERE ApjProjectID = %d"

#define APJDB_SQL_GET_ID_MAX_APJ_PARAM \
"SELECT MAX(ParamID)\n\
FROM Params"

#define APJDB_SQL_GET_ID_APJ_PARAM \
"SELECT ParamID\n\
FROM Params\n\
WHERE ParamName = '%s' AND ApjProjectID = %d"

#define APJDB_SQL_READ_APJ_PARAM \
"SELECT ParamName,\n\
Unit,\n\
Format,\n\
StdMois,\n\
UseStdMois\n\
FROM Params\n\
WHERE ApjProjectID = %d"

//////////////////////////////////////////////////////////
// �������

#define APJDB_SQL_CREATE_TAB_APJ_SAMPLE \
"CREATE TABLE Samples (\n\
SampleID		COUNTER		PRIMARY KEY,\n\
ApjProjectID	INT			NOT NULL REFERENCES ApjProjects(ApjProjectID) ON UPDATE CASCADE ON DELETE CASCADE,\n\
SampleName		TEXT(%d)	NOT NULL,\n\
Iden2			TEXT(%d)	NOT NULL,\n\
Iden3			TEXT(%d)	NOT NULL,\n\
CONSTRAINT PRAPJSAMPLE UNIQUE (ApjProjectID, SampleName))"

#define APJDB_SQL_INSERT_APJ_SAMPLE \
"INSERT INTO Samples(\n\
SampleID,\n\
ApjProjectID,\n\
SampleName,\n\
Iden2,\n\
Iden3)\n\
VALUES (%d, %d, '%s', '%s', '%s')"

#define APJDB_SQL_UPDATE_APJ_SAMPLE \
"UPDATE Samples\n\
SET SampleName = '%s',\n\
Iden2 = '%s',\n\
Iden3 = '%s'\n\
WHERE SampleID = %d AND ApjProjectID = %d"

#define APJDB_SQL_DELETE_APJ_SAMPLE \
"DELETE FROM Samples\n\
WHERE SampleName = '%s' AND ApjProjectID = (SELECT ApjProjectID FROM ApjProjects WHERE ApjProjectName = '%s')"

#define APJDB_SQL_DELETE_ALL_APJ_SAMPLE \
"DELETE FROM Samples\n\
WHERE ApjProjectID = %d"

#define APJDB_SQL_ISEXISTS_APJ_SAMPLE \
"SELECT COUNT(*)\n\
FROM Samples\n\
WHERE SampleName = '%s' AND ApjProjectID = %d"

#define APJDB_SQL_GET_ID_MAX_APJ_SAMPLE \
"SELECT MAX(SampleID)\n\
FROM Samples"

#define APJDB_SQL_GET_ID_APJ_SAMPLE \
"SELECT SampleID\n\
FROM Samples\n\
WHERE SampleName = '%s' AND ApjProjectID = %d"

#define APJDB_SQL_GET_SAMNAMES_ID_APJ_SPECTRA \
"SELECT SampleName\n\
FROM Samples\n\
WHERE ApjProjectID = (SELECT ApjProjectID FROM ApjProjects WHERE ApjProjectName = '%s')"

#define APJDB_SQL_READ_APJ_SAMPLE \
"SELECT SampleID,\n\
SampleName,\n\
Iden2,\n\
Iden3\n\
FROM Samples\n\
WHERE ApjProjectID = %d"

//////////////////////////////////////////////////////////
// ����������� ������

#define APJDB_SQL_CREATE_TAB_APJ_REFDATA \
"CREATE TABLE RefDatas(\n\
RefDataID		COUNTER		PRIMARY KEY,\n\
ParamID			INT			NOT NULL REFERENCES Params(ParamID) ON UPDATE CASCADE ON DELETE CASCADE,\n\
SampleID		INT			NOT NULL REFERENCES Samples(SampleID) ON UPDATE CASCADE ON DELETE CASCADE,\n\
Data			FLOAT		NOT NULL,\n\
IsDataExist		BIT			NOT NULL,\n\
CONSTRAINT PRAPJREFDATA UNIQUE (ParamID, SampleID))"

#define APJDB_SQL_INSERT_APJ_REFDATA \
"INSERT INTO RefDatas(\n\
RefDataID,\n\
ParamID,\n\
SampleID,\n\
Data,\n\
IsDataExist)\n\
VALUES (%d, %d, %d, %s, %d)"

#define APJDB_SQL_DELETE_ALL_APJ_REFDATA \
"DELETE FROM RefDatas\n\
WHERE SampleID = %d"

#define APJDB_SQL_SET_DATA_APJ_REFDATA \
"UPDATE RefDatas\n\
SET Data = %s,\n\
IsDataExist = %d\n\
WHERE ParamID = %d AND SampleID = %d"

#define APJDB_SQL_GET_ID_MAX_APJ_REFDATA \
"SELECT MAX(RefDataID)\n\
FROM RefDatas"

#define APJDB_SQL_READ_APJ_REFDATA \
"SELECT p.ParamName,\n\
r.Data,\n\
r.IsDataExist\n\
FROM RefDatas AS r\n\
LEFT OUTER JOIN Params AS p\n\
ON r.ParamID = p.ParamID\n\
WHERE SampleID = %d"

//////////////////////////////////////////////////////////
// ������

#define APJDB_SQL_CREATE_TAB_APJ_SPECTRA \
"CREATE TABLE Spectra(\n\
SpectrumID		COUNTER		PRIMARY KEY,\n\
SampleID		INT			NOT NULL REFERENCES Samples(SampleID) ON UPDATE CASCADE ON DELETE CASCADE,\n\
SpecNum			INT			NOT NULL,\n\
DateCreated		DATETIME	NOT NULL,\n\
CONSTRAINT PRAPJSPECTRA UNIQUE (SampleID, SpecNum))"

#define APJDB_SQL_INSERT_APJ_SPECTRA \
"INSERT INTO Spectra(\n\
SpectrumID,\n\
SampleID,\n\
SpecNum,\n\
DateCreated)\n\
VALUES (%d, %d, %d, #%s#)"

#define APJDB_SQL_DELETE_APJ_SPECTRA \
"DELETE FROM Spectra\n\
WHERE SpecNum = %d AND SampleID = (SELECT SampleID FROM Samples WHERE SampleName = '%s' AND ApjProjectID = (SELECT ApjProjectID FROM ApjProjects WHERE ApjProjectName = '%s'))"

#define APJDB_SQL_DELETE_ALL_APJ_SPECTRA \
"DELETE FROM Spectra\n\
WHERE SampleID = %d"

#define APJDB_SQL_ISEXISTS_APJ_SPECTRA \
"SELECT COUNT(*)\n\
FROM Spectra\n\
WHERE SpecNum = %d AND SampleID = %d"

#define APJDB_SQL_GET_ID_MAX_APJ_SPECTRA \
"SELECT MAX(SpectrumID)\n\
FROM Spectra"

#define APJDB_SQL_GET_ID_APJ_SPECTRA \
"SELECT SpectrumID\n\
FROM Spectra\n\
WHERE SpecNum = %d AND SampleID = (SELECT SampleID FROM Samples WHERE SampleName = '%s' AND ApjProjectID = (SELECT ApjProjectID FROM ApjProjects WHERE ApjProjectName = '%s'))"

#define APJDB_SQL_GET_SAMSPECTRA_ID_APJ_SPECTRA \
"SELECT SpectrumID\n\
FROM Spectra\n\
WHERE SampleID = (SELECT SampleID FROM Samples WHERE SampleName = '%s' AND ApjProjectID = (SELECT ApjProjectID FROM ApjProjects WHERE ApjProjectName = '%s'))"

#define APJDB_SQL_GET_NUM_MAX_APJ_SPECTRA \
"SELECT MAX(SpecNum)\n\
FROM Spectra\n\
WHERE SampleID = (SELECT SampleID FROM Samples WHERE SampleName = '%s' AND ApjProjectID = (SELECT ApjProjectID FROM ApjProjects WHERE ApjProjectName = '%s'))"

#define APJDB_SQL_READ_APJ_SPECTRA \
"SELECT SpecNum,\n\
DateCreated\n\
FROM Spectra\n\
WHERE SampleID = %d"

#define APJDB_SQL_READ_APJ_SPECTRA_2 \
"SELECT SpectrumID,\n\
SpecNum,\n\
DateCreated\n\
FROM Spectra\n\
WHERE SampleID = %d"


#endif
