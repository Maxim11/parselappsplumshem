#ifndef _PRDB_METHOD_SQL_H_
#define _PRDB_METHOD_SQL_H_

#pragma once

//////////////////////////////////////////////////////////
// �������������� ������
//////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////
// �����

#define MTDDB_SQL_CREATE_TAB_MTD \
"CREATE TABLE Methods(\n\
MethodID		COUNTER		PRIMARY KEY,\n\
MethodName		TEXT(%d)	NOT NULL,\n\
DateCreated		DATETIME	NOT NULL,\n\
MoisParam		TEXT(%d)	NOT NULL,\n\
DevNum			INT			NOT NULL,\n\
DevType			INT			NOT NULL,\n\
SpecRangeLow	INT			NOT NULL,\n\
SpecRangeUpper	INT			NOT NULL,\n\
ResSub			INT			NOT NULL,\n\
ApodSub			INT			NOT NULL,\n\
ZeroFill		INT			NOT NULL,\n\
UseRef			BIT			NOT NULL,\n\
ResRef			INT			NOT NULL,\n\
ApodRef			INT			NOT NULL,\n\
SmplScan		INT			NOT NULL,\n\
SmplMeas		INT			NOT NULL,\n\
StdScan			INT			NOT NULL,\n\
StdMeas			INT			NOT NULL,\n\
Xtransform		BIT			NOT NULL,\n\
UseBath			BIT			NOT NULL,\n\
BathLength		FLOAT		NOT NULL,\n\
IsSST			BIT			NOT NULL,\n\
TempLow			INT			NOT NULL,\n\
TempUpper		INT			NOT NULL,\n\
TempDiff		INT			NOT NULL,\n\
CONSTRAINT PRMTD UNIQUE (MethodName))"

#define MTDDB_SQL_INSERT_MTD \
"INSERT INTO Methods (\n\
MethodID,\n\
MethodName,\n\
DateCreated,\n\
MoisParam,\n\
DevNum,\n\
DevType,\n\
SpecRangeLow,\n\
SpecRangeUpper,\n\
ResSub,\n\
ApodSub,\n\
ZeroFill,\n\
UseRef,\n\
ResRef,\n\
ApodRef,\n\
SmplScan,\n\
SmplMeas,\n\
StdScan,\n\
StdMeas,\n\
Xtransform,\n\
UseBath,\n\
BathLength,\n\
IsSST,\n\
TempLow,\n\
TempUpper,\n\
TempDiff)\n\
VALUES (%d, '%s', #%s#, '%s', %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %s, %d, %d, %d, %d)"

#define MTDDB_SQL_UPDATE_MTD \
"UPDATE Methods\n\
SET MethodName = '%s',\n\
DateCreated = #%s#,\n\
MoisParam = '%s',\n\
DevNum = %d,\n\
DevType = %d,\n\
SpecRangeLow = %d,\n\
SpecRangeUpper = %d,\n\
ResSub = %d,\n\
ApodSub = %d,\n\
ZeroFill = %d,\n\
UseRef = %d,\n\
ResRef = %d,\n\
ApodRef = %d,\n\
SmplScan = %d,\n\
SmplMeas = %d,\n\
StdScan = %d,\n\
StdMeas = %d,\n\
Xtransform = %d,\n\
UseBath = %d,\n\
BathLength = %s,\n\
IsSST = %d,\n\
TempLow = %d,\n\
TempUpper = %d,\n\
TempDiff = %d\n\
WHERE MethodName = '%s'"

#define MTDDB_SQL_DELETE_MTD \
"DELETE FROM Methods WHERE MethodName = '%s'"

#define MTDDB_SQL_ISEXISTS_MTD \
"SELECT COUNT(*)\n\
FROM Methods\n\
WHERE MethodName = '%s'"

#define MTDDB_SQL_GET_MTD_ID_MAX \
"SELECT MAX(MethodID)\n\
FROM Methods"

#define MTDDB_SQL_GET_MTD_ID \
"SELECT MethodID\n\
FROM Methods\n\
WHERE MethodName = '%s'"

#define MTDDB_SQL_READ_MTD \
"SELECT MethodID,\n\
MethodName,\n\
DateCreated,\n\
MoisParam,\n\
DevNum,\n\
DevType,\n\
SpecRangeLow,\n\
SpecRangeUpper,\n\
ResSub,\n\
ApodSub,\n\
ZeroFill,\n\
UseRef,\n\
ResRef,\n\
ApodRef,\n\
SmplScan,\n\
SmplMeas,\n\
StdScan,\n\
StdMeas,\n\
Xtransform,\n\
UseBath,\n\
BathLength,\n\
IsSST,\n\
TempLow,\n\
TempUpper,\n\
TempDiff\n\
FROM Methods"

//////////////////////////////////////////////////////////
// ���������� ������

#define MTDDB_SQL_CREATE_TAB_MTDPARAM \
"CREATE TABLE MtdParams(\n\
MtdParamID		COUNTER		PRIMARY KEY,\n\
MethodID		INT			NOT NULL REFERENCES Methods(MethodID) ON UPDATE CASCADE ON DELETE CASCADE,\n\
ParamName		TEXT(%d)	NOT NULL,\n\
Unit			TEXT(%d)	NOT NULL,\n\
Format			TEXT(%d)	NOT NULL,\n\
UseDefModel		BIT			NOT NULL,\n\
DefModel		TEXT(%d)	NOT NULL,\n\
UseRefMois		BIT			NOT NULL,\n\
RefMois			FLOAT		NOT NULL,\n\
UseStdMois		BIT			NOT NULL,\n\
StdMois			FLOAT		NOT NULL,\n\
CONSTRAINT PRMTDPAR UNIQUE (MethodID, ParamName))"

#define MTDDB_SQL_INSERT_MTDPARAM \
"INSERT INTO MtdParams(\n\
MtdParamID,\n\
MethodID,\n\
ParamName,\n\
Unit,\n\
Format,\n\
UseDefModel,\n\
DefModel,\n\
UseRefMois,\n\
RefMois,\n\
UseStdMois,\n\
StdMois)\n\
VALUES (%d, %d, '%s', '%s', '%s', %d, '%s', %d, %s, %d, %s)"

#define MTDDB_SQL_DELETE_MTDPARAM_ALL \
"DELETE FROM MtdParams\n\
WHERE MethodID = %d"

#define MTDDB_SQL_GET_MTDPARAM_ID_MAX \
"SELECT MAX(MtdParamID)\n\
FROM MtdParams"

#define MTDDB_SQL_GET_MTDPARAM_ID \
"SELECT MtdParamID\n\
FROM MtdParams\n\
WHERE MethodID = %d AND ParamName = '%s'"

#define MTDDB_SQL_READ_MTDPARAM \
"SELECT MtdParamID,\n\
ParamName,\n\
Unit,\n\
Format,\n\
UseDefModel,\n\
DefModel,\n\
UseRefMois,\n\
RefMois,\n\
UseStdMois,\n\
StdMois\n\
FROM MtdParams\n\
WHERE MethodID = %d"

//////////////////////////////////////////////////////////
// ������ ������

#define MTDDB_SQL_CREATE_TAB_MTDMODEL \
"CREATE TABLE MtdModels(\n\
MtdModelID		COUNTER		PRIMARY KEY,\n\
MethodID		INT			NOT NULL REFERENCES Methods(MethodID) ON UPDATE CASCADE ON DELETE CASCADE,\n\
ModelName		TEXT(%d)	NOT NULL,\n\
ModelType		INT			NOT NULL,\n\
SpecRangeLow	INT			NOT NULL,\n\
SpecRangeUpper	INT			NOT NULL,\n\
FreqMin			FLOAT		NOT NULL,\n\
FreqStep		FLOAT		NOT NULL,\n\
NFreq			INT			NOT NULL,\n\
SpType			INT			NOT NULL,\n\
NSpec			INT			NOT NULL,\n\
Preproc			TEXT(%d)	NOT NULL,\n\
MaxMah			FLOAT		NOT NULL,\n\
MeanMah			DOUBLE		NOT NULL,\n\
IsTrans			BIT			NOT NULL,\n\
FreqMinTrans	FLOAT		NOT NULL,\n\
FreqStepTrans	FLOAT		NOT NULL,\n\
NFreqTrans		INT			NOT NULL,\n\
SpTypeTrans		INT			NOT NULL,\n\
PreprocTrans	TEXT(%d)	NOT NULL,\n\
T0				FLOAT		NOT NULL,\n\
FMax			DOUBLE		NOT NULL,\n\
CONSTRAINT PRMTDPAR UNIQUE (MethodID, ModelName))"

#define MTDDB_SQL_INSERT_MTDMODEL \
"INSERT INTO MtdModels(\n\
MtdModelID,\n\
MethodID,\n\
ModelName,\n\
ModelType,\n\
SpecRangeLow,\n\
SpecRangeUpper,\n\
FreqMin,\n\
FreqStep,\n\
NFreq,\n\
SpType,\n\
NSpec,\n\
Preproc,\n\
MaxMah,\n\
MeanMah,\n\
IsTrans,\n\
FreqMinTrans,\n\
FreqStepTrans,\n\
NFreqTrans,\n\
SpTypeTrans,\n\
PreprocTrans,\n\
T0,\n\
FMax)\n\
VALUES (%d, %d, '%s', %d, %d, %d, %s, %s, %d, %d, %d, '%s', %s, %s, %d, %s, %s, %d, %d, '%s', %s, %s)"

#define MTDDB_SQL_DELETE_MTDMODEL_ALL \
"DELETE FROM MtdModels\n\
WHERE MethodID = %d"

#define MTDDB_SQL_GET_MTDMODEL_ID_MAX \
"SELECT MAX(MtdModelID)\n\
FROM MtdModels"

#define MTDDB_SQL_GET_MTDMODEL_ID \
"SELECT MtdModelID\n\
FROM MtdModels\n\
WHERE MethodID = %d AND ModelName = '%s'"

#define MTDDB_SQL_READ_MTDMODEL \
"SELECT MtdModelID,\n\
ModelName,\n\
ModelType,\n\
SpecRangeLow,\n\
SpecRangeUpper,\n\
FreqMin,\n\
FreqStep,\n\
NFreq,\n\
SpType,\n\
NSpec,\n\
Preproc,\n\
MaxMah,\n\
MeanMah,\n\
IsTrans,\n\
FreqMinTrans,\n\
FreqStepTrans,\n\
NFreqTrans,\n\
SpTypeTrans,\n\
PreprocTrans,\n\
T0,\n\
FMax\n\
FROM MtdModels\n\
WHERE MethodID = %d"

//////////////////////////////////////////////////////////
// ������ ����������� ������

#define MTDDB_SQL_CREATE_TAB_PARMODEL \
"CREATE TABLE ParModels(\n\
ParModelID		COUNTER		PRIMARY KEY,\n\
MtdParamID		INT			NOT NULL REFERENCES MtdParams(MtdParamID) ON UPDATE CASCADE ON DELETE CASCADE,\n\
MtdModelID		INT			NOT NULL REFERENCES MtdModels(MtdModelID) ON UPDATE CASCADE ON DELETE CASCADE,\n\
LowCalibRange	FLOAT		NOT NULL,\n\
UpperCalibRange	FLOAT		NOT NULL,\n\
MaxSKO			FLOAT		NOT NULL,\n\
CorrB			DOUBLE		NOT NULL,\n\
CorrA			DOUBLE		NOT NULL,\n\
SEC				FLOAT		NOT NULL,\n\
MeanComp		DOUBLE		NOT NULL,\n\
MeanCompTrans	DOUBLE		NOT NULL,\n\
MeanCompStd		DOUBLE		NOT NULL,\n\
CONSTRAINT PRMTDPAR UNIQUE (MtdParamID, MtdModelID))"

#define MTDDB_SQL_INSERT_PARMODEL \
"INSERT INTO ParModels(\n\
ParModelID,\n\
MtdParamID,\n\
MtdModelID,\n\
LowCalibRange,\n\
UpperCalibRange,\n\
MaxSKO,\n\
CorrB,\n\
CorrA,\n\
SEC,\n\
MeanComp,\n\
MeanCompTrans,\n\
MeanCompStd)\n\
VALUES (%d, %d, %d, %s, %s, %s, %s, %s, %s, %s, %s, %s)"

#define MTDDB_SQL_SET_CORR_PARMODEL \
"UPDATE ParModels\n\
SET CorrB = %s,\n\
CorrA = %s\n\
WHERE MtdParamID = %d AND MtdModelID = %d"

#define MTDDB_SQL_GET_PARMODEL_ID_MAX \
"SELECT MAX(ParModelID)\n\
FROM ParModels"

#define MTDDB_SQL_READ_PARMODEL \
"SELECT p.ParModelID,\n\
m.ModelName,\n\
p.LowCalibRange,\n\
p.UpperCalibRange,\n\
p.MaxSKO,\n\
p.CorrB,\n\
p.CorrA,\n\
p.SEC,\n\
p.MeanComp,\n\
p.MeanCompTrans,\n\
p.MeanCompStd\n\
FROM ParModels AS p\n\
LEFT OUTER JOIN MtdModels AS m\n\
ON p.MtdModelID = m.MtdModelID\n\
WHERE MtdParamID = %d"

//////////////////////////////////////////////////////////
// ����������� �����

#define MTDDB_SQL_CREATE_TAB_EXCPNT \
"CREATE TABLE MtdModelExcPoints (\n\
MtdModelExcPointID	COUNTER		PRIMARY KEY,\n\
MtdModelID			INT			NOT NULL REFERENCES MtdModels(MtdModelID) ON UPDATE CASCADE ON DELETE CASCADE,\n\
ExcPoint			INT			NOT NULL,\n\
CONSTRAINT PRMTDPAR UNIQUE (MtdModelID, ExcPoint))"

#define MTDDB_SQL_INSERT_EXCPNT \
"INSERT INTO MtdModelExcPoints (\n\
MtdModelID,\n\
ExcPoint)\n\
VALUES (%d, %d)"

#define MTDDB_SQL_READ_EXCPNT \
"SELECT ExcPoint\n\
FROM MtdModelExcPoints\n\
WHERE MtdModelID = %d\n\
ORDER BY MtdModelExcPointID"

//////////////////////////////////////////////////////////
// ������� ������� ��� ������

#define MTDDB_SQL_CREATE_TAB_MEANSPEC \
"CREATE TABLE MeanSpectra(\n\
MeanSpectrumID	COUNTER		PRIMARY KEY,\n\
MtdModelID		INT			NOT NULL REFERENCES MtdModels(MtdModelID) ON UPDATE CASCADE ON DELETE CASCADE,\n\
MeanSpecType	INT			NOT NULL,\n\
CONSTRAINT PRMTDPAR UNIQUE (MtdModelID, MeanSpecType))"

#define MTDDB_SQL_INSERT_MEANSPEC \
"INSERT INTO MeanSpectra (\n\
MeanSpectrumID,\n\
MtdModelID,\n\
MeanSpecType)\n\
VALUES (%d, %d, %d)"

#define MTDDB_SQL_GET_MEANSPEC_ID_MAX \
"SELECT MAX(MeanSpectrumID)\n\
FROM MeanSpectra"

#define MTDDB_SQL_READ_MEANSPEC \
"SELECT MeanSpectrumID,\n\
MeanSpecType\n\
FROM MeanSpectra\n\
WHERE MtdModelID = %d"

//////////////////////////////////////////////////////////
// ������ �� ������� �������� ��� ������

#define MTDDB_SQL_CREATE_TAB_MEANSPEC_DATA \
"CREATE TABLE MeanSpectraDatas (\n\
MeanSpectraDataID	COUNTER		PRIMARY KEY,\n\
MeanSpectrumID		INT			NOT NULL REFERENCES MeanSpectra(MeanSpectrumID) ON UPDATE CASCADE ON DELETE CASCADE,\n\
Data				DOUBLE		NOT NULL)"

#define MTDDB_SQL_INSERT_MEANSPEC_DATA \
"INSERT INTO MeanSpectraDatas (\n\
MeanSpectrumID,\n\
Data)\n\
VALUES (%d, %s)"

#define MTDDB_SQL_READ_MEANSPEC_DATA \
"SELECT Data\n\
FROM MeanSpectraDatas\n\
WHERE MeanSpectrumID = %d\n\
ORDER BY MeanSpectraDataID"

//////////////////////////////////////////////////////////
// ������� �� �������������� ������ ��� ����������� ������

#define MTDDB_SQL_CREATE_TAB_CALIBVECTOR \
"CREATE TABLE CalibVectors(\n\
CalibVectorID	COUNTER		PRIMARY KEY,\n\
ParModelID		INT			NOT NULL REFERENCES ParModels(ParModelID) ON UPDATE CASCADE ON DELETE CASCADE,\n\
CalibVectorType	INT			NOT NULL,\n\
CONSTRAINT PRMTDPAR UNIQUE (ParModelID, CalibVectorType))"

#define MTDDB_SQL_INSERT_CALIBVECTOR \
"INSERT INTO CalibVectors (\n\
CalibVectorID,\n\
ParModelID,\n\
CalibVectorType)\n\
VALUES (%d, %d, %d)"

#define MTDDB_SQL_GET_CALIBVECTOR_ID_MAX \
"SELECT MAX(CalibVectorID)\n\
FROM CalibVectors"

#define MTDDB_SQL_READ_CALIBVECTOR \
"SELECT CalibVectorID,\n\
CalibVectorType\n\
FROM CalibVectors\n\
WHERE ParModelID = %d"

//////////////////////////////////////////////////////////
// ������ �� �������� �� �������������� ������ ��� ����������� ������

#define MTDDB_SQL_CREATE_TAB_CALIBVECTOR_DATA \
"CREATE TABLE CalibVectorDatas (\n\
CalibVectorDataID	COUNTER		PRIMARY KEY,\n\
CalibVectorID		INT			NOT NULL REFERENCES CalibVectors(CalibVectorID) ON UPDATE CASCADE ON DELETE CASCADE,\n\
Data				DOUBLE		NOT NULL)"

#define MTDDB_SQL_INSERT_CALIBVECTOR_DATA \
"INSERT INTO CalibVectorDatas (\n\
CalibVectorID,\n\
Data)\n\
VALUES (%d, %s)"

#define MTDDB_SQL_READ_CALIBVECTOR_DATA \
"SELECT Data\n\
FROM CalibVectorDatas\n\
WHERE CalibVectorID = %d\n\
ORDER BY CalibVectorDataID"


//////////////////////////////////////////////////////////
// ������������ ������
//////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////
// �����

#define MTDDB_SQL_CREATE_TAB_MTD_QLT \
"CREATE TABLE Methods(\n\
MethodID		COUNTER		PRIMARY KEY,\n\
MethodName		TEXT(%d)	NOT NULL,\n\
DateCreated		DATETIME	NOT NULL,\n\
DevNum			INT			NOT NULL,\n\
DevType			INT			NOT NULL,\n\
SpecRangeLow	INT			NOT NULL,\n\
SpecRangeUpper	INT			NOT NULL,\n\
ResSub			INT			NOT NULL,\n\
ApodSub			INT			NOT NULL,\n\
ZeroFill		INT			NOT NULL,\n\
UseRef			BIT			NOT NULL,\n\
ResRef			INT			NOT NULL,\n\
ApodRef			INT			NOT NULL,\n\
SmplScan		INT			NOT NULL,\n\
SmplMeas		INT			NOT NULL,\n\
StdScan			INT			NOT NULL,\n\
StdMeas			INT			NOT NULL,\n\
Xtransform		BIT			NOT NULL,\n\
UseBath			BIT			NOT NULL,\n\
BathLength		FLOAT		NOT NULL,\n\
TempLow			INT			NOT NULL,\n\
TempUpper		INT			NOT NULL,\n\
TempDiff		INT			NOT NULL,\n\
CONSTRAINT PRMTD UNIQUE (MethodName))"

#define MTDDB_SQL_INSERT_MTD_QLT \
"INSERT INTO Methods (\n\
MethodID,\n\
MethodName,\n\
DateCreated,\n\
DevNum,\n\
DevType,\n\
SpecRangeLow,\n\
SpecRangeUpper,\n\
ResSub,\n\
ApodSub,\n\
ZeroFill,\n\
UseRef,\n\
ResRef,\n\
ApodRef,\n\
SmplScan,\n\
SmplMeas,\n\
StdScan,\n\
StdMeas,\n\
Xtransform,\n\
UseBath,\n\
BathLength,\n\
TempLow,\n\
TempUpper,\n\
TempDiff)\n\
VALUES (%d, '%s', #%s#, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %s, %d, %d, %d)"

#define MTDDB_SQL_UPDATE_MTD_QLT \
"UPDATE Methods\n\
SET MethodName = '%s',\n\
DateCreated = #%s#,\n\
DevNum = %d,\n\
DevType = %d,\n\
SpecRangeLow = %d,\n\
SpecRangeUpper = %d,\n\
ResSub = %d,\n\
ApodSub = %d,\n\
ZeroFill = %d,\n\
UseRef = %d,\n\
ResRef = %d,\n\
ApodRef = %d,\n\
SmplScan = %d,\n\
SmplMeas = %d,\n\
StdScan = %d,\n\
StdMeas = %d,\n\
Xtransform = %d,\n\
UseBath = %d,\n\
BathLength = %s,\n\
TempLow = %d,\n\
TempUpper = %d,\n\
TempDiff = %d\n\
WHERE MethodName = '%s'"

#define MTDDB_SQL_DELETE_MTD_QLT \
"DELETE FROM Methods WHERE MethodName = '%s'"

#define MTDDB_SQL_ISEXISTS_MTD_QLT \
"SELECT COUNT(*)\n\
FROM Methods\n\
WHERE MethodName = '%s'"

#define MTDDB_SQL_GET_MTD_QLT_ID_MAX \
"SELECT MAX(MethodID)\n\
FROM Methods"

#define MTDDB_SQL_GET_MTD_QLT_ID \
"SELECT MethodID\n\
FROM Methods\n\
WHERE MethodName = '%s'"

#define MTDDB_SQL_READ_MTD_QLT \
"SELECT MethodID,\n\
MethodName,\n\
DateCreated,\n\
DevNum,\n\
DevType,\n\
SpecRangeLow,\n\
SpecRangeUpper,\n\
ResSub,\n\
ApodSub,\n\
ZeroFill,\n\
UseRef,\n\
ResRef,\n\
ApodRef,\n\
SmplScan,\n\
SmplMeas,\n\
StdScan,\n\
StdMeas,\n\
Xtransform,\n\
UseBath,\n\
BathLength,\n\
TempLow,\n\
TempUpper,\n\
TempDiff\n\
FROM Methods"

//////////////////////////////////////////////////////////
// ������ ������

#define MTDDB_SQL_CREATE_TAB_MTD_QLTMODEL \
"CREATE TABLE MtdModels(\n\
MtdModelID		COUNTER		PRIMARY KEY,\n\
MethodID		INT			NOT NULL REFERENCES Methods(MethodID) ON UPDATE CASCADE ON DELETE CASCADE,\n\
ModelName		TEXT(%d)	NOT NULL,\n\
ModelInd		INT			NOT NULL,\n\
SpecRangeLow	INT			NOT NULL,\n\
SpecRangeUpper	INT			NOT NULL,\n\
FreqMin			FLOAT		NOT NULL,\n\
FreqStep		FLOAT		NOT NULL,\n\
NFreq			INT			NOT NULL,\n\
SpType			INT			NOT NULL,\n\
Preproc			TEXT(%d)	NOT NULL,\n\
MaxMah			FLOAT		NOT NULL,\n\
MaxSKO			FLOAT		NOT NULL,\n\
MeanMah			DOUBLE		NOT NULL,\n\
IsTrans			BIT			NOT NULL,\n\
FreqMinTrans	FLOAT		NOT NULL,\n\
FreqStepTrans	FLOAT		NOT NULL,\n\
NFreqTrans		INT			NOT NULL,\n\
SpTypeTrans		INT			NOT NULL,\n\
PreprocTrans	TEXT(%d)	NOT NULL,\n\
CONSTRAINT PRMTDPAR UNIQUE (MethodID, ModelName))"

#define MTDDB_SQL_INSERT_MTD_QLTMODEL \
"INSERT INTO MtdModels(\n\
MtdModelID,\n\
MethodID,\n\
ModelName,\n\
ModelInd,\n\
SpecRangeLow,\n\
SpecRangeUpper,\n\
FreqMin,\n\
FreqStep,\n\
NFreq,\n\
SpType,\n\
Preproc,\n\
MaxMah,\n\
MaxSKO,\n\
MeanMah,\n\
IsTrans,\n\
FreqMinTrans,\n\
FreqStepTrans,\n\
NFreqTrans,\n\
SpTypeTrans,\n\
PreprocTrans)\n\
VALUES (%d, %d, '%s', %d, %d, %d, %s, %s, %d, %d, '%s', %s, %s, %s, %d, %s, %s, %d, %d, '%s')"

#define MTDDB_SQL_DELETE_MTD_QLTMODEL_ALL \
"DELETE FROM MtdModels\n\
WHERE MethodID = %d"

#define MTDDB_SQL_GET_MTD_QLTMODEL_ID_MAX \
"SELECT MAX(MtdModelID)\n\
FROM MtdModels"

#define MTDDB_SQL_GET_MTD_QLTMODEL_ID \
"SELECT MtdModelID\n\
FROM MtdModels\n\
WHERE MethodID = %d AND ModelName = '%s'"

#define MTDDB_SQL_READ_MTD_QLTMODEL \
"SELECT MtdModelID,\n\
ModelName,\n\
ModelInd,\n\
SpecRangeLow,\n\
SpecRangeUpper,\n\
FreqMin,\n\
FreqStep,\n\
NFreq,\n\
SpType,\n\
Preproc,\n\
MaxMah,\n\
MaxSKO,\n\
MeanMah,\n\
IsTrans,\n\
FreqMinTrans,\n\
FreqStepTrans,\n\
NFreqTrans,\n\
SpTypeTrans,\n\
PreprocTrans\n\
FROM MtdModels\n\
WHERE MethodID = %d"

//////////////////////////////////////////////////////////
// ����������� �����

#define MTDDB_SQL_CREATE_TAB_QLT_EXCPNT \
"CREATE TABLE MtdModelExcPoints (\n\
MtdModelExcPointID	COUNTER		PRIMARY KEY,\n\
MtdModelID			INT			NOT NULL REFERENCES MtdModels(MtdModelID) ON UPDATE CASCADE ON DELETE CASCADE,\n\
ExcPoint			INT			NOT NULL,\n\
CONSTRAINT PRMTDPAR UNIQUE (MtdModelID, ExcPoint))"

#define MTDDB_SQL_INSERT_QLT_EXCPNT \
"INSERT INTO MtdModelExcPoints (\n\
MtdModelID,\n\
ExcPoint)\n\
VALUES (%d, %d)"

#define MTDDB_SQL_READ_QLT_EXCPNT \
"SELECT ExcPoint\n\
FROM MtdModelExcPoints\n\
WHERE MtdModelID = %d\n\
ORDER BY MtdModelExcPointID"

//////////////////////////////////////////////////////////
// ������� ������� ��� ������

#define MTDDB_SQL_CREATE_TAB_QLT_MEANSPEC \
"CREATE TABLE MeanSpectra(\n\
MeanSpectrumID	COUNTER		PRIMARY KEY,\n\
MtdModelID		INT			NOT NULL REFERENCES MtdModels(MtdModelID) ON UPDATE CASCADE ON DELETE CASCADE,\n\
MeanSpecType	INT			NOT NULL,\n\
CONSTRAINT PRMTDPAR UNIQUE (MtdModelID, MeanSpecType))"

#define MTDDB_SQL_INSERT_QLT_MEANSPEC \
"INSERT INTO MeanSpectra (\n\
MeanSpectrumID,\n\
MtdModelID,\n\
MeanSpecType)\n\
VALUES (%d, %d, %d)"

#define MTDDB_SQL_GET_QLT_MEANSPEC_ID_MAX \
"SELECT MAX(MeanSpectrumID)\n\
FROM MeanSpectra"

#define MTDDB_SQL_READ_QLT_MEANSPEC \
"SELECT MeanSpectrumID,\n\
MeanSpecType\n\
FROM MeanSpectra\n\
WHERE MtdModelID = %d"

//////////////////////////////////////////////////////////
// ������ �� ������� �������� ��� ������

#define MTDDB_SQL_CREATE_TAB_QLT_MEANSPEC_DATA \
"CREATE TABLE MeanSpectraDatas (\n\
MeanSpectraDataID	COUNTER		PRIMARY KEY,\n\
MeanSpectrumID		INT			NOT NULL REFERENCES MeanSpectra(MeanSpectrumID) ON UPDATE CASCADE ON DELETE CASCADE,\n\
Data				DOUBLE		NOT NULL)"

#define MTDDB_SQL_INSERT_QLT_MEANSPEC_DATA \
"INSERT INTO MeanSpectraDatas (\n\
MeanSpectrumID,\n\
Data)\n\
VALUES (%d, %s)"

#define MTDDB_SQL_READ_QLT_MEANSPEC_DATA \
"SELECT Data\n\
FROM MeanSpectraDatas\n\
WHERE MeanSpectrumID = %d\n\
ORDER BY MeanSpectraDataID"

//////////////////////////////////////////////////////////
// ������ ������������� �������

#define MTDDB_SQL_CREATE_TAB_QLT_FORM \
"CREATE TABLE MethodQltForms (\n\
MethodQltFormID	COUNTER		PRIMARY KEY,\n\
MethodID		INT			NOT NULL REFERENCES Methods(MethodID) ON UPDATE CASCADE ON DELETE CASCADE,\n\
Type			INT			NOT NULL,\n\
ModelInd		INT			NOT NULL)"

#define MTDDB_SQL_INSERT_QLT_FORM \
"INSERT INTO MethodQltForms(\n\
MethodID,\n\
Type,\n\
ModelInd)\n\
VALUES (%d, %d, %d)"

#define MTDDB_SQL_DELETE_QLT_FORM_ALL \
"DELETE FROM MethodQltForms\n\
WHERE MethodID = %d"

#define MTDDB_SQL_READ_QLT_FORM \
"SELECT Type,\n\
ModelInd\n\
FROM MethodQltForms\n\
WHERE MethodID = %d\n\
ORDER BY MethodQltFormID"


#endif _PRDB_METHOD_SQL_H_
