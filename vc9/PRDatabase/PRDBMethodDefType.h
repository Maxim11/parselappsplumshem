#ifndef _PARCEL_PRDB_METHOD_DEFTYPE_H_
#define _PARCEL_PRDB_METHOD_DEFTYPE_H_

#pragma once

#include <vector>

namespace mtddb
{
	// ���������� ����� ������������ �����
	const int MTDDBMethodName		= 50;	// ����� ������
	const int MTDDBParamName		= 50;	// ����� ����������
	const int MTDDBParamUnit		= 20;	// ������ ��������� ���. ������
	const int MTDDBParamFormat		= 5;	// ������� ���. ������
	const int MTDDBModelName		= 50;	// ����� ������
	const int MTDDBPreproc			= 8;	// ������ �������������

	// ������� ������ ���� ������
	const long PRDB_METHOD_QNT_CURRENT_VERSION	= 116;
	const long PRDB_METHOD_QLT_CURRENT_VERSION	= 105;

	// �������������� ������� ��������
	const int MTDDBVectorTypeMean			= 0;	// ������� ������ ����. ������
	const int MTDDBVectorTypeMeanMSC		= 1;	// ������� ������ ��� ����������������� ���������
	const int MTDDBVectorTypeMeanStd		= 2;	// c�������. ���������� ��� �������� ����. ������
	const int MTDDBVectorTypeMeanTrans		= 3;	// ������� ������ ������������� ������
	const int MTDDBVectorTypeMeanMSCTrans	= 4;	// ������� ������ ��� ����������������� ��������� ������������� ������
	const int MTDDBVectorTypeMeanStdTrans	= 5;	// c�������. ���������� ��� �������� ������������� ������
	const int MTDDBVectorTypeCalibrQlt		= 6;	// �������������� ������� ��� ������������ �����������
	const int MTDDBVectorTypeFactors		= 7;	// ������� ��������
	const int MTDDBVectorTypeMeanHQO		= 8;	// ������� ������ ��� ��������� HQO
	const int MTDDBVectorTypeMeanMSCHQO		= 9;	// ������� ������ ��� ����������������� ��������� ��� ��������� HQO
	const int MTDDBVectorTypeMeanStdHQO		= 10;	// c�������. ���������� �������� ��� ��������� HQO
	const int MTDDBVectorTypeScores			= 11;	// ������� scores

	// �������������� �������������� �������
	const int MTDDBMatrixTypeCalibrLin		= 0;	// �������� �����
	const int MTDDBMatrixTypeCalibrNonLin	= 1;	// ���������� �����

	// �������������� ��������� ������ ������� ������������� ������
	const int MTDDBQltFormMean				= 0;
	const int MTDDBQltFormAnd				= 1;
	const int MTDDBQltFormOr				= 2;
	const int MTDDBQltFormOpen				= 3;
	const int MTDDBQltFormClose				= 4;

	// ����� ���������� � ������ ������
	struct PRMtdModel
	{
		CString ModelName;		// ��� ������

		int ModelType;			// ��� ������ (0 - PLS, 1 - PCR, 2 - HQO
		int SpecRangeLow;		// ������ ������� ������������� ���������
		int SpecRangeUpper;		// ������� ������� ������������� ���������
		double FreqMin;			// ����������� �������
		double FreqStep;		// ��� �� �������
		int NFreq;				// ����� ������
		int SpType;				// ��� �������
		int NSpec;				// ����� ��������
		CString Preproc;		// ������ ������������� (������ �����, �������� ��� ������)
		double MaxMah;			// ���������� ��������� ������������
		double MeanMah;			// ������� ���������� ������������ ��� �������� ��������������� ������

		bool IsTrans;			// ������� ������������� ������������� ������
		double FreqMinTrans;	// ����������� ������� ������������� ������
		double FreqStepTrans;	// ��� �� ������� ������������� ������
		int NFreqTrans;			// ����� ������ ������������� ������
		int SpTypeTrans;		// ��� ������� ������������� ������
		CString PreprocTrans;	// ������ ������������� ������������� ������

		double T0;				// ����������� ���������
		double FMax;			// ������. �������� ����. ������ ��� ������������� �H�

		VInt ExcPoints;			// ������ ����������� ������������ �����

		VDbl MeanSpecTrans;		// ������� ������ ����. ������
		VDbl MeanSpecMSCTrans;	// ������� ������ ��� ����������������� ���������
		VDbl MeanSpecStdTrans;	// c�������. ���������� ��� �������� ��������������� ������

		VDbl MeanSpec;			// ������� ������ ������������� ������
		VDbl MeanSpecMSC;		// ������� ������ ��� ����������������� ��������� ������������� ������
		VDbl MeanSpecStd;		// c�������. ���������� ��� �������� ������������� ������

		VDbl MeanSpecHQO;		// ������� ������ ��� ��������� HQO
		VDbl MeanSpecMSCHQO;	// ������� ������ ��� ����������������� ��������� ��� ��������� HQO
		VDbl MeanSpecStdHQO;	// c�������. ���������� ��� �������� ��� ��������� HQO

		MDbl Factors;			// ������� ��������
		MDbl Scores;			// ������� scores
	};

	// ���������� � ���������� (�����������) ���������� ������
	struct PRParModel
	{
		CString ModelName;		// ��� ������

		double LowCalibRange;	// ������ ������� ��������� �����������
		double UpperCalibRange;	// ������� ������� ��������� �����������
		double MaxSKO;			// ���������� ��� �� �����������
		double CorrB;			// ����������� ��������� (����������������� ��������)
		double CorrA;			// ����������� ��������� (���������� ��������)
		double SEC;				// �������� SEC

		double MeanComp;		// ������� ������������ ����. ������ ��� ������ ����������
		double MeanCompTrans;	// ������� ������������ ����. ������ ��� ������ ���������� ��� ������������� ��������
		double MeanCompStd;		// c�������. ���������� ����. ������ ��� ������ ����������

		VDbl CalibrLin;			// �������� ����� �������������� ������� ��� ������� ����������
		VDbl CalibrNonLin;		// ���������� ����� �������������� ������� ��� ������� ����������
	};

	// ���������� � ���������� ������
	struct PRMtdParam
	{
		CString ParamName;		// ��� ����������

		CString Unit;			// ������� ��������� ���. ������
		CString Format;			// ������ ���. ������
		bool UseDefModel;		// ������� ������������� ������ �� ���������
		CString DefModel;		// ��� ������ �� ���������
		bool UseRefMois;		// ������� ������������� ������������ ���������� (���������) ��� ������������ ������
		double RefMois;			// �������� ������������ ���������� (���������) ��� ������������ ������
		bool UseStdMois;		// ������� ������������� ����������� �������� ������������ ���������� 9���������)
		double StdMois;			// ����������� �������� ������������ ���������� (���������)

		vector<PRParModel> ParModels;	// ������ ������� ��� ������� ����������
	};

	// ���������� � ������ ��������������� �������
	struct PRMethodQnt
	{
		CString MethodName;		// ��� ������

		CTime DateCreated;		// ���� ��������
		CString MoisParam;		// ��� ���������� "��� ���������", ����� ���� �����������
		int DevNum;				// �������� ����� �������
		int DevType;			// ��� ������� ��� ID0
		int SpecRangeLow;		// ������ ������� ������������� ��������� ������� �������
		int SpecRangeUpper;		// ������� ������� ������������� ��������� ������� �������
		int ResSub;				// ���������� ����������� ������
		int ApodSub;			// ���������� ����������� ������
		int ZeroFill;			// ���������� ������
		bool UseRef;			// ������� ������������� �������� ������
		int ResRef;				// ���������� �������� ������
		int ApodRef;			// ���������� �������� ������
		int SmplScan;			// ����� ������ ����������
		int SmplMeas;			// ����� ��������� ����������
		int StdScan;			// ����� ������ �������
		int StdMeas;			// ����� ��������� �������
		bool Xtransform;		// ������� �-�������������
		bool UseBath;			// ����� ������������� ������������ �������/������
		double BathLength;		// ����� ������
		bool IsSST;				// ������� SST
		int TempLow;			// ������ ������� ��������� ���������� �������
		int TempUpper;			// ������� ������� ��������� ���������� �������
		int TempDiff;			// ���������� ���������� �� ����������� ���. �����

		vector<PRMtdParam> MtdParams;	// ������ ����������� ������
		vector<PRMtdModel> MtdModels;	// ������ ������� ������
	};

// ����� ������������� �������

	// ���������� � ������ ������
	struct PRMtdQltModel
	{
		CString ModelName;		// ��� ������
		int ModelInd;			// ���������� ����� ������ � ������

		int SpecRangeLow;		// ������ ������� ������������� ���������
		int SpecRangeUpper;		// ������� ������� ������������� ���������
		double FreqMin;			// ����������� �������
		double FreqStep;		// ��� �� �������
		int NFreq;				// ����� ������
		int SpType;				// ��� �������
		CString Preproc;		// ������ ������������� (������ �����, �������� ��� ������)
		double MaxMah;			// ���������� ��������� ������������
		double MaxSKO;			// ���������� ��� �����������
		double MeanMah;			// ������� ���������� ������������ ��� �������� ��������������� ������

		bool IsTrans;			// ������� ������������� ������������� ������
		double FreqMinTrans;	// ����������� ������� ������������� ������
		double FreqStepTrans;	// ��� �� ������� ������������� ������
		int NFreqTrans;			// ����� ������ ������������� ������
		int SpTypeTrans;		// ��� ������� ������������� ������
		CString PreprocTrans;	// ������ ������������� ������������� ������

		VInt ExcPoints;			// ������ ����������� ������������ �����

		VDbl MeanSpecTrans;		// ������� ������ ����. ������
		VDbl MeanSpecMSCTrans;	// ������� ������ ��� ����������������� ���������
		VDbl MeanSpecStdTrans;	// c�������. ���������� ��� �������� ��������������� ������

		VDbl MeanSpec;			// ������� ������ ������������� ������
		VDbl MeanSpecMSC;		// ������� ������ ��� ����������������� ��������� ������������� ������
		VDbl MeanSpecStd;		// c�������. ���������� ��� �������� ������������� ������

		MDbl Factors;			// �������������� �������
	};

	// ������� ������ ������������� �������
	struct PRMtdQltElement
	{
		int Type;				// ��� ��������
		int ModelInd;			// ���������� ����� ������ (��� ���� MTDDBQltFormMean)
	};

	// ���������� � ������ ������������� �������
	struct PRMethodQlt
	{
		CString MethodName;		// ��� ������

		CTime DateCreated;		// ���� ��������
		int DevNum;				// �������� ����� �������
		int DevType;			// ��� ������� ��� ID0
		int SpecRangeLow;		// ������ ������� ������������� ��������� ������� �������
		int SpecRangeUpper;		// ������� ������� ������������� ��������� ������� �������
		int ResSub;				// ���������� ����������� ������
		int ApodSub;			// ���������� ����������� ������
		int ZeroFill;			// ���������� ������
		bool UseRef;			// ������� ������������� �������� ������
		int ResRef;				// ���������� �������� ������
		int ApodRef;			// ���������� �������� ������
		int SmplScan;			// ����� ������ ����������
		int SmplMeas;			// ����� ��������� ����������
		int StdScan;			// ����� ������ �������
		int StdMeas;			// ����� ��������� �������
		bool Xtransform;		// ������� �-�������������
		bool UseBath;			// ����� ������������� ������������ �������/������
		double BathLength;		// ����� ������
		int TempLow;			// ������ ������� ��������� ���������� �������
		int TempUpper;			// ������� ������� ��������� ���������� �������
		int TempDiff;			// ���������� ���������� �� ����������� ���. �����

		vector<PRMtdQltModel> MtdQltModels;	// ������ ������� ������
		vector<PRMtdQltElement> Formula;	// ������ ������������� �������
	};

}

#endif
