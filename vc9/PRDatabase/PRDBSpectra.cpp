#include "../stdafx.h"

#include "PRDBSpectra.h"
#include "..\file4spa.h"
#include "..\FileSystem.h"

using namespace filesystem;
using namespace file4spa;

#define VerifySuccess if (res != LumexDB::enSuccess) { assert(false); return false; }
#define VerifySuccessAndNoData if (res != LumexDB::enSuccess && res != LumexDB::enNoData) { assert(false); return false; }
#define VerifyDataPointer if (pData == NULL) {	assert(false); return false; }
#define StringLen (int)(pData->m_Size / sizeof(TCHAR) + 1)
#define BufferLen (pData->m_Size + sizeof(TCHAR))



CPRDBSpectra::CPRDBSpectra() : CLDatabase()
{
}

CPRDBSpectra::~CPRDBSpectra()
{
}

bool CPRDBSpectra::ApjProjectAddEx(CString ApjProjectName_OLD, const PRApjProject *const pApj)
{
	if(IsApjProjectExists(ApjProjectName_OLD))
		return projectUpdate(ApjProjectName_OLD, pApj);
	else
		return projectAdd(pApj);
}


bool CPRDBSpectra::ApjProjectDelete(CString ApjProjectName)
{
	vector<int> SpecIDs;
	if(!GetApjProjectSpectraID(ApjProjectName, SpecIDs))
	{
		assert(false);
		return false;
	}
	if(!DeleteSpectraFiles(&SpecIDs))
	{
		assert(false);
		return false;
	}

	m_sSQL.Format(_T(APJDB_SQL_DELETE_APJ), ApjProjectName);
	return executeSQL();
}

bool CPRDBSpectra::IsApjProjectExists(CString ApjProjectName)
{
	m_sSQL.Format(_T(APJDB_SQL_ISEXISTS_APJ), replaceChar(ApjProjectName));
	return isDataExist() > 0;
}

bool CPRDBSpectra::IsApjSampleExists(CString SampleName, long lApj)
{
	m_sSQL.Format(_T(APJDB_SQL_ISEXISTS_APJ_SAMPLE), replaceChar(SampleName), lApj);
	return isDataExist() > 0;
}

bool CPRDBSpectra::IsApjSpectrumExists(int SpecNum, long lSam)
{
	m_sSQL.Format(_T(APJDB_SQL_ISEXISTS_APJ_SPECTRA), SpecNum, lSam);
	return isDataExist() > 0;
}

bool CPRDBSpectra::ApjSampleAddEx(CString ApjProjectName, CString SampleName_OLD, const PRApjSample *const pSam)
{
	m_sSQL.Format(_T(APJDB_SQL_GET_ID_APJ), replaceChar(ApjProjectName));
	long lApj = isDataExist();
	if(lApj <= 0)
		return false;

	if(IsApjSampleExists(SampleName_OLD, lApj))
		return sampleUpdate(SampleName_OLD, lApj, pSam);
	else
		return sampleAdd(lApj, pSam);
}

bool CPRDBSpectra::ApjSampleDelete(CString ApjProjectName, CString SampleName)
{
	vector<int> SpecIDs;
	if(!GetSampleSpectraID(ApjProjectName, SampleName, SpecIDs))
	{
		assert(false);
		return false;
	}
	if(!DeleteSpectraFiles(&SpecIDs))
	{
		assert(false);
		return false;
	}

	m_sSQL.Format(_T(APJDB_SQL_DELETE_APJ_SAMPLE), SampleName, ApjProjectName);
	return executeSQL();
}

bool CPRDBSpectra::SetRefData(CString ApjProjectName, CString SampleName, CString ParamName, double Value, bool IsValue)
{
	m_sSQL.Format(_T(APJDB_SQL_GET_ID_APJ), replaceChar(ApjProjectName));
	long lApj = isDataExist();
	if(lApj <= 0)
		return false;

	m_sSQL.Format(_T(APJDB_SQL_GET_ID_APJ_SAMPLE), replaceChar(SampleName), lApj);
	long lSam = isDataExist();
	if(lSam <= 0)
		return false;

	m_sSQL.Format(_T(APJDB_SQL_GET_ID_APJ_PARAM), replaceChar(ParamName), lApj);
	long lPar = isDataExist();
	if(lPar <= 0)
		return false;


	m_sSQL.Format(_T(APJDB_SQL_SET_DATA_APJ_REFDATA), floatWithPoint(Value), IsValue, lPar, lSam);
	if(!executeSQL())
		return false;

	return true;
}

bool CPRDBSpectra::ApjSpectrumAdd(CString ApjProjectName, CString SampleName, const PRApjSpectrum *const pSpec)
{
	m_sSQL.Format(_T(APJDB_SQL_GET_ID_APJ), replaceChar(ApjProjectName));
	long lApj = isDataExist();
	if(lApj <= 0)
		return false;

	m_sSQL.Format(_T(APJDB_SQL_GET_ID_APJ_SAMPLE), replaceChar(SampleName), lApj);
	long lSam = isDataExist();
	if(lSam <= 0)
		return false;

	if(IsApjSpectrumExists(pSpec->SpecNum, lSam))
		return false;
	else
		return spectrumAdd(lSam, pSpec);
}

bool CPRDBSpectra::ApjSpectrumDelete(CString ApjProjectName, CString SampleName, int SpecNum)
{
	vector<int> SpecIDs;
	m_sSQL.Format(_T(APJDB_SQL_GET_ID_APJ_SPECTRA), SpecNum, replaceChar(SampleName), replaceChar(ApjProjectName));
	long lSpecID = isDataExist();
	if(lSpecID <= 0)
		return false;

	SpecIDs.push_back(lSpecID);
	if(!DeleteSpectraFiles(&SpecIDs))
		return false;

	m_sSQL.Format(_T(APJDB_SQL_DELETE_APJ_SPECTRA), SpecNum, replaceChar(SampleName), replaceChar(ApjProjectName));
	return executeSQL();
}

int CPRDBSpectra::GetSpectrumNumMax(CString ApjProjectName, CString SampleName)
{
	m_sSQL.Format(_T(APJDB_SQL_GET_NUM_MAX_APJ_SPECTRA), replaceChar(SampleName), replaceChar(ApjProjectName));
	long res = isDataExist();

	return (int(res));
}

bool CPRDBSpectra::ApjProjectsRead()
{
	m_sSQL.Format(_T(APJDB_SQL_READ_APJ));
	return selectSQL();
}

bool CPRDBSpectra::ApjProjectGet(PRApjProject *const pApj)
{
	if(m_pDataSet == NULL)
	{
		assert(false);
		return false;
	}

	LumexDB::CDataPtr<ILumexDBDataSet::TColumnData> pData = m_pDataSet->GetColumn(0);
	VerifyDataPointer;	
	long lApj;
	LumexDB::EnOperationResult res = pData->GetData(&lApj, sizeof(lApj));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(1);
	VerifyDataPointer;	
	LPTSTR lpBuf = pApj->ProjectName.GetBuffer(StringLen);	
	res = pData->GetData(lpBuf, BufferLen, false);
	pApj->ProjectName.ReleaseBuffer();
	VerifySuccess;
	returnChar(pApj->ProjectName);

	pData = m_pDataSet->GetColumn(2);
	VerifyDataPointer;
	TIMESTAMP_STRUCT date;
	res = pData->GetData(&date, sizeof(date));
	VerifySuccess;		
	pApj->DateCreated = CTime(date.year, date.month, date.day, date.hour, date.minute, date.second);

	pData = m_pDataSet->GetColumn(3);
	VerifyDataPointer;		
	res = pData->GetData(&pApj->DevNum, sizeof(pApj->DevNum));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(4);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->DevType, sizeof(pApj->DevType));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(5);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->SpecRangeLow, sizeof(pApj->SpecRangeLow));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(6);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->SpecRangeUpper, sizeof(pApj->SpecRangeUpper));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(7);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->ResSub, sizeof(pApj->ResSub));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(8);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->ApodSub, sizeof(pApj->ApodSub));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(9);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->ZeroFill, sizeof(pApj->ZeroFill));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(10);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->UseRef, sizeof(pApj->UseRef));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(11);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->ResRef, sizeof(pApj->ResRef));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(12);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->ApodRef, sizeof(pApj->ApodRef));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(13);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->SmplScan, sizeof(pApj->SmplScan));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(14);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->SmplMeas, sizeof(pApj->SmplMeas));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(15);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->StdScan, sizeof(pApj->StdScan));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(16);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->StdMeas, sizeof(pApj->StdMeas));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(17);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->Xtransform, sizeof(pApj->Xtransform));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(18);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->IsSST, sizeof(pApj->IsSST));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(19);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->UseBath, sizeof(pApj->UseBath));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(20);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->BathLength, sizeof(pApj->BathLength));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(21);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->TempLow, sizeof(pApj->TempLow));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(22);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->TempUpper, sizeof(pApj->TempUpper));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(23);
	VerifyDataPointer;		
	res = pData->GetData(&pApj->TempDiff, sizeof(pApj->TempDiff));
	VerifySuccess;


	CString sql;
	ILumexDBDataSet *pDataSet;

	// ��������� �����������
	{
		sql.Format(_T(APJDB_SQL_READ_APJ_PARAM), lApj);
		
		res = m_pDB->ExecuteSQL(sql, &pDataSet);
		if (res != LumexDB::enSuccess || pDataSet == NULL)
		{
			assert(false);
			return false;
		}

		pApj->Params.clear();

		while(!pDataSet->IsEOF())
		{
			PRApjParam newParam;

			pData = pDataSet->GetColumn(0);
			VerifyDataPointer;	
			lpBuf = newParam.ParamName.GetBuffer(StringLen);	
			res = pData->GetData(lpBuf, BufferLen, false);
			newParam.ParamName.ReleaseBuffer();
			VerifySuccess;
			returnChar(newParam.ParamName);

			pData = pDataSet->GetColumn(1);
			VerifyDataPointer;	
			lpBuf = newParam.Unit.GetBuffer(StringLen);	
			res = pData->GetData(lpBuf, BufferLen, false);
			newParam.Unit.ReleaseBuffer();
			VerifySuccess;
			returnChar(newParam.Unit);

			pData = pDataSet->GetColumn(2);
			VerifyDataPointer;	
			lpBuf = newParam.Format.GetBuffer(StringLen);	
			res = pData->GetData(lpBuf, BufferLen, false);
			newParam.Format.ReleaseBuffer();
			VerifySuccess;
			returnChar(newParam.Format);

			pData = pDataSet->GetColumn(3);
			VerifyDataPointer;		
			res = pData->GetData(&newParam.StdMois, sizeof(newParam.StdMois));
			VerifySuccess;

			pData = pDataSet->GetColumn(4);
			VerifyDataPointer;		
			res = pData->GetData(&newParam.UseStdMois, sizeof(newParam.UseStdMois));
			VerifySuccess;

			pApj->Params.push_back(newParam);

			res = pDataSet->NextRow();
			VerifySuccessAndNoData;
		}
		pDataSet->Release();
	}

	// ��������� ��������
	{
		sql.Format(_T(APJDB_SQL_READ_APJ_SAMPLE), lApj);
		
		res = m_pDB->ExecuteSQL(sql, &pDataSet);
		if (res != LumexDB::enSuccess || pDataSet == NULL)
		{
			assert(false);
			return false;
		}

		pApj->Samples.clear();

		while(!pDataSet->IsEOF())
		{
			PRApjSample newSample;

			int lSam;
			pData = pDataSet->GetColumn(0);
			VerifyDataPointer;		
			res = pData->GetData(&lSam, sizeof(lSam));
			VerifySuccess;

			pData = pDataSet->GetColumn(1);
			VerifyDataPointer;	
			lpBuf = newSample.SampleName.GetBuffer(StringLen);	
			res = pData->GetData(lpBuf, BufferLen, false);
			newSample.SampleName.ReleaseBuffer();
			VerifySuccess;
			returnChar(newSample.SampleName);

			pData = pDataSet->GetColumn(2);
			VerifyDataPointer;	
			lpBuf = newSample.Iden2.GetBuffer(StringLen);	
			res = pData->GetData(lpBuf, BufferLen, false);
			newSample.Iden2.ReleaseBuffer();
			VerifySuccess;
			returnChar(newSample.Iden2);

			pData = pDataSet->GetColumn(3);
			VerifyDataPointer;	
			lpBuf = newSample.Iden3.GetBuffer(StringLen);	
			res = pData->GetData(lpBuf, BufferLen, false);
			newSample.Iden3.ReleaseBuffer();
			VerifySuccess;
			returnChar(newSample.Iden3);


			CString sql2;
			ILumexDBDataSet *pDataSet2;

			// ��������� ����������� ������
			{
				sql2.Format(_T(APJDB_SQL_READ_APJ_REFDATA), lSam);
				
				res = m_pDB->ExecuteSQL(sql2, &pDataSet2);
				if(res != LumexDB::enSuccess || pDataSet2 == NULL)
				{
					assert(false);
					return false;
				}

				while(!pDataSet2->IsEOF())
				{
					PRApjRefData newRef;

					pData = pDataSet2->GetColumn(0);
					VerifyDataPointer;	
					lpBuf = newRef.ParamName.GetBuffer(StringLen);	
					res = pData->GetData(lpBuf, BufferLen, false);
					newRef.ParamName.ReleaseBuffer();
					VerifySuccess;
					returnChar(newRef.ParamName);

					pData = pDataSet2->GetColumn(1);
					VerifyDataPointer;		
					res = pData->GetData(&newRef.RefData, sizeof(newRef.RefData));
					VerifySuccess;

					pData = pDataSet2->GetColumn(2);
					VerifyDataPointer;		
					res = pData->GetData(&newRef.IsDataExists, sizeof(newRef.IsDataExists));
					VerifySuccess;

					newSample.RefData.push_back(newRef);

					res = pDataSet2->NextRow();
					VerifySuccessAndNoData;
				}
				pDataSet2->Release();
			}

			// ��������� ��������
			{
				sql2.Format(_T(APJDB_SQL_READ_APJ_SPECTRA), lSam);
				
				res = m_pDB->ExecuteSQL(sql2, &pDataSet2);
				if(res != LumexDB::enSuccess || pDataSet2 == NULL)
				{
					assert(false);
					return false;
				}

				while(!pDataSet2->IsEOF())
				{
					PRApjSpectrum newSpec;

					pData = pDataSet2->GetColumn(0);
					VerifyDataPointer;		
					res = pData->GetData(&newSpec.SpecNum, sizeof(newSpec.SpecNum));
					VerifySuccess;

					pData = pDataSet2->GetColumn(1);
					VerifyDataPointer;
					TIMESTAMP_STRUCT date;
					res = pData->GetData(&date, sizeof(date));
					VerifySuccess;		
					newSpec.DateCreated = CTime(date.year, date.month, date.day, date.hour, date.minute, date.second);

					newSample.Spectra.push_back(newSpec);

					res = pDataSet2->NextRow();
					VerifySuccessAndNoData;
				}
				pDataSet2->Release();
			}

			pApj->Samples.push_back(newSample);

			res = pDataSet->NextRow();
			VerifySuccessAndNoData;
		}
		pDataSet->Release();
	}

	return true;
}

bool CPRDBSpectra::ApjProjectGetWithSpecData(PRApjProject *const pApj)
{
	if(m_pDataSet == NULL)
	{
		assert(false);
		return false;
	}

	LumexDB::CDataPtr<ILumexDBDataSet::TColumnData> pData = m_pDataSet->GetColumn(0);
	VerifyDataPointer;	
	long lApj;
	LumexDB::EnOperationResult res = pData->GetData(&lApj, sizeof(lApj));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(1);
	VerifyDataPointer;	
	LPTSTR lpBuf = pApj->ProjectName.GetBuffer(StringLen);	
	res = pData->GetData(lpBuf, BufferLen, false);
	pApj->ProjectName.ReleaseBuffer();
	VerifySuccess;
	returnChar(pApj->ProjectName);

	pData = m_pDataSet->GetColumn(2);
	VerifyDataPointer;
	TIMESTAMP_STRUCT date;
	res = pData->GetData(&date, sizeof(date));
	VerifySuccess;		
	pApj->DateCreated = CTime(date.year, date.month, date.day, date.hour, date.minute, date.second);

	pData = m_pDataSet->GetColumn(3);
	VerifyDataPointer;		
	res = pData->GetData(&pApj->DevNum, sizeof(pApj->DevNum));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(4);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->DevType, sizeof(pApj->DevType));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(5);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->SpecRangeLow, sizeof(pApj->SpecRangeLow));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(6);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->SpecRangeUpper, sizeof(pApj->SpecRangeUpper));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(7);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->ResSub, sizeof(pApj->ResSub));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(8);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->ApodSub, sizeof(pApj->ApodSub));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(9);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->ZeroFill, sizeof(pApj->ZeroFill));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(10);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->UseRef, sizeof(pApj->UseRef));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(11);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->ResRef, sizeof(pApj->ResRef));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(12);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->ApodRef, sizeof(pApj->ApodRef));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(13);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->SmplScan, sizeof(pApj->SmplScan));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(14);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->SmplMeas, sizeof(pApj->SmplMeas));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(15);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->StdScan, sizeof(pApj->StdScan));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(16);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->StdMeas, sizeof(pApj->StdMeas));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(17);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->Xtransform, sizeof(pApj->Xtransform));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(18);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->IsSST, sizeof(pApj->IsSST));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(19);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->UseBath, sizeof(pApj->UseBath));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(20);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->BathLength, sizeof(pApj->BathLength));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(21);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->TempLow, sizeof(pApj->TempLow));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(22);
	VerifyDataPointer;	
	res = pData->GetData(&pApj->TempUpper, sizeof(pApj->TempUpper));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(23);
	VerifyDataPointer;		
	res = pData->GetData(&pApj->TempDiff, sizeof(pApj->TempDiff));
	VerifySuccess;


	CString sql;
	ILumexDBDataSet *pDataSet;

	// ��������� �����������
	{
		sql.Format(_T(APJDB_SQL_READ_APJ_PARAM), lApj);
		
		res = m_pDB->ExecuteSQL(sql, &pDataSet);
		if (res != LumexDB::enSuccess || pDataSet == NULL)
		{
			assert(false);
			return false;
		}

		pApj->Params.clear();

		while(!pDataSet->IsEOF())
		{
			PRApjParam newParam;

			pData = pDataSet->GetColumn(0);
			VerifyDataPointer;	
			lpBuf = newParam.ParamName.GetBuffer(StringLen);	
			res = pData->GetData(lpBuf, BufferLen, false);
			newParam.ParamName.ReleaseBuffer();
			VerifySuccess;
			returnChar(newParam.ParamName);

			pData = pDataSet->GetColumn(1);
			VerifyDataPointer;	
			lpBuf = newParam.Unit.GetBuffer(StringLen);	
			res = pData->GetData(lpBuf, BufferLen, false);
			newParam.Unit.ReleaseBuffer();
			VerifySuccess;
			returnChar(newParam.Unit);

			pData = pDataSet->GetColumn(2);
			VerifyDataPointer;	
			lpBuf = newParam.Format.GetBuffer(StringLen);	
			res = pData->GetData(lpBuf, BufferLen, false);
			newParam.Format.ReleaseBuffer();
			VerifySuccess;
			returnChar(newParam.Format);

			pData = pDataSet->GetColumn(3);
			VerifyDataPointer;		
			res = pData->GetData(&newParam.StdMois, sizeof(newParam.StdMois));
			VerifySuccess;

			pData = pDataSet->GetColumn(4);
			VerifyDataPointer;		
			res = pData->GetData(&newParam.UseStdMois, sizeof(newParam.UseStdMois));
			VerifySuccess;

			pApj->Params.push_back(newParam);

			res = pDataSet->NextRow();
			VerifySuccessAndNoData;
		}
		pDataSet->Release();
	}

	// ��������� ��������
	{
		sql.Format(_T(APJDB_SQL_READ_APJ_SAMPLE), lApj);
		
		res = m_pDB->ExecuteSQL(sql, &pDataSet);
		if (res != LumexDB::enSuccess || pDataSet == NULL)
		{
			assert(false);
			return false;
		}

		pApj->Samples.clear();

		while(!pDataSet->IsEOF())
		{
			PRApjSample newSample;

			int lSam;
			pData = pDataSet->GetColumn(0);
			VerifyDataPointer;		
			res = pData->GetData(&lSam, sizeof(lSam));
			VerifySuccess;

			pData = pDataSet->GetColumn(1);
			VerifyDataPointer;	
			lpBuf = newSample.SampleName.GetBuffer(StringLen);	
			res = pData->GetData(lpBuf, BufferLen, false);
			newSample.SampleName.ReleaseBuffer();
			VerifySuccess;
			returnChar(newSample.SampleName);

			pData = pDataSet->GetColumn(2);
			VerifyDataPointer;	
			lpBuf = newSample.Iden2.GetBuffer(StringLen);	
			res = pData->GetData(lpBuf, BufferLen, false);
			newSample.Iden2.ReleaseBuffer();
			VerifySuccess;
			returnChar(newSample.Iden2);

			pData = pDataSet->GetColumn(3);
			VerifyDataPointer;	
			lpBuf = newSample.Iden3.GetBuffer(StringLen);	
			res = pData->GetData(lpBuf, BufferLen, false);
			newSample.Iden3.ReleaseBuffer();
			VerifySuccess;
			returnChar(newSample.Iden3);


			CString sql2;
			ILumexDBDataSet *pDataSet2;

			// ��������� ����������� ������
			{
				sql2.Format(_T(APJDB_SQL_READ_APJ_REFDATA), lSam);
				
				res = m_pDB->ExecuteSQL(sql2, &pDataSet2);
				if(res != LumexDB::enSuccess || pDataSet2 == NULL)
				{
					assert(false);
					return false;
				}

				while(!pDataSet2->IsEOF())
				{
					PRApjRefData newRef;

					pData = pDataSet2->GetColumn(0);
					VerifyDataPointer;	
					lpBuf = newRef.ParamName.GetBuffer(StringLen);	
					res = pData->GetData(lpBuf, BufferLen, false);
					newRef.ParamName.ReleaseBuffer();
					VerifySuccess;
					returnChar(newRef.ParamName);

					pData = pDataSet2->GetColumn(1);
					VerifyDataPointer;		
					res = pData->GetData(&newRef.RefData, sizeof(newRef.RefData));
					VerifySuccess;

					pData = pDataSet2->GetColumn(2);
					VerifyDataPointer;		
					res = pData->GetData(&newRef.IsDataExists, sizeof(newRef.IsDataExists));
					VerifySuccess;

					newSample.RefData.push_back(newRef);

					res = pDataSet2->NextRow();
					VerifySuccessAndNoData;
				}
				pDataSet2->Release();
			}

			// ��������� ��������
			{
				sql2.Format(_T(APJDB_SQL_READ_APJ_SPECTRA_2), lSam);
				
				res = m_pDB->ExecuteSQL(sql2, &pDataSet2);
				if(res != LumexDB::enSuccess || pDataSet2 == NULL)
				{
					assert(false);
					return false;
				}

				while(!pDataSet2->IsEOF())
				{
					PRApjSpectrum newSpec;
					int lSp;

					pData = pDataSet2->GetColumn(0);
					VerifyDataPointer;		
					res = pData->GetData(&lSp, sizeof(lSp));
					VerifySuccess;

					pData = pDataSet2->GetColumn(1);
					VerifyDataPointer;		
					res = pData->GetData(&newSpec.SpecNum, sizeof(newSpec.SpecNum));
					VerifySuccess;

					pData = pDataSet2->GetColumn(2);
					VerifyDataPointer;
					TIMESTAMP_STRUCT date;
					res = pData->GetData(&date, sizeof(date));
					VerifySuccess;		
					newSpec.DateCreated = CTime(date.year, date.month, date.day, date.hour, date.minute, date.second);

					CString Path = GetPathToSpec(lSp);
					if(!IsExistFile(Path))
					{
						assert(false);
						return false;
					}

					newSpec.Data.clear();
					bool res2 = ReadSpaDataFromFile(Path, newSpec.Data);
					if(!res2)
					{
						assert(false);
						return false;
					}

					newSample.Spectra.push_back(newSpec);

					res = pDataSet2->NextRow();
					VerifySuccessAndNoData;
				}
				pDataSet2->Release();
			}

			pApj->Samples.push_back(newSample);

			res = pDataSet->NextRow();
			VerifySuccessAndNoData;
		}
		pDataSet->Release();
	}

	return true;
}

bool CPRDBSpectra::ApjSpectrumDataGet(CString ApjProjectName, CString SampleName, PRApjSpectrum *const pSpec)
{
	m_sSQL.Format(_T(APJDB_SQL_GET_ID_APJ_SPECTRA), pSpec->SpecNum, replaceChar(SampleName), replaceChar(ApjProjectName));
	long lSpec = isDataExist();
	if(lSpec <= 0)
		return false;

	CString Path = GetPathToSpec(lSpec);
	if(!IsExistFile(Path))
	{
		assert(false);
		return false;
	}

	pSpec->Data.clear();
	bool res = ReadSpaDataFromFile(Path, pSpec->Data);
	if(!res)
	{
		assert(false);
		return false;
	}

	return true;
}

bool CPRDBSpectra::projectAdd(const PRApjProject *const pApj)
{
	m_sSQL = _T(APJDB_SQL_GET_ID_MAX_APJ);
	long lApj = 1 + isDataExist();
	if(lApj <= 0)
		return false;

	m_sSQL.Format(_T(APJDB_SQL_INSERT_APJ), lApj, replaceChar(pApj->ProjectName), pApj->DateCreated.Format(m_pszFormatCTime),
		pApj->DevNum, pApj->DevType, pApj->SpecRangeLow, pApj->SpecRangeUpper, pApj->ResSub, pApj->ApodSub,
		pApj->ZeroFill, pApj->UseRef, pApj->ResRef, pApj->ApodRef, pApj->SmplScan, pApj->SmplMeas, pApj->StdScan,
		pApj->StdMeas, pApj->Xtransform, pApj->IsSST, pApj->UseBath, floatWithPoint(pApj->BathLength),
		pApj->TempLow, pApj->TempUpper, pApj->TempDiff);

	if(!executeSQL())
		return false;

	return projectAddVect(lApj, pApj);
}

bool CPRDBSpectra::projectUpdate(CString ApjProjectName, const PRApjProject *const pApj)
{
	m_sSQL.Format(_T(APJDB_SQL_GET_ID_APJ), replaceChar(ApjProjectName));
	long lApj = isDataExist();
	if(lApj <= 0)
		return false;

	m_sSQL.Format(_T(APJDB_SQL_UPDATE_APJ), replaceChar(pApj->ProjectName), pApj->DateCreated.Format(m_pszFormatCTime),
		pApj->DevNum, pApj->DevType, pApj->SpecRangeLow, pApj->SpecRangeUpper, pApj->ResSub, pApj->ApodSub,
		pApj->ZeroFill, pApj->UseRef, pApj->ResRef, pApj->ApodRef, pApj->SmplScan, pApj->SmplMeas, pApj->StdScan,
		pApj->StdMeas, pApj->Xtransform, pApj->IsSST, pApj->UseBath, floatWithPoint(pApj->BathLength),
		pApj->TempLow, pApj->TempUpper, pApj->TempDiff, replaceChar(ApjProjectName));

	if(!executeSQL())
		return false;

	m_sSQL.Format(_T(APJDB_SQL_DELETE_ALL_APJ_PARAM), lApj);
	if(!executeSQL())
		return false;

	m_sSQL.Format(_T(APJDB_SQL_DELETE_ALL_APJ_SAMPLE), lApj);
	if(!executeSQL())
		return false;

	return projectAddVect(lApj, pApj);
}

bool CPRDBSpectra::projectAddVect(long lApj, const PRApjProject *const pApj)
{
	for(vector<PRApjParam>::const_iterator itP=pApj->Params.begin(); itP!=pApj->Params.end(); ++itP)
	{
		m_sSQL = _T(APJDB_SQL_GET_ID_MAX_APJ_PARAM);
		long lPar = 1 + isDataExist();
		if(lPar <= 0)
			return false;

		m_sSQL.Format(_T(APJDB_SQL_INSERT_APJ_PARAM), lPar, lApj, replaceChar(itP->ParamName),
			replaceChar(itP->Unit), replaceChar(itP->Format), floatWithPoint(itP->StdMois), itP->UseStdMois);
		if(!executeSQL())
			return false;
	}

	for(vector<PRApjSample>::const_iterator itS=pApj->Samples.begin(); itS!=pApj->Samples.end(); ++itS)
	{
		m_sSQL = _T(APJDB_SQL_GET_ID_MAX_APJ_SAMPLE);
		long lSam = 1 + isDataExist();
		if(lSam <= 0)
			return false;

		m_sSQL.Format(_T(APJDB_SQL_INSERT_APJ_SAMPLE), lSam, lApj, replaceChar(itS->SampleName),
			replaceChar(itS->Iden2), replaceChar(itS->Iden3));
		if(!executeSQL())
			return false;

		for(std::vector<PRApjRefData>::const_iterator itR=itS->RefData.begin(); itR!=itS->RefData.end(); ++itR) 
		{
			m_sSQL = _T(APJDB_SQL_GET_ID_MAX_APJ_REFDATA);
			long lRef = 1 + isDataExist();
			if(lRef <= 0)
				return false;

			m_sSQL.Format(_T(APJDB_SQL_GET_ID_APJ_PARAM), replaceChar(itR->ParamName), lApj);
			long lPar = isDataExist();
			if(lPar <= 0)
				return false;

			m_sSQL.Format(_T(APJDB_SQL_INSERT_APJ_REFDATA), lRef, lPar, lSam, floatWithPoint(itR->RefData),
				itR->IsDataExists);
			if(!executeSQL())
				return false;
		}
	}

	return true;
}

bool CPRDBSpectra::sampleAdd(long lApj, const PRApjSample *const pSam)
{
	m_sSQL = _T(APJDB_SQL_GET_ID_MAX_APJ_SAMPLE);
	long lSam = 1 + isDataExist();
	if(lSam <= 0)
		return false;

	m_sSQL.Format(_T(APJDB_SQL_INSERT_APJ_SAMPLE), lSam, lApj, replaceChar(pSam->SampleName),
		replaceChar(pSam->Iden2), replaceChar(pSam->Iden3));
	if(!executeSQL())
		return false;

	return sampleAddVect(lApj, lSam, pSam);
}

bool CPRDBSpectra::sampleUpdate(CString SampleName_OLD, long lApj, const PRApjSample *const pSam)
{
	m_sSQL.Format(_T(APJDB_SQL_GET_ID_APJ_SAMPLE), replaceChar(SampleName_OLD), lApj);
	long lSam = isDataExist();
	if(lSam <= 0)
		return false;

	m_sSQL.Format(_T(APJDB_SQL_UPDATE_APJ_SAMPLE), replaceChar(pSam->SampleName), replaceChar(pSam->Iden2),
		replaceChar(pSam->Iden3), lSam, lApj);
	if(!executeSQL())
		return false;

	m_sSQL.Format(_T(APJDB_SQL_DELETE_ALL_APJ_REFDATA), lSam);
	if(!executeSQL())
		return false;

	m_sSQL.Format(_T(APJDB_SQL_DELETE_ALL_APJ_SPECTRA), lSam);
	if(!executeSQL())
		return false;

	return sampleAddVect(lApj, lSam, pSam);
}

bool CPRDBSpectra::sampleAddVect(long lApj, long lSam, const PRApjSample *const pSam)
{
	for(std::vector<PRApjRefData>::const_iterator itR=pSam->RefData.begin(); itR!=pSam->RefData.end(); ++itR) 
	{
		m_sSQL = _T(APJDB_SQL_GET_ID_MAX_APJ_REFDATA);
		long lRef = 1 + isDataExist();
		if(lRef <= 0)
			return false;

		m_sSQL.Format(_T(APJDB_SQL_GET_ID_APJ_PARAM), replaceChar(itR->ParamName), lApj);
		long lPar = isDataExist();
		if(lPar <= 0)
			return false;

		m_sSQL.Format(_T(APJDB_SQL_INSERT_APJ_REFDATA), lRef, lPar, lSam, floatWithPoint(itR->RefData),
			itR->IsDataExists);
		if(!executeSQL())
			return false;
	}

	return true;
}

bool CPRDBSpectra::spectrumAdd(long lSam, const PRApjSpectrum *const pSpec)
{
	m_sSQL = _T(APJDB_SQL_GET_ID_MAX_APJ_SPECTRA);
	long lSpec = 1 + isDataExist();
	if(lSpec <= 0)
		return false;

	m_sSQL.Format(_T(APJDB_SQL_INSERT_APJ_SPECTRA), lSpec, lSam, pSpec->SpecNum,
		pSpec->DateCreated.Format(m_pszFormatCTime));
	if(!executeSQL())
		return false;

	bool res = CheckDirectory();
	if(res)
	{
		CString Path = GetPathToSpec(lSpec);
		if(IsExistFile(Path))
			DeleteFileByPath(Path);

		res = SaveSpaDataAtFile(Path, pSpec->Data);
	}

	return res;
}

bool CPRDBSpectra::GetApjProjectSpectraID(CString ApjProjectName, std::vector<int>& SpecIDs)
{
	CString sql;
	ILumexDBDataSet *pDataSet;

	sql.Format(_T(APJDB_SQL_GET_SAMNAMES_ID_APJ_SPECTRA), replaceChar(ApjProjectName));
	LumexDB::EnOperationResult res = m_pDB->ExecuteSQL(sql, &pDataSet);
	if (res != LumexDB::enSuccess || pDataSet == NULL)
	{
		assert(false);
		return false;
	}

	while(!pDataSet->IsEOF())
	{
		CString SamName;
		LumexDB::CDataPtr<ILumexDBDataSet::TColumnData> pData = pDataSet->GetColumn(0);
		LPTSTR lpBuf = SamName.GetBuffer(StringLen);	
		LumexDB::EnOperationResult res = pData->GetData(lpBuf, BufferLen, false);
		SamName.ReleaseBuffer();
		VerifySuccess;
		returnChar(SamName);

		GetSampleSpectraID(ApjProjectName, SamName, SpecIDs);

		res = pDataSet->NextRow();
		VerifySuccessAndNoData;
	}
	pDataSet->Release();

	return true;
}

bool CPRDBSpectra::GetSampleSpectraID(CString ApjProjectName, CString SampleName, std::vector<int>& SpecIDs)
{
	CString sql;
	ILumexDBDataSet *pDataSet;

	sql.Format(_T(APJDB_SQL_GET_SAMSPECTRA_ID_APJ_SPECTRA), replaceChar(SampleName), replaceChar(ApjProjectName));
	LumexDB::EnOperationResult res = m_pDB->ExecuteSQL(sql, &pDataSet);
	if (res != LumexDB::enSuccess || pDataSet == NULL)
	{
		assert(false);
		return false;
	}

	while(!pDataSet->IsEOF())
	{
		LumexDB::CDataPtr<ILumexDBDataSet::TColumnData> pData = pDataSet->GetColumn(0);
		VerifyDataPointer;
		int lSpec;
		LumexDB::EnOperationResult res = pData->GetData(&lSpec, sizeof(lSpec));
		VerifySuccess;

		SpecIDs.push_back(lSpec);

		res = pDataSet->NextRow();
		VerifySuccessAndNoData;
	}
	pDataSet->Release();

	return true;
}

bool CPRDBSpectra::DeleteSpectraFiles(const std::vector<int> *const SpecIDs)
{
	for(std::vector<int>::const_iterator it=SpecIDs->begin(); it!=SpecIDs->end(); ++it)
	{
		CString Path = GetPathToSpec(*it);
		if(IsExistFile(Path))
			if(!DeleteFileByPath(Path))
				return false;
	}

	return true;
}

bool CPRDBSpectra::CheckDirectory()
{
	CString Path = ExtractDirPath(m_sDB);
	CString Base = ExtractFileNameWithoutExt(m_sDB);
	CString Dir = _T("APJX_") + Base;
	CString Full = MakePath(Path, Dir);

	if(IsExistDirectory(Full))
		return true;

	return EnsureDirectory(Full);
}

CString CPRDBSpectra::GetPathToSpec(int iSp)
{
	CString Path = ExtractDirPath(m_sDB);
	CString Base = ExtractFileNameWithoutExt(m_sDB);
	CString Dir = _T("APJX_") + Base;
	CString Name;
	Name.Format(_T("%1d.dat"), iSp);
	CString Full = MakePath(Path, Dir, Name);

	return Full;
}

void CPRDBSpectra::createTables( std::vector<CString> *pV )
{
	CString tmp;

	tmp.Format(_T(APJDB_SQL_CREATE_TAB_APJ), APJDBProjectName);
	pV->push_back(tmp);
	tmp.Format(_T(APJDB_SQL_CREATE_TAB_APJ_PARAM), APJDBParamName, APJDBParamUnit, APJDBParamFormat);
	pV->push_back(tmp);
	tmp.Format(_T(APJDB_SQL_CREATE_TAB_APJ_SAMPLE), APJDBSampleName, APJDBSampleIden2, APJDBSampleIden3);
	pV->push_back(tmp);
	tmp.Format(_T(APJDB_SQL_CREATE_TAB_APJ_REFDATA));
	pV->push_back(tmp);
	tmp.Format(_T(APJDB_SQL_CREATE_TAB_APJ_SPECTRA));
	pV->push_back(tmp);
}

long CPRDBSpectra::getCurrentVersion()
{
	return APJDB_CURRENT_VERSION;
}

CString CPRDBSpectra::getCurrentDBName()
{
	return _T("APJPROJECT_DATABASE_LUMEX_NIOKR");
}
