#ifndef _PARCEL_PRDB_PROJECT_H_
#define _PARCEL_PRDB_PROJECT_H_

#pragma once

#include "LDatabase/LDatabase.h"
#include "PRDBProjectSQL.h"
#include "PRDBProjectDefType.h"

using namespace prdb;

class CPRDBProject : public CLDatabase
{
public:
	CPRDBProject( void );
	virtual ~CPRDBProject( void );

	bool SetOptions(PRUser &userIdentity, int iHistoryActivity, bool fFailIfHistoryFolse, bool bIsOpen );

	// �������� ����� �� � �� ��������
	bool CreateDatabase( LPCTSTR );

	// �������� ��
	bool Open( LPCTSTR lpszFilePath, bool IsNeedCheck = false );

	// �������� ��
	void Close();

	////////////////////////////////////////
	//				HISTORY				  //
	////////////////////////////////////////

	bool GetHistory( vector<PRHistory> *pVHistory, bool bJustLast );
	bool GetHistory(PRUser * pUserIdentity, vector<PRHistory> *pVHistory, bool bJustLast );

	////////////////////////////////////////
	//				������				  //
	////////////////////////////////////////

	
    	// ��������� ����� ������, ��� ���������, ���� ��� ��� �����������
	bool ApplianceAddEx( int ApplianceSN_OLD, const PRAppliance *const );

		// ������� ������������ ������
	bool ApplianceDelete( int ApplianceSN );

		// ������ � ������� ����������?
	bool IsApplianceExist( int ApplianceSN );

		// ������� ���������� � ���� ��������
	bool ApplianceRead();

		// �������� ���������� � �������
	bool ApplianceGet(PRAppliance *const );


	////////////////////////////////////////
	//				������				  //
	////////////////////////////////////////

		// �������� ������ � �������, ��� ���������, ���� �����������
	bool ProjectAddEx( int ApplianceSN, CString ProjectName_OLD, const PRAnalisisProject *const );
	
		// ������ � ������� ����������?
	bool IsProjectExist( int ApplianceSN, CString ProjectName );

		// ������� ������
	bool ProjectDelete( int ApplianceSN, CString ProjectName );

		// ������� ���������� � �������� ������� �������
	bool ProjectRead( int ApplianceSN );
    
		// �������� ���������� � �������
	bool ProjectGet(PRAnalisisProject *const );


	////////////////////////////////////////
	//			  ����������			  //
	////////////////////////////////////////

		// �������� / update ���������� � �������
	bool IndexAddEx( int ApplianceSN, CString ProjectName, CString IndexName_OLD, const PRIndex *const );

		// ���������� ����������?
	bool IsIndexExist( int ApplianceSN, CString ProjectName, CString IndexName );

		// ������� ����������
	bool IndexDelete( int ApplianceSN, CString ProjectName, CString IndexName );

		// ��������� ���������� �������
	bool IndicesRead( int ApplianceSN, CString ProjectName );

		// �������� ���������� �� ��������� �����������
	bool IndexGet(PRIndex *const );


	////////////////////////////////////////
	//	  ������ ������������ ��������	  //
	////////////////////////////////////////

		// �������� / update ����� � �������
	bool TransAddEx( int ApplianceSN, CString ProjectName, CString TransName_OLD, const PRTrans *const );

		// ����� ����������?
	bool IsTransExist( int ApplianceSN, CString ProjectName, CString TransName );

		// ������� �����
	bool TransDelete( int ApplianceSN, CString ProjectName, CString TransName );

		// ��������� ������ �������
	bool TransRead( int ApplianceSN, CString ProjectName );

		// �������� ���������� �� ��������� �������
	bool TransGet(PRTrans *const );


	////////////////////////////////////////
	//			  �������				  //
	////////////////////////////////////////

		// �������� / update ������� � �������
	bool SampleAddEx( int ApplianceSN, CString ProjectName, CString TransName, CString Sample1_OLD, const PRSample *const );

		// ������� ����������?
	bool IsSampleExist( int ApplianceSN, CString ProjectName, CString TransName, CString Sample1 );

		// ID
	bool SampleGetID( int ApplianceSN, CString ProjectName, CString TransName, CString Sample1, int *pID );

		// ������� �������
	bool SampleDelete( int ApplianceSN, CString ProjectName, CString TransName, CString Sample1 );

		// ��������� ������� �������
	bool SamplesRead( int ApplianceSN, CString ProjectName, CString TransName );

		// �������� ���������� �� ��������� ��������
	bool SampleGet(PRSample *const );

	////////////////////////////////////////
	//		  ����������� ������		  //
	////////////////////////////////////////

		// �������� / update ������� � �������
	bool RefDataAddEx( int ApplianceSN, CString ProjectName, CString TransName, CString Sample1, const PRRefData *const );
	
		// ������� ����������?
	bool IsRefDataExist( int ApplianceSN, CString ProjectName, CString TransName, CString Sample1, CString IndexName );

		// ������� ����������� ������
	bool RefDataDelete( int ApplianceSN, CString ProjectName, CString TransName, CString Sample1, CString IndexName );

		// ��������� ������� �������
	bool RefDataRead( int ApplianceSN, CString ProjectName, CString TransName, CString Sample1 );
	bool RefDataReadBySampleID( int SampleID );

		// �������� ���������� �� ��������� ��������
	bool RefDataGet(PRRefData *const );

	////////////////////////////////////////
	//				������				  //
	////////////////////////////////////////

		// �������� ����� ������. ������ ����������, ���� ��������� ��� ��� ������������ ������
	bool SpectrumAddEx( int ApplianceSN, CString ProjectName, CString TransName, CString Sample1, const PRSpectrum *const );

		// ������� ������
	bool SpectrumDelete( int ApplianceSN, CString ProjectName, CString TransName, CString Sample1, int Num );

		// �������� ����� �������� � ���������
	bool SpectrumLinkGet( int ApplianceSN, CString ProjectName, CString TransName, CString Sample1, vector<int> *const );

		// ������ ����������
	bool IsSpectrumExist( int ApplianceSN, CString ProjectName, CString TransName, CString Sample1, int spNum );

		// �������� ������ ������� �� ��� ������ ( PRSpectrum::SpecNum ������ ���� �������� )
	bool SpectrumGet( int ApplianceSN, CString ProjectName, CString TransName, CString Sample1, PRSpectrum *const );
	bool SpectrumGet( int ApplianceSN, CString ProjectName, CString TransName, CString Sample1, PRSpectrumLite *const );

		// �������� ������ ������� �� SpecID
	bool SpectrumGetByID( int nSpecID, PRSpectrum *const );
	bool SpectrumGetByID( int nSpecID, PRSpectrumLite *const );

	////////////////////////////////////////
	//				������				  //
	////////////////////////////////////////

		// �������� / update ������
	bool ModelAddEx( int ApplianceSN, CString ProjectName, CString ModelName_OLD, const PRModel *const );

		// ������ ����������
	bool IsModelExist( int ApplianceSN, CString ProjectName, CString ModelName );

		// ������� ������
	bool ModelDelete( int ApplianceSN, CString ProjectName, CString ModelName );

		// ��������� ������ �������
	bool ModelRead( int ApplianceSN, CString ProjectName );

		// �������� ������ �������
	bool ModelGet(PRModel *const );

	////////////////////////////////////////
	//				�����				  //
	////////////////////////////////////////

		// �������� �����
	bool MethodAddEx( int ApplianceSN, CString ProjectName, CString MethodName_OLD, const PRMethod *const );

		// ����� ����������?
	bool IsMethodExist( int ApplianceSN, CString ProjectName, CString MethodName );

		// ������� �����
	bool MethodDelete( int ApplianceSN, CString ProjectName, CString MethodName );

		// ��������� ������ �������
	bool MethodRead( int ApplianceSN, CString ProjectName );

		// �������� ������ �������
	bool MethodGet(PRMethod *const );

	
//protected:

	bool addHistoryEvent( bool bPrevRes, int hType, int hTarget );
	bool setUserIdentity(PRUser * pUserIdentity );
	long checkUser(PRUser *pUserIdentity );
	long checkRole(PRRoles *pRole );

	long			m_user_identity;
	int				m_history_activity;
	int				m_history_one_per_one;
	bool			m_bFailIfHistoryFolse;
	bool			m_bSetIdentity;

	PRUser	m_user;

	using CLDatabase::CreateDatabase;
	using CLDatabase::Open;
	using CLDatabase::Close;
    	
	virtual void createTables( vector<CString> * );
	virtual long getCurrentVersion();
	virtual CString getCurrentDBName();

	bool sampleGetID(  int ApplianceSN, CString ProjectName, CString TransName, CString Sample1, int *pID);
	bool spectrumGet( int ApplianceSN, CString ProjectName, CString Sample1, CString TransName, bool bFull, PRSpectrum *const, PRSpectrumLite *const );
	bool spectrumGetByID( int nSpecID, bool bFull, PRSpectrum *const, PRSpectrumLite *const );
	bool applianceAdd( const PRAppliance *const );
	bool applianceUpdate( int ApplianceSN, const PRAppliance *const );
	bool projectAdd( int ApplianceSN, const PRAnalisisProject *const );
	bool projectUpdate( int ApplianceSN, CString ProjectName, const PRAnalisisProject *const );
	bool indexAdd( int ApplianceSN, CString ProjectName, const PRIndex *const );
	bool indexUpdate( int ApplianceSN, CString ProjectName, CString IndexName, const PRIndex *const );
	bool transAdd( int ApplianceSN, CString ProjectName, const PRTrans *const );
	bool transAddVect(long TransID, const PRTrans *const );
	bool transUpdate( int ApplianceSN, CString ProjectName, CString TransName, const PRTrans *const );
	bool sampleAdd( int ApplianceSN, CString ProjectName, CString TransName, const PRSample *const );
	bool sampleUpdate( int ApplianceSN, CString ProjectName, CString TransName, CString Sample1, const PRSample *const );
	bool refDataAdd( int ApplianceSN, CString ProjectName, CString TransName, CString Sample1, const PRRefData *const );
	bool refDataUpdate( int ApplianceSN, CString ProjectName, CString TransName, CString Sample1, const PRRefData *const );
	bool spectrumAdd( int ApplianceSN, CString ProjectName, CString TransName, CString Sample1, const PRSpectrum *const );
	bool spectrumUpdateUse( int ApplianceSN, CString ProjectName, CString TransName, CString Sample1, const PRSpectrum *const );
	bool modelAdd( int ApplianceSN, CString ProjectName, const PRModel *const );
	bool modelAddVect( int ApplianceSN, CString ProjectName, long ModelID, const PRModel *const );
	bool modelUpdate( int ApplianceSN, CString ProjectName, CString ModelName_OLD, const PRModel *const );
	bool methodAdd( int ApplianceSN, CString ProjectName, const PRMethod *const );
	bool methodAddVect( int ApplianceSN, CString ProjectName, long MethodID, const PRMethod *const );
	bool methodUpdate( int ApplianceSN, CString ProjectName, CString MethodName_OLD, const PRMethod *const );

	bool GetApplianceSpectraID(int ApplianceSNe, vector<int>& SpecIDs);
	bool GetProjectSpectraID(int ApplianceSN, CString ProjectName, vector<int>& SpecIDs);
	bool GetTransSpectraID(int ApplianceSN, CString ProjectName, CString TransName, vector<int>& SpecIDs);
	bool GetSampleSpectraID(int ApplianceSN, CString ProjectName, CString TransName, CString SamName, vector<int>& SpecIDs);
	bool GetSampleSpectraID(int nSampleID, vector<int>& SpecIDs);
	bool DeleteSpectraFiles(const vector<int> *const SpecIDs);
	bool CheckDirectory();
	CString GetPathToSpec(int iSp);
};

#endif