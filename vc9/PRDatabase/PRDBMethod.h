#ifndef _PRDB_METHOD_H_
#define _PRDB_METHOD_H_

#pragma once

#include "LDatabase/LDatabase.h"
#include "PRDBMethodSQL.h"
#include "PRDBMethodDefType.h"

using namespace mtddb;

// Метод количественного анализа

class CPRDBMethodQnt : public CLDatabase
{
public:

	CPRDBMethodQnt( void );
	virtual ~CPRDBMethodQnt( void );

	bool MethodAddEx(CString MethodName_OLD, const PRMethodQnt *const );
	bool MethodDelete(CString MethodName);
	bool MethodsRead();
	bool MethodGet(PRMethodQnt *const);
	bool SetMtdModelCorr(CString MethodName, CString ParamName, CString ModelName, double CorrB, double CorrA);

protected:

	bool IsMethodExists(CString MethodName);

	bool methodAdd(const PRMethodQnt *const);
	bool methodUpdate(CString MethodName, const PRMethodQnt *const );
	bool methodAddVect(long lMtd, const PRMethodQnt *const pMtd);

	virtual void createTables(std::vector<CString>*);
	virtual long getCurrentVersion();
	virtual CString getCurrentDBName();	

};

// Метод качественного анализа

class CPRDBMethodQlt : public CLDatabase
{
public:

	CPRDBMethodQlt( void );
	virtual ~CPRDBMethodQlt( void );

	bool MethodAddEx(CString MethodName_OLD, const PRMethodQlt *const );
	bool MethodDelete(CString MethodName);
	bool MethodsRead();
	bool MethodGet(PRMethodQlt *const);

protected:

	bool IsMethodExists(CString MethodName);

	bool methodAdd(const PRMethodQlt *const);
	bool methodUpdate(CString MethodName, const PRMethodQlt *const );
	bool methodAddVect(long lMtd, const PRMethodQlt *const pMtd);

	virtual void createTables(std::vector<CString>*);
	virtual long getCurrentVersion();
	virtual CString getCurrentDBName();	

};

#endif _PRDB_METHOD_H_
