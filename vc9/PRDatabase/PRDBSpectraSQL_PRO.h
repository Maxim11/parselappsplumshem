#ifndef _PARCEL_PRDB_SPECTRA_SQL_PRO_H_
#define _PARCEL_PRDB_SPECTRA_SQL_PRO_H_

#pragma once

//////////////////////////////////////////////////////////
// ��������� �������

// SQL-������ - �������� ������ �������� ������� � ����
#define APJDB_SQL_GET_ID_APJ \
"SELECT ApjProjectID\n\
FROM ApjProjects\n\
WHERE ApjProjectName = '%s'"

// SQL-������ - ��������� ��������� �������
#define APJDB_SQL_READ_APJ \
"SELECT ApjProjectID,\n\
ApjProjectName,\n\
DateCreated,\n\
DevNum,\n\
DevType,\n\
SpecRangeLow,\n\
SpecRangeUpper,\n\
ResSub,\n\
ApodSub,\n\
ZeroFill,\n\
UseRef,\n\
ResRef,\n\
ApodRef,\n\
SmplScan,\n\
SmplMeas,\n\
StdScan,\n\
StdMeas,\n\
Xtransform,\n\
IsSST,\n\
UseBath,\n\
BathLength,\n\
TempLow,\n\
TempUpper,\n\
TempDiff\n\
FROM ApjProjects"

//////////////////////////////////////////////////////////
// ����������

// SQL-������ - �������� ������ ���������� � ����
#define APJDB_SQL_GET_ID_APJ_PARAM \
"SELECT ParamID\n\
FROM Params\n\
WHERE ParamName = '%s' AND ApjProjectID = %d"

// SQL-������ - ��������� ���������� � ����������
#define APJDB_SQL_READ_APJ_PARAM \
"SELECT ParamName,\n\
Unit,\n\
Format,\n\
StdMois,\n\
UseStdMois\n\
FROM Params\n\
WHERE ApjProjectID = %d"

//////////////////////////////////////////////////////////
// �������

// SQL-������ - �������� ����� �������
#define APJDB_SQL_INSERT_APJ_SAMPLE \
"INSERT INTO Samples(\n\
SampleID,\n\
ApjProjectID,\n\
SampleName,\n\
Iden2,\n\
Iden3)\n\
VALUES (%d, %d, '%s', '%s', '%s')"

// SQL-������ - �������� ���������� �� �������
#define APJDB_SQL_UPDATE_APJ_SAMPLE \
"UPDATE Samples\n\
SET SampleName = '%s',\n\
Iden2 = '%s',\n\
Iden3 = '%s'\n\
WHERE SampleID = %d AND ApjProjectID = %d"

// SQL-������ - ������� �������
#define APJDB_SQL_DELETE_APJ_SAMPLE \
"DELETE FROM Samples\n\
WHERE SampleName = '%s' AND ApjProjectID = (SELECT ApjProjectID FROM ApjProjects WHERE ApjProjectName = '%s')"

// SQL-������ - ��������� ������� ������� � ����
#define APJDB_SQL_ISEXISTS_APJ_SAMPLE \
"SELECT COUNT(*)\n\
FROM Samples\n\
WHERE SampleName = '%s' AND ApjProjectID = %d"

// SQL-������ - �������� ������������ ������ �������� � ����
#define APJDB_SQL_GET_ID_MAX_APJ_SAMPLE \
"SELECT MAX(SampleID)\n\
FROM Samples"

// SQL-������ - �������� ������ ������� � ����
#define APJDB_SQL_GET_ID_APJ_SAMPLE \
"SELECT SampleID\n\
FROM Samples\n\
WHERE SampleName = '%s' AND ApjProjectID = %d"

// SQL-������ - ��������� ���������� �� �������
#define APJDB_SQL_READ_APJ_SAMPLE \
"SELECT SampleID,\n\
SampleName,\n\
Iden2,\n\
Iden3\n\
FROM Samples\n\
WHERE ApjProjectID = %d"

//////////////////////////////////////////////////////////
// ����������� ������

// SQL-������ - �������� ����������� ������
#define APJDB_SQL_INSERT_APJ_REFDATA \
"INSERT INTO RefDatas(\n\
RefDataID,\n\
ParamID,\n\
SampleID,\n\
Data,\n\
IsDataExist)\n\
VALUES (%d, %d, %d, %s, %d)"

// SQL-������ - ������� ��� ����������� ������ �������
#define APJDB_SQL_DELETE_ALL_APJ_REFDATA \
"DELETE FROM RefDatas\n\
WHERE SampleID = %d"

// SQL-������ - ������� ����������� ������
#define APJDB_SQL_SET_DATA_APJ_REFDATA \
"UPDATE RefDatas\n\
SET Data = %s,\n\
IsDataExist = %d\n\
WHERE ParamID = %d AND SampleID = %d"

// SQL-������ - �������� ������������ ������ ��������� � ����
#define APJDB_SQL_GET_ID_MAX_APJ_REFDATA \
"SELECT MAX(RefDataID)\n\
FROM RefDatas"

// SQL-������ - ��������� ���������� � ����������� ������
#define APJDB_SQL_READ_APJ_REFDATA \
"SELECT p.ParamName,\n\
r.Data,\n\
r.IsDataExist\n\
FROM RefDatas AS r\n\
LEFT OUTER JOIN Params AS p\n\
ON r.ParamID = p.ParamID\n\
WHERE SampleID = %d"

//////////////////////////////////////////////////////////
// ������

// SQL-������ - �������� ����� ������
#define APJDB_SQL_INSERT_APJ_SPECTRA \
"INSERT INTO Spectra(\n\
SpectrumID,\n\
SampleID,\n\
SpecNum,\n\
DateCreated)\n\
VALUES (%d, %d, %d, #%s#)"

// SQL-������ - ������� ������
#define APJDB_SQL_DELETE_APJ_SPECTRA \
"DELETE FROM Spectra\n\
WHERE SpecNum = %d AND SampleID = (SELECT SampleID FROM Samples WHERE SampleName = '%s' AND ApjProjectID = (SELECT ApjProjectID FROM ApjProjects WHERE ApjProjectName = '%s'))"

// SQL-������ - ������� ��� ������� �������
#define APJDB_SQL_DELETE_ALL_APJ_SPECTRA \
"DELETE FROM Spectra\n\
WHERE SampleID = %d"

// SQL-������ - ��������� ������� ������� � ����
#define APJDB_SQL_ISEXISTS_APJ_SPECTRA \
"SELECT COUNT(*)\n\
FROM Spectra\n\
WHERE SpecNum = %d AND SampleID = %d"

// SQL-������ - �������� ������������ ������ �������� � ����
#define APJDB_SQL_GET_ID_MAX_APJ_SPECTRA \
"SELECT MAX(SpectrumID)\n\
FROM Spectra"

// SQL-������ - �������� ������ ������ � ����
#define APJDB_SQL_GET_ID_APJ_SPECTRA \
"SELECT SpectrumID\n\
FROM Spectra\n\
WHERE SpecNum = %d AND SampleID = (SELECT SampleID FROM Samples WHERE SampleName = '%s' AND ApjProjectID = (SELECT ApjProjectID FROM ApjProjects WHERE ApjProjectName = '%s'))"

// SQL-������ - �������� ������� ���� �������� �������
#define APJDB_SQL_GET_SAMSPECTRA_ID_APJ_SPECTRA \
"SELECT SpectrumID\n\
FROM Spectra\n\
WHERE SampleID = (SELECT SampleID FROM Samples WHERE SampleName = '%s' AND ApjProjectID = (SELECT ApjProjectID FROM ApjProjects WHERE ApjProjectName = '%s'))"

// SQL-������ - �������� ������������ ����� �������� �������
#define APJDB_SQL_GET_NUM_MAX_APJ_SPECTRA \
"SELECT MAX(SpecNum)\n\
FROM Spectra\n\
WHERE SampleID = (SELECT SampleID FROM Samples WHERE SampleName = '%s' AND ApjProjectID = (SELECT ApjProjectID FROM ApjProjects WHERE ApjProjectName = '%s'))"

// SQL-������ - ��������� ���������� � �������
#define APJDB_SQL_READ_APJ_SPECTRA \
"SELECT SpecNum,\n\
DateCreated\n\
FROM Spectra\n\
WHERE SampleID = %d"


#endif
