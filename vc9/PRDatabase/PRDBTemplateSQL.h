#ifndef _PARCEL_PRDB_TEMPLATE_SQL_H_
#define _PARCEL_PRDB_TEMPLATE_SQL_H_

#pragma once

// ������� � ������� �������� �����������

#define PRDB_SQL_TMPL_CREATE_TAB_TMPL \
"CREATE TABLE Templates(\n\
TemplateID		COUNTER		PRIMARY KEY,\n\
TemplateName	TEXT(%d)	NOT NULL,\n\
TemplateNote	TEXT(%d)	NOT NULL,\n\
DateCreated		DATETIME	NOT NULL,\n\
NCompMean		INT			NOT NULL,\n\
HiperMean		FLOAT		NOT NULL,\n\
NHarmMean		INT			NOT NULL,\n\
KfnlMean		FLOAT		NOT NULL,\n\
SpecLeftMean	INT			NOT NULL,\n\
SpecRightMean	INT			NOT NULL,\n\
LastCrit		INT			NOT NULL,\n\
PrjSpecMin		INT			NOT NULL,\n\
PrjSpecMax		INT			NOT NULL,\n\
PrjResol		INT			NOT NULL,\n\
PrjZeroFilling	INT			NOT NULL,\n\
CONSTRAINT PRTMPL UNIQUE (TemplateName))"

#define PRDB_SQL_TMPL_INSERT_TMPL \
"INSERT INTO Templates (\n\
TemplateID,\n\
TemplateName,\n\
TemplateNote,\n\
DateCreated,\n\
NCompMean,\n\
HiperMean,\n\
NHarmMean,\n\
KfnlMean,\n\
SpecLeftMean,\n\
SpecRightMean,\n\
LastCrit,\n\
PrjSpecMin,\n\
PrjSpecMax,\n\
PrjResol,\n\
PrjZeroFilling)\n\
VALUES (%d, '%s', '%s', #%s#, %d, %s, %d, %s, %d, %d, %d, %d, %d, %d, %d)"

#define PRDB_SQL_TMPL_UPDATE_TMPL \
"UPDATE Templates\n\
SET TemplateName = '%s',\n\
TemplateNote = '%s',\n\
DateCreated = #%s#,\n\
NCompMean = %d,\n\
HiperMean = %s,\n\
NHarmMean = %d,\n\
KfnlMean = %s,\n\
SpecLeftMean = %d,\n\
SpecRightMean = %d,\n\
LastCrit = %d,\n\
PrjSpecMin = %d,\n\
PrjSpecMax = %d,\n\
PrjResol = %d,\n\
PrjZeroFilling = %d\n\
WHERE TemplateName = '%s'"

#define PRDB_SQL_TMPL_DELETE_TMPL \
"DELETE FROM Templates WHERE TemplateName = '%s'"

#define PRDB_SQL_TMPL_ISEXISTS_TMPL \
"SELECT COUNT(*)\n\
FROM Templates\n\
WHERE TemplateName = '%s'"

#define PRDB_SQL_TMPL_GET_TMPL_ID_MAX \
"SELECT MAX(TemplateID)\n\
FROM Templates"

#define PRDB_SQL_TMPL_GET_TMPL_ID \
"SELECT TemplateID\n\
FROM Templates\n\
WHERE TemplateName = '%s'"

#define PRDB_SQL_TMPL_READ_TMPL \
"SELECT TemplateID,\n\
TemplateName,\n\
TemplateNote,\n\
DateCreated,\n\
NCompMean,\n\
HiperMean,\n\
NHarmMean,\n\
KfnlMean,\n\
SpecLeftMean,\n\
SpecRightMean,\n\
LastCrit,\n\
PrjSpecMin,\n\
PrjSpecMax,\n\
PrjResol,\n\
PrjZeroFilling\n\
FROM Templates"

// ������� � ������� ����� �����������

#define PRDB_SQL_TMPL_CREATE_TAB_STEP \
"CREATE TABLE Steps(\n\
StepID			COUNTER		PRIMARY KEY,\n\
TemplateID		INT			NOT NULL REFERENCES Templates(TemplateID) ON UPDATE CASCADE ON DELETE CASCADE,\n\
StepNum			INT			NOT NULL,\n\
ModelType		INT			NOT NULL,\n\
SpType			INT			NOT NULL,\n\
NCompPlus		INT			NOT NULL,\n\
NCompStep		INT			NOT NULL,\n\
HiperPlus		FLOAT		NOT NULL,\n\
HiperStep		FLOAT		NOT NULL,\n\
NHarmPlus		INT			NOT NULL,\n\
NHarmStep		INT			NOT NULL,\n\
NHarmType		INT			NOT NULL,\n\
KfnlPlus		FLOAT		NOT NULL,\n\
KfnlStep		FLOAT		NOT NULL,\n\
KfnlType		INT			NOT NULL,\n\
SpecLeftPlus	INT			NOT NULL,\n\
SpecLeftStep	INT			NOT NULL,\n\
SpecRightPlus	INT			NOT NULL,\n\
SpecRightStep	INT			NOT NULL,\n\
MaxModels		INT			NOT NULL,\n\
MainCrit		INT			NOT NULL,\n\
SortCrit		INT			NOT NULL,\n\
IsTest			BIT			NOT NULL,\n\
Operation		INT			NOT NULL,\n\
CONSTRAINT PRSTEP UNIQUE (TemplateID, StepNum))"

#define PRDB_SQL_TMPL_INSERT_STEP \
"INSERT INTO Steps(\n\
TemplateID,\n\
StepNum,\n\
ModelType,\n\
SpType,\n\
NCompPlus,\n\
NCompStep,\n\
HiperPlus,\n\
HiperStep,\n\
NHarmPlus,\n\
NHarmStep,\n\
KfnlPlus,\n\
KfnlStep,\n\
SpecLeftPlus,\n\
SpecLeftStep,\n\
SpecRightPlus,\n\
SpecRightStep,\n\
MaxModels,\n\
MainCrit,\n\
SortCrit,\n\
IsTest,\n\
Operation,\n\
NHarmType,\n\
KfnlType)\n\
VALUES (%d, %d, %d, %d, %d, %d, %s, %s, %d, %d, %s, %s, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d)"

#define PRDB_SQL_TMPL_DELETE_STEP_ALL \
"DELETE FROM Steps\n\
WHERE TemplateID = %d"

#define PRDB_SQL_TMPL_READ_STEP \
"SELECT s.StepID,\n\
s.StepNum,\n\
s.ModelType,\n\
s.SpType,\n\
s.NCompPlus,\n\
s.NCompStep,\n\
s.HiperPlus,\n\
s.HiperStep,\n\
s.NHarmPlus,\n\
s.NHarmStep,\n\
s.KfnlPlus,\n\
s.KfnlStep,\n\
s.SpecLeftPlus,\n\
s.SpecLeftStep,\n\
s.SpecRightPlus,\n\
s.SpecRightStep,\n\
s.MaxModels,\n\
s.MainCrit,\n\
s.SortCrit,\n\
s.IsTest,\n\
s.Operation,\n\
p.PreprocMax,\n\
p.PreprocBest,\n\
p.PreprocAdd,\n\
p.IsA,\n\
p.IsB,\n\
p.IsM,\n\
p.IsD,\n\
p.IsC,\n\
p.IsN,\n\
p.Is1,\n\
p.Is2,\n\
p.IsNo,\n\
p.IsUse,\n\
s.NHarmType,\n\
s.KfnlType,\n\
o.LimMah,\n\
o.LimSEC,\n\
o.LimSECV,\n\
o.LimBOUND,\n\
o.IsMah,\n\
o.IsSEC,\n\
o.IsSECV,\n\
o.IsBad,\n\
o.IsBound\n\
FROM (Preprocs AS p\n\
LEFT OUTER JOIN Steps AS s\n\
ON p.StepID = s.StepID)\n\
LEFT OUTER JOIN Outs AS o\n\
ON s.StepID = o.StepID\n\
WHERE TemplateID = %d\n\
ORDER BY StepNum"
/*
FROM (Indices AS i\n\
LEFT OUTER JOIN Transes AS t\n\
ON i.AProjectID = t.AProjectID)\n\
LEFT OUTER JOIN Samples AS s\n\
ON t.TransID = s.TransID\n\
*/
// ������� � ������� �������������

#define PRDB_SQL_TMPL_CREATE_TAB_PREPS \
"CREATE TABLE Preprocs(\n\
PreprocID		COUNTER		PRIMARY KEY,\n\
StepID			INT			NOT NULL REFERENCES Steps(StepID) ON UPDATE CASCADE ON DELETE CASCADE,\n\
PreprocMax		INT			NOT NULL,\n\
PreprocBest		INT			NOT NULL,\n\
PreprocAdd		TEXT(%d)	NOT NULL,\n\
IsA				BIT			NOT NULL,\n\
IsB				BIT			NOT NULL,\n\
IsM				BIT			NOT NULL,\n\
IsD				BIT			NOT NULL,\n\
IsC				BIT			NOT NULL,\n\
IsN				BIT			NOT NULL,\n\
Is1				BIT			NOT NULL,\n\
Is2				BIT			NOT NULL,\n\
IsNo			BIT			NOT NULL,\n\
IsUse			BIT			NOT NULL,\n\
CONSTRAINT PRPREPS UNIQUE (StepID))"

#define PRDB_SQL_TMPL_INSERT_PREPS \
"INSERT INTO Preprocs (\n\
StepID,\n\
PreprocMax,\n\
PreprocBest,\n\
PreprocAdd,\n\
IsA,\n\
IsB,\n\
IsM,\n\
IsD,\n\
IsC,\n\
IsN,\n\
Is1,\n\
Is2,\n\
IsNo,\n\
IsUse)\n\
SELECT StepID, %d, %d, '%s', %d, %d, %d, %d, %d, %d, %d, %d, %d, %d\n\
FROM Steps WHERE TemplateID = %d AND StepNum = %d"

#define PRDB_SQL_TMPL_DELETE_PREPS \
"DELETE FROM Preprocs\n\
WHERE StepID = %d"

#define PRDB_SQL_TMPL_READ_PREPS \
"SELECT PreprocMax,\n\
PreprocBest,\n\
PreprocAdd,\n\
IsA,\n\
IsB,\n\
IsM,\n\
IsD,\n\
IsC,\n\
IsN,\n\
Is1,\n\
Is2,\n\
IsNo,\n\
IsUse\n\
FROM Preprocs\n\
WHERE StepID = %d"


// ������� � ������� ���������� ������� �� �������

#define PRDB_SQL_TMPL_CREATE_TAB_OUTS \
"CREATE TABLE Outs(\n\
OutID			COUNTER		PRIMARY KEY,\n\
StepID			INT			NOT NULL REFERENCES Steps(StepID) ON UPDATE CASCADE ON DELETE CASCADE,\n\
LimMah			FLOAT		NOT NULL,\n\
LimSEC			FLOAT		NOT NULL,\n\
LimSECV			FLOAT		NOT NULL,\n\
LimBound		FLOAT		NOT NULL,\n\
IsMah			BIT			NOT NULL,\n\
IsSEC			BIT			NOT NULL,\n\
IsSECV			BIT			NOT NULL,\n\
IsBad			BIT			NOT NULL,\n\
IsBound			BIT			NOT NULL,\n\
CONSTRAINT PRPREPS UNIQUE (StepID))"

#define PRDB_SQL_TMPL_INSERT_OUTS \
"INSERT INTO Outs (\n\
StepID,\n\
LimMah,\n\
LimSEC,\n\
LimSECV,\n\
LimBound,\n\
IsMah,\n\
IsSEC,\n\
IsSECV,\n\
IsBad,\n\
IsBound)\n\
SELECT StepID, %s, %s, %s, %s, %d, %d, %d, %d, %d\n\
FROM Steps WHERE TemplateID = %d AND StepNum = %d"

#define PRDB_SQL_TMPL_DELETE_OUTS \
"DELETE FROM Outs\n\
WHERE StepID = %d"

#define PRDB_SQL_TMPL_READ_OUTS \
"SELECT LimMah,\n\
LimSEC,\n\
LimSECV,\n\
LimBound,\n\
IsMah,\n\
IsSEC,\n\
IsSECV,\n\
IsBad,\n\
IsBound\n\
FROM Outs\n\
WHERE StepID = %d"

// ������� � ������� ����������� �����

#define PRDB_SQL_TMPL_CREATE_TAB_SPEXC \
"CREATE TABLE SpExclude (\n\
SpExcID			COUNTER		PRIMARY KEY,\n\
TemplateID		INT			NOT NULL REFERENCES Templates(TemplateID) ON UPDATE CASCADE ON DELETE CASCADE,\n\
SpDataExclude	INT			NOT NULL)"

#define PRDB_SQL_TMPL_INSERT_SPEXC \
"INSERT INTO SpExclude (\n\
TemplateID,\n\
SpDataExclude)\n\
VALUES (%d, %d)"

#define PRDB_SQL_TMPL_DELETE_SPEXC \
"DELETE FROM SpExclude\n\
WHERE TemplateID = %d"

#define PRDB_SQL_TMPL_READ_SPEXC \
"SELECT SpDataExclude\n\
FROM SpExclude\n\
WHERE TemplateID = %d\n\
ORDER BY SpExcID"

#endif _PARCEL_PRDB_TEMPLATE_SQL_H_
