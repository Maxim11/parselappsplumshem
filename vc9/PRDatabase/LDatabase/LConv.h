////////////////////////////////////////////////////////////////////////////
//	Author      : Alekseev Andrey 2006
//	Email       : spblex@yandex.ru
//	Module      : LConv.h
//	Version		: 1.01
//	Description : 
////////////////////////////////////////////////////////////////////////////

#ifndef LCONV_H
#define LCONV_H
#pragma once

namespace lconvert
{
		// �������� ��������� (� �������� ����������) ����������� ������� �����
//	 LPCSTR GetLocalDecimalSeparator();
	TCHAR GetLocalDecimalSeparator(); // ��������

		// ���������� USER_DEFAULT ��� SYSTEM_DEFAULT (���� ������ �� ���������) ����������� ������� �����
	 TCHAR GetSystemDecimalSeparator();

		// ����������� dbl � ������ � ������������ � GetSystemDecimalSeparator � frm
	 CString LocalToSystemDouble( double dbl, LPCTSTR frm = _T("%f") );
}

#endif LCONV_H