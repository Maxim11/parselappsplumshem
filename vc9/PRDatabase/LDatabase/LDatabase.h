////////////////////////////////////////////////////////////////////////////
//	Author      : Alekseev Andrey 2006
//	Email       : spblex@yandex.ru
//	Module      : LDatabase.h
//	Version		: 1.21
//	Description : 
////////////////////////////////////////////////////////////////////////////

#ifndef LDATABASE_H
#define LDATABASE_H
#pragma once

#include "afxdb.h"
#include "odbcinst.h"
#include "shlwapi.h"
#include <vector>

#include "LDatabaseSQL.h"

class CLDatabase
{
public:
	CLDatabase( void );
	virtual ~CLDatabase( void );

	////////////////////////////////////////
	//			�������� ��				  //
	////////////////////////////////////////

		// ��������� �� �� ��������� ����.
	bool Open( LPCTSTR lpszFilePath, bool IsNeedCheck = false ) throw();

		// ��������� ��
	bool IsOpen() const throw();

		// �������� ��
	void Close() throw();

		// �������� ����� �� � �� ��������
    bool CreateDatabase( LPCTSTR ) throw();

		// ������� �� ������� ��� �� ���������� ����
	bool DeleteDatabase( LPCTSTR = NULL ) throw();
	


	//-------------���������-------------//

		// ���������� ��������� ������
	void Free() throw();

	
		// ����� ���������� ������
	bool IsEOF() const throw();
	

		// ������� � ��������� ������
	bool MoveNext( int pos = 1 ) throw();

	////////////////////////////////////////
	//									  //
	////////////////////////////////////////
	
		// ������ �������� ��
	long ReadDatabaseVersion() throw();

		// ��� ��
	CString ReadDatabaseName() throw();

	//----------------------------------

	bool CheckDatabase() throw();

	// ����������

	bool BeginTrans();
	bool Rollback();
	bool CommitTrans();
	
protected:		

		//---------------------
		/* �������� SQL-���������� �� �������� ������
			������:
			class TryBD : public CLDatabase
			{
			protected:
				virtual void createTables( std::vector<CString> *pV )
				{
					pV->push_back(_T("CREATE TABLE One1 (F1 INT)"));
					pV->push_back(_T("CREATE TABLE One2 (F2 INT)"));
				}
			};		*/
	virtual void createTables( std::vector<CString> * ) = 0;
	virtual long getCurrentVersion() = 0;
	virtual CString getCurrentDBName() = 0;
		//---------------------


	bool executeSQL();
	bool selectSQL();
	long isDataExist();
	CString replaceChar( CString buffer );
	void returnChar( CString & );	
	CString floatWithPoint( double dbl );
	CString doubleWithPoint( double dbl );

	/* Conductor
	bool			m_IsTransaction;
	/**/
	CString			m_sDB;
	CString			m_sSQL;
	LPCTSTR			m_pszFormatCTime;
	/* Conductor
	CDatabase		m_db;
	CRecordset		m_rst;
	/**/

	/* Conductor */
	ILumexDB *m_pDB;
	ILumexDBDataSet *m_pDataSet;
	/**/
	
private:
	bool openEx();
	bool createFile( LPCTSTR lpszFile, bool *_m_cr );
	bool createStructs();
	bool fillStructures( const std::vector<CString> *const );
	void deleteDatabases( bool );
};

#endif LDATABASE_H