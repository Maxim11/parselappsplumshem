////////////////////////////////////////////////////////////////////////////
//	Author      : Alekseev Andrey 2006
//	Email       : spblex@yandex.ru
//	Module      : LConv.cpp
//	Version		: 1.01
//	Description : 
////////////////////////////////////////////////////////////////////////////

#include "../stdafx.h"
#include "winnls.h"
//#include "locale.h"
#include <locale>
#include ".\lconv.h"

// Геннадий

TCHAR lconvert::GetLocalDecimalSeparator()
{           
#ifdef _UNICODE
      return std::use_facet< std::ctype<wchar_t> >( std::locale() ).widen( *localeconv()->decimal_point );
#else
      return *localeconv()->decimal_point;
#endif
}
/*
LPCSTR lconvert::GetLocalDecimalSeparator()
{		 
	struct lconv *loc = localeconv();
	return loc->decimal_point;
}
*/
TCHAR lconvert::GetSystemDecimalSeparator()
{
	TCHAR Buffer[5];
	if (::GetLocaleInfo( LOCALE_USER_DEFAULT, LOCALE_SDECIMAL | LOCALE_USE_CP_ACP, Buffer, 4 ) > 0)
		return Buffer[0];

	if (::GetLocaleInfo( LOCALE_SYSTEM_DEFAULT, LOCALE_SDECIMAL | LOCALE_USE_CP_ACP, Buffer, 4 ) > 0)
		return Buffer[0];

//	return (TCHAR)_T(",");
	return _T(',');  // Геннадий
}

CString lconvert::LocalToSystemDouble( double prm, LPCTSTR frm )
{
	CString Buffer;
	Buffer.Format( frm, prm );
	Buffer.Replace( /***/lconvert::GetLocalDecimalSeparator(), lconvert::GetSystemDecimalSeparator() );
	return Buffer;
}