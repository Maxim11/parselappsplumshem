////////////////////////////////////////////////////////////////////////////
//	Author      : Alekseev Andrey 2006
//	Email       : spblex@yandex.ru
//	Module      : PRDBProject.cpp
//	Version		: 1.21
//	Description : 
////////////////////////////////////////////////////////////////////////////

#include "../stdafx.h"
#include "LDatabase.h"
#include "LConv.h"
							/* Conductor */
CLDatabase::CLDatabase() : /*m_db(), m_rst(&m_db),*/ m_pDB(NULL), m_pDataSet(NULL)
{
	/* Conductor
	m_IsTransaction = false;
	/**/
	m_pszFormatCTime = _T("%Y/%m/%d %H:%M:%S");
}

CLDatabase::~CLDatabase()
{
	Close();

	/* Conductor */
	if (m_pDataSet != NULL)
	{
		m_pDataSet->Release();
	}

	if (m_pDB != NULL)
	{
		m_pDB->Release();
	}
	/**/
}

////////////////////////////////////////
//			�������� ��				  //
////////////////////////////////////////
bool CLDatabase::Open( LPCTSTR lpszFilePath, bool IsNeedCheck )
{
	m_sDB	= lpszFilePath;

	if ( !openEx() )
		return false;

	if (IsNeedCheck)
	{
		if ( CheckDatabase() )
			return true;
		else
		{
			Close();
			return false;
		}
	}
	return true;
}

void CLDatabase::Close()
{
	/* Conductor
	if ( m_IsTransaction )
		m_db.Rollback();
	Free();
	if( m_db.IsOpen() )
		m_db.Close();
	/**/

	/* Conductor */
	if (m_pDataSet != NULL)
	{
		m_pDataSet->Release();
		m_pDataSet = NULL;
	}

	if (m_pDB != NULL)
	{
		m_pDB->Release();
		m_pDB = NULL;
	}
	/**/
}

bool CLDatabase::IsOpen() const
{
	/* Conductor */
	return m_pDB != NULL ? m_pDB->IsConnected() : false;
	/**/

	/* Conductor
	return m_db.IsOpen() != 0;
	/**/
}

bool CLDatabase::CreateDatabase( LPCTSTR lpszFile )
{
	Close();
	m_sDB	= lpszFile;
	
	bool bPR = false;

		// ������� ����
	if ( !createFile( m_sDB, &bPR ) )
		return false;

		// ������� ���������
	if ( !createStructs() )
	{
		deleteDatabases( bPR );
		return false;
	}
	m_sSQL.Format( _T(LDB_SQL_SET_DB_PR_SYS), getCurrentVersion(), getCurrentDBName() );
	return executeSQL();
}

bool CLDatabase::DeleteDatabase( LPCTSTR lpszFile /*= NULL */)
{
	if ( lpszFile == NULL || lpszFile == m_sDB )
	{
		Close();
		return DeleteFile( m_sDB ) != 0;
	}
	else
		return DeleteFile( lpszFile ) != 0;
}


bool CLDatabase::IsEOF() const
{
	/* Conductor */
	if (m_pDataSet == NULL)
	{
		assert(false);
		return true;
	}

	return m_pDataSet->IsEOF();

	/**/

	/* Conductor
	return m_rst.IsEOF() != 0;
	/**/
}


bool CLDatabase::MoveNext( int pos /* = 1  */)
{
	/* Conductor */
	if (m_pDataSet == NULL)
	{
		return false;
	}

	return m_pDataSet->MoveRelative(pos) == LumexDB::enSuccess;

	/**/

	/* Conductor
	try
	{
		m_rst.Move( pos );
	}
	catch ( CDBException *xcp )
	{
		TRACE(_T("CLDatabase::MoveNext: "));
		TRACE(xcp->m_strError);
		xcp->Delete();
		return false;
	}
	return true;
	/**/
}

////////////////////////////////////////
//				OTHER				  //
////////////////////////////////////////
long CLDatabase::ReadDatabaseVersion()
{
	m_sSQL = _T(LDB_SQL_GET_DB_VERSION);
	if ( !selectSQL() )
		return -1;

	/* Conductor */
	if (m_pDataSet == NULL)
	{
		return -1;
	}

	if (m_pDataSet->FirstRow() != LumexDB::enSuccess)
	{
		return -1;
	}

	LumexDB::CDataPtr<ILumexDBDataSet::TColumnData> pData = m_pDataSet->GetColumn(0);
	if (pData == NULL)
	{
		return -1;
	}

	int nData;
	if (pData->GetData(&nData, sizeof(nData)) != LumexDB::enSuccess)
	{
		return -1;
	}
	
	return nData;

	/**/

	/* Conductor
	CDBVariant var;
	try
	{
		m_rst.GetFieldValue( (short)0, var );
	}
	catch ( CDBException *xcp )
	{
		TRACE(_T("CLDatabase::ReadDatabaseVersion"));
		TRACE(xcp->m_strError);
		xcp->Delete();		
		return -1;
	}	
	return var.m_lVal;
	/**/
}

CString CLDatabase::ReadDatabaseName()
{
	m_sSQL = _T(LDB_SQL_GET_DB_NAME);
	if ( !selectSQL() )
		return _T("ERROR");

	/* Conductor */
	if (m_pDataSet == NULL)
	{
		return _T("ERROR");
	}

	if (m_pDataSet->FirstRow() != LumexDB::enSuccess)
	{
		return _T("ERROR");
	}

	LumexDB::CDataPtr<ILumexDBDataSet::TColumnData> pData = m_pDataSet->GetColumn(0);
	if (pData == NULL)
	{
		return _T("ERROR");
	}

	LPTSTR pText = new TCHAR[pData->m_Size / sizeof(TCHAR) + 1];	
	if (pData->GetData(pText, pData->m_Size + sizeof(TCHAR), false) != LumexDB::enSuccess)
	{
		delete []pText;
		return _T("ERROR");
	}

	CString db_name = pText;
	delete []pText;

	return db_name;

	/**/

	/* Conductor
	try
	{
		CString db_name;
		m_rst.GetFieldValue( (short)0, db_name );
		return db_name;
	}
	catch ( CDBException *xcp )
	{
		TRACE(_T("CLDatabase::ReadDatabaseVersion"));
		TRACE(xcp->m_strError);
		xcp->Delete();		
		return _T("ERROR");
	}
	/**/
}

bool CLDatabase::CheckDatabase()
{
	return ( this->getCurrentDBName() == this->ReadDatabaseName() && this->getCurrentVersion() == this->ReadDatabaseVersion() );
}

void CLDatabase::Free()
{
	/* Conductor */
	if (m_pDataSet)
	{
		m_pDataSet->Release();
		m_pDataSet = NULL;
	}
	/**/

	/* Conductor
	if ( m_rst.IsOpen() )
		m_rst.Close();
	/**/
}

////////////////////////////////////////
//			PROTECTED				  //
////////////////////////////////////////
bool CLDatabase::openEx()
{
	/* Conductor */

	Close();

	m_pDB = ::GetLumexDBManager()->OpenDB(m_sDB);
	return m_pDB == NULL ? false : true;
	/**/
	
	/* Conductor
	m_sSQL.Format( _T("UID=%s;DRIVER={%s};ExtendedANSISQL=%d;DBQ=%s"), _T(LDB_CONN_UID), _T(LDB_CONN_DRV), LDB_CONN_ANSI_SQL92, m_sDB );	
	try
	{
		Close();
		m_db.OpenEx( m_sSQL, CDatabase::noOdbcDialog );
	}
	catch (CDBException *xcp)
	{
		TRACE(_T("CLDatabase::openEx: "));
		TRACE(xcp->m_strError);
		xcp->Delete();
		return false;
	}
	return true;
	/**/
}

void CLDatabase::deleteDatabases( bool bPR )
{
	Close();
	if ( bPR )
		DeleteFile( m_sDB );
}


bool CLDatabase::createFile( LPCTSTR lpszFile, bool *_m_cr )
{
	/* Conductor */
	Close();

	if ( !PathFileExists( lpszFile ) )
	{
		*_m_cr = true;
		m_pDB = ::GetLumexDBManager()->CreateDB(lpszFile);
	}
	else
	{
		m_pDB = ::GetLumexDBManager()->OpenDB(lpszFile);
	}

	return m_pDB == NULL ? false : true;

	/**/

	/* Conductor
	CString m_fpath;
	m_fpath.Format( _T("CREATE_DB=\"%s\"\0"), lpszFile );
	if ( !PathFileExists( lpszFile ) )
	{
		*_m_cr = true;
		if ( !SQLConfigDataSource( NULL, ODBC_ADD_DSN, _T(LDB_CONN_DRV), m_fpath ) )
			return false;
	}
	return true;
	/**/
}

bool CLDatabase::createStructs()
{
	/* Conductor
	if ( !m_db.IsOpen() )
		openEx();
	/**/

	/* Conductor */
	if (m_pDB == NULL || !m_pDB->IsConnected())
	{
		openEx();
	}
	/**/
	
		// CREATE USERS TABLES
	std::vector<CString> vect;
	createTables( &vect );

		// CREATE SYS TABLES
	vect.push_back( _T(LDB_SQL_CREATE_TAB_PR_SYS) );

	if ( !fillStructures( &vect ) )
		return false;
	
	return true;
}

bool CLDatabase::fillStructures( const std::vector<CString> *const pV )
{	
	/* Conductor 
	m_IsTransaction = m_db.BeginTrans() != 0;
	for ( std::vector<CString>::const_iterator it = pV->begin(); it != pV->end(); it++ )
	{
		m_sSQL = *it;
		if ( !executeSQL() )
		{
			m_IsTransaction = false;
			m_db.Rollback();
			return false;
		}
	}
	m_IsTransaction = false;
	return m_db.CommitTrans() != 0;
	/**/

	/* Conductor */
	if (m_pDB == NULL)
	{
		assert(false);
		return false;
	}

	LumexDB::EnOperationResult res = m_pDB->BeginTransaction();
	if (res != LumexDB::enSuccess)
	{
		assert(false);
		return false;
	}

	for ( std::vector<CString>::const_iterator it = pV->begin(); it != pV->end(); it++ )
	{
		m_sSQL = *it;
		if ( !executeSQL() )
		{		
			m_pDB->EndTransaction(false);
			return false;
		}
	}
	
	return m_pDB->EndTransaction(true) == LumexDB::enSuccess;

	/**/
}

bool CLDatabase::executeSQL()
{
	/* Conductor */

	if (m_pDB == NULL)
	{
		assert(false);
		return false;
	}

	return m_pDB->ExecuteSQL(m_sSQL) == LumexDB::enSuccess;

	/**/

	/* Conductor
	try
	{
		m_db.ExecuteSQL( m_sSQL );
	}
	catch ( CDBException *xcp )
	{	
		TRACE(_T("CLDatabase::executeSQL: "));
		TRACE(xcp->m_strError);
		TRACE(_T("m_sSQL: ") + m_sSQL + _T("\n"));
		xcp->Delete();
		return false;
	}
	return true;
	/**/
}

bool CLDatabase::selectSQL()
{
	/* Conductor */

	if (m_pDB == NULL)
	{
		assert(false);
		return false;
	}

	Free();

	return m_pDB->ExecuteSQL(m_sSQL, &m_pDataSet) == LumexDB::enSuccess;

	/**/

	/* Conductor
	try
	{
		Free();
		m_rst.Open( CRecordset::snapshot, m_sSQL, CRecordset::readOnly );
	}
	catch ( CDBException *xcp )
	{
		TRACE(_T("CLDatabase::selectSQL: "));
		TRACE(xcp->m_strError);
		TRACE(_T("m_sSQL: ") + m_sSQL + _T("\n"));
		xcp->Delete();		
		return false;
	}
	return true;
	/**/
}

long CLDatabase::isDataExist()
{
	/* Conductor */
	if (!selectSQL())
		return -1;

	if (m_pDataSet == NULL)
	{
		return -1;
	}	

	if (m_pDataSet->FirstRow() != LumexDB::enSuccess)
	{
		Free();
		return -1;
	}

	LumexDB::CDataPtr<ILumexDBDataSet::TColumnData> pData = m_pDataSet->GetColumn(0);
	if (pData == NULL)
	{
		Free();
		return -1;
	}

	int nData;
	LumexDB::EnOperationResult res = pData->GetData(&nData, sizeof(nData));
	if (res != LumexDB::enSuccess && res != LumexDB::enNoData)
	{
		Free();
		return -1;
	}

	Free();

	return nData;

	/**/

	/* Conductor
	if (!selectSQL())
		return -1;
	try
	{
		CDBVariant var;
		m_rst.GetFieldValue( (short)0, var );
		m_rst.Close();
		return var.m_lVal;
	}
	catch ( CDBException *xcp )
	{
		TRACE(_T("CLDatabase::isDataExists: "));
		TRACE(xcp->m_strError);
		TRACE(_T("m_sSQL: ") + m_sSQL + _T("\n"));
		xcp->Delete();		
		return -1;
	}
	/**/
}

bool CLDatabase::BeginTrans()
{
	/* Conductor */

	if (m_pDB == NULL)
	{
		assert(false);
		return false;
	}

	return m_pDB->BeginTransaction() == LumexDB::enSuccess;

	/**/

	/* Conductor
	if ( m_IsTransaction )
		return false;
	return (m_IsTransaction = (m_db.BeginTrans() != 0 ) );
	/**/
}
bool CLDatabase::CommitTrans()
{
	/* Conductor */
	if (m_pDB == NULL)
	{
		assert(false);
		return false;
	}

	return m_pDB->EndTransaction(true) == LumexDB::enSuccess;
	/**/

	/* Conductor
	m_IsTransaction = false;
	return ( m_db.CommitTrans() != 0 );
	/**/
}
bool CLDatabase::Rollback()
{
	/* Conductor */
	if (m_pDB == NULL)
	{
		assert(false);
		return false;
	}

	return m_pDB->EndTransaction(false) == LumexDB::enSuccess;
	/**/

	/* Conductor
	m_IsTransaction = false;
	return ( m_db.Rollback() != 0 );
	/**/
}

CString CLDatabase::replaceChar( CString buffer )
{
	buffer.Replace( _T(LDB_CHAR_OLD), _T(LDB_CHAR_TEMP) );
	return buffer;
}

void CLDatabase::returnChar( CString &buffer )
{
	buffer.Replace(_T(LDB_CHAR_TEMP), _T(LDB_CHAR_OLD));
}

CString CLDatabase::floatWithPoint( double dbl )
{
	CString tmp;
	tmp.Format(_T("%f"), dbl);
	tmp.Replace((CString)lconvert::GetLocalDecimalSeparator(), _T("."));
	return tmp;
}

CString CLDatabase::doubleWithPoint( double dbl )
{
	CString tmp;
	tmp.Format(_T("%17.15f"), dbl);
	tmp.Replace((CString)lconvert::GetLocalDecimalSeparator(), _T("."));
	return tmp;
}
