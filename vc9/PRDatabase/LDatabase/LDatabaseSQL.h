////////////////////////////////////////////////////////////////////////////
//	Author      : Alekseev Andrey 2006
//	Email       : spblex@yandex.ru
//	Module      : LDatabaseSQL.h
//	Version		: 1.21
//	Description : 
////////////////////////////////////////////////////////////////////////////

#ifndef LDATABASESQL_H
#define LDATABASESQL_H
#pragma once

	// OPEN
#define LDB_CONN_UID			"Admin"
#define LDB_CONN_DRV			"MICROSOFT ACCESS DRIVER (*.MDB)"
#define LDB_CONN_ANSI_SQL89	0
#define LDB_CONN_ANSI_SQL92	1

	// REPLACE CHAR
#define LDB_CHAR_OLD			"'"
#define LDB_CHAR_TEMP			"#^"

////////////////////////////////////////
//			DATABSE VERSION			  //
////////////////////////////////////////

#define LDB_SQL_CREATE_TAB_PR_SYS \
"CREATE TABLE PRSys(\n\
DBVersion		INT			NOT NULL,\n\
DBName			TEXT(100)	NOT NULL)"

#define LDB_SQL_GET_DB_VERSION \
"SELECT DBVersion FROM PRSys"

#define LDB_SQL_GET_DB_NAME \
"SELECT DBName FROM PRSys"

#define LDB_SQL_SET_DB_PR_SYS \
"INSERT INTO PRSys (\n\
DBVersion,\n\
DBName)\n\
VALUES (%d, '%s')"

#endif LDATABASESQL_H