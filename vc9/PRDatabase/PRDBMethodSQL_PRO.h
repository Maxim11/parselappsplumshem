#ifndef _PRDB_METHOD_SQL_H_
#define _PRDB_METHOD_SQL_H_

#pragma once


//////////////////////////////////////////////////////////
// �������������� ������
//////////////////////////////////////////////////////////

// SQL-������ - �������� ������ ������ � ���� �� �����
#define MTDDB_SQL_GET_MTD_ID \
"SELECT MethodID\n\
FROM Methods\n\
WHERE MethodName = '%s'"

// SQL-������ - ��������� ������ � ������
#define MTDDB_SQL_READ_MTD \
"SELECT MethodID,\n\
MethodName,\n\
DateCreated,\n\
MoisParam,\n\
DevNum,\n\
DevType,\n\
SpecRangeLow,\n\
SpecRangeUpper,\n\
ResSub,\n\
ApodSub,\n\
ZeroFill,\n\
UseRef,\n\
ResRef,\n\
ApodRef,\n\
SmplScan,\n\
SmplMeas,\n\
StdScan,\n\
StdMeas,\n\
Xtransform,\n\
UseBath,\n\
BathLength,\n\
IsSST,\n\
TempLow,\n\
TempUpper,\n\
TempDiff\n\
FROM Methods"

// SQL-������ - �������� ������ ���������� ������  � ���� �� ������� ������ � ����� ����������
#define MTDDB_SQL_GET_MTDPARAM_ID \
"SELECT MtdParamID\n\
FROM MtdParams\n\
WHERE MethodID = %d AND ParamName = '%s'"

// SQL-������ - ��������� ������ � ���������� ������
#define MTDDB_SQL_READ_MTDPARAM \
"SELECT MtdParamID,\n\
ParamName,\n\
Unit,\n\
Format,\n\
UseDefModel,\n\
DefModel,\n\
UseRefMois,\n\
RefMois,\n\
UseStdMois,\n\
StdMois\n\
FROM MtdParams\n\
WHERE MethodID = %d"

// SQL-������ - �������� ������ ������ ������ � ���� �� ������� ������ � ����� ������
#define MTDDB_SQL_GET_MTDMODEL_ID \
"SELECT MtdModelID\n\
FROM MtdModels\n\
WHERE MethodID = %d AND ModelName = '%s'"

// SQL-������ - ��������� ������ � ������ ������
#define MTDDB_SQL_READ_MTDMODEL \
"SELECT MtdModelID,\n\
ModelName,\n\
ModelType,\n\
SpecRangeLow,\n\
SpecRangeUpper,\n\
FreqMin,\n\
FreqStep,\n\
NFreq,\n\
SpType,\n\
NSpec,\n\
Preproc,\n\
MaxMah,\n\
MeanMah,\n\
IsTrans,\n\
FreqMinTrans,\n\
FreqStepTrans,\n\
NFreqTrans,\n\
SpTypeTrans,\n\
PreprocTrans,\n\
T0,\n\
FMax\n\
FROM MtdModels\n\
WHERE MethodID = %d"

// SQL-������ - ���������� ����� �������� ������������� ���������
#define MTDDB_SQL_SET_CORR_PARMODEL \
"UPDATE ParModels\n\
SET CorrB = %s,\n\
CorrA = %s\n\
WHERE MtdParamID = %d AND MtdModelID = %d"

// SQL-������ - �������� ������ � ���������� ���������� ������
#define MTDDB_SQL_READ_PARMODEL \
"SELECT p.ParModelID,\n\
m.ModelName,\n\
p.LowCalibRange,\n\
p.UpperCalibRange,\n\
p.MaxSKO,\n\
p.CorrB,\n\
p.CorrA,\n\
p.SEC,\n\
p.MeanComp,\n\
p.MeanCompTrans,\n\
p.MeanCompStd\n\
FROM ParModels AS p\n\
LEFT OUTER JOIN MtdModels AS m\n\
ON p.MtdModelID = m.MtdModelID\n\
WHERE MtdParamID = %d"

// SQL-������ - �������� ������ ����������� ������������ �����
#define MTDDB_SQL_READ_EXCPNT \
"SELECT ExcPoint\n\
FROM MtdModelExcPoints\n\
WHERE MtdModelID = %d\n\
ORDER BY MtdModelExcPointID"

// SQL-������ - �������� ������ �� ������� ��������
#define MTDDB_SQL_READ_MEANSPEC \
"SELECT MeanSpectrumID,\n\
MeanSpecType\n\
FROM MeanSpectra\n\
WHERE MtdModelID = %d"

// SQL-������ - �������� ������� ������
#define MTDDB_SQL_READ_MEANSPEC_DATA \
"SELECT Data\n\
FROM MeanSpectraDatas\n\
WHERE MeanSpectrumID = %d\n\
ORDER BY MeanSpectraDataID"

// SQL-������ - �������� ������ � ������� �������������� �������
#define MTDDB_SQL_READ_CALIBVECTOR \
"SELECT CalibVectorID,\n\
CalibVectorType\n\
FROM CalibVectors\n\
WHERE ParModelID = %d"

// SQL-������ - �������� ������ �������������� �������
#define MTDDB_SQL_READ_CALIBVECTOR_DATA \
"SELECT Data\n\
FROM CalibVectorDatas\n\
WHERE CalibVectorID = %d\n\
ORDER BY CalibVectorDataID"

//////////////////////////////////////////////////////////
// ������������ ������
//////////////////////////////////////////////////////////

// SQL-������ - �������� ������ ������ � ���� �� �����
#define MTDDB_SQL_GET_MTD_QLT_ID \
"SELECT MethodID\n\
FROM Methods\n\
WHERE MethodName = '%s'"

// SQL-������ - ��������� ������ � ������
#define MTDDB_SQL_READ_MTD_QLT \
"SELECT MethodID,\n\
MethodName,\n\
DateCreated,\n\
DevNum,\n\
DevType,\n\
SpecRangeLow,\n\
SpecRangeUpper,\n\
ResSub,\n\
ApodSub,\n\
ZeroFill,\n\
UseRef,\n\
ResRef,\n\
ApodRef,\n\
SmplScan,\n\
SmplMeas,\n\
StdScan,\n\
StdMeas,\n\
Xtransform,\n\
UseBath,\n\
BathLength,\n\
TempLow,\n\
TempUpper,\n\
TempDiff\n\
FROM Methods"

// SQL-������ - �������� ������ ������ ������ � ���� �� ������� ������ � ����� ������
#define MTDDB_SQL_GET_MTD_QLTMODEL_ID \
"SELECT MtdModelID\n\
FROM MtdModels\n\
WHERE MethodID = %d AND ModelName = '%s'"

// SQL-������ - ��������� ������ � ������ ������
#define MTDDB_SQL_READ_MTD_QLTMODEL \
"SELECT MtdModelID,\n\
ModelName,\n\
ModelInd,\n\
SpecRangeLow,\n\
SpecRangeUpper,\n\
FreqMin,\n\
FreqStep,\n\
NFreq,\n\
SpType,\n\
NSpec,\n\
Preproc,\n\
MaxMah,\n\
MaxSKO,\n\
MeanMah,\n\
IsTrans,\n\
FreqMinTrans,\n\
FreqStepTrans,\n\
NFreqTrans,\n\
SpTypeTrans,\n\
PreprocTrans\n\
FROM MtdModels\n\
WHERE MethodID = %d"

// SQL-������ - �������� ������ ����������� ������������ �����
#define MTDDB_SQL_READ_QLT_EXCPNT \
"SELECT ExcPoint\n\
FROM MtdModelExcPoints\n\
WHERE MtdModelID = %d\n\
ORDER BY MtdModelExcPointID"

// SQL-������ - �������� ������ �� ������� ��������
#define MTDDB_SQL_READ_QLT_MEANSPEC \
"SELECT MeanSpectrumID,\n\
MeanSpecType\n\
FROM MeanSpectra\n\
WHERE MtdModelID = %d"

// SQL-������ - �������� ������� ������
#define MTDDB_SQL_READ_QLT_MEANSPEC_DATA \
"SELECT Data\n\
FROM MeanSpectraDatas\n\
WHERE MeanSpectrumID = %d\n\
ORDER BY MeanSpectraDataID"

// SQL-������ - �������� ������ ������������� �������
#define MTDDB_SQL_READ_QLT_FORM \
"SELECT Type,\n\
ModelInd\n\
FROM MethodQltForms\n\
WHERE MethodID = %d\n\
ORDER BY MethodQltFormID"


#endif _PRDB_METHOD_SQL_H_
