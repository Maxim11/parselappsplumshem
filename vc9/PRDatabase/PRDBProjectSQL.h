////////////////////////////////////////////////////////////////////////////
//	Author      : Alekseev Andrey 2006
//	Email       : spblex@yandex.ru
//	Module      : PRDBProjectSQL.h
//	Version		: 1.27
//	Description : 
////////////////////////////////////////////////////////////////////////////

#ifndef PRDBPROJECTSQL_H
#define PRDBPROJECTSQL_H
#pragma once

#include "..\stdafx.h"

#define PRDB_SQL_CREATE_TAB_PR_ROLES \
"CREATE TABLE Roles(\n\
RoleID			COUNTER		PRIMARY KEY,\n\
RoleType		INT			NOT NULL,\n\
RoleDescription	TEXT(%d),\n\
CONSTRAINT PRROLE UNIQUE (RoleType))"

#define PRDB_SQL_CREATE_TAB_PR_USERS \
"CREATE TABLE Users(\n\
UserID			COUNTER		PRIMARY KEY,\n\
RoleID			INT			NOT NULL REFERENCES Roles(RoleID) ON UPDATE CASCADE ON DELETE CASCADE,\n\
UserNIC			TEXT(%d)	NOT NULL,\n\
FirstName		TEXT(%d),\n\
LastName		TEXT(%d),\n\
MiddleName		TEXT(%d),\n\
CONSTRAINT PRUSER UNIQUE (UserNIC))"

#define PRDB_SQL_CREATE_TAB_PR_HIS \
"CREATE TABLE History(\n\
HistoryID		COUNTER		PRIMARY KEY,\n\
UserID			INT			NOT NULL REFERENCES Users(UserID) ON UPDATE CASCADE ON DELETE CASCADE,\n\
HistoryDate		DATETIME	NOT NULL,\n\
EventType		INT			NOT NULL,\n\
HistoryTarget	INT)"

#define PRDB_SQL_READ_HIS_USER_LAST \
"SELECT MAX(HistoryDate),\n\
EventType,\n\
HistoryTarget\n\
FROM History\n\
WHERE UserID = (SELECT UserID FROM Users WHERE UserNIC = '%s')"

#define PRDB_SQL_READ_HIS_USER \
"SELECT HistoryDate,\n\
EventType,\n\
HistoryTarget\n\
FROM History\n\
WHERE UserID = (SELECT UserID FROM Users WHERE UserNIC = '%s')"

#define PRDB_SQL_READ_HIS_LAST \
"SELECT MAX(h.HistoryDate),\n\
h.EventType,\n\
h.HistoryTarget\n\
u.UserNIC,\n\
u.FirstName,\n\
u.LastName,\n\
u.MiddleName,\n\
r.RoleType,\n\
r.RoleDescription\n\
FROM History AS h\n\
INNER JOIN Users AS u\n\
ON u.UserID = h.UserID\n\
LEFT OUTER JOIN Roles AS r\n\
ON r.RoleID = u.RoleID"

#define PRDB_SQL_READ_HIS \
"SELECT h.HistoryDate,\n\
h.EventType,\n\
h.HistoryTarget\n\
u.UserNIC,\n\
u.FirstName,\n\
u.LastName,\n\
u.MiddleName,\n\
r.RoleType,\n\
r.RoleDescription\n\
FROM History AS h\n\
INNER JOIN Users AS u\n\
ON u.UserID = h.UserID\n\
LEFT OUTER JOIN Roles AS r\n\
ON r.RoleID = u.RoleID"

#define PRDB_SQL_IS_EXITS_ROLE \
"SELECT RoleID\n\
FROM Roles\n\
WHERE RoleType = %d"

#define PRDB_SQL_MAX_ROLE \
"SELECT MAX(RoleID)\n\
FROM Roles"

#define PRDB_SQL_INSERT_ROLE \
"INSERT INTO Roles(\n\
RoleID,\n\
RoleType,\n\
RoleDescription)\n\
VALUES (%d, %d, '%s')"

#define PRDB_SQL_IS_EXITS_USER \
"SELECT UserID\n\
FROM Users\n\
WHERE UserNIC = '%s'"

#define PRDB_SQL_MAX_USER \
"SELECT MAX(UserID)\n\
FROM Users"

#define PRDB_SQL_INSERT_USER \
"INSERT INTO Users(\n\
UserID,\n\
RoleID,\n\
UserNIC,\n\
FirstName,\n\
LastName,\n\
MiddleName)\n\
VALUES (%d, %d, '%s', '%s', '%s', '%s' )"

#define PRDB_SQL_INSERT_HISTORY \
"INSERT INTO History (\n\
UserID,\n\
HistoryDate,\n\
EventType,\n\
HistoryTarget)\n\
VALUES (%d, #%s#, %d, %d)"

////////////////////////////////////////
//				������				  //
////////////////////////////////////////

#define PRDB_SQL_CREATE_TAB_PR_APP \
"CREATE TABLE Appliances(\n\
ApplianceID		COUNTER		PRIMARY KEY,\n\
AcType			INT			NOT NULL,\n\
AcSN			INT			NOT NULL,\n\
CONSTRAINT PRAPP UNIQUE (AcSN))"

#define PRDB_SQL_INSERT_APPLIENCE \
"INSERT INTO Appliances (\n\
AcType,\n\
AcSN)\n\
VALUES (%d, %d)"

#define PRDB_SQL_UPDATE_APPLIENCE \
"UPDATE Appliances\n\
SET AcType = %d,\n\
AcSN = %d\n\
WHERE AcSN = %d"

#define PRDB_SQL_DELETE_APPLIENCE \
"DELETE FROM Appliances WHERE AcSN = %d"

#define PRDB_SQL_ISEXISTS_APPLIENCE \
"SELECT COUNT(*)\n\
FROM Appliances\n\
WHERE AcSN = %d"

#define PRDB_SQL_READ_APPLIENCE \
"SELECT AcType,\n\
AcSN\n\
FROM Appliances"

////////////////////////////////////////
//				������				  //
////////////////////////////////////////

#define PRDB_SQL_CREATE_TAB_PR_APROJ \
"CREATE TABLE AProjects(\n\
AProjectID		COUNTER		PRIMARY KEY,\n\
ApplianceID		INT			NOT NULL REFERENCES Appliances(ApplianceID) ON UPDATE CASCADE ON DELETE CASCADE,\n\
APTitle			TEXT(%d)	NOT NULL,\n\
SST				BIT			NOT NULL,\n\
SpRangeDown		INT			NOT NULL,\n\
SpRangeUp		INT			NOT NULL,\n\
SampleScanCount	INT			NOT NULL,\n\
SampleMeasCount	INT			NOT NULL,\n\
StScanCount		INT			NOT NULL,\n\
StMeasCount		INT			NOT NULL,\n\
CuvetteLength	FLOAT		NOT NULL,\n\
SubChPermit		INT			NOT NULL,\n\
SubChApod		INT			NOT NULL,\n\
ZeroFilling		INT			NOT NULL,\n\
UseRefChannel	BIT			NOT NULL,\n\
RefChPermit		INT			NOT NULL,\n\
RefChApod		INT			NOT NULL,\n\
TempRangeDown	INT			NOT NULL,\n\
TempRangeUp		INT			NOT NULL,\n\
TempDiff		INT			NOT NULL,\n\
SamSortName		TEXT(%d)	NOT NULL,\n\
SamSortDir		BIT			NOT NULL,\n\
CONSTRAINT PRPROJ UNIQUE (ApplianceID, APTitle))"

#define PRDB_SQL_INSERT_APROJ \
"INSERT INTO AProjects(\n\
ApplianceID,\n\
APTitle,\n\
SST,\n\
SpRangeDown,\n\
SpRangeUp,\n\
SampleScanCount,\n\
SampleMeasCount,\n\
StScanCount,\n\
StMeasCount,\n\
CuvetteLength,\n\
SubChPermit,\n\
SubChApod,\n\
ZeroFilling,\n\
UseRefChannel,\n\
RefChPermit,\n\
RefChApod,\n\
TempRangeDown,\n\
TempRangeUp,\n\
TempDiff,\n\
SamSortName,\n\
SamSortDir)\n\
SELECT a.ApplianceID, '%s', %d, %d, %d, %d, %d, %d, %d, %s, %d, %d, %d, %d, %d, %d, %d, %d, %d, '%s', %d\n\
FROM Appliances AS a\n\
WHERE a.AcSN = %d"

#define PRDB_SQL_UPDATE_APROJ \
"UPDATE AProjects\n\
SET APTitle = '%s',\n\
SST = %d,\n\
SpRangeDown = %d,\n\
SpRangeUp = %d,\n\
SampleScanCount = %d,\n\
SampleMeasCount = %d,\n\
StScanCount = %d,\n\
StMeasCount = %d,\n\
CuvetteLength = %s,\n\
SubChPermit = %d,\n\
SubChApod = %d,\n\
ZeroFilling = %d,\n\
UseRefChannel = %d,\n\
RefChPermit = %d,\n\
RefChApod = %d,\n\
TempRangeDown = %d,\n\
TempRangeUp = %d,\n\
TempDiff = %d,\n\
SamSortName = '%s',\n\
SamSortDir = %d\n\
WHERE APTitle = '%s' AND ApplianceID = (\n\
SELECT ApplianceID\n\
FROM Appliances\n\
WHERE AcSN = %d)"

#define PRDB_SQL_DELETE_APROJ \
"DELETE FROM AProjects\n\
WHERE APTitle = '%s' AND ApplianceID = (\n\
SELECT ApplianceID FROM Appliances WHERE AcSN = %d)"

#define PRDB_SQL_DELETE_APROJ_ALL \
"DELETE FROM AProjects\n\
WHERE ApplianceID = (\n\
SELECT ApplianceID FROM Appliances WHERE AcSN = %d)"

#define PRDB_SQL_ISEXISTS_APROJ \
"SELECT COUNT(*)\n\
FROM AProjects\n\
WHERE APTitle = '%s' AND ApplianceID = (SELECT ApplianceID FROM Appliances WHERE AcSN = %d)"

#define PRDB_SQL_GET_DEVPROJECTS_NAME \
"SELECT APTitle\n\
FROM AProjects\n\
WHERE ApplianceID = (SELECT ApplianceID FROM Appliances WHERE AcSN = %d)"

#define PRDB_SQL_READ_APROJECT \
"SELECT APTitle,\n\
SST,\n\
SpRangeDown,\n\
SpRangeUp,\n\
SampleScanCount,\n\
SampleMeasCount,\n\
StScanCount,\n\
StMeasCount,\n\
CuvetteLength,\n\
SubChPermit,\n\
SubChApod,\n\
ZeroFilling,\n\
UseRefChannel,\n\
RefChPermit,\n\
RefChApod,\n\
TempRangeDown,\n\
TempRangeUp,\n\
TempDiff,\n\
SamSortName,\n\
SamSortDir\n\
FROM AProjects\n\
WHERE ApplianceID = (SELECT ApplianceID FROM Appliances WHERE AcSN = %d)"

////////////////////////////////////////
//				����������			  //
////////////////////////////////////////

#define PRDB_SQL_CREATE_TAB_PR_IDX \
"CREATE TABLE Indices (\n\
IndexID			COUNTER		PRIMARY KEY,\n\
AProjectID		INT			NOT NULL REFERENCES AProjects(AProjectID) ON UPDATE CASCADE ON DELETE CASCADE,\n\
IdxName			TEXT(%d)	NOT NULL,\n\
IdxUnits		TEXT(%d)	NOT NULL,\n\
IdxFormat		INT			NOT NULL,\n\
StHumidity		FLOAT		NOT NULL,\n\
IsUseStHumidity BIT			NOT NULL,\n\
CONSTRAINT PRIDX UNIQUE (AProjectID, IdxName))"

#define PRDB_SQL_INSERT_IDX \
"INSERT INTO Indices (\n\
AProjectID,\n\
IdxName,\n\
IdxUnits,\n\
IdxFormat,\n\
StHumidity,\n\
IsUseStHumidity)\n\
SELECT AProjectID, '%s', '%s', %d, %s, %d\n\
FROM AProjects WHERE APTitle = '%s' AND ApplianceID = (SELECT ApplianceID FROM Appliances WHERE AcSN = %d)"

#define PRDB_SQL_UPDATE_IDX \
"UPDATE Indices\n\
SET IdxName = '%s',\n\
IdxUnits = '%s',\n\
IdxFormat = %d,\n\
StHumidity = %s,\n\
IsUseStHumidity = %d\n\
WHERE IdxName = '%s' AND AProjectID = (SELECT AProjectID FROM AProjects WHERE APTitle = '%s' AND ApplianceID = (SELECT ApplianceID FROM Appliances WHERE AcSN = %d))"

#define PRDB_SQL_DELETE_IDX \
"DELETE FROM Indices\n\
WHERE IdxName = '%s' AND AProjectID = (SELECT AProjectID FROM AProjects WHERE APTitle = '%s' AND ApplianceID = (SELECT ApplianceID FROM Appliances WHERE AcSN = %d))"

#define PRDB_SQL_ISEXISTS_IDX \
"SELECT COUNT(*)\n\
FROM Indices\n\
WHERE IdxName = '%s' AND AProjectID = (SELECT AProjectID FROM AProjects WHERE APTitle = '%s' AND ApplianceID = (SELECT ApplianceID FROM Appliances WHERE AcSN = %d))"

#define PRDB_SQL_READ_IDX \
"SELECT IdxName,\n\
IdxUnits,\n\
IdxFormat,\n\
StHumidity,\n\
IsUseStHumidity\n\
FROM Indices\n\
WHERE AProjectID = (SELECT AProjectID FROM AProjects WHERE APTitle = '%s' AND ApplianceID = (SELECT ApplianceID FROM Appliances WHERE AcSN = %d))"

////////////////////////////////////////
//		������������ �����			  //
////////////////////////////////////////

#define PRDB_SQL_CREATE_TAB_PR_TRANS \
"CREATE TABLE Transes (\n\
TransID			COUNTER		PRIMARY KEY,\n\
AProjectID		INT			NOT NULL REFERENCES AProjects(AProjectID) ON UPDATE CASCADE ON DELETE CASCADE,\n\
TransName		TEXT(%d)	NOT NULL,\n\
Type			INT			NOT NULL,\n\
MasterType		INT			NOT NULL,\n\
MasterNumber	INT			NOT NULL,\n\
DateCreated		DATETIME	NOT NULL,\n\
LowLim			INT			NOT NULL,\n\
UpperLim		INT			NOT NULL,\n\
Preproc			TEXT(%d)	NOT NULL,\n\
FrameName		TEXT(%d)	NOT NULL,\n\
CONSTRAINT PRTRNS UNIQUE (AProjectID, TransName))"

#define PRDB_SQL_INSERT_TRANS \
"INSERT INTO Transes (\n\
TransID,\n\
AProjectID,\n\
TransName,\n\
Type,\n\
MasterType,\n\
MasterNumber,\n\
DateCreated,\n\
LowLim,\n\
UpperLim,\n\
Preproc,\n\
FrameName)\n\
SELECT %d, AProjectID, '%s', %d, %d, %d, #%s#, %d, %d, '%s', '%s'\n\
FROM AProjects WHERE APTitle = '%s' AND ApplianceID = (SELECT ApplianceID FROM Appliances WHERE AcSN = %d)"

#define PRDB_SQL_UPDATE_TRANS \
"UPDATE Transes\n\
SET TransName = '%s',\n\
Type = %d,\n\
MasterType = %d,\n\
MasterNumber = %d,\n\
DateCreated = #%s#,\n\
LowLim = %d,\n\
UpperLim = %d,\n\
Preproc = '%s',\n\
FrameName = '%s'\n\
WHERE TransID = %d"

#define PRDB_SQL_DELETE_TRANS \
"DELETE FROM Transes\n\
WHERE TransName = '%s' AND AProjectID = (SELECT AProjectID FROM AProjects WHERE APTitle = '%s' AND ApplianceID = (SELECT ApplianceID FROM Appliances WHERE AcSN = %d))"

#define PRDB_SQL_ISEXISTS_TRANS \
"SELECT COUNT(*)\n\
FROM Transes\n\
WHERE TransName = '%s' AND AProjectID = (SELECT AProjectID FROM AProjects WHERE APTitle = '%s' AND ApplianceID = (SELECT ApplianceID FROM Appliances WHERE AcSN = %d))"

#define PRDB_SQL_GET_TRANS_ID_MAX \
"SELECT MAX(TransID)\n\
FROM Transes"

#define PRDB_SQL_GET_TRANS_ID \
"SELECT TransID\n\
FROM Transes\n\
WHERE TransName = '%s' AND AProjectID = (SELECT AProjectID FROM AProjects\n\
WHERE APTitle = '%s' AND ApplianceID = (SELECT ApplianceID FROM Appliances WHERE AcSN = %d))"

#define PRBD_SQL_GETNAME_TRANS \
"SELECT TransName\n\
FROM Transes\n\
WHERE TransID = %d"

#define PRDB_SQL_GET_PRJTRANSES_NAME \
"SELECT TransName\n\
FROM Transes\n\
WHERE AProjectID = (SELECT AProjectID FROM AProjects WHERE APTitle = '%s' AND ApplianceID = (SELECT ApplianceID FROM Appliances WHERE AcSN = %d))"

#define PRDB_SQL_READ_TRANS \
"SELECT TransName,\n\
Type,\n\
MasterType,\n\
MasterNumber,\n\
DateCreated,\n\
LowLim,\n\
UpperLim,\n\
Preproc,\n\
FrameName\n\
FROM Transes\n\
WHERE AProjectID = (SELECT AProjectID FROM AProjects WHERE APTitle = '%s' AND ApplianceID = (SELECT ApplianceID FROM Appliances WHERE AcSN = %d))"

////////////////////////////////////////
//		��������					  //
////////////////////////////////////////

#define PRDB_SQL_CREATE_TAB_PR_PLATE \
"CREATE TABLE Plates (\n\
PlateID			COUNTER		PRIMARY KEY,\n\
TransID			INT			NOT NULL REFERENCES Transes(TransID) ON UPDATE CASCADE ON DELETE CASCADE,\n\
PlateNum		INT			NOT NULL,\n\
SamName			TEXT(%d)	NOT NULL,\n\
KoeffRef		FLOAT		NOT NULL,\n\
Wight0			FLOAT		NOT NULL,\n\
Wight1			FLOAT		NOT NULL,\n\
Wight2			FLOAT		NOT NULL,\n\
Wight3			FLOAT		NOT NULL,\n\
Wight4			FLOAT		NOT NULL,\n\
CONSTRAINT PRPLATE UNIQUE (TransID, PlateNum))"

#define PRDB_SQL_INSERT_PLATE \
"INSERT INTO Plates (\n\
TransID,\n\
PlateNum,\n\
SamName,\n\
KoeffRef,\n\
Wight0,\n\
Wight1,\n\
Wight2,\n\
Wight3,\n\
Wight4)\n\
VALUES (%d, %d, '%s', %s, %s, %s, %s, %s, %s)"

#define PRDB_SQL_DELETE_PLATE \
"DELETE FROM Plates\n\
WHERE TransID = %d"

#define PRDB_SQL_ISEXISTS_PLATE \
"SELECT COUNT(*)\n\
FROM Plates\n\
WHERE PlateNum = %d AND TransID = %d"

#define PRDB_SQL_READ_PLATE \
"SELECT PlateNum,\n\
SamName,\n\
KoeffRef,\n\
Wight0,\n\
Wight1,\n\
Wight2,\n\
Wight3,\n\
Widht4\n\
FROM Plates\n\
WHERE TransID = %d"

////////////////////////////////////////
//				�������				  //
////////////////////////////////////////

#define PRDB_SQL_CREATE_TAB_PR_SMPL \
"CREATE TABLE Samples (\n\
SampleID		COUNTER		PRIMARY KEY,\n\
TransID			INT			NOT NULL REFERENCES Transes(TransID) ON UPDATE CASCADE ON DELETE CASCADE,\n\
Sample1			TEXT(%d)	NOT NULL,\n\
Sample2			TEXT(%d)	NOT NULL,\n\
Sample3			TEXT(%d)	NOT NULL,\n\
CONSTRAINT PRSMPL UNIQUE (TransID, Sample1))"

#define PRDB_SQL_INSERT_SMPL \
"INSERT INTO Samples (\n\
TransID,\n\
Sample1,\n\
Sample2,\n\
Sample3)\n\
SELECT TransID, '%s', '%s', '%s'\n\
FROM Transes WHERE TransName = '%s' AND AProjectID = (SELECT AProjectID FROM AProjects WHERE APTitle = '%s' AND ApplianceID = (SELECT ApplianceID FROM Appliances WHERE AcSN = %d))"

#define PRDB_SQL_UPDATE_SMPL \
"UPDATE Samples\n\
SET Sample1 = '%s',\n\
Sample2 = '%s',\n\
Sample3 = '%s'\n\
WHERE Sample1 = '%s' AND TransID = (SELECT TransID FROM Transes WHERE TransName = '%s' AND AProjectID = (SELECT AProjectID FROM AProjects WHERE APTitle = '%s' AND ApplianceID = (SELECT ApplianceID FROM Appliances WHERE AcSN = %d)))"

#define PRDB_SQL_DELETE_SMPL \
"DELETE FROM Samples\n\
WHERE Sample1 = '%s' AND TransID = (SELECT TransID FROM Transes WHERE TransName = '%s' AND AProjectID = (SELECT AProjectID FROM AProjects WHERE APTitle = '%s' AND ApplianceID = (SELECT ApplianceID FROM Appliances WHERE AcSN = %d)))"

#define PRDB_SQL_SELECT_SAMPLE_ID \
"SELECT SampleID\n\
FROM Samples\n\
WHERE Sample1 = '%s' AND TransID = (SELECT TransID FROM Transes WHERE TransName = '%s' AND AProjectID = (SELECT AProjectID FROM AProjects WHERE APTitle = '%s' AND ApplianceID = (SELECT ApplianceID FROM Appliances WHERE AcSN = %d)))"

#define PRDB_SQL_GET_TRANSSAMPLES_NAME \
"SELECT Sample1\n\
FROM Samples\n\
WHERE TransID = (SELECT TransID FROM Transes WHERE TransName = '%s' AND AProjectID = (SELECT AProjectID FROM AProjects WHERE APTitle = '%s' AND ApplianceID = (SELECT ApplianceID FROM Appliances WHERE AcSN = %d)))"

#define PRDB_SQL_ISEXISTS_SMPL \
"SELECT COUNT(*)\n\
FROM Samples\n\
WHERE Sample1 = '%s' AND TransID = (SELECT TransID FROM Transes WHERE TransName = '%s' AND AProjectID = (SELECT AProjectID FROM AProjects WHERE APTitle = '%s' AND ApplianceID = (SELECT ApplianceID FROM Appliances WHERE AcSN = %d)))"

#define PRDB_SQL_READ_SMPL \
"SELECT Sample1,\n\
Sample2,\n\
Sample3\n\
FROM Samples\n\
WHERE TransID = (SELECT TransID FROM Transes WHERE TransName = '%s' AND AProjectID = (SELECT AProjectID FROM AProjects WHERE APTitle = '%s' AND ApplianceID = (SELECT ApplianceID FROM Appliances WHERE AcSN = %d)))"

////////////////////////////////////////
//		����������� ������			  //
////////////////////////////////////////

#define PRDB_SQL_CREATE_TAB_PR_RFDT \
"CREATE TABLE RefDatas (\n\
RefDataID		COUNTER		PRIMARY KEY,\n\
IndexID			INT			NOT NULL REFERENCES Indices(IndexID) ON UPDATE CASCADE ON DELETE CASCADE,\n\
SampleID		INT			NOT NULL REFERENCES Samples(SampleID) ON UPDATE CASCADE ON DELETE CASCADE,\n\
IsDataExists	BIT			NOT NULL,\n\
RefData			FLOAT		NOT NULL,\n\
CONSTRAINT PRRFDT UNIQUE (IndexID, SampleID))"

#define PRDB_SQL_INSERT_RFDT \
"INSERT INTO RefDatas (\n\
IndexID,\n\
SampleID,\n\
IsDataExists,\n\
RefData)\n\
SELECT i.IndexID, s.SampleID, %d, %s\n\
FROM (Indices AS i\n\
LEFT OUTER JOIN Transes AS t\n\
ON i.AProjectID = t.AProjectID)\n\
LEFT OUTER JOIN Samples AS s\n\
ON t.TransID = s.TransID\n\
WHERE i.IdxName = '%s' AND s.Sample1 = '%s' AND t.TransName = '%s' AND\n\
i.AProjectID = (SELECT AProjectID FROM AProjects WHERE APTitle = '%s' AND ApplianceID = (SELECT ApplianceID FROM Appliances WHERE AcSN = %d))"

#define PRDB_SQL_UPDATE_RFDT \
"UPDATE RefDatas\n\
SET IsDataExists = %d,\n\
RefData = %s\n\
WHERE IndexID = (\n\
SELECT IndexID FROM Indices WHERE IdxName = '%s' AND AProjectID = (SELECT AProjectID FROM AProjects WHERE APTitle = '%s' AND ApplianceID = (SELECT ApplianceID FROM Appliances WHERE AcSN = %d))) AND\n\
SampleID = (SELECT SampleID FROM Samples WHERE Sample1 = '%s' AND TransID = (SELECT TransID FROM Transes WHERE TransName = '%s' AND AProjectID = (SELECT AProjectID FROM AProjects WHERE APTitle = '%s' AND ApplianceID = (SELECT ApplianceID FROM Appliances WHERE AcSN = %d))))"

#define PRDB_SQL_DELETE_RFDT \
"DELETE FROM RefDatas\n\
WHERE IndexID = (\n\
SELECT IndexID FROM Indices WHERE IdxName = '%s' AND AProjectID = (SELECT AProjectID FROM AProjects WHERE APTitle = '%s' AND ApplianceID = (SELECT ApplianceID FROM Appliances WHERE AcSN = %d))) AND\n\
SampleID = (SELECT SampleID FROM Samples WHERE Sample1 = '%s' AND TransID = (SELECT TransID FROM Transes WHERE TransName = '%s' AND AProjectID = (SELECT AProjectID FROM AProjects WHERE APTitle = '%s' AND ApplianceID = (SELECT ApplianceID FROM Appliances WHERE AcSN = %d))))"

#define PRDB_SQL_ISEXISTS_RFDT \
"SELECT COUNT(*)\n\
FROM RefDatas\n\
WHERE IndexID = (\n\
SELECT IndexID FROM Indices WHERE IdxName = '%s' AND AProjectID = (SELECT AProjectID FROM AProjects WHERE APTitle = '%s' AND ApplianceID = (SELECT ApplianceID FROM Appliances WHERE AcSN = %d))) AND\n\
SampleID = (SELECT SampleID FROM Samples WHERE Sample1 = '%s' AND TransID = (SELECT TransID FROM Transes WHERE TransName = '%s' AND AProjectID = (SELECT AProjectID FROM AProjects WHERE APTitle = '%s' AND ApplianceID = (SELECT ApplianceID FROM Appliances WHERE AcSN = %d))))"

#define PRDB_SQL_READ_RFDT \
"SELECT i.IdxName,\n\
r.IsDataExists,\n\
r.RefData\n\
FROM RefDatas AS r\n\
LEFT OUTER JOIN Indices AS i\n\
ON r.IndexID = i.IndexID\n\
WHERE r.SampleID = (SELECT SampleID FROM Samples WHERE Sample1 = '%s' AND TransID = (SELECT TransID FROM Transes WHERE TransName = '%s' AND AProjectID = (SELECT AProjectID FROM AProjects WHERE APTitle = '%s' AND ApplianceID = (SELECT ApplianceID FROM Appliances WHERE AcSN = %d))))"

#define PRDB_SQL_READ_RFDT_BYID \
"SELECT i.IdxName,\n\
r.IsDataExists,\n\
r.RefData \n\
FROM RefDatas AS r\n\
LEFT OUTER JOIN Indices AS i\n\
ON r.IndexID = i.IndexID\n\
WHERE r.SampleID = %d"

////////////////////////////////////////
//				������				  //
////////////////////////////////////////

#define PRDB_SQL_CREATE_TAB_PR_SPRM \
"CREATE TABLE Spectrums (\n\
SpectrumID		COUNTER		PRIMARY KEY,\n\
SampleID		INT			NOT NULL REFERENCES Samples(SampleID) ON UPDATE CASCADE ON DELETE CASCADE,\n\
SpecNum			INT			NOT NULL,\n\
DateCreated		DATETIME	NOT NULL,\n\
IsUse			BIT			NOT NULL,\n\
CONSTRAINT PRSPRM UNIQUE (SampleID, SpecNum))"

#define PRDB_SQL_INSERT_SPRM \
"INSERT INTO Spectrums (\n\
SpectrumID,\n\
SampleID,\n\
SpecNum,\n\
DateCreated,\n\
IsUse)\n\
SELECT %d, SampleID, %d, #%s#, %d\n\
FROM Samples\n\
WHERE Sample1 = '%s' AND TransID = (SELECT TransID FROM Transes WHERE TransName = '%s' AND AProjectID = (SELECT AProjectID FROM AProjects WHERE APTitle = '%s' AND ApplianceID = (SELECT ApplianceID FROM Appliances WHERE AcSN = %d)))"

#define PRDB_SQL_UPDATE_USE_SPRM \
"UPDATE Spectrums\n\
SET IsUse = %d\n\
WHERE SpecNum = %d AND SampleID = (SELECT SampleID FROM Samples\n\
WHERE Sample1 = '%s' AND TransID = (SELECT TransID FROM Transes WHERE TransName = '%s' AND AProjectID = (SELECT AProjectID FROM AProjects WHERE APTitle = '%s' AND ApplianceID = (SELECT ApplianceID FROM Appliances WHERE AcSN = %d))))"

#define PRDB_SQL_DELETE_SPRM \
"DELETE FROM Spectrums\n\
WHERE SpecNum = %d AND\n\
SampleID = (SELECT SampleID FROM Samples WHERE Sample1 = '%s' AND TransID = (SELECT TransID FROM Transes WHERE TransName = '%s' AND AProjectID = (SELECT AProjectID FROM AProjects WHERE APTitle = '%s' AND ApplianceID = (SELECT ApplianceID FROM Appliances WHERE AcSN = %d))))"

#define PRDB_SQL_GET_SPRM_ID_MAX \
"SELECT MAX(SpectrumID)\n\
FROM Spectrums"

#define PRDB_SQL_ISEXISTS_SPRM \
"SELECT COUNT(*)\n\
FROM Spectrums\n\
WHERE SpecNum = %d AND SampleID = (SELECT SampleID FROM Samples\n\
WHERE Sample1 = '%s' AND TransID = (SELECT TransID FROM Transes WHERE TransName = '%s' AND AProjectID = (SELECT AProjectID FROM AProjects WHERE APTitle = '%s' AND ApplianceID = (SELECT ApplianceID FROM Appliances WHERE AcSN = %d))))"

#define PRDB_SQL_SELECT_SPECTRUM_ID \
"SELECT SpectrumID\n\
FROM Spectrums\n\
WHERE SpecNum = %d AND SampleID = (SELECT SampleID FROM Samples\n\
WHERE Sample1 = '%s' AND TransID = (SELECT TransID FROM Transes WHERE TransName = '%s' AND AProjectID = (SELECT AProjectID FROM AProjects WHERE APTitle = '%s' AND ApplianceID = (SELECT ApplianceID FROM Appliances WHERE AcSN = %d))))"

#define PRDB_SQL_GET_SAMSPECTRA_ID \
"SELECT SpectrumID\n\
FROM Spectrums\n\
WHERE SampleID = (SELECT SampleID FROM Samples\n\
WHERE Sample1 = '%s' AND TransID = (SELECT TransID FROM Transes WHERE TransName = '%s' AND AProjectID = (SELECT AProjectID FROM AProjects WHERE APTitle = '%s' AND ApplianceID = (SELECT ApplianceID FROM Appliances WHERE AcSN = %d))))"

#define PRDB_SQL_GET_SAMSPECTRA_ID2 \
"SELECT SpectrumID\n\
FROM Spectrums\n\
WHERE SampleID = %d"

#define PRDB_SQL_SELECT_SPRM_SPECNUM \
"SELECT SpecNum\n\
FROM Spectrums\n\
WHERE SpectrumID = %d"

#define PRDB_SQL_SELECT_SPRM_SMPL \
"SELECT Sample1,\n\
SampleID,\n\
TransID\n\
FROM Samples\n\
WHERE SampleID = (SELECT SampleID FROM Spectrums WHERE SpectrumID = %d)"

#define PRDB_SQL_READ_SPRM_LINK \
"SELECT SpecNum\n\
FROM Spectrums\n\
WHERE SampleID = (SELECT SampleID FROM Samples\n\
WHERE Sample1 = '%s' AND TransID = (SELECT TransID FROM Transes WHERE TransName = '%s' AND AProjectID = (SELECT AProjectID FROM AProjects WHERE APTitle = '%s' AND ApplianceID = (SELECT ApplianceID FROM Appliances WHERE AcSN = %d))))\n\
ORDER BY SpectrumID"

#define PRDB_SQL_READ_SPRM \
"SELECT DateCreated,\n\
IsUse,\n\
SpectrumID\n\
FROM Spectrums\n\
WHERE SpecNum = %d AND SampleID = (SELECT SampleID FROM Samples\n\
WHERE Sample1 = '%s' AND TransID = (SELECT TransID FROM Transes WHERE TransName = '%s' AND AProjectID = (SELECT AProjectID FROM AProjects WHERE APTitle = '%s' AND ApplianceID = (SELECT ApplianceID FROM Appliances WHERE AcSN = %d))))"

#define PRDB_SQL_READ_SPRM_BYID \
"SELECT DateCreated,\n\
IsUse,\n\
SpecNum \n\
FROM Spectrums \n\
WHERE SpectrumID = %d"

/* ������ � ��������� ������������ ������ � ����

////////////////////////////////////////
//		������������ ������			  //
////////////////////////////////////////

#define PRDB_SQL_CREATE_TAB_PR_SPRMDATA \
"CREATE TABLE SpectrumDatas (\n\
SpectrumDataID	COUNTER		PRIMARY KEY,\n\
SpectrumID		INT			NOT NULL REFERENCES Spectrums(SpectrumID) ON UPDATE CASCADE ON DELETE CASCADE,\n\
SpectrumData	FLOAT		NOT NULL)"

#define PRDB_SQL_INSERT_SPRMDATA \
"INSERT INTO SpectrumDatas (\n\
SpectrumID,\n\
SpectrumData)\n\
VALUES (%d, %s)"

#define PRDB_SQL_READ_SPRMDATA \
"SELECT SpectrumData\n\
FROM SpectrumDatas\n\
WHERE SpectrumID = %d\n\
ORDER BY SpectrumDataID"

����� ������� ���� */

////////////////////////////////////////
//				������				  //
////////////////////////////////////////

#define PRDB_SQL_CREATE_TAB_PR_MDL \
"CREATE TABLE Models (\n\
ModelID			COUNTER		PRIMARY KEY,\n\
AProjectID		INT			NOT NULL REFERENCES AProjects(AProjectID) ON UPDATE CASCADE ON DELETE CASCADE,\n\
ModelName		TEXT(%d)	NOT NULL,\n\
TransName		TEXT(%d)	NOT NULL,\n\
DateCreated		DATETIME	NOT NULL,\n\
AnalisisType	INT			NOT NULL,\n\
ModelType		INT			NOT NULL,\n\
NumCompQNT		INT,	\n\
NumCompQLT		INT,	\n\
HCubeVolume		FLOAT,	\n\
HarmonyCount	INT,	\n\
Nonlinearity	FLOAT,	\n\
PredH			FLOAT		NOT NULL,\n\
PredCKO			FLOAT,	\n\
UseSECV			BIT			NOT NULL,\n\
SpectrumType	INT			NOT NULL,\n\
SpRangeRealDown	INT			NOT NULL,\n\
SpRangeRealUp	INT			NOT NULL,\n\
Preproc			TEXT(%d)	,\n\
CONSTRAINT PRMDL UNIQUE (AProjectID, ModelName))"

#define PRDB_SQL_INSERT_MDL \
"INSERT INTO Models (\n\
ModelID,\n\
AProjectID,\n\
ModelName,\n\
TransName,\n\
DateCreated,\n\
AnalisisType,\n\
ModelType,\n\
NumCompQNT,\n\
NumCompQLT,\n\
HCubeVolume,\n\
HarmonyCount,\n\
Nonlinearity,\n\
PredH,\n\
PredCKO,\n\
UseSECV,\n\
SpectrumType,\n\
SpRangeRealDown,\n\
SpRangeRealUp,\n\
Preproc)\n\
SELECT %d, AProjectID, '%s', '%s', #%s#, %d, %d, %d, %d, %s, %d, %s, %s, %s, %d, %d, %d, %d, '%s'\n\
FROM AProjects\n\
WHERE APTitle = '%s' AND ApplianceID = (SELECT ApplianceID FROM Appliances WHERE AcSN = %d)"

#define PRDB_SQL_UPDATE_MDL \
"UPDATE Models\n\
SET ModelName = '%s',\n\
TransName = '%s',\n\
DateCreated = #%s#,\n\
AnalisisType = %d,\n\
ModelType = %d,\n\
NumCompQNT = %d,\n\
NumCompQLT = %d,\n\
HCubeVolume = %s,\n\
HarmonyCount = %d,\n\
Nonlinearity = %s,\n\
PredH = %s,\n\
PredCKO = %s,\n\
UseSECV = %d,\n\
SpectrumType = %d,\n\
SpRangeRealDown = %d,\n\
SpRangeRealUp = %d,\n\
Preproc = '%s'\n\
WHERE ModelID = %d"

#define PRDB_SQL_DELETE_MDL \
"DELETE FROM Models\n\
WHERE ModelName = '%s' AND AProjectID = (SELECT AProjectID FROM AProjects\n\
WHERE APTitle = '%s' AND ApplianceID = (SELECT ApplianceID FROM Appliances WHERE AcSN = %d))"

#define PRDB_SQL_ISEXISTS_MDL \
"SELECT COUNT(*)\n\
FROM Models\n\
WHERE ModelName = '%s' AND\n\
AProjectID = (SELECT AProjectID FROM AProjects WHERE APTitle = '%s' AND ApplianceID = (SELECT ApplianceID FROM Appliances WHERE AcSN = %d))"

#define PRDB_SQL_GET_MDL_ID_MAX \
"SELECT MAX(ModelID)\n\
FROM Models"

#define PRDB_SQL_GET_MDL_ID \
"SELECT ModelID\n\
FROM Models\n\
WHERE ModelName = '%s' AND AProjectID = (SELECT AProjectID FROM AProjects\n\
WHERE APTitle = '%s' AND ApplianceID = (SELECT ApplianceID FROM Appliances WHERE AcSN = %d))"

#define PRDB_SQL_GET_MDL_NAME \
"SELECT ModelName\n\
FROM Models\n\
WHERE ModelID = %d"

#define PRDB_SQL_READ_MDL \
"SELECT ModelID,\n\
ModelName,\n\
TransName,\n\
DateCreated,\n\
AnalisisType,\n\
ModelType,\n\
NumCompQNT,\n\
NumCompQLT,\n\
HCubeVolume,\n\
HarmonyCount,\n\
Nonlinearity,\n\
PredH,\n\
PredCKO,\n\
UseSECV,\n\
SpectrumType,\n\
SpRangeRealDown,\n\
SpRangeRealUp,\n\
Preproc\n\
FROM Models\n\
WHERE AProjectID = (SELECT AProjectID FROM AProjects\n\
WHERE APTitle = '%s' AND ApplianceID = (SELECT ApplianceID FROM Appliances WHERE AcSN = %d))"

////////////////////////////////////////
//		����������� �����			  //
////////////////////////////////////////

#define PRDB_SQL_CREATE_TAB_PR_MDL_SE \
"CREATE TABLE ModelSpExclude (\n\
ModelSpEID		COUNTER		PRIMARY KEY,\n\
ModelID			INT			NOT NULL REFERENCES Models(ModelID) ON UPDATE CASCADE ON DELETE CASCADE,\n\
SpDataExclude	INT			NOT NULL)"

#define PRDB_SQL_INSERT_MDL_SE \
"INSERT INTO ModelSpExclude (\n\
ModelID,\n\
SpDataExclude)\n\
VALUES (%d, %d)"

#define PRDB_SQL_DELETE_MDL_SE \
"DELETE FROM ModelSpExclude\n\
WHERE ModelID = %d"

#define PRDB_SQL_READ_MDL_SE \
"SELECT SpDataExclude\n\
FROM ModelSpExclude\n\
WHERE ModelID = %d\n\
ORDER BY ModelSpEID"


/////////////////////////////////////////
// ������� ��� ����������� � ��������� //
/////////////////////////////////////////

#define PRDB_SQL_CREATE_TAB_MDL_SP \
"CREATE TABLE ModelSpectra	(\n\
ModelSpectraID	COUNTER		PRIMARY KEY,\n\
ModelID			INT			NOT NULL REFERENCES Models(ModelID) ON UPDATE CASCADE ON DELETE CASCADE,\n\
SpectrumID		INT			NOT NULL REFERENCES Spectrums(SpectrumID) ON UPDATE CASCADE ON DELETE CASCADE,\n\
IsCalib			BIT			NOT NULL,\n\
IsValid			BIT			NOT NULL,\n\
CONSTRAINT PRMDLSP UNIQUE (ModelID, SpectrumID))"

#define PRDB_SQL_INSERT_MDL_SP \
"INSERT INTO ModelSpectra (\n\
ModelSpectraID,\n\
ModelID,\n\
SpectrumID,\n\
IsCalib,\n\
IsValid)\n\
VALUES (%d, %d, %d, %d, %d)"

#define PRDB_SQL_DELETE_MDL_SP \
"DELETE FROM ModelSpectra\n\
WHERE ModelID = %d"

#define PRDB_SQL_MAX_MDL_SP \
"SELECT MAX(ModelSpectraID) FROM ModelSpectra"

#define PRDB_SQL_READ_MDL_SP \
"SELECT SpectrumID,\n\
IsCalib,\n\
IsValid\n\
FROM ModelSpectra\n\
WHERE ModelID = %d"

////////////////////////////////////////
//			���������� ������		  //
////////////////////////////////////////

#define PRDB_SQL_CREATE_TAB_PR_MDL_IX \
"CREATE TABLE ModelIX (\n\
ModelIXID		COUNTER		PRIMARY KEY,\n\
ModelID			INT			NOT NULL REFERENCES Models(ModelID) ON UPDATE CASCADE ON DELETE CASCADE,\n\
IndexID			INT			NOT NULL REFERENCES Indices(IndexID) ON UPDATE CASCADE ON DELETE CASCADE,\n\
PredCKOIX		FLOAT,\n\
CalibDown		FLOAT,\n\
CalibUp			FLOAT,\n\
CorrB			FLOAT,\n\
CorrA			FLOAT)"

#define PRDB_SQL_INSERT_MDL_IX \
"INSERT INTO ModelIX (\n\
ModelID,\n\
IndexID,\n\
PredCKOIX,\n\
CalibDown,\n\
CalibUp,\n\
CorrB,\n\
CorrA)\n\
SELECT %d, IndexID, %s, %s, %s, %s, %s\n\
FROM Indices\n\
WHERE IdxName = '%s' AND AProjectID = (SELECT AProjectID FROM AProjects\n\
WHERE APTitle = '%s' AND ApplianceID = (SELECT ApplianceID FROM Appliances WHERE AcSN = %d))"

#define PRDB_SQL_DELETE_MDL_IX \
"DELETE FROM ModelIX\n\
WHERE ModelID = %d"

#define PRDB_SQL_READ_MDL_IX \
"SELECT i.IdxName,\n\
m.PredCKOIX,\n\
m.CalibDown,\n\
m.CalibUp,\n\
m.CorrB,\n\
m.CorrA\n\
FROM ModelIX AS m\n\
LEFT OUTER JOIN Indices AS i\n\
ON m.IndexID = i.IndexID\n\
WHERE ModelID = %d"

////////////////////////////////////////
//				�����				  //
////////////////////////////////////////

#define PRDB_SQL_CREATE_TAB_PR_MHD \
"CREATE TABLE Methods (\n\
MethodID		COUNTER		PRIMARY KEY,\n\
AProjectID		INT			NOT NULL REFERENCES AProjects(AProjectID) ON UPDATE CASCADE ON DELETE CASCADE,\n\
MethodName		TEXT(%d)	NOT NULL,\n\
DateCreated		DATETIME	NOT NULL,\n\
AnalisisType	INT			NOT NULL,\n\
IndexAsHum		TEXT(%d)	NOT NULL,\n\
TempRangeRDown	INT			NOT NULL,\n\
TempRangeRUp	INT			NOT NULL,\n\
TempDiff		INT			NOT NULL,\n\
CONSTRAINT PRMHD UNIQUE (MethodName, DateCreated))"

#define PRDB_SQL_INSERT_MHD \
"INSERT INTO Methods (\n\
MethodID,\n\
AProjectID,\n\
MethodName,\n\
DateCreated,\n\
AnalisisType,\n\
IndexAsHum,\n\
TempRangeRDown,\n\
TempRangeRUp,\n\
TempDiff)\n\
SELECT %d, AProjectID, '%s', #%s#, %d, '%s', %d, %d, %d\n\
FROM AProjects\n\
WHERE APTitle = '%s' AND ApplianceID = (SELECT ApplianceID FROM Appliances WHERE AcSN = %d)"

#define PRDB_SQL_UPDATE_MHD \
"UPDATE Methods\n\
SET MethodName = '%s',\n\
DateCreated = #%s#,\n\
AnalisisType = %d,\n\
IndexAsHum = '%s',\n\
TempRangeRDown = %d,\n\
TempRangeRUp = %d,\n\
TempDiff = %d\n\
WHERE MethodID = %d"

#define PRDB_SQL_DELETE_MHD_EX \
"DELETE FROM Methods\n\
WHERE MethodName = '%s' AND AProjectID = (SELECT AProjectID FROM AProjects\n\
WHERE APTitle = '%s' AND ApplianceID = (SELECT ApplianceID FROM Appliances WHERE AcSN = %d))"

#define PRDB_SQL_IS_EXISTS_MHD_EX \
"SELECT COUNT(MethodID)\n\
FROM Methods\n\
WHERE MethodName = '%s' AND AProjectID = (SELECT AProjectID FROM AProjects\n\
WHERE APTitle = '%s' AND ApplianceID = (SELECT ApplianceID FROM Appliances WHERE AcSN = %d))"

#define PRDB_SQL_GET_MHD_ID_MAX \
"SELECT MAX(MethodID) FROM Methods"

#define PRDB_SQL_GET_MHD_ID \
"SELECT MethodID\n\
FROM Methods\n\
WHERE MethodName = '%s' AND AProjectID = (SELECT AProjectID FROM AProjects\n\
WHERE APTitle = '%s' AND ApplianceID = (SELECT ApplianceID FROM Appliances WHERE AcSN = %d))"

#define PRDB_SQL_READ_MHD \
"SELECT MethodID,\n\
MethodName,\n\
DateCreated,\n\
AnalisisType,\n\
IndexAsHum,\n\
TempRangeRDown,\n\
TempRangeRUp,\n\
TempDiff\n\
FROM Methods\n\
WHERE AProjectID = (SELECT AProjectID FROM AProjects\n\
WHERE APTitle = '%s' AND ApplianceID = (SELECT ApplianceID FROM Appliances WHERE AcSN = %d))"

////////////////////////////////////////
//			���������� ������		  //
////////////////////////////////////////

#define PRDB_SQL_CREATE_TAB_PR_MHD_MIX \
"CREATE TABLE MethodIX (\n\
MethodIXID		COUNTER		PRIMARY KEY,\n\
MethodID		INT			NOT NULL REFERENCES Methods(MethodID) ON UPDATE CASCADE ON DELETE CASCADE,\n\
IndexID			INT			NOT NULL REFERENCES Indices(IndexID) ON UPDATE CASCADE ON DELETE CASCADE,\n\
MainModel		TEXT(%d)	NOT NULL,\n\
StHum			FLOAT		NOT NULL,\n\
UseStdMoisture	BIT)"

#define PRDB_SQL_INSERT_MHD_MIX \
"INSERT INTO MethodIX (\n\
MethodIXID,\n\
MethodID,\n\
IndexID,\n\
MainModel,\n\
StHum,\n\
UseStdMoisture)\n\
SELECT %d, %d, IndexID, '%s', %s, %d\n\
FROM Indices\n\
WHERE IdxName = '%s' AND AProjectID = (SELECT AProjectID FROM AProjects WHERE APTitle = '%s' AND\n\
ApplianceID = (SELECT ApplianceID FROM Appliances WHERE AcSN = %d))"

#define PRDB_SQL_DELETE_MHD_MIX \
"DELETE FROM MethodIX\n\
WHERE MethodID = %d"

#define PRDB_SQL_GET_MHD_MIX_ID_MAX \
"SELECT MAX(MethodIXID) FROM MethodIX"

#define PRDB_SQL_READ_MHD_MIX \
"SELECT x.MethodIXID,\n\
i.IdxName,\n\
x.MainModel,\n\
x.StHum,\n\
x.UseStdMoisture\n\
FROM (MethodIX AS x\n\
LEFT OUTER JOIN Indices AS i\n\
ON x.IndexID = i.IndexID)\n\
WHERE x.MethodID = %d"

////////////////////////////////////////
//			������ ������			  //
////////////////////////////////////////

#define PRDB_SQL_CREATE_TAB_PR_MHD_MIX_IX \
"CREATE TABLE MethodIXMod (\n\
MethodIXModID	COUNTER		PRIMARY KEY,\n\
MethodIXID		INT			NOT NULL REFERENCES MethodIX(MethodIXID) ON UPDATE CASCADE ON DELETE CASCADE,\n\
ModelID			INT			NOT NULL REFERENCES Models(ModelID) ON UPDATE CASCADE ON DELETE CASCADE)"

#define PRDB_SQL_INSERT_MHD_MIX_IX \
"INSERT INTO MethodIXMod (\n\
MethodIXID,\n\
ModelID)\n\
SELECT %d, ModelID\n\
FROM Models\n\
WHERE ModelName = '%s' AND AProjectID = (SELECT AProjectID FROM AProjects\n\
WHERE APTitle = '%s' AND ApplianceID = (SELECT ApplianceID FROM Appliances WHERE AcSN = %d))"

#define PRDB_SQL_READ_MHD_MIX_IX \
"SELECT ModelName\n\
FROM Models\n\
WHERE ModelID IN (SELECT ModelID FROM MethodIXMod WHERE MethodIXID = %d)"

////////////////////////////////////////
//	������ ��� ������������� �������  //
////////////////////////////////////////

#define PRDB_SQL_CREATE_TAB_PR_MHD_MQLT \
"CREATE TABLE MethodModQLT (\n\
MethodModQLTID	COUNTER		PRIMARY KEY,\n\
MethodID		INT			NOT NULL REFERENCES Methods(MethodID) ON UPDATE CASCADE ON DELETE CASCADE,\n\
ModelID			INT			NOT NULL REFERENCES Models(ModelID) ON UPDATE CASCADE ON DELETE CASCADE,\n\
ModelInd		INT			NOT NULL,\n\
CONSTRAINT PRMTDQLTMOD UNIQUE (MethodID, ModelID))"

#define PRDB_SQL_INSERT_MHD_MQLT \
"INSERT INTO MethodModQLT (\n\
MethodID,\n\
ModelID,\n\
ModelInd)\n\
SELECT %d, ModelID, %d\n\
FROM Models\n\
WHERE ModelName = '%s' AND AProjectID = (SELECT AProjectID FROM AProjects\n\
WHERE APTitle = '%s' AND ApplianceID = (SELECT ApplianceID FROM Appliances WHERE AcSN = %d))"

#define PRDB_SQL_DELETE_MHD_MQLT_ALL \
"DELETE FROM MethodModQLT\n\
WHERE MethodID = %d"

#define PRDB_SQL_READ_MHD_MQLT \
"SELECT MethodModQLTID,\n\
ModelInd,\n\
ModelID\n\
FROM MethodModQLT\n\
WHERE MethodID = %d"

////////////////////////////////////////
//	������ ������������� �������	  //
////////////////////////////////////////

#define PRDB_SQL_CREATE_TAB_PR_MHD_QLT_FORM \
"CREATE TABLE MethodQltForms (\n\
MethodQltFormID	COUNTER		PRIMARY KEY,\n\
MethodID		INT			NOT NULL REFERENCES Methods(MethodID) ON UPDATE CASCADE ON DELETE CASCADE,\n\
Type			INT			NOT NULL,\n\
ModelInd		INT			NOT NULL)"

#define PRDB_SQL_INSERT_MHD_QLT_FORM \
"INSERT INTO MethodQltForms(\n\
MethodID,\n\
Type,\n\
ModelInd)\n\
VALUES (%d, %d, %d)"

#define PRDB_SQL_DELETE_MHD_QLT_FORM_ALL \
"DELETE FROM MethodQltForms\n\
WHERE MethodID = %d"

#define PRDB_SQL_READ_MHD_QLT_FORM \
"SELECT Type,\n\
ModelInd\n\
FROM MethodQltForms\n\
WHERE MethodID = %d\n\
ORDER BY MethodQltFormID"

////////////////////////////////////////
// END                                //
////////////////////////////////////////

#endif PRDBPROJECTSQL_H