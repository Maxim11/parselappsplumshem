#include "../stdafx.h"

#include "PRDBTemplate.h"

#define VerifySuccess if (res != LumexDB::enSuccess) { assert(false); return false; }
#define VerifySuccessAndNoData if (res != LumexDB::enSuccess && res != LumexDB::enNoData) { assert(false); return false; }
#define VerifyDataPointer if (pData == NULL) {	assert(false); return false; }
#define StringLen (int)(pData->m_Size / sizeof(TCHAR) + 1)
#define BufferLen (pData->m_Size + sizeof(TCHAR))

CPRDBTemplate::CPRDBTemplate() : CLDatabase()
{
}

CPRDBTemplate::~CPRDBTemplate()
{
}

bool CPRDBTemplate::TemplateAddEx(CString TemplateName_OLD, const PRTemplate *const pTmpl)
{
	if(IsTemplateExists(TemplateName_OLD))
//		return addHistoryEvent(templateUpdate(TemplateName_OLD, pTmpl), prdbHistoryType::PRDB_HISTORY_TYPE_UPDATE, prdbHistoryTarget::PRDB_HISTORY_TRG_APROJECTS );
		return templateUpdate(TemplateName_OLD, pTmpl);
	else
//		return addHistoryEvent(templateAdd(pTmpl), prdbHistoryType::PRDB_HISTORY_TYPE_INSERT, prdbHistoryTarget::PRDB_HISTORY_TRG_APROJECTS );
		return templateAdd(pTmpl);
}


bool CPRDBTemplate::TemplateDelete(CString TemplateName)
{
	m_sSQL.Format(_T(PRDB_SQL_TMPL_DELETE_TMPL), TemplateName);
//	return addHistoryEvent( executeSQL(), prdbHistoryType::PRDB_HISTORY_TYPE_DELETE, prdbHistoryTarget::PRDB_HISTORY_TRG_APROJECTS);
	return executeSQL();
}

bool CPRDBTemplate::IsTemplateExists(CString TemplateName)
{
	m_sSQL.Format(_T(PRDB_SQL_TMPL_ISEXISTS_TMPL), replaceChar(TemplateName));
	return isDataExist() > 0;
}

bool CPRDBTemplate::TemplatesRead()
{
	m_sSQL.Format(_T(PRDB_SQL_TMPL_READ_TMPL));
//	return addHistoryEvent( selectSQL(), prdbHistoryType::PRDB_HISTORY_TYPE_READ, prdbHistoryTarget::PRDB_HISTORY_TRG_APROJECTS );
	return selectSQL();
}

bool CPRDBTemplate::TemplateGet(PRTemplate *const pTmpl)
{
	if(m_pDataSet == NULL)
	{
		assert(false);
		return false;
	}

	LumexDB::CDataPtr<ILumexDBDataSet::TColumnData> pData = m_pDataSet->GetColumn(0);
	VerifyDataPointer;	
	long lTmpl;
	LumexDB::EnOperationResult res = pData->GetData(&lTmpl, sizeof(lTmpl));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(1);
	VerifyDataPointer;	
	LPTSTR lpBuf = pTmpl->TemplateName.GetBuffer(StringLen);	
	res = pData->GetData(lpBuf, BufferLen, false);
	pTmpl->TemplateName.ReleaseBuffer();
	VerifySuccess;
	returnChar(pTmpl->TemplateName);

	pData = m_pDataSet->GetColumn(2);
	VerifyDataPointer;
	lpBuf = pTmpl->TemplateNote.GetBuffer(StringLen);
	res = pData->GetData(lpBuf, BufferLen, false);
	pTmpl->TemplateNote.ReleaseBuffer();
	VerifySuccess;
	returnChar(pTmpl->TemplateNote);

	pData = m_pDataSet->GetColumn(3);
	VerifyDataPointer;
	TIMESTAMP_STRUCT date;
	res = pData->GetData(&date, sizeof(date));
	VerifySuccess;		
	pTmpl->DateCreated = CTime(date.year, date.month, date.day, date.hour, date.minute, date.second);

	pData = m_pDataSet->GetColumn(4);
	VerifyDataPointer;		
	res = pData->GetData(&pTmpl->NCompMean, sizeof(pTmpl->NCompMean));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(5);
	VerifyDataPointer;		
	res = pData->GetData(&pTmpl->HiperMean, sizeof(pTmpl->HiperMean));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(6);
	VerifyDataPointer;		
	res = pData->GetData(&pTmpl->NHarmMean, sizeof(pTmpl->NHarmMean));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(7);
	VerifyDataPointer;		
	res = pData->GetData(&pTmpl->KfnlMean, sizeof(pTmpl->KfnlMean));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(8);
	VerifyDataPointer;		
	res = pData->GetData(&pTmpl->SpecLeftMean, sizeof(pTmpl->SpecLeftMean));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(9);
	VerifyDataPointer;		
	res = pData->GetData(&pTmpl->SpecRightMean, sizeof(pTmpl->SpecRightMean));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(10);
	VerifyDataPointer;		
	res = pData->GetData(&pTmpl->LastCrit, sizeof(pTmpl->LastCrit));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(11);
	VerifyDataPointer;		
	res = pData->GetData(&pTmpl->PrjSpecMin, sizeof(pTmpl->PrjSpecMin));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(12);
	VerifyDataPointer;		
	res = pData->GetData(&pTmpl->PrjSpecMax, sizeof(pTmpl->PrjSpecMax));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(13);
	VerifyDataPointer;		
	res = pData->GetData(&pTmpl->PrjResol, sizeof(pTmpl->PrjResol));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(14);
	VerifyDataPointer;		
	res = pData->GetData(&pTmpl->PrjZeroFilling, sizeof(pTmpl->PrjZeroFilling));
	VerifySuccess;

	if(m_pDB == NULL || !m_pDB->IsConnected())
	{
		assert(false);
		return false;
	}

	CString sql;
	// ������ ����������� ������������ �����
	{
		sql.Format(_T(PRDB_SQL_TMPL_READ_SPEXC), lTmpl);
		
		ILumexDBDataSet *pDataSet;
		LumexDB::EnOperationResult res = m_pDB->ExecuteSQL(sql, &pDataSet);
		if (res != LumexDB::enSuccess || pDataSet == NULL)
		{
			assert(false);
			return false;
		}

		pTmpl->SpDataExclude.clear();

		while(!pDataSet->IsEOF())
		{
			int nData;
			pData = pDataSet->GetColumn(0);
			VerifyDataPointer;	
			res = pData->GetData(&nData, sizeof(nData));
			VerifySuccess;
			
			pTmpl->SpDataExclude.push_back(nData);
			
			res = pDataSet->NextRow();
			VerifySuccessAndNoData;
		}
		pDataSet->Release();
	}

	// ������ ����� �����������
	{
		sql.Format(_T(PRDB_SQL_TMPL_READ_STEP), lTmpl);
		
		ILumexDBDataSet *pDataSet2;
		LumexDB::EnOperationResult res = m_pDB->ExecuteSQL(sql, &pDataSet2);
		if (res != LumexDB::enSuccess || pDataSet2 == NULL)
		{
			assert(false);
			return false;
		}

		pTmpl->Steps.clear();

		while(!pDataSet2->IsEOF())
		{
			PRTemplateStep NewStep;

			LPTSTR lpBuf;
			int lStep;

			pData = pDataSet2->GetColumn(0);
			VerifyDataPointer;	
			res = pData->GetData(&lStep, sizeof(lStep));
			VerifySuccess;

			pData = pDataSet2->GetColumn(1);
			VerifyDataPointer;	
			res = pData->GetData(&NewStep.StepNum, sizeof(NewStep.StepNum));
			VerifySuccess;

			pData = pDataSet2->GetColumn(2);
			VerifyDataPointer;	
			res = pData->GetData(&NewStep.ModelType, sizeof(NewStep.ModelType));
			VerifySuccess;

			pData = pDataSet2->GetColumn(3);
			VerifyDataPointer;	
			res = pData->GetData(&NewStep.SpType, sizeof(NewStep.SpType));
			VerifySuccess;

			pData = pDataSet2->GetColumn(4);
			VerifyDataPointer;	
			res = pData->GetData(&NewStep.NCompPlus, sizeof(NewStep.NCompPlus));
			VerifySuccess;

			pData = pDataSet2->GetColumn(5);
			VerifyDataPointer;	
			res = pData->GetData(&NewStep.NCompStep, sizeof(NewStep.NCompStep));
			VerifySuccess;

			pData = pDataSet2->GetColumn(6);
			VerifyDataPointer;	
			res = pData->GetData(&NewStep.HiperPlus, sizeof(NewStep.HiperPlus));
			VerifySuccess;

			pData = pDataSet2->GetColumn(7);
			VerifyDataPointer;	
			res = pData->GetData(&NewStep.HiperStep, sizeof(NewStep.HiperStep));
			VerifySuccess;

			pData = pDataSet2->GetColumn(8);
			VerifyDataPointer;	
			res = pData->GetData(&NewStep.NHarmPlus, sizeof(NewStep.NHarmPlus));
			VerifySuccess;

			pData = pDataSet2->GetColumn(9);
			VerifyDataPointer;	
			res = pData->GetData(&NewStep.NHarmStep, sizeof(NewStep.NHarmStep));
			VerifySuccess;

			pData = pDataSet2->GetColumn(10);
			VerifyDataPointer;	
			res = pData->GetData(&NewStep.KfnlPlus, sizeof(NewStep.KfnlPlus));
			VerifySuccess;

			pData = pDataSet2->GetColumn(11);
			VerifyDataPointer;	
			res = pData->GetData(&NewStep.KfnlStep, sizeof(NewStep.KfnlStep));
			VerifySuccess;

			pData = pDataSet2->GetColumn(12);
			VerifyDataPointer;	
			res = pData->GetData(&NewStep.SpecLeftPlus, sizeof(NewStep.SpecLeftPlus));
			VerifySuccess;

			pData = pDataSet2->GetColumn(13);
			VerifyDataPointer;	
			res = pData->GetData(&NewStep.SpecLeftStep, sizeof(NewStep.SpecLeftStep));
			VerifySuccess;

			pData = pDataSet2->GetColumn(14);
			VerifyDataPointer;	
			res = pData->GetData(&NewStep.SpecRightPlus, sizeof(NewStep.SpecRightPlus));
			VerifySuccess;

			pData = pDataSet2->GetColumn(15);
			VerifyDataPointer;	
			res = pData->GetData(&NewStep.SpecRightStep, sizeof(NewStep.SpecRightStep));
			VerifySuccess;

			pData = pDataSet2->GetColumn(16);
			VerifyDataPointer;	
			res = pData->GetData(&NewStep.MaxModels, sizeof(NewStep.MaxModels));
			VerifySuccess;

			pData = pDataSet2->GetColumn(17);
			VerifyDataPointer;	
			res = pData->GetData(&NewStep.MainCrit, sizeof(NewStep.MainCrit));
			VerifySuccess;

			pData = pDataSet2->GetColumn(18);
			VerifyDataPointer;	
			res = pData->GetData(&NewStep.SortCrit, sizeof(NewStep.SortCrit));
			VerifySuccess;

			pData = pDataSet2->GetColumn(19);
			VerifyDataPointer;	
			res = pData->GetData(&NewStep.IsTest, sizeof(NewStep.IsTest));
			VerifySuccess;

			pData = pDataSet2->GetColumn(20);
			VerifyDataPointer;	
			res = pData->GetData(&NewStep.Operation, sizeof(NewStep.Operation));
			VerifySuccess;

			pData = pDataSet2->GetColumn(21);
			VerifyDataPointer;	
			res = pData->GetData(&NewStep.Preproc.pMax, sizeof(&NewStep.Preproc.pMax));
			VerifySuccess;

			pData = pDataSet2->GetColumn(22);
			VerifyDataPointer;	
			res = pData->GetData(&NewStep.Preproc.pBest, sizeof(&NewStep.Preproc.pBest));
			VerifySuccess;

			pData = pDataSet2->GetColumn(23);
			VerifyDataPointer;
			lpBuf = NewStep.Preproc.pAdd.GetBuffer(StringLen);
			res = pData->GetData(lpBuf, BufferLen, false);
			NewStep.Preproc.pAdd.ReleaseBuffer();
			VerifySuccess;
			returnChar(NewStep.Preproc.pAdd);

			pData = pDataSet2->GetColumn(24);
			VerifyDataPointer;	
			res = pData->GetData(&NewStep.Preproc.pA, sizeof(NewStep.Preproc.pA));
			VerifySuccess;

			pData = pDataSet2->GetColumn(25);
			VerifyDataPointer;	
			res = pData->GetData(&NewStep.Preproc.pB, sizeof(NewStep.Preproc.pB));
			VerifySuccess;

			pData = pDataSet2->GetColumn(26);
			VerifyDataPointer;	
			res = pData->GetData(&NewStep.Preproc.pM, sizeof(NewStep.Preproc.pM));
			VerifySuccess;

			pData = pDataSet2->GetColumn(27);
			VerifyDataPointer;	
			res = pData->GetData(&NewStep.Preproc.pD, sizeof(NewStep.Preproc.pD));
			VerifySuccess;

			pData = pDataSet2->GetColumn(28);
			VerifyDataPointer;	
			res = pData->GetData(&NewStep.Preproc.pC, sizeof(NewStep.Preproc.pC));
			VerifySuccess;

			pData = pDataSet2->GetColumn(29);
			VerifyDataPointer;	
			res = pData->GetData(&NewStep.Preproc.pN, sizeof(NewStep.Preproc.pN));
			VerifySuccess;

			pData = pDataSet2->GetColumn(30);
			VerifyDataPointer;	
			res = pData->GetData(&NewStep.Preproc.p1, sizeof(NewStep.Preproc.p1));
			VerifySuccess;

			pData = pDataSet2->GetColumn(31);
			VerifyDataPointer;	
			res = pData->GetData(&NewStep.Preproc.p2, sizeof(NewStep.Preproc.p2));
			VerifySuccess;

			pData = pDataSet2->GetColumn(32);
			VerifyDataPointer;	
			res = pData->GetData(&NewStep.Preproc.pNo, sizeof(NewStep.Preproc.pNo));
			VerifySuccess;

			pData = pDataSet2->GetColumn(33);
			VerifyDataPointer;	
			res = pData->GetData(&NewStep.Preproc.pUseBest, sizeof(NewStep.Preproc.pUseBest));
			VerifySuccess;

			pData = pDataSet2->GetColumn(34);
			VerifyDataPointer;	
			res = pData->GetData(&NewStep.NHarmType, sizeof(NewStep.NHarmType));
			VerifySuccess;

			pData = pDataSet2->GetColumn(35);
			VerifyDataPointer;	
			res = pData->GetData(&NewStep.KfnlType, sizeof(NewStep.KfnlType));
			VerifySuccess;

			pData = pDataSet2->GetColumn(36);
			VerifyDataPointer;	
			res = pData->GetData(&NewStep.Outs.limMah, sizeof(NewStep.Outs.limMah));
			VerifySuccess;

			pData = pDataSet2->GetColumn(37);
			VerifyDataPointer;	
			res = pData->GetData(&NewStep.Outs.limSEC, sizeof(NewStep.Outs.limSEC));
			VerifySuccess;

			pData = pDataSet2->GetColumn(38);
			VerifyDataPointer;	
			res = pData->GetData(&NewStep.Outs.limSECV, sizeof(NewStep.Outs.limSECV));
			VerifySuccess;

			pData = pDataSet2->GetColumn(39);
			VerifyDataPointer;	
			res = pData->GetData(&NewStep.Outs.limBound, sizeof(NewStep.Outs.limBound));
			VerifySuccess;

			pData = pDataSet2->GetColumn(40);
			VerifyDataPointer;	
			res = pData->GetData(&NewStep.Outs.chMah, sizeof(NewStep.Outs.chMah));
			VerifySuccess;

			pData = pDataSet2->GetColumn(41);
			VerifyDataPointer;	
			res = pData->GetData(&NewStep.Outs.chSEC, sizeof(NewStep.Outs.chSEC));
			VerifySuccess;

			pData = pDataSet2->GetColumn(42);
			VerifyDataPointer;	
			res = pData->GetData(&NewStep.Outs.chSECV, sizeof(NewStep.Outs.chSECV));
			VerifySuccess;

			pData = pDataSet2->GetColumn(43);
			VerifyDataPointer;	
			res = pData->GetData(&NewStep.Outs.chBad, sizeof(NewStep.Outs.chBad));
			VerifySuccess;

			pData = pDataSet2->GetColumn(44);
			VerifyDataPointer;	
			res = pData->GetData(&NewStep.Outs.chBound, sizeof(NewStep.Outs.chBound));
			VerifySuccess;

			pTmpl->Steps.push_back(NewStep);
			
			res = pDataSet2->NextRow();
			VerifySuccessAndNoData;
		}
		pDataSet2->Release();
	}

	return true;
}

bool CPRDBTemplate::templateAdd(const PRTemplate *const pTmpl)
{
	m_sSQL = _T(PRDB_SQL_TMPL_GET_TMPL_ID_MAX);
	long lTmpl = 1 + isDataExist();
	if(lTmpl <= 0)
		return false;

	m_sSQL.Format(_T(PRDB_SQL_TMPL_INSERT_TMPL), lTmpl, replaceChar(pTmpl->TemplateName), replaceChar(pTmpl->TemplateNote),
		pTmpl->DateCreated.Format(m_pszFormatCTime), pTmpl->NCompMean, floatWithPoint(pTmpl->HiperMean),
		pTmpl->NHarmMean, floatWithPoint(pTmpl->KfnlMean), pTmpl->SpecLeftMean, pTmpl->SpecRightMean, pTmpl->LastCrit,
		pTmpl->PrjSpecMin, pTmpl->PrjSpecMax, pTmpl->PrjResol, pTmpl->PrjZeroFilling);

	if(!executeSQL())
		return false;

	return templateAddVect(lTmpl, pTmpl);
}

bool CPRDBTemplate::templateUpdate(CString TemplateName, const PRTemplate *const pTmpl)
{
	m_sSQL.Format(_T(PRDB_SQL_TMPL_GET_TMPL_ID), replaceChar(TemplateName));
	long lTmpl = isDataExist();
	if(lTmpl <= 0)
		return false;

	m_sSQL.Format(_T(PRDB_SQL_TMPL_UPDATE_TMPL), replaceChar(pTmpl->TemplateName), replaceChar(pTmpl->TemplateNote),
		pTmpl->DateCreated.Format(m_pszFormatCTime), pTmpl->NCompMean, floatWithPoint(pTmpl->HiperMean),
		pTmpl->NHarmMean, floatWithPoint(pTmpl->KfnlMean), pTmpl->SpecLeftMean, pTmpl->SpecRightMean, pTmpl->LastCrit,
		pTmpl->PrjSpecMin, pTmpl->PrjSpecMax, pTmpl->PrjResol, pTmpl->PrjZeroFilling, replaceChar(TemplateName));

	if(!executeSQL())
		return false;

	m_sSQL.Format( _T(PRDB_SQL_TMPL_DELETE_SPEXC), lTmpl);
	if(!executeSQL())
		return false;

	m_sSQL.Format( _T(PRDB_SQL_TMPL_DELETE_STEP_ALL), lTmpl);
	if(!executeSQL())
		return false;

	return templateAddVect(lTmpl, pTmpl);
}

bool CPRDBTemplate::templateAddVect(long lTmpl, const PRTemplate *const pTmpl)
{
	// ������ ����������� ������������ �����
	if(pTmpl->SpDataExclude.size() != NULL)
	{
		for(vector<int>::const_iterator it=pTmpl->SpDataExclude.begin(); it!=pTmpl->SpDataExclude.end(); ++it)
		{
			m_sSQL.Format(_T(PRDB_SQL_TMPL_INSERT_SPEXC), lTmpl, (*it));
			if(!executeSQL())
				return false;
		}
	}

	// ������ ����� �����������

	if(pTmpl->Steps.size() != NULL)
	{
		for(vector<PRTemplateStep>::const_iterator it=pTmpl->Steps.begin(); it!=pTmpl->Steps.end(); ++it)
		{
			m_sSQL.Format(_T(PRDB_SQL_TMPL_INSERT_STEP), lTmpl, it->StepNum, it->ModelType, it->SpType,	it->NCompPlus,
				it->NCompStep, floatWithPoint(it->HiperPlus), floatWithPoint(it->HiperStep), it->NHarmPlus,
				it->NHarmStep, floatWithPoint(it->KfnlPlus), floatWithPoint(it->KfnlStep), it->SpecLeftPlus,
				it->SpecLeftStep, it->SpecRightPlus, it->SpecRightStep, it->MaxModels, it->MainCrit, it->SortCrit,
				it->IsTest,	it->Operation, it->NHarmType, it->KfnlType);
			if(!executeSQL())
				return false;

			m_sSQL.Format(_T(PRDB_SQL_TMPL_INSERT_PREPS), it->Preproc.pMax, it->Preproc.pBest, replaceChar(it->Preproc.pAdd), it->Preproc.pA,
				it->Preproc.pB, it->Preproc.pM, it->Preproc.pD,	it->Preproc.pC, it->Preproc.pN, it->Preproc.p1,
				it->Preproc.p2, it->Preproc.pNo, it->Preproc.pUseBest, lTmpl, it->StepNum);
			if(!executeSQL())
				return false;

			m_sSQL.Format(_T(PRDB_SQL_TMPL_INSERT_OUTS), floatWithPoint(it->Outs.limMah), floatWithPoint(it->Outs.limSEC),
				floatWithPoint(it->Outs.limSECV), floatWithPoint(it->Outs.limBound), it->Outs.chMah, it->Outs.chSEC,
				it->Outs.chSECV, it->Outs.chBad, it->Outs.chBound, lTmpl, it->StepNum);
			if(!executeSQL())
				return false;
		}
	}

	return true;
}

void CPRDBTemplate::createTables( vector<CString> *pV )
{
	CString tmp;
	tmp.Format(_T(PRDB_SQL_TMPL_CREATE_TAB_TMPL), TMPLDBName, TMPLDBNote);
	pV->push_back( tmp );
	pV->push_back(_T(PRDB_SQL_TMPL_CREATE_TAB_STEP));
	tmp.Format(_T(PRDB_SQL_TMPL_CREATE_TAB_PREPS), TMPLDBAdd);
	pV->push_back(tmp);
	pV->push_back(_T(PRDB_SQL_TMPL_CREATE_TAB_OUTS));
	pV->push_back(_T(PRDB_SQL_TMPL_CREATE_TAB_SPEXC));
}

long CPRDBTemplate::getCurrentVersion()
{
	return PRDB_TEMPLATE_CURRENT_VERSION;
}

CString CPRDBTemplate::getCurrentDBName()
{
	return _T("PARSEL_TEMPLATE_DATABASE_LUMEX_NIOKR");
}
