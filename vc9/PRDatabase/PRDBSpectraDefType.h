#ifndef _PARCEL_PRDB_SPECTRA_DEFTYPE_H_
#define _PARCEL_PRDB_SPECTRA_DEFTYPE_H_

#pragma once

#include <vector>

namespace apjdb
{
	// ���������� ����� ������������ �����
	const int APJDBProjectName		= 50;	// ����� �������
	const int APJDBParamName		= 50;	// ����� ����������
	const int APJDBParamUnit		= 20;	// ������ ��������� ���. ������
	const int APJDBParamFormat		= 6;	// ������� ���. ������
	const int APJDBSampleName		= 50;	// ����� �������
	const int APJDBSampleIden2		= 40;	// �������������� 2
	const int APJDBSampleIden3		= 255;	// �������������� 3

	// ������� ������ ���� ������
	const long APJDB_CURRENT_VERSION	= 104;


	// ���������� � ����������
	struct PRApjParam
	{
		CString	ParamName;		// ��� ����������
		CString	Unit;			// ������� ���������
		CString Format;			// ������ ������
		double StdMois;			// ����������� ���������
		bool UseStdMois;		// ���� ������������� ����������� ���������
	};

	struct PRApjRefData
	{
		CString ParamName;		// ��� ����������
		double RefData;			// �������� ����������
		bool IsDataExists;		// ���� ������� ���������
	};

	struct PRApjSpectrum
	{
		int SpecNum;			// ����� �������
		CTime DateCreated;		// ���� � ����� ����������� �������

		vector<double> Data;	// ������������ ������ 
	};

	struct PRApjSample
	{
		CString SampleName;		// ��� �������
		CString Iden2;			// ������������� 2
		CString Iden3;			// ������������� 3

		vector<PRApjRefData> RefData;	// ����������� ������
		vector<PRApjSpectrum> Spectra;	// �������
	};

	// ���������� � ���������� ������������
	struct PRApjProject
	{
		CString ProjectName;	// ��� �������

		CTime DateCreated;		// ���� ��������
		int DevNum;				// �������� ����� �������
		int DevType;			// ��� ������� ��� ID0
		int SpecRangeLow;		// ������ ������� ������������� ��������� ������� �������
		int SpecRangeUpper;		// ������� ������� ������������� ��������� ������� �������
		int ResSub;				// ���������� ����������� ������
		int ApodSub;			// ���������� ����������� ������
		int ZeroFill;			// ���������� ������
		bool UseRef;			// ������� ������������� �������� ������
		int ResRef;				// ���������� �������� ������
		int ApodRef;			// ���������� �������� ������
		int SmplScan;			// ����� ������ ����������
		int SmplMeas;			// ����� ��������� ����������
		int StdScan;			// ����� ������ �������
		int StdMeas;			// ����� ��������� �������
		bool Xtransform;		// ������� �-�������������
		bool IsSST;				// ������� ������������ �������
		bool UseBath;			// ����� ������������� ������������ �������/������
		double BathLength;		// ����� ������
		int TempLow;			// ������ ������� ��������� ���������� �������
		int TempUpper;			// ������� ������� ��������� ���������� �������
		int TempDiff;			// ���������� ���������� �� ����������� ���. �����

		vector<PRApjParam> Params;		// ����������
		vector<PRApjSample> Samples;	// �������
	};

}

#endif
