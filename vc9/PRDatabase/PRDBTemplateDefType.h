#ifndef _PARCEL_PRDB_TEMPLATE_DEFTYPE_H_
#define _PARCEL_PRDB_TEMPLATE_DEFTYPE_H_

#pragma once

#include <vector>

namespace tmpldb
{
	// ���������� ����� ������������ �����

	const long TMPLDBName				= 50;
	const long TMPLDBNote				= 50;
	const long TMPLDBAdd				= 40;
	const long TMPLDBPreproc			= 8;	// ������ �������������

	// ������� ������ ���� ������

	const long PRDB_TEMPLATE_CURRENT_VERSION	= 112;

	struct PRTemplatePreps
	{
		bool pA;
		bool pB;
		bool pM;
		bool pD;
		bool pC;
		bool pN;
		bool p1;
		bool p2;
		bool pNo;
		bool pUseBest;
		int pMax;
		int pBest;
		CString pAdd;
	};

	struct PRTemplateOuts
	{
		bool chMah;
		bool chSEC;
		bool chSECV;
		bool chBad;
		bool chBound;

		double limMah;
		double limSEC;
		double limSECV;
		double limBound;
	};

	struct PRTemplateStep			// ��� ������� �����������
	{
		int	StepNum;				// ���������� ����� ���� �����������
		int ModelType;
		int SpType;
		int NCompPlus;
		int NCompStep;
		double HiperPlus;
		double HiperStep;
		int NHarmPlus;
		int NHarmStep;
		int NHarmType;
		double KfnlPlus;
		double KfnlStep;
		int KfnlType;
		int SpecLeftPlus;
		int SpecLeftStep;
		int SpecRightPlus;
		int SpecRightStep;
		int MaxModels;
		int MainCrit;
		int SortCrit;
		bool IsTest;
		int Operation;

		PRTemplatePreps Preproc;
		PRTemplateOuts Outs;
	};

	struct PRTemplate				// ������ �����������
	{
		CString	TemplateName;		// ��� �������
		CString TemplateNote;
		CTime DateCreated;
		int NCompMean;
		double HiperMean;
		int NHarmMean;
		double KfnlMean;
		int SpecLeftMean;
		int SpecRightMean;
		int LastCrit;
		int PrjSpecMin;
		int PrjSpecMax;
		int PrjResol;
		int PrjZeroFilling;

		vector<int> SpDataExclude;				// ������ ����������� ������������ �����
		vector<PRTemplateStep> Steps;
	};

}

#endif
