#ifndef _PARCEL_PRDB_TEMPLATE_H_
#define _PARCEL_PRDB_TEMPLATE_H_

#pragma once

#include "LDatabase/LDatabase.h"
#include "PRDBTemplateSQL.h"
#include "PRDBTemplateDefType.h"

using namespace tmpldb;

class CPRDBTemplate : public CLDatabase
{
public:

	CPRDBTemplate( void );
	virtual ~CPRDBTemplate( void );

	bool TemplateAddEx(CString TemplateName_OLD, const PRTemplate *const );
	bool IsTemplateExists(CString TemplateName);
	bool TemplateDelete(CString TemplateName);
	bool TemplatesRead();
	bool TemplateGet(PRTemplate *const);

protected:

	bool templateAdd(const PRTemplate *const);
	bool templateUpdate(CString TemplateName, const PRTemplate *const );
	bool templateAddVect(long lTmpl, const PRTemplate *const pTmpl);
	bool stepAdd(CString TemplateName, const PRTemplateStep *const );
	bool stepUpdate(CString TemplateName, int StepNum, const PRTemplateStep *const );

	virtual void createTables( vector<CString> * );
	virtual long getCurrentVersion();
	virtual CString getCurrentDBName();	

};

#endif _PARCEL_PRDB_TEMPLATE_H_
