#include "../stdafx.h"

#include "PRDBMethod.h"

#define VerifySuccess if (res != LumexDB::enSuccess) { assert(false); return false; }
#define VerifySuccessAndNoData if (res != LumexDB::enSuccess && res != LumexDB::enNoData) { assert(false); return false; }
#define VerifyDataPointer if (pData == NULL) {	assert(false); return false; }
#define StringLen (int)(pData->m_Size / sizeof(TCHAR) + 1)
#define BufferLen (pData->m_Size + sizeof(TCHAR))

//////////////////////////////////////////////////////////
// ����� ������������� �������

CPRDBMethodQnt::CPRDBMethodQnt() : CLDatabase()
{
}

CPRDBMethodQnt::~CPRDBMethodQnt()
{
}

bool CPRDBMethodQnt::MethodAddEx(CString MethodName_OLD, const PRMethodQnt *const pMtd)
{
	if(IsMethodExists(MethodName_OLD))
		return methodUpdate(MethodName_OLD, pMtd);
	else
		return methodAdd(pMtd);
}


bool CPRDBMethodQnt::MethodDelete(CString MethodName)
{
	m_sSQL.Format(_T(MTDDB_SQL_DELETE_MTD), MethodName);
	return executeSQL();
}

bool CPRDBMethodQnt::SetMtdModelCorr(CString MethodName, CString ParamName, CString ModelName,
								  double CorrB, double CorrA)
{
	m_sSQL.Format(_T(MTDDB_SQL_GET_MTD_ID), replaceChar(MethodName));
	long lMtd = isDataExist();
	if(lMtd <= 0)
		return false;

	m_sSQL.Format(_T(MTDDB_SQL_GET_MTDPARAM_ID), lMtd, replaceChar(ParamName));
	long lMtdPar = isDataExist();
	if(lMtdPar <= 0)
		return false;

	m_sSQL.Format(_T(MTDDB_SQL_GET_MTDMODEL_ID), lMtd, replaceChar(ModelName));
	long lMtdMod = isDataExist();
	if(lMtdMod <= 0)
		return false;

	m_sSQL.Format(_T(MTDDB_SQL_SET_CORR_PARMODEL), doubleWithPoint(CorrB), doubleWithPoint(CorrA), lMtdPar, lMtdMod);
	if(!executeSQL())
		return false;

	return true;
}

bool CPRDBMethodQnt::IsMethodExists(CString MethodName)
{
	m_sSQL.Format(_T(MTDDB_SQL_ISEXISTS_MTD), replaceChar(MethodName));
	return isDataExist() > 0;
}

bool CPRDBMethodQnt::MethodsRead()
{
	m_sSQL.Format(_T(MTDDB_SQL_READ_MTD));
	return selectSQL();
}

bool CPRDBMethodQnt::MethodGet(PRMethodQnt *const pMtd)
{
	if(m_pDataSet == NULL)
	{
		assert(false);
		return false;
	}

	LumexDB::CDataPtr<ILumexDBDataSet::TColumnData> pData = m_pDataSet->GetColumn(0);
	VerifyDataPointer;	
	long lMtd;
	LumexDB::EnOperationResult res = pData->GetData(&lMtd, sizeof(lMtd));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(1);
	VerifyDataPointer;	
	LPTSTR lpBuf = pMtd->MethodName.GetBuffer(StringLen);	
	res = pData->GetData(lpBuf, BufferLen, false);
	pMtd->MethodName.ReleaseBuffer();
	VerifySuccess;
	returnChar(pMtd->MethodName);

	pData = m_pDataSet->GetColumn(2);
	VerifyDataPointer;
	TIMESTAMP_STRUCT date;
	res = pData->GetData(&date, sizeof(date));
	VerifySuccess;		
	pMtd->DateCreated = CTime(date.year, date.month, date.day, date.hour, date.minute, date.second);

	pData = m_pDataSet->GetColumn(3);
	VerifyDataPointer;	
	lpBuf = pMtd->MoisParam.GetBuffer(StringLen);	
	res = pData->GetData(lpBuf, BufferLen, false);
	pMtd->MoisParam.ReleaseBuffer();
	VerifySuccess;
	returnChar(pMtd->MoisParam);

	pData = m_pDataSet->GetColumn(4);
	VerifyDataPointer;		
	res = pData->GetData(&pMtd->DevNum, sizeof(pMtd->DevNum));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(5);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->DevType, sizeof(pMtd->DevType));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(6);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->SpecRangeLow, sizeof(pMtd->SpecRangeLow));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(7);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->SpecRangeUpper, sizeof(pMtd->SpecRangeUpper));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(8);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->ResSub, sizeof(pMtd->ResSub));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(9);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->ApodSub, sizeof(pMtd->ApodSub));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(10);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->ZeroFill, sizeof(pMtd->ZeroFill));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(11);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->UseRef, sizeof(pMtd->UseRef));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(12);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->ResRef, sizeof(pMtd->ResRef));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(13);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->ApodRef, sizeof(pMtd->ApodRef));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(14);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->SmplScan, sizeof(pMtd->SmplScan));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(15);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->SmplMeas, sizeof(pMtd->SmplMeas));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(16);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->StdScan, sizeof(pMtd->StdScan));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(17);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->StdMeas, sizeof(pMtd->StdMeas));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(18);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->Xtransform, sizeof(pMtd->Xtransform));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(19);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->UseBath, sizeof(pMtd->UseBath));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(20);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->BathLength, sizeof(pMtd->BathLength));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(21);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->IsSST, sizeof(pMtd->IsSST));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(22);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->TempLow, sizeof(pMtd->TempLow));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(23);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->TempUpper, sizeof(pMtd->TempUpper));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(24);
	VerifyDataPointer;		
	res = pData->GetData(&pMtd->TempDiff, sizeof(pMtd->TempDiff));
	VerifySuccess;

	CString sql;
	ILumexDBDataSet *pDataSet;

	// ��������� ������� ������
	{
		sql.Format(_T(MTDDB_SQL_READ_MTDMODEL), lMtd);
		
		LumexDB::EnOperationResult res = m_pDB->ExecuteSQL(sql, &pDataSet);
		if (res != LumexDB::enSuccess || pDataSet == NULL)
		{
			assert(false);
			return false;
		}

		pMtd->MtdModels.clear();

		while(!pDataSet->IsEOF())
		{
			PRMtdModel newMtdMod;

			int lMtdMod;
			pData = pDataSet->GetColumn(0);
			VerifyDataPointer;	
			res = pData->GetData(&lMtdMod, sizeof(lMtdMod));
			VerifySuccess;

			pData = pDataSet->GetColumn(1);
			VerifyDataPointer;	
			lpBuf = newMtdMod.ModelName.GetBuffer(StringLen);	
			res = pData->GetData(lpBuf, BufferLen, false);
			newMtdMod.ModelName.ReleaseBuffer();
			VerifySuccess;
			returnChar(newMtdMod.ModelName);

			pData = pDataSet->GetColumn(2);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.ModelType, sizeof(newMtdMod.ModelType));
			VerifySuccess;

			pData = pDataSet->GetColumn(3);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.SpecRangeLow, sizeof(newMtdMod.SpecRangeLow));
			VerifySuccess;

			pData = pDataSet->GetColumn(4);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.SpecRangeUpper, sizeof(newMtdMod.SpecRangeUpper));
			VerifySuccess;

			pData = pDataSet->GetColumn(5);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.FreqMin, sizeof(newMtdMod.FreqMin));
			VerifySuccess;

			pData = pDataSet->GetColumn(6);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.FreqStep, sizeof(newMtdMod.FreqStep));
			VerifySuccess;

			pData = pDataSet->GetColumn(7);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.NFreq, sizeof(newMtdMod.NFreq));
			VerifySuccess;

			pData = pDataSet->GetColumn(8);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.SpType, sizeof(newMtdMod.SpType));
			VerifySuccess;

			pData = pDataSet->GetColumn(9);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.NSpec, sizeof(newMtdMod.NSpec));
			VerifySuccess;

			pData = pDataSet->GetColumn(10);
			VerifyDataPointer;	
			lpBuf = newMtdMod.Preproc.GetBuffer(StringLen);	
			res = pData->GetData(lpBuf, BufferLen, false);
			newMtdMod.Preproc.ReleaseBuffer();
			VerifySuccess;
			returnChar(newMtdMod.Preproc);

			pData = pDataSet->GetColumn(11);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.MaxMah, sizeof(newMtdMod.MaxMah));
			VerifySuccess;

			pData = pDataSet->GetColumn(12);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.MeanMah, sizeof(newMtdMod.MeanMah));
			VerifySuccess;

			pData = pDataSet->GetColumn(13);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.IsTrans, sizeof(newMtdMod.IsTrans));
			VerifySuccess;

			pData = pDataSet->GetColumn(14);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.FreqMinTrans, sizeof(newMtdMod.FreqMinTrans));
			VerifySuccess;

			pData = pDataSet->GetColumn(15);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.FreqStepTrans, sizeof(newMtdMod.FreqStepTrans));
			VerifySuccess;

			pData = pDataSet->GetColumn(16);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.NFreqTrans, sizeof(newMtdMod.NFreqTrans));
			VerifySuccess;

			pData = pDataSet->GetColumn(17);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.SpTypeTrans, sizeof(newMtdMod.SpTypeTrans));
			VerifySuccess;

			pData = pDataSet->GetColumn(18);
			VerifyDataPointer;	
			lpBuf = newMtdMod.PreprocTrans.GetBuffer(StringLen);	
			res = pData->GetData(lpBuf, BufferLen, false);
			newMtdMod.PreprocTrans.ReleaseBuffer();
			VerifySuccess;
			returnChar(newMtdMod.PreprocTrans);

			pData = pDataSet->GetColumn(19);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.T0, sizeof(newMtdMod.T0));
			VerifySuccess;

			pData = pDataSet->GetColumn(20);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.FMax, sizeof(newMtdMod.FMax));
			VerifySuccess;

			// ��������� ������ ����������� ������������ �����

			CString sql2;
			ILumexDBDataSet *pDataSet2;
			{
				sql2.Format(_T(MTDDB_SQL_READ_EXCPNT), lMtdMod);
				
				LumexDB::EnOperationResult res = m_pDB->ExecuteSQL(sql2, &pDataSet2);
				if(res != LumexDB::enSuccess || pDataSet2 == NULL)
				{
					assert(false);
					return false;
				}

				newMtdMod.ExcPoints.clear();

				while(!pDataSet2->IsEOF())
				{
					int nData;
					pData = pDataSet2->GetColumn(0);
					VerifyDataPointer;	
					res = pData->GetData(&nData, sizeof(nData));
					VerifySuccess;
					
					newMtdMod.ExcPoints.push_back(nData);
					
					res = pDataSet2->NextRow();
					VerifySuccessAndNoData;
				}
				pDataSet2->Release();
			}

			// ��������� ������� ��������
			CString sql3;
			ILumexDBDataSet *pDataSet3;
			{
				sql3.Format(_T(MTDDB_SQL_READ_MEANSPEC), lMtdMod);
				
				LumexDB::EnOperationResult res = m_pDB->ExecuteSQL(sql3, &pDataSet3);
				if(res != LumexDB::enSuccess || pDataSet3 == NULL)
				{
					assert(false);
					return false;
				}

				while(!pDataSet3->IsEOF())
				{
					int lMeanSpec;
					pData = pDataSet3->GetColumn(0);
					VerifyDataPointer;	
					res = pData->GetData(&lMeanSpec, sizeof(lMeanSpec));
					VerifySuccess;

					int Type;
					pData = pDataSet3->GetColumn(1);
					VerifyDataPointer;	
					res = pData->GetData(&Type, sizeof(Type));
					VerifySuccess;
					
					// ��������� ������ �� ������� ��������

					CString sql4;
					ILumexDBDataSet *pDataSet4;
					{
						sql4.Format(_T(MTDDB_SQL_READ_MEANSPEC_DATA), lMeanSpec);
						
						LumexDB::EnOperationResult res = m_pDB->ExecuteSQL(sql4, &pDataSet4);
						if(res != LumexDB::enSuccess || pDataSet4 == NULL)
						{
							assert(false);
							return false;
						}

						if(Type != MTDDBVectorTypeCalibrQlt && Type != MTDDBVectorTypeFactors && Type != MTDDBVectorTypeScores)
						{
							while(!pDataSet4->IsEOF())
							{
								double fData;
								pData = pDataSet4->GetColumn(0);
								VerifyDataPointer;	
								res = pData->GetData(&fData, sizeof(fData));
								VerifySuccess;

								switch(Type)
								{
								case MTDDBVectorTypeMean:  newMtdMod.MeanSpec.push_back(fData);  break;
								case MTDDBVectorTypeMeanMSC:  newMtdMod.MeanSpecMSC.push_back(fData);  break;
								case MTDDBVectorTypeMeanStd:  newMtdMod.MeanSpecStd.push_back(fData);  break;
								case MTDDBVectorTypeMeanTrans:  newMtdMod.MeanSpecTrans.push_back(fData);  break;
								case MTDDBVectorTypeMeanMSCTrans:  newMtdMod.MeanSpecMSCTrans.push_back(fData);  break;
								case MTDDBVectorTypeMeanStdTrans:  newMtdMod.MeanSpecStdTrans.push_back(fData);  break;
								case MTDDBVectorTypeMeanHQO:  newMtdMod.MeanSpecHQO.push_back(fData);  break;
								case MTDDBVectorTypeMeanMSCHQO:  newMtdMod.MeanSpecMSCHQO.push_back(fData);  break;
								case MTDDBVectorTypeMeanStdHQO:  newMtdMod.MeanSpecStdHQO.push_back(fData);  break;
								}
								
								res = pDataSet4->NextRow();
								VerifySuccessAndNoData;
							}
						}
						else if(Type == MTDDBVectorTypeFactors)
						{
							int count = 0;
							int Nf = newMtdMod.NFreq - int(newMtdMod.ExcPoints.size());
							vector<double> NewVect;
							while(!pDataSet4->IsEOF())
							{
								double fData;
								pData = pDataSet4->GetColumn(0);
								VerifyDataPointer;	
								res = pData->GetData(&fData, sizeof(fData));
								VerifySuccess;

								NewVect.push_back(fData);

								count++;
								if(count == Nf)
								{
									newMtdMod.Factors.push_back(NewVect);
									NewVect.clear();
									count = 0;
								}

								res = pDataSet4->NextRow();
								VerifySuccessAndNoData;
							}
						}
						else if(Type == MTDDBVectorTypeScores)
						{
							int count = 0;
							int Ns = newMtdMod.NSpec;
							vector<double> NewVect;
							while(!pDataSet4->IsEOF())
							{
								double fData;
								pData = pDataSet4->GetColumn(0);
								VerifyDataPointer;	
								res = pData->GetData(&fData, sizeof(fData));
								VerifySuccess;

								NewVect.push_back(fData);

								count++;
								if(count == Ns)
								{
									newMtdMod.Scores.push_back(NewVect);
									NewVect.clear();
									count = 0;
								}

								res = pDataSet4->NextRow();
								VerifySuccessAndNoData;
							}
						}

						pDataSet4->Release();
					}

					res = pDataSet3->NextRow();
					VerifySuccessAndNoData;
				}
				pDataSet3->Release();
			}

			pMtd->MtdModels.push_back(newMtdMod);
			
			res = pDataSet->NextRow();
			VerifySuccessAndNoData;
		}
		pDataSet->Release();
	}

	// ��������� ����������� ������
	{
		sql.Format(_T(MTDDB_SQL_READ_MTDPARAM), lMtd);
		
		LumexDB::EnOperationResult res = m_pDB->ExecuteSQL(sql, &pDataSet);
		if (res != LumexDB::enSuccess || pDataSet == NULL)
		{
			assert(false);
			return false;
		}

		pMtd->MtdParams.clear();

		while(!pDataSet->IsEOF())
		{
			PRMtdParam newMtdPar;

			int lMtdPar;
			pData = pDataSet->GetColumn(0);
			VerifyDataPointer;	
			res = pData->GetData(&lMtdPar, sizeof(lMtdPar));
			VerifySuccess;

			pData = pDataSet->GetColumn(1);
			VerifyDataPointer;	
			lpBuf = newMtdPar.ParamName.GetBuffer(StringLen);	
			res = pData->GetData(lpBuf, BufferLen, false);
			newMtdPar.ParamName.ReleaseBuffer();
			VerifySuccess;
			returnChar(newMtdPar.ParamName);

			pData = pDataSet->GetColumn(2);
			VerifyDataPointer;	
			lpBuf = newMtdPar.Unit.GetBuffer(StringLen);	
			res = pData->GetData(lpBuf, BufferLen, false);
			newMtdPar.Unit.ReleaseBuffer();
			VerifySuccess;
			returnChar(newMtdPar.Unit);

			pData = pDataSet->GetColumn(3);
			VerifyDataPointer;	
			lpBuf = newMtdPar.Format.GetBuffer(StringLen);	
			res = pData->GetData(lpBuf, BufferLen, false);
			newMtdPar.Format.ReleaseBuffer();
			VerifySuccess;
			returnChar(newMtdPar.Format);

			pData = pDataSet->GetColumn(4);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdPar.UseDefModel, sizeof(newMtdPar.UseDefModel));
			VerifySuccess;

			pData = pDataSet->GetColumn(5);
			VerifyDataPointer;	
			lpBuf = newMtdPar.DefModel.GetBuffer(StringLen);	
			res = pData->GetData(lpBuf, BufferLen, false);
			newMtdPar.DefModel.ReleaseBuffer();
			VerifySuccess;
			returnChar(newMtdPar.DefModel);

			pData = pDataSet->GetColumn(6);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdPar.UseRefMois, sizeof(newMtdPar.UseRefMois));
			VerifySuccess;

			pData = pDataSet->GetColumn(7);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdPar.RefMois, sizeof(newMtdPar.RefMois));
			VerifySuccess;

			pData = pDataSet->GetColumn(8);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdPar.UseStdMois, sizeof(newMtdPar.UseStdMois));
			VerifySuccess;

			pData = pDataSet->GetColumn(9);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdPar.StdMois, sizeof(newMtdPar.StdMois));
			VerifySuccess;

			// ��������� ������� ���������� ������

			CString sql2;
			ILumexDBDataSet *pDataSet2;
			{
				sql2.Format(_T(MTDDB_SQL_READ_PARMODEL), lMtdPar);
				
				LumexDB::EnOperationResult res = m_pDB->ExecuteSQL(sql2, &pDataSet2);
				if(res != LumexDB::enSuccess || pDataSet2 == NULL)
				{
					assert(false);
					return false;
				}

				while(!pDataSet2->IsEOF())
				{
					PRParModel newParMod;

					int lParMod;
					pData = pDataSet2->GetColumn(0);
					VerifyDataPointer;	
					res = pData->GetData(&lParMod, sizeof(lParMod));
					VerifySuccess;

					pData = pDataSet2->GetColumn(1);
					VerifyDataPointer;	
					lpBuf = newParMod.ModelName.GetBuffer(StringLen);	
					res = pData->GetData(lpBuf, BufferLen, false);
					newParMod.ModelName.ReleaseBuffer();
					VerifySuccess;
					returnChar(newParMod.ModelName);

					pData = pDataSet2->GetColumn(2);
					VerifyDataPointer;		
					res = pData->GetData(&newParMod.LowCalibRange, sizeof(newParMod.LowCalibRange));
					VerifySuccess;

					pData = pDataSet2->GetColumn(3);
					VerifyDataPointer;		
					res = pData->GetData(&newParMod.UpperCalibRange, sizeof(newParMod.UpperCalibRange));
					VerifySuccess;

					pData = pDataSet2->GetColumn(4);
					VerifyDataPointer;		
					res = pData->GetData(&newParMod.MaxSKO, sizeof(newParMod.MaxSKO));
					VerifySuccess;

					pData = pDataSet2->GetColumn(5);
					VerifyDataPointer;		
					res = pData->GetData(&newParMod.CorrB, sizeof(newParMod.CorrB));
					VerifySuccess;

					pData = pDataSet2->GetColumn(6);
					VerifyDataPointer;		
					res = pData->GetData(&newParMod.CorrA, sizeof(newParMod.CorrA));
					VerifySuccess;

					pData = pDataSet2->GetColumn(7);
					VerifyDataPointer;		
					res = pData->GetData(&newParMod.SEC, sizeof(newParMod.SEC));
					VerifySuccess;

					pData = pDataSet2->GetColumn(8);
					VerifyDataPointer;		
					res = pData->GetData(&newParMod.MeanComp, sizeof(newParMod.MeanComp));
					VerifySuccess;

					pData = pDataSet2->GetColumn(9);
					VerifyDataPointer;		
					res = pData->GetData(&newParMod.MeanCompTrans, sizeof(newParMod.MeanCompTrans));
					VerifySuccess;

					pData = pDataSet2->GetColumn(10);
					VerifyDataPointer;		
					res = pData->GetData(&newParMod.MeanCompStd, sizeof(newParMod.MeanCompStd));
					VerifySuccess;

					// ��������� �������� �� �������������� ������
					CString sql3;
					ILumexDBDataSet *pDataSet3;
					{
						sql3.Format(_T(MTDDB_SQL_READ_CALIBVECTOR), lParMod);
						
						LumexDB::EnOperationResult res = m_pDB->ExecuteSQL(sql3, &pDataSet3);
						if(res != LumexDB::enSuccess || pDataSet3 == NULL)
						{
							assert(false);
							return false;
						}

						VDbl Types;
						while(!pDataSet3->IsEOF())
						{
							int lCalibVect;
							pData = pDataSet3->GetColumn(0);
							VerifyDataPointer;	
							res = pData->GetData(&lCalibVect, sizeof(lCalibVect));
							VerifySuccess;

							int Type;
							pData = pDataSet3->GetColumn(1);
							VerifyDataPointer;	
							res = pData->GetData(&Type, sizeof(Type));
							VerifySuccess;
							
							// ��������� ������ �� �������� �� �������������� �������

							CString sql4;
							ILumexDBDataSet *pDataSet4;
							{
								sql4.Format(_T(MTDDB_SQL_READ_CALIBVECTOR_DATA), lCalibVect);
								
								LumexDB::EnOperationResult res = m_pDB->ExecuteSQL(sql4, &pDataSet4);
								if(res != LumexDB::enSuccess || pDataSet4 == NULL)
								{
									assert(false);
									return false;
								}

								while(!pDataSet4->IsEOF())
								{
									double fData;
									pData = pDataSet4->GetColumn(0);
									VerifyDataPointer;	
									res = pData->GetData(&fData, sizeof(fData));
									VerifySuccess;

									switch(Type)
									{
									case MTDDBMatrixTypeCalibrLin:  newParMod.CalibrLin.push_back(fData);  break;
									case MTDDBMatrixTypeCalibrNonLin:  newParMod.CalibrNonLin.push_back(fData);  break;
									}
									
									res = pDataSet4->NextRow();
									VerifySuccessAndNoData;
								}
								pDataSet4->Release();
							}
							
							res = pDataSet3->NextRow();
							VerifySuccessAndNoData;
						}
						pDataSet3->Release();
					}

					newMtdPar.ParModels.push_back(newParMod);
					
					res = pDataSet2->NextRow();
					VerifySuccessAndNoData;
				}
				pDataSet2->Release();
			}

			pMtd->MtdParams.push_back(newMtdPar);

			res = pDataSet->NextRow();
			VerifySuccessAndNoData;
		}
		pDataSet->Release();
	}

	return true;
}

bool CPRDBMethodQnt::methodAdd(const PRMethodQnt *const pMtd)
{
	m_sSQL = _T(MTDDB_SQL_GET_MTD_ID_MAX);
	long lMtd = 1 + isDataExist();
	if(lMtd <= 0)
		return false;

	m_sSQL.Format(_T(MTDDB_SQL_INSERT_MTD), lMtd, replaceChar(pMtd->MethodName),	pMtd->DateCreated.Format(m_pszFormatCTime),
		pMtd->MoisParam, pMtd->DevNum, pMtd->DevType, pMtd->SpecRangeLow, pMtd->SpecRangeUpper, pMtd->ResSub,
		pMtd->ApodSub, pMtd->ZeroFill, pMtd->UseRef, pMtd->ResRef, pMtd->ApodRef, pMtd->SmplScan, pMtd->SmplMeas,
		pMtd->StdScan, pMtd->StdMeas, pMtd->Xtransform, pMtd->UseBath, floatWithPoint(pMtd->BathLength), pMtd->IsSST,
		pMtd->TempLow, pMtd->TempUpper, pMtd->TempDiff);

	if(!executeSQL())
		return false;

	return methodAddVect(lMtd, pMtd);
}

bool CPRDBMethodQnt::methodUpdate(CString MethodName, const PRMethodQnt *const pMtd)
{
	m_sSQL.Format(_T(MTDDB_SQL_GET_MTD_ID), replaceChar(MethodName));
	long lMtd = isDataExist();
	if(lMtd <= 0)
		return false;

	m_sSQL.Format(_T(MTDDB_SQL_UPDATE_MTD), replaceChar(pMtd->MethodName), pMtd->DateCreated.Format(m_pszFormatCTime),
		pMtd->MoisParam, pMtd->DevNum, pMtd->DevType, pMtd->SpecRangeLow, pMtd->SpecRangeUpper, pMtd->ResSub,
		pMtd->ApodSub, pMtd->ZeroFill, pMtd->UseRef, pMtd->ResRef, pMtd->ApodRef, pMtd->SmplScan, pMtd->SmplMeas,
		pMtd->StdScan, pMtd->StdMeas, pMtd->Xtransform, pMtd->UseBath, floatWithPoint(pMtd->BathLength), pMtd->IsSST,
		pMtd->TempLow, pMtd->TempUpper, pMtd->TempDiff, replaceChar(MethodName));

	if(!executeSQL())
		return false;

	m_sSQL.Format(_T(MTDDB_SQL_DELETE_MTDMODEL_ALL), lMtd);
	if(!executeSQL())
		return false;

	m_sSQL.Format(_T(MTDDB_SQL_DELETE_MTDPARAM_ALL), lMtd);
	if(!executeSQL())
		return false;

	return methodAddVect(lMtd, pMtd);
}

bool CPRDBMethodQnt::methodAddVect(long lMtd, const PRMethodQnt *const pMtd)
{
	for(std::vector<PRMtdModel>::const_iterator itM=pMtd->MtdModels.begin(); itM!=pMtd->MtdModels.end(); ++itM)
	{
		m_sSQL = _T(MTDDB_SQL_GET_MTDMODEL_ID_MAX);
		long lMtdMod = 1 + isDataExist();
		if(lMtdMod <= 0)
			return false;

		m_sSQL.Format(_T(MTDDB_SQL_INSERT_MTDMODEL), lMtdMod, lMtd, replaceChar(itM->ModelName), itM->ModelType,
			itM->SpecRangeLow, itM->SpecRangeUpper, floatWithPoint(itM->FreqMin), floatWithPoint(itM->FreqStep),
			itM->NFreq, itM->SpType, itM->NSpec, replaceChar(itM->Preproc), floatWithPoint(itM->MaxMah),
			doubleWithPoint(itM->MeanMah), itM->IsTrans, floatWithPoint(itM->FreqMinTrans),
			floatWithPoint(itM->FreqStepTrans), itM->NFreqTrans, itM->SpTypeTrans, replaceChar(itM->PreprocTrans),
			floatWithPoint(itM->T0), doubleWithPoint(itM->FMax));
		if(!executeSQL())
			return false;

		// ������ ����������� ������������ �����
		if(itM->ExcPoints.size() != NULL )
		{
			for(std::vector<int>::const_iterator itEP=itM->ExcPoints.begin(); itEP!=itM->ExcPoints.end(); ++itEP)
			{
				m_sSQL.Format(_T(MTDDB_SQL_INSERT_EXCPNT), lMtdMod, (*itEP));
				if(!executeSQL())
					return false;
			}
		}

		// ������� ������� ������

		long lMeanSpec;
		if(itM->MeanSpec.size() != NULL)
		{
			m_sSQL = _T(MTDDB_SQL_GET_MEANSPEC_ID_MAX);
			lMeanSpec = 1 + isDataExist();
			if(lMeanSpec <= 0)
				return false;

			m_sSQL.Format(_T(MTDDB_SQL_INSERT_MEANSPEC), lMeanSpec, lMtdMod, MTDDBVectorTypeMean);
				if(!executeSQL())
					return false;

			for(std::vector<double>::const_iterator itMS=itM->MeanSpec.begin(); itMS!=itM->MeanSpec.end(); ++itMS)
			{
				m_sSQL.Format(_T(MTDDB_SQL_INSERT_MEANSPEC_DATA), lMeanSpec, doubleWithPoint(*itMS));
				if(!executeSQL())
					return false;
			}
		}
		if(itM->MeanSpecMSC.size() != NULL)
		{
			m_sSQL = _T(MTDDB_SQL_GET_MEANSPEC_ID_MAX);
			lMeanSpec = 1 + isDataExist();
			if(lMeanSpec <= 0)
				return false;

			m_sSQL.Format(_T(MTDDB_SQL_INSERT_MEANSPEC), lMeanSpec, lMtdMod, MTDDBVectorTypeMeanMSC);
				if(!executeSQL())
					return false;

			for(std::vector<double>::const_iterator itMS=itM->MeanSpecMSC.begin(); itMS!=itM->MeanSpecMSC.end(); ++itMS)
			{
				m_sSQL.Format(_T(MTDDB_SQL_INSERT_MEANSPEC_DATA), lMeanSpec, doubleWithPoint(*itMS));
				if(!executeSQL())
					return false;
			}
		}
		if(itM->MeanSpecStd.size() != NULL)
		{
			m_sSQL = _T(MTDDB_SQL_GET_MEANSPEC_ID_MAX);
			lMeanSpec = 1 + isDataExist();
			if(lMeanSpec <= 0)
				return false;

			m_sSQL.Format(_T(MTDDB_SQL_INSERT_MEANSPEC), lMeanSpec, lMtdMod, MTDDBVectorTypeMeanStd);
				if(!executeSQL())
					return false;

			for(std::vector<double>::const_iterator itMS=itM->MeanSpecStd.begin(); itMS!=itM->MeanSpecStd.end(); ++itMS)
			{
				m_sSQL.Format(_T(MTDDB_SQL_INSERT_MEANSPEC_DATA), lMeanSpec, doubleWithPoint(*itMS));
				if(!executeSQL())
					return false;
			}
		}
		if(itM->MeanSpecTrans.size() != NULL)
		{
			m_sSQL = _T(MTDDB_SQL_GET_MEANSPEC_ID_MAX);
			lMeanSpec = 1 + isDataExist();
			if(lMeanSpec <= 0)
				return false;

			m_sSQL.Format(_T(MTDDB_SQL_INSERT_MEANSPEC), lMeanSpec, lMtdMod, MTDDBVectorTypeMeanTrans);
				if(!executeSQL())
					return false;

			for(std::vector<double>::const_iterator itMS=itM->MeanSpecTrans.begin(); itMS!=itM->MeanSpecTrans.end(); ++itMS)
			{
				m_sSQL.Format(_T(MTDDB_SQL_INSERT_MEANSPEC_DATA), lMeanSpec, doubleWithPoint(*itMS));
				if(!executeSQL())
					return false;
			}
		}
		if(itM->MeanSpecMSCTrans.size() != NULL)
		{
			m_sSQL = _T(MTDDB_SQL_GET_MEANSPEC_ID_MAX);
			lMeanSpec = 1 + isDataExist();
			if(lMeanSpec <= 0)
				return false;

			m_sSQL.Format(_T(MTDDB_SQL_INSERT_MEANSPEC), lMeanSpec, lMtdMod, MTDDBVectorTypeMeanMSCTrans);
				if(!executeSQL())
					return false;

			for(std::vector<double>::const_iterator itMS=itM->MeanSpecMSCTrans.begin(); itMS!=itM->MeanSpecMSCTrans.end(); ++itMS)
			{
				m_sSQL.Format(_T(MTDDB_SQL_INSERT_MEANSPEC_DATA), lMeanSpec, doubleWithPoint(*itMS));
				if(!executeSQL())
					return false;
			}
		}
		if(itM->MeanSpecStdTrans.size() != NULL)
		{
			m_sSQL = _T(MTDDB_SQL_GET_MEANSPEC_ID_MAX);
			lMeanSpec = 1 + isDataExist();
			if(lMeanSpec <= 0)
				return false;

			m_sSQL.Format(_T(MTDDB_SQL_INSERT_MEANSPEC), lMeanSpec, lMtdMod, MTDDBVectorTypeMeanStdTrans);
				if(!executeSQL())
					return false;

			for(std::vector<double>::const_iterator itMS=itM->MeanSpecStdTrans.begin(); itMS!=itM->MeanSpecStdTrans.end(); ++itMS)
			{
				m_sSQL.Format(_T(MTDDB_SQL_INSERT_MEANSPEC_DATA), lMeanSpec, doubleWithPoint(*itMS));
				if(!executeSQL())
					return false;
			}
		}
		if(itM->MeanSpecHQO.size() != NULL)
		{
			m_sSQL = _T(MTDDB_SQL_GET_MEANSPEC_ID_MAX);
			lMeanSpec = 1 + isDataExist();
			if(lMeanSpec <= 0)
				return false;

			m_sSQL.Format(_T(MTDDB_SQL_INSERT_MEANSPEC), lMeanSpec, lMtdMod, MTDDBVectorTypeMeanHQO);
				if(!executeSQL())
					return false;

			for(std::vector<double>::const_iterator itMS=itM->MeanSpecHQO.begin(); itMS!=itM->MeanSpecHQO.end(); ++itMS)
			{
				m_sSQL.Format(_T(MTDDB_SQL_INSERT_MEANSPEC_DATA), lMeanSpec, doubleWithPoint(*itMS));
				if(!executeSQL())
					return false;
			}
		}
		if(itM->MeanSpecMSCHQO.size() != NULL)
		{
			m_sSQL = _T(MTDDB_SQL_GET_MEANSPEC_ID_MAX);
			lMeanSpec = 1 + isDataExist();
			if(lMeanSpec <= 0)
				return false;

			m_sSQL.Format(_T(MTDDB_SQL_INSERT_MEANSPEC), lMeanSpec, lMtdMod, MTDDBVectorTypeMeanMSCHQO);
				if(!executeSQL())
					return false;

			for(std::vector<double>::const_iterator itMS=itM->MeanSpecMSCHQO.begin(); itMS!=itM->MeanSpecMSCHQO.end(); ++itMS)
			{
				m_sSQL.Format(_T(MTDDB_SQL_INSERT_MEANSPEC_DATA), lMeanSpec, doubleWithPoint(*itMS));
				if(!executeSQL())
					return false;
			}
		}
		if(itM->MeanSpecStdHQO.size() != NULL)
		{
			m_sSQL = _T(MTDDB_SQL_GET_MEANSPEC_ID_MAX);
			lMeanSpec = 1 + isDataExist();
			if(lMeanSpec <= 0)
				return false;

			m_sSQL.Format(_T(MTDDB_SQL_INSERT_MEANSPEC), lMeanSpec, lMtdMod, MTDDBVectorTypeMeanStdHQO);
				if(!executeSQL())
					return false;

			for(std::vector<double>::const_iterator itMS=itM->MeanSpecStdHQO.begin(); itMS!=itM->MeanSpecStdHQO.end(); ++itMS)
			{
				m_sSQL.Format(_T(MTDDB_SQL_INSERT_MEANSPEC_DATA), lMeanSpec, doubleWithPoint(*itMS));
				if(!executeSQL())
					return false;
			}
		}

		if(itM->Factors.size() != NULL)
		{
			m_sSQL = _T(MTDDB_SQL_GET_MEANSPEC_ID_MAX);
			lMeanSpec = 1 + isDataExist();
			if(lMeanSpec <= 0)
				return false;

			m_sSQL.Format(_T(MTDDB_SQL_INSERT_MEANSPEC), lMeanSpec, lMtdMod, MTDDBVectorTypeFactors);
				if(!executeSQL())
					return false;

			int k, len = int(itM->Factors.size());
			for(k=0; k<len; k++)
			{
				for(std::vector<double>::const_iterator itMS=itM->Factors[k].begin(); itMS!=itM->Factors[k].end(); ++itMS)
				{
					m_sSQL.Format(_T(MTDDB_SQL_INSERT_MEANSPEC_DATA), lMeanSpec, doubleWithPoint(*itMS));
					if(!executeSQL())
						return false;
				}
			}
		}

		if(itM->Scores.size() != NULL)
		{
			m_sSQL = _T(MTDDB_SQL_GET_MEANSPEC_ID_MAX);
			lMeanSpec = 1 + isDataExist();
			if(lMeanSpec <= 0)
				return false;

			m_sSQL.Format(_T(MTDDB_SQL_INSERT_MEANSPEC), lMeanSpec, lMtdMod, MTDDBVectorTypeScores);
				if(!executeSQL())
					return false;

			int k, len = int(itM->Scores.size());
			for(k=0; k<len; k++)
			{
				for(std::vector<double>::const_iterator itMS=itM->Scores[k].begin(); itMS!=itM->Scores[k].end(); ++itMS)
				{
					m_sSQL.Format(_T(MTDDB_SQL_INSERT_MEANSPEC_DATA), lMeanSpec, doubleWithPoint(*itMS));
					if(!executeSQL())
						return false;
				}
			}
		}
	}

	for(std::vector<PRMtdParam>::const_iterator itP=pMtd->MtdParams.begin(); itP!=pMtd->MtdParams.end(); ++itP)
	{
		m_sSQL = _T(MTDDB_SQL_GET_MTDPARAM_ID_MAX);
		long lMtdPar = 1 + isDataExist();
		if(lMtdPar <= 0)
			return false;

		m_sSQL.Format(_T(MTDDB_SQL_INSERT_MTDPARAM), lMtdPar, lMtd, replaceChar(itP->ParamName),
			replaceChar(itP->Unit), replaceChar(itP->Format), itP->UseDefModel, replaceChar(itP->DefModel),
			itP->UseRefMois, floatWithPoint(itP->RefMois), itP->UseStdMois, floatWithPoint(itP->StdMois));
		if(!executeSQL())
			return false;

		for(std::vector<PRParModel>::const_iterator itPM=itP->ParModels.begin(); itPM!=itP->ParModels.end(); ++itPM) 
		{
			m_sSQL = _T(MTDDB_SQL_GET_PARMODEL_ID_MAX);
			long lParMod = 1 + isDataExist();
			if(lParMod <= 0)
				return false;

			m_sSQL.Format(_T(MTDDB_SQL_GET_MTDMODEL_ID), lMtd, replaceChar(itPM->ModelName));
			long lMod = isDataExist();

			m_sSQL.Format(_T(MTDDB_SQL_INSERT_PARMODEL), lParMod, lMtdPar, lMod, floatWithPoint(itPM->LowCalibRange),
				floatWithPoint(itPM->UpperCalibRange), floatWithPoint(itPM->MaxSKO), doubleWithPoint(itPM->CorrB),
				doubleWithPoint(itPM->CorrA), floatWithPoint(itPM->SEC), doubleWithPoint(itPM->MeanComp),
				doubleWithPoint(itPM->MeanCompTrans), doubleWithPoint(itPM->MeanCompStd));
			if(!executeSQL())
				return false;

			// ������� �������������� ������

			long lCalibVect;
			if(itPM->CalibrLin.size() != NULL)
			{
				m_sSQL = _T(MTDDB_SQL_GET_CALIBVECTOR_ID_MAX);
				lCalibVect = 1 + isDataExist();
				if(lCalibVect <= 0)
					return false;

				m_sSQL.Format(_T(MTDDB_SQL_INSERT_CALIBVECTOR), lCalibVect, lParMod, MTDDBMatrixTypeCalibrLin);
					if(!executeSQL())
						return false;

				for(std::vector<double>::const_iterator itCV=itPM->CalibrLin.begin(); itCV!=itPM->CalibrLin.end(); ++itCV)
				{
					m_sSQL.Format(_T(MTDDB_SQL_INSERT_CALIBVECTOR_DATA), lCalibVect, doubleWithPoint(*itCV));
					if(!executeSQL())
						return false;
				}
			}
			if(itPM->CalibrNonLin.size() != NULL)
			{
				m_sSQL = _T(MTDDB_SQL_GET_CALIBVECTOR_ID_MAX);
				lCalibVect = 1 + isDataExist();
				if(lCalibVect <= 0)
					return false;

				m_sSQL.Format(_T(MTDDB_SQL_INSERT_CALIBVECTOR), lCalibVect, lParMod, MTDDBMatrixTypeCalibrNonLin);
					if(!executeSQL())
						return false;

				for(std::vector<double>::const_iterator itCV=itPM->CalibrNonLin.begin(); itCV!=itPM->CalibrNonLin.end(); ++itCV)
				{
					m_sSQL.Format(_T(MTDDB_SQL_INSERT_CALIBVECTOR_DATA), lCalibVect, doubleWithPoint(*itCV));
					if(!executeSQL())
						return false;
				}
			}
		}
	}

	return true;
}

void CPRDBMethodQnt::createTables( std::vector<CString> *pV )
{
	CString tmp;
	tmp.Format(_T(MTDDB_SQL_CREATE_TAB_MTD), MTDDBMethodName, MTDDBParamName);
	pV->push_back(tmp);
	tmp.Format(_T(MTDDB_SQL_CREATE_TAB_MTDPARAM), MTDDBParamName, MTDDBParamUnit, MTDDBParamFormat, MTDDBModelName);
	pV->push_back(tmp);
	tmp.Format(_T(MTDDB_SQL_CREATE_TAB_MTDMODEL), MTDDBModelName, MTDDBPreproc, MTDDBPreproc);
	pV->push_back(tmp);
	pV->push_back(_T(MTDDB_SQL_CREATE_TAB_PARMODEL));
	pV->push_back(_T(MTDDB_SQL_CREATE_TAB_EXCPNT));
	pV->push_back(_T(MTDDB_SQL_CREATE_TAB_MEANSPEC));
	pV->push_back(_T(MTDDB_SQL_CREATE_TAB_MEANSPEC_DATA));
	pV->push_back(_T(MTDDB_SQL_CREATE_TAB_CALIBVECTOR));
	pV->push_back(_T(MTDDB_SQL_CREATE_TAB_CALIBVECTOR_DATA));
}

long CPRDBMethodQnt::getCurrentVersion()
{
	return PRDB_METHOD_QNT_CURRENT_VERSION;
}

CString CPRDBMethodQnt::getCurrentDBName()
{
	return _T("METHOD_QNT_DATABASE_LUMEX_NIOKR");
}

//////////////////////////////////////////////////////////
// ����� ������������� �������

CPRDBMethodQlt::CPRDBMethodQlt() : CLDatabase()
{
}

CPRDBMethodQlt::~CPRDBMethodQlt()
{
}

bool CPRDBMethodQlt::MethodAddEx(CString MethodName_OLD, const PRMethodQlt *const pMtd)
{
	if(IsMethodExists(MethodName_OLD))
		return methodUpdate(MethodName_OLD, pMtd);
	else
		return methodAdd(pMtd);
}

bool CPRDBMethodQlt::MethodDelete(CString MethodName)
{
	m_sSQL.Format(_T(MTDDB_SQL_DELETE_MTD_QLT), MethodName);
	return executeSQL();
}

bool CPRDBMethodQlt::IsMethodExists(CString MethodName)
{
	m_sSQL.Format(_T(MTDDB_SQL_ISEXISTS_MTD_QLT), replaceChar(MethodName));
	return isDataExist() > 0;
}

bool CPRDBMethodQlt::MethodsRead()
{
	m_sSQL.Format(_T(MTDDB_SQL_READ_MTD_QLT));
	return selectSQL();
}

bool CPRDBMethodQlt::MethodGet(PRMethodQlt *const pMtd)
{
	if(m_pDataSet == NULL)
	{
		assert(false);
		return false;
	}

	LumexDB::CDataPtr<ILumexDBDataSet::TColumnData> pData = m_pDataSet->GetColumn(0);
	VerifyDataPointer;	
	long lMtd;
	LumexDB::EnOperationResult res = pData->GetData(&lMtd, sizeof(lMtd));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(1);
	VerifyDataPointer;	
	LPTSTR lpBuf = pMtd->MethodName.GetBuffer(StringLen);	
	res = pData->GetData(lpBuf, BufferLen, false);
	pMtd->MethodName.ReleaseBuffer();
	VerifySuccess;
	returnChar(pMtd->MethodName);

	pData = m_pDataSet->GetColumn(2);
	VerifyDataPointer;
	TIMESTAMP_STRUCT date;
	res = pData->GetData(&date, sizeof(date));
	VerifySuccess;		
	pMtd->DateCreated = CTime(date.year, date.month, date.day, date.hour, date.minute, date.second);

	pData = m_pDataSet->GetColumn(3);
	VerifyDataPointer;		
	res = pData->GetData(&pMtd->DevNum, sizeof(pMtd->DevNum));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(4);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->DevType, sizeof(pMtd->DevType));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(5);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->SpecRangeLow, sizeof(pMtd->SpecRangeLow));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(6);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->SpecRangeUpper, sizeof(pMtd->SpecRangeUpper));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(7);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->ResSub, sizeof(pMtd->ResSub));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(8);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->ApodSub, sizeof(pMtd->ApodSub));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(9);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->ZeroFill, sizeof(pMtd->ZeroFill));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(10);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->UseRef, sizeof(pMtd->UseRef));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(11);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->ResRef, sizeof(pMtd->ResRef));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(12);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->ApodRef, sizeof(pMtd->ApodRef));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(13);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->SmplScan, sizeof(pMtd->SmplScan));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(14);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->SmplMeas, sizeof(pMtd->SmplMeas));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(15);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->StdScan, sizeof(pMtd->StdScan));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(16);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->StdMeas, sizeof(pMtd->StdMeas));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(17);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->Xtransform, sizeof(pMtd->Xtransform));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(18);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->UseBath, sizeof(pMtd->UseBath));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(19);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->BathLength, sizeof(pMtd->BathLength));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(20);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->TempLow, sizeof(pMtd->TempLow));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(21);
	VerifyDataPointer;	
	res = pData->GetData(&pMtd->TempUpper, sizeof(pMtd->TempUpper));
	VerifySuccess;

	pData = m_pDataSet->GetColumn(22);
	VerifyDataPointer;		
	res = pData->GetData(&pMtd->TempDiff, sizeof(pMtd->TempDiff));
	VerifySuccess;

	CString sql;
	ILumexDBDataSet *pDataSet;

	// ��������� ������� ������
	{
		sql.Format(_T(MTDDB_SQL_READ_MTD_QLTMODEL), lMtd);
		
		LumexDB::EnOperationResult res = m_pDB->ExecuteSQL(sql, &pDataSet);
		if (res != LumexDB::enSuccess || pDataSet == NULL)
		{
			assert(false);
			return false;
		}

		pMtd->MtdQltModels.clear();

		while(!pDataSet->IsEOF())
		{
			PRMtdQltModel newMtdMod;

			int lMtdMod;
			pData = pDataSet->GetColumn(0);
			VerifyDataPointer;	
			res = pData->GetData(&lMtdMod, sizeof(lMtdMod));
			VerifySuccess;

			pData = pDataSet->GetColumn(1);
			VerifyDataPointer;	
			lpBuf = newMtdMod.ModelName.GetBuffer(StringLen);	
			res = pData->GetData(lpBuf, BufferLen, false);
			newMtdMod.ModelName.ReleaseBuffer();
			VerifySuccess;
			returnChar(newMtdMod.ModelName);

			pData = pDataSet->GetColumn(2);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.ModelInd, sizeof(newMtdMod.ModelInd));
			VerifySuccess;

			pData = pDataSet->GetColumn(3);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.SpecRangeLow, sizeof(newMtdMod.SpecRangeLow));
			VerifySuccess;

			pData = pDataSet->GetColumn(4);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.SpecRangeUpper, sizeof(newMtdMod.SpecRangeUpper));
			VerifySuccess;

			pData = pDataSet->GetColumn(5);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.FreqMin, sizeof(newMtdMod.FreqMin));
			VerifySuccess;

			pData = pDataSet->GetColumn(6);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.FreqStep, sizeof(newMtdMod.FreqStep));
			VerifySuccess;

			pData = pDataSet->GetColumn(7);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.NFreq, sizeof(newMtdMod.NFreq));
			VerifySuccess;

			pData = pDataSet->GetColumn(8);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.SpType, sizeof(newMtdMod.SpType));
			VerifySuccess;

			pData = pDataSet->GetColumn(9);
			VerifyDataPointer;	
			lpBuf = newMtdMod.Preproc.GetBuffer(StringLen);	
			res = pData->GetData(lpBuf, BufferLen, false);
			newMtdMod.Preproc.ReleaseBuffer();
			VerifySuccess;
			returnChar(newMtdMod.Preproc);

			pData = pDataSet->GetColumn(10);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.MaxMah, sizeof(newMtdMod.MaxMah));
			VerifySuccess;

			pData = pDataSet->GetColumn(11);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.MaxSKO, sizeof(newMtdMod.MaxSKO));
			VerifySuccess;

			pData = pDataSet->GetColumn(12);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.MeanMah, sizeof(newMtdMod.MeanMah));
			VerifySuccess;

			pData = pDataSet->GetColumn(13);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.IsTrans, sizeof(newMtdMod.IsTrans));
			VerifySuccess;

			pData = pDataSet->GetColumn(14);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.FreqMinTrans, sizeof(newMtdMod.FreqMinTrans));
			VerifySuccess;

			pData = pDataSet->GetColumn(15);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.FreqStepTrans, sizeof(newMtdMod.FreqStepTrans));
			VerifySuccess;

			pData = pDataSet->GetColumn(16);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.NFreqTrans, sizeof(newMtdMod.NFreqTrans));
			VerifySuccess;

			pData = pDataSet->GetColumn(17);
			VerifyDataPointer;		
			res = pData->GetData(&newMtdMod.SpTypeTrans, sizeof(newMtdMod.SpTypeTrans));
			VerifySuccess;

			pData = pDataSet->GetColumn(18);
			VerifyDataPointer;	
			lpBuf = newMtdMod.PreprocTrans.GetBuffer(StringLen);	
			res = pData->GetData(lpBuf, BufferLen, false);
			newMtdMod.PreprocTrans.ReleaseBuffer();
			VerifySuccess;
			returnChar(newMtdMod.PreprocTrans);

			// ��������� ������ ����������� ������������ �����

			CString sql2;
			ILumexDBDataSet *pDataSet2;
			{
				sql2.Format(_T(MTDDB_SQL_READ_QLT_EXCPNT), lMtdMod);
				
				LumexDB::EnOperationResult res = m_pDB->ExecuteSQL(sql2, &pDataSet2);
				if(res != LumexDB::enSuccess || pDataSet2 == NULL)
				{
					assert(false);
					return false;
				}

				newMtdMod.ExcPoints.clear();

				while(!pDataSet2->IsEOF())
				{
					int nData;
					pData = pDataSet2->GetColumn(0);
					VerifyDataPointer;	
					res = pData->GetData(&nData, sizeof(nData));
					VerifySuccess;
					
					newMtdMod.ExcPoints.push_back(nData);
					
					res = pDataSet2->NextRow();
					VerifySuccessAndNoData;
				}
				pDataSet2->Release();
			}

			// ��������� ������� �������� � �������������� �������
			CString sql3;
			ILumexDBDataSet *pDataSet3;
			{
				sql3.Format(_T(MTDDB_SQL_READ_QLT_MEANSPEC), lMtdMod);
				
				LumexDB::EnOperationResult res = m_pDB->ExecuteSQL(sql3, &pDataSet3);
				if(res != LumexDB::enSuccess || pDataSet3 == NULL)
				{
					assert(false);
					return false;
				}

				while(!pDataSet3->IsEOF())
				{
					int lMeanSpec;
					pData = pDataSet3->GetColumn(0);
					VerifyDataPointer;	
					res = pData->GetData(&lMeanSpec, sizeof(lMeanSpec));
					VerifySuccess;

					int Type;
					pData = pDataSet3->GetColumn(1);
					VerifyDataPointer;	
					res = pData->GetData(&Type, sizeof(Type));
					VerifySuccess;
					
					// ��������� ������ �� ������� �������� � �������������� �������

					CString sql4;
					ILumexDBDataSet *pDataSet4;
					{
						sql4.Format(_T(MTDDB_SQL_READ_QLT_MEANSPEC_DATA), lMeanSpec);
						
						LumexDB::EnOperationResult res = m_pDB->ExecuteSQL(sql4, &pDataSet4);
						if(res != LumexDB::enSuccess || pDataSet4 == NULL)
						{
							assert(false);
							return false;
						}

						if(Type != MTDDBVectorTypeCalibrQlt && Type != MTDDBVectorTypeFactors)
						{
							while(!pDataSet4->IsEOF())
							{
								double fData;
								pData = pDataSet4->GetColumn(0);
								VerifyDataPointer;	
								res = pData->GetData(&fData, sizeof(fData));
								VerifySuccess;

								switch(Type)
								{
								case MTDDBVectorTypeMean:  newMtdMod.MeanSpec.push_back(fData);  break;
								case MTDDBVectorTypeMeanMSC:  newMtdMod.MeanSpecMSC.push_back(fData);  break;
								case MTDDBVectorTypeMeanStd:  newMtdMod.MeanSpecStd.push_back(fData);  break;
								case MTDDBVectorTypeMeanTrans:  newMtdMod.MeanSpecTrans.push_back(fData);  break;
								case MTDDBVectorTypeMeanMSCTrans:  newMtdMod.MeanSpecMSCTrans.push_back(fData);  break;
								case MTDDBVectorTypeMeanStdTrans:  newMtdMod.MeanSpecStdTrans.push_back(fData);  break;
								}
								
								res = pDataSet4->NextRow();
								VerifySuccessAndNoData;
							}
						}
						else if(Type != MTDDBVectorTypeFactors)
						{
							int count = 0;
							int Nf = newMtdMod.NFreq - int(newMtdMod.ExcPoints.size());
							vector<double> NewVect;
							while(!pDataSet4->IsEOF())
							{
								double fData;
								pData = pDataSet4->GetColumn(0);
								VerifyDataPointer;	
								res = pData->GetData(&fData, sizeof(fData));
								VerifySuccess;

								NewVect.push_back(fData);

								count++;
								if(count == Nf)
								{
									newMtdMod.Factors.push_back(NewVect);
									NewVect.clear();
									count = 0;
								}

								res = pDataSet4->NextRow();
								VerifySuccessAndNoData;
							}
						}

						pDataSet4->Release();
					}

					res = pDataSet3->NextRow();
					VerifySuccessAndNoData;
				}
				pDataSet3->Release();
			}

			pMtd->MtdQltModels.push_back(newMtdMod);
			
			res = pDataSet->NextRow();
			VerifySuccessAndNoData;
		}
		pDataSet->Release();
	}

	// ��������� ������ ������������� �������

	{
		sql.Format(_T(MTDDB_SQL_READ_QLT_FORM), lMtd);

		ILumexDBDataSet *pDataSet;
		LumexDB::EnOperationResult res = m_pDB->ExecuteSQL(sql, &pDataSet);
		if (res != LumexDB::enSuccess || pDataSet == NULL)
		{
			assert(false);
			return false;
		}

		pMtd->Formula.clear();
		while (!pDataSet->IsEOF())
		{
			PRMtdQltElement NewElem;

			pData = pDataSet->GetColumn(0);
			VerifyDataPointer;	
			res = pData->GetData(&NewElem.Type, sizeof(NewElem.Type));
			VerifySuccess;			

			pData = pDataSet->GetColumn(1);
			VerifyDataPointer;	
			res = pData->GetData(&NewElem.ModelInd, sizeof(NewElem.ModelInd));
			VerifySuccess;			

			pMtd->Formula.push_back(NewElem);
			
			res = pDataSet->NextRow();
			VerifySuccessAndNoData;
		}
		pDataSet->Release();
	}

	return true;
}

bool CPRDBMethodQlt::methodAdd(const PRMethodQlt *const pMtd)
{
	m_sSQL = _T(MTDDB_SQL_GET_MTD_QLT_ID_MAX);
	long lMtd = 1 + isDataExist();
	if(lMtd <= 0)
		return false;

	m_sSQL.Format(_T(MTDDB_SQL_INSERT_MTD_QLT), lMtd, replaceChar(pMtd->MethodName), pMtd->DateCreated.Format(m_pszFormatCTime),
		pMtd->DevNum, pMtd->DevType, pMtd->SpecRangeLow, pMtd->SpecRangeUpper, pMtd->ResSub,
		pMtd->ApodSub, pMtd->ZeroFill, pMtd->UseRef, pMtd->ResRef, pMtd->ApodRef, pMtd->SmplScan, pMtd->SmplMeas,
		pMtd->StdScan, pMtd->StdMeas, pMtd->Xtransform, pMtd->UseBath, floatWithPoint(pMtd->BathLength),
		pMtd->TempLow, pMtd->TempUpper, pMtd->TempDiff);

	if(!executeSQL())
		return false;

	return methodAddVect(lMtd, pMtd);
}

bool CPRDBMethodQlt::methodUpdate(CString MethodName, const PRMethodQlt *const pMtd)
{
	m_sSQL.Format(_T(MTDDB_SQL_GET_MTD_QLT_ID), replaceChar(MethodName));
	long lMtd = isDataExist();
	if(lMtd <= 0)
		return false;

	m_sSQL.Format(_T(MTDDB_SQL_UPDATE_MTD_QLT), replaceChar(pMtd->MethodName), pMtd->DateCreated.Format(m_pszFormatCTime),
		pMtd->DevNum, pMtd->DevType, pMtd->SpecRangeLow, pMtd->SpecRangeUpper, pMtd->ResSub,
		pMtd->ApodSub, pMtd->ZeroFill, pMtd->UseRef, pMtd->ResRef, pMtd->ApodRef, pMtd->SmplScan, pMtd->SmplMeas,
		pMtd->StdScan, pMtd->StdMeas, pMtd->Xtransform, pMtd->UseBath, floatWithPoint(pMtd->BathLength),
		pMtd->TempLow, pMtd->TempUpper, pMtd->TempDiff, replaceChar(MethodName));

	if(!executeSQL())
		return false;

	m_sSQL.Format(_T(MTDDB_SQL_DELETE_MTD_QLTMODEL_ALL), lMtd);
	if(!executeSQL())
		return false;

	m_sSQL.Format(_T(MTDDB_SQL_DELETE_QLT_FORM_ALL), lMtd);
	if(!executeSQL())
		return false;

	return methodAddVect(lMtd, pMtd);
}

bool CPRDBMethodQlt::methodAddVect(long lMtd, const PRMethodQlt *const pMtd)
{
	for(std::vector<PRMtdQltModel>::const_iterator itM=pMtd->MtdQltModels.begin(); itM!=pMtd->MtdQltModels.end(); ++itM)
	{
		m_sSQL = _T(MTDDB_SQL_GET_MTD_QLTMODEL_ID_MAX);
		long lMtdMod = 1 + isDataExist();
		if(lMtdMod <= 0)
			return false;

		m_sSQL.Format(_T(MTDDB_SQL_INSERT_MTD_QLTMODEL), lMtdMod, lMtd, replaceChar(itM->ModelName), itM->ModelInd,
			itM->SpecRangeLow, itM->SpecRangeUpper, floatWithPoint(itM->FreqMin), floatWithPoint(itM->FreqStep),
			itM->NFreq, itM->SpType, replaceChar(itM->Preproc), floatWithPoint(itM->MaxMah),
			floatWithPoint(itM->MaxSKO), doubleWithPoint(itM->MeanMah), itM->IsTrans,
			floatWithPoint(itM->FreqMinTrans), floatWithPoint(itM->FreqStepTrans), itM->NFreqTrans, itM->SpTypeTrans,
			replaceChar(itM->PreprocTrans));
		if(!executeSQL())
			return false;

		// ������ ����������� ������������ �����
		if(itM->ExcPoints.size() != NULL )
		{
			for(std::vector<int>::const_iterator itEP=itM->ExcPoints.begin(); itEP!=itM->ExcPoints.end(); ++itEP)
			{
				m_sSQL.Format(_T(MTDDB_SQL_INSERT_QLT_EXCPNT), lMtdMod, (*itEP));
				if(!executeSQL())
					return false;
			}
		}

		// ������� ������� ������

		long lMeanSpec;
		if(itM->MeanSpec.size() != NULL)
		{
			m_sSQL = _T(MTDDB_SQL_GET_QLT_MEANSPEC_ID_MAX);
			lMeanSpec = 1 + isDataExist();
			if(lMeanSpec <= 0)
				return false;

			m_sSQL.Format(_T(MTDDB_SQL_INSERT_QLT_MEANSPEC), lMeanSpec, lMtdMod, MTDDBVectorTypeMean);
				if(!executeSQL())
					return false;

			for(std::vector<double>::const_iterator itMS=itM->MeanSpec.begin(); itMS!=itM->MeanSpec.end(); ++itMS)
			{
				m_sSQL.Format(_T(MTDDB_SQL_INSERT_QLT_MEANSPEC_DATA), lMeanSpec, doubleWithPoint(*itMS));
				if(!executeSQL())
					return false;
			}
		}
		if(itM->MeanSpecMSC.size() != NULL)
		{
			m_sSQL = _T(MTDDB_SQL_GET_QLT_MEANSPEC_ID_MAX);
			lMeanSpec = 1 + isDataExist();
			if(lMeanSpec <= 0)
				return false;

			m_sSQL.Format(_T(MTDDB_SQL_INSERT_QLT_MEANSPEC), lMeanSpec, lMtdMod, MTDDBVectorTypeMeanMSC);
				if(!executeSQL())
					return false;

			for(std::vector<double>::const_iterator itMS=itM->MeanSpecMSC.begin(); itMS!=itM->MeanSpecMSC.end(); ++itMS)
			{
				m_sSQL.Format(_T(MTDDB_SQL_INSERT_QLT_MEANSPEC_DATA), lMeanSpec, doubleWithPoint(*itMS));
				if(!executeSQL())
					return false;
			}
		}
		if(itM->MeanSpecStd.size() != NULL)
		{
			m_sSQL = _T(MTDDB_SQL_GET_QLT_MEANSPEC_ID_MAX);
			lMeanSpec = 1 + isDataExist();
			if(lMeanSpec <= 0)
				return false;

			m_sSQL.Format(_T(MTDDB_SQL_INSERT_QLT_MEANSPEC), lMeanSpec, lMtdMod, MTDDBVectorTypeMeanStd);
				if(!executeSQL())
					return false;

			for(std::vector<double>::const_iterator itMS=itM->MeanSpecStd.begin(); itMS!=itM->MeanSpecStd.end(); ++itMS)
			{
				m_sSQL.Format(_T(MTDDB_SQL_INSERT_QLT_MEANSPEC_DATA), lMeanSpec, doubleWithPoint(*itMS));
				if(!executeSQL())
					return false;
			}
		}
		if(itM->MeanSpecTrans.size() != NULL)
		{
			m_sSQL = _T(MTDDB_SQL_GET_QLT_MEANSPEC_ID_MAX);
			lMeanSpec = 1 + isDataExist();
			if(lMeanSpec <= 0)
				return false;

			m_sSQL.Format(_T(MTDDB_SQL_INSERT_QLT_MEANSPEC), lMeanSpec, lMtdMod, MTDDBVectorTypeMeanTrans);
				if(!executeSQL())
					return false;

			for(std::vector<double>::const_iterator itMS=itM->MeanSpecTrans.begin(); itMS!=itM->MeanSpecTrans.end(); ++itMS)
			{
				m_sSQL.Format(_T(MTDDB_SQL_INSERT_QLT_MEANSPEC_DATA), lMeanSpec, doubleWithPoint(*itMS));
				if(!executeSQL())
					return false;
			}
		}
		if(itM->MeanSpecMSCTrans.size() != NULL)
		{
			m_sSQL = _T(MTDDB_SQL_GET_QLT_MEANSPEC_ID_MAX);
			lMeanSpec = 1 + isDataExist();
			if(lMeanSpec <= 0)
				return false;

			m_sSQL.Format(_T(MTDDB_SQL_INSERT_QLT_MEANSPEC), lMeanSpec, lMtdMod, MTDDBVectorTypeMeanMSCTrans);
				if(!executeSQL())
					return false;

			for(std::vector<double>::const_iterator itMS=itM->MeanSpecMSCTrans.begin(); itMS!=itM->MeanSpecMSCTrans.end(); ++itMS)
			{
				m_sSQL.Format(_T(MTDDB_SQL_INSERT_QLT_MEANSPEC_DATA), lMeanSpec, doubleWithPoint(*itMS));
				if(!executeSQL())
					return false;
			}
		}
		if(itM->MeanSpecStdTrans.size() != NULL)
		{
			m_sSQL = _T(MTDDB_SQL_GET_QLT_MEANSPEC_ID_MAX);
			lMeanSpec = 1 + isDataExist();
			if(lMeanSpec <= 0)
				return false;

			m_sSQL.Format(_T(MTDDB_SQL_INSERT_QLT_MEANSPEC), lMeanSpec, lMtdMod, MTDDBVectorTypeMeanStdTrans);
				if(!executeSQL())
					return false;

			for(std::vector<double>::const_iterator itMS=itM->MeanSpecStdTrans.begin(); itMS!=itM->MeanSpecStdTrans.end(); ++itMS)
			{
				m_sSQL.Format(_T(MTDDB_SQL_INSERT_QLT_MEANSPEC_DATA), lMeanSpec, doubleWithPoint(*itMS));
				if(!executeSQL())
					return false;
			}
		}
		if(itM->Factors.size() != NULL)
		{
			m_sSQL = _T(MTDDB_SQL_GET_QLT_MEANSPEC_ID_MAX);
			lMeanSpec = 1 + isDataExist();
			if(lMeanSpec <= 0)
				return false;

			m_sSQL.Format(_T(MTDDB_SQL_INSERT_QLT_MEANSPEC), lMeanSpec, lMtdMod, MTDDBVectorTypeCalibrQlt);
				if(!executeSQL())
					return false;

			int k, len = int(itM->Factors.size());
			for(k=0; k<len; k++)
			{
				for(std::vector<double>::const_iterator itMS=itM->Factors[k].begin(); itMS!=itM->Factors[k].end(); ++itMS)
				{
					m_sSQL.Format(_T(MTDDB_SQL_INSERT_QLT_MEANSPEC_DATA), lMeanSpec, doubleWithPoint(*itMS));
					if(!executeSQL())
						return false;
				}
			}
		}
	}

	if(pMtd->Formula.size() != NULL)
	{
		for(std::vector<PRMtdQltElement>::const_iterator it=pMtd->Formula.begin(); it!=pMtd->Formula.end(); it++)
		{
			m_sSQL.Format(_T(MTDDB_SQL_INSERT_QLT_FORM), lMtd, it->Type, it->ModelInd);
			if(!executeSQL())
				return false;
		}
	}

	return true;
}

void CPRDBMethodQlt::createTables( std::vector<CString> *pV )
{
	CString tmp;
	tmp.Format(_T(MTDDB_SQL_CREATE_TAB_MTD_QLT), MTDDBMethodName);
	pV->push_back(tmp);
	tmp.Format(_T(MTDDB_SQL_CREATE_TAB_MTD_QLTMODEL), MTDDBModelName, MTDDBPreproc, MTDDBPreproc);
	pV->push_back(tmp);
	pV->push_back(_T(MTDDB_SQL_CREATE_TAB_QLT_EXCPNT));
	pV->push_back(_T(MTDDB_SQL_CREATE_TAB_QLT_MEANSPEC));
	pV->push_back(_T(MTDDB_SQL_CREATE_TAB_QLT_MEANSPEC_DATA));
	pV->push_back(_T(MTDDB_SQL_CREATE_TAB_QLT_FORM));
}

long CPRDBMethodQlt::getCurrentVersion()
{
	return PRDB_METHOD_QLT_CURRENT_VERSION;
}

CString CPRDBMethodQlt::getCurrentDBName()
{
	return _T("METHOD_QLT_DATABASE_LUMEX_NIOKR");
}
