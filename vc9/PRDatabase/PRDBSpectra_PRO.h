#ifndef _PARCEL_PRDB_SPECTRA_PRO_H_
#define _PARCEL_PRDB_SPECTRA_PRO_H_

#pragma once

#include "LDatabase/LDatabase.h"
#include "PRDBSpectraSQL_PRO.h"
#include "PRDBSpectraDefType.h"

using namespace apjdb;

// �����, �������������� ������ � ����� �������������� �������
class CPRDBSpectra_PRO : public CLDatabase
{
public:

	CPRDBSpectra_PRO( void );
	virtual ~CPRDBSpectra_PRO( void );

	// ������ ��������� ������� ������� (����� ������������ ������)
	bool ApjProjectsRead();
	bool ApjProjectGet(PRApjProject *const);

	// ���������� ������ ���� ���������� ������������� �������
	bool ApjSampleAddEx(CString ApjProjectName, CString SampleName_OLD, const PRApjSample *const);
	// �������� �������
	bool ApjSampleDelete(CString ApjProjectName, CString SampleName);
	// ��������� �������� ����������
	bool SetRefData(CString ApjProjectName, CString SampleName, CString ParamName, double Value, bool IsValue);
	// ���������� ������ �������
	bool ApjSpectrumAdd(CString ApjProjectName, CString SampleName, const PRApjSpectrum *const);
	// �������� �������
	bool ApjSpectrumDelete(CString ApjProjectName, CString SampleName, int SpecNum);
	// ��������� ������������� ������ ������� � �������
	int GetSpectrumNumMax(CString ApjProjectName, CString SampleName);
	// �������� ������������ ������ ��� �������
	bool ApjSpectrumDataGet(CString ApjProjectName, CString SampleName, PRApjSpectrum *const pSpec);

protected:

	// �������� ������� ������� � ����
	bool IsApjSampleExists(CString SampleName, long lApj);
	// �������� ������� ������� � ����
	bool IsApjSpectrumExists(int SpecNum, long lSam);

	// ������� � ��������� � ���������
	bool sampleAdd(long lApj, const PRApjSample *const pSam);
	bool sampleUpdate(CString SampleName_OLD, long lApj, const PRApjSample *const pSam);
	bool sampleAddVect(long lApj, long lSam, const PRApjSample *const pSam);
	bool spectrumAdd(long lSam, const PRApjSpectrum *const pSpec);

	// �������� ������ ���� ������, � PRO �� ������������
	virtual void createTables(std::vector<CString>*)
	{};
	// �������� ������ ���� ������
	virtual long getCurrentVersion();
	// �������� ����� ���� ������
	virtual CString getCurrentDBName();	

	// ��������� ������ �������� �������� � �������
	bool GetSampleSpectraID(CString ApjProjectName, CString SampleName, std::vector<int>& SpecIDs);
	// �������� ������ ������������ ������
	bool DeleteSpectraFiles(const std::vector<int> *const SpecIDs);
	// �������� ������� ���������� � ������� ������������ ������
	bool CheckDirectory();
	// ���������� ���� � ���������� � ������� ������������ ������
	CString GetPathToSpec(int iSp);
};

#endif
