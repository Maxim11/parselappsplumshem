#ifndef _LOAD_TRANSFER_PB_DLG_H_
#define _LOAD_TRANSFER_PB_DLG_H_

#pragma once

#include "ProgressBarDlg.h"
#include "TransferCalculate.h"


///////////////////////////////////////////////////////////////////////////////////////////////////
// ������ ��������� �������� �������� ������������ ������ ��� ������

class CTransferPBDlg : public CProgressBarDlg
{
	DECLARE_DYNAMIC(CTransferPBDlg)

public:

	CTransferPBDlg(CTransferCalculate* trans, CWnd* pParent = NULL);		// �����������
	virtual ~CTransferPBDlg();															// ����������

	enum { IDD = CProgressBarDlg::IDD };	// ������������� �������

protected:

	CTransferCalculate* TransCalc;	// ��������� �� �������������� ������, ��� ������� �������� ������������ ������
	int NAll;						// ����� ����� �������������
	int CurPrep;					// ���������� ����� ������� �������������
	int ipb;						// ����� ����������� ��������
	int passtime;					// �����, ��������� � ������ ������

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();						// ������������� �������

	virtual void OK();					// ��������� �������� ����� �� �������
	virtual void Stop();				// ��������� ����������� ����� �� �������
	virtual void Work();				// ������ �������� �������
	virtual void ResumeWork();			// ����������� ������� ����� �����

	void CalculateCorrection();		// �������� ������������ ������

	DECLARE_MESSAGE_MAP()
};

#endif
