#include "stdafx.h"

#include "ModelCalculate.h"
#include "BranchInfo.h"
#include <math.h>

void log_me_mc(const char* msg, bool bAppend=true)
{
	static char strLogName[256];

	SYSTEMTIME t;
	GetLocalTime(&t);

	if(!bAppend)
	{
		sprintf(strLogName, "PARSEL_log_mc.txt");
	}

	FILE *f = fopen(strLogName, bAppend ? "at" : "wt");
	if(!f) return;
	fprintf(f, "%04d-%02d-%02d [%02d:%02d:%02d.%03d]  ", t.wYear, t.wMonth, t.wDay, t.wHour, t.wMinute, t.wSecond, t.wMilliseconds);
	fprintf(f, "\t%s\n", msg);
	fclose(f);
}

const double T0[200] = { 12.7062,  4.3027,  3.1824,  2.7764,  2.5706,  2.4469,  2.3646,  2.3060,  2.2622,  2.2281,
					      2.2010,  2.1788,  2.1604,  2.1448,  2.1314,  2.1199,  2.1098,  2.1009,  2.0930,  2.0860,
					      2.0796,  2.0739,  2.0687,  2.0639,  2.0595,  2.0555,  2.0518,  2.0484,  2.0452,  2.0423,
					      2.0395,  2.0369,  2.0345,  2.0322,  2.0301,  2.0281,  2.0262,  2.0244,  2.0227,  2.0211,
					      2.0195,  2.0181,  2.0167,  2.0154,  2.0141,  2.0129,  2.0117,  2.0106,  2.0096,  2.0086,
                          2.0040,  2.0040,  2.0040,  2.0040,  2.0040,  2.0003,  2.0003,  2.0003,  2.0003,  2.0003,
					      1.9971,  1.9971,  1.9971,  1.9971,  1.9971,  1.9944,  1.9944,  1.9944,  1.9944,  1.9944,
					      1.9921,  1.9921,  1.9921,  1.9921,  1.9921,  1.99006, 1.99006, 1.99006, 1.99006, 1.99006,
					      1.98827, 1.98827, 1.98827, 1.98827, 1.98827, 1.98667, 1.98667, 1.98667, 1.98667, 1.98667,
					      1.98525, 1.98525, 1.98525, 1.98525, 1.98525, 1.98397, 1.98397, 1.98397, 1.98397, 1.98397,
					      1.98282, 1.98282, 1.98282, 1.98282, 1.98282, 1.98177, 1.98177, 1.98177, 1.98177, 1.98177,
					      1.98081, 1.98081, 1.98081, 1.98081, 1.98081, 1.97993, 1.97993, 1.97993, 1.97993, 1.97993,
					      1.97912, 1.97912, 1.97912, 1.97912, 1.97912, 1.97838, 1.97838, 1.97838, 1.97838, 1.97838,
					      1.97769, 1.97769, 1.97769, 1.97769, 1.97769, 1.97705, 1.97705, 1.97705, 1.97705, 1.97705,
					      1.97646, 1.97646, 1.97646, 1.97646, 1.97646, 1.97591, 1.97591, 1.97591, 1.97591, 1.97591,
					      1.97539, 1.97539, 1.97539, 1.97539, 1.97539, 1.97490, 1.97490, 1.97490, 1.97490, 1.97490,
					      1.97445, 1.97445, 1.97445, 1.97445, 1.97445, 1.97402, 1.97402, 1.97402, 1.97402, 1.97402,
					      1.97361, 1.97361, 1.97361, 1.97361, 1.97361, 1.97323, 1.97323, 1.97323, 1.97323, 1.97323,
					      1.97253, 1.97253, 1.97253, 1.97253, 1.97253, 1.97220, 1.97220, 1.97220, 1.97220, 1.97220,
					      1.97190, 1.97190, 1.97190, 1.97190, 1.97190, 1.97190, 1.97190, 1.97190, 1.97190, 1.97190
						};

static void *pCalibQlt = NULL;
static void *pCalibQnt = NULL;
static void *pSECV = NULL;

CModelCalculate::CModelCalculate(CModelData* Mod)
{
	Model = Mod;
	IsAvr = (Model->GetPreprocInd(0) == C_PREPROC_A);
	IsHSO = (Model->GetModelType() == C_MOD_MODEL_HSO);

	Prj = GetProjectByHItem(Model->GetParentHItem());
	Trans = Prj->GetTransfer(Model->GetTransName());
	IsTrans = (Trans->GetName().Compare(cOwn)) ? true : false;
	IsTransNormal = (Trans->GetType() == C_TRANS_TYPE_NORMAL) ? true : false;
}

CModelCalculate::~CModelCalculate()
{
}

bool CModelCalculate::CalculateModel(bool isExp)
{
	vector<CSampleProxy> Samples;
	CFreqScale FreqScale;
	CSpecPreproc prePar;
	char strlog[256];

	double tll1 = GetLLTimerSec();

	if(!IsTransNormal)
		FillSampleProxyOwn(Samples);
	else
	{
		vector<CSampleProxy> SamplesOwn, SamplesTrans, SamplesOwnTmp;
		CFreqScale FreqScaleTrans;
		CSpecPreprocData_Parcel preData;

		FillSampleProxyOwn(SamplesOwn, true);

		if(SamplesOwn.size() > 0)
		{
			FillFreqScaleTrans(FreqScaleTrans);
			FillSpecPreprocData(preData);

			USES_CONVERSION;

			string strPreproc(T2A(Model->GetTransPreprocAsStr()));

			IRCalibrate_PreCreate(&SamplesOwn, &FreqScaleTrans,	strPreproc, &preData);

			ExtractFromSampleProxyOwn(SamplesOwn, SamplesOwnTmp);
		}

		FillSampleProxyTrans(SamplesTrans);

		vector<CSampleProxy>::iterator itOwn = SamplesOwnTmp.begin();
		vector<CSampleProxy>::iterator itTrans = SamplesTrans.begin();
		int i, nCalib = Model->GetNSamplesForCalib();
		for(i=0; i<nCalib; i++)
		{
			ModelSample *MSam = Model->GetSampleForCalib(i);
			if(!MSam->IsTransN())
			{
				Samples.push_back(*itOwn);
				++itOwn;
			}
			else
			{
				Samples.push_back(*itTrans);
				++itTrans;
			}
		}
	}

	FillFreqScaleModel(FreqScale);
	FillSpecPreproc(prePar);

	tll1 = GetLLTimerSec()-tll1;
	sprintf(strlog, "p0-p1: %.3f", tll1);
	log_me_mc(strlog, false);
	tll1 = GetLLTimerSec();

	if(Model->GetAnalysisType() == C_ANALYSIS_QLT)
	{
		if(pCalibQlt)
		{
			QltAnaliz_Destroy(pCalibQlt);
			pCalibQlt = NULL;
		}

		CQnt_Parcel *pQnt;

		pCalibQlt = QltAnaliz_Create(&Samples, &FreqScale, &prePar, Model->GetNCompQlt(), &pQnt);

		if(pCalibQlt == NULL)
		{
			ParcelError(ParcelLoadString(STRID_ERROR_MODEL_CALC_NOTCREATED));
			return false;
		}

		int err = QltAnaliz_GetError(pCalibQlt);
		if(err)
		{
			if(err == 21 || err == 2)
				ParcelError(ParcelLoadString(STRID_ERROR_MODEL_CALC_NCOMP));
			else
			{
				TCHAR str[256];
				_stprintf(str, _T("QLT_%d"), err);
				int ecode = CALCERR_MATH;
				CalcError(ecode, str);
			}

			QltAnaliz_Destroy(pCalibQlt);
			pCalibQlt = NULL;

			return false;
		}

		if(!isExp)
			FillQltModelResult(pQnt, &Model->ModelResult);
		else
			FillQltModelResultExp(pQnt, &Model->ModelResult);

		if(pCalibQlt)
		{
			QltAnaliz_Destroy(pCalibQlt);
			pCalibQlt = NULL;
		}

		return true;
	}

	if(Model->GetModelType() == C_MOD_MODEL_PLS || Model->GetModelType() == C_MOD_MODEL_PCR)
	{
		int ModelType = (Model->GetModelType() == C_MOD_MODEL_PLS) ? MODEL_PLS : MODEL_PCR;

		if(pCalibQnt)
		{
			IRCalibrate_Destroy(pCalibQnt);
			pCalibQnt = NULL;
		}

		CQnt_Parcel *pQnt;
		
		pCalibQnt = IRCalibrate_Create(&Samples, &FreqScale, &prePar, ModelType, Model->GetNCompQnt(), &pQnt);

		if(pCalibQnt == NULL)
		{
			ParcelError(ParcelLoadString(STRID_ERROR_MODEL_CALC_NOTCREATED));
			return false;
		}

		int err = IRCalibrate_GetError(pCalibQnt);
		if(err)
		{
			if(err == 21 || err == 2)
				ParcelError(ParcelLoadString(STRID_ERROR_MODEL_CALC_NCOMP));
			else
			{
				TCHAR str[256];
				_stprintf(str, _T("QNT_%d"), err);
				int ecode = CALCERR_MATH;
				CalcError(ecode, str);
			}

			IRCalibrate_Destroy(pCalibQnt);
			pCalibQnt = NULL;

			return false;
		}

		tll1 = GetLLTimerSec()-tll1;
		sprintf(strlog, "p1-p2: %.3f", tll1);
		log_me_mc(strlog);
		tll1 = GetLLTimerSec();

		if(!isExp && Model->IsUseSECV())
		{
			pSECV = IRSecv_Create(pCalibQnt); // ����� ����� � SpLumChem

			if(pSECV == NULL)
			{
				ParcelError(ParcelLoadString(STRID_ERROR_MODEL_CALC_SECVNOTCREATED));
				return false;
			}
			err = IRSecv_GetError(pSECV);
			if(err)
			{
				if(err == 21 || err == 2)
					ParcelError(ParcelLoadString(STRID_ERROR_MODEL_CALC_NCOMP));
				else
				{
					TCHAR str[256];
					_stprintf(str, _T("QNT_%d"), err);
					int ecode = CALCERR_MATH;
					CalcError(ecode, str);
				}

				IRCalibrate_Destroy(pCalibQnt);
				pCalibQnt = NULL;

				return false;
			}
		}

		tll1 = GetLLTimerSec()-tll1;
		sprintf(strlog, "p2-p3: %.3f", tll1);
		log_me_mc(strlog);
		tll1 = GetLLTimerSec();

		if(!isExp)
		{
			FillModelResult(pQnt, &Model->ModelResult);
			if(Model->GetNSamplesForValid() > 0)
			{
				VDbl corrA, corrB;
				ItModPar itP;
				for(itP=Model->ModelParams.begin(); itP!=Model->ModelParams.end(); ++itP)
				{
					corrA.push_back(itP->CorrA);
					corrB.push_back(itP->CorrB);
				}
				Model->ModelResult.CalcCorrections(corrA, corrB, Model->GetNFactor());
			}
		}
		else
			FillModelResultExp(pQnt, NULL, &Model->ModelResult);

		tll1 = GetLLTimerSec()-tll1;
		sprintf(strlog, "p3-p4: %.3f", tll1);
		log_me_mc(strlog);
		tll1 = GetLLTimerSec();

		if(pSECV)
		{
			IRSecv_Destroy(pSECV);
			pSECV = NULL;
		}
		if(pCalibQnt)
		{
			IRCalibrate_Destroy(pCalibQnt);
			pCalibQnt = NULL;
		}

		tll1 = GetLLTimerSec()-tll1;
		sprintf(strlog, "p4-p5: %.3f", tll1);
		log_me_mc(strlog);
		tll1 = GetLLTimerSec();

		return true;
	}

	if(Model->GetModelType() == C_MOD_MODEL_HSO)
	{
		if(pCalibQnt)
		{
			IRCalibrate_Destroy(pCalibQnt);
			pCalibQnt = NULL;
		}

		MODEL_MMP MMP;
		FillModelMMP(MMP);

		CQnt_Parcel *pQnt;

		pCalibQnt = IRCalibrate_Create(&Samples, &FreqScale, &prePar, &MMP/*, Model->GetNCompQnt()*/, &pQnt);

		if(pCalibQnt == NULL)
		{
			ParcelError(ParcelLoadString(STRID_ERROR_MODEL_CALC_NOTCREATED));
			return false;
		}

		int err = IRCalibrate_GetError(pCalibQnt);
		if(err)
		{
			if(err == 21 || err == 2)
				ParcelError(ParcelLoadString(STRID_ERROR_MODEL_CALC_NCOMP));
			else
			{
				TCHAR str[256];
				_stprintf(str, _T("QNT_%d"), err);
				int ecode = CALCERR_MATH;
				CalcError(ecode, str);
			}

			IRCalibrate_Destroy(pCalibQnt);
			pCalibQnt = NULL;

			return false;
		}

		if(!isExp && Model->IsUseSECV())
		{
			pSECV = IRSecv_Create(pCalibQnt);

			if(pSECV == NULL)
			{
				ParcelError(ParcelLoadString(STRID_ERROR_MODEL_CALC_SECVNOTCREATED));
				return false;
			}
			err = IRSecv_GetError(pSECV);
			if(err)
			{
				if(err == 21 || err == 2)
					ParcelError(ParcelLoadString(STRID_ERROR_MODEL_CALC_NCOMP));
				else
				{
					TCHAR str[256];
					_stprintf(str, _T("QNT_%d"), err);
					int ecode = CALCERR_MATH;
					CalcError(ecode, str);
				}

				IRCalibrate_Destroy(pCalibQnt);
				pCalibQnt = NULL;

				return false;
			}
		}


		if(pCalibQlt)
		{
			QltAnaliz_Destroy(pCalibQlt);
			pCalibQlt = NULL;
		}

		CQnt_Parcel *pQlt;

		pCalibQlt = QltAnaliz_Create(&Samples, &FreqScale, &prePar, Model->GetNCompQnt(), &pQlt);

		if(pCalibQlt == NULL)
		{
			ParcelError(ParcelLoadString(STRID_ERROR_MODEL_CALC_NOTCREATED));
			return false;
		}

		err = QltAnaliz_GetError(pCalibQlt);
		if(err)
		{
			if(err == 21 || err == 2)
				ParcelError(ParcelLoadString(STRID_ERROR_MODEL_CALC_NCOMP));
			else
			{
				TCHAR str[256];
				_stprintf(str, _T("QLT_%d"), err);
				int ecode = CALCERR_MATH;
				CalcError(ecode, str);
			}

			QltAnaliz_Destroy(pCalibQlt);
			pCalibQlt = NULL;

			return false;
		}

		if(!isExp)
		{
			FillHSOModelResult(pQnt, pQlt, &Model->ModelResult);
			if(Model->GetNSamplesForValid() > 0)
			{
				VDbl corrA, corrB;
				ItModPar itP;
				for(itP=Model->ModelParams.begin(); itP!=Model->ModelParams.end(); ++itP)
				{
					corrA.push_back(itP->CorrA);
					corrB.push_back(itP->CorrB);
				}
				Model->ModelResult.CalcCorrections(corrA, corrB, Model->GetNFactor());
			}
		}
		else
			FillModelResultExp(pQnt, pQlt, &Model->ModelResult);

		if(pSECV)
		{
			IRSecv_Destroy(pSECV);
			pSECV = NULL;
		}
		if(pCalibQnt)
		{
			IRCalibrate_Destroy(pCalibQnt);
			pCalibQnt = NULL;
		}
		if(pCalibQlt)
		{
			QltAnaliz_Destroy(pCalibQlt);
			pCalibQlt = NULL;
		}

		return true;
	}

	return false;
}

void CModelCalculate::FillSampleProxyOwn(vector<CSampleProxy>& Samples, bool IsPre)
{
	int i, j, l, N = Model->GetNSamplesForCalib(), M = Model->GetNModelParams();

	Samples.clear();
	for(i=0; i<N; i++)
	{
		ModelSample* ModSam = Model->GetSampleForCalib(i);
		if(ModSam->IsTransN())
			continue;
		CTransferData *pTr = Prj->GetTransfer(ModSam->TransName);
		if(pTr == NULL)
			continue;

		int minfreq = (IsPre) ? pTr->GetSpecPointInd(Trans->GetSpecPoint(0))
			: pTr->GetSpecPointInd(Model->GetSpecPoint(0));
		int maxfreq = (IsPre) ? pTr->GetSpecPointInd(Trans->GetSpecPoint(Trans->GetNSpecPoints() - 1))
			: pTr->GetSpecPointInd(Model->GetSpecPoint(Model->GetNSpecPoints() - 1));

		CSampleData* Sam = pTr->GetSample(ModSam->SampleName);
		CSampleProxy OneSample;

		for(j=0; j<M; j++)
		{
			CString ParName = Model->GetModelParam(j)->ParamName;
			ReferenceData* Ref = Sam->GetReferenceData(ParName);
			if(Ref->bNoValue)
				continue;

			OneSample.vCompConc.push_back(Ref->Value);
		}

		ItIntBool it;
		for(it=ModSam->Spectra.begin(); it!=ModSam->Spectra.end(); ++it)
		{
			if(!it->second)
				continue;

			Spectrum* Spec = Sam->GetSpectrumByNum(it->first);
			if(Spec == NULL)
				continue;

			VDbl SpD;
			for(l=minfreq; l<=maxfreq; l++)
				SpD.push_back(Spec->Data[l]);

			OneSample.mSpectra.push_back(SpD);
		}

		Samples.push_back(OneSample);
	}
}

void CModelCalculate::FillSampleProxyTrans(vector<CSampleProxy>& Samples)
{
	int i, j, l, N = Model->GetNSamplesForCalib(), M = Model->GetNModelParams();
	int minfreq = Trans->GetSpecPointInd(Model->GetSpecPoint(0));	
	int maxfreq = Trans->GetSpecPointInd(Model->GetSpecPoint(Model->GetNSpecPoints() - 1));

	Samples.clear();

	for(i=0; i<N; i++)
	{
		ModelSample* ModSam = Model->GetSampleForCalib(i);
		if(!ModSam->IsTransN())
			continue;
		CTransferData *pTr = Prj->GetTransfer(ModSam->TransName);
		if(pTr == NULL)
			continue;

		CSampleData* Sam = pTr->GetSample(ModSam->SampleName);
		CSampleProxy OneSample;

		for(j=0; j<M; j++)
		{
			CString ParName = Model->GetModelParam(j)->ParamName;
			ReferenceData* Ref = Sam->GetReferenceData(ParName);
			if(Ref->bNoValue)
				continue;

			OneSample.vCompConc.push_back(Ref->Value);
		}

		ItIntBool it;
		for(it=ModSam->Spectra.begin(); it!=ModSam->Spectra.end(); ++it)
		{
			if(!it->second)
				continue;

			Spectrum* Spec = Sam->GetSpectrumByNum(it->first);
			if(Spec == NULL)
				continue;

			VDbl SpD;
			for(l=minfreq; l<=maxfreq; l++)
				SpD.push_back(Spec->Data[l]);

			OneSample.mSpectra.push_back(SpD);
		}

		Samples.push_back(OneSample);
	}
}

void CModelCalculate::FillSampleProxySEVOwn(vector<CSampleProxy>& Samples, bool IsPre)
{
	int i, j, l, N = Model->GetNSamplesForValid(), M = Model->GetNModelParams();

	Samples.clear();
	for(i=0; i<N; i++)
	{
		ModelSample* ModSam = Model->GetSampleForValid(i);
		if(ModSam->IsTransN())
			continue;
		CTransferData *pTr = Prj->GetTransfer(ModSam->TransName);
		if(pTr == NULL)
			continue;

		int minfreq = (IsPre) ? pTr->GetSpecPointInd(Trans->GetSpecPoint(0))
			: pTr->GetSpecPointInd(Model->GetSpecPoint(0));
		int maxfreq = (IsPre) ? pTr->GetSpecPointInd(Trans->GetSpecPoint(Trans->GetNSpecPoints() - 1))
			: pTr->GetSpecPointInd(Model->GetSpecPoint(Model->GetNSpecPoints() - 1));

		CSampleData* Sam = pTr->GetSample(ModSam->SampleName);
		CSampleProxy OneSample;

		for(j=0; j<M; j++)
		{
			CString ParName = Model->GetModelParam(j)->ParamName;
			ReferenceData* Ref = Sam->GetReferenceData(ParName);
			if(Ref->bNoValue)
				continue;

			OneSample.vCompConc.push_back(Ref->Value);
		}

		ItIntBool it;
		for(it=ModSam->Spectra.begin(); it!=ModSam->Spectra.end(); ++it)
		{
			if(!it->second)
				continue;

			Spectrum* Spec = Sam->GetSpectrumByNum(it->first);
			if(Spec == NULL)
				continue;

			VDbl SpD;
			for(l=minfreq; l<=maxfreq; l++)
				SpD.push_back(Spec->Data[l]);

			OneSample.mSpectra.push_back(SpD);
		}

		Samples.push_back(OneSample);
	}
}

void CModelCalculate::FillSampleProxySEVTrans(vector<CSampleProxy>& Samples)
{
	int i, j, l, N = Model->GetNSamplesForValid(), M = Model->GetNModelParams();
	int minfreq = Trans->GetSpecPointInd(Model->GetSpecPoint(0));	
	int maxfreq = Trans->GetSpecPointInd(Model->GetSpecPoint(Model->GetNSpecPoints() - 1));

	Samples.clear();

	for(i=0; i<N; i++)
	{
		ModelSample* ModSam = Model->GetSampleForValid(i);
		if(!ModSam->IsTransN())
			continue;
		CTransferData *pTr = Prj->GetTransfer(ModSam->TransName);
		if(pTr == NULL)
			continue;

		CSampleData* Sam = pTr->GetSample(ModSam->SampleName);
		CSampleProxy OneSample;

		for(j=0; j<M; j++)
		{
			CString ParName = Model->GetModelParam(j)->ParamName;
			ReferenceData* Ref = Sam->GetReferenceData(ParName);
			if(Ref->bNoValue)
				continue;

			OneSample.vCompConc.push_back(Ref->Value);
		}

		ItIntBool it;
		for(it=ModSam->Spectra.begin(); it!=ModSam->Spectra.end(); ++it)
		{
			if(!it->second)
				continue;

			Spectrum* Spec = Sam->GetSpectrumByNum(it->first);
			if(Spec == NULL)
				continue;

			VDbl SpD;
			for(l=minfreq; l<=maxfreq; l++)
				SpD.push_back(Spec->Data[l]);

			OneSample.mSpectra.push_back(SpD);
		}

		Samples.push_back(OneSample);
	}
}

void CModelCalculate::ExtractFromSampleProxyOwn(vector<CSampleProxy>& SamplesOwn, vector<CSampleProxy>& Samples)
{
	int minfreq = Trans->GetSpecPointInd(Model->GetSpecPoint(0));	
	int maxfreq = Trans->GetSpecPointInd(Model->GetSpecPoint(Model->GetNSpecPoints() - 1));

	vector<CSampleProxy>::iterator it;
	for(it=SamplesOwn.begin(); it!=SamplesOwn.end(); ++it)
	{
		CSampleProxy OneSample;

		copy(it->vCompConc.begin(), it->vCompConc.end(), inserter(OneSample.vCompConc, OneSample.vCompConc.begin()));

		ItDblDbl it2;
		for(it2=it->mSpectra.begin(); it2!=it->mSpectra.end(); ++it2)
		{
			VDbl OneSpec;

			copy(it2->begin() + minfreq, it2->begin() + (maxfreq + 1), inserter(OneSpec, OneSpec.begin()));

			OneSample.mSpectra.push_back(OneSpec);
		}

		Samples.push_back(OneSample);
	}
}

void CModelCalculate::FillFreqScaleModel(CFreqScale& FreqScale)
{
	FreqScale.nNumberOfFreq = Model->GetNSpecPoints();
	if(FreqScale.nNumberOfFreq > 0)
	{
		FreqScale.fStartFreq = Model->GetSpecPoint(0);
		if(FreqScale.nNumberOfFreq > 1)
			FreqScale.fStepFreq = Model->GetSpecPoint(1) - Model->GetSpecPoint(0);
	}
}

void CModelCalculate::FillFreqScaleTrans(CFreqScale& FreqScale)
{
	FreqScale.nNumberOfFreq = Trans->GetNSpecPoints();
	if(FreqScale.nNumberOfFreq > 0)
	{
		FreqScale.fStartFreq = Trans->GetSpecPoint(0);
		if(FreqScale.nNumberOfFreq > 1)
			FreqScale.fStepFreq = Trans->GetSpecPoint(1) - Trans->GetSpecPoint(0);
	}
}

void CModelCalculate::FillSpecPreproc(CSpecPreproc& prePar)
{
	int i;

	// ���� ������ ���������� ������������ ����� ��������, �� ������ �����������, ��� �����
	// �� ��� ������ TRA, ���� �� ����� ���� ABS. (����������� SpLumChem.dll)
	if(Model->GetSpectrumType() == C_MOD_SPECTRUM_TRA || Trans->GetType() == C_TRANS_TYPE_NORMAL)
		prePar.nSpType = STYPE_TRA;
	else
		prePar.nSpType = STYPE_ABS;

	USES_CONVERSION;

	string s(T2A(Model->GetOwnPreprocAsStr()));
	string st(T2A(Model->GetTransPreprocAsStr()));

	prePar.strPreprocPar = s;
	prePar.strPreprocParTrans = st;

	int shift = Prj->GetSpecPointInd(Model->GetSpecPoint(0));

	prePar.vFreqExcluded.clear();
	for(i=0; i<Model->GetNExcPoints(); i++)
	{
		int pnt = Model->GetExcPoint(i) - shift;
		if(pnt < 0 || pnt >= Model->GetNSpecPoints())
			continue;

		prePar.vFreqExcluded.push_back(pnt);
	}

	for(i=0; i<Model->GetNModelParams(); i++)
	{
		string s1(T2A(GetTmpParName(i)));
		prePar.strCompName.push_back(s1);
	}
}

void CModelCalculate::FillSpecPreprocData(CSpecPreprocData_Parcel& preData)
{
	if(Trans == NULL)
		return;

	CSampleData* ArtSample = Trans->GetArtSample();
	if(ArtSample == NULL)
		return;

	ArtSample->LoadSpectraData();

	Spectrum* MeanSpec = ArtSample->GetSpectrumByNum(C_TRANS_SPECNUM_MEAN);
	if(MeanSpec != NULL)
		copy(MeanSpec->Data.begin(), MeanSpec->Data.end(), inserter(preData.meanSpectrum, preData.meanSpectrum.begin()));

	Spectrum* MeanSpecMSC = ArtSample->GetSpectrumByNum(C_TRANS_SPECNUM_MEANMSC);
	if(MeanSpecMSC != NULL)
		copy(MeanSpecMSC->Data.begin(), MeanSpecMSC->Data.end(), inserter(preData.meanSpectrumMSC, preData.meanSpectrumMSC.begin()));

	Spectrum* MeanSpecStd = ArtSample->GetSpectrumByNum(C_TRANS_SPECNUM_MEANSTD);
	if(MeanSpecStd != NULL)
		copy(MeanSpecStd->Data.begin(), MeanSpecStd->Data.end(), inserter(preData.meanStdSpec, preData.meanStdSpec.begin()));

	ArtSample->FreeSpectraData();
}

void CModelCalculate::FillModelMMP(MODEL_MMP &MMP)
{
	MMP.Type = MODEL_HSO;
	MMP.sizeGcub = Model->GetDimHiper();
	MMP.garm = Model->GetNHarm();
	MMP.knl = Model->GetKfnl();
}

void CModelCalculate::FillModelResult(CQnt_Parcel *pQnt, CModelResult *pMRes)
{
	pMRes->Clear();

	copy(Model->TransNames.begin(), Model->TransNames.end(), inserter(pMRes->TransNames, pMRes->TransNames.begin()));

	CString SampleResName;
	int i, j, k, l, m;
	int nP = Model->GetNModelParams();
	int nS = Model->GetNSamplesForCalib();
	int nV = Model->GetNSamplesForValid();
	int nSpCalib = Model->GetNSpectraForCalib();
	int nSpValid = Model->GetNSpectraForValid();
	int nComp = Model->GetNCompQnt();
	int nSpCalibAvr = (IsAvr) ? nS : nSpCalib;
	if(Model->GetPreprocNum(C_PREPROC_M) >= 0)
		nSpCalibAvr--;

	pMRes->SetNVal(nV);

	CFreqScale FreqScale;
	vector<CSampleProxy> SamplesVal;
	VDbl ResSEV(nP);
	VDbl ResSDV(nP);
	VDbl ResR2sev(nP);
	VDbl ResE(nP);
	MDbl CompConc;

	pMRes->SetAverage(IsAvr);

	if(nV > 0)
	{
		if(!IsTransNormal)
			FillSampleProxySEVOwn(SamplesVal);
		else
		{
			vector<CSampleProxy> SamplesOwn, SamplesTrans, SamplesOwnTmp;
			CFreqScale FreqScaleTrans;
			CSpecPreprocData_Parcel preData;

			FillSampleProxySEVOwn(SamplesOwn, true);

			if(SamplesOwn.size() > 0)
			{
				FillFreqScaleTrans(FreqScaleTrans);
				FillSpecPreprocData(preData);

				USES_CONVERSION;

				string strPreproc(T2A(Model->GetTransPreprocAsStr()));

				IRCalibrate_PreCreate(&SamplesOwn, &FreqScaleTrans,	strPreproc, &preData);

				ExtractFromSampleProxyOwn(SamplesOwn, SamplesOwnTmp);
			}

			FillSampleProxySEVTrans(SamplesTrans);

			vector<CSampleProxy>::iterator itOwn = SamplesOwnTmp.begin();
			vector<CSampleProxy>::iterator itTrans = SamplesTrans.begin();
			int i, nValid = Model->GetNSamplesForValid();
			for(i=0; i<nValid; i++)
			{
				ModelSample *MSam = Model->GetSampleForValid(i);
				if(!MSam->IsTransN())
				{
					SamplesVal.push_back(*itOwn);
					++itOwn;
				}
				else
				{
					SamplesVal.push_back(*itTrans);
					++itTrans;
				}
			}
		}

		FillFreqScaleModel(FreqScale);

		VDbl Tmp(nP);
		for(k=0; k<nV; k++)
		{
			ModelSample *pSamV = Model->GetSampleForValid(k);
			if(pSamV == NULL)
				continue;

			SampleResName = Model->GetSampleTransName(pSamV);

			ItIntBool it;
			for(it=pSamV->Spectra.begin(); it!=pSamV->Spectra.end(); ++it)
			{
				if(!it->second)
					continue;

				CompConc.push_back(Tmp);
			}
		}

		IRCalibrate_PredictSamples(pCalibQnt, &SamplesVal, &FreqScale, &CompConc, &ResSEV, &ResE, &ResSDV, &ResR2sev);
	}

	ItDbl itd;
	double fMeanMah, SqrSumMah = 0.;
	for(itd=pQnt->vMahalanobisDistances.begin(); itd!=pQnt->vMahalanobisDistances.end(); ++itd)
		SqrSumMah += (*itd)*(*itd);
	if(nSpCalib > 1)
		fMeanMah = sqrt(SqrSumMah / (nSpCalib - 1));
	else
		fMeanMah = 1.;

	for(i=0; i<nP; i++)
	{
		CString ParName = Model->GetModelParam(i)->ParamName;
		ParamSumResult *pPSR = pMRes->AddPSResult(ParName);
		if(pPSR == NULL)
			continue;

		USES_CONVERSION;

		string parname(T2A(GetTmpParName(i)));

		double secv = 0., r2 = 0., f = 0.;
		if(Model->IsUseSECV())
		{
			IRSecv_GetSECV(pSECV, parname, &secv);
			IRSecv_GetR2stat(pSECV, parname, &r2);
			IRSecv_GetFstat(pSECV, parname, &f);
		}
		pPSR->SEC = pQnt->vSEC[i];
		pPSR->R2sec = pQnt->vR2Stat[i];
		pPSR->SECV = secv;
		pPSR->R2secv = r2;
		pPSR->F = f;
		pPSR->SEV = ResSEV[i];
		pPSR->SDV = ResSDV[i];
		pPSR->R2sev = ResR2sev[i];
		pPSR->e = ResE[i];
		pPSR->t = (fabs(pPSR->SDV) > 1.e-9) ?
			(fabs(pPSR->e) * sqrt(double(nSpValid))) / pPSR->SDV : 0.;
		pPSR->t0 = GetT0(nSpValid - 1);
		pPSR->norm = GetT0(nSpCalibAvr - Model->GetNFactor());

		copy(pQnt->m_Model.mYloadings[i].begin(), pQnt->m_Model.mYloadings[i].end(),
			inserter(pPSR->Chem_GK, pPSR->Chem_GK.begin()));
	}

	int minfreq = Prj->GetSpecPointInd(Model->GetSpecPoint(0));
	int maxfreq = Prj->GetSpecPointInd(Model->GetSpecPoint(Model->GetNSpecPoints() - 1));
	int NExc = Model->GetNExcPoints();
	for(k=-1, l=minfreq; l<=maxfreq; l++)
	{
		for(i=0; i<NExc; i++)
		{
			int esp = Model->GetExcPoint(i);
			if(l == esp)
				break;
		}
		if(i < NExc)
			continue;

		k++;

		LoadingResult *pLR = pMRes->AddLoadingResult(l);
		if(pLR == NULL)
			continue;

		pLR->Spec_GK.clear();
		for(m=0; m<nComp; m++)
		{
			VDbl NewFact;
			copy(pQnt->m_Model.mFactors[m].begin(), pQnt->m_Model.mFactors[m].end(), inserter(NewFact, NewFact.begin()));

			pLR->Spec_GK.push_back(NewFact[k]);
		}
	}

	int counter = 0;
	for(k=0; k<nS; k++)
	{
		ModelSample *pSam = Model->GetSampleForCalib(k);
		if(pSam == NULL)
			continue;

		SampleResName = Model->GetSampleTransName(pSam);

		VDbl RefRes(nP);
		IRCalibrate_GetRefConc(pCalibQnt, k, &RefRes);

		VDbl PredSECVsec(nP, 0.);
		if(Model->IsUseSECV())
			IRSecv_GetSEC(pSECV, k, &PredSECVsec);

		ItIntBool it;
		for(j=0, it=pSam->Spectra.begin(); it!=pSam->Spectra.end(); ++it)
		{
			if(!it->second)
				continue;

			SpectrumResult *pCalibRes = pMRes->AddCalibResult(pSam->SampleName, pSam->TransName, it->first, IsAvr);
			if(pCalibRes == NULL)
				continue;

			VDbl PredRes(nP);
			VDbl PredSECV(nP, 0.);
			double MahMean = 1.;
			if(IsAvr)
			{
				IRCalibrate_GetPredConcAv(pCalibQnt, k, &PredRes);
				if(Model->IsUseSECV())
					IRSecv_GetPredConcAv(pSECV, k, &PredSECV);

				MahMean = IRCalibrate_GetSampleNormDistanceAv(pCalibQnt, k);
			}
			else
			{
				IRCalibrate_GetPredConc(pCalibQnt, k, j, &PredRes);
				if(Model->IsUseSECV())
					IRSecv_GetPredConc(pSECV, k, j, &PredSECV);

				MahMean = pQnt->vMahalanobisDistances[counter] / fMeanMah;
			}

			int ind = (IsAvr) ? k : counter;

			pCalibRes->Out_Mah = pQnt->vMahalanobisDistances[ind] / fMeanMah;

			for(m=0; m<nComp; m++)
			{
				if(IsHSO)
					pCalibRes->Scores_GK.push_back(pQnt->m_Model.mScores[ind][m]);
				else
					pCalibRes->Scores_GK.push_back(pQnt->m_Model.mScores[ind][m]);
			}

			for(i=0; i<nP; i++)
			{
				SpectrumParamResult NewSpPR;

				NewSpPR.ParamName = Model->GetModelParam(i)->ParamName;
				NewSpPR.Reference = RefRes[i];
				NewSpPR.SEC_Reference = RefRes[i];
				NewSpPR.SEC_Prediction = PredRes[i];
				NewSpPR.SEC_RefPred = RefRes[i] - PredRes[i];
				NewSpPR.SEC_Proc = (RefRes[i] > 1.0e-9) ? 100. * NewSpPR.SEC_RefPred /RefRes[i] : 0.;
				NewSpPR.SECV_Reference = RefRes[i];
				NewSpPR.SECV_Prediction = PredSECV[i];
				NewSpPR.SECV_RefPred = RefRes[i] - PredSECV[i];
				NewSpPR.SECV_Proc = (RefRes[i] > 1.0e-9) ? 100. * NewSpPR.SECV_RefPred /RefRes[i] : 0.;
				NewSpPR.SECV_SEC = PredSECVsec[i];
				NewSpPR.Bad_Reference = RefRes[i];
				NewSpPR.Bad_Prediction = PredRes[i];
				NewSpPR.Bad_RefPred = RefRes[i] - PredRes[i];
				NewSpPR.Bad_Proc = (RefRes[i] > 1.0e-9) ? 100. * NewSpPR.Bad_RefPred /RefRes[i] : 0.;
				NewSpPR.Bad_Remainder =	fabs(NewSpPR.Bad_RefPred) / (pQnt->vSEC[i] *
					sqrt(1. + MahMean * MahMean * nComp / nSpCalibAvr));

				pCalibRes->SpecParamData.push_back(NewSpPR);
			}

			PredRes.clear();

			j++;
			counter++;

			if(IsAvr)  break;
		}

		RefRes.clear();
	}

	vector<CSampleProxy>::iterator itV;
	for(j=0, k=0, itV = SamplesVal.begin(); k<nV; k++, ++itV)
	{
		ModelSample *pSamV = Model->GetSampleForValid(k);
		if(pSamV == NULL)
			continue;

		CSampleData* Sam = Prj->GetTransfer(pSamV->TransName)->GetSample(pSamV->SampleName);
		SampleResName = Model->GetSampleTransName(pSamV);

		VDbl RefRes;
		for(i=0; i<nP; i++)
		{
			CString ParName = Model->GetModelParam(i)->ParamName;
			ReferenceData* Ref = Sam->GetReferenceData(ParName);
			RefRes.push_back(Ref->Value);
		}

		ItIntBool it;
		int isp;
		for(isp=0, it=pSamV->Spectra.begin(); it!=pSamV->Spectra.end(); ++it)
		{
			if(!it->second)
				continue;

			SpectrumValResult *pValidRes = pMRes->AddValidResult(pSamV->SampleName, pSamV->TransName, it->first);
			if(pValidRes == NULL)
				continue;

			pValidRes->Out_Mah = IRCalibrate_GetExtSpecNormDistance(pCalibQnt, &FreqScale, &itV->mSpectra[isp]);

			for(i=0; i<nP; i++)
			{
				SpectrumValParamResult NewSpVPR;

				NewSpVPR.ParamName = Model->GetModelParam(i)->ParamName;
				NewSpVPR.Reference = RefRes[i];
				NewSpVPR.Prediction = CompConc[j][i];
				NewSpVPR.Corrected = Model->GetModelParam(i)->CorrB * NewSpVPR.Prediction +
					Model->GetModelParam(i)->CorrA;
				NewSpVPR.RefPred = NewSpVPR.Reference - NewSpVPR.Corrected;
				NewSpVPR.Proc = (NewSpVPR.Reference > 1.0e-9) ? 100. * NewSpVPR.RefPred /NewSpVPR.Reference : 0.;
				NewSpVPR.Delta = pQnt->vSEC[i] *
					sqrt(1. + pValidRes->Out_Mah * pValidRes->Out_Mah * nComp / nSpCalibAvr) *
					GetT0(nSpCalibAvr - Model->GetNFactor());

				pValidRes->SpecValParamData.push_back(NewSpVPR);
			}

			isp++;
			j++;
		}
	}

	CalcAllTrendLine(pMRes);
}

void CModelCalculate::FillHSOModelResult(CQnt_Parcel *pQnt, CQnt_Parcel *pQlt, CModelResult *pMRes)
{
	pMRes->Clear();

	CString SampleResName;
	int i, j, k, l, m;
	int nP = Model->GetNModelParams();
	int nS = Model->GetNSamplesForCalib();
	int nV = Model->GetNSamplesForValid();
	int nSpCalib = Model->GetNSpectraForCalib();
	int nSpValid = Model->GetNSpectraForValid();
	int nComp = Model->GetNCompQnt();
	int nSpCalibAvr = (IsAvr) ? nS : nSpCalib;
	if(Model->GetPreprocNum(C_PREPROC_M) >= 0)
		nSpCalibAvr--;

	pMRes->SetNVal(nV);

	CFreqScale FreqScale;
	vector<CSampleProxy> SamplesVal;
	VDbl ResSEV(nP);
	VDbl ResSDV(nP);
	VDbl ResR2sev(nP);
	VDbl ResE(nP);
	MDbl CompConc;

	pMRes->SetAverage(IsAvr);

	if(nV > 0)
	{
		if(!IsTransNormal)
			FillSampleProxySEVOwn(SamplesVal);
		else
		{
			vector<CSampleProxy> SamplesOwn, SamplesTrans, SamplesOwnTmp;
			CFreqScale FreqScaleTrans;
			CSpecPreprocData_Parcel preData;

			FillSampleProxySEVOwn(SamplesOwn, true);

			if(SamplesOwn.size() > 0)
			{
				FillFreqScaleTrans(FreqScaleTrans);
				FillSpecPreprocData(preData);

				USES_CONVERSION;

				string strPreproc(T2A(Model->GetTransPreprocAsStr()));

				IRCalibrate_PreCreate(&SamplesOwn, &FreqScaleTrans,	strPreproc, &preData);

				ExtractFromSampleProxyOwn(SamplesOwn, SamplesOwnTmp);
			}

			FillSampleProxySEVTrans(SamplesTrans);

			vector<CSampleProxy>::iterator itOwn = SamplesOwnTmp.begin();
			vector<CSampleProxy>::iterator itTrans = SamplesTrans.begin();
			int i, nValid = Model->GetNSamplesForValid();
			for(i=0; i<nValid; i++)
			{
				ModelSample *MSam = Model->GetSampleForValid(i);
				if(!MSam->IsTransN())
				{
					SamplesVal.push_back(*itOwn);
					++itOwn;
				}
				else
				{
					SamplesVal.push_back(*itTrans);
					++itTrans;
				}
			}
		}

		FillFreqScaleModel(FreqScale);

		VDbl Tmp(nP);
		for(k=0; k<nV; k++)
		{
			ModelSample *pSamV = Model->GetSampleForValid(k);
			if(pSamV == NULL)
				continue;

			SampleResName = Model->GetSampleTransName(pSamV);

			ItIntBool it;
			for(it=pSamV->Spectra.begin(); it!=pSamV->Spectra.end(); ++it)
			{
				if(!it->second)
					continue;

				CompConc.push_back(Tmp);
			}
		}

		IRCalibrate_PredictSamples(pCalibQnt, &SamplesVal, &FreqScale, &CompConc, &ResSEV, &ResE, &ResSDV, &ResR2sev);
	}

	ItDbl itd;
	double fMeanMah, SqrSumMah = 0.;
	for(itd=pQlt->vMahalanobisDistances.begin(); itd!=pQlt->vMahalanobisDistances.end(); ++itd)
		SqrSumMah += (*itd)*(*itd);
	if(nSpCalib > 1)
		fMeanMah = sqrt(SqrSumMah / (nSpCalib - 1));
	else
		fMeanMah = 1.;

	for(i=0; i<nP; i++)
	{
		CString ParName = Model->GetModelParam(i)->ParamName;
		ParamSumResult *pPSR = pMRes->AddPSResult(ParName);
		if(pPSR == NULL)
			continue;

		USES_CONVERSION;

		string parname(T2A(GetTmpParName(i)));

		double secv = 0., r2 = 0., f = 0.;
		if(Model->IsUseSECV())
		{
			IRSecv_GetSECV(pSECV, parname, &secv);
			IRSecv_GetR2stat(pSECV, parname, &r2);
			IRSecv_GetFstat(pSECV, parname, &f);
		}
		pPSR->SEC = pQnt->vSEC[i];
		pPSR->R2sec = pQnt->vR2Stat[i];
		pPSR->SECV = secv;
		pPSR->R2secv = r2;
		pPSR->F = f;
		pPSR->SEV = ResSEV[i];
		pPSR->SDV = ResSDV[i];
		pPSR->R2sev = ResR2sev[i];
		pPSR->e = ResE[i];
		pPSR->t = (fabs(pPSR->SDV) > 1.e-9) ?
			(fabs(pPSR->e) * sqrt(double(nSpValid))) / pPSR->SDV : 0.;
		pPSR->t0 = GetT0(nSpValid - 1);
		pPSR->norm = GetT0(nSpCalibAvr - Model->GetNFactor());

		for(m=0; m<nComp; m++)
			pPSR->Chem_GK.push_back(0.);
	}

	int minfreq = Prj->GetSpecPointInd(Model->GetSpecPoint(0));
	int maxfreq = Prj->GetSpecPointInd(Model->GetSpecPoint(Model->GetNSpecPoints() - 1));
	int NExc = Model->GetNExcPoints();
	for(k=-1, l=minfreq; l<=maxfreq; l++)
	{
		for(i=0; i<NExc; i++)
		{
			int esp = Model->GetExcPoint(i);
			if(l == esp)
				break;
		}
		if(i < NExc)
			continue;

		k++;

		LoadingResult *pLR = pMRes->AddLoadingResult(l);
		if(pLR == NULL)
			continue;

		pLR->Spec_GK.clear();
		for(m=0; m<nComp; m++)
		{
			VDbl NewFact;
			copy(pQlt->m_Model.mFactors[m].begin(), pQlt->m_Model.mFactors[m].end(), inserter(NewFact, NewFact.begin()));

			pLR->Spec_GK.push_back(NewFact[k]);
		}
	}

	int counter = 0;
	for(k=0; k<nS; k++)
	{
		ModelSample *pSam = Model->GetSampleForCalib(k);
		if(pSam == NULL)
			continue;

		SampleResName = Model->GetSampleTransName(pSam);

		VDbl RefRes(nP);
		IRCalibrate_GetRefConc(pCalibQnt, k, &RefRes);

		VDbl PredSECVsec(nP, 0.);
		if(Model->IsUseSECV())
			IRSecv_GetSEC(pSECV, k, &PredSECVsec);

		ItIntBool it;
		for(j=0, it=pSam->Spectra.begin(); it!=pSam->Spectra.end(); ++it)
		{
			if(!it->second)
				continue;

			SpectrumResult *pCalibRes = pMRes->AddCalibResult(pSam->SampleName, pSam->TransName, it->first, IsAvr);
			if(pCalibRes == NULL)
				continue;

			VDbl PredRes(nP);
			VDbl PredSECV(nP, 0.);
			double MahMean = 1.;
			if(IsAvr)
			{
				IRCalibrate_GetPredConcAv(pCalibQnt, k, &PredRes);
				if(Model->IsUseSECV())
					IRSecv_GetPredConcAv(pSECV, k, &PredSECV);

				MahMean = QltAnaliz_GetSampleNormDistanceAv(pCalibQlt, k);
			}
			else
			{
				IRCalibrate_GetPredConc(pCalibQnt, k, j, &PredRes);
				if(Model->IsUseSECV())
					IRSecv_GetPredConc(pSECV, k, j, &PredSECV);

				MahMean = pQlt->vMahalanobisDistances[counter] / fMeanMah;
			}

			int ind = (IsAvr) ? k : counter;

			pCalibRes->Out_Mah = pQlt->vMahalanobisDistances[ind] / fMeanMah;

			for(m=0; m<nComp; m++)
			{
				if(IsHSO)
					pCalibRes->Scores_GK.push_back(pQlt->m_Model.mScores[ind][m]);
				else
					pCalibRes->Scores_GK.push_back(pQlt->m_Model.mScores[ind][m]);
			}

			for(i=0; i<nP; i++)
			{
				SpectrumParamResult NewSpPR;

				NewSpPR.ParamName = Model->GetModelParam(i)->ParamName;
				NewSpPR.Reference = RefRes[i];
				NewSpPR.SEC_Reference = RefRes[i];
				NewSpPR.SEC_Prediction = PredRes[i];
				NewSpPR.SEC_RefPred = RefRes[i] - PredRes[i];
				NewSpPR.SEC_Proc = (RefRes[i] > 1.0e-9) ? 100. * NewSpPR.SEC_RefPred /RefRes[i] : 0.;
				NewSpPR.SECV_Reference = RefRes[i];
				NewSpPR.SECV_Prediction = PredSECV[i];
				NewSpPR.SECV_RefPred = RefRes[i] - PredSECV[i];
				NewSpPR.SECV_Proc = (RefRes[i] > 1.0e-9) ? 100. * NewSpPR.SECV_RefPred /RefRes[i] : 0.;
				NewSpPR.SECV_SEC = PredSECVsec[i];
				NewSpPR.Bad_Reference = RefRes[i];
				NewSpPR.Bad_Prediction = PredRes[i];
				NewSpPR.Bad_RefPred = RefRes[i] - PredRes[i];
				NewSpPR.Bad_Proc = (RefRes[i] > 1.0e-9) ? 100. * NewSpPR.Bad_RefPred /RefRes[i] : 0.;
				NewSpPR.Bad_Remainder =	fabs(NewSpPR.Bad_RefPred) / (pQnt->vSEC[i] *
					sqrt(1. + MahMean * MahMean * nComp / nSpCalibAvr));

				pCalibRes->SpecParamData.push_back(NewSpPR);
			}

			PredRes.clear();

			j++;
			counter++;

			if(IsAvr)  break;
		}

		RefRes.clear();
	}

	vector<CSampleProxy>::iterator itV;
	for(j=0, k=0, itV = SamplesVal.begin(); k<nV; k++, ++itV)
	{
		ModelSample *pSamV = Model->GetSampleForValid(k);
		if(pSamV == NULL)
			continue;

		CSampleData* Sam = Prj->GetTransfer(pSamV->TransName)->GetSample(pSamV->SampleName);
		SampleResName = Model->GetSampleTransName(pSamV);

		VDbl RefRes;
		for(i=0; i<nP; i++)
		{
			CString ParName = Model->GetModelParam(i)->ParamName;
			ReferenceData* Ref = Sam->GetReferenceData(ParName);
			RefRes.push_back(Ref->Value);
		}

		ItIntBool it;
		int isp;
		for(isp=0, it=pSamV->Spectra.begin(); it!=pSamV->Spectra.end(); ++it)
		{
			if(!it->second)
				continue;

			SpectrumValResult *pValidRes = pMRes->AddValidResult(pSamV->SampleName, pSamV->TransName, it->first);
			if(pValidRes == NULL)
				continue;

			pValidRes->Out_Mah = QltAnaliz_GetExtSpecNormDistance(pCalibQlt, &FreqScale, &itV->mSpectra[isp]);

			for(i=0; i<nP; i++)
			{
				SpectrumValParamResult NewSpVPR;

				NewSpVPR.ParamName = Model->GetModelParam(i)->ParamName;
				NewSpVPR.Reference = RefRes[i];
				NewSpVPR.Prediction = CompConc[j][i];
				NewSpVPR.Corrected = Model->GetModelParam(i)->CorrB * NewSpVPR.Prediction +
					Model->GetModelParam(i)->CorrA;
				NewSpVPR.RefPred = NewSpVPR.Reference - NewSpVPR.Prediction;
				NewSpVPR.Proc = (NewSpVPR.Reference > 1.0e-9) ? 100. * NewSpVPR.RefPred /NewSpVPR.Reference : 0.;
				NewSpVPR.Delta = pQnt->vSEC[i] *
					sqrt(1. + pValidRes->Out_Mah * pValidRes->Out_Mah * nComp / nSpCalibAvr) *
					GetT0(nSpCalibAvr - Model->GetNFactor());

				pValidRes->SpecValParamData.push_back(NewSpVPR);
			}

			isp++;
			j++;
		}
	}

	CalcAllTrendLine(pMRes);
}

void CModelCalculate::FillQltModelResult(CQnt_Parcel *pQnt, CModelResult *pMRes)
{
	pMRes->Clear();

	CString SampleResName;
	int i, j, k, l, m;
	int nS = Model->GetNSamplesForCalib();
	int nV = Model->GetNSamplesForValid();
	int nSpCalib = Model->GetNSpectraForCalib();
	int nComp = Model->GetNCompQlt();

	pMRes->SetNVal(nV);

	CFreqScale FreqScale;
	vector<CSampleProxy> SamplesVal;

	pMRes->SetAverage(IsAvr);

	if(nV > 0)
	{
		if(!IsTransNormal)
			FillSampleProxySEVOwn(SamplesVal);
		else
		{
			vector<CSampleProxy> SamplesOwn, SamplesTrans, SamplesOwnTmp;
			CFreqScale FreqScaleTrans;
			CSpecPreprocData_Parcel preData;

			FillSampleProxySEVOwn(SamplesOwn, true);

			if(SamplesOwn.size() > 0)
			{
				FillFreqScaleTrans(FreqScaleTrans);
				FillSpecPreprocData(preData);

				USES_CONVERSION;

				string strPreproc(T2A(Model->GetTransPreprocAsStr()));

				IRCalibrate_PreCreate(&SamplesOwn, &FreqScaleTrans,	strPreproc, &preData);

				ExtractFromSampleProxyOwn(SamplesOwn, SamplesOwnTmp);
			}

			FillSampleProxySEVTrans(SamplesTrans);

			vector<CSampleProxy>::iterator itOwn = SamplesOwnTmp.begin();
			vector<CSampleProxy>::iterator itTrans = SamplesTrans.begin();
			int i, nValid = Model->GetNSamplesForValid();
			for(i=0; i<nValid; i++)
			{
				ModelSample *MSam = Model->GetSampleForValid(i);
				if(!MSam->IsTransN())
				{
					SamplesVal.push_back(*itOwn);
					++itOwn;
				}
				else
				{
					SamplesVal.push_back(*itTrans);
					++itTrans;
				}
			}
		}

		FillFreqScaleModel(FreqScale);
	}

	ItDbl itd;
	double fMeanMah, SqrSumMah = 0.;
	for(itd=pQnt->vMahalanobisDistances.begin(); itd!=pQnt->vMahalanobisDistances.end(); ++itd)
		SqrSumMah += (*itd)*(*itd);
	if(nSpCalib > 1)
		fMeanMah = sqrt(SqrSumMah / (nSpCalib - 1));
	else
		fMeanMah = 1.;

	int minfreq = Prj->GetSpecPointInd(Model->GetSpecPoint(0));
	int maxfreq = Prj->GetSpecPointInd(Model->GetSpecPoint(Model->GetNSpecPoints() - 1));
	int NExc = Model->GetNExcPoints();
	for(k=-1, l=minfreq; l<=maxfreq; l++)
	{
		for(i=0; i<NExc; i++)
		{
			int esp = Model->GetExcPoint(i);
			if(l == esp)
				break;
		}
		if(i < NExc)
			continue;

		k++;

		LoadingResult *pLR = pMRes->AddLoadingResult(l);
		if(pLR == NULL)
			continue;

		pLR->Spec_GK.clear();
		for(m=0; m<nComp; m++)
		{
			VDbl NewFact;
			copy(pQnt->m_Model.mFactors[m].begin(), pQnt->m_Model.mFactors[m].end(), inserter(NewFact, NewFact.begin()));

			pLR->Spec_GK.push_back(NewFact[k]);
		}
	}

	int counter = 0;
	for(k=0; k<nS; k++)
	{
		ModelSample *pSam = Model->GetSampleForCalib(k);
		if(pSam == NULL)
			continue;

		SampleResName = Model->GetSampleTransName(pSam);

		ItIntBool it;
		for(j=0, it=pSam->Spectra.begin(); it!=pSam->Spectra.end(); ++it)
		{
			if(!it->second)
				continue;

			SpectrumResult *pCalibRes = pMRes->AddCalibResult(pSam->SampleName, pSam->TransName, it->first, IsAvr);
			if(pCalibRes == NULL)
				continue;

			int ind = (IsAvr) ? k : counter;

			pCalibRes->Out_Mah = pQnt->vMahalanobisDistances[ind] / fMeanMah;

			for(m=0; m<nComp; m++)
			{
				if(IsHSO)
					pCalibRes->Scores_GK.push_back(pQnt->m_Model.mScores[ind][m]);
				else
					pCalibRes->Scores_GK.push_back(pQnt->m_Model.mScores[ind][m]);
			}

			j++;
			counter++;

			if(IsAvr)  break;
		}
	}

	vector<CSampleProxy>::iterator itV;
	for(j=0, k=0, itV = SamplesVal.begin(); k<nV; k++, ++itV)
	{
		ModelSample *pSamV = Model->GetSampleForValid(k);
		if(pSamV == NULL)
			continue;

		SampleResName = Model->GetSampleTransName(pSamV);

		ItIntBool it;
		int isp;
		for(isp=0, it=pSamV->Spectra.begin(); it!=pSamV->Spectra.end(); ++it)
		{
			if(!it->second)
				continue;

			SpectrumValResult *pValidRes = pMRes->AddValidResult(pSamV->SampleName, pSamV->TransName, it->first);
			if(pValidRes == NULL)
				continue;

			pValidRes->Out_Mah = QltAnaliz_GetExtSpecNormDistance(pCalibQlt, &FreqScale, &itV->mSpectra[isp]);

			isp++;
			j++;
		}
	}
}

void CModelCalculate::FillModelResultExp(CQnt_Parcel *pQnt, CQnt_Parcel *pQlt, CModelResult *pMRes)
{
	ExportResult *ExpRes = &pMRes->ExpResult;
	ExpRes->Clear();

	CString SampleResName;
	int i, j;
	int nP = Model->GetNModelParams();
	int nS = Model->GetNSamplesForCalib();
	int nSpCalib = Model->GetNSpectraForCalib();
	int nSpCalibAvr = (IsAvr) ? nS : nSpCalib;
	if(Model->GetPreprocNum(C_PREPROC_M) >= 0)
		nSpCalibAvr--;

	ExpRes->ModelName = Model->GetName();
	ExpRes->ModelType = Model->GetModelType();

	bool IsHQO = (ExpRes->ModelType == C_MOD_MODEL_HSO);

	ExpRes->SpecRangeLow = Model->GetLowSpecRange();
	ExpRes->SpecRangeUpper = Model->GetUpperSpecRange();
	ExpRes->FreqMin = Model->GetSpecPoint(0);
	ExpRes->FreqStep = Model->GetSpecPoint(1) - Model->GetSpecPoint(0);
	ExpRes->NFreq = Model->GetNSpecPoints();
	ExpRes->SpType = (IsTransNormal) ? C_MOD_SPECTRUM_ABS : Model->GetSpectrumType();
	ExpRes->Preproc = Model->GetOwnPreprocAsStr();
	ExpRes->MaxMah = Model->GetMaxMah();
	ExpRes->MeanMah = (IsHQO) ? pQlt->fMeanMahDist : pQnt->fMeanMahDist;
	ExpRes->IsTrans = IsTrans;
	ExpRes->FreqMinTrans = Trans->GetSpecPoint(0);
	ExpRes->FreqStepTrans = Trans->GetSpecPoint(1) - Trans->GetSpecPoint(0);
	ExpRes->NFreqTrans = Trans->GetNSpecPoints();
	ExpRes->SpTypeTrans = C_MOD_SPECTRUM_TRA;
	ExpRes->PreprocTrans = Model->GetTransPreprocAsStr();
	ExpRes->T0 = GetT0(nSpCalibAvr - Model->GetNFactor());
//	ExpRes->FMax = (IsHQO) ? pQlt->m_specPreprocData.fMax : pQnt->m_specPreprocData.fMax;
	ExpRes->FMax = pQnt->m_specPreprocData.fMax;

	bool isM = cIsPrepInStr(ExpRes->Preproc, C_PREPROC_M);
	bool isC = cIsPrepInStr(ExpRes->Preproc, C_PREPROC_C);
	bool isD = cIsPrepInStr(ExpRes->Preproc, C_PREPROC_D);
	bool isMt = cIsPrepInStr(ExpRes->PreprocTrans, C_PREPROC_M);
	bool isCt = cIsPrepInStr(ExpRes->PreprocTrans, C_PREPROC_C);
	bool isDt = cIsPrepInStr(ExpRes->PreprocTrans, C_PREPROC_D);

	ExpRes->ExcPoints.clear();
	int shift = Prj->GetSpecPointInd(Model->GetSpecPoint(0));
	for(i=0; i<Model->GetNExcPoints(); i++)
	{
		int pnt = Model->GetExcPoint(i) - shift;
		if(pnt < 0 || pnt >= Model->GetNSpecPoints())
			continue;

		ExpRes->ExcPoints.push_back(pnt);
	}

	if(IsTrans && Trans->GetType() == C_TRANS_TYPE_NORMAL)
	{
		CSampleData* ArtSample = Trans->GetArtSample();
		if(ArtSample == NULL)
			return;

		ArtSample->LoadSpectraData();
		if(isMt)
		{
			Spectrum* MeanSpec = ArtSample->GetSpectrumByNum(C_TRANS_SPECNUM_MEAN);
			if(MeanSpec != NULL)
				copy(MeanSpec->Data.begin(), MeanSpec->Data.end(),
					inserter(ExpRes->MeanSpecTrans, ExpRes->MeanSpecTrans.begin()));
		}
		if(isCt)
		{
			Spectrum* MeanSpecMSC = ArtSample->GetSpectrumByNum(C_TRANS_SPECNUM_MEANMSC);
			if(MeanSpecMSC != NULL)
				copy(MeanSpecMSC->Data.begin(), MeanSpecMSC->Data.end(),
					inserter(ExpRes->MeanSpecMSCTrans, ExpRes->MeanSpecMSCTrans.begin()));
		}
		if(isDt)
		{
			Spectrum* MeanSpecStd = ArtSample->GetSpectrumByNum(C_TRANS_SPECNUM_MEANSTD);
			if(MeanSpecStd != NULL)
				copy(MeanSpecStd->Data.begin(), MeanSpecStd->Data.end(),
					inserter(ExpRes->MeanSpecStdTrans, ExpRes->MeanSpecStdTrans.begin()));
		}
		ArtSample->FreeSpectraData();
	}

	if(isM)
	{
		copy(pQnt->m_specPreprocData.meanSpectrum.begin(), pQnt->m_specPreprocData.meanSpectrum.end(),
			inserter(ExpRes->MeanSpec, ExpRes->MeanSpec.begin()));

		if(IsHQO)
			copy(pQlt->m_specPreprocData.meanSpectrum.begin(), pQlt->m_specPreprocData.meanSpectrum.end(),
				inserter(ExpRes->MeanSpecHQO, ExpRes->MeanSpecHQO.begin()));
	}
	if(isC)
	{
		copy(pQnt->m_specPreprocData.meanSpectrumMSC.begin(), pQnt->m_specPreprocData.meanSpectrumMSC.end(),
			inserter(ExpRes->MeanSpecMSC, ExpRes->MeanSpecMSC.begin()));

		if(IsHQO)
			copy(pQlt->m_specPreprocData.meanSpectrumMSC.begin(), pQlt->m_specPreprocData.meanSpectrumMSC.end(),
				inserter(ExpRes->MeanSpecMSCHQO, ExpRes->MeanSpecMSCHQO.begin()));
	}
	if(isD)
	{
		copy(pQnt->m_specPreprocData.meanStdSpec.begin(), pQnt->m_specPreprocData.meanStdSpec.end(),
			inserter(ExpRes->MeanSpecStd, ExpRes->MeanSpecStd.begin()));

		if(IsHQO)
			copy(pQlt->m_specPreprocData.meanStdSpec.begin(), pQlt->m_specPreprocData.meanStdSpec.end(),
				inserter(ExpRes->MeanSpecStdHQO, ExpRes->MeanSpecStdHQO.begin()));
	}

	if(!IsHQO)
	{
		int len = int(pQnt->m_Model.mFactors.size());
		for(j=0; j<len; j++)
		{
			VDbl NewFact;
			copy(pQnt->m_Model.mFactors[j].begin(), pQnt->m_Model.mFactors[j].end(), inserter(NewFact, NewFact.begin()));

			ExpRes->Factors.push_back(NewFact);
		}

		len = int(pQnt->m_Model.mScores.size());
		for(j=0; j<len; j++)
		{
			VDbl NewScores;
			copy(pQnt->m_Model.mScores[j].begin(), pQnt->m_Model.mScores[j].end(), inserter(NewScores, NewScores.begin()));

			ExpRes->NSpec = len;
			ExpRes->Scores.push_back(NewScores);
		}
	}
	else
	{
		int len = int(pQlt->m_Model.mFactors.size());
		for(j=0; j<len; j++)
		{
			VDbl NewFact;
			copy(pQlt->m_Model.mFactors[j].begin(), pQlt->m_Model.mFactors[j].end(), inserter(NewFact, NewFact.begin()));

			ExpRes->Factors.push_back(NewFact);
		}

		len = int(pQlt->m_Model.mScores.size());
		for(j=0; j<len; j++)
		{
			VDbl NewScores;
			copy(pQlt->m_Model.mScores[j].begin(), pQlt->m_Model.mScores[j].end(), inserter(NewScores, NewScores.begin()));

			ExpRes->NSpec = len;
			ExpRes->Scores.push_back(NewScores);
		}
	}

	for(i=0; i<nP; i++)
	{
		ExportParamResult NewExpPR;
		ModelParam* ModPar = Model->GetModelParam(i);

		NewExpPR.ParamName = ModPar->ParamName;
		NewExpPR.LowCalibRange = ModPar->LowValue;
		NewExpPR.UpperCalibRange = ModPar->UpperValue;
		NewExpPR.MaxSKO = ModPar->LimMSD;
		NewExpPR.CorrB = ModPar->CorrB;
		NewExpPR.CorrA = ModPar->CorrA;
		NewExpPR.SEC = pQnt->vSEC[i];
		NewExpPR.MeanComp = (isM) ? pQnt->m_specPreprocData.meanComponents[i] : 0.;
		NewExpPR.MeanCompTrans = (isMt) ? pQnt->m_specPreprocDataTrans.meanComponents[i] : 0.;
		NewExpPR.MeanCompStd = (isD) ? pQnt->m_specPreprocData.meanStdComp[i] : 0.;

		if(!IsHQO)
		{
			int len = int(pQnt->m_Model.mCalibr.size());
			for(j=0; j<len; j++)
				NewExpPR.CalibrLin.push_back(pQnt->m_Model.mCalibr[j][i]);
		}
		else
		{
			int len = int(pQnt->m_Model.mCalibrLinear.size());
			for(j=0; j<len; j++)
			{
				NewExpPR.CalibrLin.push_back(pQnt->m_Model.mCalibrLinear[j][i]);
				NewExpPR.CalibrNonLin.push_back(pQnt->m_Model.mCalibrNonLinear[j][i]);
			}
		}

		ExpRes->ParRes.push_back(NewExpPR);
	}
}

void CModelCalculate::FillQltModelResultExp(CQnt_Parcel *pQnt, CModelResult *pMRes)
{
	ExportQltResult *ExpQltRes = &pMRes->ExpQltResult;
	ExpQltRes->Clear();

	CString SampleResName;
	int i, j;
	int nS = Model->GetNSamplesForCalib();
	int nSpCalib = Model->GetNSpectraForCalib();
	int nSpCalibAvr = (IsAvr) ? nS : nSpCalib;
	if(Model->GetPreprocNum(C_PREPROC_M) >= 0)
		nSpCalibAvr--;

	ExpQltRes->ModelName = Model->GetName();
	ExpQltRes->SpecRangeLow = Model->GetLowSpecRange();
	ExpQltRes->SpecRangeUpper = Model->GetUpperSpecRange();
	ExpQltRes->FreqMin = Model->GetSpecPoint(0);
	ExpQltRes->FreqStep = Model->GetSpecPoint(1) - Model->GetSpecPoint(0);
	ExpQltRes->NFreq = Model->GetNSpecPoints();
	ExpQltRes->SpType = (IsTransNormal) ? C_MOD_SPECTRUM_ABS : Model->GetSpectrumType();
	ExpQltRes->Preproc = Model->GetOwnPreprocAsStr();
	ExpQltRes->MaxMah = Model->GetMaxMah();
	ExpQltRes->MaxSKO = Model->GetMaxMSD();
	ExpQltRes->MeanMah = pQnt->fMeanMahDist;
	ExpQltRes->IsTrans = IsTrans;
	ExpQltRes->FreqMinTrans = Trans->GetSpecPoint(0);
	ExpQltRes->FreqStepTrans = Trans->GetSpecPoint(1) - Trans->GetSpecPoint(0);
	ExpQltRes->NFreqTrans = Trans->GetNSpecPoints();
	ExpQltRes->SpTypeTrans = C_MOD_SPECTRUM_TRA;
	ExpQltRes->PreprocTrans = Model->GetTransPreprocAsStr();

	bool isM = cIsPrepInStr(ExpQltRes->Preproc, C_PREPROC_M);
	bool isC = cIsPrepInStr(ExpQltRes->Preproc, C_PREPROC_C);
	bool isD = cIsPrepInStr(ExpQltRes->Preproc, C_PREPROC_D);
	bool isMt = cIsPrepInStr(ExpQltRes->PreprocTrans, C_PREPROC_M);
	bool isCt = cIsPrepInStr(ExpQltRes->PreprocTrans, C_PREPROC_C);
	bool isDt = cIsPrepInStr(ExpQltRes->PreprocTrans, C_PREPROC_D);


	ExpQltRes->ExcPoints.clear();
	int shift = Prj->GetSpecPointInd(Model->GetSpecPoint(0));
	for(i=0; i<Model->GetNExcPoints(); i++)
	{
		int pnt = Model->GetExcPoint(i) - shift;
		if(pnt < 0 || pnt >= Model->GetNSpecPoints())
			continue;

		ExpQltRes->ExcPoints.push_back(pnt);
	}

	if(IsTrans && Trans->GetType() == C_TRANS_TYPE_NORMAL)
	{
		CSampleData* ArtSample = Trans->GetArtSample();
		if(ArtSample == NULL)
			return;

		ArtSample->LoadSpectraData();
		if(isMt)
		{
			Spectrum* MeanSpec = ArtSample->GetSpectrumByNum(C_TRANS_SPECNUM_MEAN);
			if(MeanSpec != NULL)
				copy(MeanSpec->Data.begin(), MeanSpec->Data.end(),
					inserter(ExpQltRes->MeanSpecTrans, ExpQltRes->MeanSpecTrans.begin()));
		}
		if(isCt)
		{
			Spectrum* MeanSpecMSC = ArtSample->GetSpectrumByNum(C_TRANS_SPECNUM_MEANMSC);
			if(MeanSpecMSC != NULL)
				copy(MeanSpecMSC->Data.begin(), MeanSpecMSC->Data.end(),
					inserter(ExpQltRes->MeanSpecMSCTrans, ExpQltRes->MeanSpecMSCTrans.begin()));
		}
		if(isDt)
		{
			Spectrum* MeanSpecStd = ArtSample->GetSpectrumByNum(C_TRANS_SPECNUM_MEANSTD);
			if(MeanSpecStd != NULL)
				copy(MeanSpecStd->Data.begin(), MeanSpecStd->Data.end(),
					inserter(ExpQltRes->MeanSpecStdTrans, ExpQltRes->MeanSpecStdTrans.begin()));
		}
		ArtSample->FreeSpectraData();
	}

	if(isM)
	{
		copy(pQnt->m_specPreprocData.meanSpectrum.begin(), pQnt->m_specPreprocData.meanSpectrum.end(),
			inserter(ExpQltRes->MeanSpec, ExpQltRes->MeanSpec.begin()));
	}
	if(isC)
	{
		copy(pQnt->m_specPreprocData.meanSpectrumMSC.begin(), pQnt->m_specPreprocData.meanSpectrumMSC.end(),
			inserter(ExpQltRes->MeanSpecMSC, ExpQltRes->MeanSpecMSC.begin()));
	}
	if(isD)
	{
		copy(pQnt->m_specPreprocData.meanStdSpec.begin(), pQnt->m_specPreprocData.meanStdSpec.end(),
			inserter(ExpQltRes->MeanSpecStd, ExpQltRes->MeanSpecStd.begin()));
	}

	int len = int(pQnt->m_Model.mFactors.size());
	for(j=0; j<len; j++)
	{
		VDbl NewFact;
		copy(pQnt->m_Model.mFactors[j].begin(), pQnt->m_Model.mFactors[j].end(), inserter(NewFact, NewFact.begin()));

		ExpQltRes->Factors.push_back(NewFact);
	}
}

void CModelCalculate::CalcAllTrendLine(CModelResult *pMRes)
{
	if(pMRes == NULL)
		return;

	int nPSM = pMRes->GetNPSResult();
	for(int k=0; k<nPSM; k++)
	{
		ParamSumResult *pPSR = pMRes->GetPSResult(k);

		pMRes->CreateGraphSEC(k);
		pMRes->CreateGraphSECV(k);
		pMRes->CreateGraphSEV(k);

		pPSR->tSECb = pMRes->GetGraphSEC()->b;
		pPSR->tSECa = pMRes->GetGraphSEC()->a;
		pPSR->tSECVb = pMRes->GetGraphSECV()->b;
		pPSR->tSECVa = pMRes->GetGraphSECV()->a;
		pPSR->tSEVb = pMRes->GetGraphSEV()->b;
		pPSR->tSEVa = pMRes->GetGraphSEV()->a;
	}
}

void CModelCalculate::CalcError(int ecode, const TCHAR *comment)
{
	if(ecode <= CALCERR_OK)
		return;

	CString cmt = comment, str = GetErrorStr(ecode);
	str += _T(" ") + cmt;

	ParcelError(str);
}

CString CModelCalculate::GetErrorStr(int ecode)
{
	CString err;

	switch(ecode)
	{
	case CALCERR_OK:
		err = ParcelLoadString(STRID_ERROR_MODEL_CALC_OK);
		break;
	case CALCERR_NOSPEC:
		err = ParcelLoadString(STRID_ERROR_MODEL_CALC_NOSPEC);
		break;
	case CALCERR_NOREF:
		err = ParcelLoadString(STRID_ERROR_MODEL_CALC_NOREF);
		break;
	case CALCERR_NOCOMP:
		err = ParcelLoadString(STRID_ERROR_MODEL_CALC_NOCOMP);
		break;
	case CALCERR_MATH:
		err = ParcelLoadString(STRID_ERROR_MODEL_CALC_MATH);
		break;
	case CALCERR_QLTFACT:
		err = ParcelLoadString(STRID_ERROR_MODEL_CALC_QLTFACT);
		break;
	}

	return err;
}

double CModelCalculate::GetT0(int diff)
{
	if(diff <= 0)
		return 0.;
	if(diff > 200)
		return 1.9600;

	return T0[diff - 1];
}

CString CModelCalculate::GetTmpParName(int ind)
{
	CString TmpName;
	TmpName.Format(_T("Param%1d"), ind);

	return TmpName;
}
