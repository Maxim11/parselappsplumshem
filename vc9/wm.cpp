#include "stdafx.h"

#include "Parcel.h"
#include "MainFrm.h"
#include "wm.h"


LRESULT ParcelSendMDIFrame(UINT Message, WPARAM wParam, LPARAM lParam)
{
	return (pFrame != 0) ? pFrame->SendMessage(Message, wParam, lParam) : 0;
}

void ParcelSetStatusBar(int Index, CString Text)
{
	USES_CONVERSION;

	const char *Txt = T2A(Text);

	ParcelSendMDIFrame(PARCEL_WM_SET_STATUS_BAR, Index, (LPARAM) Txt);
}

void ParcelOpenProgressBar(int min, int max, int step)
{
	ParcelSendMDIFrame(PARCEL_WM_OPEN_PROGRESS_BAR, step, MAKELPARAM(max, min));
}

void ParcelCloseProgressBar()
{
	ParcelSendMDIFrame(PARCEL_WM_CLOSE_PROGRESS_BAR);
}

void ParcelSetProgressBar(int pos)
{
	ParcelSendMDIFrame(PARCEL_WM_SET_PROGRESS_BAR, TRUE, pos);
}

void ParcelSetProgressBar()
{
	ParcelSendMDIFrame(PARCEL_WM_SET_PROGRESS_BAR, false);
}
