#include "stdafx.h"

#include "DeleteProjectWaitDlg.h"
#include "BranchInfo.h"

IMPLEMENT_DYNAMIC(CDeleteProjectWaitDlg, CWaitingDlg)

CDeleteProjectWaitDlg::CDeleteProjectWaitDlg(CDeviceData* dev, CString name, CWnd* pParent) : CWaitingDlg(pParent)
{
	DevData = dev;
	PrjName = name;

	Comment = ParcelLoadString(STRID_PROJECT_DELETE_WAIT_COMMENT);
}

CDeleteProjectWaitDlg::~CDeleteProjectWaitDlg()
{
}

void CDeleteProjectWaitDlg::DoDataExchange(CDataExchange* pDX)
{
	CWaitingDlg::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDeleteProjectWaitDlg, CWaitingDlg)
END_MESSAGE_MAP()


BOOL CDeleteProjectWaitDlg::OnInitDialog()
{
	CWaitingDlg::OnInitDialog();

	return true;
}

void CDeleteProjectWaitDlg::OK()
{
	CWaitingDlg::OK();
}

void CDeleteProjectWaitDlg::Stop()
{
	CWaitingDlg::Stop();
}

void CDeleteProjectWaitDlg::Work()
{
	ParcelWait(true);
	
	bool res = GetDatabase()->ProjectDelete(DevData->GetNumber(), PrjName);

	ParcelWait(false);

	if(res)
		OK();
	else
		Stop();
}
