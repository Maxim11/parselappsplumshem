#ifndef _CHOOSE_GK_DLG_H_
#define _CHOOSE_GK_DLG_H_

#pragma once

#include "Resource.h"
#include "ModelResult.h"


///////////////////////////////////////////////////////////////////////////////////////////////////
// ������ ������ ������� ���������

class CChooseGKDlg : public CDialog
{
	DECLARE_DYNAMIC(CChooseGKDlg)

public:

	CChooseGKDlg(CModelResult* data, CWnd* pParent = NULL);		// �����������
	virtual ~CChooseGKDlg();									// ����������

	enum { IDD = IDD_CHOOSE_GK };	// ������������� �������

	CListBox m_listAll;				// �������, ���������� ������ ���� ��������
	CListBox m_listSel;				// �������, ���������� ������ ���� ��������, ��������� ��� �����������

protected:

	CModelResult* Data;

	SInt Sel;

	bool IsBtnClk;

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();						// ������������� �������

	virtual void OnOK()
	{};
	virtual void OnCancel();				// ��������� ������� "�������� ���� � ����� �� �������"

	afx_msg void OnAccept();				// ��������� ������� "������� ������ � ����� �� �������"
	afx_msg void OnAdd();
	afx_msg void OnAddAll();
	afx_msg void OnRemove();
	afx_msg void OnRemoveAll();
	afx_msg void OnHelp();					// ��������� ������� "������� �������"

	void FillListAll(int newsel = -1);
	void FillListSel(int newsel = -1);

	bool IsChanged();

	DECLARE_MESSAGE_MAP()
};

#endif
