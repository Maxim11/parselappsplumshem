#include "stdafx.h"

#include "TemplateNameDlg.h"
#include "BranchInfo.h"
#include "MainFrm.h"


IMPLEMENT_DYNAMIC(CTemplateNameDlg, CDialog)

CTemplateNameDlg::CTemplateNameDlg(Template* data, int mode, CWnd* pParent)
	: CDialog(CTemplateNameDlg::IDD, pParent)
{
	Tmpl = data;

	Mode = mode;

	m_vName = Tmpl->Name;
	m_vNote = Tmpl->Note;

}

CTemplateNameDlg::~CTemplateNameDlg()
{
}

void CTemplateNameDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_INPUT_NAME, m_Name);
	DDX_Control(pDX, IDC_INPUT_NOTE, m_Note);

	cParcelDDX(pDX, IDC_INPUT_NAME, m_vName);
	DDV_MaxChars(pDX, m_vName, 50);
	cParcelDDX(pDX, IDC_INPUT_NOTE, m_vNote);
	DDV_MaxChars(pDX, m_vNote, 50);
}


BEGIN_MESSAGE_MAP(CTemplateNameDlg, CDialog)

	ON_BN_CLICKED(IDC_ACCEPT, OnAccept)
	ON_BN_CLICKED(IDHELP, OnHelp)

END_MESSAGE_MAP()


BOOL CTemplateNameDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	CString Hdr;
	switch(Mode)
	{
	case C_TMPL_MODE_CREATE:  Hdr = ParcelLoadString(STRID_TEMPLATE_NAME_HDR_CREATE);  break;
	case C_TMPL_MODE_EDIT:  Hdr = ParcelLoadString(STRID_TEMPLATE_NAME_HDR_EDIT);  break;
	}
	SetWindowText(Hdr);

	GetDlgItem(IDC_ACCEPT)->SetWindowText(ParcelLoadString(STRID_TEMPLATE_NAME_ACCEPT));
	GetDlgItem(IDCANCEL)->SetWindowText(ParcelLoadString(STRID_TEMPLATE_NAME_DECLINE));
	GetDlgItem(IDHELP)->SetWindowText(ParcelLoadString(STRID_TEMPLATE_NAME_HELP));
	GetDlgItem(IDC_STATIC_1)->SetWindowText(ParcelLoadString(STRID_TEMPLATE_NAME_NAME));
	GetDlgItem(IDC_STATIC_2)->SetWindowText(ParcelLoadString(STRID_TEMPLATE_NAME_NOTE));

	if(Mode == C_TMPL_MODE_EDIT)
	{
		GetDlgItem(IDC_STATIC_1)->EnableWindow(false);
		GetDlgItem(IDC_INPUT_NAME)->EnableWindow(false);
	}

	GetDlgItem(IDHELP)->EnableWindow(pFrame->IsExistHelp());

	return true;
}

void CTemplateNameDlg::OnAccept()
{
	if(!UpdateData())
		return;

	m_vName.Trim();
	if(m_vName.IsEmpty())
	{
		ParcelError(ParcelLoadString(STRID_TEMPLATE_NAME_ERROR_EMPTYNAME));
		return;
	}
	if(Mode == C_TMPL_MODE_CREATE || Tmpl->Name.Compare(m_vName))
	{
		if(GetOptimTemplates()->GetTemplate(m_vName) != NULL)
		{
			CString s;
			s.Format(ParcelLoadString(STRID_TEMPLATE_NAME_ERROR_NAME), m_vName);
			ParcelError(s);
			return;
		}
	}

	Tmpl->Name = m_vName;
	Tmpl->Note = m_vNote;

	CDialog::OnOK();
}

void CTemplateNameDlg::OnCancel()
{
	CDialog::OnCancel();
}

void CTemplateNameDlg::OnHelp()
{
	int IdHelp = DLGID_MODEL_TEMPLATE_NAME;

	pFrame->ShowHelp(IdHelp);
}
