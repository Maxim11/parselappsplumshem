#pragma once

//	legends

#define LCUL_NONE			0x000
#define LCUL_HIDDEN			0x001
#define LCUL_MARKER			0x010
#define LCUL_LINE			0x020

class CLCULString
{
friend class CLC;
friend class CLCUL;

	UINT nFlags;

	CString strText;

	COLORREF clrLineColor;
	int nLineStyle;
	int nLineWidth;

	CLumChartDataMarker marker;

public:
	CLCULString();

	void SetLine(COLORREF clr, int nStyle, int nWidth) {clrLineColor = clr; nLineStyle = nStyle; nLineWidth = nWidth;}
	void SetMarker(CLumChartDataMarker _marker) {marker =_marker;}
	void SetText(CString str) {strText = str;}

	void Enable(bool bEnable);			//	default
	void EnableLine(bool bEnable);		//		 
	void EnableMarker(bool bEnable);	//	 
};

#define LCUL_FIT_MOVE		0x100
#define LCUL_FIT_SIZE		0x200
#define LCUL_AUTO_WIDTH		0x400
#define LCUL_AUTO_HEIGHT	0x800

class CLCUL
{
friend class CLC;

	int nStepX;
	int nStepY;
	int nLines;

	CRect FitToRect(CRect rect, CRect rectTo);
	CSize EstimateSize(CDC *pDC);

	UINT nFlags;

	CPoint ptOrg;
	CSize szOrg;

	COLORREF clrFill;
	COLORREF clrBorder;
	COLORREF clrText;

	UINT nFontSize;
	CString strFontName;

	int nStrings;
	CLCULString *strings;

public:

	CLCUL();
	CLCUL(const CLCUL &legend);
	~CLCUL();

//	left-top corner relative to graph area left-top corner 
	void MoveTo(int newX, int newY);

//	size in pixels
	void Resize(int newWidth, int newHeight);

//	strings data array
	void SetStrings(CLCULString *stringsNew, int numStrings);
	void SetStrings(const std::vector<CLCULString> &stringsNew, int numStrings=-1);

//	font face and size 
	void SetFont(CString strName=_T("Arial"), int nSize=10);

//	colors
	void SetFillColor(COLORREF clrNewFill = RGB(255,255,255));
	void SetBorderColor(COLORREF clrNewBorder = RGB(128,0,0));
	void SetTextColor(COLORREF clrNewText = RGB(0,0,0));

//	flags
		
	void Enable(bool bEnable);				//	default
	void EnableMoveToFit(bool bEnable);		//	default
	void EnableSizeToFit(bool bEnable);			
	void EnableAutoWidth(bool bEnable);		//	default
	void EnableAutoHeight(bool bEnable);	//	default
};

