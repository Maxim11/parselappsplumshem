#pragma once

#pragma warning( disable : 4702 )
#include <vector>
#pragma warning( default : 4702 )


//	������� ������
class CLCDataSet;
class CLC;


//	���� ������
#define LCF_OK	0
#define LCF_ERR	1

//	������� ������������ ������ �� �������
#define LCF_CONNECT_NONE		0		//	��� ������������
#define LCF_CONNECT_DIRECT		1		//	��������
#define LCF_CONNECT_SPLINE		2		//	���������� ������
#define LCF_CONNECT_STEPH		3		//	��������� ������-�����
#define LCF_CONNECT_STEPV		4		//	��������� �����-������
#define LCF_CONNECT_HISTO		5		//	�����������
#define LCF_CONNECT_EXT			6		//	callback

//	���� �������� ������
#define LCF_MARKERS_NONE		0x00	//	��� ��������
#define LCF_MARKERS_SQUARE		0x01		
#define LCF_MARKERS_ROUND		0x02
#define LCF_MARKERS_DIAMOND		0x03
#define LCF_MARKERS_TRIUP		0x04
#define LCF_MARKERS_TRIDN		0x05

//	����� ���������, ��������� etc
#define LCF_GRAPH_HIDDEN		0x01
#define LCF_GRAPH_MUSER			0x02
#define LCF_GRAPH_TEXT			0x04

typedef double (LC_PLOTFUNC) (double);

struct CLumChartDataMarker
{
	int nSize;
	int nType;
	COLORREF nColor;
	CLumChartDataMarker(): nSize(4), nType(LCF_MARKERS_NONE), nColor(RGB(0,0,0)) {};
	CLumChartDataMarker(int ntype, int nsize, int ncolor): nSize(nsize), nType(ntype), nColor(ncolor) {};
};

#include "LCUL.h"


//	������
class CLumChartDataSet
{
friend class CLumChart;

protected:
	CLCDataSet *pDataSet;
	POSITION pos;

	UINT nAttrFlags;

	CString strName;

	CString strFmtX;
	CString strFmtY;

	int nConnectType;

	int nLineWidth;
	int nLineStyle;
	COLORREF nLineColor;


	int nMarkerSize;
	int nMarkerType;
	COLORREF nMarkerColor;

	CLumChartDataMarker *pUserMarkers;
	bool bUserMarkersEnabled;

	void PreInit();
	CLCDataSet *GetDataSet() {return pDataSet;}

public:
//----------------------------------------------------------------------------------------------
//	������ �����������
	CLumChartDataSet();

//	����������� ��� ������ X-Y
	CLumChartDataSet(int nPoints, double *x, double *y);
	CLumChartDataSet(int nPoints, const std::vector<double> &vx, const std::vector<double> &vy);

//	����������� ��� �������������� X � �������� Y
	CLumChartDataSet(int nPoints, double x0, double dx, double *y);
	CLumChartDataSet(int nPoints, double x0, double dx, const std::vector<double> &vy);

//	������������ � ���������
//	��� �������� X
	CLumChartDataSet(int nPoints, double *x, double (*f) (double));
	CLumChartDataSet(int nPoints, const std::vector<double> &vx, double (*f) (double));

//	��� �������������� X
	CLumChartDataSet(int nPoints, double x0, double dx, double (*f) (double));

//	����������� �-� �� ��������� [x1-x2]
	CLumChartDataSet(double x1, double x2, double (*f) (double));
//----------------------------------------------------------------------------------------------

//	������������� ������� ����� ����������
	POSITION GetPos() {return pos;}

//	�������� (�����������)
//	��� ������ ������ (��� �������)
	int SetName(CString strName);						//d

//	������ ���������� ����������� ������ (��� �������)
	int SetFormat(CString _strFmtX, CString _strFmtY);						//d

//	���� �������
	int SetColor(COLORREF nColor);						//d

//	������ ����� �������
	int SetWidth(int nWidth);							//d

//	����� ����� �������
	int SetStyle(int nStyle);

//	������ ������������
	int SetInterpolation(int nConnect);

//	���, ������ � ���� �������� ������
	int SetMarkerType(int nType);
	int SetMarkerSize(int nSize);
	int SetMarkerColor(COLORREF Color);
	int SetMarker(int nType, int nSize, COLORREF Color);


//	�������������� ������� ������
	int SetMarkerArray(CLumChartDataMarker *pMarkers);
	int EnableUserMarkers(bool bEnable);
};

#define LCF_RECT_CLIENT	1
#define LCF_RECT_SCREEN	2

// User callbacks stuff
typedef int (LC_CALLBACK)(UINT, CPoint, void*);

#define LCF_CB_KEYDOWN		0
#define LCF_CB_LBDOWN		1
#define LCF_CB_LBUP			2
#define LCF_CB_LBDBLCLICK	3
#define LCF_CB_RBDOWN		4
#define LCF_CB_RBUP			5
#define LCF_CB_MOUSEMOVE	6
#define LCF_CB_NUM			7

struct LC_CALLBACKS
{
	LC_CALLBACK* pFunc[LCF_CB_NUM];
	UINT nFlags;
};

class CLumChart
{
	CLC* pWndLC;

public:
//	�����������
//	������� �������� ���� � ������� rect ������������� ���� pParent
	CLumChart(CRect rect, CWnd *pParent, UINT nFlags);
	~CLumChart();

//	���������������� ����
	void SetUserPopupMenu(CWnd *pWnd, CMenu *pMenu);
	void EnableUserPopupMenu(bool bEnable);

//	���������� ������� (������ ������) �� ������
	POSITION AddGraph(CLumChartDataSet* pDataSet, bool bVisible);

//	���������� ���������� �������
	int ShowGraph(POSITION pos, bool bShow);
	int IsGraphEnabled(POSITION pos, bool *pbShow);

//	���������� ���������� ������ � �����������
	int EnableGraphText(POSITION pos, bool bEnable);

//	�������� �������
	int RemoveGraph(POSITION pos);
	int RemoveAllGraphs();
//	����� �������
	int FindGraph(POSITION pos, CLumChartDataSet *pGraph);
//	���������� �������
	int UpdateGraph(POSITION pos, CLumChartDataSet* pDataSet, bool bRedrawChart);
//	�������� ������
	int SetActiveGraph(POSITION pos);

//	���������� ��������� ������������
	POSITION AddMask(double x1, double x2, bool bBorder, bool bForeground, COLORREF color);
	int RemoveMask(POSITION pos);
	int RemoveAllMasks();

//	���������� �������
	POSITION AddUserLegend(CLCUL legend);
	CLCUL* GetUserLegend(POSITION pos);
	int RemoveUserLegend(POSITION pos);
	int RemoveUserLegends();


//	����� ����������� �������, ���������� � �������� ������
	int LocateGraphPoints(POSITION posGraph, CRect rect, int *pnPoints);
	int IsPointInSelRect(CPoint point, int *pnResult);
	int IsGraphInRect(POSITION posGraph, CRect rect, bool *pbResult);

//	�-� ����
	int RedrawWindow();
	CWnd* GetWnd();

//	��������
	//	�������� ����
	int SetAxesNames(CString strX, CString strY);
	//	���� ������ � �����
	COLORREF SetBgOutsideColor(COLORREF clrFill, COLORREF clrBorder=-1);
	//	���� ������� ����������� ������
	COLORREF SetBgInsideColor(COLORREF clr);

	//  Highlighting
	int SetHighlightMode(bool mode);
	int SetHighlightColor(COLORREF color);

	//	����
	int SetMargins(int mLeft, int mTop, int mRight, int mBottom);

//	�������� �����
	int SetGridFontSize(UINT nFontSize);

//	�������� ����� ���������
	int SetSelFrameProps(COLORREF clr, int nWidth);


//	�������� �������
	int SetLegendCaption(CString strCaption);
	int SetLegendFontName(CString strFontName);
	int SetLegendFontSize(UINT nFontSize);
	int SetLegendPos(CPoint p);
	int SetLegendOrder(bool bLastComesFirst);


//	������ �������
	int IsLegendEnabled(bool *pbEnabled);
	int IsUpperLegendEnabled(bool *pbEnabled);
	int IsCoordLegendEnabled(bool *pbEnabled);
	int IsGridEnabled(bool *pbHorzOn, bool *pbVertOn);
	int IsGoForwardAvailable(bool *pbAvailable);
	int IsGoBackAvailable(bool *pbAvailable);


//	��������
	//	��������� � ��������� �������� ��������� ����������� ������
	int GetViewPort(double *xMin, double *yMin, double *xMax, double *yMax);
	int SetViewPort(double xMin, double yMin, double xMax, double yMax);
	int SetUserViewPort(double xMin, double yMin, double xMax, double yMax);
	int GetViewPortXY(CPoint point, double *pX, double *pY);


//  ������� X � Y �������� ����
//  ����������/������ ��������� �������� ����
	int SetSolidAxisHorzAt(double ay);
	int EnableSolidAxisHorz(bool bEnable);
	int SetSolidAxisVertAt(double ax);
	int EnableSolidAxisVert(bool bEnable);

	//	������ ���������� �� ��� �, � ����� �� ������. �� ��������� = 0.1 
	int SetScroll(double fDelta);

	//	��������/�������� �����
	int ShowGrid(bool bGridHorz, bool bGridVert);

	//	��������/�������� �������
	int ShowLegend(int bShow);
	int EnableUpperLegend(int bEnable);
	int EnableCoordLegend(int bEnable);

	//	�������� ���� ���������
	int EmptyZoomList();


	//	������������ ����
	int DoScrollLeft();
	int DoScrollRight();
	int DoFullScreen();
	int DoAutoY();
	int DoPrevScale();
	int DoNextScale();

	int ExportPicture(const TCHAR* strName);


//	��������
	LC_CALLBACKS Callbacks;
	void ResetUserCallbacks();
	void SetUserCallback(UINT nEvent, LC_CALLBACK *pCallback);
	int EnableUserCallback(UINT nEvent, bool bEnable);
};

