#pragma once

/////////////////////////////////////////////////////////////////////////////
// CLCDataSet


#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

#include "LumChart.h"


// Data types
#define LC_TYPE_FIXED 0 // Fixed X data gap
#define LC_TYPE_XDATA 1 // X data array present
#define LC_TYPE_CBACK 2 // Use supplied callback function

// Interpolation methods
#define LC_IT_NONE		LCF_CONNECT_NONE	// no connection
#define LC_IT_DIRECT	LCF_CONNECT_DIRECT  // Connect points directly
#define LC_IT_SPLINE	LCF_CONNECT_SPLINE  // Spline interpolation
#define LC_IT_STEPH		LCF_CONNECT_STEPH	// Connect points with horz steps
#define LC_IT_STEPV		LCF_CONNECT_STEPV	// Connect points with vert steps
#define LC_IT_HISTO		LCF_CONNECT_HISTO	// Draw hystogramm
#define LC_IT_EXTERNAL	LCF_CONNECT_EXT		// Draw function

class CLCDataSet
{
public:
	CLCDataSet(int type);						// Create CLCDataSet of the specified type
	~CLCDataSet();								// Destroys CLCDataSet object
	int GetDataType(void);						// Gets data type
	void SetCallBack(double (*pFunc)(double));	// Set callback routine for TYPE_CBACK type
	void Sort();

	// Array level interface
	void SetLength(int length);				// Set number of points. Allocate memory.
	int GetPointCount(void);					// Get number of points

	int *iOrg;									// 
	double *ax;									// X data
	double *ay;									// Y data
	double xMin, xMax;							// X axis data range for evently space data

	// ���������� ��������� �� ������ �������� �������
	double* GetDataPtr();

	// Other routines
	double GetIMIN(void); // Returns the minimum X point
	double GetIMAX(void); // Returns the maximum X point

	// Calculates the minimum and maximum Y data values on the specified gap
	virtual void GetMinMax(double x0, double x1, double *y0, double *y1);  
	                    
	// Retrieves number range of points between x0 and x1
	void GetPointsRange(double x0, double x1, long *n0, long *n1);	

	// Retrieves coordinates of the point number n.
	void GetPoint(int n, double *x, double *y) const; 

	int CLCDataSet::GetOrgIdx(int n) const;

	// Interpolation routines
	void SetInterpolation(int type); // Sets interpolation method

	int GetInterpolation(void);      // Gets interpolation method

	double f(double x); // Calculates the value in the specified point
	                    // using specified interpolation method

	// Other properties
	CTime date; // Time of data accusing

protected:

	double* y2; // Used for spline calculations
	int nItype;  // Interpolation method
	int nDtype;  // Data type
	int nPoints; // Number of dots

	double (*pcb)(double);				// Pointer to callback function
	long FindFirst(double x);			// Find first element, greater than x
	long FindLast(double x);			// Find last element, lower than x
	void Spline(void);					// Build spline

};

/*
#include <vector>

// ����������� ������ ������ � ������
extern std::vector<double> ConvertDataSetToVector(const CLCDataSet* pData);
// ����������� ������ � ������ ������
extern void ConvertVectorToDataSet(CLCDataSet* pData, const std::vector<double>& vecData);
*/