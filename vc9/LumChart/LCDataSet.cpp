// GCLCDataSet.cpp : implementation file
//

#include "../stdafx.h"
#include "LCDataSet.h"
#include "math.h"

//#ifdef _DEBUG
//#define new DEBUG_NEW
//#undef THIS_FILE
//static TCHAR THIS_FILE[] = __FILE__;
//#endif




struct DPOINT
{
	int idx;
	double x;
	double y;
};


static int compare_dpoints(const void *p1, const void *p2)
{
	double x1 = ((DPOINT*)p1)->x;
	double x2 = ((DPOINT*)p2)->x;
	return (x2 > x1) ? -1 : ((x2 < x1) ? 1 : 0);
}


static int sort_dpoints(int len, double *x, double *y, int *idx)
{
	DPOINT *a = new DPOINT[len];
	if(!a) return 0;

	for(int i=0; i<len; i++)
	{
		a[i].idx = i;
		a[i].x = x[i];
		if(y) a[i].y = y[i];
	}

	qsort((void *)a, len, sizeof(DPOINT), compare_dpoints);

	for(int i=0; i<len; i++)
	{
		idx[i] = a[i].idx;
		x[i] = a[i].x;
		if(y) y[i] = a[i].y;
	}
	
	delete [] a;
	return 1;
}


/////////////////////////////////////////////////////////////////////////////
// CLCDataSet

CLCDataSet::CLCDataSet(int type)
{
	nPoints = 0;
	ax = NULL;
	ay = NULL;
	iOrg = NULL;

//	date = CTime(1970, 1, 1, 24, 0, 0);
	date = CTime::GetCurrentTime();
	nDtype = type;
	pcb = NULL;
	xMin = xMax = 0;

	y2 = NULL;
}

CLCDataSet::~CLCDataSet()
{
	if (iOrg != NULL) delete [] iOrg;
	if (ax != NULL) delete [] ax;
	if (ay != NULL) delete [] ay;
	if (y2 != NULL) delete [] y2;
}

int CLCDataSet::GetDataType(void)
{
	return nDtype;
}

void CLCDataSet::SetLength(int n)
{
	if (iOrg != NULL) delete [] iOrg;
	iOrg = NULL;
	if (ax != NULL) delete [] ax;
	ax = NULL;
	if (ay != NULL) delete [] ay;
	ay = NULL;

	switch (nDtype) 
	{
	case LC_TYPE_FIXED:
		iOrg = new int[n];
		ay = new double[n];
		break;

	case LC_TYPE_XDATA:
		iOrg = new int[n];
		ax = new double[n];
		ay = new double[n];
		break;
	}

	nPoints = n;
}

void CLCDataSet::Sort(void)
{
	sort_dpoints(nPoints, ax, ay, iOrg);
	xMin = ax[0];
	xMax = ax[nPoints-1];
}

int CLCDataSet::GetPointCount(void)
{
	return nPoints;
}

// ���������� ��������� �� ������ �������� �������
double* CLCDataSet::GetDataPtr()
{
	return ay;
}

int CLCDataSet::GetOrgIdx(int n) const
{
	if(iOrg && n<nPoints) 
		return iOrg[n];
	else
		return 0;
}

void CLCDataSet::GetPoint(int n, double *x, double *y) const
{
	switch (nDtype) 
	{
	case LC_TYPE_FIXED:
		{
			if (nPoints == 1)	
				*x = xMin; 
			else if (n == nPoints-1) 
				*x = xMax; 
			else 
				*x = xMin + n*(xMax - xMin)/(nPoints - 1);

			*y = ay[n];
			break;
		}

	case LC_TYPE_XDATA:
		{
			*x = ax[n];
			*y = ay[n];
			break;
		}

	case LC_TYPE_CBACK:
		break;
	}
}



long CLCDataSet::FindFirst(double x)
{
	long n;
	double dx;
	long l, r, b;
	switch (nDtype) 
	{
	case LC_TYPE_FIXED:
		if (xMin >= x) return 0;
		if (xMax <= x) return nPoints-1;

		dx = (xMax - xMin)/(nPoints - 1);
		n = (int)((x - xMin)/dx);
		while ((xMin + n*dx) > x) n--;
		while ((xMin + n*dx) <= x) n++;
		return n;

	case LC_TYPE_XDATA:
		if (ax[0] >= x) return 0;
		if (ax[nPoints-1] < x) return nPoints-1;

		l = 0;
		r = nPoints-1;
		while (r-l > 1) 
		{
			b = (l+r)/2;
			if (ax[b] < x) 
				l = b; 
			else
				r = b;
		}
		return r;

	case LC_TYPE_CBACK:
		return -1;
	}

	return -1;
}

long CLCDataSet::FindLast(double x)
{
	long n;
	double dx;
	long l, r, b;
	switch (nDtype) 
	{
	case LC_TYPE_FIXED:
		if (xMin >= x) return 0;
		if (xMax <= x) return nPoints-1;

		dx = (xMax - xMin)/(nPoints - 1);
		n = (int)((x - xMin)/dx);
		while ((xMin + n*dx) < x) n++;
		while ((xMin + n*dx) >= x) n--;

		return n;
	case LC_TYPE_XDATA:
		if (ax[0] > x) return 0;
		if (ax[nPoints-1] <= x) return nPoints-1;

		l = 0;
		r = nPoints-1;
		while (r-l > 1) 
		{
			b = (l+r)/2;
			if (ax[b] <= x) 
				l = b; 
			else
				r = b;
		}
		return l;
	case LC_TYPE_CBACK:
		return -1;
	}

	return -1;
}

double CLCDataSet::f(double x)
{
	long n;
	double h,b,a;
	double dx;

	switch (nDtype) 
	{
	case LC_TYPE_FIXED:
		switch (nItype) 
		{
		case LC_IT_SPLINE:
			{
				if (y2 == NULL) return 0;
				if (x < xMin) return 0;
				if (x > xMax) return 0;
				if (nPoints == 1) return ay[0];
				n = FindLast(x); 
				if (n < 0) return ay[0];
				if (n >= nPoints-1) return ay[nPoints-1];
				h = (xMax - xMin)/(nPoints - 1);
				a = (xMin - x)/h + n + 1;
				b = (x - xMin)/h - n;
				return (a*ay[n] + b*ay[n+1] + ((a*a*a - a)*y2[n]+(b*b*b - b)*y2[n+1])*(h*h)/6.0);
			}
		case LC_IT_STEPH:
		case LC_IT_HISTO:
			{
				if (x < xMin) return 0;
				if (x > xMax) return 0;
				n = FindLast(x); 
				if (n < 0) return ay[0];
				if (n >= nPoints-1) return ay[nPoints-1];
				return ay[n];
			}
		case LC_IT_STEPV:
			{
				if (x < xMin) return 0;
				if (x > xMax) return 0;
				n = FindLast(x); 
				if (n < 0) n = 0;
				if (n >= (nPoints - 1)) n = nPoints - 2;
				return ay[n+1];
			}
		case LC_IT_DIRECT:
		case LC_IT_NONE:
			{
				if (x < xMin) return 0;
				if (x > xMax) return 0;
				if (nPoints == 1) return ay[0];
				n = FindLast(x); 
				if (n < 0) return ay[0];
				if (n >= nPoints-1) return ay[nPoints-1];
				dx = (xMax - xMin)/(nPoints - 1);
				return ay[n] + (ay[n+1] - ay[n])*(x - xMin - dx*n)/dx;
			}
		}
		break;
	case LC_TYPE_XDATA:
		switch (nItype) 
		{
		case LC_IT_SPLINE:
			if (y2 == NULL) return 0;
			if (x < ax[0]) return 0;
			if (x > ax[nPoints-1]) return 0;
			if (nPoints == 1) return ay[0];
			n = FindLast(x); 
			if (n < 0) return ay[0];
			if (n >= nPoints-1) return ay[nPoints-1];
			h = ax[n+1] - ax[n];
			if (h == 0) return ax[n+1];
			a = (ax[n+1]-x)/h;
			b = (x-ax[n])/h;
			return (a*ay[n] + b*ay[n+1] + ((a*a*a-a)*y2[n]+(b*b*b-b)*y2[n+1])*(h*h)/6.0);

		case LC_IT_HISTO:
		case LC_IT_STEPH:
			if (x < ax[0]) return 0;
			if (x > ax[nPoints-1]) return 0;
			n = FindLast(x); if (n < 0) n = 0;
			return ay[n];

		case LC_IT_STEPV:
			if (x < ax[0]) return 0;
			if (x > ax[nPoints-1]) return 0;
			n = FindLast(x); 
			if (n < 0) n = 0;
			if (n > (nPoints-2)) n = nPoints-2;
			return ay[n+1];

		case LC_IT_DIRECT:
		case LC_IT_NONE:
			if (x < ax[0]) return 0;
			if (x > ax[nPoints-1]) return 0;
			if (nPoints == 1) return ay[0];
			n = FindLast(x); 
			if (n < 0) return ay[0];
			if (n >= nPoints-1) return ay[nPoints-1];
			if (ax[n+1] == ax[n]) return ay[n];
			return ay[n]+(ay[n+1]-ay[n])*(x-ax[n])/(ax[n+1]-ax[n]);
		}
		break;

	case LC_TYPE_CBACK:
		return (*pcb)(x);
	}

	return 0;
}

double CLCDataSet::GetIMIN(void)
{
	switch (nDtype) 
	{
	case LC_TYPE_FIXED:
		return xMin;

	case LC_TYPE_XDATA:
		return ax[0];

	case LC_TYPE_CBACK:
		return xMin;
	}

	return 0;
}

double CLCDataSet::GetIMAX(void)
{
	switch (nDtype) 
	{
	case LC_TYPE_FIXED:
		return xMax;

	case LC_TYPE_XDATA:
		return ax[nPoints-1];

	case LC_TYPE_CBACK:
		return xMax;
	}

	return 0;
}

void CLCDataSet::GetMinMax(double x0, double x1, double *y0, double *y1)
{
	double min, max;
	long n0, n1, n;
	double x, y, dx;
	
	if (x0 < GetIMIN()) x0 = GetIMIN();
	if (x1 > GetIMAX()) x1 = GetIMAX();
	
	if (x0 > x1) 
	{
		*y0 = 0;
		*y1 = 0;
		return;
	}
	
	switch (nDtype) 
	{
	case LC_TYPE_FIXED:
	case LC_TYPE_XDATA:
		min = max = f(x0);
		if (f(x1) > max) max = f(x1);
		if (f(x1) < min) min = f(x1);
		GetPointsRange(x0, x1, &n0, &n1);
		for (n = n0; n <= n1; n++) {
			GetPoint(n, &x, &y);
			if (y > max) max = y;
			if (y < min) min = y;
		}
		break;

	case LC_TYPE_CBACK:
		dx = (x1 - x0) / 100;
		x = x0;
		min = max = f(x0);
		while (x < x1) {
			y = f(x);
			if (y > max) max = y;
			if (y < min) min = y;
			x += dx;
		}
		break;
	}
	if (y0 != NULL) *y0 = min;
	if (y1 != NULL) *y1 = max;
}

void CLCDataSet::GetPointsRange(double x0, double x1, long *n0, long *n1)
{
	if (n0 != NULL) *n0 = FindFirst(x0);
	if (n1 != NULL) *n1 = FindLast(x1);
}

void CLCDataSet::Spline(void)
{
	int i,k;
	double p,qn,sig,un;
	double *u;
	double dx;

	if ((nDtype != LC_TYPE_FIXED) && (nDtype != LC_TYPE_XDATA)) return;

	if (y2 != NULL) delete y2;
	y2 = new double[nPoints];
	u = new double[nPoints];

	y2[0] = 0.0;
	u[0] = 0.0;
	switch (nDtype) 
	{
	case LC_TYPE_FIXED:
		dx = (xMax - xMin)/(nPoints - 1);
		for (i = 1; i<nPoints-1; i++) 
		{
			sig = 0.5;
			p = sig*y2[i-1]+2.0;
			y2[i] = (sig-1.0)/p;
			u[i] = (ay[i-1]-2*ay[i]+ay[i+1])/dx;
			u[i] = (3.0*u[i]/dx-sig*u[i-1])/p;
		}
		break;
	case LC_TYPE_XDATA:
		for (i = 1; i<nPoints-1; i++) {
			sig = (ax[i]-ax[i-1])/(ax[i+1]-ax[i-1]);
			p = sig*y2[i-1]+2.0;
			y2[i] = (sig-1.0)/p;
			u[i] = (ay[i+1]-ay[i])/(ax[i+1]-ax[i]) - (ay[i]-ay[i-1])/(ax[i]-ax[i-1]);
			u[i] = (6.0*u[i]/(ax[i+1]-ax[i-1])-sig*u[i-1])/p;
		}
		break;
	}
	qn = 0.0;
	un = 0.0;
	y2[nPoints-1] = (un-qn*u[nPoints-2])/(qn*y2[nPoints-2]+1.0);
	for (k = nPoints-2; k>=0; k--)
		y2[k] = y2[k]*y2[k+1]+u[k];

	delete [] u;
}

void CLCDataSet::SetInterpolation(int type)
{
	if (y2 != NULL) { delete y2; y2 = NULL; }
	nItype = LC_IT_DIRECT;
	
	switch (type) 
	{
	case LC_IT_SPLINE:
		Spline();
		break;

	case LC_IT_HISTO:
	case LC_IT_STEPH:
	case LC_IT_STEPV:
		break;

	case LC_IT_NONE:
	case LC_IT_DIRECT:
	case LC_IT_EXTERNAL:
		break;

	default:
		return;
	}

	nItype = type;
}

int CLCDataSet::GetInterpolation(void)
{
	return nItype;
}

void CLCDataSet::SetCallBack(double (*pFunc)(double))
{
	pcb = pFunc;
}
/*
// ����������� ������ ������ � ������
std::vector<double> ConvertDataSetToVector(const CLCDataSet* pData)
{
	return std::vector<double>();
}

// ����������� ������ � ������ ������
void ConvertVectorToDataSet(CLCDataSet* pData, const std::vector<double>& vecData)
{
}
*/