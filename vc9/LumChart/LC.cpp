// LC.cpp : implementation file
//

#include "../stdafx.h"
//#include "../dlview/dlview.h"
#include "LC.h"
#include "math.h"

#pragma warning( disable : 4018) 
#pragma warning( disable : 4244) 

enum { MS_NONE, MS_SELRECT, MS_POINT, MS_MOVE,
	MS_RIGHT, MS_LEFT, MS_TOP, MS_BOTTOM, MS_DTL, MS_DTR, MS_DBL, MS_DBR };

typedef struct {double x0, y0, x1, y1;} VPORT;

#define LC_PLOTCLNUM 8
COLORREF LC_PLOTCOLOR[LC_PLOTCLNUM] = {
	RGB(0, 0, 0),
	RGB(192, 0, 0),
	RGB(0, 192, 0),
	RGB(0, 0, 192),
	RGB(0, 192, 192),
	RGB(192, 0, 192),
	RGB(192, 192, 0),
	RGB(192, 192, 192) 
};
/*
COLORREF PLOTCOLOR[LC_PLOTCLNUM] = {
	RGB(0, 0, 0),
	RGB(192, 0, 0),
	RGB(0, 192, 0),
	RGB(0, 0, 192),
	RGB(0, 192, 192),
	RGB(192, 0, 192),
	RGB(192, 192, 0),
	RGB(192, 192, 192) 
};
*/
void ForcePointToRect(CPoint &p, CRect rect)
{
	if(rect.PtInRect(p)) return;
	p.x = min(p.x, rect.right);
	p.x = max(p.x, rect.left);
	p.y = min(p.y, rect.bottom);
	p.y = max(p.y, rect.top);
}

double fround(double x)
{
	double d = x - floor(x);
	return (d < 0.5) ? floor(x) : ceil(x);
}

/////////////////////////////////////////////////////////////////////////////
// CLC

CLC::CLC()
{
	listZoom.RemoveAll();
	listMasks.RemoveAll();
	listLegends.RemoveAll();
	listLocator.RemoveAll();
	listPlotInfo.RemoveAll();

	bCapture	= 0;
	posZoom		= NULL;
	MsAction	= MS_NONE;
	sel			= CRect(0, 0, 0, 0);
	actplot		= NULL;

	bPif		= false;
	bPifEnable	= true;
	bCross		= false;
	bData		= false;
	bLegend		= LC_LEGEND;
	bLocatorEnable	= true;
	bLocator	= false;
	bMasks		= false;

	bMarkersAuto= false;

	bGridHorz	= true;
	bGridVert	= true;

	bUserMenu	= false;
	pUserMenu	= NULL;
	pUserWnd	= NULL;

	nCaps		= LC_CAPS;
	nScaleMode	= LC_SCALE_NOFUNC;
	vpx0 = vpy0 = -0.01;
	vpx1 = vpy1 = 1.01;
	fScroll		= 0.1;

	bInvertX	= false;

	nLocatorSpan = 2;

	hlMode.nFlags = LC_HL_COLOR;
	hlMode.color  = RGB(0,0,0);

	clrBgOutBorder = LC_DKBG_COLOR;
	clrBgOut	= LC_DKBG_COLOR;
	clrBgIn		= LC_LTBG_COLOR;

	rMargins = CRect(LC_GAP_L, LC_GAP_T, LC_GAP_R, LC_GAP_B);

	strLegendText = _T("");
	strLegendFont = _T("Arial");
	nLegendFontSize = 100;
	nLegendChars = 10;
	nLegendLines = -1;

	nGridFontSize = 100;

	bLegendOrder = false;
	ptLegend = CPoint(0,0);
//	ptLegend = CPoint(10, 10);

	pSelFrameBrush = NULL;
	SetSelFrameProps(RGB(0,0,0), 4);

	SetSolidAxisHorzAt(0);
	EnableSolidAxisHorz(false);
	SetSolidAxisVertAt(0);
	EnableSolidAxisVert(false);


	pCallbacks = 0;
}

CLC::~CLC()
{
	POSITION pos;

	pos = listZoom.GetHeadPosition();
	while (pos != NULL)
		delete (VPORT*)listZoom.GetNext(pos);
	listZoom.RemoveAll();

	pos = listPlotInfo.GetHeadPosition();
	while (pos != NULL)
		delete (LC_PLOTINFO*)listPlotInfo.GetNext(pos);
	listPlotInfo.RemoveAll();

	if(pSelFrameBrush) delete pSelFrameBrush; 
}


BEGIN_MESSAGE_MAP(CLC, CWnd)
	//{{AFX_MSG_MAP(CLC)
	ON_WM_PAINT()
	ON_WM_SIZE()
	ON_WM_ERASEBKGND()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_KEYDOWN()
	ON_WM_RBUTTONUP()
	ON_WM_RBUTTONDOWN()
	ON_COMMAND(LC_POPUP_FULL_SCREEN, OnFullScreen)
	ON_COMMAND(LC_POPUP_GO_BACK, OnGoBack)
	ON_COMMAND(LC_POPUP_GO_FORWARD, OnGoForward)
	ON_COMMAND(LC_POPUP_ZOOM_IN, OnZoomIn)
	ON_COMMAND(LC_POPUP_LEGEND, OnLegend)
	ON_COMMAND(LC_POPUP_XLEFT, OnXleft)
	ON_COMMAND(LC_POPUP_XRIGHT, OnXright)
	ON_COMMAND(LC_POPUP_AUTOY, OnAutoY)
	ON_COMMAND(LC_POPUP_EXPORT, OnExportImage)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CLC message handlers

void CLC::SetSelFrameProps(COLORREF clr, int nWidth)
{
	nSelFrameWidth = nWidth;
	clrSelFrame = clr;

	if(pSelFrameBrush) delete pSelFrameBrush; 
	pSelFrameBrush = new CBrush(HS_BDIAGONAL, clr);
}

// Mouse & keyboard events

void CLC::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
/*
	if(pCallbacks)
	{
		UINT nEvent = LCF_CB_KEYDOWN;
		if(pCallbacks->nFlags & (1<<nEvent))
		{
			LC_CALLBACK *pFunc = pCallbacks->pFunc[nEvent];
			if(pFunc != NULL)
			{
				void *pCallbackData = 0;
				if(!pFunc(nFlags, point, pCallbackData)) return;
			}
		}
	}
*/
	if (!bData) return;

	if (nChar == VK_ESCAPE) 
	{
		MsAction = MS_NONE;
		SetActCur(MsAction);
		RedrawWindow();
	}
	CWnd::OnKeyDown(nChar, nRepCnt, nFlags);
}


void CLC::OnMouseMove(UINT nFlags, CPoint point) 
{
	if(pCallbacks)
	{
		UINT nEvent = LCF_CB_MOUSEMOVE;
		if(pCallbacks->nFlags & (1<<nEvent))
		{
			LC_CALLBACK *pFunc = pCallbacks->pFunc[nEvent];
			if(pFunc != NULL)
			{
				void *pCallbackData = 0;
				if(!pFunc(nFlags, point, pCallbackData)) return;
			}
		}
	}

	if (!bData) return;

	if (MsAction == MS_NONE) 
	{
		if (!sel.IsRectEmpty())	SetActCur(GetPos(point));
	} 
	else 
	{
		SetActCur(MsAction);
	}

	if (MsAction != MS_NONE) 
	{
		CDC *pDC = GetDC();
		CRect nr, or;
		switch (MsAction) 
		{
		case MS_POINT:
			if (point.x < rPlot.left)   point.x = rPlot.left;
			if (point.y < rPlot.top)    point.y = rPlot.top;
			if (point.x > rPlot.right)  point.x = rPlot.right;
			if (point.y > rPlot.bottom) point.y = rPlot.bottom;
			DrawCross(pDC, mspt);
			mspt = point;
			if ((abs(pd.x-point.x) > 20) ||	(abs(pd.y-point.y) > 20)) 
			{
				MsAction = MS_SELRECT;
				sel.SetRectEmpty();
				dsel.SetRectEmpty();
				bDragFirst = 1;
				bCross = 0;
				nr = CRect(pd.x, pd.y, point.x, point.y);
			} 
			else
			{
				DrawCross(pDC, mspt);
			}
			break;

		case MS_MOVE:
			nr = RValid(sel+point-pd);
			if (nr.left   < rPlot.left)   { nr.right  -= nr.left-rPlot.left;     nr.left   = rPlot.left;   }
			if (nr.top    < rPlot.top)    { nr.bottom -= nr.top-rPlot.top;       nr.top    = rPlot.top;    }
			if (nr.right  > rPlot.right)  { nr.left   += rPlot.right-nr.right;   nr.right  = rPlot.right;  }
			if (nr.bottom > rPlot.bottom) { nr.top    += rPlot.bottom-nr.bottom; nr.bottom = rPlot.bottom; }
			break;

		case MS_SELRECT:
			if (point.x < rPlot.left)   point.x = rPlot.left;
			if (point.y < rPlot.top)    point.y = rPlot.top;
			if (point.x > rPlot.right)  point.x = rPlot.right;
			if (point.y > rPlot.bottom) point.y = rPlot.bottom;
			nr = CRect(pd.x, pd.y, point.x, point.y);
			break;

		case MS_LEFT:
			nr = CRect(sel.left+point.x-pd.x, sel.top, sel.right, sel.bottom);
			if (nr.left < rPlot.left)  nr.left = rPlot.left;
			if (nr.left > rPlot.right) nr.left = rPlot.right;
			break;

		case MS_RIGHT:
			nr = CRect(sel.left, sel.top, sel.right+point.x-pd.x, sel.bottom);
			if (nr.right < rPlot.left)  nr.right = rPlot.left;
			if (nr.right > rPlot.right) nr.right = rPlot.right;
			break;

		case MS_TOP:
			nr = CRect(sel.left, sel.top+point.y-pd.y, sel.right, sel.bottom);
			if (nr.top < rPlot.top)    nr.top = rPlot.top;
			if (nr.top > rPlot.bottom) nr.top = rPlot.bottom;
			break;

		case MS_BOTTOM:
			nr = CRect(sel.left, sel.top, sel.right, sel.bottom+point.y-pd.y);
			if (nr.bottom < rPlot.top)    nr.bottom = rPlot.top;
			if (nr.bottom > rPlot.bottom) nr.bottom = rPlot.bottom;
			break;

		case MS_DTL:
			nr = CRect(sel.left+point.x-pd.x, sel.top+point.y-pd.y, sel.right, sel.bottom);
			if (nr.top  < rPlot.top)    nr.top = rPlot.top;
			if (nr.top  > rPlot.bottom) nr.top = rPlot.bottom;
			if (nr.left < rPlot.left)   nr.left = rPlot.left;
			if (nr.left > rPlot.right)  nr.left = rPlot.right;
			break;

		case MS_DTR:
			nr = CRect(sel.left, sel.top+point.y-pd.y, sel.right+point.x-pd.x, sel.bottom);
			if (nr.top   < rPlot.top)    nr.top = rPlot.top;
			if (nr.top   > rPlot.bottom) nr.top = rPlot.bottom;
			if (nr.right < rPlot.left)   nr.right = rPlot.left;
			if (nr.right > rPlot.right)  nr.right = rPlot.right;
			break;

		case MS_DBL:
			nr = CRect(sel.left+point.x-pd.x, sel.top, sel.right, sel.bottom+point.y-pd.y);
			if (nr.bottom < rPlot.top)    nr.bottom = rPlot.top;
			if (nr.bottom > rPlot.bottom) nr.bottom = rPlot.bottom;
			if (nr.left   < rPlot.left)   nr.left = rPlot.left;
			if (nr.left   > rPlot.right)  nr.left = rPlot.right;
			break;

		case MS_DBR:
			nr = CRect(sel.left, sel.top, sel.right+point.x-pd.x, sel.bottom+point.y-pd.y);
			if (nr.bottom < rPlot.top)    nr.bottom = rPlot.top;
			if (nr.bottom > rPlot.bottom) nr.bottom = rPlot.bottom;
			if (nr.right  < rPlot.left)   nr.right = rPlot.left;
			if (nr.right  > rPlot.right)  nr.right = rPlot.right;
			break;
		}

		if (MsAction != MS_POINT) 
		{
			or = dsel;
			nr = RValid(nr);
			dsel = nr;
			int nw = nSelFrameWidth;
			nr.InflateRect(nw,nw,nw,nw);
			or.InflateRect(nw,nw,nw,nw);
			CBrush brush(clrSelFrame^clrBgIn); 
			if (bDragFirst)
				pDC->DrawDragRect(nr, CSize(nw,nw), NULL, CSize(nw, nw), &brush, &brush); 
			else
				pDC->DrawDragRect(nr, CSize(nw,nw), or, CSize(nw, nw), &brush, &brush);

			bDragFirst = 0;
		}

		if (MsAction != MS_MOVE) 
		{
			if (point.x < rPlot.left)   point.x = rPlot.left;
			if (point.y < rPlot.top)    point.y = rPlot.top;
			if (point.x > rPlot.right)  point.x = rPlot.right;
			if (point.y > rPlot.bottom) point.y = rPlot.bottom;
			double x = vpx0 + (vpx1-vpx0)*(point.x-rPlot.left)/(rPlot.Width()+1);
			double y = vpy0 + (vpy1-vpy0)*(rPlot.bottom-point.y)/(rPlot.Height()+1);
			if(bInvertX) x = vpx0 + vpx1 - x;

			LC_CLICK_AREA* pArea = LocateData(point);
			if(!pArea)
			{
				SetPIF(x, y);
			}
			else 
			{
				SetPIF(pArea->x, pArea->y);
			}

//			SetPIF(x, y);
			DrawPIF(pDC); // Draw point info frame
		}

		DrawLegend(pDC); // Draw legend
		DrawUserLegends(pDC);
		ReleaseDC(pDC);
	}
}

void CLC::OnLButtonDown(UINT nFlags, CPoint point) 
{
	if(pCallbacks)
	{
		UINT nEvent = LCF_CB_LBDOWN;
		if(pCallbacks->nFlags & (1<<nEvent))
		{
			LC_CALLBACK *pFunc = pCallbacks->pFunc[nEvent];
			if(pFunc != NULL)
			{
				void *pCallbackData = 0;
				if(!pFunc(nFlags, point, pCallbackData)) return;
			}
		}
	}

	if (!bData) return;
	SetCapture();
	bCapture = 1;

	bDragFirst = 1;
	pd = point;
	dsel = sel;

	MsAction = GetPos(point);
	if (MsAction == MS_NONE) 
	{
		if (point.x < rPlot.left)   point.x = rPlot.left;
		if (point.y < rPlot.top)    point.y = rPlot.top;
		if (point.x > rPlot.right)  point.x = rPlot.right;
		if (point.y > rPlot.bottom) point.y = rPlot.bottom;

		mspt = pd = point;

		dsel.SetRectEmpty();
		if (!sel.IsRectEmpty()) 
		{
			sel.SetRectEmpty();
			RedrawWindow();
		}


		MsAction = MS_POINT;
		CDC *pDC = GetDC();
		double x = vpx0 + (vpx1-vpx0)*(point.x-rPlot.left)/(rPlot.Width()+1);
		double y = vpy0 + (vpy1-vpy0)*(rPlot.bottom-point.y)/(rPlot.Height()+1);
		if(bInvertX) x = vpx0 + vpx1 - x;

		LC_CLICK_AREA* pArea = LocateData(point);
		if(!pArea)
		{
			SetPIF(x, y);
			bLocator = false;
		}
		else 
		{
			hlMode.nFlags &= ~LC_HL_SELECTION;
			hlMode.nFlags |= LC_HL_POINT;
			hlMode.point = point;
//			hlMode.color = RGB(255-GetRValue(clrBgIn), 255-GetGValue(clrBgIn), 255 - GetBValue(clrBgIn));
			DrawDataSet(pDC, pArea->pPlotInfo, 1);
			SetPIF(pArea->x, pArea->y);
			bLocator = true;
		}

		DrawCross(pDC, point);
		bCross = 1;

		DrawPIF(pDC); // Draw point info frame

		DrawLegend(pDC);  // Draw legend
		DrawUserLegends(pDC);
		DrawLocatorData(pDC, pArea);
		ReleaseDC(pDC);
	} 
	else 
	{
		SetActCur(MsAction);
	}
}

void CLC::OnLButtonUp(UINT nFlags, CPoint point) 
{
	if (!bData) return;

	if(pCallbacks)
	{
		UINT nEvent = LCF_CB_LBUP;
		if(pCallbacks->nFlags & (1<<nEvent))
		{
			LC_CALLBACK *pFunc = pCallbacks->pFunc[nEvent];
			if(pFunc != NULL)
			{
				void *pCallbackData = 0;
				if (MsAction != MS_POINT && MsAction != MS_NONE) 
				{
					pCallbackData = &dsel;
				}
				if(!pFunc(nFlags, point, pCallbackData)) return;
			}
		}
	}

	if (bCapture) ReleaseCapture();
	bCapture = 0;

	// Remove point info frame
	RemovePIF();
	bCross = 0;

	if (MsAction == MS_POINT) 
	{
		DrawCross(NULL, mspt);
//		RedrawWindow(rPoint);
		RedrawWindow();
		DrawLegend(NULL);
		DrawUserLegends(NULL);
	} 
	else if (MsAction != MS_NONE) 
	{
		sel = dsel;
		GetParent()->SendMessage(WM_GRAPH, GCN_SEL_CHANGED);
		RedrawWindow();

		hlMode.nFlags &= ~LC_HL_SELECTION;
		hlMode.nFlags |= LC_HL_RECT;
		hlMode.rect = sel;
		CDC *pDC = GetDC();
		DrawPlot(pDC, 1);
		DrawSel(pDC);
		DrawLegend(pDC);
		DrawUserLegends(pDC);
		ReleaseDC(pDC);
	}

	MsAction = MS_NONE;
}

void CLC::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
	if(pCallbacks)
	{
		UINT nEvent = LCF_CB_LBDBLCLICK;
		if(pCallbacks->nFlags & (1<<nEvent))
		{
			LC_CALLBACK *pFunc = pCallbacks->pFunc[nEvent];
			if(pFunc != NULL)
			{
				void *pCallbackData = 0;
				if(!pFunc(nFlags, point, pCallbackData)) return;
			}
		}
	}

	if (!bData) return;

	double x0, x1, y0, y1;

	if (GetPos(point) == MS_MOVE) 
	{
		if (sel.left == rPlot.left)     x0 = vpx0; else
			x0 = vpx0 + (vpx1-vpx0)*(sel.left-rPlot.left)/(rPlot.Width()+1);
		if (sel.right == rPlot.right)   x1 = vpx1; else
			x1 = vpx0 + (vpx1-vpx0)*(sel.right+1-rPlot.left)/(rPlot.Width()+1);
		if (sel.bottom == rPlot.bottom) y0 = vpy0; else
			y0 = vpy0 + (vpy1-vpy0)*(rPlot.bottom-sel.bottom)/(rPlot.Height()+1);
		if (sel.top == rPlot.top)       y1 = vpy1; else
			y1 = vpy0 + (vpy1-vpy0)*(rPlot.bottom-sel.top+1)/(rPlot.Height()+1);

		if(bInvertX)
		{
			double x;
			x = vpx0 + vpx1 - x1;
			x1 = vpx0 + vpx1 - x0;
			x0 = x;
		}

		if (!SetViewPort(x0, y0, x1, y1))
		{
			CString strMsg = _T("As zoom sa limit");
			MessageBox(strMsg);
		}
		sel.SetRectEmpty();
		GetParent()->SendMessage(WM_GRAPH, GCN_SEL_CHANGED);
		RedrawWindow();
		GetParent()->PostMessage(WM_TIMER, 1000 + LC_POPUP_ZOOM_IN, 0);
	} 
	else
	{
		OnLButtonDown(nFlags, point);
	}
}


void CLC::OnRButtonDown(UINT nFlags, CPoint point) 
{
	if(pCallbacks)
	{
		UINT nEvent = LCF_CB_RBDOWN;
		if(pCallbacks->nFlags & (1<<nEvent))
		{
			LC_CALLBACK *pFunc = pCallbacks->pFunc[nEvent];
			if(pFunc != NULL)
			{
				void *pCallbackData = 0;
				if(!pFunc(nFlags, point, pCallbackData)) return;
			}
			return;
		}
	}

	if (!bData) return;

//	OnLButtonUp(nFlags, point);
}

void CLC::OnRButtonUp(UINT nFlags, CPoint point) 
{
	if(pCallbacks)
	{
		UINT nEvent = LCF_CB_RBUP;
		if(pCallbacks->nFlags & (1<<nEvent))
		{
			LC_CALLBACK *pFunc = pCallbacks->pFunc[nEvent];
			if(pFunc != NULL)
			{
				void *pCallbackData = 0;
				if(!pFunc(nFlags, point, pCallbackData)) return;
			}
		}
	}

	if(bUserMenu && (pUserMenu != NULL) && (pUserWnd != NULL))
	{
		ClientToScreen(&point);
//		UINT nSelID = pUserMenu->TrackPopupMenuEx(TPM_LEFTALIGN|TPM_RIGHTBUTTON|TPM_RETURNCMD|TPM_NONOTIFY, 
//			                        point.x, point.y, pUserWnd, 0);
		pUserMenu->TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON, point.x, point.y, pUserWnd);
		return;
	}

	if (!bData) return;
	if (!sel.IsRectEmpty() && sel.PtInRect(point)) 
	{
		CMenu *pMenu;
		pMenu = new CMenu;
		pMenu->CreatePopupMenu();
		BuildSelPopup(pMenu);
		ClientToScreen(&point);
		pMenu->TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON, point.x, point.y, this);
		pMenu->DestroyMenu();
		delete pMenu;
	} 
	else 
	{
		CMenu *pMenu;
		pMenu = new CMenu;
		pMenu->CreatePopupMenu();
		BuildMainPopup(pMenu);
		ClientToScreen(&point);
		pMenu->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, this);
		pMenu->DestroyMenu();
		delete pMenu;
	}
}


// WM_SIZE message

void CLC::OnSize(UINT nType, int cx, int cy) 
{
	CWnd::OnSize(nType, cx, cy);
	
	rGrid = CRect(0, 0, cx-1, cy-1);
	rPlot = CRect(rMargins.left, rMargins.top, cx-rMargins.right-1, cy-rMargins.bottom-1);
 	rLocator = CRect(rPlot.left+140, rPlot.top - 25, rPlot.right, rPlot.top - 2);
	if (sel.right > rPlot.right) sel.right = rPlot.right;
	if (sel.bottom > rPlot.bottom) sel.bottom = rPlot.bottom;
	if (sel.left >= sel.right) sel.SetRectEmpty();
	if (sel.top >= sel.bottom) sel.SetRectEmpty();
	if ((rPlot.left >= rPlot.right) || (rPlot.top >= rPlot.bottom)) rPlot.SetRectEmpty();
	UpdateLPSize();
}


// Drawing functions

void CLC::Print(CDC* pDC, CPrintInfo* pInfo)
{
	DrawBg(pDC);
	DrawGrid(pDC);
	DrawPlot(pDC, 0);
	DrawSel(pDC);
	if(bCross) DrawCross(pDC, mspt);
	DrawLegend(pDC);
	DrawUserLegends(pDC);
	DrawPIF(pDC);
}

void CLC::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
	DrawBg(&dc);
	DrawGrid(&dc);
	DrawPlot(&dc, 0);
	DrawSel(&dc);
	if(bCross) DrawCross(&dc, mspt);
	UpdateLPSize();
	DrawLegend(&dc);
	DrawUserLegends(&dc);
	DrawPIF(&dc);
}


BOOL CLC::OnEraseBkgnd(CDC* pDC) 
{
	DrawBg(pDC);

	return TRUE;
}

int CLC::SetRectMargins(CRect rect)
{
	if(rect.left >= 0) rMargins.left = rect.left;
	if(rect.top >= 0) rMargins.top = rect.top;
	if(rect.right >= 0) rMargins.right = rect.right;
	if(rect.bottom >= 0) rMargins.bottom = rect.bottom;

	OnSize(SIZE_RESTORED, rGrid.Width()+1, rGrid.Height()+1); 

	return 1;
}

COLORREF CLC::SetBgInside(COLORREF clr)
{
	COLORREF clrOld = clrBgIn;
	clrBgIn = clr;
	return clrOld;
}

COLORREF CLC::SetBgOutside(COLORREF clrFill, COLORREF clrBorder)
{
	COLORREF clrOld = clrBgOut;
	clrBgOut = clrFill;
	if(clrBorder != -1) 
	{
		clrBgOutBorder = clrBorder; 
	}
	else
	{
		clrBgOutBorder = clrFill; 
	}
	return clrOld;
}


void CLC::DrawBg(CDC *pDC)
{
	CPen *pDkGray, *pLtGray, *pOutBorder, *oldpen;
	pDkGray = new CPen(PS_SOLID, 1, RGB(104,104,104));
	pLtGray = new CPen(PS_SOLID, 1, RGB(224,224,224));
	pOutBorder = new CPen(PS_SOLID, 1, clrBgOutBorder);
	oldpen = pDC->GetCurrentPen();

//	if (rPlot.IsRectEmpty() || (!bData)) 
	if (rPlot.IsRectEmpty()) 
	{

		// Fill outside background
		pDC->FillSolidRect(rGrid.left, rGrid.top, rGrid.Width()+1, rGrid.Height()+1, clrBgOut);
	} 
	else 
	{

		// Fill outside background
		pDC->FillSolidRect(rGrid.left,    rGrid.top,      rGrid.Width()+1,           rPlot.top-rGrid.top-1,       clrBgOut);
		pDC->FillSolidRect(rGrid.left,    rPlot.bottom+2, rGrid.Width()+1,           rGrid.bottom-rPlot.bottom-1, clrBgOut);
		pDC->FillSolidRect(rGrid.left,    rPlot.top-1,    rPlot.left-rGrid.left-1,   rPlot.Height()+3,            clrBgOut);
		pDC->FillSolidRect(rPlot.right+2, rPlot.top-1,    rGrid.right-rPlot.right-1, rPlot.Height()+3,            clrBgOut);

		// Draw inside frame
		pDC->SelectObject(pDkGray);
		pDC->MoveTo(rPlot.left-1,  rPlot.bottom+1);
		pDC->LineTo(rPlot.left-1,  rPlot.top-1);
		pDC->LineTo(rPlot.right+1, rPlot.top-1);
		pDC->SelectObject(pLtGray);
		pDC->LineTo(rPlot.right+1, rPlot.bottom+1);
		pDC->LineTo(rPlot.left-1,  rPlot.bottom+1);

		// Fill inside background
		pDC->FillSolidRect(rPlot.left, rPlot.top, rPlot.Width()+1, rPlot.Height()+1, clrBgIn);
		DrawMasks(pDC, false);
	}

	pDC->SelectObject(pOutBorder);
	pDC->MoveTo(rGrid.left,  rGrid.bottom);
	pDC->LineTo(rGrid.left,  rGrid.top);
	pDC->LineTo(rGrid.right, rGrid.top);
	pDC->LineTo(rGrid.right, rGrid.bottom);
	pDC->LineTo(rGrid.left, rGrid.bottom);

	pDC->SelectObject(oldpen);
	delete pLtGray;
	delete pDkGray;
	delete pOutBorder;
}

void CLC::DrawMasks(CDC *pDC, bool bFg)
{
	LC_MASK* pMask;
	POSITION pos = listMasks.GetTailPosition();
	while(pos != NULL) 
	{
		pMask = (LC_MASK*)listMasks.GetPrev(pos);

		if(pMask->x1 > vpx1) continue;
		if(pMask->x2 < vpx0) continue;
		if(bool(pMask->nFlags & LC_MASK_FG) != bFg) continue;

		double x1 = max(pMask->x1, vpx0);
		double x2 = min(pMask->x2, vpx1);

		int ix1 = InvX(fround((x1-vpx0)/(vpx1-vpx0)*(rPlot.Width()+1)));
		int ix2 = InvX(fround((x2-vpx0)/(vpx1-vpx0)*(rPlot.Width()+1)));

		pDC->FillSolidRect(ix1+1, rPlot.top, ix2-ix1-1, rPlot.Height()+1, pMask->color);

		if(pMask->nFlags & LC_MASK_BORDER)
		{
			CPen *pDkGray = new CPen(PS_SOLID, 1, RGB(64,64,64));
			CPen *pOldPen = pDC->SelectObject(pDkGray);
			Move2(pDC, ix1, rPlot.top);
			Line2(pDC, ix1, rPlot.bottom);
			Move2(pDC, ix2, rPlot.top);
			Line2(pDC, ix2, rPlot.bottom);
			delete pDC->SelectObject(pOldPen);
		}
	}
}



void CLC::DrawGrid(CDC *pDC)
{
	if (rPlot.IsRectEmpty()) return;
//	if (!bData) return;

	CDC *pdc = pDC;
	if (pdc == NULL) pDC = GetDC();

	int n2;
	int l;
	CString s;
	CSize ss;
	int n;
	double d, z, i;


	// Create paint tools
	CPen *pDkGrid, *pLtGrid, *pBlack, *pWhite, *oldpen;
	CFont *oldfont, *pLblFnt;

	pBlack  = new CPen(PS_SOLID, 1, RGB(0,0,0));
	pWhite  = new CPen(PS_SOLID, 1, RGB(255,255,255));
	pDkGrid = new CPen(PS_DOT,   1, RGB(104,104,104));
	pLtGrid = new CPen(PS_DOT,   1, RGB(192,192,192));
	pLblFnt = new CFont();

	pLblFnt->CreatePointFont(nGridFontSize, _T("Arial"), pDC);
	oldpen = pDC->GetCurrentPen();
	oldfont = pDC->GetCurrentFont();

	// Calculate grid gaps


	int nx2 = 5;  // Number of gaps between bold lines
	double dx = (vpx1-vpx0)*20/(rPlot.Width()+1);
	if (dx != 0) 
	{
		n = 0;
		while(dx < 1) { dx *= 10.; n++; }

		while(dx >= 10.) { dx /= 10.; n--; }
		
		if(dx < 1.5)	
		{ 
			dx = 1.; nx2 = 5; 
		} 
		else if (dx < 3)
		{ 
			dx = 2.; nx2 = 5; 
		} 
		else if(dx < 7)
		{ 
			dx = 5.; nx2 = 4; 
		} 
		else
		{ 
			dx = 10.; nx2 = 5; 
		}
		
		while (n > 0) { dx /= 10.; n--; }
		while (n < 0) { dx *= 10.; n++; }
	}


	int ny2 = 5;  // Number of gaps between bold lines
	double dy = (vpy1-vpy0)*20/(rPlot.Height()+1);
	if (dy != 0) 
	{
		n = 0;
		while (dy < 1) { dy *= 10; n++; }
		while (dy >= 10) { dy /= 10; n--; }
		if (dy < 1.5)	{ dy = 1;   ny2 = 2; } else
		if (dy < 3)		{ dy = 2.5; ny2 = 2; } else
		if (dy < 7)		{ dy = 5;   ny2 = 2; } else
						{ dy = 10; ny2 = 2; }
		while (n > 0) { dy /= 10; n--; }
		while (n < 0) { dy *= 10; n++; }
	}

	// Range checking (don't let to zoom too much)
	int ok = 1;
	if (dx <= 0) ok = 0;
	if (dy <= 0) ok = 0;
	if (nx2 <= 0) ok = 0;
	if (ny2 <= 0) ok = 0;
	if (vpx0 >= vpx1) ok = 0;
	if (vpy0 >= vpy1) ok = 0;
	d = fabs(vpx0); if (fabs(vpx1) > d) d = fabs(vpx1); d *= 10;
	if (d + dx == d) ok = 0;
	d = fabs(vpy0); if (fabs(vpy1) > d) d = fabs(vpy1); d *= 10;
	if (d + dy == d) ok = 0;

	// Draw grid and data labels
	pDC->SetBkMode(TRANSPARENT);
	pDC->SelectObject(pLblFnt);

	if (ok) 
	{
		LC_FLABEL frmx, frmy;
		d = floor(vpx0/dx)*dx;
		while (d < vpx0) d+=dx;
		pDC->SelectObject(pLtGrid);

		while (d <=	 vpx1) 
		{
			if (fabs(d) < dx/2) d = 0;
	
			i = fround((d-vpx0)/(vpx1-vpx0)*(rPlot.Width()+1));
			z = fabs(fmod(d, dx*nx2)); if (z > dx*nx2/2) z=dx*nx2-z;
			if (z < dx/2) n2 = 1; else n2 = 0;
			if (n2)
				pDC->SelectObject(pDkGrid); 
			else
				pDC->SelectObject(pLtGrid);

			if(bGridVert)
			{
				Move2(pDC, rPlot.left+i, rPlot.top);
				Line2(pDC, rPlot.left+i, rPlot.bottom);
			}
	
			l = 5; if (n2) l = 10;

			pDC->SelectObject(pBlack);
			pDC->MoveTo(rPlot.left+(int)floor(i),   rPlot.bottom+3);
			pDC->LineTo(rPlot.left+(int)floor(i),   rPlot.bottom+3+l);

			pDC->SelectObject(pWhite);
			pDC->MoveTo(rPlot.left+(int)floor(i)+1, rPlot.bottom+3);
			pDC->LineTo(rPlot.left+(int)floor(i)+1, rPlot.bottom+3+l);
	
			if (n2) 
			{
				FormatLabel(d, dx*nx2, 8, &frmx);
				s = Num2Str(d, frmx);
//				s = Time2Str(d, false);
				ss = pDC->GetTextExtent(s);
				if(bInvertX == false)
				{
					pDC->TextOut(rPlot.left+(int)i+1-ss.cx/2, rPlot.bottom+2+l, s);
				}
				else
				{
					pDC->TextOut(rPlot.right-(int)i-1-ss.cx/2, rPlot.bottom+2+l, s);
				}
			}
		
			if (d + dx == d) break;	// Range checking (don't let to zoom too much)
			d += dx;
		}

		d = floor(vpy0/dy)*dy;
		while (d < vpy0) d+=dy;
		pDC->SelectObject(pLtGrid);
		while (d <= vpy1) 
		{
			if (fabs(d) < dy/2) d = 0;
	
			double v = (d-vpy0)/(vpy1-vpy0)*(rPlot.Height()+1);
			i = fround(v);
			z = fabs(fmod(d, dy*ny2)); if (z > dy*ny2/2) z=dy*ny2-z;
			if (z < dy/2) n2 = 1; else n2 = 0;
			if (n2)
				pDC->SelectObject(pDkGrid); 
			else
				pDC->SelectObject(pLtGrid);

			if(bGridHorz)
			{
				Move2(pDC, rPlot.left,  rPlot.bottom-i);
				Line2(pDC, rPlot.right, rPlot.bottom-i);
			}
	
			l = 5; if (n2) l = 10;
			
			pDC->SelectObject(pBlack);
			pDC->MoveTo(rPlot.left-2-l, rPlot.bottom-(int)ceil(i));
			pDC->LineTo(rPlot.left-2,   rPlot.bottom-(int)ceil(i));
			
			pDC->SelectObject(pWhite);
			pDC->MoveTo(rPlot.left-2-l, rPlot.bottom-(int)ceil(i)+1);
			pDC->LineTo(rPlot.left-2,   rPlot.bottom-(int)ceil(i)+1);
			
			if (n2) 
			{
				FormatLabel(d, dy*ny2, 8, &frmy);
				s = Num2Str(d, frmy);
				ss = pDC->GetTextExtent(s);
				pDC->TextOut(rPlot.left-4-l-ss.cx, rPlot.bottom-(int)i-ss.cy/2, s);
			}
	
			if (d + dy == d) break;	// Range checking (don't let to zoom too much)
			d += dy;
		}
	}

	if(bSolidVert)
	{
		if(usx > vpx0 && usx < vpx1)
		{
			int ix = InvX(fround((usx-vpx0)/(vpx1-vpx0)*(rPlot.Width()+1)));
			pDC->SelectObject(pBlack);
			Move2(pDC, ix, rPlot.top);
			Line2(pDC, ix, rPlot.bottom);
		}
	}

	if(bSolidHorz)
	{
		if(usy > vpy0 && usy < vpy1)
		{
			int iy = fround((usy-vpy0)/(vpy1-vpy0)*(rPlot.Height()+1));
			pDC->SelectObject(pBlack);
			Move2(pDC, rPlot.left,  rPlot.bottom-iy);
			Line2(pDC, rPlot.right, rPlot.bottom-iy);
		}
	}


	// Print axis names
	CString hname = _T(""), vname = _T("");
	hname = strAxisX;
	vname = strAxisY;

	ss = pDC->GetTextExtent(hname);
	pDC->TextOut(rPlot.right-ss.cx, rPlot.bottom+30,   hname);
	ss = pDC->GetTextExtent(vname);
	pDC->TextOut(rGrid.left+20,     rPlot.top-ss.cy-10, vname);

	// Delete paint tools
	pDC->SelectObject(oldpen);
	pDC->SelectObject(oldfont);
	delete pLtGrid;
	delete pDkGrid;
	delete pBlack;
	delete pWhite;
	delete pLblFnt;

	if (pdc == NULL) ReleaseDC(pDC);
}

void CLC::DrawPlot(CDC *pDC, int nHighlight)
{
	if (rPlot.IsRectEmpty()) return;
	if (!bData) return;

	CDC *pdc = pDC;
	if (pdc == NULL) pDC = GetDC();

	if(!nHighlight) EmptyLocatorList();

	LC_PLOTINFO *pinfo;
	POSITION pos = listPlotInfo.GetTailPosition();
	while(pos != NULL) 
	{
		if(pos != actplot) 
		{
			pinfo = (LC_PLOTINFO*)listPlotInfo.GetPrev(pos);
			DrawDataSet(pDC, pinfo, nHighlight);
		} 
		else 
			listPlotInfo.GetPrev(pos);
	}

	if(actplot != NULL) 
	{
		pinfo = (LC_PLOTINFO*)listPlotInfo.GetAt(actplot);
		DrawDataSet(pDC, pinfo, nHighlight);
	}

	DrawMasks(pDC, true);

	if(pdc == NULL) ReleaseDC(pDC);
}

void CLC::DrawUserLegends(CDC *pDC)
{
	CDC *pdc = pDC;
	if (pdc == NULL) pDC = GetDC();

	CLCUL *pLegend;
	POSITION pos = listLegends.GetTailPosition();
	while(pos != NULL) 
	{
		pLegend = (CLCUL*)listLegends.GetAt(pos);
		DrawUserLegend(pDC, pLegend);
		listLegends.GetPrev(pos);
	}

	if(pdc == NULL) ReleaseDC(pDC);
}

void CLC::DrawLocatorData(CDC *pDC, LC_CLICK_AREA* pArea)
{
	if (!bLocatorEnable) return;
	if (rLocator.IsRectEmpty()) return;

	CDC *pdc = pDC;
	if (pdc == NULL) pDC = GetDC();

	if (!bLocator || !bData || !pArea)
	{
		pDC->FillSolidRect(rLocator, clrBgOut);
	}
	else
	{
		CBrush *bgbrush, *oldbrush;
		CPen *pen, *oldpen;
		CFont *pLblFnt, *oldfont;

		bgbrush = new CBrush(clrBgIn);
		oldbrush = pDC->SelectObject(bgbrush);

		pen = new CPen(PS_SOLID, 1, RGB(32,32,32));
		oldpen = pDC->SelectObject(pen);

		pLblFnt = new CFont();
//		pLblFnt->CreatePointFont(100, _T("Arial"), pDC);
		pLblFnt->CreatePointFont(nGridFontSize, _T("Arial"), pDC);
		oldfont = pDC->SelectObject(pLblFnt);


		CSize size = pDC->GetTextExtent(_T("0"));
		int dx = size.cx;
		int dy = size.cy;

		pDC->RoundRect(rLocator, CPoint(10, 10));

		LC_PLOTINFO *pinfo = pArea->pPlotInfo;

		CPoint p1(rLocator.left+2*dx, rLocator.CenterPoint().y-2); 
		CPoint p2(p1.x+dx*6, p1.y+4); 
		DrawDataLegend(pDC, CRect(p1,p2), pinfo, pArea->idxPoint);

		int nOldBkMode = pDC->SetBkMode(TRANSPARENT);


		int len = rLegend.Width()-11*dx;
		CString strName = pinfo->strDescr;
		if (pDC->GetTextExtent(strName).cx > len) 
		{
			size = pDC->GetTextExtent(_T("..."));
			
			while (pDC->GetTextExtent(strName).cx > len-size.cx)
			{
				strName.Delete(strName.GetLength() - 1);
			}

			strName += _T("...");
		}
		

		CString str;
//		str += _T("<");
		str += strName;
		str += _T("   ");
//		str += _T("> ");

		CString strData;

//		strData.Format(_T("[%d] ( "), pArea->idxPoint);
//		str += strData;

		strData.Format(pinfo->strFmtX, pArea->x);
		str += strData;

//		str += Time2Str(pArea->x, true);

		str += _T(" , y=");
		strData.Format(pinfo->strFmtY, pArea->y);
		str += strData;

//		str += _T(" )");

		pDC->TextOut(rLocator.left+9*dx, (rLocator.CenterPoint()).y - dy/2, str);

		pDC->SelectObject(oldbrush);
		pDC->SelectObject(oldpen);
		pDC->SelectObject(oldfont);
		delete bgbrush;
		delete pen;
		delete pLblFnt;

		pDC->SetBkMode(nOldBkMode);
	}

	if (pdc == NULL) ReleaseDC(pDC);
}

void CLC::DrawLegend(CDC *pDC)
{
	if (!bLegend) return;
	if (!bData) return;
	if (rLegend.IsRectEmpty()) return;

	CDC *pdc = pDC;
	if (pdc == NULL) pDC = GetDC();

	CBrush *bgbrush, *oldbrush;
	CPen *pen, *oldpen;
	CFont *pLblFnt, *oldfont;
	bgbrush = new CBrush(RGB(255,255,255));
	pen = new CPen(PS_SOLID, 1, RGB(128,0,0));
	pLblFnt = new CFont();
	pLblFnt->CreatePointFont(nLegendFontSize, strLegendFont, pDC);
	oldbrush = pDC->SelectObject(bgbrush);
	oldpen = pDC->SelectObject(pen);
	oldfont = pDC->SelectObject(pLblFnt);
	pDC->SetBkMode(TRANSPARENT);

	CSize size = pDC->GetTextExtent(_T("0"));
	int cx = size.cx;
	int cy = size.cy;

	pDC->RoundRect(rLegend, CPoint(16, 16));
	int deslen = rLegend.Width() - 9*cx;

	POSITION pos = bLegendOrder ? listPlotInfo.GetHeadPosition() : listPlotInfo.GetTailPosition();

	size_t count = GetActivePlotsCount();

	int j = rLegend.top + cy/2;	// y0 
	int dj = cy+cy/3;				// y step = 1 1/3 line

	if (strLegendText.GetLength() > 0) 
	{
		pDC->TextOut(rLegend.left + cx*7, j, strLegendText);
		j += dj;
	}

	int dots = 0;
	if (dj*count > (rLegend.bottom - j)) dots = cy;

	while (pos != NULL) 
	{
		LC_PLOTINFO *pinfo = (LC_PLOTINFO*)(bLegendOrder ? listPlotInfo.GetNext(pos) : listPlotInfo.GetPrev(pos));
		if(pinfo->nFlags & LCF_GRAPH_HIDDEN) continue;

		if ((rLegend.bottom - dots - j) >= dj) 
		{
/*
			if (pos == actplot)
			{
				pDC->TextOut(rLegend.right - 2*cx, j, _T("*"));
			}
*/
			CPoint p1(rLegend.left + 2*cx, j + cy*7/15); 
			CPoint p2(p1.x + cx*6, p1.y + cy/5); 
			DrawDataLegend(pDC, CRect(p1,p2), pinfo, -1);

			CString str = pinfo->strDescr;
			if (pDC->GetTextExtent(str).cx > deslen) 
			{
				size = pDC->GetTextExtent(_T("..."));

				while (pDC->GetTextExtent(str).cx > deslen - size.cx)
				{
					str.Delete(str.GetLength() - 1);
				}

				str += _T("...");
			}

			pDC->TextOut(rLegend.left + 9*cx, j, str);
			j += dj;
		} 
		else 
			break;
	}

	if (dots)
	{
		pDC->TextOut(rLegend.left + 9*cx, j, _T("[...]"));
	}

	pDC->SelectObject(oldbrush);
	pDC->SelectObject(oldpen);
	pDC->SelectObject(oldfont);
	delete bgbrush;
	delete pen;
	delete pLblFnt;

	if (pdc == NULL) ReleaseDC(pDC);
}

void CLC::DrawUserLegend(CDC *pDC, CLCUL *pLegend)
{
	if(!pLegend) return;
	if(pLegend->nFlags & LCUL_HIDDEN) return;

	CBrush *bgbrush, *oldbrush;
	CPen *pen, *oldpen;
	CFont *font, *oldfont;
	bgbrush = new CBrush(pLegend->clrFill);
	pen = new CPen(PS_SOLID, 1, pLegend->clrBorder);
	font = new CFont();
	font->CreatePointFont(pLegend->nFontSize*10, pLegend->strFontName, pDC);
	oldbrush = pDC->SelectObject(bgbrush);
	oldpen = pDC->SelectObject(pen);
	oldfont = pDC->SelectObject(font);
	pDC->SetBkMode(TRANSPARENT);
	COLORREF oldtextcolor = pDC->GetTextColor();
	pDC->SetTextColor(pLegend->clrText);


	CRect rDraw(pLegend->ptOrg, pLegend->szOrg);
	rDraw.left += rPlot.left;
	rDraw.right += rPlot.left;
	rDraw.top += rPlot.top;
	rDraw.bottom += rPlot.top;

	CSize sizeAuto = pLegend->EstimateSize(pDC);
	if(pLegend->nFlags & LCUL_AUTO_WIDTH) rDraw.right = rDraw.left + sizeAuto.cx;
	if(pLegend->nFlags & LCUL_AUTO_HEIGHT) rDraw.bottom = rDraw.top + sizeAuto.cy;

	rDraw = pLegend->FitToRect(rDraw, rPlot);

	pDC->RoundRect(rDraw, CPoint(16, 16));

	int nxt0 = pLegend->nStepX/2;
	int nxg0 = pLegend->nStepX/2;
	if(pLegend->nFlags & LCUL_LINE)
	{
		nxt0 += pLegend->nStepX*(5+1);
	}
	else if(pLegend->nFlags & LCUL_MARKER)
	{
		nxt0 += pLegend->nStepX*(3+1);
	}
	int nxtl = rDraw.Width() - nxt0 - pLegend->nStepX/2;

	int nyt = pLegend->nStepY/4;

	int n = 0;
	for(int i=0; i<pLegend->nStrings; i++)
	{
		if(pLegend->strings[i].nFlags & LCUL_HIDDEN) continue;

		CString str = pLegend->strings[i].strText;
		if(pDC->GetTextExtent(str).cx > nxtl) 
		{
			CSize sz = pDC->GetTextExtent(_T("..."));
			while (pDC->GetTextExtent(str).cx > (nxtl - sz.cx))
			{
				str.Delete(str.GetLength()-1);
			}
			str += _T("...");
		}

		if((nyt >= (rDraw.Height() - pLegend->nStepY*2))&& (n != pLegend->nLines-1))
		{
			pDC->TextOut(rDraw.left+nxt0, rDraw.top+nyt, _T("..."));
			break;
		}
		else
			pDC->TextOut(rDraw.left+nxt0, rDraw.top+nyt, str);

		if(pLegend->nFlags & (LCUL_LINE|LCUL_MARKER))
		{
			CRect rect = CRect(CPoint(rDraw.left + nxg0, rDraw.top + nyt), CSize(nxt0-nxg0, pLegend->nStepY));
			rect.left += pLegend->nStepX;
			rect.right -= pLegend->nStepX;
			DrawDataLegend(pDC, rect, &(pLegend->strings[i]));
		}

		n++;
		nyt += pLegend->nStepY;
	}


	pDC->SetTextColor(oldtextcolor);
	pDC->SelectObject(oldbrush);
	pDC->SelectObject(oldpen);
	pDC->SelectObject(oldfont);
	delete bgbrush;
	delete pen;
	delete font;
}


void CLC::DrawPIF(CDC *pDC)
{
	if (!bPifEnable) return;
	if (!bPif) return;
	if (!bData) return;
	if (rPoint.IsRectEmpty()) return;

	CDC *pdc = pDC;
	if (pdc == NULL) pDC = GetDC();

	CBrush *bgbrush, *oldbrush;
	CPen *pen, *oldpen;
	CFont *pLblFnt, *oldfont;
	bgbrush = new CBrush(RGB(255, 255, 255));
	pen = new CPen(PS_SOLID, 1, clrBgOut);
	pLblFnt = new CFont();
	pLblFnt->CreatePointFont(100, _T("Arial"), pDC);
	oldbrush = pDC->SelectObject(bgbrush);
	oldpen = pDC->SelectObject(pen);
	oldfont = pDC->SelectObject(pLblFnt);
	pDC->SetBkMode(TRANSPARENT);

	pDC->RoundRect(rPoint, CPoint(10, 10));
	CString s;
	s = Num2Str4PIF(pifX, 9);
//	s = Time2Str4PIF(pifX, false);
	s += ",";
	pDC->TextOut(rPoint.left + 5, rPoint.top + 2, s);
	s = Num2Str4PIF(pifY, 9);
	pDC->TextOut((rPoint.left+rPoint.right)/2, rPoint.top + 2, s);
	pDC->SelectObject(oldbrush);
	pDC->SelectObject(oldpen);
	pDC->SelectObject(oldfont);
	delete bgbrush;
	delete pen;
	delete pLblFnt;

	if (pdc == NULL) ReleaseDC(pDC);
}

int CLC::InvX(int ix)
{
	return bInvertX ? (rPlot.right-ix) : (rPlot.left+ix);
}

void CLC::DrawDataLegend(CDC *pDC, CRect rect, LC_PLOTINFO *pinfo, int nIdx)
{
	if(pinfo->nFlags & LCF_GRAPH_HIDDEN) return;

	//	line drawing
	bool bHisto = (pinfo->pDataSet->GetInterpolation() == LC_IT_HISTO);

	if(pinfo->pDataSet->GetInterpolation() != LC_IT_NONE)
	{
		CPen *pen, *oldpen;
		pen = new CPen(pinfo->line_style, pinfo->line_width, pinfo->line_color);
		oldpen = pDC->SelectObject(pen);

		if(!bHisto)
		{
			CPoint p[2];
			p[0] = CPoint(rect.left, rect.CenterPoint().y);
			p[1] = CPoint(rect.right, rect.CenterPoint().y);
			DrawPath(pDC, p, 2, 0);
		}
		else	//	histogram
		{
			CLumChartDataMarker marker;
			if(pinfo->nFlags & LCF_GRAPH_MUSER && pinfo->pUserMarkers != 0 && nIdx >= 0)
			{
				marker = pinfo->pUserMarkers[nIdx];
			}
			else
			{
				marker = pinfo->marker;
			}

			CBrush *brush, *oldbrush;
			brush = new CBrush(marker.nColor);
			oldbrush = pDC->SelectObject(brush);

			CPoint p[4];
			p[0] = CPoint(rect.left, rect.top-1);
			p[1] = CPoint(rect.right, rect.top-1);
			p[2] = CPoint(rect.right, rect.bottom+1);
			p[3] = CPoint(rect.left, rect.bottom+1);
			pDC->Polygon(p, 4);
			delete pDC->SelectObject(oldbrush);
		}

		delete pDC->SelectObject(oldpen);
	}

// Show markers
	if(!bHisto && (pinfo->marker.nType != LCF_MARKERS_NONE))
	{
		CLumChartDataMarker marker;
		if(pinfo->nFlags & LCF_GRAPH_MUSER && pinfo->pUserMarkers != 0 && nIdx >= 0)
		{
			marker = pinfo->pUserMarkers[nIdx];
		}
		else
		{
			marker = pinfo->marker;
		}

		CBrush *brush, *oldbrush;
		brush = new CBrush(marker.nColor);
		oldbrush = pDC->SelectObject(brush);

		COLORREF mclr = RGB(GetRValue(pinfo->line_color)/2, 
			                GetGValue(pinfo->line_color)/2, 
							GetBValue(pinfo->line_color)/2);
		CPen *pen, *oldpen;
		pen = new CPen(PS_SOLID, 1, mclr);
		oldpen = pDC->SelectObject(pen);

		int ix = rect.CenterPoint().x;
		int iy = rect.CenterPoint().y;
		int ix1 = ix - marker.nSize;
		int ix2 = ix + marker.nSize;
		int iy1 = iy - marker.nSize;
		int iy2 = iy + marker.nSize;
		switch(marker.nType & 0x0F)
		{
		case LCF_MARKERS_TRIUP:
			{
				CPoint points[3];
				points[0] = CPoint(ix, iy1-1);
				points[1] = CPoint(ix2, iy2-1);
				points[2] = CPoint(ix1, iy2-1);
				pDC->Polygon(points, 3);
				break;
			}
		case LCF_MARKERS_TRIDN:
			{
				CPoint points[3];
				points[0] = CPoint(ix, iy2);
				points[1] = CPoint(ix2, iy1);
				points[2] = CPoint(ix1, iy1);
				pDC->Polygon(points, 3);

				break;
			}

		case LCF_MARKERS_DIAMOND:
			{
				CPoint points[4];
				points[0] = CPoint(ix, iy1);
				points[1] = CPoint(ix2, iy);
				points[2] = CPoint(ix, iy2);
				points[3] = CPoint(ix1, iy);
				pDC->Polygon(points, 4);
				break;
			}

		case LCF_MARKERS_SQUARE:
			pDC->Rectangle(ix1, iy1, ix2, iy2);
			break;

		case LCF_MARKERS_ROUND:
			pDC->Ellipse(ix1, iy1, ix2, iy2);
			break;
			default:
			break;
		}
		pDC->SelectObject(oldbrush);
		pDC->SelectObject(oldpen);

		delete brush;
		delete pen;
	}
}

void CLC::DrawDataLegend(CDC *pDC, CRect rect, CLCULString* pData)
{
	if(pData->nFlags & LCUL_LINE)
	{

		CPen *pen, *oldpen;
		pen = new CPen(pData->nLineStyle, pData->nLineWidth, pData->clrLineColor);
		oldpen = pDC->SelectObject(pen);
		CPoint p[2];
		p[0] = CPoint(rect.left, rect.CenterPoint().y);
		p[1] = CPoint(rect.right, rect.CenterPoint().y);
		DrawPath(pDC, p, 2, 0);
		delete pDC->SelectObject(oldpen);

	}

// Show markers
	CLumChartDataMarker marker = pData->marker;
	if(pData->nFlags & LCUL_MARKER && marker.nType != LCF_MARKERS_NONE)
	{
		CBrush *brush, *oldbrush;
		brush = new CBrush(marker.nColor);
		oldbrush = pDC->SelectObject(brush);

		CPen *pen, *oldpen;
		COLORREF mclr = RGB(GetRValue(marker.nColor)/2, 
			                GetGValue(marker.nColor)/2, 
							GetBValue(marker.nColor)/2);
		pen = new CPen(PS_SOLID, 1, mclr);
		oldpen = pDC->SelectObject(pen);

		int ix = rect.CenterPoint().x;
		int iy = rect.CenterPoint().y;
		int ix1 = ix - marker.nSize;
		int ix2 = ix + marker.nSize;
		int iy1 = iy - marker.nSize;
		int iy2 = iy + marker.nSize;
		switch(marker.nType & 0x0F)
		{
		case LCF_MARKERS_TRIUP:
			{
				CPoint points[3];
				points[0] = CPoint(ix, iy1-1);
				points[1] = CPoint(ix2, iy2-1);
				points[2] = CPoint(ix1, iy2-1);
				pDC->Polygon(points, 3);
				break;
			}
		case LCF_MARKERS_TRIDN:
			{
				CPoint points[3];
				points[0] = CPoint(ix, iy2);
				points[1] = CPoint(ix2, iy1);
				points[2] = CPoint(ix1, iy1);
				pDC->Polygon(points, 3);

				break;
			}

		case LCF_MARKERS_DIAMOND:
			{
				CPoint points[4];
				points[0] = CPoint(ix, iy1);
				points[1] = CPoint(ix2, iy);
				points[2] = CPoint(ix, iy2);
				points[3] = CPoint(ix1, iy);
				pDC->Polygon(points, 4);
				break;
			}

		case LCF_MARKERS_SQUARE:
			pDC->Rectangle(ix1, iy1, ix2, iy2);
			break;

		case LCF_MARKERS_ROUND:
			pDC->Ellipse(ix1, iy1, ix2, iy2);
			break;
			default:
			break;
		}
		pDC->SelectObject(oldbrush);
		pDC->SelectObject(oldpen);

		delete brush;
		delete pen;
	}
}

void CLC::DrawDataSet(CDC *pDC, LC_PLOTINFO *pinfo, int nHighlight)
{
	if(pinfo->nFlags & LCF_GRAPH_HIDDEN) return;

//	data preparation

	CLCDataSet *pDataSet;
	CPen *pen, *oldpen;
	double x, y;

	pDataSet = pinfo->pDataSet;
	
	double x0 = vpx0;
	double x1 = vpx1;

	if (x0 < pDataSet->GetIMIN()) x0 = pDataSet->GetIMIN();
	if (x1 > pDataSet->GetIMAX()) x1 = pDataSet->GetIMAX();

	if (x0 > x1) return;

	int i0 = (int)floor(((rPlot.Width()+1)*(x0-vpx0)/(vpx1-vpx0)));
	int i1 = (int)ceil(((rPlot.Width()+1)*(x1-vpx0)/(vpx1-vpx0)));

	long n0, n1, n;
	bool bMinMax = 0;
	bool bMarkers = 0;

	if ((pDataSet->GetDataType() == LC_TYPE_FIXED) || (pDataSet->GetDataType() == LC_TYPE_XDATA)) 
	{
		pDataSet->GetPointsRange(x0, x1, &n0, &n1);
		n0--;
		n1++;
		if (n0 < 0) n0 = 0;
		if (n1 > pDataSet->GetPointCount()-1) n1 = pDataSet->GetPointCount()-1;
		if (n0 > n1) return;

		bMarkers = (nCaps & LC_CAPS_MARKERS) && (pinfo->marker.nType != LCF_MARKERS_NONE);
		bMarkers &= (!bMarkersAuto || ((n1-n0) <= (i1-i0+1)/pinfo->marker.nSize/2));

		if (pDataSet->GetInterpolation() == LC_IT_STEPH 
			|| pDataSet->GetInterpolation() == LC_IT_STEPV
			|| pDataSet->GetInterpolation() == LC_IT_HISTO)
		{
			bMarkers = 0;
		}

		bMinMax  =  0;//(nCaps & LC_CAPS_MINMAX)  && (n1-n0 > (i1-i0+1)*2);

	}

//	line drawing

	if (bMinMax && (pDataSet->GetInterpolation() != LC_IT_NONE) && (pDataSet->GetInterpolation() != LC_IT_HISTO)) 
	{
		pen = new CPen(PS_SOLID, 1, pinfo->line_color);
		oldpen = pDC->SelectObject(pen);

		n = n0;
		pDataSet->GetPoint(n, &x, &y);
		for (int i = i0; i < i1; i += 1) 
		{
			double tx0 = vpx0 + (vpx1-vpx0)*i/(rPlot.Width()+1);
			double tx1 = vpx0 + (vpx1-vpx0)*(i+1)/(rPlot.Width()+1);
			double ty0, ty1;
			while (x < tx0) 
			{
				n++;
				if (n > n1) break;
				pDataSet->GetPoint(n, &x, &y);
			}
			if (n > n1) break;

			ty0 = y; ty1 = y;
			while (x <= tx1) 
			{
				n++;
				if (n > n1) break;
				pDataSet->GetPoint(n, &x, &y);
				if (y < ty0) ty0 = y;
				if (y > ty1) ty1 = y;
			}

			ty0 = (rPlot.Height()+1)*(ty0-vpy0)/(vpy1-vpy0);
			ty1 = (rPlot.Height()+1)*(ty1-vpy0)/(vpy1-vpy0);

			int ix = InvX(i);
			Move2(pDC, ix, rPlot.bottom-ty0);
			Line2(pDC, ix, rPlot.bottom-ty1);
		}
	} 
	else
	{

		//	Line style 
		COLORREF color = pinfo->line_color;
		if(nHighlight && ((hlMode.nFlags & LC_HL_METHOD) != LC_HL_NONE) 
			          && ((hlMode.nFlags & LC_HL_SELECTION) != LC_HL_RECT))
		{
			color = hlMode.color;
		}

		switch (pDataSet->GetInterpolation()) 
		{
		case LC_IT_NONE:
		case LC_IT_DIRECT:
		case LC_IT_STEPH:
		case LC_IT_STEPV:
			pen = new CPen(pinfo->line_style, pinfo->line_width, color);
			break;
		case LC_IT_HISTO:
			pen = new CPen(PS_SOLID, 1, pinfo->line_color);
			break;
		default:
			pen = new CPen(pinfo->line_style, pinfo->line_width, color);
			break;
		}
		oldpen = pDC->SelectObject(pen);

		//	Drawing and setting sensitive area 
		switch (pDataSet->GetInterpolation()) 
		{
		case LC_IT_NONE:
			break;

		case LC_IT_DIRECT:
			{
				CPoint *p = new CPoint[n1-n0+1];
				for (n=n0; n<=n1; n++) 
				{
					pDataSet->GetPoint(n, &x, &y);
					int ix = InvX((rPlot.Width()+1)*(x-vpx0)/(vpx1-vpx0));
					int iy = (rPlot.Height()+1)*(y-vpy0)/(vpy1-vpy0);
					p[n-n0].x = ix;
					p[n-n0].y = rPlot.bottom - iy;

					if(!nHighlight && !bMarkers)
					{
						CRect rect(	p[n-n0].x - nLocatorSpan, 
									p[n-n0].y - nLocatorSpan, 
									p[n-n0].x + nLocatorSpan, 
									p[n-n0].y + nLocatorSpan);

						LC_CLICK_AREA *pArea = new LC_CLICK_AREA;
						pArea->rectArea = rect;
						pArea->x = x;
						pArea->y = y;
						pArea->idxPoint = pDataSet->GetOrgIdx(n);
						pArea->pPlotInfo = pinfo;
						AddLocatorArea(pArea);
					}
				}
				DrawPath(pDC, p, n1-n0+1);
				delete [] p;
				break;
			}

		case LC_IT_HISTO:
			{
				CBrush *brush, *oldbrush;
				brush = new CBrush(pinfo->marker.nColor);
				oldbrush = pDC->SelectObject(brush);

				CFont *pLblFnt = new CFont();
				pLblFnt->CreatePointFont(80, _T("Arial"), pDC);
				CFont *pOldFnt = pDC->SelectObject(pLblFnt);
				COLORREF clrOld = pDC->GetTextColor();
//				pDC->SetTextColor(clrBgIn);
				pDC->SetTextColor(pinfo->line_color);
				UINT oldTA = pDC->GetTextAlign();
				pDC->SetTextAlign(TA_CENTER|TA_BASELINE);
				pDC->SetBkMode(TRANSPARENT);

				for (n=n0; n<n1; n++) 
				{
					int iy0 = (rPlot.Height()+1)*(0-vpy0)/(vpy1-vpy0)+1;
					if(iy0 < 0) iy0 = 0;
					if(iy0 > rPlot.Height()) iy0 = rPlot.Height();
					
					double x1, y1;
					pDataSet->GetPoint(n, &x1, &y1);
					int ix1 = InvX((rPlot.Width()+1)*(x1-vpx0)/(vpx1-vpx0));
					int iy1 = (rPlot.Height()+1)*(y1-vpy0)/(vpy1-vpy0);

					double x2, y2;
					pDataSet->GetPoint(n+1, &x2, &y2);
					int ix2 = InvX((rPlot.Width()+1)*(x2-vpx0)/(vpx1-vpx0));

					CPoint points[4];
					points[0] = CPoint(ix1, rPlot.bottom-iy0);
					ForcePointToRect(points[0], rPlot);
					points[1] = CPoint(ix1, rPlot.bottom-iy1);
					ForcePointToRect(points[1], rPlot);
					points[2] = CPoint(ix2, rPlot.bottom-iy1);
					ForcePointToRect(points[2], rPlot);
					points[3] = CPoint(ix2, rPlot.bottom-iy0);
					ForcePointToRect(points[3], rPlot);

					CRect rect(	points[0], points[2]);
					rect.NormalizeRect();

					int nDoHighlight = 0;
					if(!nHighlight)
					{
						LC_CLICK_AREA *pArea = new LC_CLICK_AREA;
						pArea->rectArea = rect;
						pArea->x = x1;
						pArea->y = y1;
						pArea->idxPoint = pDataSet->GetOrgIdx(n);
						pArea->pPlotInfo = pinfo;
						AddLocatorArea(pArea);
					}
					else
					{
						switch(hlMode.nFlags & LC_HL_SELECTION)
						{
						case LC_HL_POINT:
							{
								if(rect.PtInRect(hlMode.point)) nDoHighlight = 1;
								break;
							}
						case LC_HL_RECT:
							{
								CRect trect;
								if(trect.IntersectRect(&rect, &(hlMode.rect))) nDoHighlight = 1;
								break;
							}
						case LC_HL_GRAPH:
							{
								nDoHighlight = 1;
								break;
							}
						default:
							{
								nDoHighlight = 0;
								break;
							}
						}

					}

					CBrush *tbrush = 0;				
					if(nDoHighlight && ((hlMode.nFlags & LC_HL_METHOD) != LC_HL_NONE)) 
					{
						tbrush = brush;
						brush = new CBrush(hlMode.color);
						pDC->SelectObject(brush);
					}
					else if(pinfo->nFlags & LCF_GRAPH_MUSER && pinfo->pUserMarkers != 0)
					{
						tbrush = brush;
						int iOrg = pDataSet->GetOrgIdx(n);
						brush = new CBrush(pinfo->pUserMarkers[iOrg].nColor);
						pDC->SelectObject(brush);
					}

					pDC->Polygon(points, 4);


					if(pinfo->nFlags & LCF_GRAPH_TEXT)
					{
						CString s;
						s.Format(pinfo->strFmtY, y1);
						pDC->TextOut(rect.CenterPoint().x, rect.CenterPoint().y, s);
					}

					if(tbrush)
					{
						delete brush;
						brush = tbrush;
						pDC->SelectObject(brush);
					}
					
				}
				delete pDC->SelectObject(oldbrush);
				delete pDC->SelectObject(pOldFnt);
				pDC->SetTextColor(clrOld);
				pDC->SetTextAlign(oldTA);
				break;
			}

		case LC_IT_STEPH:
		case LC_IT_STEPV:
			{
				CPoint *p = new CPoint[2*(n1-n0)+1];
				int i = 0;
				for (n=n0; n<=n1; n++) 
				{
					pDataSet->GetPoint(n, &x, &y);
					int ix = InvX((rPlot.Width()+1)*(x-vpx0)/(vpx1-vpx0));
					int iy = (rPlot.Height()+1)*(y-vpy0)/(vpy1-vpy0);

					if(n == n0) 
					{
						p[i++] = CPoint(ix, rPlot.bottom - iy);
					} 
					else 
					{
						if(!nHighlight)
						{
							CRect rect(	p[i-1].x - nLocatorSpan, 
										p[i-1].y - nLocatorSpan, 
										p[i-1].x + nLocatorSpan, 
										p[i-1].y + nLocatorSpan);

							LC_CLICK_AREA *pArea = new LC_CLICK_AREA;
							pArea->rectArea = rect;
							pArea->x = x;
							pArea->y = y;
							pArea->idxPoint = pDataSet->GetOrgIdx(n);
							pArea->pPlotInfo = pinfo;
							AddLocatorArea(pArea);
						}

						if(pDataSet->GetInterpolation() == LC_IT_STEPH) 
						{
							//	step horz
							p[i] = CPoint(ix, p[i-1].y);
							i++;
						}
						else	
						{
							//	step vert
							p[i] = CPoint(p[i-1].x, rPlot.bottom - iy);
							i++;
						}

						if(!nHighlight)
						{
							CRect rect(	p[i-1].x - nLocatorSpan, 
										p[i-1].y - nLocatorSpan, 
										p[i-1].x + nLocatorSpan, 
										p[i-1].y + nLocatorSpan);

							LC_CLICK_AREA *pArea = new LC_CLICK_AREA;
							pArea->rectArea = rect;
							pArea->x = x;
							pArea->y = y;
							pArea->idxPoint = pDataSet->GetOrgIdx(n);
							pArea->pPlotInfo = pinfo;
							AddLocatorArea(pArea);
						}
						p[i++] = CPoint(ix, rPlot.bottom - iy);
					}
				}
				DrawPath(pDC, p, 2*(n1-n0)+1);
				delete [] p;
			}
			break;

		default: // splines & callbacks
			{
				int iymin, iymax;
//				if(!nHighlight && !bMarkers)
				if(!nHighlight)
				{
					for (int i = i0; i <= i1; i += 1) 
					{
						x = vpx0 + (vpx1-vpx0)*i/(rPlot.Width()+1);
						if (x < pDataSet->GetIMIN()) x = pDataSet->GetIMIN();
						if (x > pDataSet->GetIMAX()) x = pDataSet->GetIMAX();
						y = pDataSet->f(x);

						int ix = InvX(i);
						int iy = (rPlot.Height()+1)*(y-vpy0)/(vpy1-vpy0);

						if((i - i0)%nLocatorSpan == 0)
						{
							if(i != i0)
							{
								iymin = min(iy, iymin);
								iymax = max(iy, iymax);

								LC_CLICK_AREA *pArea = new LC_CLICK_AREA;
								CRect rect(	ix - nLocatorSpan*3/2, 
											rPlot.bottom - (iymin-nLocatorSpan), 
											ix + nLocatorSpan/2, 
											rPlot.bottom - (iymax+nLocatorSpan));
//								pDC->Rectangle(rect);
								pArea->rectArea = rect;
								pArea->x = x;
								pArea->y = y;
								pArea->idxPoint = -1;
								pArea->pPlotInfo = pinfo;
								AddLocatorArea(pArea);
							}
							iymin = iymax = iy;
						}
						else
						{
							iymin = min(iy, iymin);
							iymax = max(iy, iymax);
						}
					}
				}

				CPoint *p = new CPoint[i1-i0+1];
				for (int i = i0; i <= i1; i += 1) 
				{
					x = vpx0 + (vpx1-vpx0)*i/(rPlot.Width()+1);
					if (x < pDataSet->GetIMIN()) x = pDataSet->GetIMIN();
					if (x > pDataSet->GetIMAX()) x = pDataSet->GetIMAX();
					y = pDataSet->f(x);

					int ix = InvX(i);
					int iy = (rPlot.Height()+1)*(y-vpy0)/(vpy1-vpy0);

					if(i == i0)
					{
//						Move2(pDC, ix, rPlot.bottom - iy);
					}
					else
					{
//						Line2(pDC, ix, rPlot.bottom - iy);
					}
					p[i-i0].x = ix;
					p[i-i0].y = rPlot.bottom - iy;
				}

				DrawPath(pDC, p, i1-i0+1);
				delete [] p;
				break;
			}
		}
	}
	delete pDC->SelectObject(oldpen);

// Show markers
	if (bMarkers && (pinfo->marker.nType != LCF_MARKERS_NONE)) 
	{
		CBrush *brushN, *brushH, *oldbrush;
		brushN = new CBrush(pinfo->marker.nColor);
		brushH = new CBrush(hlMode.color);
		oldbrush = pDC->SelectObject(brushN);

		COLORREF mclr = RGB(GetRValue(pinfo->line_color)/2, 
			                GetGValue(pinfo->line_color)/2, 
							GetBValue(pinfo->line_color)/2);
		CPen *mpenN, *mpenH;
		mpenN = new CPen(PS_SOLID, 1, mclr);
		mpenH = new CPen(PS_SOLID, 1, hlMode.color);
		oldpen = pDC->SelectObject(mpenN);

		CLumChartDataMarker marker = pinfo->marker;

		bool *pbDp = 0;
		if(bMinMax)
		{
			int l = n1-n0+1;
			pbDp = new bool[l];

			int ipx = -1;
			int kmin, kmax, iymin, iymax;
			for(int k=0; k<l; k++) 
			{
				pbDp[k] = false;
				n = k + n0;

				pDataSet->GetPoint(n, &x, &y);
				if ((x < vpx0) || (x > vpx1)) continue;
				if ((y < vpy0) || (y > vpy1)) continue;

				int ix = InvX(floor((rPlot.Width()+1)*(x-vpx0)/(vpx1-vpx0)));
				int iy = rPlot.bottom - floor((rPlot.Height()+1)*(y-vpy0)/(vpy1-vpy0))+0;
				if(ipx == -1)
				{
					ipx = ix;
					kmin = kmax = k;
					iymin = iymax = iy;
				}
				else if((k == l-1) || (ix != ipx))
				{
					pbDp[kmin] = true;
					pbDp[kmax] = true;
					ipx = ix;
					kmin = kmax = k;
					iymin = iymax = iy;
				}
				else
				{
					if(iy > iymax)
					{
						iymax = iy;
						kmax = k;
					}
					if(iy < iymin)
					{
						iymin = iy;
						kmin = k;
					}
				}
			}


		}

		for (n=n0; n<=n1; n++) 
		{
			if(pbDp && !pbDp[n-n0]) continue;

			pDataSet->GetPoint(n, &x, &y);
			int iOrg = pDataSet->GetOrgIdx(n);

			if ((x < vpx0) || (x > vpx1)) continue;
			if ((y < vpy0) || (y > vpy1)) continue;

			CBrush *tbrush = 0;				
			if(pinfo->nFlags & LCF_GRAPH_MUSER && pinfo->pUserMarkers != 0)
			{
//				if(marker.nColor != pinfo->pUserMarkers[iOrg].nColor)
				{
					tbrush = brushN;
					brushN = new CBrush(pinfo->pUserMarkers[iOrg].nColor);
					pDC->SelectObject(brushN);
				}
				marker = pinfo->pUserMarkers[iOrg];
			}

			int ix = InvX(floor((rPlot.Width()+1)*(x-vpx0)/(vpx1-vpx0)));
			int iy = rPlot.bottom - floor((rPlot.Height()+1)*(y-vpy0)/(vpy1-vpy0))+0;

			int ix1 = ix - marker.nSize;
			int ix2 = ix + marker.nSize;
			int iy1 = iy - marker.nSize;
			int iy2 = iy + marker.nSize;

			int nDoHighlight = 0;
			if(!nHighlight)
			{
				LC_CLICK_AREA *pArea = new LC_CLICK_AREA;
				CRect rect(ix1, iy1, ix2, iy2);
				pArea->rectArea = rect;
				pArea->x = x;
				pArea->y = y;
				pArea->idxPoint = pDataSet->GetOrgIdx(n);
				pArea->pPlotInfo = pinfo;
				AddLocatorArea(pArea);
			}
			else
			{
				switch(hlMode.nFlags & LC_HL_SELECTION)
				{
				case LC_HL_POINT:
					{
						CRect rect(	ix - nLocatorSpan, iy - nLocatorSpan, 
									ix + nLocatorSpan, iy + nLocatorSpan);

						if(rect.PtInRect(hlMode.point)) nDoHighlight = 1;
						break;
					}
				case LC_HL_RECT:
					{
						if(hlMode.rect.PtInRect(CPoint(ix,iy))) nDoHighlight = 1;
						break;
					}
				case LC_HL_GRAPH:
					{
						nDoHighlight = 1;
						break;
					}
				default:
					{
						nDoHighlight = 0;
						break;
					}
				}

				if(nDoHighlight && ((hlMode.nFlags & LC_HL_METHOD) != LC_HL_NONE)) 
				{
					switch(hlMode.nFlags & LC_HL_METHOD)
					{
					case LC_HL_XOR:
						pDC->SetROP2(R2_XORPEN);
						break;

					case LC_HL_NOT:
						pDC->SetROP2(R2_NOT);
						break;

					case LC_HL_COLOR:
						pDC->SetROP2(R2_COPYPEN);
						pDC->SelectObject(brushH);
						break;

					default:
						pDC->SetROP2(R2_COPYPEN);
						break;
					}
				}
				else
				{
					pDC->SetROP2(R2_COPYPEN);
					pDC->SelectObject(brushN);
					pDC->SelectObject(mpenN);
				}
			}

			switch(marker.nType & 0x0F)
			{
			case LCF_MARKERS_TRIUP:
				{
					CPoint points[3];
					points[0] = CPoint(ix, iy1);
					points[1] = CPoint(ix2, iy2);
					points[2] = CPoint(ix1, iy2);
					pDC->Polygon(points, 3);
					break;
				}
			case LCF_MARKERS_TRIDN:
				{
					CPoint points[3];
					points[0] = CPoint(ix, iy2);
					points[1] = CPoint(ix2, iy1);
					points[2] = CPoint(ix1, iy1);
					pDC->Polygon(points, 3);

					break;
				}

			case LCF_MARKERS_DIAMOND:
				{
					CPoint points[4];
					points[0] = CPoint(ix, iy1);
					points[1] = CPoint(ix2, iy);
					points[2] = CPoint(ix, iy2);
					points[3] = CPoint(ix1, iy);
					pDC->Polygon(points, 4);
					break;
				}

			case LCF_MARKERS_SQUARE:
				pDC->Rectangle(ix1, iy1, ix2, iy2);
				break;

			case LCF_MARKERS_ROUND:
				pDC->Ellipse(ix1, iy1, ix2, iy2);
				break;
			default:
				break;
			}

			if(tbrush)
			{
				delete brushN;
				brushN = tbrush;
				pDC->SelectObject(brushN);
			}
		}

		if(pbDp) delete[] pbDp;

		pDC->SelectObject(oldbrush);
		pDC->SelectObject(oldpen);

		delete brushH;
		delete brushN;
		delete mpenH;
		delete mpenN;
	}

	pDC->SetROP2(R2_COPYPEN);
}


struct LC_PATHDATA
{
	CDC *pDC;
	COLORREF clr;
	int nWid; 
	UINT nPat;

	CRect rect;
	int nCnt;
};

void CALLBACK PathDDAProc(int iX, int iY, LPARAM lpData)
{
	LC_PATHDATA *pd = (LC_PATHDATA*)lpData;
	UINT nMask = (1<<(pd->nCnt%16))& pd->nPat;
	if(nMask)
	{
		if(pd->rect.IsRectNull() || pd->rect.PtInRect(CPoint(iX,iY))) pd->pDC->SetPixel(iX, iY, pd->clr);
		for(int i=1; i<pd->nWid; i++)
		{
			if(pd->rect.IsRectNull() || pd->rect.PtInRect(CPoint(iX+i,iY))) pd->pDC->SetPixel(iX+i, iY, pd->clr);
			if(pd->rect.IsRectNull() || pd->rect.PtInRect(CPoint(iX,iY+i))) pd->pDC->SetPixel(iX, iY+i, pd->clr);
			if(pd->rect.IsRectNull() || pd->rect.PtInRect(CPoint(iX-i,iY))) pd->pDC->SetPixel(iX-i, iY, pd->clr);
			if(pd->rect.IsRectNull() || pd->rect.PtInRect(CPoint(iX,iY-i))) pd->pDC->SetPixel(iX, iY-i, pd->clr);
		}
	}
	pd->nCnt++;
}

int CLC::DrawPath(CDC *pDC, CPoint *p, int np, int nClip)
{
	CPen *pen = pDC->GetCurrentPen();
	LOGPEN logpen;
	pen->GetLogPen(&logpen);

	LC_PATHDATA pathdata;
	pathdata.pDC = pDC;
	pathdata.clr = logpen.lopnColor;
	pathdata.nWid = logpen.lopnWidth.x;
	pathdata.rect = rPlot;
	pathdata.nCnt = 0;
	if(!nClip) 	pathdata.rect.SetRectEmpty();

	switch(logpen.lopnStyle)
	{
	case PS_NULL:
		pathdata.nPat = 0x0000;
		break;

	case PS_DASH:
		pathdata.nPat = 0x00FF;
		break;

	case PS_DOT:
		pathdata.nPat = 0x3C3C;
		break;

	case PS_DASHDOT:
		pathdata.nPat = 0x18FF;
		break;

	case PS_DASHDOTDOT:
		pathdata.nPat = 0x330F;
		break;

	case PS_SOLID:
	default:
		pathdata.nPat = 0xFFFF;
		break;
	}

	for(int i=1; i<np; i++)
	{
		LineDDA(p[i-1].x, p[i-1].y, p[i].x, p[i].y, PathDDAProc, (LPARAM)(&pathdata));
	}
	return pathdata.nCnt;	
}

int CLC::IsPointInSel(CPoint point)
{
	if (!sel.IsRectEmpty()) 
	{
		return sel.PtInRect(point);
	}
	else
		return -1;
}

void CLC::DrawSel(CDC *pDC)
{
	if (!sel.IsRectEmpty()) 
	{
		CPen *pen, *oldpen;
		pen = new CPen(PS_SOLID, 1, clrSelFrame);
		oldpen = pDC->SelectObject(pen);
		pDC->MoveTo(sel.left-1,  sel.top-1);
		pDC->LineTo(sel.right+1, sel.top-1);
		pDC->LineTo(sel.right+1, sel.bottom+1);
		pDC->LineTo(sel.left-1,  sel.bottom+1);
		pDC->LineTo(sel.left-1,  sel.top-1);
		pDC->SelectObject(oldpen);
		delete pen;


		int nw = nSelFrameWidth + 1;
		int nw2 = (nSelFrameWidth + 0)/2;

		pDC->SetROP2(R2_COPYPEN);
		pDC->FillRect(CRect(sel.left-nw,  sel.top-nw,    sel.right+nw, sel.top-1),    pSelFrameBrush);
		pDC->FillRect(CRect(sel.left-nw,  sel.top-nw,    sel.left-1,  sel.bottom+nw), pSelFrameBrush);
		pDC->FillRect(CRect(sel.left-nw,  sel.bottom+2, sel.right+nw, sel.bottom+nw+1), pSelFrameBrush);
		pDC->FillRect(CRect(sel.right+2, sel.top-nw,    sel.right+nw+1, sel.bottom+nw), pSelFrameBrush);

		pDC->FillSolidRect(sel.left-nw,  sel.top-nw,    nw, nw, clrSelFrame);
		pDC->FillSolidRect(sel.left-nw,  sel.bottom+1, nw, nw, clrSelFrame);
		pDC->FillSolidRect(sel.right+1, sel.top-nw,    nw, nw, clrSelFrame);
		pDC->FillSolidRect(sel.right+1, sel.bottom+1, nw, nw, clrSelFrame);

		pDC->FillSolidRect(sel.left-nw, sel.CenterPoint().y-nw2, nw, nw, clrSelFrame);
		pDC->FillSolidRect(sel.CenterPoint().x-nw2, sel.top-nw, nw, nw, clrSelFrame);
		pDC->FillSolidRect(sel.right+1, sel.CenterPoint().y-nw2, nw, nw, clrSelFrame);
		pDC->FillSolidRect(sel.CenterPoint().x-nw2, sel.bottom+1, nw, nw, clrSelFrame);
	}
}

void CLC::DrawCross(CDC *pDC, CPoint point)
{
	CDC *pdc = pDC;
	if (pdc == NULL) pDC = GetDC();

	CPen *pen, *oldpen;
	pen = new CPen(PS_SOLID, 1, RGB(0,0,0));
	oldpen = pDC->SelectObject(pen);
	pDC->SetROP2(R2_NOT);
	pDC->MoveTo(rPlot.left,  point.y);
	pDC->LineTo(rPlot.right, point.y);
	pDC->MoveTo(point.x, rPlot.top);
	pDC->LineTo(point.x, rPlot.bottom);

//	CRect rectLocator(point.x-10, point.y-10, point.x+10, point.y+10);
//	pDC->InvertRect(rectLocator);

	pDC->SetROP2(R2_COPYPEN);
	pDC->SelectObject(oldpen);
	delete pen;

	if (pdc == NULL) ReleaseDC(pDC);
}

void CLC::Move2(CDC *pDC, double x, double y)
{
	curX = x;
	curY = y;
}

void CLC::Line2(CDC *pDC, double x, double y)
{
	double x0, y0, x1, y1;
	x0 = curX;
	y0 = curY;
	x1 = x;
	y1 = y;

	curX = x;
	curY = y;

	if ((x0 <  rPlot.left)     && (x1 <  rPlot.left))     return;
	if ((x0 <  rPlot.left)     && (x1 >= rPlot.left))     { y0 += (y1-y0)*(rPlot.left-x0)/(x1-x0);   x0 = rPlot.left; }
	if ((x0 >= rPlot.left)     && (x1 <  rPlot.left))     { y1 += (y0-y1)*(rPlot.left-x1)/(x0-x1);   x1 = rPlot.left; }
	if ((y0 <  rPlot.top)      && (y1 <  rPlot.top))      return;
	if ((y0 <  rPlot.top)      && (y1 >= rPlot.top))      { x0 += (x1-x0)*(rPlot.top-y0)/(y1-y0);    y0 = rPlot.top; }
	if ((y0 >= rPlot.top)      && (y1 <  rPlot.top))      { x1 += (x0-x1)*(rPlot.top-y1)/(y0-y1);    y1 = rPlot.top; }
	if ((x0 >= rPlot.right+1)  && (x1 >= rPlot.right+1))  return;
	if ((x0 <  rPlot.right+1)  && (x1 >= rPlot.right+1))  { y1 -= (y1-y0)*(x1-rPlot.right-1)/(x1-x0);  x1 = rPlot.right; }
	if ((x0 >= rPlot.right+1)  && (x1 <  rPlot.right+1))  { y0 -= (y0-y1)*(x0-rPlot.right-1)/(x0-x1);  x0 = rPlot.right; }
	if ((y0 >= rPlot.bottom+1) && (y1 >= rPlot.bottom+1)) return;
	if ((y0 <  rPlot.bottom+1) && (y1 >= rPlot.bottom+1)) { x1 -= (x1-x0)*(y1-rPlot.bottom-1)/(y1-y0); y1 = rPlot.bottom; }
	if ((y0 >= rPlot.bottom+1) && (y1 <  rPlot.bottom+1)) { x0 -= (x0-x1)*(y0-rPlot.bottom-1)/(y0-y1); y0 = rPlot.bottom; }

	x0 = floor(x0);
	x1 = floor(x1);
	y0 = floor(y0);
	y1 = floor(y1);

	CPen *pen = pDC->GetCurrentPen();
	LOGPEN logpen;
	pen->GetLogPen(&logpen);
	if (((int)x0 == (int)x1) && ((int)y0 == (int)y1)) 
	{
		pDC->SetPixel((int)x0, (int)y0, logpen.lopnColor);
	} 
	else 
	{
		pDC->SetPixel((int)x1, (int)y1, logpen.lopnColor);
		pDC->MoveTo((int)x0, (int)y0);
		pDC->LineTo((int)x1, (int)y1);
	}
}

// Other functions

int CLC::SetUserViewPort(double x0, double y0, double x1, double y1)
{
	double z;
	// Range checking (don't let to zoom too much)
	if (x0 >= x1) return 0;
	if (y0 >= y1) return 0;
	z = fabs(x0); if (fabs(x1) > z) z = fabs(x1);
	if (z > (x1-x0)*1E10) return 0;
	z = fabs(y0); if (fabs(y1) > z) z = fabs(y1);
	if (z > (y1-y0)*1E10) return 0;

	upx0 = x0;
	upx1 = x1;
	upy0 = y0;
	upy1 = y1;
	nScaleMode |= LC_SCALE_USER;
	SetViewPort(upx0, upy0, upx1, upy1);
	return 1;
}

int CLC::SetViewPort(double x0, double y0, double x1, double y1)
{
	double z;
	// Range checking (don't let to zoom too much)
	if (x0 >= x1) return 0;
	if (y0 >= y1) return 0;
	z = fabs(x0); if (fabs(x1) > z) z = fabs(x1);
	if (z > (x1-x0)*1E10) return 0;
	z = fabs(y0); if (fabs(y1) > z) z = fabs(y1);
	if (z > (y1-y0)*1E10) return 0;

	vpx0 = x0;
	vpx1 = x1;
	vpy0 = y0;
	vpy1 = y1;

	VPORT *pv = new VPORT;
	pv->x0 = vpx0;
	pv->x1 = vpx1;
	pv->y0 = vpy0;
	pv->y1 = vpy1;
	POSITION pos;
	if (posZoom != NULL) {
		listZoom.GetNext(posZoom);
		while (posZoom != NULL) {
			pos = posZoom;
			delete listZoom.GetNext(posZoom);
			listZoom.RemoveAt(pos);
		}
	}
	listZoom.AddTail(pv);
	posZoom = listZoom.GetTailPosition();

	GetParent()->SendMessage(WM_GRAPH, GCN_VPORT_CHANGED);
	return 1;
}


void CLC::ZoomBack()
{
	if (!CanZoomBack()) return;
	if (posZoom == NULL) return;
	listZoom.GetPrev(posZoom);
	if (posZoom == NULL) return;
	VPORT *vp = (VPORT*)listZoom.GetAt(posZoom);
	vpx0 = vp->x0;
	vpx1 = vp->x1;
	vpy0 = vp->y0;
	vpy1 = vp->y1;
	GetParent()->SendMessage(WM_GRAPH, GCN_VPORT_CHANGED);
}

void CLC::ZoomForward()
{
	if (!CanZoomForward()) return;

	if (posZoom == NULL) return;

	listZoom.GetNext(posZoom);

	if (posZoom == NULL) return;

	VPORT *vp = (VPORT*)listZoom.GetAt(posZoom);
	vpx0 = vp->x0;
	vpx1 = vp->x1;
	vpy0 = vp->y0;
	vpy1 = vp->y1;
	GetParent()->SendMessage(WM_GRAPH, GCN_VPORT_CHANGED);
}

int CLC::CanZoomBack()
{
	POSITION pos = posZoom;
	if (pos == NULL) return 0;
	listZoom.GetPrev(pos);
	if (pos == NULL) return 0;
	return 1;
}

int CLC::CanZoomForward(void)
{
	POSITION pos = posZoom;
	if (pos == NULL) return 0;
	listZoom.GetNext(pos);
	if (pos == NULL) return 0;
	return 1;
}

void CLC::GetViewPort(double *x0, double *y0, double *x1, double *y1)
{
	if(x0) *x0 = vpx0;
	if(x1) *x1 = vpx1;
	if(y0) *y0 = vpy0;
	if(y1) *y1 = vpy1;
}

void CLC::GetViewPortXY(CPoint point, double *pX, double *pY)
{
	if(pX) *pX = vpx0 + (vpx1-vpx0)*(point.x-rPlot.left)/(rPlot.Width()+1);
	if(pY) *pY = vpy0 + (vpy1-vpy0)*(rPlot.bottom-point.y)/(rPlot.Height()+1);
}

void CLC::ShowLegend(void)
{
	bLegend = 1;
}

void CLC::HideLegend(void)
{
	bLegend = 0;
}

CRect CLC::RValid(CRect r)
{
	CRect rect;
	if (r.left < r.right) 
	{
		rect.left = r.left;
		rect.right = r.right;
	} 
	else 
	{
		rect.left = r.right;
		rect.right = r.left;
	}

	if (r.top < r.bottom) 
	{
		rect.top = r.top;
		rect.bottom = r.bottom;
	} 
	else 
	{
		rect.top = r.bottom;
		rect.bottom = r.top;
	}
	return rect;
}

int CLC::GetPos(CPoint p)
{
	if (sel.IsRectEmpty()) return MS_NONE;

	int nw = nSelFrameWidth+1;

	if (CRect(sel.left-nw,  sel.top-nw,    sel.left,    sel.top     ).PtInRect(p)) return MS_DTL;

	if (CRect(sel.left-nw,  sel.bottom+1, sel.left,    sel.bottom+6).PtInRect(p)) return MS_DBL;

	if (CRect(sel.right+1, sel.top-nw,    sel.right+nw+1, sel.top     ).PtInRect(p)) return MS_DTR;

	if (CRect(sel.right+1, sel.bottom+1, sel.right+nw+1, sel.bottom+6).PtInRect(p)) return MS_DBR;

	if (CRect(sel.left-nw, sel.CenterPoint().y-3, sel.left, sel.CenterPoint().y+3).PtInRect(p)) return MS_LEFT;

	if (CRect(sel.CenterPoint().x-3, sel.top-nw, sel.CenterPoint().x+3, sel.top).PtInRect(p)) return MS_TOP;

	if (CRect(sel.right+1, sel.CenterPoint().y-3, sel.right+nw+1, sel.CenterPoint().y+3).PtInRect(p)) return MS_RIGHT;
	if (CRect(sel.CenterPoint().x-3, sel.bottom+1, sel.CenterPoint().x+3, sel.bottom+nw+1).PtInRect(p)) return MS_BOTTOM;
	if (sel.PtInRect(p)) return MS_MOVE;

	return MS_NONE;
}

void CLC::SetActCur(int action)
{
	if (action == MS_SELRECT) 
	{
		action = MS_DBR;
		if ((dsel.left < pd.x) && (dsel.top < pd.y)) action = MS_DTL;
		if ((dsel.right > pd.x) && (dsel.bottom > pd.y)) action = MS_DBR;
		if ((dsel.left < pd.x) && (dsel.bottom > pd.y)) action = MS_DBL;
		if ((dsel.right > pd.x) && (dsel.top < pd.y)) action = MS_DTR;
	}
	switch (action) {
	case MS_LEFT:
	case MS_RIGHT:
		SetCursor(LoadCursor(NULL, IDC_SIZEWE));
		break;
	case MS_TOP:
	case MS_BOTTOM:
		SetCursor(LoadCursor(NULL, IDC_SIZENS));
		break;
	case MS_DTL:
	case MS_DBR:
		SetCursor(LoadCursor(NULL, IDC_SIZENWSE));
		break;
	case MS_DTR:
	case MS_DBL:
		SetCursor(LoadCursor(NULL, IDC_SIZENESW));
		break;
	case MS_MOVE:
		SetCursor(LoadCursor(NULL, IDC_SIZEALL));
		break;
	default:
		SetCursor(LoadCursor(NULL, IDC_ARROW));
		break;
	}
}

void CLC::SetPIF(double x, double y)
{
	bPif = 1;
	pifX = x;
	pifY = y;
}

void CLC::RemovePIF(void)
{
	bPif = 0;
}

void CLC::FormatLabel(double x, double dx, int dig, LC_FLABEL *frm)
{
	int n, k;
	int dd;

	// Clean frm data (in case of error)
	frm->hexp = 0;
	frm->ven = 0;
	frm->dpd = 0;

	if (x < 0) dig--;
	x = fabs(x);

	if (dx <= 0) return;
	if (x < dx/2) x = 0;
	if (x == 0) return;

	k = 0;
	while (x < 1)   { x *= 10; k++; }
	while (x >= 10) { x /= 10; k--; }
	n = 0;
	while (dx < 1)   { dx *= 10; n++; }
	while (dx >= 10) { dx /= 10; n--; }

	if (k > 0) {
		dd = 3;
		if (k >= 10) dd++;
		if (k >= 100) dd++;
		if ((n+2 > dig) && (k > dd)) {
			frm->hexp = 1;
			frm->ven = -k;
			k += frm->ven;
			n += frm->ven;
			dig -= dd;
		}
		frm->dpd = n;
		if (frm->dpd > dig-2) frm->dpd = dig-2;
		if (frm->dpd < 0) frm->dpd = 0;
	} else {
		if (-k+1 > dig) {
			frm->hexp = 1;
			frm->ven = -k;
			dd = 3;
			if (-k >= 10) dd++;
			if (-k >= 100) dd++;
			k += frm->ven;
			n += frm->ven;
			dig -= dd;
		}
		frm->dpd = n;
		if (frm->dpd > dig-1-(-k+1)) frm->dpd = dig-1-(-k+1);
		if (frm->dpd < 0) frm->dpd = 0;
	}
}

CString CLC::Num2Str(double x, LC_FLABEL frm)
{
	int k;
	CString s, sf;

	if (frm.hexp) 
	{
		k = frm.ven;
		while (k > 0) { x /= 10; k--; }
		while (k < 0) { x *= 10; k++; }
		sf.Format(_T("%%.%dfE%+02d"), frm.dpd, frm.ven);
		s.Format(sf, x);
	} 
	else 
	{
		sf.Format(_T("%%.%df"), frm.dpd);
		s.Format(sf, x);
	}

	return s;
}
/*
CString CLC::Time2Str(double x, bool bShowDate)
{
	__time64_t t64 = (__time64_t)x;
	CTime t(t64);
	CString str;
	if(bShowDate)
	{
		str.Format(_T("[ %4d-%02d-%02d   %02d:%02d:%02d ]"), t.GetYear(), t.GetMonth(), t.GetDay(), t.GetHour(), t.GetMinute(), t.GetSecond());
	}
	else
	{
		str.Format(_T("%02d:%02d:%02d"), t.GetHour(), t.GetMinute(), t.GetSecond());
	}
	return str;
}

CString CLC::Time2Str4PIF(double d, bool bShowDate)
{
	__time64_t t64 = (__time64_t)d;
	CTime t(t64);
	CString str;
	if(bShowDate)
	{
		str.Format(_T("[ %4d-%02d-%02d   %02d:%02d:%02d ]"), t.GetYear(), t.GetMonth(), t.GetDay(), t.GetHour(), t.GetMinute(), t.GetSecond());
	}
	else
	{
		str.Format(_T("%02d:%02d:%02d"), t.GetHour(), t.GetMinute(), t.GetSecond());
	}
	return str;
}
*/

CString CLC::Num2Str4PIF(double x, int dig)
{
	int hexp;   // Boolean: exponent present
	int ven;    // Exponent value
	int dpd;    // Digits after dot
	int hs;     // Sign present
	int n;
	int dd;

	if (x < 0) hs = 1; else hs = 0;
	x = fabs(x);
	
	if (x < 1E-99) {	// x = 0
		hexp = 0;
		x = 0;
		dpd = dig-2;
		hs = 0;
	} else
	if (x >= 1) 
	{
		n = 0;
		while (x >= 1) { x /= 10; n++; } // n - digits before dot
		if (n > dig-hs) {
			hexp = 1;
			ven = n-1;
			n = 1;
			dd = 3;
			if (ven >= 10) dd++;
			if (ven >= 100) dd++;
			dpd = dig-dd-hs-2;
		} else {
			hexp = 0;
			dpd = dig-hs-n-1;
			if (dpd < 0) dpd = 0;
		}
		while (n > 0) { x *= 10; n--; }
	} else
	if (x < 1) 
	{
		n = 0;
		while (x < 1) { x *= 10; n++; }
		dd = 3;
		if (n >= 10) dd++;
		if (n >= 100) dd++;
		if (n > dd) {
			hexp = 1;
			ven = -n;
			n = 0;
			dpd = dig-dd-hs-2;
		} else {
			hexp = 0;
			dpd = dig-hs-2;
			if (dpd < 0) dpd = 0;
		}
		while (n > 0) { x /= 10; n--; }
	}

	if (hs) x = -x;

	CString s, sf;
	if (hexp) 
	{
		sf.Format(_T("%%.%dfE%+02d"), dpd, ven);
		s.Format(sf, x);
	} 
	else 
	{
		sf.Format(_T("%%.%df"), dpd);
		s.Format(sf, x);
	}
	return s;
}

POSITION CLC::GetFirstPlotPos()
{
	return listPlotInfo.GetHeadPosition();
}

CLCDataSet *CLC::GetNextPlotData(POSITION &pos)
{
	if (pos == NULL) return NULL;
	LC_PLOTINFO *pinfo = (LC_PLOTINFO*)listPlotInfo.GetNext(pos);
	return pinfo->pDataSet;
}

CLCDataSet *CLC::GetPlotData(POSITION pos)
{
	if (pos == NULL) return NULL;
	LC_PLOTINFO *pinfo = (LC_PLOTINFO*)listPlotInfo.GetAt(pos);
	return pinfo->pDataSet;
}

POSITION CLC::FindPlot(CLCDataSet *pdset)
{
	POSITION pos, pos2;
	pos = listPlotInfo.GetHeadPosition();
	
	while (pos != NULL) 
	{
		pos2 = pos;
		if (((LC_PLOTINFO*)listPlotInfo.GetNext(pos))->pDataSet == pdset)
			return pos2;
	}
	
	return NULL;
}

POSITION CLC::AddPlot(CLCDataSet *pDataSet)
{
	LC_PLOTINFO *pinfo = new LC_PLOTINFO;
	pinfo->pDataSet = pDataSet;
	pinfo->pUserMarkers = 0;

	pinfo->line_width = 1;
	pinfo->line_style = PS_SOLID;
	pinfo->line_color = LC_PLOTCOLOR[listPlotInfo.GetCount()%LC_PLOTCLNUM];

	pinfo->marker.nType = LCF_MARKERS_SQUARE;
	pinfo->marker.nSize = 3;

	pinfo->nFlags = 0;
	pinfo->strDescr = _T("New plot");
	pinfo->strFmtX = _T("%f");
	pinfo->strFmtY = _T("%f");

	POSITION pos = listPlotInfo.AddHead((void*)pinfo);

#ifdef LC_ACTIVATE_NEW_PLOT
	if (actplot == NULL) actplot = pos;
#endif
	
	UpdateLPSize();
	bData = 1;
	EmptyZoomList();
	return pos;
}


void CLC::RemoveAllPlots()
{
	POSITION pos;
	pos = listPlotInfo.GetHeadPosition();
	
	while (pos != NULL) 
	{
		RemovePlot(pos);
		pos = listPlotInfo.GetHeadPosition();
	}
}

void CLC::SetPlotFlags(POSITION pos, UINT nFlags)
{
	if (pos == NULL) return;

	LC_PLOTINFO *pinfo = (LC_PLOTINFO*)listPlotInfo.GetAt(pos);
	pinfo->nFlags = nFlags;
}

UINT CLC::GetPlotFlags(POSITION pos)
{
	if (pos == NULL) return 0;

	LC_PLOTINFO *pinfo = (LC_PLOTINFO*)listPlotInfo.GetAt(pos);
	return pinfo->nFlags;
}

void CLC::RemovePlot(POSITION pos)
{
	if (pos == NULL) return;

	LC_PLOTINFO *pinfo = (LC_PLOTINFO*)(listPlotInfo.GetAt(pos));
	delete pinfo->pDataSet;
	listPlotInfo.RemoveAt(pos);
	delete pinfo;

	bData = (listPlotInfo.GetCount() != 0);
	
	if (actplot == pos) 
	{
		if (listPlotInfo.GetCount() != 0)
			actplot = listPlotInfo.GetHeadPosition(); 
		else
			actplot = NULL;
	}
	
	UpdateLPSize();
	EmptyZoomList();
}

void CLC::SetActivePlotPosition(POSITION pos)
{
	actplot = pos;
}

POSITION CLC::GetActivePlotPosition(void)
{
	return actplot;
}

void CLC::SetPlotLineColor(POSITION pos, COLORREF color)
{
	if (pos == NULL) return;
	LC_PLOTINFO *pinfo = (LC_PLOTINFO*)listPlotInfo.GetAt(pos);
	pinfo->line_color = color;
}

COLORREF CLC::GetPlotLineColor(POSITION pos)
{
	if (pos == NULL) return RGB(0,0,0);
	LC_PLOTINFO *pinfo = (LC_PLOTINFO*)listPlotInfo.GetAt(pos);
	return pinfo->line_color;
}

void CLC::SetPlotLineWidth(POSITION pos, int nWidth)
{
	if (pos == NULL) return;
	LC_PLOTINFO *pinfo = (LC_PLOTINFO*)listPlotInfo.GetAt(pos);
	pinfo->line_width = nWidth;
}

int CLC::GetPlotLineWidth(POSITION pos)
{
	if (pos == NULL) return RGB(0,0,0);
	LC_PLOTINFO *pinfo = (LC_PLOTINFO*)listPlotInfo.GetAt(pos);
	return pinfo->line_width;
}

void CLC::SetPlotLineStyle(POSITION pos, int nStyle)
{
	if (pos == NULL) return;
	LC_PLOTINFO *pinfo = (LC_PLOTINFO*)listPlotInfo.GetAt(pos);
	pinfo->line_style = nStyle;
}

int CLC::GetPlotLineStyle(POSITION pos)
{
	if (pos == NULL) return RGB(0,0,0);
	LC_PLOTINFO *pinfo = (LC_PLOTINFO*)listPlotInfo.GetAt(pos);
	return pinfo->line_style;
}

void CLC::SetPlotMarkers(POSITION pos, int nMarkerType, int nMarkerSize)
{
	if (pos == NULL) return;
	LC_PLOTINFO *pinfo = (LC_PLOTINFO*)listPlotInfo.GetAt(pos);
	pinfo->marker.nType = nMarkerType;
	pinfo->marker.nSize = nMarkerSize;
}

void CLC::SetPlotUserMarkers(POSITION pos, CLumChartDataMarker *pMarkers)
{
	if (pos == NULL) return;
	LC_PLOTINFO *pinfo = (LC_PLOTINFO*)listPlotInfo.GetAt(pos);
	pinfo->pUserMarkers = pMarkers;
}


void CLC::SetPlotMarkerSize(POSITION pos, int nMarkerSize)
{
	if (pos == NULL) return;
	LC_PLOTINFO *pinfo = (LC_PLOTINFO*)listPlotInfo.GetAt(pos);
	pinfo->marker.nSize = nMarkerSize;
}

int CLC::GetPlotMarkerSize(POSITION pos)
{
	if (pos == NULL) return 0;
	LC_PLOTINFO *pinfo = (LC_PLOTINFO*)listPlotInfo.GetAt(pos);
	return pinfo->marker.nSize;
}

void CLC::SetPlotMarkerType(POSITION pos, int nMarkerType)
{
	if (pos == NULL) return;
	LC_PLOTINFO *pinfo = (LC_PLOTINFO*)listPlotInfo.GetAt(pos);
	pinfo->marker.nType = nMarkerType;
}

int CLC::GetPlotMarkerType(POSITION pos)
{
	if (pos == NULL) return 0;
	LC_PLOTINFO *pinfo = (LC_PLOTINFO*)listPlotInfo.GetAt(pos);
	return pinfo->marker.nType;
}

void CLC::SetPlotMarkerColor(POSITION pos, COLORREF nMarkerColor)
{
	if (pos == NULL) return;
	LC_PLOTINFO *pinfo = (LC_PLOTINFO*)listPlotInfo.GetAt(pos);
	pinfo->marker.nColor = nMarkerColor;
}

COLORREF CLC::GetPlotMarkerColor(POSITION pos)
{
	if (pos == NULL) return 0;
	LC_PLOTINFO *pinfo = (LC_PLOTINFO*)listPlotInfo.GetAt(pos);
	return pinfo->marker.nColor;
}

void CLC::SetPlotDescr(POSITION pos, CString descr)
{
	if (pos == NULL) return;
	LC_PLOTINFO *pinfo = (LC_PLOTINFO*)listPlotInfo.GetAt(pos);
	pinfo->strDescr = descr;
}

CString CLC::GetPlotDescr(POSITION pos)
{
	if (pos == NULL) return CString(_T(""));
	LC_PLOTINFO *pinfo = (LC_PLOTINFO*)listPlotInfo.GetAt(pos);
	return pinfo->strDescr;
}

void CLC::SetPlotDataFormat(POSITION pos, CString strFmtX, CString strFmtY)
{
	if (pos == NULL) return;
	LC_PLOTINFO *pinfo = (LC_PLOTINFO*)listPlotInfo.GetAt(pos);
	pinfo->strFmtX = strFmtX;
	pinfo->strFmtY = strFmtY;
}

void CLC::SetAxesName(CString strNameX, CString strNameY)
{
	strAxisX = strNameX;
	strAxisY = strNameY;
}

int CLC::GetActivePlotsCount()
{
	int n = 0;
	POSITION pos = listPlotInfo.GetHeadPosition();
	while (pos != NULL) 
	{
		if(((LC_PLOTINFO*)listPlotInfo.GetNext(pos))->nFlags & LCF_GRAPH_HIDDEN) continue;
		n++;
	}
	return n;
}


void CLC::UpdateLPSize(void)
{
	if (rPlot.IsRectEmpty()) 
	{
		rPoint.SetRectEmpty();
		rLegend.SetRectEmpty();
	} 
	else 
	{
		CDC *pDC = GetDC();
		CSize size = pDC->GetTextExtent(_T("0"));
		rPoint = CRect(rPlot.left+10, rPlot.bottom-10-size.cy-size.cy/4, rPlot.left+10+size.cx*20, rPlot.bottom-10);
		if ((rPoint.top < rPlot.top+10) || (rPoint.right > rPlot.right-10)) rPoint.SetRectEmpty();

		rLegend.SetRectEmpty();
		CRect rPlot1 = rPlot;
		rPlot1.DeflateRect(10,10);

		int n = GetActivePlotsCount();
		if(strLegendText.GetLength() > 0) n++;
		if(nLegendLines > 0) n = min(n, nLegendLines);
		
		int nCharsW = nLegendChars + 9 + 2;
		int nDy = size.cy + size.cy/3;

		if ((rPlot1.right - size.cx*nCharsW) >= rPlot1.left) 
		{
			while((nDy*n+size.cy) > rPlot1.Height()) 
			{
				if(--n <= 0) break;
			}
			
			if ((n > 1) || ((strLegendText.GetLength() == 0) && (n > 0)))
			{
				rLegend = CRect(rPlot1.right - size.cx*nCharsW, rPlot1.top, 
								rPlot1.right,            		rPlot1.top + nDy*n + size.cy);
			}
		}

		if(ptLegend != CPoint(0,0))
		{
			rLegend.MoveToXY(ptLegend);
/*
			if(ptLegend.x >= 0)
				rLegend.MoveToX(rPlot.left + ptLegend.x);
			else
				rLegend.MoveToX(rPlot.right + ptLegend.x);

			if(ptLegend.y >= 0)
				rLegend.MoveToY(rPlot.top + ptLegend.y);
			else
				rLegend.MoveToY(rPlot.bottom + ptLegend.y);
*/
		}

		ReleaseDC(pDC);
	}
}

BOOL CLC::PreCreateWindow(CREATESTRUCT& cs) 
{
	if (!CWnd::PreCreateWindow(cs))
		return FALSE;
/*	
	cs.dwExStyle |= WS_EX_CLIENTEDGE;
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW|CS_VREDRAW|CS_DBLCLKS, 
		::LoadCursor(NULL, IDC_ARROW), HBRUSH(COLOR_WINDOW+1), NULL);
*/
	return TRUE;
}



//	Masks
void CLC::EmptyMasksList()
{
	POSITION pos, tpos;
	pos = listMasks.GetHeadPosition();

	while (pos != NULL) 
	{
		tpos = pos;
		delete listMasks.GetNext(pos);
		listMasks.RemoveAt(tpos);
	}
}

POSITION CLC::AddMask(LC_MASK* pMask)
{
	LC_MASK *pNewMask = new LC_MASK();
	*pNewMask = *pMask;
	return listMasks.AddTail((void*)pNewMask);
}

void CLC::RemoveMask(POSITION pos)
{
	if (pos == NULL) return;
	listMasks.RemoveAt(pos);
}

	//	User Legends
void CLC::EmptyLegendsList()
{
	POSITION pos, tpos;
	pos = listLegends.GetHeadPosition();

	while (pos != NULL) 
	{
		tpos = pos;
		delete listLegends.GetNext(pos);
		listLegends.RemoveAt(tpos);
	}
}

POSITION  CLC::AddLegend(CLCUL* pLegend)
{
	CLCUL *pNewLegend = new CLCUL(*pLegend);
	return listLegends.AddTail((void*)pNewLegend);
}

void CLC::RemoveLegend(POSITION pos)
{
	if (pos == NULL) return;
	listLegends.RemoveAt(pos);
}

CLCUL* CLC::GetLegend(POSITION pos)
{
	if (pos == NULL) return NULL;
	return (CLCUL*)listLegends.GetAt(pos);
}


//	Locator
void CLC::EmptyLocatorList()
{
	POSITION pos, tpos;
	pos = listLocator.GetHeadPosition();

	while (pos != NULL) 
	{
		tpos = pos;
		delete listLocator.GetNext(pos);
		listLocator.RemoveAt(tpos);
	}
}

POSITION CLC::AddLocatorArea(LC_CLICK_AREA* pArea)
{
	pArea->rectArea.NormalizeRect();
	return listLocator.AddTail((void*)pArea);
}

LC_CLICK_AREA* CLC::LocateData(CPoint point)
{
	POSITION pos = listLocator.GetHeadPosition();
	while(pos != NULL)
	{
		LC_CLICK_AREA* pArea = (LC_CLICK_AREA*)listLocator.GetNext(pos);
		if(pArea->rectArea.PtInRect(point)) 
		{
			return pArea;
		}
	}
	return NULL;
}

int CLC::IsGraphInRect(POSITION posGraph, CRect rect)
{
	POSITION pos = listLocator.GetHeadPosition();
	while(pos != NULL)
	{
		LC_CLICK_AREA* pArea = (LC_CLICK_AREA*)listLocator.GetNext(pos);
		CRect trect;
		if(trect.IntersectRect(&(pArea->rectArea), &rect))
//		if(pArea->rectArea.PtInRect(rect.CenterPoint()))
		{
			if(posGraph == FindPlot(pArea->pPlotInfo->pDataSet)) return 1;
		}
	}
	return 0;
}


int CLC::LocateGraphPoints(POSITION posGraph, CRect rect, int *pnPoints)
{
	LC_PLOTINFO *pinfo = (LC_PLOTINFO*)listPlotInfo.GetAt(posGraph);
	if(!pinfo) return -1;


	CLCDataSet *pDataSet = pinfo->pDataSet;

	if((pDataSet->GetDataType() != LC_TYPE_FIXED) && (pDataSet->GetDataType() != LC_TYPE_XDATA)) return 0;
	if (pinfo->marker.nType == LCF_MARKERS_NONE) return 0;

	double x0 = vpx0;
	double x1 = vpx1;

	if (x0 < pDataSet->GetIMIN()) x0 = pDataSet->GetIMIN();
	if (x1 > pDataSet->GetIMAX()) x1 = pDataSet->GetIMAX();

	if (x0 > x1) return 0;

	long n0, n1, n;

	pDataSet->GetPointsRange(x0, x1, &n0, &n1);
	n0--;
	n1++;
	if (n0 < 0) n0 = 0;
	if (n1 > pDataSet->GetPointCount()-1) n1 = pDataSet->GetPointCount()-1;
	if (n0 > n1) return 0;

	int k = 0;
	for (n=n0; n<=n1; n++) 
	{
		if(pDataSet->GetInterpolation() == LCF_CONNECT_HISTO)
		{
			int iy0 = (rPlot.Height()+1)*(0-vpy0)/(vpy1-vpy0)+1;
			if(iy0 < 0) iy0 = 0;
			if(iy0 > rPlot.Height()) iy0 = rPlot.Height();
			iy0 = rPlot.bottom - iy0;
			
			double x1, y1;
			pDataSet->GetPoint(n, &x1, &y1);
			int ix1 = InvX((rPlot.Width()+1)*(x1-vpx0)/(vpx1-vpx0));
			int iy1 = rPlot.bottom - (rPlot.Height()+1)*(y1-vpy0)/(vpy1-vpy0);

			double x2, y2;
			pDataSet->GetPoint(n+1, &x2, &y2);
			int ix2 = InvX((rPlot.Width()+1)*(x2-vpx0)/(vpx1-vpx0));

			CPoint p0, p2;
			p0 = CPoint(ix1, iy0);
			ForcePointToRect(p0, rPlot);
			p2 = CPoint(ix2, iy1);
			ForcePointToRect(p2, rPlot);

			CRect rectH(p0, p2);
			rectH.NormalizeRect();

			CRect trect;
			if(trect.IntersectRect(&rect, &rectH))
			{
				if(pnPoints != NULL) pnPoints[k] = pDataSet->GetOrgIdx(n);
				k++;
			}
		}
		else
		{
			double x, y;
			pDataSet->GetPoint(n, &x, &y);
			if ((x < vpx0) || (x > vpx1)) continue;
			if ((y < vpy0) || (y > vpy1)) continue;

			int ix = InvX((rPlot.Width()+1)*(x-vpx0)/(vpx1-vpx0));
			int iy = rPlot.bottom - (rPlot.Height()+1)*(y-vpy0)/(vpy1-vpy0);

			if(rect.PtInRect(CPoint(ix,iy)))
			{
				if(pnPoints != NULL) pnPoints[k] = pDataSet->GetOrgIdx(n);
				k++;
			}
		}
	}

	return k;
}



void CLC::EmptyZoomList()
{
	POSITION pos, tpos;
	pos = listZoom.GetHeadPosition();

	while (pos != NULL) 
	{
		tpos = pos;
		delete listZoom.GetNext(pos);
		listZoom.RemoveAt(tpos);
	}

	VPORT *pv = new VPORT;
	pv->x0 = vpx0;
	pv->x1 = vpx1;
	pv->y0 = vpy0;
	pv->y1 = vpy1;
	listZoom.AddTail(pv);
	posZoom = listZoom.GetTailPosition();
}

int CLC::GetSelection(double *x0, double *y0, double *x1, double *y1)
{
	if (sel.IsRectEmpty()) return 0;
	if (x0 != NULL) *x0 = vpx0 + (vpx1-vpx0)*(sel.left-rPlot.left)/(rPlot.Width()+1);
	if (x1 != NULL) *x1 = vpx0 + (vpx1-vpx0)*(sel.right-rPlot.left)/(rPlot.Width()+1);
	if (y0 != NULL) *y0 = vpy0 + (vpy1-vpy0)*(rPlot.bottom-sel.bottom)/(rPlot.Height()+1);
	if (y1 != NULL) *y1 = vpy0 + (vpy1-vpy0)*(rPlot.bottom-sel.top)/(rPlot.Height()+1);
	return 1;
}

void CLC::RemoveSelection(void)
{
	sel.SetRectEmpty();
	GetParent()->SendMessage(WM_GRAPH, GCN_SEL_CHANGED);
}

int CLC::IsLegendVisible(void)
{
	return bLegend;
}

void CLC::SetLegendText(const TCHAR *text)
{
	strLegendText = text;
}

void CLC::SetCaps(DWORD nFlags)
{
	nCaps = nFlags;
}

void CLC::SetScaleMode(int mode)
{
	nScaleMode = mode;
}

void CLC::RescaleFull() 
{
	RemoveSelection();

	double x0, y0, x1, y1;
	CLCDataSet *dset;
	POSITION pos;
	
	if(nScaleMode & LC_SCALE_USER) 
	{
		SetViewPort(upx0, upy0, upx1, upy1);
	}
	else
	{
		if(nScaleMode & LC_SCALE_ACTIVE) 
		{
			pos = GetActivePlotPosition();
			if (pos == NULL) return;
			dset = GetPlotData(pos);
			if(nScaleMode & LC_SCALE_NOFUNC && dset->GetInterpolation() == LCF_CONNECT_EXT) return;
			x0 = dset->GetIMIN();
			x1 = dset->GetIMAX();
			dset->GetMinMax(x0, x1, &y0, &y1);
		} 
		else 
		{
			pos = GetFirstPlotPos();
			int first = 1;
			while (pos != NULL) 
			{
				dset = GetNextPlotData(pos);
				if(nScaleMode & LC_SCALE_NOFUNC && dset->GetInterpolation() == LCF_CONNECT_EXT) continue;
				if (first) 
				{
					x0 = dset->GetIMIN();
					x1 = dset->GetIMAX();
					dset->GetMinMax(x0, x1, &y0, &y1);
					first = 0;
				} 
				else 
				{
					if (dset->GetIMIN() < x0) x0 = dset->GetIMIN();
					if (dset->GetIMAX() > x1) x1 = dset->GetIMAX();
					double miny, maxy;
					dset->GetMinMax(dset->GetIMIN(), dset->GetIMAX(), &miny, &maxy);
					if (miny < y0) y0 = miny;
					if (maxy > y1) y1 = maxy;
				}
			}
			if(first) return;
		}

		if (y0 == y1) { y0-=1; y1+=1; }
		if (x0 == x1) { x0-=1; x1+=1; }
		
		SetViewPort(x0-(x1-x0)/10, y0-(y1-y0)/10, x1+(x1-x0)/10, y1+(y1-y0)/10);
	}
}



// Overridables
void CLC::BuildMainPopup(CMenu *pMenu)
{
	CString str;
	UINT nFlags;

	nFlags = CanZoomBack() ? 0 : MF_GRAYED;
//	str = GETSTRUI(180);//_T("Prev view");
	str = _T("Prev view");
	pMenu->AppendMenu(MF_STRING | nFlags, LC_POPUP_GO_BACK, str);

	nFlags = CanZoomForward() ? 0 : MF_GRAYED;
//	str = GETSTRUI(181);//_T("Next view");
	str = _T("Next view");
	pMenu->AppendMenu(MF_STRING | nFlags, LC_POPUP_GO_FORWARD, str);
	pMenu->AppendMenu(MF_SEPARATOR);

	if(nScaleMode & LC_SCALE_ACTIVE) 
	{
		nFlags = (GetActivePlotPosition() ? 0 : MF_GRAYED);	
	}
	else 
	{
		nFlags = (bData ? 0 : MF_GRAYED);
	}
	
//	str = GETSTRUI(179);// _T("Full screen"); //NLMLoadString(STRID_NLM_FULL_SCREEN);
	str = _T("Full screen");
	pMenu->AppendMenu(MF_STRING | nFlags, LC_POPUP_FULL_SCREEN, str);

//	str = GETSTRUI(184);// _T("Auto Y"); //NLMLoadString(STRID_NLM_AUTO_YSCALE);
	str = _T("Auto Y");
	pMenu->AppendMenu(MF_STRING | nFlags, LC_POPUP_AUTOY, str);

	pMenu->AppendMenu(MF_SEPARATOR);
//	pMenu->AppendMenu(MF_STRING | nFlags, LC_POPUP_XLEFT, GETSTRUI(182)); //_T("Scroll Left"));
	pMenu->AppendMenu(MF_STRING | nFlags, LC_POPUP_XLEFT, _T("Scroll Left"));
//	pMenu->AppendMenu(MF_STRING | nFlags, LC_POPUP_XRIGHT, GETSTRUI(183)); //_T("Scroll Right"));
	pMenu->AppendMenu(MF_STRING | nFlags, LC_POPUP_XRIGHT, _T("Scroll Right"));

	pMenu->AppendMenu(MF_SEPARATOR);
/*
	nFlags = (IsLegendVisible() ? MF_CHECKED : 0);
	str = theApp.GetStrUI(43); //_T("Show legend"); //NLMLoadString(STRID_NLM_SHOW_LEGEND);
	pMenu->AppendMenu(MF_STRING | nFlags, LC_POPUP_LEGEND, str);
*/
	nFlags = (bData ? 0 : MF_GRAYED);
//	str = GETSTRUI(657);//_T("Export Image"); //NLMLoadString(STRID_NLM_EXPORT_EMF);
	str = _T("Export Image");
	pMenu->AppendMenu(MF_STRING | nFlags, LC_POPUP_EXPORT, str);
}


void CLC::BuildSelPopup(CMenu *pMenu)
{
	CString str = _T("Zoom in"); //NLMLoadString(STRID_NLM_ZOOM_IN);
	pMenu->AppendMenu(MF_STRING, LC_POPUP_ZOOM_IN, str);
}

// Context menu implementation
void CLC::OnFullScreen() 
{
	RescaleFull();
	RedrawWindow();
	GetParent()->PostMessage(WM_TIMER, 1000 + LC_POPUP_FULL_SCREEN, 0);
}

void CLC::OnGoBack() 
{
	ZoomBack();
	RedrawWindow();
	GetParent()->PostMessage(WM_TIMER, 1000 + LC_POPUP_GO_BACK, 0);
}

void CLC::OnGoForward() 
{
	ZoomForward();
	RedrawWindow();
	GetParent()->PostMessage(WM_TIMER, 1000 + LC_POPUP_GO_FORWARD, 0);
}

void CLC::OnZoomIn() 
{
	double x0, y0, x1, y1;
	
	if (GetSelection(&x0, &y0, &x1, &y1)) 
	{
		SetViewPort(x0, y0, x1, y1);
		RemoveSelection();
		RedrawWindow();
		GetParent()->PostMessage(WM_TIMER, 1000 + LC_POPUP_ZOOM_IN, 0);
	}
}

void CLC::OnLegend() 
{
	if (IsLegendVisible()) {
		HideLegend();
	} else {
		ShowLegend();
	}
	RedrawWindow();
}

void CLC::OnXleft() 
{
	double x0, y0, x1, y1;
	GetViewPort(&x0, &y0, &x1, &y1);
	double nx0 = x0 - (x1-x0)*fScroll;
	double nx1 = x1 - (x1-x0)*fScroll;
	SetViewPort(nx0, y0, nx1, y1);
	RedrawWindow();
	GetParent()->PostMessage(WM_TIMER, 1000 + LC_POPUP_XLEFT, 0);
}

void CLC::OnXright() 
{
	double x0, y0, x1, y1;
	GetViewPort(&x0, &y0, &x1, &y1);
	double nx0 = x0 + (x1-x0)*fScroll;
	double nx1 = x1 + (x1-x0)*fScroll;
	SetViewPort(nx0, y0, nx1, y1);
	RedrawWindow();
	GetParent()->PostMessage(WM_TIMER, 1000 + LC_POPUP_XRIGHT, 0);
}

void CLC::OnAutoY() 
{
	RemoveSelection();

	double x0, y0, x1, y1;
	POSITION pos;
	GetViewPort(&x0, &y0, &x1, &y1);
	
	if(nScaleMode & LC_SCALE_ACTIVE) 
	{
		pos = GetActivePlotPosition();
		if (pos == NULL) return;
		CLCDataSet *dset = GetPlotData(pos);
		if(nScaleMode & LC_SCALE_NOFUNC && dset->GetInterpolation() == LCF_CONNECT_EXT) return;
		dset->GetMinMax(x0, x1, &y0, &y1);
	} 
	else //if(nScaleMode == LC_AUTO_SCALE_ALL) 
	{
		pos = GetFirstPlotPos();
		int isFirst = 1;
		while (pos != NULL) 
		{
			LC_PLOTINFO* pinfo = (LC_PLOTINFO*)listPlotInfo.GetNext(pos);
			if(nScaleMode & LC_SCALE_NOFUNC && pinfo->pDataSet->GetInterpolation() == LCF_CONNECT_EXT) continue;
			if(pinfo->nFlags & LCF_GRAPH_HIDDEN) continue;

			if (isFirst) 
			{
				pinfo->pDataSet->GetMinMax(x0, x1, &y0, &y1);
				isFirst = 0;
			} 
			else 
			{
				double miny, maxy;
				pinfo->pDataSet->GetMinMax(x0, x1, &miny, &maxy);
				if (miny < y0) y0 = miny;
				if (maxy > y1) y1 = maxy;
			}
		}
		if(isFirst) return;
	}

	if (y0 == y1) { y0 -= 1; y1 += 1; }
	SetViewPort(x0, y0-(y1-y0)/10, x1, y1+(y1-y0)/10);
	RedrawWindow();
}

void CLC::ExportPicture(const TCHAR* strName)
{
	CRect clientRect;
	GetClientRect(clientRect);

	CPaintDC dc(this);

	CImage img;
	img.Create(clientRect.Width(), -clientRect.Height(), dc.GetDeviceCaps(BITSPIXEL), 0);
//	img.Create(666, 598, dc.GetDeviceCaps(BITSPIXEL), 0);

	if(img.GetDC() == NULL) return;
	CDC* pDC = CDC::FromHandle(img.GetDC());
	Print(pDC, 0);

	img.Save(strName);
	img.ReleaseDC();
}

void CLC::OnExportImage() 
{
	CString strFilter = _T("JPEG image|*.jpg|Bitmap image|*.bmp|GIF image|*.gif|PNG image|*.png|Windows EMF|*.emf||");
	CFileDialog dlg(FALSE,NULL,NULL,OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT | OFN_EXPLORER,strFilter);
	if(dlg.DoModal() != IDOK) return;

	CString strFileName = dlg.m_ofn.lpstrFile;

	int nOpt = 0;
	// add the file extension if the user didn't supply one
	if (dlg.m_ofn.nFileExtension == 0) 
	{
		CString strExt;
		nOpt = dlg.m_ofn.nFilterIndex;
		switch (nOpt)
		{
		case 1:
			strExt = _T(".jpg");
			break;
		case 2:
			strExt = _T(".bmp");
			break;
		case 3:
			strExt = _T(".gif");
			break;
		case 4:
			strExt = _T(".png");
			break;
		case 5:
			strExt = _T(".emf");
			break;
		default:
			return;
		}
		strFileName += strExt;
	}

	if(nOpt == 5)	// Windows EMF
	{
		HDC hDC;
		hDC = CreateEnhMetaFile(NULL, strFileName, NULL, NULL);
	
		if (hDC == NULL) 
		{
			MessageBox(_T("Failed to create EMF"), _T("GC error"), MB_ICONEXCLAMATION | MB_OK);
			return;
		}

		CDC *pDC = new CDC();
		pDC->Attach(hDC);
		Print(pDC, NULL);
		pDC->Detach();
		delete pDC;
		HENHMETAFILE hMetaFile;
		hMetaFile = CloseEnhMetaFile(hDC);
		DeleteEnhMetaFile(hMetaFile);
	}
	else			//	raster image
	{
		CRect clientRect;
		GetClientRect(clientRect);

		CPaintDC dc(this);

		CImage img;
		img.Create(clientRect.Width(), -clientRect.Height(), dc.GetDeviceCaps(BITSPIXEL), 0);

		if(img.GetDC() == NULL) return;
		CDC* pDC = CDC::FromHandle(img.GetDC());
		Print(pDC, 0);

		img.Save(strFileName);
		img.ReleaseDC();
	}

	RedrawWindow();
}


// 
void CLC::SetInvertMode(bool mode)
{
	if(mode == bInvertX) return;
	bInvertX = !bInvertX;
//	OnAutoY();
}

//  Highlighting
void CLC::SetHighlightMode(bool mode)
{
	hlMode.nFlags &= ~LC_HL_METHOD;
	if(mode) hlMode.nFlags |= LC_HL_COLOR;
}

void CLC::SetHighlightColor(COLORREF color)
{
	hlMode.color = color;
}