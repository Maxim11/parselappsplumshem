#include "../stdafx.h"
#include "LumChart.h"



CLCULString::CLCULString(): 
	nFlags(LCUL_NONE), 
	strText(_T("some string")), 
	clrLineColor(RGB(0,0,0)),
	nLineStyle(PS_SOLID),
	nLineWidth(1)
{
};



void CLCULString::Enable(bool bEnable) 
{
	if(bEnable) 
		nFlags &= ~LCUL_HIDDEN; 
	else 
		nFlags |= LCUL_HIDDEN;
}

void CLCULString::EnableMarker(bool bEnable) 
{
	if(!bEnable) 
		nFlags &= ~LCUL_MARKER; 
	else 
		nFlags |= LCUL_MARKER;
}

void CLCULString::EnableLine(bool bEnable)
{
	if(!bEnable) 
		nFlags &= ~LCUL_LINE; 
	else 
		nFlags |= LCUL_LINE;
}


/////////////////////////////////////////////////////////////////////////////////////////////


CLCUL::CLCUL()
{
	nFlags = LCUL_AUTO_HEIGHT|LCUL_AUTO_WIDTH;

	clrFill = RGB(255, 255, 255);
	clrBorder = RGB(128, 0, 0);
	clrText = RGB(0, 0, 0);

	ptOrg = CPoint(0,0);
	szOrg = CSize(0,0);

	nFontSize = 10;
	strFontName = _T("Arial");

	strings = 0;
	nStrings = 0;
}

CLCUL::CLCUL(const CLCUL &legend)
{
	*this = legend;
	SetStrings(legend.strings, legend.nStrings);
}


CLCUL::~CLCUL()
{
	if(strings) delete [] strings;
}

void CLCUL::MoveTo(int newX, int newY) 
{
	ptOrg = CPoint(newX, newY);
}

void CLCUL::Resize(int newWidth, int newHeight) 
{
	szOrg = CSize(newWidth, newHeight);
}

void CLCUL::SetStrings(CLCULString *stringsNew, int numStrings) 
{
	nStrings = numStrings;
	if(nStrings < 1)
	{
		nStrings = 0;
		return;
	}

	strings = new CLCULString[nStrings];
	for(int i=0; i<nStrings; i++)
	{
		strings[i] = stringsNew[i];
	}
}

void CLCUL::SetStrings(const std::vector<CLCULString> &stringsNew, int numStrings)
{
	nStrings = numStrings == -1 ? stringsNew.size() : numStrings;
	if(nStrings < 1)
	{
		nStrings = 0;
		return;
	}

	strings = new CLCULString[nStrings];
	for(int i=0; i<nStrings; i++)
	{
		strings[i] = stringsNew[i];
	}
}


void CLCUL::SetFont(CString strName, int nSize) 
{
	strFontName = strName; 
	nFontSize = nSize;
}

void CLCUL::SetFillColor(COLORREF clrNewFill) 
{
	clrFill = clrNewFill;
}

void CLCUL::SetBorderColor(COLORREF clrNewBorder) 
{
	clrBorder = clrNewBorder;
}

void CLCUL::SetTextColor(COLORREF clrNewText) 
{
	clrText = clrNewText;
}

void CLCUL::Enable(bool bEnable) 
{
	if(bEnable) 
		nFlags &= ~LCUL_HIDDEN; 
	else 
		nFlags |= LCUL_HIDDEN;
}

void CLCUL::EnableMoveToFit(bool bEnable) 
{
	if(!bEnable) 
		nFlags &= ~LCUL_FIT_MOVE; 
	else 
		nFlags |= LCUL_FIT_MOVE;
}

void CLCUL::EnableSizeToFit(bool bEnable) 
{
	if(!bEnable) 
		nFlags &= ~LCUL_FIT_SIZE; 
	else 
		nFlags |= LCUL_FIT_SIZE;
}

void CLCUL::EnableAutoWidth(bool bEnable) 
{
	if(!bEnable) 
		nFlags &= ~LCUL_AUTO_WIDTH; 
	else 
		nFlags |= LCUL_AUTO_WIDTH;
}

void CLCUL::EnableAutoHeight(bool bEnable) 
{
	if(!bEnable) 
		nFlags &= ~LCUL_AUTO_HEIGHT; 
	else 
		nFlags |= LCUL_AUTO_HEIGHT;
}


///////////////////////////////////////////////////////////////////////////
CRect CLCUL::FitToRect(CRect rect1, CRect rectTo)
{
	if(rect1.left < rectTo.left)
	{
		int nShift = rectTo.left - rect1.left + 1;
		if(nFlags & LCUL_FIT_MOVE)
		{
			rect1.left += nShift; 
			rect1.right += nShift; 
		}
		else if(nFlags & LCUL_FIT_SIZE)
		{
			rect1.left += nShift; 
		}
	}

	if(rect1.right > rectTo.right)
	{
		int nShift = rect1.right - rectTo.right + 1;
		if(nFlags & LCUL_FIT_MOVE)
		{
			rect1.left -= nShift; 
			rect1.right -= nShift; 
		}
		else if(nFlags & LCUL_FIT_SIZE)
		{
			rect1.right -= nShift; 
		}
	}

	if(rect1.top < rectTo.top)
	{
		int nShift = rectTo.top - rect1.top + 1;
		if(nFlags & LCUL_FIT_MOVE)
		{
			rect1.top += nShift; 
			rect1.bottom += nShift; 
		}
		else if(nFlags & LCUL_FIT_SIZE)
		{
			rect1.top += nShift; 
		}
	}

	if(rect1.bottom > rectTo.bottom)
	{
		int nShift = rect1.bottom - rectTo.bottom + 1;
		if(nFlags & LCUL_FIT_MOVE)
		{
			rect1.top -= nShift; 
			rect1.bottom -= nShift; 
		}
		else if(nFlags & LCUL_FIT_SIZE)
		{
			rect1.bottom -= nShift; 
		}
	}

	return rect1;
}

CSize CLCUL::EstimateSize(CDC *pDC)
{
	CSize s0 = pDC->GetTextExtent(_T("0"));
	nStepX = s0.cx; 
	nStepY = s0.cy; 

	int nmaxx=0, nmaxy=0;
	nLines = 0;
	nFlags &= ~LCUL_LINE;
	nFlags &= ~LCUL_MARKER;
	for(int i=0; i<nStrings; i++)
	{
		if(strings[i].nFlags & LCUL_HIDDEN) continue;
		if(strings[i].nFlags & LCUL_LINE) nFlags |= LCUL_LINE;
		if(strings[i].nFlags & LCUL_MARKER) nFlags |= LCUL_MARKER;

		CSize sz = pDC->GetTextExtent(strings[i].strText);
		if(sz.cx > nmaxx) nmaxx = sz.cx;
		if(sz.cy > nmaxy) nmaxy = sz.cy;

		nLines++;
	}

	if(nFlags & LCUL_LINE)
	{
		nmaxx += nStepX*(5+1);
	}
	else if(nFlags & LCUL_MARKER)
	{
		nmaxx += nStepX*(3+1);
	}

	if(nStepY < nmaxy) nStepY = nmaxy;

	CSize sizeAuto;
	sizeAuto.cx = nmaxx + nStepX;
	sizeAuto.cy = nStepY*(nLines+1./2);
	return sizeAuto;
}

