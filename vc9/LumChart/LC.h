#pragma once

#include "LCDataSet.h"
#include "LumChart.h"

#include <atlimage.h>


/////////////////////////////////////////////////////////////////////////////
// CLC window

#define WM_GRAPH (WM_USER+0x01)
/*
WM_GRAPH message is send to the parent window.
The value in WPARAM specifies a notification code.
Valid values for WPARAM are listed below
*/

#define GCN_VPORT_CHANGED 0x01
/*
Send when view port was chahged, but before the component was redrawn.
Can be used for updating "Back" and "Forward" buttons, updating some information about
current view port, etc.
*/
#define GCN_SEL_CHANGED 0x02
/*
Send when selection was changed.
Can be used for updating some information about current selection, etc.
*/

// skin
#define LC_GAP_L 70
#define LC_GAP_T 30
#define LC_GAP_R 20
#define LC_GAP_B 60
#define LC_DKBG_COLOR RGB(176,176,164)
#define LC_LTBG_COLOR RGB(255,255,239)

#define LC_CAPS (LC_CAPS_MARKERS | LC_CAPS_MINMAX)
#define LC_LEGEND 1

#define LC_ACTIVATE_NEW_PLOT

// Context menu functions
#define LC_POPUP_FULL_SCREEN	1
#define LC_POPUP_GO_BACK		2
#define LC_POPUP_GO_FORWARD		3
#define LC_POPUP_ZOOM_IN		4
#define LC_POPUP_LEGEND			5
#define LC_POPUP_XLEFT			6 //??
#define LC_POPUP_XRIGHT			7 //??
#define LC_POPUP_AUTOY			8
#define LC_POPUP_EXPORT			9
#define LC_POPUP_USER			10

struct LC_MENUITEM
{
	UINT nFlags;
	CString strItem;
	UINT nCommand;
	CWnd *pWnd;
};

struct LC_FLABEL
{
	int hexp; // Boolean: exponent present
	int ven;  // Exponent value
	int dpd;  // Digits after dot
};

struct LC_PLOTINFO
{
	int line_style;
	int line_width;
	COLORREF line_color;

	CLumChartDataMarker marker;

	UINT nFlags;
	CString strDescr;
	CString strFmtX;
	CString strFmtY;

	CLCDataSet *pDataSet;
	CLumChartDataMarker *pUserMarkers;
};

struct LC_CLICK_AREA
{
	CRect rectArea;
	LC_PLOTINFO *pPlotInfo;
	int idxPoint;
	double x;
	double y;
};

//	Highlighting

#define LC_HL_NONE				0x0000

#define LC_HL_GRAPH				0x0001
#define LC_HL_POINT				0x0002
#define LC_HL_RECT				0x0003
#define LC_HL_SELECTION			0x000F

#define LC_HL_COLOR				0x0100
#define LC_HL_NOT				0x0200
#define LC_HL_XOR				0x0300
#define LC_HL_METHOD			0x0F00

struct LC_HIGHLIGHT
{
	UINT nFlags;
	CRect rect;
	CPoint point;
	COLORREF color;
};



//	masking

#define LC_MASK_BG				0x01
#define LC_MASK_FG				0x02
#define LC_MASK_BORDER			0x04
#define LC_MASK_EVENTS			0x08

struct LC_MASK
{
	UINT nFlags;
	double x1, x2;
	COLORREF color;
};






// Capabilities flags
#define LC_CAPS_MARKERS 0x01 // Automatically show markers
#define LC_CAPS_MINMAX  0x02 // When the scale is too big use minmax representation method

#define LC_SCALE_ALL		0x00
#define LC_SCALE_ACTIVE		0x01
#define LC_SCALE_USER		0x02
#define LC_SCALE_NOFUNC		0x04

class CLC : public CWnd
{
friend class CLumChart;
// Construction
public:
	CLC();

// Attributes
public:
	POSITION GetFirstPlotPos();						// Retrieves the position of the first plot in the list
	CLCDataSet* GetNextPlotData(POSITION &pos);		// Returns data set at position pos and sets pos to the next element in the list
	CLCDataSet* GetPlotData(POSITION pos);			// Returns plot data by its position
	POSITION FindPlot(CLCDataSet *pDataSet);		// Returns plot position by its data
	int GetActivePlotsCount();	
	void RemoveAllPlots();

	POSITION AddPlot(CLCDataSet *pDataSet);
	void RemovePlot(POSITION pos);

	void SetActivePlotPosition(POSITION pos);
	POSITION GetActivePlotPosition(void);

	void SetPlotLineColor(POSITION pos, COLORREF color);
	COLORREF GetPlotLineColor(POSITION pos);

	void SetPlotLineWidth(POSITION pos, int nWidth);
	int GetPlotLineWidth(POSITION pos);

	void SetPlotLineStyle(POSITION pos, int nStyle);
	int GetPlotLineStyle(POSITION pos);

	void SetPlotMarkers(POSITION pos, int nMarkerType, int nMarkerSize);

	void SetPlotMarkerSize(POSITION pos, int nMarkerSize);
	int GetPlotMarkerSize(POSITION pos);

	void SetPlotMarkerType(POSITION pos, int nMarkerType);
	int GetPlotMarkerType(POSITION pos);

	void SetPlotMarkerColor(POSITION pos, COLORREF nMarkerColor);
	COLORREF GetPlotMarkerColor(POSITION pos);

	void SetPlotUserMarkers(POSITION pos, CLumChartDataMarker *pMarkers);

	void SetPlotDescr(POSITION pos, CString strText);
	CString GetPlotDescr(POSITION pos);

	void SetPlotDataFormat(POSITION pos, CString strFmtX, CString strFmtY);

	void SetPlotFlags(POSITION pos, UINT nFlags);
	UINT GetPlotFlags(POSITION pos);

	void SetAxesName(CString strNameX, CString strNameY);

	COLORREF SetBgInside(COLORREF clr);
	COLORREF SetBgOutside(COLORREF clrFill, COLORREF clrBorder);

	int SetRectMargins(CRect rect);

	//  Highlighting
	void SetHighlightMode(bool mode);
	void SetHighlightColor(COLORREF color);


	int SetUserViewPort(double x0, double y0, double x1, double y1);

	int SetViewPort(double x0, double y0, double x1, double y1);
	void GetViewPort(double *x0, double *y0, double *x1, double *y1);
	void GetViewPortXY(CPoint point, double *pX, double *pY);

	void SetSolidAxisHorzAt(double ay) {usy = ay;}
	void EnableSolidAxisHorz(bool bEnable) {bSolidHorz = bEnable;}
	void SetSolidAxisVertAt(double ax) {usx = ax;}
	void EnableSolidAxisVert(bool bEnable) {bSolidVert = bEnable;}


	int GetSelection(double *x0, double *y0, double *x1, double *y1);
	void RemoveSelection(void);
	
	void ShowLegend(void);
	void HideLegend(void);
	int IsLegendVisible(void);
	void SetLegendText(const TCHAR *text);

	void SetCaps(DWORD flags);

	void RescaleFull();
	void SetScaleMode(int mode);
	void SetInvertMode(bool mode);
	void EnableMarkersAutoHide(bool bEnable) {bMarkersAuto = bEnable;}

	void SetUserPopupMenu(CWnd *pWnd, CMenu *pMenu) {pUserWnd = pWnd; pUserMenu = pMenu;}
	void EnableUserPopupMenu(bool bEnable) {bUserMenu = bEnable;}

	void Print(CDC* pDC, CPrintInfo* pInfo);
	void ExportPicture(const TCHAR* strName);

	// Overridables
	virtual void BuildMainPopup(CMenu *pMenu);
	virtual void BuildSelPopup(CMenu *pMenu);

	// Message handlers
	void OnFullScreen();
	void OnGoBack();
	void OnGoForward();
	void OnZoomIn();
	void OnLegend();
	void OnXleft();
	void OnXright();
	void OnAutoY();
	void OnExportImage();

protected:

	bool bLocatorEnable;
	bool bLocator;
	bool bLegend;
	bool bMasks;
	bool bCapture;
	bool bCross;
	bool bData;
	bool bPif;
	bool bPifEnable;
	bool bDragFirst;
	bool bMarkersAuto;
	bool bGridHorz;
	bool bGridVert;
	bool bSolidHorz;
	bool bSolidVert;

	bool bInvertX;
	int InvX(int ix);

	bool bUserMenu;
	CMenu *pUserMenu;
	CWnd *pUserWnd;

	double vpx0, vpx1, vpy0, vpy1;
	double upx0, upx1, upy0, upy1;		//	user-defined viewport
	double usx, usy;
	CRect rGrid, rMargins, rPlot, rPoint, rLocator;

	int nGridFontSize;


	CString strLegendText;
	CString strLegendFont;
	int nLegendFontSize;
	int nLegendChars;
	int nLegendLines;
	bool bLegendOrder;
	CRect rLegend;
	CPoint ptLegend;


	double fScroll;
	void SetScroll(double fDelta) {fScroll = fDelta;}


	//	Zoom list and operations
	CPtrList listZoom;
	POSITION posZoom;
	void EmptyZoomList();
	void ZoomBack();
	void ZoomForward();
	int  CanZoomBack();
	int  CanZoomForward();


	//	Data locator and operations
	int nLocatorSpan;
	CPtrList listLocator;
	void EmptyLocatorList();
	POSITION  AddLocatorArea(LC_CLICK_AREA* );
	LC_CLICK_AREA* LocateData(CPoint point);

	int LocateGraphPoints(POSITION posGraph, CRect rect, int *pnPoints);
	int IsGraphInRect(POSITION posGraph, CRect rect);
	int IsPointInSel(CPoint point);

	//	Highlighting
	LC_HIGHLIGHT hlMode;


	//	Masking
	CPtrList listMasks;
	void EnableMasks(bool bEnable) {bMasks = bEnable;}
	void EmptyMasksList();
	POSITION  AddMask(LC_MASK* pMask);
	void RemoveMask(POSITION pos);
	bool IsPointMasked(CPoint point);
	bool IsRectMasked(CRect rect);

	//	User Legends
	CPtrList listLegends;
	void EmptyLegendsList();
	POSITION  AddLegend(CLCUL* pLegend);
	void RemoveLegend(POSITION pos);
	CLCUL* GetLegend(POSITION pos);


	//	Drawing
	void DrawGrid(CDC *pDC);
	void DrawMasks(CDC *pDC, bool bFg);
	void DrawPlot(CDC *pDC, int nHighlight);
	void DrawLegend(CDC *pDC);
	void DrawUserLegends(CDC *pDC);
	void DrawUserLegend(CDC *pDC, CLCUL *pLegend);
	void DrawDataLegend(CDC *pDC, CRect rect, CLCULString* pData);
	void DrawLocatorData(CDC *pDC, LC_CLICK_AREA* pArea);
	void DrawPIF(CDC *pDC);
	void DrawDataLegend(CDC *pDC, CRect rect, LC_PLOTINFO *pinfo, int nIdx);
	void DrawDataSet(CDC *pDC, LC_PLOTINFO *pinfo, int nHighlight);
	void DrawSel(CDC *pDC);
	void DrawCross(CDC *pDC, CPoint point);
	void Move2(CDC *pDC, double x, double y);
	void Line2(CDC *pDC, double x, double y);
	int DrawPath(CDC *pDC, CPoint *p, int np, int nClip=1); 

	int MsAction;
	CPoint pd;
	CRect RValid(CRect r);
	CRect sel, dsel;
	int GetPos(CPoint p);
	void SetActCur(int action);

	double curX, curY;

	double pifX, pifY;
	void SetPIF(double x, double y);
	void RemovePIF(void);

//	void FormatLabel(double x1, double x2, double dx, int dig, FLABEL *frm);
	void FormatLabel(double x, double dx, int dig, LC_FLABEL *frm);
	
	CString Num2Str(double x, LC_FLABEL frm);
	CString Num2Str4PIF(double d, int dig);

//	CString Time2Str(double x, bool bShowDate);
//	CString Time2Str4PIF(double d, bool bShowDate);

	CPoint mspt;

	CPtrList listPlotInfo;

	CString strAxisX, strAxisY;

	POSITION actplot;
	void UpdateLPSize(void);
	DWORD nCaps;
	int nScaleMode;

	COLORREF clrBgOutBorder;
	COLORREF clrBgOut;
	COLORREF clrBgIn;

	LC_CALLBACKS *pCallbacks;

	CBrush *pSelFrameBrush;
	COLORREF clrSelFrame;
	int nSelFrameWidth;

	void SetSelFrameProps(COLORREF clr, int nWidth);


// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLC)
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CLC();

	// Generated message map functions
protected:
	void DrawBg(CDC *pDC);

	afx_msg void OnPaint();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	DECLARE_MESSAGE_MAP()
};

