#include "../stdafx.h"
#include "LumChart.h"
#include "LC.h"

#define LCAF_NONE			0x0000
#define LCAF_NAME			0x0001
#define LCAF_CONNECT		0x0002
#define LCAF_MSIZE			0x0004
#define LCAF_MTYPE			0x0008
#define LCAF_MCOLOR			0x0010
#define LCAF_COLOR			0x0020
#define LCAF_WIDTH			0x0040
#define LCAF_STYLE			0x0080
#define LCAF_FORMAT			0x0100
#define LCAF_MARRAY			0x0200
#define LCAF_MUSER			0x0400

#define LCAF_DATA			0x8000

void CLumChartDataSet::PreInit()
{
	strName = _T("unnamed");
	nConnectType = LCF_CONNECT_DIRECT;
	nMarkerSize = 3;
	nMarkerType = LCF_MARKERS_DIAMOND;
	nMarkerColor = RGB(255,255,255);
	pUserMarkers = 0;
	bUserMarkersEnabled = false;

	nLineColor = RGB(0,0,0);
	nLineWidth = 1;
	nLineStyle = PS_SOLID;

	nAttrFlags = LCAF_NONE;
	pos = NULL;
}

//	various constructors
CLumChartDataSet::CLumChartDataSet()
{
	pDataSet = 0;
	PreInit();
}


CLumChartDataSet::CLumChartDataSet(int nPoints, double *x, double *y)
{
	pDataSet = new CLCDataSet(LC_TYPE_XDATA);
	if(!pDataSet) return;
	pDataSet->SetLength(nPoints);

	for(int i=0; i<nPoints; i++)
	{
		pDataSet->ax[i] = x[i];
		pDataSet->ay[i] = y[i];
	}
	pDataSet->Sort();

	PreInit();
	nAttrFlags |= LCAF_DATA;
}

CLumChartDataSet::CLumChartDataSet(int nPoints, const std::vector<double> &vx, const std::vector<double> &vy)
{
	pDataSet = new CLCDataSet(LC_TYPE_XDATA);
	if(!pDataSet) return;
	pDataSet->SetLength(nPoints);

	for(int i=0; i<nPoints; i++)
	{
		pDataSet->ax[i] = vx[i];
		pDataSet->ay[i] = vy[i];
	}
	pDataSet->Sort();

	PreInit();
	nAttrFlags |= LCAF_DATA;
}

CLumChartDataSet::CLumChartDataSet(int nPoints, double x0, double dx, double *y)
{
	pDataSet = new CLCDataSet(LC_TYPE_FIXED);
	if(!pDataSet) return;
	pDataSet->SetLength(nPoints);

	pDataSet->xMin = x0;
	pDataSet->xMax = x0 + (nPoints-1)*dx;

	for(int i=0; i<nPoints; i++)
	{
		pDataSet->iOrg[i] = i;
		pDataSet->ay[i] = y[i];
	}

	PreInit();
	nAttrFlags |= LCAF_DATA;
}

CLumChartDataSet::CLumChartDataSet(int nPoints, double x0, double dx, const std::vector<double> &vy)
{
	pDataSet = new CLCDataSet(LC_TYPE_FIXED);
	if(!pDataSet) return;
	pDataSet->SetLength(nPoints);
	pDataSet->xMin = x0;
	pDataSet->xMax = x0 + (nPoints-1)*dx;

	for(int i=0; i<nPoints; i++)
	{
		pDataSet->iOrg[i] = i;
		pDataSet->ay[i] = vy[i];
	}

	PreInit();
	nAttrFlags |= LCAF_DATA;
}

CLumChartDataSet::CLumChartDataSet(double x1, double x2, double (*f) (double))
{
	pDataSet = new CLCDataSet(LC_TYPE_CBACK);
	if(!pDataSet) return;

	pDataSet->xMin = x1;
	pDataSet->xMax = x2;
	pDataSet->SetCallBack(f);

	PreInit();
	SetMarkerType(LCF_MARKERS_NONE);
	pDataSet->SetInterpolation(LCF_CONNECT_EXT);
	nAttrFlags |= LCAF_DATA;
}

CLumChartDataSet::CLumChartDataSet(int nPoints, double *x, double (*f) (double))
{
	pDataSet = new CLCDataSet(LC_TYPE_XDATA);
	if(!pDataSet) return;
	pDataSet->SetLength(nPoints);

	for(int i=0; i<nPoints; i++)
	{
		pDataSet->ax[i] = x[i];
		pDataSet->ay[i] = f(x[i]);
	}
	pDataSet->Sort();

	PreInit();
	nAttrFlags |= LCAF_DATA;
}

CLumChartDataSet::CLumChartDataSet(int nPoints, const std::vector<double> &vx, double (*f) (double))
{
	pDataSet = new CLCDataSet(LC_TYPE_XDATA);
	if(!pDataSet) return;
	pDataSet->SetLength(nPoints);

	for(int i=0; i<nPoints; i++)
	{
		pDataSet->ax[i] = vx[i];
		pDataSet->ay[i] = f(vx[i]);
	}
	pDataSet->Sort();

	PreInit();
	nAttrFlags |= LCAF_DATA;
}




CLumChartDataSet::CLumChartDataSet(int nPoints, double x0, double dx, double (*f) (double))
{
	pDataSet = new CLCDataSet(LC_TYPE_FIXED);
	if(!pDataSet) return;

	pDataSet->xMin = x0;
	pDataSet->xMax = x0 + (nPoints-1)*dx;
	pDataSet->SetLength(nPoints);

	for(int i=0; i<nPoints; i++)
	{
		pDataSet->iOrg[i] = i;
		pDataSet->ay[i] = f(x0+i*dx);
	}

	PreInit();
	nAttrFlags |= LCAF_DATA;
}

int CLumChartDataSet::SetInterpolation(int nConnect)
{
	if(!pDataSet) return LCF_ERR;
	if(nConnect < LCF_CONNECT_NONE) return LCF_ERR;
	if(nConnect > LCF_CONNECT_HISTO) return LCF_ERR;

	nConnectType = nConnect;
//	pDataSet->SetInterpolation(nConnectType);

	nAttrFlags |= LCAF_CONNECT;
	return LCF_OK;
}

int CLumChartDataSet::SetName(CString _strName)
{
	if(!pDataSet) return LCF_ERR;

	strName = _strName;

	nAttrFlags |= LCAF_NAME;
	return LCF_OK;
}

int CLumChartDataSet::SetFormat(CString _strFmtX, CString _strFmtY)
{
	if(!pDataSet) return LCF_ERR;

	strFmtX = _strFmtX;
	strFmtY = _strFmtY;

	nAttrFlags |= LCAF_FORMAT;
	return LCF_OK;
}

int CLumChartDataSet::SetColor(COLORREF _nColor)
{
	if(!pDataSet) return LCF_ERR;

	nLineColor = _nColor;

	nAttrFlags |= LCAF_COLOR;
	return LCF_OK;
}

int CLumChartDataSet::SetWidth(int nWidth)
{
	if(!pDataSet) return LCF_ERR;

	nLineWidth = nWidth;

	nAttrFlags |= LCAF_WIDTH;
	return LCF_OK;
}

int CLumChartDataSet::SetStyle(int nStyle)
{
	if(!pDataSet) return LCF_ERR;

	nLineStyle = nStyle;

	nAttrFlags |= LCAF_STYLE;
	return LCF_OK;
}

int CLumChartDataSet::SetMarkerType(int nType)
{
	if(!pDataSet) return LCF_ERR;
	if(nType < LCF_MARKERS_NONE) return LCF_ERR;

	nMarkerType = nType; 

	nAttrFlags |= LCAF_MTYPE;
	return LCF_OK;
}

int CLumChartDataSet::SetMarkerSize(int nSize)
{
	if(!pDataSet) return LCF_ERR;
	if(nSize < 3) nSize = 3;

	nMarkerSize = nSize; 

	nAttrFlags |= LCAF_MSIZE;
	return LCF_OK;
}

int CLumChartDataSet::SetMarkerColor(COLORREF color)
{
	if(!pDataSet) return LCF_ERR;

	nMarkerColor = color; 

	nAttrFlags |= LCAF_MCOLOR;
	return LCF_OK;
}

int CLumChartDataSet::SetMarker(int nType, int nSize, COLORREF color)
{
	if(!pDataSet) return LCF_ERR;

	if(nType < LCF_MARKERS_NONE) return LCF_ERR;
	nMarkerType = nType; 
	nAttrFlags |= LCAF_MTYPE;

	if(nSize < 3) nSize = 3;
	nMarkerSize = nSize; 
	nAttrFlags |= LCAF_MSIZE;

	nMarkerColor = color; 
	nAttrFlags |= LCAF_MCOLOR;

	return LCF_OK;
}

int CLumChartDataSet::SetMarkerArray(CLumChartDataMarker *pMarkers)
{
	if(!pDataSet) return LCF_ERR;

	pUserMarkers = pMarkers; 
	nAttrFlags |= LCAF_MARRAY;

	return LCF_OK;
}

int CLumChartDataSet::EnableUserMarkers(bool bEnable)
{
	if(!pDataSet) return LCF_ERR;

	bUserMarkersEnabled = bEnable; 
	nAttrFlags |= LCAF_MUSER;

	return LCF_OK;
}


//////////////////////////////////////////////////////////////////////////////////////

CLumChart::CLumChart(CRect rect, CWnd *pParent, UINT nFlags)
{
	pWndLC = new CLC();
	if(!pWndLC) return;

	if(nFlags & LCF_RECT_CLIENT)
	{
		pWndLC->Create(NULL, NULL, WS_CHILD|WS_VISIBLE, rect, pParent, AFX_IDW_PANE_LAST, NULL);	
	}
	else
	{
		CRect client_rect;
		pParent->GetClientRect(client_rect);

		pWndLC->Create(NULL, NULL, WS_CHILD|WS_VISIBLE, client_rect, pParent, AFX_IDW_PANE_LAST, NULL);

		pWndLC->GetWindowRect(client_rect);
		pWndLC->MoveWindow(	rect.left - client_rect.left,
							rect.top - client_rect.top,
							rect.Width(),
							rect.Height());
	}

	ResetUserCallbacks();
	pWndLC->pCallbacks = &Callbacks;
}

CLumChart::~CLumChart()
{
	if(!pWndLC) return;
	delete pWndLC;	
}

//	UserCallbacks
void CLumChart::SetUserCallback(UINT nEvent, LC_CALLBACK *pCallback)
{
	if(nEvent < LCF_CB_NUM) Callbacks.pFunc[nEvent] = pCallback;
}

int CLumChart::EnableUserCallback(UINT nEvent, bool bEnable)
{
	if(nEvent >= LCF_CB_NUM) return LCF_ERR;
	if(Callbacks.pFunc[nEvent] == NULL) return LCF_ERR;

	if(bEnable)
	{
		Callbacks.nFlags |= 1<<nEvent;
	}
	else
	{
		Callbacks.nFlags &= ~(1<<nEvent);
	}
	return LCF_OK;
}

void CLumChart::ResetUserCallbacks()
{
	Callbacks.nFlags = 0;
	for(int i=0; i<LCF_CB_NUM; i++)
	{
		Callbacks.pFunc[i] = NULL;
	}
}


//	User Menu staff
void CLumChart::SetUserPopupMenu(CWnd *pWnd, CMenu *pMenu)
{
	if(!pWndLC) return;
	pWndLC->SetUserPopupMenu(pWnd, pMenu);
}

void CLumChart::EnableUserPopupMenu(bool bEnable)
{
	if(!pWndLC) return;
	pWndLC->EnableUserPopupMenu(bEnable);
}

//	Window 
int CLumChart::RedrawWindow()
{
	if(!pWndLC) return LCF_ERR;
	pWndLC->RedrawWindow();
	return LCF_OK;
}

CWnd* CLumChart::GetWnd()
{
	return pWndLC;
}

//	Attributes 

//	�������� ���� ���������
int CLumChart::EmptyZoomList()
{
	if(!pWndLC) return LCF_ERR;
	pWndLC->EmptyZoomList();
	return LCF_OK;
}



int CLumChart::ShowLegend(int bShow)
{
	if(!pWndLC) return LCF_ERR;
	bShow ? pWndLC->ShowLegend() : pWndLC->HideLegend();
	pWndLC->RedrawWindow();
	return LCF_OK;
}

int CLumChart::EnableUpperLegend(int bEnable)
{
	if(!pWndLC) return LCF_ERR;
	pWndLC->bLocatorEnable = bEnable;
	return LCF_OK;
}

int CLumChart::EnableCoordLegend(int bEnable)
{
	if(!pWndLC) return LCF_ERR;
	pWndLC->bPifEnable = bEnable;
	return LCF_OK;
}


int CLumChart::ShowGrid(bool bGridHorz, bool bGridVert)
{
	if(!pWndLC) return LCF_ERR;
	pWndLC->bGridHorz = bGridHorz;
	pWndLC->bGridVert = bGridVert;
	pWndLC->RedrawWindow();
	return LCF_OK;
}



int CLumChart::SetAxesNames(CString strX, CString strY)
{
	if(!pWndLC) return LCF_ERR;
	pWndLC->SetAxesName(strX, strY);
//	pWndLC->RedrawWindow();
	return LCF_OK;
}

int CLumChart::SetUserViewPort(double x0, double y0, double x1, double y1)
{
	if(!pWndLC) return LCF_ERR;
	pWndLC->SetUserViewPort(x0, y0, x1, y1);
	return LCF_OK;
}

int CLumChart::SetViewPort(double x0, double y0, double x1, double y1)
{
	if(!pWndLC) return LCF_ERR;
	pWndLC->SetViewPort(x0, y0, x1, y1);
	return LCF_OK;
}

int CLumChart::GetViewPort(double *x0, double *y0, double *x1, double *y1)
{
	if(!pWndLC) return LCF_ERR;
	pWndLC->GetViewPort(x0, y0, x1, y1);
	return LCF_OK;
}

int CLumChart::GetViewPortXY(CPoint point, double *pX, double *pY)
{
	if(!pWndLC) return LCF_ERR;
	pWndLC->GetViewPortXY(point, pX, pY);
	return LCF_OK;
}

int CLumChart::SetSolidAxisHorzAt(double ay)
{
	if(!pWndLC) return LCF_ERR;
	pWndLC->SetSolidAxisHorzAt(ay);
	return LCF_OK;
}

int CLumChart::EnableSolidAxisHorz(bool bEnable)
{
	if(!pWndLC) return LCF_ERR;
	pWndLC->EnableSolidAxisHorz(bEnable);
	return LCF_OK;
}


int CLumChart::SetSolidAxisVertAt(double ax)
{
	if(!pWndLC) return LCF_ERR;
	pWndLC->SetSolidAxisVertAt(ax);
	return LCF_OK;
}

int CLumChart::EnableSolidAxisVert(bool bEnable)
{
	if(!pWndLC) return LCF_ERR;
	pWndLC->EnableSolidAxisVert(bEnable);
	return LCF_OK;
}



//	������ ���������� �� ��� �, � ����� �� ������. �� ��������� = 0.1 
int CLumChart::SetScroll(double fDelta)
{
	if(!pWndLC) return LCF_ERR;
	pWndLC->SetScroll(fDelta);
	return LCF_OK;
}

	//	����
int CLumChart::SetMargins(int mLeft, int mTop, int mRight, int mBottom)
{
	CRect rect(mLeft, mTop, mRight, mBottom);
	if(!pWndLC) return LCF_ERR;
	pWndLC->SetRectMargins(rect);
	return LCF_OK;
}


COLORREF CLumChart::SetBgInsideColor(COLORREF clr)
{
	if(!pWndLC) return LCF_ERR;
	return pWndLC->SetBgInside(clr);
}

COLORREF CLumChart::SetBgOutsideColor(COLORREF clrFill, COLORREF clrBorder)
{
	if(!pWndLC) return LCF_ERR;
	return pWndLC->SetBgOutside(clrFill, clrBorder);
}

int CLumChart::SetHighlightMode(bool mode)
{
	if(!pWndLC) return LCF_ERR;
	pWndLC->SetHighlightMode(mode);
	return LCF_OK;
}

int CLumChart::SetHighlightColor(COLORREF color)
{
	if(!pWndLC) return LCF_ERR;
	pWndLC->SetHighlightColor(color);
	return LCF_OK;
}


POSITION CLumChart::AddGraph(CLumChartDataSet* pGraph, bool bVisible)
{
	if(!pWndLC) return NULL;
	if(!pGraph->GetDataSet())  return NULL;

	if(pGraph->nAttrFlags & LCAF_CONNECT) 
		pGraph->pDataSet->SetInterpolation(pGraph->nConnectType);

//	adding dataset and setting attributes
	POSITION pos = pWndLC->AddPlot(pGraph->GetDataSet());
	pGraph->pos = pos;

	if(pos != NULL)
	{
		if(pGraph->nAttrFlags & LCAF_NAME) 
			pWndLC->SetPlotDescr(pos, pGraph->strName);

		if(pGraph->nAttrFlags & LCAF_FORMAT) 
			pWndLC->SetPlotDataFormat(pos, pGraph->strFmtX, pGraph->strFmtY);

		if(pGraph->nAttrFlags & LCAF_COLOR) 
			pWndLC->SetPlotLineColor(pos, pGraph->nLineColor);
//			pWndLC->SetPlotMarkerColor(pos, pGraph->nLineColor);

		if(pGraph->nAttrFlags & LCAF_WIDTH) 
			pWndLC->SetPlotLineWidth(pos, pGraph->nLineWidth);

		if(pGraph->nAttrFlags & LCAF_STYLE) 
			pWndLC->SetPlotLineStyle(pos, pGraph->nLineStyle);

		if(pGraph->nAttrFlags & LCAF_MTYPE) 
			pWndLC->SetPlotMarkerType(pos, pGraph->nMarkerType);

		if(pGraph->nAttrFlags & LCAF_MSIZE) 
			pWndLC->SetPlotMarkerSize(pos, pGraph->nMarkerSize);

		if(pGraph->nAttrFlags & LCAF_MCOLOR) 
			pWndLC->SetPlotMarkerColor(pos, pGraph->nMarkerColor);

		if(pGraph->nAttrFlags & LCAF_MARRAY) 
			pWndLC->SetPlotUserMarkers(pos, pGraph->pUserMarkers);

		if(pGraph->nAttrFlags & LCAF_MUSER) 
		{
			UINT nFlags = pWndLC->GetPlotFlags(pos);

			if(pGraph->bUserMarkersEnabled)
			{
				nFlags |= LCF_GRAPH_MUSER;
			}
			else
			{
				nFlags &= ~LCF_GRAPH_MUSER;
			}

			pWndLC->SetPlotFlags(pos, nFlags);
		}

		if(!bVisible)
		{
			UINT nFlags = pWndLC->GetPlotFlags(pos);
			nFlags |= LCF_GRAPH_HIDDEN;
			pWndLC->SetPlotFlags(pos, nFlags);
		}

	//	last dataset is the active one
		pWndLC->SetActivePlotPosition(pos);
	}


//	rescaling
	pWndLC->RescaleFull();
	pWndLC->EmptyZoomList();
	return pos;
}


int CLumChart::EnableGraphText(POSITION pos, bool bEnable)
{
	if(!pWndLC) return LCF_ERR;
	UINT nFlags = pWndLC->GetPlotFlags(pos);

	if(!bEnable)
	{
		nFlags &= ~LCF_GRAPH_TEXT;
	}
	else
	{
		nFlags |= LCF_GRAPH_TEXT;
	}

	pWndLC->SetPlotFlags(pos, nFlags);

	return LCF_OK;
}

int CLumChart::ShowGraph(POSITION pos, bool bShow)
{
	if(!pWndLC) return LCF_ERR;
	UINT nFlags = pWndLC->GetPlotFlags(pos);

	if(bShow)
	{
		nFlags &= ~LCF_GRAPH_HIDDEN;
	}
	else
	{
		nFlags |= LCF_GRAPH_HIDDEN;
	}

	pWndLC->SetPlotFlags(pos, nFlags);

//	RedrawWindow();
	return LCF_OK;
}

int CLumChart::IsGraphEnabled(POSITION pos, bool *pbShow)
{
	if(!pWndLC) return LCF_ERR;
	UINT nFlags = pWndLC->GetPlotFlags(pos);

	*pbShow = !(nFlags & LCF_GRAPH_HIDDEN);

	return LCF_OK;
}


//	�������� �������
int CLumChart::RemoveGraph(POSITION pos)
{
	if(!pWndLC) return LCF_ERR;
	pWndLC->RemovePlot(pos);
	return LCF_OK;
}

int CLumChart::RemoveAllGraphs()
{
	if(!pWndLC) return LCF_ERR;
	pWndLC->RemoveAllPlots();
	return LCF_OK;
}

int CLumChart::SetActiveGraph(POSITION pos)
{
	if(!pWndLC) return LCF_ERR;
	pWndLC->SetActivePlotPosition(pos);
	return LCF_OK;
}


//	����� ����������� �������, ���������� � �������� ������
int CLumChart::LocateGraphPoints(POSITION posGraph, CRect rect, int *pnPoints)
{
	if(!pWndLC) return -1;
	return pWndLC->LocateGraphPoints(posGraph, rect, pnPoints);
}

int CLumChart::IsPointInSelRect(CPoint point, int *pnResult)
{
	if(!pWndLC) return LCF_ERR;
	*pnResult = pWndLC->IsPointInSel(point);
	return LCF_OK;
}

int CLumChart::IsGraphInRect(POSITION posGraph, CRect rect, bool *pbResult)
{
	if(!pWndLC) return LCF_ERR;
	*pbResult = pWndLC->IsGraphInRect(posGraph, rect);
	return LCF_OK;
}


//	����� �������
int CLumChart::FindGraph(POSITION pos, CLumChartDataSet *pGraph)
{
	if(!pGraph) return LCF_ERR;
	if(!pWndLC) return LCF_ERR;

	pGraph->pDataSet = pWndLC->GetPlotData(pos);
	if(!pGraph->pDataSet) return LCF_ERR;

	pGraph->pos = pos;

	pGraph->nConnectType = pGraph->pDataSet->GetInterpolation();

	pGraph->strName		= pWndLC->GetPlotDescr(pos);

	pGraph->nLineColor = pWndLC->GetPlotLineColor(pos);
	pGraph->nLineStyle = pWndLC->GetPlotLineStyle(pos);
	pGraph->nLineWidth = pWndLC->GetPlotLineWidth(pos);

	pGraph->nMarkerSize = pWndLC->GetPlotMarkerSize(pos);
	pGraph->nMarkerType = pWndLC->GetPlotMarkerType(pos);
	pGraph->nMarkerColor = pWndLC->GetPlotMarkerColor(pos);

	pGraph->nAttrFlags = LCAF_NONE;

	return LCF_OK;
}

//	���������� �������
int CLumChart::UpdateGraph(POSITION pos, CLumChartDataSet* pGraph, bool bRedraw)
{
	if(!pWndLC) return LCF_ERR;
	if(!pGraph)  return LCF_ERR;
	if(pos == NULL)  return LCF_ERR;

		if(pGraph->nAttrFlags & LCAF_NAME) 
			pWndLC->SetPlotDescr(pos, pGraph->strName);

		if(pGraph->nAttrFlags & LCAF_FORMAT) 
			pWndLC->SetPlotDataFormat(pos, pGraph->strFmtX, pGraph->strFmtY);

		if(pGraph->nAttrFlags & LCAF_COLOR) 
			pWndLC->SetPlotLineColor(pos, pGraph->nLineColor);
//			pWndLC->SetPlotMarkerColor(pos, pGraph->nLineColor);

		if(pGraph->nAttrFlags & LCAF_WIDTH) 
			pWndLC->SetPlotLineWidth(pos, pGraph->nLineWidth);

		if(pGraph->nAttrFlags & LCAF_STYLE) 
			pWndLC->SetPlotLineStyle(pos, pGraph->nLineStyle);

		if(pGraph->nAttrFlags & LCAF_MTYPE) 
			pWndLC->SetPlotMarkerType(pos, pGraph->nMarkerType);

		if(pGraph->nAttrFlags & LCAF_MSIZE) 
			pWndLC->SetPlotMarkerSize(pos, pGraph->nMarkerSize);

		if(pGraph->nAttrFlags & LCAF_MCOLOR) 
			pWndLC->SetPlotMarkerColor(pos, pGraph->nMarkerColor);

		if(pGraph->nAttrFlags & LCAF_MARRAY) 
			pWndLC->SetPlotUserMarkers(pos, pGraph->pUserMarkers);

		if(pGraph->nAttrFlags & LCAF_MUSER) 
		{
			UINT nFlags = pWndLC->GetPlotFlags(pos);

			if(pGraph->bUserMarkersEnabled)
			{
				nFlags |= LCF_GRAPH_MUSER;
			}
			else
			{
				nFlags &= ~LCF_GRAPH_MUSER;
			}

			pWndLC->SetPlotFlags(pos, nFlags);
		}

//	refreshing
	if(bRedraw) pWndLC->RedrawWindow();
	return LCF_OK;
}

//	�������� ������� (�����������)
int CLumChart::SetLegendCaption(CString strCaption)
{
	if(!pWndLC) return LCF_ERR;
	pWndLC->strLegendText = strCaption;
	return LCF_OK;
}

int CLumChart::SetLegendFontName(CString strFontName)
{
	if(!pWndLC) return LCF_ERR;
	pWndLC->strLegendFont = strFontName;
	return LCF_OK;
}

int CLumChart::SetLegendFontSize(UINT nFontSize)
{
	if(!pWndLC) return LCF_ERR;
	pWndLC->nLegendFontSize = nFontSize*10;
	return LCF_OK;
}

int CLumChart::SetLegendPos(CPoint p)
{
	if(!pWndLC) return LCF_ERR;
	pWndLC->ptLegend = p;
	return LCF_OK;
}

int CLumChart::SetGridFontSize(UINT nFontSize)
{
	if(!pWndLC) return LCF_ERR;
	pWndLC->nGridFontSize = nFontSize*10;
	return LCF_OK;
}

int CLumChart::SetSelFrameProps(COLORREF clr, int nWidth)
{
	if(!pWndLC) return LCF_ERR;
	pWndLC->SetSelFrameProps(clr, nWidth);
	return LCF_OK;
}



//	������ �������

int CLumChart::IsLegendEnabled(bool *pbOn)
{
	if(!pWndLC) return LCF_ERR;
	*pbOn = pWndLC->bLegend;
	return LCF_OK;
}

int CLumChart::IsUpperLegendEnabled(bool *pbOn)
{
	if(!pWndLC) return LCF_ERR;
	*pbOn = pWndLC->bLocatorEnable;
	return LCF_OK;
}

int CLumChart::IsCoordLegendEnabled(bool *pbOn)
{
	if(!pWndLC) return LCF_ERR;
	*pbOn = pWndLC->bPifEnable;
	return LCF_OK;
}

int CLumChart::IsGridEnabled(bool *pbHorzOn, bool *pbVertOn)
{
	if(!pWndLC) return LCF_ERR;
	*pbHorzOn = pWndLC->bGridHorz;
	*pbVertOn = pWndLC->bGridVert;
	return LCF_OK;
}


int CLumChart::IsGoForwardAvailable(bool *pbAvailable)
{
	if(!pWndLC) return LCF_ERR;
	*pbAvailable = pWndLC->CanZoomForward();
	return LCF_OK;
}

int CLumChart::IsGoBackAvailable(bool *pbAvailable)
{
	if(!pWndLC) return LCF_ERR;
	*pbAvailable = pWndLC->CanZoomBack();
	return LCF_OK;
}


//	�����������e
POSITION CLumChart::AddMask(double x1, double x2, bool bBorder, bool bForeground, COLORREF color)
{
	LC_MASK mask;
	mask.x1 = x1;
	mask.x2 = x2;
	mask.nFlags = 0;
	if(bBorder) mask.nFlags |= LC_MASK_BORDER;
	if(bForeground)
	{
		mask.nFlags |= LC_MASK_FG;
	}
	mask.color = color;

	if(!pWndLC) return NULL;
	return pWndLC->AddMask(&mask);
}

int CLumChart::RemoveMask(POSITION pos)
{
	if(!pWndLC) return LCF_ERR;
	pWndLC->RemoveMask(pos);
	return LCF_OK;
}

int CLumChart::RemoveAllMasks()
{
	if(!pWndLC) return LCF_ERR;
	pWndLC->EmptyMasksList();
	return LCF_OK;
}

//	���������� �������
POSITION CLumChart::AddUserLegend(CLCUL legend)
{
	if(!pWndLC) return NULL;
	return pWndLC->AddLegend(&legend);
}


CLCUL* CLumChart::GetUserLegend(POSITION pos)
{
	if(!pWndLC) return NULL;
	return pWndLC->GetLegend(pos);
}

int CLumChart::RemoveUserLegend(POSITION pos)
{
	if(!pWndLC) return LCF_ERR;
	pWndLC->RemoveLegend(pos);
	return LCF_OK;
}

int CLumChart::RemoveUserLegends()
{
	if(!pWndLC) return LCF_ERR;
	pWndLC->EmptyLegendsList();
	return LCF_OK;
}

//	������������ ����
int CLumChart::DoScrollLeft()
{
	if(!pWndLC) return LCF_ERR;
	pWndLC->OnXleft();
	return LCF_OK;
}

int CLumChart::DoScrollRight()
{
	if(!pWndLC) return LCF_ERR;
	pWndLC->OnXright();
	return LCF_OK;
}

int CLumChart::DoFullScreen()
{
	if(!pWndLC) return LCF_ERR;
	pWndLC->OnFullScreen();
	return LCF_OK;
}


int CLumChart::DoAutoY()
{
	if(!pWndLC) return LCF_ERR;
	pWndLC->OnAutoY();
	return LCF_OK;
}


int CLumChart::DoPrevScale()
{
	if(!pWndLC) return LCF_ERR;
	pWndLC->OnGoBack();
	return LCF_OK;
}


int CLumChart::DoNextScale()
{
	if(!pWndLC) return LCF_ERR;
	pWndLC->OnGoForward();
	return LCF_OK;
}

int CLumChart::ExportPicture(const TCHAR* strName)
{
	if(!strName) return LCF_ERR;
	if(!pWndLC) return LCF_ERR;
	pWndLC->ExportPicture(strName);
	return LCF_OK;
}



