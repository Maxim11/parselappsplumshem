#include "PbtFile.h"
#include "MatrixUtilities.h"


CPbt::CPbt()
{
	Init();
}

CPbt::CPbt(const char *fname)
{
	Init();
	Load(fname);
}

void CPbt::Init()
{
	nError = 0;
	numberOfComponents = 0;
	pstrComponentName = 0;
}

CPbt::~CPbt()
{
	delete[] pstrComponentName;
}

int CPbt::Load(const char *fname)
{
	FILE *fgh = fopen(fname, "rb");

	if (!fgh)
	{
		nError = -1;
		return nError;
	}

	char strTitle[32];

	fread(strTitle, 1, 32, fgh);

	if (strncmp(strTitle, "Preprocessing before transfer", 29) != 0)
	{
		nError = -2;
		fclose(fgh);
		return nError;
	}
	
	int nVersion;

	fread(&nVersion, 4, 1, fgh);

	fread(&nSpType, 4, 1, fgh);

	int numberOfSamples;

	fread(&numberOfSamples, 4, 1, fgh);

	string strSampleName;

	for (int i = 0; i < numberOfSamples; i++)
	{
		LoadStringPbt(strSampleName, fgh);
	}


	LoadStringPbt(strPrefix, fgh);

	int numSpRegr; // number of spectra for regression

	fread(&numSpRegr, 4, 1, fgh);

	string strSpName;

	// ������ ����� �������� �������
	for (i = 0; i < numSpRegr; i++)
	{
		LoadStringPbt(strSpName, fgh);
	}

	// ������ ����� �������� ������
	for (i = 0; i < numSpRegr; i++)
	{
		LoadStringPbt(strSpName, fgh);
	}

	int nFreq;

	fread(&nFreq, 4, 1, fgh);

	m_FreqScale.nNumberOfFreq = nFreq;

	double fStartFreq;

	fread(&fStartFreq, 8, 1, fgh);

	m_FreqScale.fStartFreq = fStartFreq;

	double fStepFreq;

	fread(&fStepFreq, 8, 1, fgh);

	m_FreqScale.fStepFreq = fStepFreq;

	fread(&numberOfComponents, 4, 1, fgh);

	if (numberOfComponents > 0)
	{
		pstrComponentName = new string[numberOfComponents];

		for (i = 0; i < numberOfComponents; i++)
		{
			LoadStringPbt(pstrComponentName[i], fgh);
		}
	}

	LoadStringPbt(strPreprocPar, fgh);

	LoadRowVector(vMeanSpectrumMSC, fgh);

	LoadRowVector(vMeanStdSpec, fgh);

	LoadRowVector(vMeanSpectrum, fgh);

	m_Correct.Load(fgh, nFreq);

	fclose(fgh);

	return 0;
}

