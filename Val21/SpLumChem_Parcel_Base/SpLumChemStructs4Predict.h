//  SpLumChemStructs4Predict.h
#ifndef _STRUCTSENUMS4PREDICT_H_
#define _STRUCTSENUMS4PREDICT_H_


typedef vector<double> VDbl; // ��� ��������
typedef vector<int> VInt; // ��� ��������
typedef vector<VDbl> MDbl; // ��� ������

// ���� ��������
const int STYPE_TRA = 0; // transmittance
const int STYPE_ABS = 1; // absorbance

// ���� ������������� ������� (���������)
const int MODEL_PLS = 0;
const int MODEL_PCR = 1;
const int MODEL_HSO = 2;
const int MODEL_PCA = 3;
const int MODEL_MLR = 4;

struct CQnt4Predict
{
	bool bFlagTrans; // ���� ��������

// ������������� ��� ��������

	// ������ �� ��������� �����
    double fStartFreqTrans; // ��������� ������� �������
    double fStepFreqTrans;  // ��� �� ������� [cm**(-1)]
	int nNumberOfFreqTrans; // ����� ������ � �������

	int nSpTypeTrans; // ��� ������� (STYPE_TRA ��� STYPE_ABS - transmittance or absorbance)
	string strPreprocParTrans; // ������ ���������� �������������, ��������������� ��� ��������� ��������

	VDbl meanSpectrumTrans;// ������� ������ ��� ��������
	VDbl meanSpectrumMSCTrans; // ������� ������ ��� ����������������� ��������� ��� ��������
	VDbl meanComponentsTrans;	// ������� ������������ ��������� ����. ������
	VDbl meanStdSpecTrans;	// c�������. ���������� ��� �������� ����. ������
	VDbl meanStdCompTrans;	// c�������. ���������� ��� 


// ������������� ��� ���������� ������
	// ������ �� ��������� �����
    double fStartFreq; // ��������� ������� �������
    double fStepFreq;  // ��� �� ������� [cm**(-1)]
	int nNumberOfFreq; // ����� ������ � �������


	int nSpType; // ��� ������� (STYPE_TRA ��� STYPE_ABS - transmittance or absorbance)
	string strPreprocPar; // ������ ���������� �������������
    VInt vFreqExcluded; // ������ ��������� ����������� ����� ������� numFreqExcluded
                         //���� ������� ����������� ����� (������ � 0)

	VDbl meanSpectrum;// ������� ������ ����. ������
	VDbl meanSpectrumMSC; // ������� ������ ��� ����������������� ���������
	VDbl meanComponents;	// ������� ������������ ��������� ����. ������
	VDbl meanStdSpec;	// c�������. ���������� ��� �������� ����. ������
	VDbl meanStdComp;	// c�������. ���������� ��� ��������� ����. ������
	double fMax; // ������. �������� ���. ������ ��� ������������� H

// ������
	int nModelType; // ��� ������
	MDbl mFactors; // ������� ��������
	MDbl mCalibr; // ������������ ������� (��� ������ HSO = �������� �����)
	//MDbl mScores;
	//MDbl mYloadings; 


	// HSO
	int numVar; // ����� ���������� ������
	MDbl mCalibrNonLinear; // ������������� ������� (���������� �����)

// ���������� �������
	VDbl vSEC;
	//VDbl vR2Stat;
	//VDbl vMahalanobisDistances;

// ������� ���������� ������������
	double fMeanMahDist;
};

//��� ��� ��������� ������ ������������� ��� ��������
// � ��� ���������� ������ ��������� ����� �����
// (������ ��� �������� ����������� ����������� ������� 
// vFreqExcluded
// � ������. �������� ���. ������ ��� ������������� 'H'
// fMax)
// �������� ������� ������ ���������

struct CPreprocData
{
	// ������ �� ��������� �����
    double fStartFreq; // ��������� ������� �������
    double fStepFreq;  // ��� �� ������� [cm**(-1)]
	int nNumberOfFreq; // ����� ������ � �������

	int nSpType; // ��� ������� (STYPE_TRA ��� STYPE_ABS)
	string strPreprocPar; // ������ ���������� �������������

	VDbl meanSpectrum;// ������� ������ ��� ��������
	VDbl meanSpectrumMSC; // ������� ������ ��� ����������������� ��������� ��� ��������
	VDbl meanComponents;	// ������� ������������ ��������� ����. ������
	VDbl meanStdSpec;	// c�������. ���������� ��� �������� ����. ������
	VDbl meanStdComp;	// c�������. ���������� ��� 
	double fMax; // ������. �������� ���. ������ ��� ������������� H
}

// � �����
struct CQnt4Predict
{
	bool bFlagTrans; // ���� ��������

// ������������� ��� ��������
	CPreprocData m_PreproDataTrans;

// ������������� ��� ���������� ������
	CPreprocData m_PreproData;

// ������
	int nModelType; // ��� ������
	MDbl mFactors; // ������� ��������
	MDbl mCalibr; // ������������ ������� (��� ������ HSO = �������� �����)

	// HSO
	int numVar; // ����� ���������� ������
	MDbl mCalibrNonLinear; // ������������� ������� (���������� �����)

// ���������� �������
	VDbl vSEC;
	//VDbl vR2Stat;
	//VDbl vMahalanobisDistances; // ���������� ������������ ��� �������� ����. ������
	double fMeanMahDist; // ������� ���������� ������������ ��� �������� ����. ������
};

struct CQlt4Predict
{
	bool bFlagTrans; // ���� ��������

// ������������� ��� ��������
	CPreprocData m_PreproDataTrans;

// ������������� ��� ���������� ������
	CPreprocData m_PreproData;

// ������
	int nModelType; // ��� ������
	MDbl mFactors; // ������� ��������
	//MDbl mScores;


// ���������� �������
	//VDbl vMahalanobisDistances; // ���������� ������������ ��� �������� ����. ������
	double fMeanMahDist; // ������� ���������� ������������ ��� �������� ����. ������
};

// ����� ����, ��������� ����� �������� � ����� �������
// ����� ������� � enum(s)

#endif // _STRUCTSENUMS4PREDICT_H_