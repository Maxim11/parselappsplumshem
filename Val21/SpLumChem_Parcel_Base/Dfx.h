// �����: �. ���������
//
// ��������:
// �������� ������� ��� ������ � ������� saa, spa, ref, lst � apj
//

#ifndef _DFX_H_
#define _DFX_H_

// Get functions
unsigned DfxGetProfileHex(const char *fname, const char *section, const char *key, unsigned uval);

int DfxGetProfileInt(const char *fname, const char *section, const char *key, int ival);

double DfxGetProfileDouble(const char *fname, const char *section, const char *key, double dval);

char* DfxGetProfileString(const char *fname, const char *section, const char *key, char* sval);

const char* DfxGetProfileText(const char *fname, const char *section, const char *key, char* sval, int insert_cr=0);


// Put functions
void DfxPutProfileString(const char *fname, const char *section, const char *key, const char* sval);

void DfxPutProfileText(const char *fname, const char *section, const char *key, const char* sval);

void DfxPutProfileHex(const char *fname, const char *section, const char *key, unsigned uval);

void DfxPutProfileInt(const char *fname, const char *section, const char *key, int ival);

void DfxPutProfileDouble(const char *fname, const char *section, const char *key, double dval);

void DfxPutProfileIntArray(const char *fname, const char *section, const char *key, int isize, int *pdata);

int DfxGetProfileIntArray(const char *fname, const char *section, const char *key, int imaxsize, int *pdata);

// Section functions

int DfxCountProfileSections(const char *fname, const char* substr=0);

int DfxQueryProfileSection(const char *fname, const char *section);

const char* DfxQueryProfileSection(const char *fname, int idx, const char* substr=0);

void DfxRemoveProfileSection(const char *fname, const char *section);

// Common functions

int DfxQueryProfileItem(const char *fname, const char *section, const char *key);

int DfxFileExist(const char *fname);

#endif // _DFX_H_