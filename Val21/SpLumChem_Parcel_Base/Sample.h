#ifndef _SAMPLE_H_
#define _SAMPLE_H_

#include <valarray>
#include <string>

#define dVec valarray<double>

using namespace std;

struct SpecDesc
{
	int fNumberOfFreq; // number of Frequencies
	double fStepFreq;  // frequency step [cm**(-1)]
	double fStartFreq; // start frequency
};


struct Sample
{
public:

	Sample(const string& sname,
		const SpecDesc& desc,
		int numSp,
		const dVec pSp[],
		const string pSpName[],
		int numComp,
		const string pCompName[],
		const dVec& Comp);
	~Sample();

protected:
	string fSampleName; // sample name

	SpecDesc fDesc; // descriptor of spectrum - the same for all spectra
	int fNumberOfSpectra; // number of spectra in the sample
	dVec* fSpectrum; // array of spectra
	string* fSpectrumName; // array of spectra names 

	int fNumberOfComponents; // number of components in the sample
	string* fComponentName; // array of component names
	dVec fComponents; // array of components concentration
};


#endif // _SAMPLE_H_