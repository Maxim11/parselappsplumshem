// ApjSampleLoader.h: interface for the CApjSampleLoaderLoader class.
//
//////////////////////////////////////////////////////////////////////

#ifndef _APJSAMPLELOADEREXT_H_
#define _APJSAMPLELOADEREXT_H_


// extended class of SampleLoader
// with capability of Excluding some spectra

#include <string>

using namespace std;

const int NSAMPLE_SPEC_MAX = 32;


class CApjSampleLoaderExt:public CApjSampleLoader
{
protected:
	int m_nSpectraInc[NSAMPLE_SPEC_MAX];

public:
	CApjSampleLoaderExt(const char* strFileName);
//	virtual ~CApjSampleLoader();

//	CopySample(CApjSampleLoader &sample);

//	int PutDfxProfile(const char *fname, const char *section);
	int GetDfxProfile(const char *fname, const char *section);

//	void SetTextID(const char *pStrID) { if(pStrID) m_strTextID = pStrID;}
//	const char* GetTextID() const {return m_strTextID;}
	
//	void SetFileTitle(const char *pStrTitle) { if(pStrTitle) m_strFileTitle = pStrTitle;}
//	const char* GetFileTitle() const {return m_strFileTitle;}
	

	int GetSpecNum() const { return m_nSpecNum; }
	int GetError() const { return nError; }

//	int GetSpecNext() const {return m_nSpecNext;}

	string GetSampleFileName();
	string GetRefDataFileName();
	string GetSpecFileName(int nIdx);
//	string GetLastSpecFileName() const { return GetSpecFileName(m_nSpecNum-1);}
enum SampleErrors{ ER_OK,
					ER_FILE_OPEN, // can not open sample file
					ER_APJ_NAME, // no title name specifyed in the sample file
					ER_APJ_SPEC }; // number of spectra in the sample = 0
};



#endif // _APJSAMPLELOADER_H_
