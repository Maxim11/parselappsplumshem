// Spectra.h: interface for the CIrSpecRaw and CIrSpectrum classes.
//
// �����: �. ���������
//
// ��������:
// ������ CIrSpecRaw � CIrSpectrum �������������
// ��� ������ � ��-�������� spa-������.
//
// ������������ ��������� ReadSpec (���� ReadSpec.h)
// � LoadSpectrum (����� SampleUtils.h)
//

#ifndef _SPECTRA_H_
#define _SPECTRA_H_

#include <stdio.h>

#include "spline.h"

class CIrSpecRaw
{

public:
	double fmin;
	double fmax;

	double *s;
	int points;
	double GetFmin() {return fmin;}
	double GetFmax() {return fmax;}
	double res;
	double GetFormalRes();
	double GetStandardRes();

	int nacc;
	int napod;
	int nzfill;
	int nfreqeq;

	int nsingle;
	int nphase;

	double amx;
	double fLambda;

	CSpline *spline;

	CIrSpecRaw(CIrSpecRaw &spec);
	CIrSpecRaw(); 
	~CIrSpecRaw(); 

	virtual int Read(const char *fname);
	virtual int ReadHeader(FILE *file);
	virtual int ReadData(FILE *file, double def=0.);

	virtual void Calculate();
	virtual void CalculateSpline();
	void Clear();


	virtual double GetFreq(int i);
	virtual double GetSpecAtFreq(double freq, int nUseSpline=0);
	virtual double GetSpecAtPoint(int i);
	virtual double GetSpecMin(int i1, int i2);
	virtual double GetSpecMax(int i1, int i2);
	virtual int GetIndex(double f);
	int SetFmin(double _fmin);
	int SetFmax(double _fmax);

};



class CIrSpectrum : public CIrSpecRaw
{
protected:
	int absflag;

public:
	int SetAbsFlag(int i);
	int GetAbsFlag() {return absflag;} 
	int FlipAbsFlag() {return SetAbsFlag(!absflag);} 

	CIrSpectrum();
	CIrSpectrum(CIrSpectrum &s);
	CIrSpectrum(CIrSpectrum *sp);


	int Read(const char *fname);

	void Calculate();

	double GetSpecAtPoint(int i);
	int SetSpec(int i, double v);
};

#endif // _SPECTRA_H_