#ifndef _SPLUMCHEM_H_
#define _SPLUMCHEM_H_

#ifdef _IMPORTING_
#define _DLL_API_ __declspec(dllimport)
#else
#define _DLL_API_ __declspec(dllexport)
#endif


#include "SpLumChemStructs.h"

//  --------------------    IRCalibrate   -------------------------------- //
//  
//	interface to IRCalibrate class
//  class to create quantitative calibration
//
// --------------------------------------------------------------------------

// Description:
// Construct a quantitative calibration

// ��� PLS, PCR �������

_DLL_API_ void*  IRCalibrate_Create(vector<CSampleProxy> *pSamples, // array of samples
					const CFreqScale *FreqScale, // ��������� ����� ��������
					const CSpecPreproc *prePar, // preprocessing parameters
					int nModelType, // calibartion model type
					int numFactors,  // number of factors
					CQnt_Parcel **pQnt); // created model

// ��� HSO ������
//_DLL_API_ void*  IRCalibrate_Create(vector<CSampleProxy> *pSamples, // array of samples
//					const CFreqScale *FreqScale, // ��������� ����� ��������
//					const CSpecPreproc *prePar, // preprocessing parameters
//                  const MODEL_MMP *pModelPar, // ��������� ������
//         		 int numPCAfactors, // ����� �������� ��� PCA-������ (���. ������ ��� ������� ���������� ������������)
// 					CQnt_Parcel **pQnt); // created model

_DLL_API_ void*  IRCalibrate_Create(vector<CSampleProxy> *pSamples, // array of samples
					const CFreqScale *FreqScale, // ��������� ����� ��������
					const CSpecPreproc *prePar, // preprocessing parameters
                    const MODEL_MMP *pModelPar,
					CQnt_Parcel **pQnt); // created model



// ��� �����������
// �������� ������
// � ���������� ������������� ������ ���� ������
// �������� � �������������:
// VDbl vCompConcCorrAdd; // ������ ���������� �������� � ������������� ��������� 
// VDbl vCompConcCorrMul; // ������ ����������������� �������� � ������������� ��������� 
// � ������ �������� ���������:
// vector<string> strCompName; // ������ �������� ��������� (�������) �������
// � ��������� CQntOutput ������ ���� ������ �����
// ���������

_DLL_API_ void*  IRCalibrate_Create(vector<CSampleProxy> *pSamples, // array of samples
					const CFreqScale *FreqScale, // ��������� ����� ��������
					const CSpecPreproc *prePar, // preprocessing parameters
					const CQntOutput *pQntOutput); // �������� (��� ����������)


// �������� �������� ��� ���������
_DLL_API_ int IRCalibrate_LoadSamples4SEV(void* pCalib, 
				  vector<CSampleProxy> *pSamples); // array of samples

// ������� ���� ������
_DLL_API_ int IRCalibrate_SetModelType(void* pCalib, 
					int nModelType); // calibartion model type


// ������� ���������� ������� �� �������� (������������� 'A')
_DLL_API_ int IRCalibrate_SetAverage(void* pCalib, 
					bool bAverage); // ���� ����������


// ������� ���� �������
_DLL_API_ int IRCalibrate_SetSpType(void* pCalib, 
					int nSpType); // ��� ������� (STYPE_TRA ��� STYPE_ABS - transmittance or absorbance)

// ������� ���������� ���������
_DLL_API_ int IRCalibrate_SetFreqRange(void* pCalib, 
					double fMin, double fMax); // ��������� ��������


// ������� �������������
_DLL_API_ int IRCalibrate_SetPreproc(void* pCalib, 
					string strPreprocPar); // ������ ���������� �������������

// ������� ����� ��������
_DLL_API_ int IRCalibrate_SetFactors(void* pCalib, 
					int numFactors);  // number of factors


// ��������� ����������� �������
_DLL_API_ int IRCalibrate_GetResults(void* pCalib, 
					CQntOutput *pQntOutput);  // number of factors





// ��� ��� ������������ ������, ����������� � ����:
_DLL_API_ void*  IRCalibrate_Create(vector<CSampleProxy> *pSamples, // array of samples
					const CFreqScale *FreqScale, // ��������� ����� ��������
					const CQnt_Parcel *pQnt); // created model

// ���������� ������ �������� ��� �������� ������ � ������������� ���������
// ������� ������ ��� ���� �������� �� ���������� ��������� 
// ����������������� (������������) ��������
// ����������������� ������� ���������� �� ����� ����������
_DLL_API_ void IRCalibrate_PreCreate(vector<CSampleProxy> *pSamples, // array of samples
					const CFreqScale *FreqScale, // ��������� ����� ��������
					string strPreproc, // ������ ������������� ��� ��������
					const CSpecPreprocData_Parcel *pPreprocData); // ������ ������������� ��� ��������



// Description:
// Destroy the object of IRCalibrate class

_DLL_API_ void IRCalibrate_Destroy(void* pCalib);

// Description:
// Return the number of variables in calibration

_DLL_API_ int IRCalibrate_GetNumVariables(void* pCalib);

// Description:
// Return the number of components in calibration

_DLL_API_ int IRCalibrate_GetNumComponents(void* pCalib); 

// Description:
// Return the number of freedom degrees

_DLL_API_ int IRCalibrate_GetNumFreedomDegrees(void* pCalib);

// Description:
// Return the error of the quantitative calibration

_DLL_API_ int IRCalibrate_GetError(void* pCalib);

// Description:
// Return the SEC (standard error of calibartion) for a given component

//_DLL_API_ int IRCalibrate_GetSEC(void* pCalib,
//								   const char* compName, // component name
//								   double* sec); // SEC for this component



// Description:
// Return the R^2 statistic for a given component

//_DLL_API_ int IRCalibrate_GetR2stat(void* pCalib,
//								   const char* compName, // component name
//								   double* R2stat); // R^2 statistic for this component


// Description:
// Save the quantitative calibration in file

//_DLL_API_ int IRCalibrate_Save(void* pCalib, const char* fileName);

//_DLL_API_ const char* IRCalibrate_GetSampleName(void* pCalib,
//									  int nSample); // sample order number
		

//_DLL_API_ const char* IRCalibrate_GetSampleSpecName(void* pCalib,
//												  int nSample, // sample order number
//												  int nSpec);   // spectrum order number

//_DLL_API_ void* IRCalibrate_Create(const char* cal_file);



// Description:
// Return the Mahalanobis distances for all spectra in the sample

//_DLL_API_ void IRCalibrate_GetSampleDistances(void* pCalib,
//												  int nSample, // sample order number
//											      double* dist); // array of distances to be returned

//_DLL_API_ void IRCalibrate_GetSampleNormDistances(void* pCalib,
//												  int nSample, // sample order number
//											      VDbl vDist); // array of distances to be returned



// Description:
// Return the Mahalanobis distances for mean spectrum of the sample

_DLL_API_ double IRCalibrate_GetSampleDistanceAv(void* pCalib,
												  int nSample); // sample order number

_DLL_API_ double IRCalibrate_GetSampleNormDistanceAv(void* pCalib,
												  int nSample); // sample order number


// Description:
// Return the Mahalanobis distances for the given spectrum in the sample

//_DLL_API_ void IRCalibrate_GetSampleSpecDistance(void* pCalib,
//												  int nSample, // sample order number
//												  int nSpec,   // spectrum order number
//											      double* dist); // the distance to be returned

// Description:
// Return the maximum possible frequency range
// in fMinFreq and fMaxFreq, and Error as result

//_DLL_API_ int  IRCalibrate_GetMaxClbFreqRange(void* pCalib, double *fMinFreq, double *fMaxFreq);

//  ������    IRPredict - ������ ���� IRCalibrate  -------------------------------- //
//
// --------------------------------------------------------------------------

// ���������� ��� �������� �������������� ������

// Description:
// Return the number of components in the model

_DLL_API_ int IRCalibrate_GetNumComponents(void* pCalib);

// Description:
// Get reference data
//_DLL_API_ void IRCalibrate_GetRefConc(void* pCalib,
//									int nSample, // sample order number
//									double* compConc); // array of reference concentrations

_DLL_API_ void IRCalibrate_GetRefConc(void* pCalib,
									int nSample, // sample order number
									VDbl *pvCompConc); // ������ ����������� �������� ������������ (numComp)


// Description:
// Get self-prediction data
//_DLL_API_ void IRCalibrate_GetPredConc(void* pCalib,
//									int nSample, // sample order number
//									int nSpec, // spec order number
//									double* compConc); // array of predicted components concentrations

_DLL_API_ void IRCalibrate_GetPredConc(void* pCalib,
									int nSample, // sample order number
									int nSpec, // spec order number
									VDbl *pvCompConc); // ������ ������������� ������������ (numComp)



// Description:
// Get self-prediction data for mean spectrum of the sample
_DLL_API_ void IRCalibrate_GetPredConcAv(void* pCalib,
									int nSample, // sample order number
									VDbl *pvCompConc); // ������ ������������� ������������ (numComp)


// ������������ ������������ �������
// Description:
// Predict a spectrum

//_DLL_API_ int IRCalibrate_PredictSpectrum(void* pCalib,
//										const CFreqScale *dsc, // spectrum descriptor
//										const VDbl *vSpectrum, // spectrum data
//									double* conc); // the estimated error of predicted concentration

_DLL_API_ int IRCalibrate_PredictSpectrum(void* pCalib,
										const CFreqScale *dsc, // spectrum descriptor
										const VDbl *vSpectrum, // spectrum data
									VDbl *pvCompConc); // ������ ������������� ������������ (numComp)


// ������������ �������� (���������)
// Description:
// Predict several samples and calculate SEV

_DLL_API_ int IRCalibrate_PredictSamples(void* pCalib,
					vector<CSampleProxy> *pProxySamples, // array of samples
					const CFreqScale *FreqScale, // ��������� ����� ��������
					MDbl *pmCompConc, // ������� ������������� ������������ (numSp x numComp)
					VDbl *pvSEV, // ������ SEV [numComp]
					VDbl *pvE,  // ������ ������� ������ (����������) [numComp]
					VDbl *pvSDV,// ������ SDV [numComp]
					VDbl *pvR2); // ������ R2 [numComp]

// Description:
// Return the Mahalanobis distance for a given spectrum


_DLL_API_ double IRCalibrate_GetExtSpecDistance(void* pCalib,
										   const CFreqScale *dsc, // spectrum descriptor
										   const VDbl *vSpectrum); // spectrum data

_DLL_API_ double IRCalibrate_GetExtSpecNormDistance(void* pCalib,
										   const CFreqScale *dsc, // spectrum descriptor
										   const VDbl *vSpectrum); // spectrum data


// ������� ����� ��������
_DLL_API_ int IRCalibrate_SetHarmonics(void* pCalib, 
					int numHarmonics);  // number of harmonics

// ������� ������������ ������������
_DLL_API_ int IRCalibrate_SetCnl(void* pCalib, 
					double fCnl);  // coefficient of non-linearity


// ������� ������� ���������
_DLL_API_ int IRCalibrate_SetShc(void* pCalib, 
					double fShc);  // size of hypercube

// for SEV
_DLL_API_ int CalculateSEVparam(const MDbl *mCompRef, // ������� ����������� �������� ������������ ����������� ������� [numSp x numComp]
                        const MDbl *mCompPred, // ������� ������������� �������� ������������ ����������� ������� [numSp x numComp]
                        int numFact, // ����� �������� ������
                        VDbl *vSEV, // ������ ������� numComp
                        VDbl *vSDV, // ������ ������� numComp
                        VDbl *vR2Sev, // ������ ������� numComp
                        VDbl *vErr); // ������ ������� numComp 



//  --------------------    IRSecv   -------------------------------- //
//  
//	interface to IRSecv class
//  (class to get standard ertror of cross-validation of the model)
//
// --------------------------------------------------------------------------

// Description:
// Construct an object of IRSecv class

_DLL_API_ void* IRSecv_Create(void* pQnt);

//_DLL_API_ void* IRSecv_Create(int numSamples, // number of samples
//					const CSampleProxy* samples, // array of samples
//					const CSpecPreproc& prePar, // preprocessing parameters
//					int nModelType, // calibartion model type (default - PLS)
//					int numFactors = 0); // number of factors

// Description:
// Destroy the object of IRSecv class

_DLL_API_ void IRSecv_Destroy(void* pSECV);

// Description:
// Return the error of cross-validation

_DLL_API_ int IRSecv_GetError(void* pSECV);

// Description:
// Return the number of samples

_DLL_API_ int IRSecv_GetNumSamples(void* pSECV);

// Description:
// Return the number of components

_DLL_API_ int IRSecv_GetNumComponents(void* pSECV);


// Description:
// Return the predicted concentrations for the spectrum of given sample

_DLL_API_ void IRSecv_GetPredConc(void* pSECV,
									int nSample, // sample order number
									int nSpec, // spectrum order number
									VDbl* vCompConc); // vector of predicted component concentrations (numComp)

// Description:
// Return the predicted concentrations for the mean spectrum of given sample

_DLL_API_ void IRSecv_GetPredConcAv(void* pSECV,
									int nSample, // sample order number
									VDbl* vCompConc); // vector of predicted component concentrations (numComp)

// Description:
// Return the refernce concentrations for sample

_DLL_API_ void IRSecv_GetRefConc(void* pSECV,
									int nSample, // sample order number
									VDbl* vCompConc); // vector of reference data for this sample (numComp)


// Description:
// Return the SECV(standard error of cross validation) for a given component

_DLL_API_ int IRSecv_GetSECV(void* pSECV,
							string compName, // the  component name of interest
							double* secv); // SECV for this component

_DLL_API_ int IRSecv_GetSECV(void* pSECV,
							const char* compName, // the  component name of interest
							double* secv); // SECV for this component

// Description:
// Return the R^2 statistic for a given component

_DLL_API_ int IRSecv_GetR2stat(void* pSECV,
								string compName, // component name
								   double* R2stat); // R^2 statistic for this component

// Description:
// Return the F statistic for a given component

_DLL_API_ int IRSecv_GetFstat(void* pSECV,
								string compName, // component name
								   double* Fstat); // F statistic for this component

// Description:
// Give the SEC array for model with excluded given sample
// Return the error code

_DLL_API_ int IRSecv_GetSEC(void* pSECV,
							int nSample, // excluded sample order number
							VDbl* vSEC); // vector of SEC values for this sample (numComp)





//  --------------------    QltAnaliz_PCA   -------------------------------- //
//  
//	interface to QltAnaliz_PCA class
//  class to create qualitative calibration
//
// --------------------------------------------------------------------------

// Description:
// Construct a qualitative calibration

_DLL_API_ void* QltAnaliz_Create(vector<CSampleProxy> *pSamples, // array of samples
					const CFreqScale *FreqScale, // ��������� ����� ��������
					const CSpecPreproc *prePar, // preprocessing parameters
					int numFactors, // number of factors
					CQnt_Parcel **pQnt); // created model

// ��� ��� ������������ ������, ����������� � ����:
//_DLL_API_ void*  QltAnaliz_Create(vector<CSampleProxy> *pSamples, // array of samples
//					const CFreqScale *FreqScale, // ��������� ����� ��������
//					const CQnt_Parcel *pQnt); // created model


// Description:
// Destroy a qualitative calibration

_DLL_API_ void QltAnaliz_Destroy(void* pQlt);

// Description:
// Return the error of the qualitative calibration

_DLL_API_ int  QltAnaliz_GetError(void* pQlt);

// Description:
// Return the number of variables in the qualitative model

_DLL_API_ int  QltAnaliz_GetNumVariables(void* pQlt);


// Description:
// Return the Mahalanobis distances for mean spectrum of the sample

//_DLL_API_ double QltAnaliz_GetSampleDistanceAv(void* pCalib,
//												  int nSample); // sample order number

_DLL_API_ double QltAnaliz_GetSampleNormDistanceAv(void* pCalib,
												  int nSample); // sample order number

// Description:
// Return the Mahalanobis distances for all spectra in the sample

//_DLL_API_ void QltAnaliz_GetSampleDistances(void* pQlt,
//												  int nSample, // sample order number
//											      VDbl *vMahDistances); // array of distances to be returned

_DLL_API_ void QltAnaliz_GetSampleNormDistances(void* pQlt,
												  int nSample, // sample order number
											      VDbl *vNormMahDistances); // array of normalized distances to be returned

// Description:
// Return the Mahalanobis distance for a given spectrum
// (prediction)

//_DLL_API_ double QltAnaliz_GetExtSpecDistance(void* pQlt,
//										const CFreqScale *dsc, // spectrum descriptor
//										const VDbl *vSpectrum); // spectrum data

_DLL_API_ double QltAnaliz_GetExtSpecNormDistance(void* pQlt,
										   const CFreqScale *dsc, // spectrum descriptor
										   const VDbl *vSpectrum); // spectrum data



// Description:
// Return the number of samples in the qualitative model

//_DLL_API_ int  QltAnaliz_GetNumSamples(void* pQlt);

// Return the number of spectra for the specific sample 
//_DLL_API_ int  QltAnaliz_GetNumSampleSpecs(void* pQlt,
//											   int nSample); // sample order number


// Description:
// Construct a qualitative calibration from file

//_DLL_API_ void* QltAnaliz_Load(const char* fileName);

// Description:
// Save the qualitative calibration in file

//_DLL_API_ void QltAnaliz_Save(void* pQlt, const char* fileName);



// Description:
// Return the Mahalanobis distances for the given spectrum in the sample
//
//_DLL_API_ void QltAnaliz_GetSampleSpecDistance(void* pQlt,
//												  int nSample, // sample order number
//												  int nSpec,   // spectrum order number
//											      double* dist); // the distance to be returned


//_DLL_API_ const char* QltAnaliz_GetSampleName(void* pQlt,
//									  int nSample); // sample order number
		

//_DLL_API_ const char* QltAnaliz_GetSampleSpecName(void* pQlt,
//												  int nSample, // sample order number
//												  int nSpec);   // spectrum order number

//_DLL_API_ void QltAnaliz_AddSamples(void *pQlt,
//					int numSamples, // number of samples
//					const CSampleProxy* pSamples, // array of samples
//					const CSpecPreproc &prePar, // preprocessing parameters
//					int numFactors = 0); // number of factors



// old Qlt
// Description:
// Construct a qualitative calibration
//_DLL_API_ void* QltAnaliz_Create(vector<CSampleProxy> *pSamples, // array of samples
//					const CFreqScale *FreqScale, // ��������� ����� ��������
//					const CSpecPreproc *prePar, // preprocessing parameters
//					int numFactors); // number of factors


// Description:
// Construct a qualitative calibration from file

//_DLL_API_ void* QltAnaliz_Load(const char* fileName);


// Description:
// Destroy a qualitative calibration

//_DLL_API_ void QltAnaliz_Destroy(void* pQlt);

// Description:
// Save the qualitative calibration in file

//_DLL_API_ void QltAnaliz_Save(void* pQlt, const char* fileName);

// Description:
// Return the error of the qualitative calibration

//_DLL_API_ int  QltAnaliz_GetError(void* pQlt);

// Description:
// Return the Mahalanobis distance for a given spectrum

//former _DLL_API_ double QltAnaliz_PCA_GetDistance(...
//_DLL_API_ double QltAnaliz_GetExtSpecDistance(void* pQlt,
//										   const CFreqScale& dsc, // spectrum descriptor
//										   const double* data); // spectrum data


//_DLL_API_ double QltAnaliz_GetExtSpecNormDistance(void* pQlt,
//										   const CFreqScale& dsc, // spectrum descriptor
//										   const double* data); // spectrum data

// Description:
// Return the Mahalanobis distances for all spectra in the sample

//_DLL_API_ void QltAnaliz_GetSampleDistances(void* pQlt,
//												  int nSample, // sample order number
//											      double* dist); // array of distances to be returned

//_DLL_API_ void QltAnaliz_GetSampleNormDistances(void* pQlt,
//												  int nSample, // sample order number
//											      double* dist); // array of distances to be returned

// Description:
// Return the Mahalanobis distance for mean spectrum of the sample

//_DLL_API_ double QltAnaliz_GetSampleDistanceAv(void* pQlt,
//												  int nSample); // sample order number


//_DLL_API_ double QltAnaliz_GetSampleNormDistanceAv(void* pQlt,
//												  int nSample); // sample order number





// Description:
// Return the Mahalanobis distances for the given spectrum in the sample

//_DLL_API_ void QltAnaliz_GetSampleSpecDistance(void* pQlt,
//												  int nSample, // sample order number
//												  int nSpec,   // spectrum order number
//											      double* dist); // the distance to be returned

// Description:
// Return the number of samples in the qualitative model

//_DLL_API_ int  QltAnaliz_GetNumSamples(void* pQlt);

// Return the number of spectra for the specific sample 
//_DLL_API_ int  QltAnaliz_GetNumSampleSpecs(void* pQlt,
//											   int nSample); // sample order number

// Description:
// Return the number of variables in the qualitative model

//_DLL_API_ int  QltAnaliz_GetNumVariables(void* pQlt);

//_DLL_API_ const char* QltAnaliz_GetSampleName(void* pQlt,
//									  int nSample); // sample order number
		

//_DLL_API_ const char* QltAnaliz_GetSampleSpecName(void* pQlt,
//												  int nSample, // sample order number
//												  int nSpec);   // spectrum order number

//_DLL_API_ void QltAnaliz_AddSamples(void *pQlt,
//					int numSamples, // number of samples
//					const CSampleProxy* pSamples, // array of samples
//					const CSpecPreproc &prePar, // preprocessing parameters
//					int numFactors = 0); // number of factors



// spectra correction for transfer
// ���������� ��� ������ (0 - ��� ��)
_DLL_API_ int IRTransfer_SpectraCorrection(vector<CSampleProxy> *pSmplsMaster, // array of Master samples
					vector<CSampleProxy> *pSmplsSlave, // array of Slave samples
					const CFreqScale *freqScale, // ��������� ����� ��������
					const CSpecPreproc *prePar, // preprocessing parameters
					double *fDistMin, // ����������� ��������� ����������
					double *fDistMax, // ������������ ��������� ����������
					double *fDistMean); // ������� ��������� ����������

//_DLL_API_ int IRTransfer_SpectraCorrection(
//					vector<CSampleProxy> *pSmplsClbMaster, // array of calibration Master samples
//					vector<CSampleProxy> *pSmplsMaster, // array of Master samples (common)
//					vector<CSampleProxy> *pSmplsSlave, // array of Slave samples (common)
//					const CFreqScale *freqScale, // ��������� ����� ��������
//					int nSpType, // ��� �������
//					vector<string> *prePar, // array of preprocessing strings
//					vector<double> *fDistMin, // ����������� ��������� ����������
//					vector<double> *fDistMax, // ������������ ��������� ����������
//					vector<double> *fDistMean); // ������� ��������� ����������

_DLL_API_ void* SpectraCorrection_Create(vector<CSampleProxy> *pSmplsClbMaster, // array of calibration Master samples
					vector<CSampleProxy> *pSmplsMaster, // array of Master samples (common)
					vector<CSampleProxy> *pSmplsSlave, // array of Slave samples (common)
					const CFreqScale *freqScale); // ��������� ����� ��������

_DLL_API_ int SpectraCorrection_Correct(void* pSpCor,
										string strPreproc); // preprocessing string

_DLL_API_ void SpectraCorrection_GetDistances(void* pSpCor,
										 vector<double> *fDistances, // ������ ���������� ���������� (����� = ����� ��������)
										double *fDistMin, // // ����������� ��������� ����������
										double *fDistMax, // ������������ ��������� ����������
										double *fDistMean); // ������� ��������� ����������

_DLL_API_ void SpectraCorrection_GetCorrectedSpectra(void* pSpCor, vector<CSampleProxy> *pSmplsClbMaster); // array of calibration Master samples to be corrected

_DLL_API_ void SpectraCorrection_GetPreprocData(void* pSpCor, CSpecPreprocData_Parcel *pPreprocData);

_DLL_API_ void SpectraCorrection_Destroy(void* pSpCor);

// Utilities
// ������� �� TRA � ABS:
// if (x > 1e-14) TRA2ABS(x) = -log10(x);
// else TRA2ABS(x) = x;
_DLL_API_ void TRA2ABS(const VDbl *vSpectrumIN, 
					VDbl *vSpectrumOUT);

// ������� �� ABS � TRA:
// if (x > 1e-14) ABS2TRA(x) = pow(10., -x);
// else ABS2TRA(x) = 0;

_DLL_API_ void ABS2TRA(const VDbl *pvSpectrumIN, 
					VDbl *pvSpectrumOUT);


#endif // _SPLUMCHEM_H_
