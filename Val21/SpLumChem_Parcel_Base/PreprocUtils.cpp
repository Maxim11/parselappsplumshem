#include "PreprocUtils.h"
#include "MatrixUtilities.h"

#include <math.h>

extern void AppendToDebugReport(const char* str);

//void CalculateMeanRow(const Matrix& mX, RowVector& 
void MeanCentering(const Matrix& mX, Matrix& mXpreproc, RowVector& vXmean)
{
	// mean center the matrix according rows
	// preproc paramater 'M'

	int numCols = mX.Ncols();
	int numRows = mX.Nrows();

	mXpreproc.ReSize(numCols, numRows);
	vXmean.ReSize(numCols);
	vXmean = 0.0;

	for (int i = 1; i <= numRows; i++)
	{
		vXmean += mX.Row(i);
	}

	vXmean /= numRows;

	for (int i = 1; i <= numRows; i++)
	{
		mXpreproc.Row(i) = mX.Row(i) - vXmean;
	}
}

void MeanCentering(Matrix& mX, RowVector& vXmean)
{
	// mean center the matrix according rows
	// preproc paramater 'M'


	int numCols = mX.Ncols();
	int numRows = mX.Nrows();

	vXmean.ReSize(numCols);
	vXmean = 0.0;

	for (int i = 1; i <= numRows; i++)
	{
		vXmean += mX.Row(i);
	}

	vXmean /= numRows;

	for (int i = 1; i <= numRows; i++)
	{
		mX.Row(i) -= vXmean;
	}
}

void ABS(Matrix& mX)
{
	int numCols = mX.Ncols();
	int numRows = mX.Nrows();

	const double minx = 1.0e-14;

	for (int i = 1; i <= numRows; i++)
	{
		for (int j = 1; j <= numCols; j++)
		{
			if (mX(i, j) > minx)
			{
				mX(i, j) = -log10(mX(i, j));
			}
		}
	}
}




int DeviationScaling(const Matrix& mX, Matrix& mXpreproc, RowVector& vXdev)
{
	// deviation scaling the matrix according colums
	// preproc paramater 'D'

	int nError = 0;

	int numCols = mX.Ncols();
	int numRows = mX.Nrows();

	mXpreproc.ReSize(numCols, numRows);
	vXdev.ReSize(numCols);

	RowVector vXmean;
	vXmean.ReSize(numCols);
	vXmean = 0.0;

	for (int j = 1; j <= numRows; j++)
	{
		vXmean += mX.Row(j);
	}

	vXmean /= numRows;



	for (int i = 1; i <= numCols; i++)
	{
		vXdev(i) = (mX.Column(i) - vXmean(i)).SumSquare();
		vXdev(i) = sqrt(vXdev(i));
	}

	if (numRows > 1)
	{
		vXdev /= sqrt(numRows - 1.0);
	}

	const double eps = 1e-13;

	for (int i = 1; i <= numCols; i++)
	{
		if (vXdev(i) < eps)
		{
			nError = -1;
			break;
		}
		else
		{
			mXpreproc.Column(i) = mX.Column(i) / vXdev(i);
		}
	}

	return nError;
}


int DeviationScaling(Matrix& mX, RowVector& vXdev)
{
	// deviation scaling the matrix according colums
	// preproc paramater 'D'

	int nError = 0;

	int numCols = mX.Ncols();
	int numRows = mX.Nrows();

	vXdev.ReSize(numCols);

	RowVector vXmean;
	vXmean.ReSize(numCols);
	vXmean = 0.0;

	for (int j = 1; j <= numRows; j++)
	{
		vXmean += mX.Row(j);
	}

	vXmean /= numRows;

	//PrintMatrix(mX, "mX.dat");
	//PrintMatrix(vXmean, "vXmean.dat");



	for (int i = 1; i <= numCols; i++)
	{
		vXdev(i) = (mX.Column(i) - vXmean(i)).SumSquare();
		vXdev(i) = sqrt(vXdev(i));
	}

	if (numRows > 1)
	{
		vXdev /= sqrt(numRows - 1.0);
	}

	const double eps = 1e-13;

	for (int i = 1; i <= numCols; i++)
	{
		if (vXdev(i) < eps)
		{
			nError = -1;
			break;
		}
		else
		{
			mX.Column(i) /= vXdev(i);
		}
	}

	return nError;
}

//void MultScatterCorrection(const Matrix& mX, Matrix& mXpreproc, RowVector& vXdev)
//{
	// multiplicative scatter correction
	// preproc paramater 'C'
//}

int SNVtransformation(const Matrix& mX, Matrix& mXpreproc, ColumnVector& vXdev, ColumnVector& vXmean)
{
	// standard normal variate transformation (SNV) and detrending
	// // preproc paramater 'N'

	int nError = 0;

	int numCols = mX.Ncols();
	int numRows = mX.Nrows();

	mXpreproc.ReSize(numCols, numRows);
	vXmean.ReSize(numRows);
	vXmean = 0.0;

	for (int i = 1; i <= numCols; i++)
	{
		vXmean += mX.Column(i);
	}

	vXmean /= numCols;

	for (int i = 1; i <= numCols; i++)
	{
		mXpreproc.Column(i) = mX.Column(i) - vXmean;
	}

	vXdev.ReSize(numRows);

	for (int i = 1; i <= numRows; i++)
	{
		vXdev(i) = (mX.Row(i)).SumSquare();
		vXdev(i) = sqrt(vXdev(i));
	}

	if (numCols > 1)
	{
		vXdev /= sqrt(numCols - 1.0);
	}

	const double eps = 1e-13;


	for (int i = 1; i <= numRows; i++)
	{
		if (vXdev(i) < eps)
		{
			nError = -1;
			break;
		}
		else
		{
			mXpreproc.Row(i) = mXpreproc.Row(i) / vXdev(i);
		}
	}

	return nError;
}


int SNVtransformation(Matrix& mX, ColumnVector& vXdev, ColumnVector& vXmean)
{
	// standard normal variate transformation (SNV) and detrending
	// // preproc paramater 'C'

	int nError = 0;

	int numCols = mX.Ncols();
	int numRows = mX.Nrows();

	vXmean.ReSize(numRows);
	vXmean = 0.0;

	for (int i = 1; i <= numCols; i++)
	{
		vXmean += mX.Column(i);
	}

	vXmean /= numCols;

	for (int i = 1; i <= numCols; i++)
	{
		mX.Column(i) -= vXmean;
	}

	vXdev.ReSize(numRows);

	for (int i = 1; i <= numRows; i++)
	{
		vXdev(i) = (mX.Row(i)).SumSquare();
		vXdev(i) = sqrt(vXdev(i));
	}

	if (numCols > 1)
	{
		vXdev /= sqrt(numCols - 1.0);
	}

	const double eps = 1e-13;

	for (int i = 1; i <= numRows; i++)
	{
		if (vXdev(i) < eps)
		{
			nError = -1;
			break;
		}
		else
		{
			mX.Row(i) /= vXdev(i);
		}
	}

	return nError;
}

int BaseLineCor(Matrix& mX)
{
	int numRows = mX.Nrows();
	int numCols = mX.Ncols();

	if (numCols <= 3)
	{
		return ER_PREPROC_SP;
	}

//	ColumnVector vMean(numRows), vMeanI(numRows), vMeanI2(numRows);

	RowVector vI(numCols), vI2(numCols);

	for (int j = 1; j <= numCols; j++)
	{
		vI(j) = j;
	}

	vI2 = SP(vI, vI);

	Matrix mI(3, numCols);

	mI.Row(1) = 1.0;
	mI.Row(2) = vI;
	mI.Row(3) = vI2;

	Matrix mMean(numRows, 3);

	mMean = mX * mI.t();

	double n = numCols;

	mMean /= n;

/*	for (int i = 1; i <= numRows; i++)
	{
		vMean(i) = (mX.Row(i)).Sum();
		vMeanI(i) = (SP(mX.Row(i), vI)).Sum();
		vMeanI2(i) = (SP(mX.Row(i), vI2)).Sum();
	}*/

/*	Matrix mS(3, 3);


	double s, si, si2, si3, si4;

	s = n;
	si = n * (n + 1);
	si /= 2.0;
	si2 = n * (n + 1) * (2 * n + 1);
	si2 /= 6.0;
	si3 = n * (n + 1);
	si3 *= si3;
	si3 /= 4.0;
	si4 = n * (n + 1) * (2 * n + 1);
	si4 *= (3 * n * n + 3 * n - 1) / 30.0;




	mS(1, 1) = s;
	mS(2, 1) = si;
	mS(3, 1) = si2;

	mS(1, 2) = si;
	mS(2, 2) = si2;
	mS(3, 2) = si3;

	mS(1, 3) = si2;
	mS(2, 3) = si3;
	mS(3, 3) = si4;

	mSinv = mS.i();*/


	Matrix mSinv(3, 3);

	double tmp = (n - 1) * (n - 2);

	mSinv(1, 1) = 3 * (3 * n * n + 3 * n + 2) / tmp;

	mSinv(1, 2) = -18 * (2 * n + 1) / tmp;

	mSinv(2, 1) = mSinv(1, 2);

	mSinv(1, 3) = 30 / tmp;

	mSinv(3, 1) = mSinv(1, 3);

	tmp = (n * n - 1) * (n * n - 4);

	mSinv(2, 2) = 12 * (2 * n + 1) * (8 * n + 11) / tmp;

	mSinv(2, 3) = -180 * (n + 1) / tmp;

	mSinv(3, 2) = mSinv(2, 3);

	mSinv(3, 3) = 180 / tmp;




/*	mMean.Column(1) = vMean;
	mMean.Column(2) = vMeanI;
	mMean.Column(3) = vMeanI2;*/

	Matrix mABC(numRows, 3);

	mABC = mMean * mSinv;

/*	PrintMatrix(mX, "mX.dat");

	PrintMatrix(mI, "mI.dat");

	PrintMatrix(mABC, "mABC.dat");

	PrintMatrix(mX, "mX2.dat");*/


/*	Matrix mI(3, numCols);

	mI.Row(1) = 1.0;
	mI.Row(2) = vI;
	mI.Row(3) = vI2;*/

	mX -= mABC * mI;

/*	PrintMatrix(mX, "mX2.dat");*/

	return ER_OK;
}


int PrepSpForPrediction(const CFreqScale *freqScale,
							const CSpecPreprocData& specPreprocData,
							const Matrix& mSp,
							Matrix& mSpPrep, bool bOptim)
{
	// bOptim - ���� �����������.
	// ��� ����������� ������� ��� ��������� � ������� �������� mSp
	int nError = ER_OK;
	int iStart, iEnd;

	//specPreprocData.Print("specPreprocData");

	if (bOptim)
	{
		iStart = FindIndex(specPreprocData.fMinFreq, *freqScale, specPreprocData, true);
		iEnd = FindIndex(specPreprocData.fMaxFreq, *freqScale, specPreprocData, false);
		mSpPrep = mSp.Columns(iStart + 1, iEnd + 1);
	}
	else
	{
		int iStart, iEnd;

		double tmp;

		tmp = (specPreprocData.fMinFreq - freqScale->fStartFreq) / freqScale->fStepFreq;
		iStart = (int)floor(tmp);
		if (tmp - iStart > 0.5) iStart++;
		
		tmp = (specPreprocData.fMaxFreq - freqScale->fStartFreq) / freqScale->fStepFreq;
		iEnd = (int)floor(tmp);
		if (tmp - iEnd > 0.5) iEnd++;
		iEnd++;

		//char strBuf[128];
		//sprintf(strBuf, "iStart=%d iEnd=%d", iStart, iEnd);
		//AppendToDebugReport(strBuf);


		if (iEnd >= freqScale->nNumberOfFreq) iEnd = freqScale->nNumberOfFreq;

		if ((iStart < 0) || (iEnd < 0) || (iStart > iEnd))
		{
			nError = ER_FREQ_RANGE;
			//AppendToDebugReport("Error freq range");
			return nError;
		}

		mSpPrep = mSp.Columns(iStart + 1, iEnd);

		if ((specPreprocData.nModelType != MODEL_HSO) && (specPreprocData.numberOfExcludedFreq > 0))
		{
			ExcludeColumns(mSpPrep, specPreprocData.numberOfExcludedFreq, specPreprocData.vUseFreq);
		}
	}


//	sprintf(strBuf, "mSpPrep.Ncols()=%d", mSpPrep.Ncols());
//	AppendToDebugReport(strBuf);


//	specPreprocData.Print("specPreprocData.txt");
//	(*freqScale).Print("freqScale.txt");

	if (mSpPrep.Ncols() != specPreprocData.numberOfChannels)
	{
		nError = ER_FREQ_RANGE;
		return nError;
	}




	if (specPreprocData.nSpType == STYPE_ABS)
	{
		ABS(mSpPrep);
	}

//	PrintMatrix(mSpPrep.Row(1), "mSpectra_1.dat");

	for (int iP = 0; iP < specPreprocData.strPreprocPar.length(); iP++)
	{
		int iSp, j;
		ColumnVector vMeanXf, vDevXf;

		switch(specPreprocData.strPreprocPar[iP])
		{
			case 'm':
			case 'M': // ����������������:

				for (iSp = 1; iSp <= mSpPrep.Nrows(); iSp++)
				{
					mSpPrep.Row(iSp) -= specPreprocData.meanSpectrum;
				}

			break;
			case 'd':
			case 'D':

				for (j = 1; j <= mSpPrep.Ncols(); j++)
				{
					mSpPrep.Column(j) /= specPreprocData.meanStdSpec(j);
				}

				break;
			case 'n':
			case 'N':

				SNVtransformation(mSpPrep, vMeanXf, vDevXf);
				break;
			case 'c':
			case 'C':
				nError = MultCorrectionForPrediction(mSpPrep, specPreprocData.meanSpectrumMSC);
				break;
			case 'b':
			case 'B':
				BaseLineCor(mSpPrep);
				break;
			case '1':
				Derivate(mSpPrep, freqScale->fStepFreq, 10, 4, 1);
				break;
			case '2':
				Derivate(mSpPrep, freqScale->fStepFreq, 10, 4, 2);
				break;
			case 'h':
			case 'H': // ������������ �� ��������:
				mSpPrep /= specPreprocData.fMax;
				break;
		}
	}

	return nError;
}


//void PrepCompForPrediction(const CSpecPreprocData& specPreprocData,
//								const Matrix& mComp,
//								Matrix& mCompPrep)
//{
//	mCompPrep = mComp;

//	for (int iP = 0; iP < specPreprocData.strPreprocPar.length(); iP++)
//	{
//		int iSp;
//		int j;

//		switch(specPreprocData.strPreprocPar[iP])
//		{
//			case 'm':
//			case 'M': // ����������������:
//
//				for (iSp = 1; iSp <= mComp.Nrows(); iSp++)
//				{
//					mCompPrep.Row(iSp) -= specPreprocData.meanComponents;
//				}
//			break;
//			case 'd':
//			case 'D':
//				for (j = 1; j <= mCompPrep.Ncols(); j++)
//				{
//					mCompPrep.Column(j) /= specPreprocData.meanStdComp(j);
//				}
//			break;
//			default:
//				break;
//		}
//	}
//}

int PostProcessComponents(Matrix& mY, const CSpecPreprocData& specPreprocData, bool bCor)
{
	// ��������� ������������ �����������
	// ����� bCor=true (������ ��� ������������ ������� � SEV)
	int nError = ER_OK;

//d
//	AppendToDebugReport("Start post processing");

	// const preprocessing
	for (int iP = specPreprocData.strPreprocParTrans.length() - 1; iP >= 0; iP--)
	{
		int iSp;
//		int j;

		switch(specPreprocData.strPreprocParTrans[iP])
		{
			case 'm':
			case 'M': // ����������������:

				for (iSp = 1; iSp <= mY.Nrows(); iSp++)
				{
					mY.Row(iSp) += specPreprocData.meanComponentsTrans;
				}
			break;
/*			case 'd':
			case 'D':
				for (j = 1; j <= mY.Ncols(); j++)
				{
					mY.Column(j) *= specPreprocData.meanStdComp(j);
				}
			break;*/
			default:
				break;
		}
	}

	for (int iP = specPreprocData.strPreprocPar.length() - 1; iP >= 0; iP--)
	{
		int iSp;
		int j;

		switch(specPreprocData.strPreprocPar[iP])
		{
			case 'm':
			case 'M': // ����������������:

				for (iSp = 1; iSp <= mY.Nrows(); iSp++)
				{
					mY.Row(iSp) += specPreprocData.meanComponents;
				}
			break;
			case 'd':
			case 'D':
//d
//		AppendToDebugReport("Post process D");

				for (j = 1; j <= mY.Ncols(); j++)
				{
					mY.Column(j) *= specPreprocData.meanStdComp(j);
				}
			break;
			default:
				break;
		}
	}

//	if (bCor)
//	{
//		for (int j = 1; j <= mY.Ncols(); j++)
//		{
//			mY.Column(j) *= specPreprocData.vComponentCorrectionsMul(j);
//			mY.Column(j) += specPreprocData.vComponentCorrectionsAdd(j);
//		}
//	}

	return nError;
}

int PrepSpForPrediction(const CSpecPreprocData& specPreprocData,
						const Matrix& mSp,
						Matrix& mSpPrep, double fStepFreq)
{

	// without frequency adjustment

	int nError = ER_OK;

	mSpPrep = mSp;

//���������� ������ ��� PLS � PCR �������
	
	if ((specPreprocData.nModelType != MODEL_HSO) && (specPreprocData.numberOfExcludedFreq > 0))
	{
		ExcludeColumns(mSpPrep, specPreprocData.numberOfExcludedFreq, specPreprocData.vUseFreq);
	}

	if (specPreprocData.nSpType == STYPE_ABS)
	{
		ABS(mSpPrep);
	}


	for (int iP = 0; iP < specPreprocData.strPreprocPar.length(); iP++)
	{
		int iSp, j;
		ColumnVector vMeanXf, vDevXf;

		switch(specPreprocData.strPreprocPar[iP])
		{
			case 'm':
			case 'M': // ����������������:

				for (iSp = 1; iSp <= mSpPrep.Nrows(); iSp++)
				{
					mSpPrep.Row(iSp) -= specPreprocData.meanSpectrum;
				}

			break;
			case 'd':
			case 'D':

				for (j = 1; j <= mSpPrep.Ncols(); j++)
				{
					mSpPrep.Column(j) /= specPreprocData.meanStdSpec(j);
				}

				break;
			case 'n':
			case 'N':

				SNVtransformation(mSpPrep, vMeanXf, vDevXf);
				break;
			case 'c':
			case 'C':
				nError = MultCorrectionForPrediction(mSpPrep, specPreprocData.meanSpectrumMSC);
				break;
			case 'b':
			case 'B':
				BaseLineCor(mSpPrep);
				break;
			case '1':
				Derivate(mSpPrep, fStepFreq, 10, 4, 1);
				break;
			case '2':
				Derivate(mSpPrep, fStepFreq, 10, 4, 2);
				break;
			case 'h':
			case 'H': // ������������ �� ��������:
				mSpPrep /= specPreprocData.fMax;
				break;
		}
	}

	return nError;
}



int Derivate(Matrix& mX,  double fStepX, int nWinSize, int nPolOrder, int nDerivOrder)
{
	// nWinSize - size of smoothing window
	// nPolOrder - odder of smoothing polynom
	// nDerivOrder - 1 or 2
// source: "Numerical Recipes in C", 14.8 Savitzky-Golay Smoothing Filters

	if (mX.Ncols() < 2 * nWinSize + 1)
	{
		return ER_DERIV1;
	}
	
	Matrix mA(2 * nWinSize + 1, nPolOrder + 1);

	mA.Column(1) = 1.0;

	ColumnVector vI(2 * nWinSize + 1);

	for (int i = -nWinSize; i <= nWinSize; i++)
	{
		vI(i + nWinSize + 1) = i;
	}

	for (int j = 1; j <= nPolOrder; j++)
	{
		mA.Column(j + 1) = SP(mA.Column(j), vI);
	}

//	PrintMatrix(mA, "mA.dat");

	Matrix mS;

	mS = (mA.t() * mA).i(); // (nPolOrder + 1) x (nPolOrder + 1)

	mA *= (mS.Row(nDerivOrder + 1)).t();

//	PrintMatrix(mS, "mS.dat");

//	PrintMatrix(mS * (mA.t() * mA), "mSmAtA.dat");

	int numCols, numRows;

	numRows = mX.Nrows();
	numCols = mX.Ncols();

	Matrix mXwin(numRows, 2 * nWinSize + 1);

//	mXwin = 0.0;

//	RowVector vDeriv(mX.Nrows());

	Matrix mXext(numRows, numCols + 2 * nWinSize);

	for (int iC = 1; iC <= nWinSize; iC++)
	{
		mXext.Column(iC) = mX.Column(1);
		mXext.Column(numCols + nWinSize + iC) = mX.Column(numCols);
	}

	mXext.Columns(nWinSize + 1, nWinSize + numCols) = mX;

	for (int iC = 1; iC <= numCols; iC++)
	{
		mXwin = mXext.Columns(iC, iC + 2 * nWinSize);
		mX.Column(iC) = mXwin * mA;
	}


	double fScale = 1;

	for (int iD = 0; iD < nDerivOrder; iD++)
	{
		fScale *= (iD + 1.0);
		fScale /= fStepX;
	}

	mX *= fScale;

	return ER_OK;
}


int MultCorrection(Matrix& mX, RowVector& vXmean)
{
	// multiplicative scatter correction
	// preproc paramater 'C'

	int nError = 0;

	int numCols = mX.Ncols();
	int numRows = mX.Nrows();

	vXmean.ReSize(numCols);
	vXmean = 0.0;

	for (int j = 1; j <= numRows; j++)
	{
		vXmean += mX.Row(j);
	}

	vXmean /= numRows;

	double mean, std;

	mean = vXmean.Sum();

	const double eps = 1e-13;

	if (vXmean.MaximumAbsoluteValue() < eps)
	{
		nError = -1;
//		AppendToDebugReport("mean spectrum = 0");
		return nError;
	}

	//mean /= numCols;

	std = vXmean.SumSquare();

	if (fabs(std - mean * mean / numCols) < eps)
	{
		nError = -1;
//		AppendToDebugReport("mean spectrum = const (SDV=0)");
		return nError;
	}

	ColumnVector vM(numRows), vB(numRows);

	for (int j = 1; j <= numRows; j++)
	{
		double s, s2;
		double sm;

		s2 = (mX.Row(j)).SumSquare();
		s = (mX.Row(j)).Sum();
		sm = (mX.Row(j) * vXmean.t()).AsScalar();

		vM(j) = (sm - s * mean / numCols) / (std - mean * mean / numCols);
		vB(j) = (s - vM(j) * mean) / numCols;
	}

	for (int j = 1; j <= numRows; j++)
	{
		mX.Row(j) -= vB(j);

		if (fabs(vM(j)) < eps)
		{
			nError = -1;
			break;
		}
		mX.Row(j) /= vM(j);
	}

	return nError;
}

int MultCorrectionForPrediction(Matrix& mX, const RowVector& vXmean)
{
	// multiplicative scatter correction
	// preproc paramater 'C'
	// variant for prediction
	int nError = 0;

	int numCols = mX.Ncols();
	int numRows = mX.Nrows();


	if (numCols != vXmean.Ncols())
	{
		return -1;
	}

	double mean, std;

	mean = vXmean.Sum();
	//mean /= numCols;

	std = vXmean.SumSquare();

	ColumnVector vM(numRows), vB(numRows);

	for (int j = 1; j <= numRows; j++)
	{
		double s, s2;
		double sm;

		s2 = (mX.Row(j)).SumSquare();
		s = (mX.Row(j)).Sum();
		sm = (mX.Row(j) * vXmean.t()).AsScalar();

		vM(j) = (sm - s * mean / numCols) / (std - mean * mean / numCols);
		vB(j) = (s - vM(j) * mean) / numCols;
	}

	const double eps = 1e-13;

	for (int j = 1; j <= numRows; j++)
	{
		mX.Row(j) -= vB(j);

		if (fabs(vM(j)) < eps)
		{
			nError = -1;
			break;
		}
		mX.Row(j) /= vM(j);
	}

	return nError;
}

void ExcludeColumns(Matrix &mX, int numberOfFreqExcluded, const VInt &vUseFreq)
{
	if (numberOfFreqExcluded > 0)
	{
		int iFinc, iFexcl;
		iFinc = iFexcl = 0;

		int numCols = mX.Ncols();
		int numRows = mX.Nrows();

		int iF = 0;

		Matrix mTmp;

		mTmp.ReSize(numRows, numCols - numberOfFreqExcluded);

		int iCol = 1;

		for (int i = 0; i < vUseFreq.size(); i++)
		{
			if (vUseFreq[i] == 1)
			{
				mTmp.Column(iCol) = mX.Column(i+1);
				iCol++;
			}
		}

		mX.CleanUp();
		mX = mTmp;
	}
}


/*int PreprocessSpectra(CSpecPreprocData& specPreprocData,
					  double fStepFreq,
				const Matrix& mSp,
				Matrix& mX)
{
	int nErrPreproc;

	mX = mSp;

	for (int iP = 0; iP < specPreprocData.strPreprocPar.length(); iP++)
	{
		ColumnVector vMeanXf, vDevXf;

		const int nWidth = 10; // window size for derivation
		const int nPoly = 4; // polynome power for derivation


		switch(specPreprocData.strPreprocPar[iP])
		{
			case 'm':
			case 'M': // ����������������:
				MeanCentering(mX, specPreprocData.meanSpectrum);

				break;
			case 'd':
			case 'D':
				nErrPreproc = DeviationScaling(mX, specPreprocData.meanStdSpec);

				if (nErrPreproc != 0)
				{
					_snprintf(strErrorMessage, MAX_ERR_MSG, "Error doing deviation scaling");
					return ER_PREPROC_SP;
				}

				break;
			case 'n':
			case 'N':
				nErrPreproc = SNVtransformation(mX, vMeanXf, vDevXf);

				if (nErrPreproc != 0)
				{
					_snprintf(strErrorMessage, MAX_ERR_MSG, "Error doing standard normal variate transformation (SNV) and detrending");
					return ER_PREPROC_SP;
				}

				break;
			case 'c':
			case 'C':
				nErrPreproc = MultCorrection(mX, specPreprocData.meanSpectrumMSC);
				if (nErrPreproc != 0)
				{
					_snprintf(strErrorMessage, MAX_ERR_MSG, "Error doing multiplicative scatter correction");
					return ER_PREPROC_SP;
				}
				break;
			case 'b':
			case 'B':
				BaseLineCor(mX);
				break;
			case '1':
				if (specPreprocData.numberOfChannels < 2 * nWidth + 1)
				{
					_snprintf(strErrorMessage, MAX_ERR_MSG, "Error derivative: too few points in spectra");
					return ER_DERIV1;
				}
				else
				{
					Derivate(mX, fStepFreq, nWidth, nPoly, 1);
				}
				break;
			case '2':
				if (specPreprocData.numberOfChannels < 2 * nWidth + 1)
				{
					_snprintf(strErrorMessage, MAX_ERR_MSG, "Error derivative: too few points in spectra");
					return ER_DERIV1;
				}
				else
				{
					Derivate(mX, fStepFreq, nWidth, nPoly, 2);
				}
				break;
			case 'h':
			case 'H': // ������������ �� ��������:
				NormMax(mX, specPreprocData.fMax);
				break;
			default:
				break;
		}
	}

	return nErrPreproc;
}*/

int PreprocessSpectra(CSpecPreprocData& specPreprocData,
			  double fStepFreq,
			Matrix& mX)
{
	int nErrPreproc = ER_OK;

	for (int iP = 0; iP < specPreprocData.strPreprocPar.length(); iP++)
	{
		ColumnVector vMeanXf, vDevXf;

		const int nWidth = 10; // window size for derivation
		const int nPoly = 4; // polynome power for derivation


		switch(specPreprocData.strPreprocPar[iP])
		{
			case 'm':
			case 'M': // ����������������:
				MeanCentering(mX, specPreprocData.meanSpectrum);

				break;
			case 'd':
			case 'D':
				nErrPreproc = DeviationScaling(mX, specPreprocData.meanStdSpec);

				if (nErrPreproc != 0)
				{
					_snprintf(strErrorMessage, MAX_ERR_MSG, "Error doing deviation scaling");
					return ER_PREPROC_SP;
				}

				break;
			case 'n':
			case 'N':
				nErrPreproc = SNVtransformation(mX, vMeanXf, vDevXf);

				if (nErrPreproc != 0)
				{
					_snprintf(strErrorMessage, MAX_ERR_MSG, "Error doing standard normal variate transformation (SNV) and detrending");
					return ER_PREPROC_SP;
				}

				break;
			case 'c':
			case 'C':
				nErrPreproc = MultCorrection(mX, specPreprocData.meanSpectrumMSC);
				if (nErrPreproc != 0)
				{
					_snprintf(strErrorMessage, MAX_ERR_MSG, "Error doing multiplicative scatter correction");
					return ER_PREPROC_SP;
				}
				break;
			case 'b':
			case 'B':
				nErrPreproc = BaseLineCor(mX);
				if (nErrPreproc != 0)
				{
					_snprintf(strErrorMessage, MAX_ERR_MSG, "Error performing base line correction");
					#ifdef _DEBUG_
						AppendToDebugReport(strErrorMessage);
					#endif
					return nErrPreproc;
				}
				break;
			case '1':
				nErrPreproc = Derivate(mX, fStepFreq, nWidth, nPoly, 1);
				if (nErrPreproc != 0)
				{
					_snprintf(strErrorMessage, MAX_ERR_MSG, "Error derivative: too few points in spectra");
					return nErrPreproc;
				}
				break;
			case '2':
				nErrPreproc = Derivate(mX, fStepFreq, nWidth, nPoly, 2);
				if (nErrPreproc != 0)
				{
					_snprintf(strErrorMessage, MAX_ERR_MSG, "Error derivative: too few points in spectra");
					return nErrPreproc;
				}
				break;
			case 'h':
			case 'H': // ������������ �� ��������:
				NormMax(mX, specPreprocData.fMax);
				break;
			default:
				break;
		}
	}

	return nErrPreproc;
}

int FindIndex(double f, const CFreqScale & freqScale, const CSpecPreprocData & specPreprocData, bool bMin)
{
	// ������� ������ ��������� � f ���������� �������
	int ind;

/*	if ((specPreprocData.numberOfExcludedFreq > 0) && (specPreprocData.nModelType != MODEL_HSO))
	{
		ind = -1;
		double fFreqPrev = 0;
		double fFreq = 0;

		for (int i = 0; i < freqScale.nNumberOfFreq; i++)
		{
			if (specPreprocData.vFreqExcluded[i] != 0)
			{
				ind++;
				fFreq = freqScale.fStartFreq + i * freqScale.fStepFreq;
				if (fFreq >= f)
				{
					break;
				}
				else
				{
					fFreqPrev = fFreq;
				}
			}
		}

		// ������� ��������� �������
		if ((fFreq >= f) && ((fFreq - f) >= (f - fFreqPrev))) ind--;
	}
	else
	{
		double tmp;

		tmp = (f - freqScale.fStartFreq) / freqScale.fStepFreq;
		ind = (int)floor(tmp);
		if (tmp - ind > 0.5) ind++;
		if (ind >= freqScale.nNumberOfFreq) ind = freqScale.nNumberOfFreq-1;
	}*/

	double tmp;

	tmp = (f - freqScale.fStartFreq) / freqScale.fStepFreq;
	ind = (int)floor(tmp);
	if (tmp - ind > 0.5) ind++;
	if (ind >= freqScale.nNumberOfFreq) ind = freqScale.nNumberOfFreq-1;

	if ((specPreprocData.numberOfExcludedFreq > 0) && (specPreprocData.nModelType != MODEL_HSO))
	{
		int i = 0;
		int nInc=0;

		// ������������ ����� ���������� ������ �� ind
		do
		{
			if (specPreprocData.vUseFreq[i]) nInc++;
			i++;
			if (i == freqScale.nNumberOfFreq) break;
		}
		while(i <= ind);

		if ((bMin == true) && (specPreprocData.vUseFreq[ind] == 0))
		{
			ind = nInc;
		}
		else
		{
			ind = nInc - 1;
		}
	}

	return ind;
}






