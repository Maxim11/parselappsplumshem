#ifndef _HSOMATRIXUTILS_H_
#define _HSOMATRIXUTILS_H_

#include <limits>
#include "newmat.h"

extern Matrix mGarmGlobal;

//using namespace std;
static double Epsilon()         // smallest number such that 1+Eps!=Eps
      { return numeric_limits<double>::epsilon(); }

//extern double durationTrans;
//extern double durationHCQP;
//extern double durationRev;

//-------1.������� ���������� �������������� ������� (2)------------------------
//���������� �������������� ������� p ��� �������� ������
//���������� ��� ������ �� ������� lemke1()
//int CalibSCM(int numGarm,int numFact,double SizeGcub,int *IndexUseFreq,
//           double *x, double *y,int numFreq,int numSpec,double *p);
//���������� ������������� �������� p � p2 ��� ���������� ������
//���������� ��� ������ �� ������� lemke1()
//int CalibSCM(int numGarm,int numFact,double SizeGcub,double Knl,int *IndexUseFreq,
//             double *x, double *y,int numFreq,int numSpec,double *p,double *p2);


//-----------2.������� ����������(��������������/���������) �������������� (4)--
//�������������� ������� �������� x :
//-� ���������� ������� ���������� ��������������
//void Transformation(int numGarm,int numFreq,int numSpec,double *x,
//                  int numFact,int *IndexUseFreq,double *xGarm,double *MGarm);

//void Transformation(int numGarm, const Matrix &mX, int numFact,
//					const int *pIndexUseFreq, Matrix &mXGarm, Matrix &mGarm);


//-��� ��������� ������� ���������� ��������������
//void Transformation(int numGarm,int numFreq,int numSpec,double *x,
//                    int numFact,int *IndexUseFreq,double *xGarm);
//void Transformation(int numGarm, const Matrix &mX, int numFact,
//					const int *pIndexUseFreq, Matrix &mXGarm);


//void Transformation_old(int numGarm,int numFreq,int numSpec,double *x,int numFact,int *IndexUseFreq,
//                                                    double *xGarm,double *MGarm);

//������������ ������� �������������� �������������� MGarm[numFreq*numFact]: p = MGarm*pGarm
//void GarmTransformation(int numFact,int numFreq,int *IndexUseFreq,double *MGarm);
//void GarmTransformation(Matrix &mGarm);

//void GarmTransformation(int numFact, const Matrix &mSp, Matrix &mSpFTrans);
//void GarmTransformation(int numFact, const Matrix &mX, Matrix &mXGarm);

void GarmTransformationFFTW(int numFact, const Matrix &mX, Matrix &mXGarm);

// mGarm [numFreq, numFact]
void GarmTransformation(Matrix &mGarm);



//������������ ������� ��������� �������������� MGarm[numFreq*numFact]: p = MGarm*pGarm
//void PointTransformation(int numFact,int numFreq,int *IndexUseFreq,double *MGarm);
//void PointTransformation(const int *pIndexUseFreq, Matrix &mGarm);


//-------------3.������� ��� ������� ������ HCQP (3)----------------------------
//���������� ��� ������ �� ������� lemke1()
//int HCQP(int n,int k,double *X,double *y,double D,double Knl,double *p);
//void rpcf(int n,int k,double* X,double* Y,double* c,double* d);

int HCQP(const Matrix &mX, const ColumnVector &vY, double D, double Knl, ColumnVector &vP);

void rpcf(const Matrix &mX, const ColumnVector &vY, Matrix &mC, Matrix &mD);


//���������� ��� ������
int lemke1 (double *c,double *d, double *xd, int m, double *x,bool STOP);
//--------------4.������� ��� �������������� ������ (3)-------------------------
//��������� ������ ���������� �������� c[m*n]=a[m*l]*b[l*n]
//void  MatrixMultiplication(int m,int n,int l,int PrTr_a,int PrTr_b,double *a,
//                         double *b,double *c);
//���������������� ������� ����������� ������� b[m*n] = a[n*m]
//void MatrixTr(int m,int n,double *a,double *b);

void InitFFTW(int numFreq, int numFact);
void FreeFFTW();

//-----------------����� 11 �������---------------------------------------------
#endif // _HSOMATRIXUTILS_H_
