#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <io.h>
#include <sys/stat.h>
#include <fcntl.h>


#include "spectra.h"




// spectra
CIrSpecRaw::CIrSpecRaw()
{
	s = 0;
	spline = 0;
	Clear();
	fLambda = 0.00006328;
}



CIrSpecRaw::CIrSpecRaw(CIrSpecRaw &spec)
{
	fmin = spec.fmin;
	fmax = spec.fmax;
	res = spec.res;
	nacc = spec.nacc;
	napod = spec.napod;
	amx = spec.amx;
	points = spec.points;
	nzfill = spec.nzfill;
	nsingle = spec.nsingle;
	nphase = spec.nphase;
	nfreqeq = spec.nfreqeq;
	s = new double[points];
	if(!s) return;
	memcpy(s, spec.s, points*sizeof(double));

	spline = 0;
}

CIrSpecRaw::~CIrSpecRaw()
{
	if(s) delete[] s;
	if(spline) delete spline;
}


int CIrSpecRaw::SetFmin(double _fmin)
{
	if(_fmin >= fmax) return 0;
	if(_fmin < 0) return 0;
	fmin = _fmin;
	return 1;
}

int CIrSpecRaw::SetFmax(double _fmax)
{
	if(_fmax > (points-1)*res) return 0;
	if(_fmax < fmin) return 0;

	fmax = _fmax;
	return 1;
}

double CIrSpecRaw::GetFreq(int i) 
{
	return i*res;
}

int CIrSpecRaw::GetIndex(double f) 
{
	return int(f/res);
}

double CIrSpecRaw::GetSpecAtPoint(int i) 
{
	return i < points ? s[i] : 0;
}

double CIrSpecRaw::GetSpecAtFreq(double freq, int nUseSpline) 
{
	double v;
	if(nUseSpline)
	{
		if(!spline) CalculateSpline();
		v = spline->GetY(freq);
	}
	else
		v = GetSpecAtPoint(GetIndex(freq));

	return v;
}


double CIrSpecRaw::GetSpecMin(int i1, int i2) 
{
	double d = GetSpecAtPoint(i1);
	for(int i=i1+1; i<=i2; i++)
	{
		double s = GetSpecAtPoint(i);
		if(s < d) d = s;
	}
	return d;
}

double CIrSpecRaw::GetSpecMax(int i1, int i2) 
{
	double d = GetSpecAtPoint(i1);
	for(int i=i1+1; i<=i2; i++)
	{
		double s = GetSpecAtPoint(i);
		if(s > d) d = s;
	}
	return d;
}


static int round(double x)
{
	int i1 = int(floor(x));
	int i2 = int(ceil(x));

	return fabs(x-i1) < fabs(x-i2) ? i1 : i2;
}

static int round_p2(double x)
{
	double p2x = round(log10(x)/log10(2.0));
	return int(pow(2., double(p2x)));
}


void CIrSpecRaw::Clear()
{
	fmin = 0;
	fmax = 0;
	res = 1;
	points = 0;
	nzfill = 1;
	nsingle = 0;
	nphase = 0;
	nfreqeq = 0;

	if(s) delete[] s;
	s = 0;

	if(spline) delete spline;
	spline = 0;

}

const int SHID_END = 0;
const int SHID_REF = 1;
const int SHID_TIM = 2;
const int SHID_RES = 3;
const int SHID_FMI = 4;
const int SHID_FMA = 5;
const int SHID_APT = 6;
const int SHID_NSC = 7;
const int SHID_SID = 8;
const int SHID_ABS = 9;
const int SHID_DID = 10;
const int SHID_ZFL = 11;
const int SHID_FEQ = 12;
const int SHID_WVL = 13;
const int SHID_SGL = 14;
const int SHID_PHA = 15;
const int SHID_DSN = 16;

const int SHID_IFG = 101;
const int SHID_SPC = 102;
const int SHID_IRS = 103;

int FindHeaderField(const char *str)
{
	if(!strncmp(str, "#END", 4)) return SHID_END;
	if(!strncmp(str, "#REF", 4)) return SHID_REF;
	if(!strncmp(str, "#TIM", 4)) return SHID_TIM;
	if(!strncmp(str, "#RES", 4)) return SHID_RES;
	if(!strncmp(str, "#FMI", 4)) return SHID_FMI;
	if(!strncmp(str, "#FMA", 4)) return SHID_FMA;
	if(!strncmp(str, "#APT", 4)) return SHID_APT;
	if(!strncmp(str, "#NSC", 4)) return SHID_NSC;
	if(!strncmp(str, "#SID", 4)) return SHID_SID;
	if(!strncmp(str, "#ABS", 4)) return SHID_ABS;
	if(!strncmp(str, "#DID", 4)) return SHID_DID;
	if(!strncmp(str, "#ZFL", 4)) return SHID_ZFL;
	if(!strncmp(str, "#FEQ", 4)) return SHID_FEQ;
	if(!strncmp(str, "#WVL", 4)) return SHID_WVL;
	if(!strncmp(str, "#SGL", 4)) return SHID_SGL;
	if(!strncmp(str, "#PHA", 4)) return SHID_PHA;
	if(!strncmp(str, "#DSN", 4)) return SHID_PHA;

	if(!strncmp(str, "#IFG", 4)) return SHID_IFG;
	if(!strncmp(str, "#SPC", 4)) return SHID_SPC;
	if(!strncmp(str, "#IRS", 4)) return SHID_IRS;

	return -1;
}

int CIrSpecRaw::ReadHeader(FILE *file)
{
	double formalres;
	char str[260];
	int iok = 0;
	while(fgets(str, 256, file))
	{
		if(!strncmp(str,"DATA",4))
		{
			iok = 1;
			break; 
		}

		int opt = FindHeaderField(str);
//		if(!opt) return 1;
		switch(opt)
		{
		case SHID_TIM:
			{
				char *tstr = str+4;
				int yr = atoi(strtok(tstr," \t\n\r-:"));
				int mo = atoi(strtok(0," \t\n\r-:"));
				int dy = atoi(strtok(0," \t\n\r-:"));
				int h = atoi(strtok(0," \t\n\r-:"));
				int m = atoi(strtok(0," \t\n\r-:"));
				int s = atoi(strtok(0," \t\n\r-:"));
			}
			break;

		case SHID_FMI:
			fmin = atof(str+4);
			break;

		case SHID_FMA:
			fmax = atof(str+4);
			break;

		case SHID_WVL:
			fLambda = atof(str+4);
			break;

		case SHID_RES:
			formalres = atof(str+4);
			break;

		case SHID_APT:
			napod = atoi(str+4);
			break;

		case SHID_ZFL:
			nzfill = atoi(str+4);
			break;

		case SHID_SGL:
			nsingle = atoi(str+4);
			break;

		case SHID_PHA:
			nphase = atoi(str+4);
			break;

		case SHID_FEQ:
			nfreqeq = atoi(str+4);
			break;

		case SHID_NSC:
			nacc = atoi(str+4);
			break;

		case SHID_SID:
			break;

		case SHID_DID:
			break;

		case SHID_DSN:
			break;

		case SHID_REF:
			break;

		default:
			break;
		}
	};

//	preservativ
	res = formalres;
	formalres = GetFormalRes();

//
	res = formalres/(16384*fLambda);
	return iok;
}


int CIrSpecRaw::ReadData(FILE *file, double def)
{
	char str[260];

	points = int(fmax/res)+2;
	s = new double[points];
	if(!s) return 0;
	double f = 0;
	for(int i=0; i<points; i++)
	{
		if((f+res) >= fmin)
		{
			if(!fgets(str, 256, file)) break;
			double f0 = atof(strtok(str, " \t\n\r"));
			s[i] = atof(strtok(0, " \t\n\r"));
		}
		else
		{
			s[i] = def;
		}
		f+=res;
	}
	return i;
}

int CIrSpecRaw::Read(const char *fname)
{
	char str[260];

	FILE *file = fopen(fname, "rt");
	if(!file) return 0;
	if(!fgets(str, 256, file)) return 0;
	if(strncmp(str, "#SPC", 4))
	{
		fclose(file);
		return 0;
	}

	Clear();
	if(!ReadHeader(file)) return 0;

	if(!ReadData(file, 0.)) return 0;

	fclose(file);
	Calculate();
	return 1;
}

void CIrSpecRaw::CalculateSpline()
{
	int i1 = GetIndex(fmin);
	int i2 = GetIndex(fmax)+1;
	int n = i2-i1+1;
	double *x = new double[n];
	double *y = new double[n];
	for(int i=0; i<n; i++)
	{
		x[i] = (i1+i)*res; 
		y[i] = GetSpecAtPoint(i1+i);
	}
	if(spline) delete spline;
	spline = new CSpline(x,y,n);
	delete [] x;
	delete [] y;
}



void CIrSpecRaw::Calculate()
{
	int i = 0;
	amx = 0;
	while(i<points)
	{
		if(s[i] > amx)
		{
			amx = s[i];
		}
		i++;
	}
}

double CIrSpecRaw::GetFormalRes()
{
	if(fabs(res-0.125) < 0.025) return 0.125;
	if(fabs(res-0.25) < 0.05) return 0.25;
	if(fabs(res-0.5) < 0.1) return 0.5;
	if(fabs(res-1) < 0.25) return 1;
	if(fabs(res-2) < 0.5) return 2;
	if(fabs(res-4) < 1) return 4;
	if(fabs(res-8) < 2) return 8;
	if(fabs(res-16) < 4) return 16;
	return -1;
}

double CIrSpecRaw::GetStandardRes()
{
	double fres = GetFormalRes();
	double sres = fres/(16384*0.00006328);
	return sres;
}


/////////////////////
// CIrSpectrum stuff //
/////////////////////

CIrSpectrum::CIrSpectrum(): CIrSpecRaw()
{
	absflag = 0;
}

CIrSpectrum::CIrSpectrum(CIrSpectrum &spec)
{
	*this = spec;
	s = new double[points];
	memcpy(s, spec.s, points*sizeof(double));
	spline = 0;
}

CIrSpectrum::CIrSpectrum(CIrSpectrum *pspec)
{
	*this = *pspec;
	s = new double[points];
	memcpy(s, pspec->s, points*sizeof(double));
	spline = 0;
}


int CIrSpectrum::SetAbsFlag(int naf) 
{
	if(absflag != naf)
	{
		absflag = naf; 
		if(spline) 
		{
			delete spline;
			spline = 0;
		}
	}
	return absflag;
} 


double CIrSpectrum::GetSpecAtPoint(int i) 
{
	if(i < points)
	{
		if(!absflag)
			return s[i]*100;
		else
			return (s[i] > 0.) ? -log10(s[i]) : 0;
	}
	else
		return absflag ? 0. : 100.;
}

int CIrSpectrum::SetSpec(int i, double v) 
{
	if(i < points)
	{
		if(!absflag)
		{
			s[i] = v/100;
		}
		else
		{
			s[i] = pow(10., -v);
		}
		return 1;
	}
	else
		return 0;
}


void CIrSpectrum::Calculate()
{
	int i = 0;
	amx = 0;
	while(i<points)
	{
		double v = GetSpecAtPoint(i);
		if(v > amx) amx = v;
		i++;
	}
}

int CIrSpectrum::Read(const char *fname)
{
	char str[260];

	FILE *file = fopen(fname, "rt");
	if(!file) return 0;
	if(!fgets(str, 256, file)) return 0;
	if(strncmp(str, "#SPC", 4))
	{
		fclose(file);
		return 0;
	}

	if(!fgets(str, 256, file)) { fclose(file); return 0;}
	if(!strncmp(str, "#ABS", 4)) absflag = atoi(str+4);

	Clear();
	if(!ReadHeader(file)) return 0;
	ReadData(file, 1.);

	fclose(file);
	Calculate();
	return 1;
}


