#include <afxwin.h>

#include "Transfer.h"
#include "QntCalibration.h"
#include "SpLumChemStructs.h"
#include <direct.h>
#include "SampleFromFile.h"
#include "newmat.h"
#include "MatrixUtilities.h"
#include "TransData.h"
#include "PreprocUtils.h"
#include <math.h>

enum TransErrors {ER_MDIR, ER_SDIR};


void IRCalibrTransfer::SaveModel(const char *clbName)
{
	if (nError == ER_OK)
	{
		char fname[_MAX_PATH];

		strcpy(fname, clbName);
		strcat(fname, ".qnt");

		if (pQntCalibration)
		{
			pQntCalibration->Save(fname);
		}
	}
}




IRCalibrTransfer::IRCalibrTransfer()
{
	nError = ER_OK;

	numberOfCommonSamples = 0;
	 
	numberOfComponents = 0;

	pQntCalibration = 0;

	pQltCalibration = 0;

	pSEV_M_Before = 0;

	pSEV_S_Before = 0;

	pSEV_M_After = 0;

	pSEV_S_After = 0;

	pdistS = 0;

	pdistS_tr = 0;

	pdistM = 0;

	pSEC_Before = 0;

	pSEC_After = 0;

	pR2_After = 0;
}

IRCalibrTransfer::~IRCalibrTransfer()
{
	delete pQntCalibration;
	delete pQltCalibration;
	delete[] pSEV_M_Before;
	delete[] pSEV_S_Before;
	delete[] pSEV_M_After;
	delete[] pSEV_S_After;

	delete[] pSEC_Before;
	delete[] pSEC_After;
	delete[] pR2_After;



	delete[] pdistS;
	delete[] pdistS_tr;
	delete[] pdistM;
	delete[] pdistM_tr;
}

const char *IRCalibrTransfer::GetCompName(int nc)
{
	if (pQntCalibration)
	{
		return pQntCalibration->GetComponentName(nc);
	}
	else
	{
		return NULL;
	}
}



int IRCalibrTransfer::Transfer(char* *pstrSampleName,
				const char* strMasterDir,
				const char* strSlaveDir,
				int numberOfSamples,
				const char* strClbName,
				int nTrType,
				vector<vector<bool>*> *pvSampleSpectraStatus)
{

	// load qnt model
	LoadModel(strClbName);

	if (nError != ER_OK) return nError;

	// now check the consistency master and slave samples
	nError = CheckSamples(pstrSampleName, strMasterDir, strSlaveDir, numberOfSamples, pvSampleSpectraStatus);

	if (nError != ER_OK) return nError;

	// load samples
	LoadSamples(pstrSampleName, strMasterDir, strSlaveDir,	numberOfSamples, pvSampleSpectraStatus);

	if (nError != ER_OK) return nError;

	// get SEC before transfer
	pSEC_Before = new double[numberOfComponents];

	for (int iC = 0; iC < numberOfComponents; iC++)
	{
		pQntCalibration->GetSEC(pstrComponentName[iC].c_str(), pSEC_Before + iC);
	}


	// at first - preprocess spectra
	// get calibrations preproc. data

	CSpecPreprocData theSpecPreprocData;

	pQntCalibration->GetSpecPreprocData(theSpecPreprocData);

	Matrix mMasterSpPrep, mSlaveSpPrep;

	PrepSpForPrediction(theFreqScale, theSpecPreprocData, mMasterSp, mMasterSpPrep);

	PrepSpForPrediction(theFreqScale, theSpecPreprocData, mSlaveSp, mSlaveSpPrep);


	// now predict and regress
	Matrix mMasterCompPredicted, mSlaveCompPredicted;


	pQntCalibration->PredictPreprocSpectra(mMasterSpPrep, mMasterCompPredicted);
	pQntCalibration->PredictPreprocSpectra(mSlaveSpPrep, mSlaveCompPredicted);


	ColumnVector vMahDist;

	pQntCalibration->CalculateNormMahalanobisDistances(mMasterSpPrep, vMahDist);

	pdistM = new double[nTotalSpectra];
	pdistS = new double[nTotalSpectra];

	for (int iS = 0; iS < nTotalSpectra; iS++)
	{
		pdistM[iS] = vMahDist(iS + 1);
	}

	pQntCalibration->CalculateNormMahalanobisDistances(mSlaveSpPrep, vMahDist);

	for (iS = 0; iS < nTotalSpectra; iS++)
	{
		pdistS[iS] = vMahDist(iS + 1);
	}



	pSEV_M_Before = new double[numberOfComponents];
	pSEV_S_Before = new double[numberOfComponents];


	ColumnVector vdifM, vdifS;

	vdifM.ReSize(nTotalSpectra);
	vdifS.ReSize(nTotalSpectra);

	for (int nc = 1; nc <= numberOfComponents; nc++)
	{
		vdifM = mMasterCompPredicted.Column(nc) - mComponents.Column(nc);
		vdifS = mSlaveCompPredicted.Column(nc) - mComponents.Column(nc);
		pSEV_M_Before[nc - 1] = sqrt(vdifM.SumSquare() / nTotalSpectra);
		pSEV_S_Before[nc - 1] = sqrt(vdifS.SumSquare() / nTotalSpectra);
	}

	CTransData theTransData;

	theTransData.SetTrType(nTrType);

	RowVector vSlopeRef, vBiasRef;
	RowVector vSlopeSp, vBiasSp;

	pdistM_tr = new double[nTotalSpectra];
	pdistS_tr = new double[nTotalSpectra];

	CSpecPreprocData theSpecPreprocDataNew;

	switch(nTrType)
	{
	case 1:

		Ordinary_LS(mMasterCompPredicted, mSlaveCompPredicted, vSlopeRef, vBiasRef);

		theTransData.SetSlopeRef(vSlopeRef);

		theTransData.SetBiasRef(vBiasRef);

		// the Mahalanobis distances are the same
		for (iS = 0; iS < nTotalSpectra; iS++)
		{
			pdistS_tr[iS] = pdistS[iS];
			pdistM_tr[iS] = pdistM[iS];
		}
		break;

	case 2:

		Ordinary_LS(mSlaveSpPrep, mMasterSpPrep, vSlopeSp, vBiasSp);

		theTransData.SetSlopeSp(vSlopeSp);

		theTransData.SetBiasSp(vBiasSp);
		break;

	case 21:
		// correct only after transform to ABS

		theSpecPreprocDataNew.fMinFreq = theSpecPreprocData.fMinFreq;
		theSpecPreprocDataNew.fMaxFreq = theSpecPreprocData.fMaxFreq;
		theSpecPreprocDataNew.nSpType = theSpecPreprocData.nSpType;
		theSpecPreprocDataNew.numberOfChannels = theSpecPreprocData.numberOfChannels;

		mMasterSpPrep.Release();
		mSlaveSpPrep.Release();

		PrepSpForPrediction(theFreqScale, theSpecPreprocDataNew, mMasterSp, mMasterSpPrep);

		PrepSpForPrediction(theFreqScale, theSpecPreprocDataNew, mSlaveSp, mSlaveSpPrep);

		Ordinary_LS(mSlaveSpPrep, mMasterSpPrep, vSlopeSp, vBiasSp);
		theTransData.SetSlopeSp(vSlopeSp);

		theTransData.SetBiasSp(vBiasSp);
		break;

	case 22:
		// correct only after transform to ABS and preprocessing BN12

		theSpecPreprocDataNew.fMinFreq = theSpecPreprocData.fMinFreq;
		theSpecPreprocDataNew.fMaxFreq = theSpecPreprocData.fMaxFreq;
		theSpecPreprocDataNew.nSpType = theSpecPreprocData.nSpType;
		theSpecPreprocDataNew.numberOfChannels = theSpecPreprocData.numberOfChannels;

		if (theSpecPreprocData.strPreprocPar.find_first_of('B') != string::npos)
		{
			theSpecPreprocDataNew.strPreprocPar += 'B';
		}

		if (theSpecPreprocData.strPreprocPar.find_first_of('N') != string::npos)
		{
			theSpecPreprocDataNew.strPreprocPar += 'N';
		}

		if (theSpecPreprocData.strPreprocPar.find_first_of('1') != string::npos)
		{
			theSpecPreprocDataNew.strPreprocPar += '1';
		}

		if (theSpecPreprocData.strPreprocPar.find_first_of('2') != string::npos)
		{
			theSpecPreprocDataNew.strPreprocPar += '2';
		}

		mMasterSpPrep.Release();
		mSlaveSpPrep.Release();

		PrepSpForPrediction(theFreqScale, theSpecPreprocDataNew, mMasterSp, mMasterSpPrep);

		PrepSpForPrediction(theFreqScale, theSpecPreprocDataNew, mSlaveSp, mSlaveSpPrep);

		Ordinary_LS(mSlaveSpPrep, mMasterSpPrep, vSlopeSp, vBiasSp);

		theTransData.SetSlopeSp(vSlopeSp);

		theTransData.SetBiasSp(vBiasSp);
		break;
	}

	pQntCalibration->Transfer(theTransData);

	pQntCalibration->GetSpecPreprocData(theSpecPreprocData);
	mMasterSpPrep.Release();
	mSlaveSpPrep.Release();
	PrepSpForPrediction(theFreqScale, theSpecPreprocData, mMasterSp, mMasterSpPrep);
	PrepSpForPrediction(theFreqScale, theSpecPreprocData, mSlaveSp, mSlaveSpPrep);


// calculate Mahalanobis distances for transfered calibrartion
	pdistM_tr = new double[nTotalSpectra];
	pdistS_tr = new double[nTotalSpectra];

	switch(nTrType)
	{
	case 1:
	// the Mahalanobis distances are the same
		for (iS = 0; iS < nTotalSpectra; iS++)
		{
			pdistS_tr[iS] = pdistS[iS];
			pdistM_tr[iS] = pdistM[iS];
		}
		break;

	case 2:
	case 21:
	case 22:

		pQntCalibration->CalculateNormMahalanobisDistances(mMasterSpPrep, vMahDist);

		for (iS = 0; iS < nTotalSpectra; iS++)
		{
			pdistM_tr[iS] = vMahDist(iS + 1);
		}

		pQntCalibration->CalculateNormMahalanobisDistances(mSlaveSpPrep, vMahDist);

		for (iS = 0; iS < nTotalSpectra; iS++)
		{
			pdistS_tr[iS] = vMahDist(iS + 1);
		}
	}

	pQntCalibration->PredictPreprocSpectra(mMasterSpPrep, mMasterCompPredicted);
	pQntCalibration->PredictPreprocSpectra(mSlaveSpPrep, mSlaveCompPredicted);


	pSEV_M_After = new double[numberOfComponents];
	pSEV_S_After = new double[numberOfComponents];



	for (nc = 1; nc <= numberOfComponents; nc++)
	{
		vdifM = mMasterCompPredicted.Column(nc) - mComponents.Column(nc);
		vdifS = mSlaveCompPredicted.Column(nc) - mComponents.Column(nc);
		pSEV_M_After[nc - 1] = sqrt(vdifM.SumSquare() / nTotalSpectra);
		pSEV_S_After[nc - 1] = sqrt(vdifS.SumSquare() / nTotalSpectra);
	}

	pSEC_After = new double[numberOfComponents];
	pR2_After = new double[numberOfComponents];

	for (iC = 0; iC < numberOfComponents; iC++)
	{
		pQntCalibration->GetR2Stat(pstrComponentName[iC].c_str(), pR2_After + iC);
		pQntCalibration->GetSEC(pstrComponentName[iC].c_str(), pSEC_After + iC);
	}


	return ER_OK;
}



double IRCalibrTransfer::GetSECbeforeTrans(int nc)
{
	if (pQntCalibration)
	{
		return pSEC_Before[nc];
	}
	else
	{
		return -1;
	}
}

double IRCalibrTransfer::GetSECafterTrans(int nc)
{
	if (pQntCalibration)
	{
		return pSEC_After[nc];
	}
	else return -1;
}


double IRCalibrTransfer::GetR2afterTrans(int nc)
{
	if (pQntCalibration)
	{
		return pR2_After[nc];
	}
	else return -1;
}

double IRCalibrTransfer::GetDistS(int numSample, int numSp)
{
	int n;

	n = pSampleSpectraIndexes[numSample] + numSp;

	return pdistS[n];
}

double IRCalibrTransfer::GetDistM(int numSample, int numSp)
{
	int n;

	n = pSampleSpectraIndexes[numSample] + numSp;

	return pdistM[n];
}

double IRCalibrTransfer::GetDistS_tr(int numSample, int numSp)
{
	int n;

	n = pSampleSpectraIndexes[numSample] + numSp;

	return pdistS_tr[n];
}

double IRCalibrTransfer::GetDistM_tr(int numSample, int numSp)
{
	int n;

	n = pSampleSpectraIndexes[numSample] + numSp;

	return pdistM_tr[n];
}

int IRCalibrTransfer::GetNumSampleSpectra(int numSample)
{
	return pSampleSpectraNumbers[numSample];
}


int IRCalibrTransfer::LoadSamples(char* *pstrSampleName,
				const char* strMasterDir,
				const char* strSlaveDir,
				int numberOfSamples,
				vector<vector<bool>*> *pvSampleSpectraStatus)
{
	// now fill the spectra and components matrixes

	mMasterSp.ReSize(nTotalSpectra, theFreqScale.nNumberOfFreq);

	mSlaveSp.ReSize(nTotalSpectra, theFreqScale.nNumberOfFreq);

	mComponents.ReSize(nTotalSpectra, numberOfComponents);


	// load master samples
	if (chdir(strMasterDir) != 0)
	{
		nError = ER_MDIR;

		return nError;
	}

	CSampleFromFile *pMSampleFromFile, *pSSampleFromFile;

	for (int i = 0; i < numberOfSamples; i++)
	{
		pMSampleFromFile = new CSampleFromFile;
		pMSampleFromFile->Load(pstrSampleName[i], (*pvSampleSpectraStatus)[i]);
		int nFirstSp = pSampleSpectraIndexes[i];

		// load spectra
		for (int iS = 0; iS < pSampleSpectraNumbers[i]; iS++)
		{
			mMasterSp.Row(nFirstSp + iS + 1) << pMSampleFromFile->GetSpectrum(iS);
		}

		// load components
		for (int iC = 0; iC < numberOfComponents; iC++)
		{
			for (iS = 0 ; iS < pSampleSpectraNumbers[i]; iS++)
			{
				mComponents(nFirstSp + iS + 1, iC + 1) = pMSampleFromFile->GetComponent(pstrComponentName[iC]);
			}
		}

		delete pMSampleFromFile;
	}

	// load slave samples
	if (chdir(strSlaveDir) != 0)
	{
		nError = ER_MDIR;

		return nError;
	}

	for (i = 0; i < numberOfSamples; i++)
	{
		pSSampleFromFile = new CSampleFromFile;
		pSSampleFromFile->Load(pstrSampleName[i], (*pvSampleSpectraStatus)[i]);

		int nFirstSp = pSampleSpectraIndexes[i];

		// load spectra
		for (int iS = 0; iS < pSampleSpectraNumbers[i]; iS++)
		{
			mSlaveSp.Row(nFirstSp + iS + 1) << pSSampleFromFile->GetSpectrum(iS);
		}

		delete pSSampleFromFile;
	}

	return nError;
}

int IRCalibrTransfer::LoadModel(const char* strClbName)
{
	char fname[_MAX_PATH];

	//load Qnt Calibration

	strcpy(fname, strClbName);

	strcat(fname, ".qnt");

	pQntCalibration = new CQntCalibration(fname);

//	int nError;

	if ((nError = pQntCalibration->GetError()) != ER_OK)
	{
//		delete pQntCalibration;

		char sError[_MAX_PATH];
		strcpy(sError, "Error loading file ");
		strcat(sError, fname);
		AfxMessageBox(sError);

		return nError;
	}

	numberOfComponents = pQntCalibration->GetNumComponents();

	pstrComponentName = new string[numberOfComponents];

	for (int iC = 0; iC < numberOfComponents; iC++)
	{
		pstrComponentName[iC] = string(pQntCalibration->GetComponentName(iC));
	}

	return nError;
}

int IRCalibrTransfer::CheckSamples(char* *pstrSampleName,
				const char* strMasterDir,
				const char* strSlaveDir,
				int numberOfSamples,
				vector<vector<bool>*> *pvSampleSpectraStatus)
{
	pSampleSpectraIndexes = new int[numberOfSamples];
	pSampleSpectraNumbers = new int[numberOfSamples];

	CSampleFromFile *pMSampleFromFile, *pSSampleFromFile;
	int nMSpectra, nSSpectra;

	double fBaseStepFreq, fBaseStartFreq;
	int nBaseNumOfFreq;

	nTotalSpectra = 0;

	char sError[_MAX_PATH];

	pMSampleFromFile = 0;
	pSSampleFromFile = 0;

	for (int i = 0; i < numberOfSamples; i++)
	{
		// master sample
		chdir(strMasterDir);
		pMSampleFromFile = new CSampleFromFile;
		pMSampleFromFile->Load(pstrSampleName[i]);

		if ((nError = pMSampleFromFile->GetError()) != ER_OK)
		{
			sprintf(sError, "Error loading master sample %s", pstrSampleName[i]);
			AfxMessageBox(sError);
			return -1;
		}

		// check stepFreq, startFreq and numOfFreq

		if (i == 0)
		{
			fBaseStepFreq = pMSampleFromFile->GetStepFreq();
			fBaseStartFreq = pMSampleFromFile->GetStartFreq();
			nBaseNumOfFreq = pMSampleFromFile->GetNumFreq();
		}
		else
		{
			if (pMSampleFromFile->GetStepFreq() != fBaseStepFreq)
			{
				sprintf(sError, "Step frequency in sample %s is different from that in sample %s", pstrSampleName[0], pstrSampleName[i]);
				AfxMessageBox(sError);
				return -1;
			}

			if (pMSampleFromFile->GetStartFreq() != fBaseStartFreq)
			{
				sprintf(sError, "Start frequency in sample %s is different from that in sample %s", pstrSampleName[0], pstrSampleName[i]);
				AfxMessageBox(sError);
				return -1;
			}

			if (pMSampleFromFile->GetNumFreq() != nBaseNumOfFreq)
			{
				sprintf(sError, "Start frequency in sample %s is different from that in sample %s", pstrSampleName[0], pstrSampleName[i]);
				AfxMessageBox(sError);
				return -1;
			}
		}

		// check components
		for (int iC = 0; iC < numberOfComponents; iC++)
		{
			if (!pMSampleFromFile->FindComponent(pstrComponentName[iC]))
			{
				sprintf(sError, "Can't find needed component %s in the sample %s", pstrComponentName[iC].c_str(), pstrSampleName[i]);
				AfxMessageBox(sError);

				return -1;
			}
		}

		nMSpectra = pMSampleFromFile->GetNumSpectra();
		delete pMSampleFromFile;

		// slave sample
		chdir(strSlaveDir);
		pSSampleFromFile = new CSampleFromFile;
		pSSampleFromFile->Load(pstrSampleName[i]);


		if ((nError = pSSampleFromFile->GetError()) != ER_OK)
		{
			sprintf(sError, "Error loading slave sample %s", pstrSampleName[i]);
			AfxMessageBox(sError);

			return -1;
		}

		nSSpectra = pSSampleFromFile->GetNumSpectra();

		// check slave step, start, num
		if (pSSampleFromFile->GetStepFreq() != fBaseStepFreq)
		{
			sprintf(sError, "Step frequency in sample %s is different from that in sample %s", pstrSampleName[0], pstrSampleName[i]);
			AfxMessageBox(sError);
			return -1;
		}

		if (pSSampleFromFile->GetStartFreq() != fBaseStartFreq)
		{
			sprintf(sError, "Start frequency in sample %s is different from that in sample %s", pstrSampleName[0], pstrSampleName[i]);
			AfxMessageBox(sError);
			return -1;
		}

		if (pSSampleFromFile->GetNumFreq() != nBaseNumOfFreq)
		{
			sprintf(sError, "Start frequency in sample %s is different from that in sample %s", pstrSampleName[0], pstrSampleName[i]);
			AfxMessageBox(sError);
			return -1;
		}

		delete pSSampleFromFile;


		// if all right

		int nSp = (*pvSampleSpectraStatus)[i]->size();

		int numSpectra = 0;

		for (int k = 0; k < nSp; k++)
		{
			if ((*(*pvSampleSpectraStatus)[i])[k]) numSpectra++;
		}


		pSampleSpectraNumbers[i] = numSpectra;
		pSampleSpectraIndexes[i] = nTotalSpectra;

		nTotalSpectra += numSpectra;
	}


	theFreqScale.fStartFreq = fBaseStartFreq;
	theFreqScale.fStepFreq = fBaseStepFreq;
	theFreqScale.nNumberOfFreq = nBaseNumOfFreq;

	return nError;
}
