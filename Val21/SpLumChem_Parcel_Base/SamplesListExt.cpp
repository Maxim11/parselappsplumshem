#include "SamplesListExt.h"
#include "Dfx.h"
#include "SpLumChemStructs.h"
#include <stdio.h>

CSamplesListExt::CSamplesListExt(const char* pstrLstName, bool flagExt)
{
	nError = 0;
	numberOfSamples = 0;
	numberOfComponents = 0;
	pSamplesArray = 0;
	pComponentsNames = 0;


	FILE* fgh = fopen(pstrLstName, "r");

	if (fgh == NULL)
	{
		nError = -1;
		return;
	}

	// read components

	char keyComp[128];
	char sectionComp[128];

	strcpy(sectionComp, "ComponentList");
	strcpy(keyComp, "numberOfComponents");

	numberOfComponents = DfxGetProfileInt(pstrLstName, sectionComp, keyComp, 0);

	pComponentsNames = new string[numberOfComponents];

	int nC = 0;

	for (int i = 1; i <= numberOfComponents; i++)
	{
		sprintf(keyComp, "Component%d", i);

		char *pstrCompName = DfxGetProfileString(pstrLstName, sectionComp, keyComp, "no");

		if (strcmp(pstrCompName, "no") != 0)
		{
			pComponentsNames[nC] = pstrCompName;
			nC++;
		}
	}

	numberOfComponents = nC;

	if (numberOfComponents == 0)
	{
		_snprintf(strErrorMessage, MAX_ERR_MSG, "Number of components in %s less or equals 0", pstrLstName);
		nError = -1;
		return;
	}

	// read samples
	char section[128], key[128];

	if (flagExt)
	{
		strcpy(section, "SampleArrayExt");
		strcpy(key, "numberOfSamples");
	}
	else
	{
		strcpy(section, "SampleArray");
		strcpy(key, "numberOfSamples");
	}

	numberOfSamples = DfxGetProfileInt(pstrLstName, section, key, 0);

	if (numberOfSamples == 0)
	{
		if (flagExt)
		{
			_snprintf(strErrorMessage, MAX_ERR_MSG, "Number of external samples in %s equals 0", pstrLstName);
		}
		else
		{
			_snprintf(strErrorMessage, MAX_ERR_MSG, "Number of samples in %s equals 0", pstrLstName);
		}


		nError = -1;
		return;
	}

	pSamplesArray = new CSampleArray[numberOfSamples];


	int nS = 0;
	CSampleArray m_SampleArray;

	for (i = 1; i <= numberOfSamples; i++)
	{
		m_SampleArray.Load(pstrLstName, i, flagExt);

		int nErr = m_SampleArray.GetError();
		if ((nErr != 0)  && (nErr != 1))
		{
			nError = -1;
			return;
		}
		else
		{
			if (nErr != 1)
			{
				pSamplesArray[nS] = m_SampleArray;
				nS++;
			}
		}
	}

	numberOfSamples = nS;

	if (numberOfSamples == 0)
	{
		if (flagExt)
		{
			_snprintf(strErrorMessage, MAX_ERR_MSG, "Number of external samples in %s equals 0", pstrLstName);
		}
		else
		{
			_snprintf(strErrorMessage, MAX_ERR_MSG, "Number of samples in %s equals 0", pstrLstName);
		}


		nError = -1;
		return;
	}
}

CSamplesListExt::~CSamplesListExt()
{
	delete[] pSamplesArray;
	delete[] pComponentsNames;
}