// CSampleListExt.h: interface for the CSampleListExt class.
//
// ��������:
// ����� CSampleListExt ������������ ��� ������
// � lst-������ ������ �������, ������� �����������
// ��������� �������(������������ ���������� Envelope4Samples.exe).
//
// ������������ ������� CSamples


#ifndef _SAMPLESLISTEXT_H_
#define _SAMPLESLISTEXT_H_

#include "SampleArray.h"

#include <string>

using namespace std;

class CSamplesListExt
{
public:
	CSamplesListExt(const char* pstrLstName, bool flagExt = false);
	int GetError() const { return nError; }
	int GetNumOfSamples() const { return numberOfSamples; }
	int GetNumOfComponents() const { return numberOfComponents; }
	CSampleArray GetSampleArray(int i) const { return pSamplesArray[i]; }
	string GetComponetName(int i) const { return pComponentsNames[i]; }
	~CSamplesListExt();
private:

	int nError;				// ��� ������
	int numberOfSamples;	// ����� ��������
	int numberOfComponents;	// ����� ���������
	CSampleArray* pSamplesArray; // ������ ��������
	string* pComponentsNames;// ������ ���� ���������
};


#endif// _SAMPLESLISTEXT_H_

