#include "MatrixUtilities.h"
#include "newmat.h"
#include <math.h>
#include "SpLumChem.h"
//#include "SpLumChemStructs.h"
//#include <stdio.h>

void SaveColumnVector(const ColumnVector& vColumn, FILE* fgh)
{
	int numRows;

	numRows = vColumn.Nrows();

	fwrite(&numRows, sizeof(int), 1, fgh);

	if (numRows > 0)
	{
		for (int i = 1; i <= numRows; i++)
		{
			double x;

			x = vColumn(i);

			fwrite(&x, sizeof(double), 1, fgh);
		}
	}
}

void SaveRowVector(const RowVector& vRow, FILE* fgh)
{
	int numCols;

	numCols = vRow.Ncols();

	fwrite(&numCols, sizeof(int), 1, fgh);

	if (numCols > 0)
	{
		for (int i = 1; i <= numCols; i++)
		{
			double x;

			x = vRow(i);

			fwrite(&x, sizeof(double), 1, fgh);
		}
	}
}



void SaveMatrix(const Matrix& mX, FILE* fgh)
{
	int numRows;
	int numCols;

	numRows = mX.Nrows();
	numCols = mX.Ncols();

	fwrite(&numRows, sizeof(int), 1, fgh);
	fwrite(&numCols, sizeof(int), 1, fgh);

	if ((numRows > 0) && (numCols > 0))
	{
		for (int i = 1; i <= numRows; i++)
		{
			double x;

			for (int j = 1; j <= numCols; j++)
			{
				x = mX(i, j);

				fwrite(&x, sizeof(double), 1, fgh);
			}
		}
	}
}


int LoadColumnVector(ColumnVector& vColumn, FILE* fgh)
{
	int numRows;

	fread(&numRows, sizeof(int), 1, fgh);

	if (numRows > 0)
	{
		vColumn.ReSize(numRows);

		for (int i = 1; i <= numRows; i++)
		{
			double x;

			fread(&x, sizeof(double), 1, fgh);
			vColumn(i) = x;
		}
	}

	return numRows;
}

int LoadRowVector(RowVector& vRow, FILE* fgh)
{
	int numCols;

	fread(&numCols, sizeof(int), 1, fgh);

	if (numCols > 0)
	{
		vRow.ReSize(numCols);

		for (int i = 1; i <= numCols; i++)
		{
			double x;

			fread(&x, sizeof(double), 1, fgh);
			vRow(i) = x;
		}
	}

	return numCols;
}

int LoadRowVector(RowVector& vRow, int nSize, FILE* fgh)
{
	int numCols;

	numCols = nSize;

	if (numCols > 0)
	{
		vRow.ReSize(numCols);

		for (int i = 1; i <= numCols; i++)
		{
			double x;

			fread(&x, sizeof(double), 1, fgh);
			vRow(i) = x;
		}
	}

	return numCols;
}

void LoadMatrix(Matrix& mX, FILE* fgh)
{
	int numRows;
	int numCols;

	fread(&numRows, sizeof(int), 1, fgh);
	fread(&numCols, sizeof(int), 1, fgh);

	if ((numRows > 0) && (numCols > 0))
	{
		mX.ReSize(numRows, numCols);

		for (int i = 1; i <= numRows; i++)
		{
			double x;

			for (int j = 1; j <= numCols; j++)
			{
				fread(&x, sizeof(double), 1, fgh);
				mX(i, j) = x;
			}
		}
	}
}


void ReadMatrix(Matrix& mX, FILE* fgh)
{
	int numRows;
	int numCols;

	fscanf(fgh, "%d", &numRows);
	fscanf(fgh, "%d", &numCols);

	if ((numRows > 0) && (numCols > 0))
	{
		mX.ReSize(numRows, numCols);

		for (int i = 1; i <= numRows; i++)
		{
			double x;

			for (int j = 1; j <= numCols; j++)
			{
				fscanf(fgh, "%le", &x);
				mX(i, j) = x;
			}
		}
	}
}

/*void SaveString(const string& str, FILE* fgh)
{
	int nLength = str.length();

	fwrite(&nLength, sizeof(int), 1, fgh);
	fwrite(str.c_str(), sizeof(char), nLength, fgh);
}*/

/*void LoadStringMy(string& str, FILE* fgh)
{
	int nLength;

	fread(&nLength, sizeof(int), 1, fgh);

	if (nLength > 0)
	{
		char* strBuf = new char[nLength + 1];
		fread(strBuf, sizeof(char), nLength, fgh);
		strBuf[nLength] = 0;
		str = string(strBuf);
		delete[] strBuf;
	}
}*/

/*void LoadStringPbt(string& str, FILE* fgh)
{
	char ch;
	int n;

	string strTmp;

	do
	{
		n = fread(&ch, 1, 1, fgh);
		strTmp += ch;
	}
	while ((ch != '\0') && (n == 1));

	str = strTmp;
}*/


void PrintMatrix(const Matrix& mX, const char* fname)
{
	 int numRows, numCols;

	 numRows = mX.Nrows();
	 numCols = mX.Ncols();
	
	 FILE *fgh = fopen(fname, "w");

	 fprintf(fgh, "Num. rows = %d\n", numRows);
	 fprintf(fgh, "Num. cols = %d\n", numCols);


	 for (int i = 1; i <= numRows; i++)
	 {
		 for (int j = 1; j <= numCols; j++)
		 {
			 fprintf(fgh, "%.6e ", mX(i, j));
		 }
		 fprintf(fgh, "\n");
	 }


	 fclose(fgh);
}

void PrintDiagonalMatrix(const DiagonalMatrix& mX, const char* fname)
{
	 int numRows;

	 numRows = mX.Nrows();
	
	 FILE *fgh = fopen(fname, "w");

	 fprintf(fgh, "Num. rows = %d\n", numRows);

	 for (int i = 1; i <= numRows; i++)
	 {

		 fprintf(fgh, "%.4e\n", mX(i));
	 }


	 fclose(fgh);
}

void PrintMatrix(const Matrix& mX, FILE* fgh)
{
	 int numRows, numCols;

	 numRows = mX.Nrows();
	 numCols = mX.Ncols();
	
	 fprintf(fgh, "Num. rows = %d\n", numRows);
	 fprintf(fgh, "Num. cols = %d\n", numCols);


	 for (int i = 1; i <= numRows; i++)
	 {
		 for (int j = 1; j <= numCols; j++)
		 {
			 fprintf(fgh, "%.4e ", mX(i, j));
		 }
		 fprintf(fgh, "\n");
	 }
}

/*void PrintMatrix(const MDbl& mX, const char* fname)
{
	 int numRows, numCols;

	 numRows = mX.size();
	 numCols = mX[0].size();

	 FILE *fgh = fopen(fname, "w");
	
	 fprintf(fgh, "Num. rows = %d\n", numRows);
	 fprintf(fgh, "Num. cols = %d\n", numCols);


	 for (int i = 0; i < numRows; i++)
	 {
		 for (int j = 0; j < numCols; j++)
		 {
			 fprintf(fgh, "%.4e ", mX[i][j]);
		 }
		 fprintf(fgh, "\n");
	 }

	 fclose(fgh);
}*/

void CalculateMean(const Matrix& mX, RowVector& vMean)
{
	// calculate mean Row (the averaging according every column)
	int numRows = mX.Nrows();
	int numCols = mX.Ncols();

	vMean.ReSize(numCols);

	vMean = mX.Row(1);

	for (int i = 2; i <= numRows; i++)
	{
		vMean += mX.Row(i);
	}

	vMean /= numRows;
}

void CalculateStd2(const Matrix& mX, RowVector& vStd2)
{
	// calculte dispersion Row (according every column)
	// (yi - <y>)^2

	int numRows = mX.Nrows();
	int numCols = mX.Ncols();

	RowVector vMean;

	CalculateMean(mX, vMean);

	vStd2.ReSize(numCols);

	for (int i = 1; i <= numCols; i++)
	{
		vStd2(i) = (mX.Column(i) - vMean(i)).SumSquare();
	}
}

int Ordinary_LS (const Matrix &Ym, const Matrix &Ys, RowVector &Slope, RowVector &Bias)
{
	int n_spec = Ym.Nrows();
	int n_vars = Ym.Ncols();

	if (n_spec != Ys.Nrows()  ||  n_vars != Ys.Ncols()  )
                                                  return -1;
   //
	Slope.ReSize(n_vars);
	Bias.ReSize(n_vars);

	double mm, ms;
	double stt, sty;

	// for debug
//	RowVector vSigma(n_vars);

	for (int ivar = 1; ivar <= n_vars; ivar++)
	{
		mm = 0.0;
		ms = 0.0;
		int ispec;

		for (ispec = 1; ispec <= n_spec; ispec++)
		{
			mm += Ym(ispec, ivar);
			ms += Ys(ispec, ivar);
		}

		mm /= n_spec;
		ms /= n_spec;

		stt = 0.0;
		sty = 0.0;

		double ti;

		for (ispec = 1; ispec <= n_spec; ispec++)
		{
			ti = Ys(ispec, ivar) - ms;
			stt += ti * ti;
			sty += ti * Ym(ispec, ivar);
		}

		double s ,b;

		s = sty / stt;
		Slope(ivar) = s;
		b = mm - s * ms;
		Bias(ivar) = b;

		// for debug
/*		double fSigma, dif;

		fSigma = 0;
		for (ispec = 1; ispec <= n_spec; ispec++)
		{
			dif = (Ys(ispec, ivar) - b - s * Ym(ispec, ivar));
			fSigma += dif * dif;
		}

		vSigma(ivar) = sqrt(fSigma / n_spec);*/
	}

//	PrintMatrix(vSigma, "vSigma.dat");
	return 0;
}


Matrix CorrectMatrix(const Matrix &Ys, const RowVector &Slope, const RowVector &Bias)
{
	int n_spec = Ys.Nrows();
	int n_vars = Ys.Ncols();

	//if (n_vars != Slope.Ncols()) return -1;
	Matrix Ym(n_spec, n_vars);

	double s, b;

	for(int ivar = 1; ivar <= n_vars; ivar++ )
	{
		for (int ispec = 1; ispec <= n_spec; ispec++ )
		{
			s = Slope(ivar);
			b = Bias(ivar);
			Ym(ispec, ivar) = b + s * Ys(ispec, ivar);
		}
	}

	return Ym;
}

void NormMax(Matrix & mX)
{// ������������ ������ ������ ������� �� ������������ ��������
//	int numRows, numCols;

//	numRows = mX.Nrows();
//	numCols = mX.Ncols();

	double fMax;

/*	for (int i = 1; i <= numRows; i++)
	{
		fMax = (mX.Row(i)).MaximumAbsoluteValue();
		mX.Row(i) /= fMax;
	}*/

	fMax = mX.MaximumAbsoluteValue();
	mX /= fMax;
}

void NormMax(Matrix &mX, double &fMax)
{// ������������ ������ ������ ������� �� ������������ ��������
//	int numRows, numCols;

//	numRows = mX.Nrows();
//	numCols = mX.Ncols();


/*	for (int i = 1; i <= numRows; i++)
	{
		fMax = (mX.Row(i)).MaximumAbsoluteValue();
		mX.Row(i) /= fMax;
	}*/

	fMax = mX.MaximumAbsoluteValue();
	mX /= fMax;
}

VDbl Row2VDbl(const RowVector &vRow)
{
	VDbl vRes;

	int n = vRow.Ncols();

//	char str[256];
//	sprintf(str, "numCols = %d\n", n);
//	AppendToDebugReport(str);

	if (n)
	{
		vRes.resize(n);

		for (int i = 0; i < n; i++)
		{
			vRes[i] = vRow(i+1);
		}
	}

	return vRes;
}

VDbl Column2VDbl(const ColumnVector &vColumn)
{
	VDbl vRes;

	int n = vColumn.Nrows();

//	char str[256];
//	sprintf(str, "numRows = %d\n", n);
//	AppendToDebugReport(str);

	if (n)
	{
		vRes.resize(n);

		for (int i = 0; i < n; i++)
		{
			vRes[i] = vColumn(i+1);
		}
	}

	return vRes;
}

MDbl Matrix2MDbl(const Matrix &mMatrix)
{
	MDbl mRes;

	int nRow = mMatrix.Nrows();
	int nCol = mMatrix.Ncols();

    if (nRow)
	{
		mRes.resize(nRow);

		for (int i = 0; i < nRow; i++)
		{
			mRes[i].resize(nCol);
			
			for (int k = 0; k < nCol; k++)
			{
				mRes[i][k] = mMatrix(i+1, k+1);
			}
		}
	}

	return mRes;
}

// another version of these function
void Row2VDbl(const RowVector &vRow, VDbl &vRes)
{
	int n = vRow.Ncols();

//	char str[256];
//	sprintf(str, "numCols = %d\n", n);
//	AppendToDebugReport(str);

	if (n)
	{

		//vRes.resize(n);
		vRes.clear();

		for (int i = 0; i < n; i++)
		{
			//vRes[i] = vRow(i+1);
			vRes.push_back(vRow(i+1));
		}
	}

	return;
}

void Column2VDbl(const ColumnVector &vColumn, VDbl &vRes)
{
	int n = vColumn.Nrows();

//	char str[256];
//	sprintf(str, "numRows = %d\n", n);
//	AppendToDebugReport(str);

	if (n)
	{
		//vRes.resize(n);
		vRes.clear();


		for (int i = 0; i < n; i++)
		{
			//vRes[i] = vColumn(i+1);
			vRes.push_back(vColumn(i+1));
		}
	}

	return;
}

int Column2VDbl(const ColumnVector &vColumn, VDbl *vRes)
{
	int nRow = vColumn.Nrows();
	int nRowVDbl = vRes->size();

	if (nRow != nRowVDbl)
	{
		return -1;
	}


	if (nRow)
	{
		for (int i = 0; i < nRow; i++)
		{
			(*vRes)[i] = vColumn(i+1);
		}
	}

	return 0;
}

void Matrix2MDblResize(const Matrix &mMatrix, MDbl &mRes)
{
	int nRow = mMatrix.Nrows();
	int nCol = mMatrix.Ncols();

	mRes.clear();

    if (nRow)
	{
		mRes.resize(nRow);

		for (int i = 0; i < nRow; i++)
		{
			mRes[i].clear();

			if (nCol)
			{
				//VDbl VTmp;
				mRes[i].resize(nCol);
			
				for (int k = 0; k < nCol; k++)
				{
					mRes[i][k] = mMatrix(i+1, k+1);
					//VTmp.push_back(mMatrix(i+1, k+1));
					//mRes.push_back(VTmp);
					//VTmp.clear();
				}
			}
		}
	}

	return;
}

int Matrix2MDbl(const Matrix &mMatrix, MDbl *mRes)
{
	int nRow = mMatrix.Nrows();
	int nCol = mMatrix.Ncols();

	int nRowMDbl = mRes->size();
	int nColMDbl = (*mRes)[0].size();

	if ((nRow != nRowMDbl) && (nCol != nColMDbl))
	{
		return -1;
	}

	for (int i = 0; i < nRow; i++)
	{
		for (int k = 0; k < nCol; k++)
		{
			(*mRes)[i][k] = mMatrix(i+1, k+1);
		}
	}

	return 0;
}

RowVector VDbl2Row(const VDbl &vRow)
{
	RowVector vRes;

	int nCol = vRow.size();

	if (nCol)
	{
		vRes.ReSize(nCol);

		for (int k = 0; k < nCol; k++)
		{
			vRes(k+1) = vRow[k];
		}
	}

	return vRes;
}

ColumnVector VDbl2Column(const VDbl &vRow)
{
	ColumnVector vRes;

	int nRow = vRow.size();

	if (nRow)
	{
		vRes.ReSize(nRow);

		for (int k = 0; k < nRow; k++)
		{
			vRes(k+1) = vRow[k];
		}
	}

	return vRes;
}

Matrix MDbl2Matrix(const MDbl &mMatrix)
{
	Matrix mRes;

	int nRow = mMatrix.size();
	int nCol = mMatrix[0].size();

	if (nRow && nCol)
	{
		mRes.ReSize(nRow, nCol);

		for (int i = 0; i < nRow; i++)
		{
			for (int k = 0; k < nCol; k++)
			{
				mRes(i+1, k+1) = mMatrix[i][k];
			}
		}
	}

	return mRes;
}

void Print(const char *strfname, const CQnt_Parcel *pQnt)
{
    //AppendToDebugReport("Start print Qnt");
	FILE *fgh = fopen(strfname, "w");

	fprintf(fgh, "pQnt->m_specPreprocData\n");
//	fprintf(fgh, "strPreprocPar=%s\n", pQnt->m_specPreprocData.strPreprocPar);
//	fprintf(fgh, "fMinFreq=%.1f\n", pQnt->m_specPreprocData.fMinFreq);
//	fprintf(fgh, "fMaxFreq=%.1f\n", pQnt->m_specPreprocData.fMaxFreq);

//	fprintf(fgh, "\npQnt->vSec\n");
//	int n = pQnt->vSEC.size();
//	fprintf(fgh, "Size = %d\n", n);
//	for (int i = 0; i < n; i++)
//	{
//		fprintf(fgh, "vSec[%d]=%.3f\n", i, pQnt->vSEC[i]);
//	}

//	fprintf(fgh, "\npQnt->vR2Stat\n");
//	n = pQnt->vR2Stat.size();
//	fprintf(fgh, "Size = %d\n", n);
//	for (i = 0; i < n; i++)
//	{
//		fprintf(fgh, "vR2Stat[%d]=%.3f\n", i, pQnt->vR2Stat[i]);
//	}

//	fprintf(fgh, "\nvMahalanobisDistances\n");
//	n = pQnt->vMahalanobisDistances.size();
//	fprintf(fgh, "Size = %d\n", n);
//	for (i = 0; i < n; i++)
//	{
//		fprintf(fgh, "vMahalanobisDistances[%d]=%.3f\n", i, pQnt->vMahalanobisDistances[i]);
//	}

	fprintf(fgh, "\npQnt->m_Model\n");
	fprintf(fgh, "pQnt->m_Model.nModelType = %d\n", pQnt->m_Model.nModelType);

	fprintf(fgh, "pQnt->m_Model.mCalibr\n");

	int nRow = pQnt->m_Model.mCalibr.size();
	int nCol = pQnt->m_Model.mCalibr[0].size();
	fprintf(fgh, "nRow = %d\n", nRow);
	fprintf(fgh, "nCol = %d\n", nCol);


	for (int i = 0; i < nRow; i++)
	{
		for (int k = 0; k < nCol; k++)
		{
			//fprintf(fgh, "m_Model.mCalibr[%d][%d]=%.3f\n", i, k, pQnt->m_Model.mCalibr[i][k]);
			fprintf(fgh, "%.3f\n", pQnt->m_Model.mCalibr[i][k]);
		}
		fprintf(fgh, "\n");
	}



	fclose(fgh);
//    AppendToDebugReport("End print Qnt");
}

void GetTW(int k, int i, const Matrix &mW, const Matrix &mP, ColumnVector &vTW)
{
	// calculate TkWi
	// ��� Tk+1 = Tk(I-mW.Column(k)*mP.Row(i)
	//AppendToDebugReport("Start GetTW");
	if (k == 0)
	{
		vTW = mW.Column(i);
		//AppendToDebugReport("GetTW k=0");
	}
	else
	{
		double fS;
		fS = (mP.Row(k)*mW.Column(i)).AsScalar();
		//AppendToDebugReport("GetTW fS");

		int n = vTW.Nrows();
		ColumnVector vTk1Wi(n); // Tk-1Wi
		ColumnVector vTk1Wk(n); // Tk-1Wk
		GetTW(k-1, i, mW, mP, vTk1Wi);
		//AppendToDebugReport("GetTW k-1 i");

		GetTW(k-1, k, mW, mP, vTk1Wk);
		//AppendToDebugReport("GetTW k-1 k");

		vTW = vTk1Wi - fS*vTk1Wk;
		//AppendToDebugReport("GetTW vTW");
	}
	//AppendToDebugReport("End GetTW");
}

int CalculateSEVparam(const MDbl *mCompRef, // ������� ����������� �������� ������������ ����������� ������� [numSp x numComp]
                        const MDbl *mCompPred, // ������� ������������� �������� ������������ ����������� ������� [numSp x numComp]
                        int numFact, // ����� �������� ������
                        VDbl *pvSEV, // ������ ������� numComp
                        VDbl *pvSDV, // ������ ������� numComp
                        VDbl *pvR2SEV, // ������ ������� numComp
                        VDbl *pvErr) // ������ ������� numComp 
{
	int nError = 0;

	int numSp, numComp;
	numComp = mCompRef->size();
	numSp = (*mCompRef)[0].size();


	if (numSp && numComp)
	{
		Matrix mErrors(numSp, numComp);
		Matrix mComponents(numSp, numComp);

		ColumnVector vSEV(numComp), vSDV(numComp), vR2StatSEV(numComp), vErr(numComp);

		for (int i = 0; i < numComp; i++)
		{
			for (int k = 0; k < numSp; k++)
			{
				mErrors(k+1, i+1) = (*mCompPred)[i][k] - (*mCompRef)[i][k];
				mComponents(k+1, i+1) = (*mCompRef)[i][k];
			}
		}

		RowVector vStd2Comp;//(numComp);

		CalculateStd2(mComponents, vStd2Comp);

		for (int iC = 1; iC <= numComp; iC++)
		{
			double fErrSum2;
			fErrSum2 = mErrors.Column(iC).SumSquare();
			vSEV(iC) = sqrt(fErrSum2 / numSp);
			vErr(iC) = mErrors.Column(iC).Sum() / numSp;
			mErrors.Column(iC) -= vErr(iC);
			vSDV(iC) =  sqrt((mErrors.Column(iC)).SumSquare());
			if (numSp > 1) vSDV(iC) /= sqrt(numSp - 1.0);
			vR2StatSEV(iC) = fErrSum2 / vStd2Comp(iC);
		}

		if ((numSp - numFact - 1.0) > 0)
		{
			vR2StatSEV *= -(numSp - 1.0) / (numSp - numFact - 1.0);
			vR2StatSEV += 1.0;
		}
		else
		{
			nError = -2;
			vR2StatSEV = 0.0;
		}

		Column2VDbl(vSEV, pvSEV);
		Column2VDbl(vSDV, pvSDV);
		Column2VDbl(vR2StatSEV, pvR2SEV);
		Column2VDbl(vErr, pvErr);
	}
	else
	{
		nError = -1;
	}

	return nError;
}

