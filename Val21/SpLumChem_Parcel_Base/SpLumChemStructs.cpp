#include "SpLumChemStructs.h"
#include "MatrixUtilities.h"

char strErrorMessage[MAX_ERR_MSG];

void AppendToDebugReport(const char* str)
{
	FILE *fgh = fopen("SpLumChem_debug.txt", "a");
	fprintf(fgh, "%s\n", str);
	fclose(fgh);
}


/*void CSpecPreprocData::Save(FILE* fgh, int clbVersion)
{
	fwrite(&nSpType, sizeof(int), 1, fgh);
	fwrite(&fMinFreq, sizeof(double), 1, fgh);
	fwrite(&fMaxFreq, sizeof(double), 1, fgh);

	SaveString(strPreprocPar, fgh);

	fwrite(&numberOfChannels, sizeof(int), 1, fgh);

	SaveRowVector(meanSpectrum, fgh);
	SaveRowVector(meanSpectrumMSC, fgh);
	SaveRowVector(meanComponents, fgh);
	SaveRowVector(meanStdSpec, fgh);
	SaveRowVector(meanStdComp, fgh);
	SaveRowVector(vComponentCorrections, fgh);

	fwrite(&fMax, sizeof(double), 1, fgh);


	// new method
//	fwrite(&numberOfFreqExcluded, sizeof(int), 1, fgh);

	if (numberOfFreqExcluded > 0)
	{
		fwrite(pFreqExcluded, sizeof(int), numberOfFreqExcluded, fgh);
	}

//	fwrite(pFreqExcluded, sizeof(int), numberOfChannels, fgh);
}*/


/*void CSpecPreprocData::Load(FILE* fgh, int clbVersion)
{
	fread(&nSpType, sizeof(int), 1, fgh);
	fread(&fMinFreq, sizeof(double), 1, fgh);
	fread(&fMaxFreq, sizeof(double), 1, fgh);

	LoadStringMy(strPreprocPar, fgh);

	fread(&numberOfChannels, sizeof(int), 1, fgh);

	LoadRowVector(meanSpectrum, fgh);
	LoadRowVector(meanSpectrumMSC, fgh);
	LoadRowVector(meanComponents, fgh);
	LoadRowVector(meanStdSpec, fgh);
	LoadRowVector(meanStdComp, fgh);
	
	if (LoadRowVector(vComponentCorrections, fgh) > 0)
	{
		bCompCorr = true;
	}
	else
	{
		bCompCorr = false;
	}

	fread(&fMax, sizeof(double), 1, fgh);


	// new method
	if (clbVersion >= nClbNewMethod)
	{
		fread(&numberOfFreqExcluded, sizeof(int), 1, fgh);

		if (numberOfFreqExcluded > 0)
		{
			pFreqExcluded = new int[numberOfFreqExcluded];
			fread(pFreqExcluded, sizeof(int), numberOfFreqExcluded, fgh);
		}
		pFreqExcluded = new int[numberOfChannels];
		fread(pFreqExcluded, sizeof(int), numberOfChannels, fgh);
	}
}*/

void CSpecPreprocData::Copy(const CSpecPreprocData& theData)
{
	nSpType = theData.nSpType;
	fMinFreq = theData.fMinFreq;  // minimum frequency for calibration
	fMaxFreq = theData.fMaxFreq;  // maximum frequency for calibration

	strPreprocPar = theData.strPreprocPar;   // preprocessing parameter string
	strPreprocParTrans = theData.strPreprocParTrans;   // const preprocessing parameter string 

	numberOfChannels = theData.numberOfChannels;

	if (theData.meanSpectrum.Ncols())
	{
		meanSpectrum = theData.meanSpectrum;
	}

	if (theData.meanSpectrumMSC.Ncols())
	{
		meanSpectrumMSC = theData.meanSpectrumMSC; // for mult. scat. correction (C) 
	}
	
	if (theData.meanComponents.Ncols())
	{
		meanComponents = theData.meanComponents;
	}

	if (theData.meanComponentsTrans.Ncols())
	{
		meanComponentsTrans = theData.meanComponentsTrans;
	}


	if (theData.meanStdSpec.Ncols())
	{
		meanStdSpec = theData.meanStdSpec;
	}

	if (theData.meanStdComp.Ncols())
	{
		meanStdComp = theData.meanStdComp;
	}

//	if (theData.vComponentCorrectionsAdd.Ncols())
//	{
//		vComponentCorrectionsAdd = theData.vComponentCorrectionsAdd; // corrections for every component
//	}

//	if (theData.vComponentCorrectionsMul.Ncols())
//	{
//		vComponentCorrectionsMul = theData.vComponentCorrectionsMul; // corrections for every component
//	}

	numberOfExcludedFreq = theData.numberOfExcludedFreq;

	nModelType = theData.nModelType;

	if (theData.vUseFreq.size())
	{
		vUseFreq.clear();
		vUseFreq.resize(theData.vUseFreq.size());
		vUseFreq = theData.vUseFreq;
	}

//	bCompCorr = theData.bCompCorr;

	fMax = theData.fMax;
}

void CSpecPreprocData::Print(const char* fileName) const
{
	FILE *fgh = fopen(fileName, "w");

	fprintf(fgh, "fMinFreq = %.3f\n", fMinFreq);
	fprintf(fgh, "fMaxFreq = %.3f\n", fMaxFreq);
	fprintf(fgh, "nSpType = %d\n", nSpType);
	fprintf(fgh, "numberOfChannels = %d\n", numberOfChannels);
	fprintf(fgh, "strpreprocPar = %s\n", strPreprocPar.c_str());
	fprintf(fgh, "strpreprocParTrans = %s\n", strPreprocParTrans.c_str());
	fprintf(fgh, "numberOfExcludedFreq = %d\n", numberOfExcludedFreq);
	fprintf(fgh, "nModelType = %d\n", nModelType);
	fprintf(fgh, "fMax = %.3f\n", fMax);


	fprintf(fgh, "meanComponents:\n");
	PrintMatrix(meanComponents, fgh);

	fprintf(fgh, "meanSpectrum:\n");
	PrintMatrix(meanSpectrum, fgh);

	fprintf(fgh, "meanSpectrumMSC:\n");
	PrintMatrix(meanSpectrumMSC, fgh);

	fprintf(fgh, "meanStdComp:\n");
	PrintMatrix(meanStdComp, fgh);

	fprintf(fgh, "meanStdSpec:\n");
	PrintMatrix(meanStdSpec, fgh);

//	fprintf(fgh, "vComponentCorrections:\n");
//	PrintMatrix(vComponentCorrections, fgh);

	fclose(fgh);
}


void CSpecPreprocData::Clear()
{
	nSpType = 0;
	fMaxFreq = fMinFreq = 0;
	numberOfChannels = 0;
	strPreprocPar = "";
	meanComponents.CleanUp();
	meanSpectrum.CleanUp();
	meanSpectrumMSC.CleanUp();
	meanStdComp.CleanUp();
	meanStdSpec.CleanUp();
//	vComponentCorrectionsAdd.CleanUp();
//	vComponentCorrectionsMul.CleanUp();
	fMax = 1;
	nModelType = 0;
	
	numberOfExcludedFreq = 0;
	vUseFreq.empty();
}

int CSpecPreprocData::LoadFromQnt_Parcel(const CFreqScale *pFreqScale, const CQnt_Parcel *pQnt)
{
	fMinFreq = pFreqScale->fStartFreq;
	fMaxFreq = pFreqScale->fStartFreq + pFreqScale->fStepFreq * (pFreqScale->nNumberOfFreq - 1);

	nSpType = pQnt->m_SpecPreproc.nSpType;
	numberOfChannels = pFreqScale->nNumberOfFreq;
	strPreprocPar = pQnt->m_SpecPreproc.strPreprocPar;
	strPreprocParTrans = pQnt->m_SpecPreproc.strPreprocParTrans;


	int nError = 0;

	for (int iP = 0; iP < strPreprocPar.length(); iP++)
	{
		if (nError == -1) break;

		switch(strPreprocPar[iP])
		{
			case 'm':
			case 'M': 
				// ������� ������������
				if (pQnt->m_specPreprocData.meanComponents.size())
				{
					meanComponents = VDbl2Row(pQnt->m_specPreprocData.meanComponents);
				}
				else
				{
					nError = -1;
					break;
				}

				// ������� ������
				if (pQnt->m_specPreprocData.meanSpectrum.size())
				{
					meanSpectrum = VDbl2Row(pQnt->m_specPreprocData.meanSpectrum);
				}
				else
				{
					nError = -1;
					break;
				}
				break;

			case 'd':
			case 'D':
				// ����������� ���������� ������������
				if (pQnt->m_specPreprocData.meanStdComp.size())
				{
					meanStdComp = VDbl2Row(pQnt->m_specPreprocData.meanStdComp);
				}
				else
				{
					nError = -1;
					break;
				}

				// ����������� ���������� ��������
				if (pQnt->m_specPreprocData.meanStdSpec.size())
				{
					meanStdSpec = VDbl2Row(pQnt->m_specPreprocData.meanStdSpec);
				}
				else
				{
					nError = -1;
					break;
				}
				break;

			case 'c':
			case 'C':
				// ������� ������ ��� ����������������� ���������
				if (pQnt->m_specPreprocData.meanSpectrumMSC.size())
				{
					meanSpectrumMSC = VDbl2Row(pQnt->m_specPreprocData.meanSpectrumMSC);
				}
				else
				{
					nError = -1;
					break;
				}
				break;

			case 'h':
			case 'H': // ������������ �� ��������:
				fMax = pQnt->m_specPreprocData.fMax;
				break;
			default:
				break;
		}
	}

//	if (pQnt->m_SpecPreproc.vCompConcCorrAdd.size())
//	{
//		vComponentCorrectionsAdd = VDbl2Row(pQnt->m_SpecPreproc.vCompConcCorrAdd);
//	}

//	if (pQnt->m_SpecPreproc.vCompConcCorrMul.size())
//	{
//		vComponentCorrectionsMul = VDbl2Row(pQnt->m_SpecPreproc.vCompConcCorrMul);
//	}

	vUseFreq.resize(pFreqScale->nNumberOfFreq, 1);

	if (!pQnt->m_SpecPreproc.vFreqExcluded.empty())
	{

		for(int i = 0; i < pQnt->m_SpecPreproc.vFreqExcluded.size(); i++)
		{
			int ind = pQnt->m_SpecPreproc.vFreqExcluded[i];
			vUseFreq[ind] = 0;
		}
	}
	

	return nError;
}

int CSpecPreprocData::Load(const CFreqScale *pFreqScale, string strPar, const CSpecPreprocData_Parcel *pPreprocData)
{
	fMinFreq = pFreqScale->fStartFreq;
	fMaxFreq = pFreqScale->fStartFreq + pFreqScale->fStepFreq * (pFreqScale->nNumberOfFreq - 1);
	numberOfChannels = pFreqScale->nNumberOfFreq;

	nSpType = STYPE_ABS;
	strPreprocPar = strPar;

	int nError = 0;

	for (int iP = 0; iP < strPreprocPar.length(); iP++)
	{
		if (nError == -1) break;

		switch(strPreprocPar[iP])
		{
			case 'm':
			case 'M': 
				// ������� ������
				if (pPreprocData->meanSpectrum.size())
				{
					meanSpectrum = VDbl2Row(pPreprocData->meanSpectrum);
				}
				else
				{
					nError = -1;
					break;
				}
				break;

			case 'd':
			case 'D':
				// ����������� ���������� ��������
				if (pPreprocData->meanStdSpec.size())
				{
					meanStdSpec = VDbl2Row(pPreprocData->meanStdSpec);
				}
				else
				{
					nError = -1;
					break;
				}
				break;

			case 'c':
			case 'C':
				// ������� ������ ��� ����������������� ���������
				if (pPreprocData->meanSpectrumMSC.size())
				{
					meanSpectrumMSC = VDbl2Row(pPreprocData->meanSpectrumMSC);
				}
				else
				{
					nError = -1;
					break;
				}
				break;

			case 'h':
			case 'H': // ������������ �� ��������:
				fMax = pPreprocData->fMax;
				break;
			default:
				break;
		}
	}

	return nError;
}

/*int CSpecPreprocData::LoadTransFromQnt_Parcel(const CFreqScale *pFreqScale, const CQnt_Parcel *pQnt)
{
	fMinFreq = pFreqScale->fStartFreq;
	fMaxFreq = pFreqScale->fStartFreq + pFreqScale->fStepFreq * (pFreqScale->nNumberOfFreq - 1);
	numberOfChannels = pFreqScale->nNumberOfFreq;


	nSpType = STYPE_ABS;
	strPreprocPar = pQnt->strPreprocParTrans;

	int nError = 0;

	for (int iP = 0; iP < strPreprocPar.length(); iP++)
	{
		if (nError == -1) break;

		switch(strPreprocPar[iP])
		{
			case 'm':
			case 'M': 
				// ������� ������
				if (pQnt->m_specPreprocData.meanSpectrum.size())
				{
					meanSpectrum = VDbl2Row(pQnt->m_specPreprocData.meanSpectrum);
				}
				else
				{
					nError = -1;
					break;
				}
				break;

			case 'd':
			case 'D':
				// ����������� ���������� ��������
				if (pQnt->m_specPreprocData.meanStdSpec.size())
				{
					meanStdSpec = VDbl2Row(pQnt->m_specPreprocData.meanStdSpec);
				}
				else
				{
					nError = -1;
					break;
				}
				break;

			case 'c':
			case 'C':
				// ������� ������ ��� ����������������� ���������
				if (pQnt->m_specPreprocData.meanSpectrumMSC.size())
				{
					meanSpectrumMSC = VDbl2Row(pQnt->m_specPreprocData.meanSpectrumMSC);
				}
				else
				{
					nError = -1;
					break;
				}
				break;

			case 'h':
			case 'H': // ������������ �� ��������:
				fMax = pQnt->m_specPreprocData.fMax;
				break;
			default:
				break;
		}
	}

	return nError;
}*/


void CFreqScale::Save(FILE* fgh)
{
	fwrite(&nNumberOfFreq, sizeof(int), 1, fgh);
	fwrite(&fStepFreq, sizeof(double), 1, fgh);
	fwrite(&fStartFreq, sizeof(double), 1, fgh);
}

void CFreqScale::Load(FILE* fgh)
{
	fread(&nNumberOfFreq, sizeof(int), 1, fgh);
	fread(&fStepFreq, sizeof(double), 1, fgh);
	fread(&fStartFreq, sizeof(double), 1, fgh);
}

void CFreqScale::Print(const char* fileName) const
{
	FILE * fgh = fopen(fileName, "a");

	fprintf(fgh, "fStartFreq = %.3f\n", fStartFreq);
	fprintf(fgh, "fSptepFreq = %.3f\n", fStepFreq);
	fprintf(fgh, "nNumberOfFreq = %d\n", nNumberOfFreq);

	fclose(fgh);
}



