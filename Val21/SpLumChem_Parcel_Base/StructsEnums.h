#ifndef _STRUCTSENUMS_H_
#define _STRUCTSENUMS_H_

const int STYPE_TRA = 0;
const int STYPE_ABS = 1;

const int MODEL_PLS = 0;
const int MODEL_PCR = 1;
const int MODEL_MLR = 2;


// Description:
// structure, which contains the info
// for spectrum preprocessing

struct CSpecPreproc
{
public:
	int nSpType; // spectrum type - transmittance or absorbance
	double fMinFreq;  // minimum frequency for calibration
	double fMaxFreq;  // maximum frequency for calibration
	char strPreprocPar[32];   // preprocessing parameter string
};

// Description:
// structure, which contains the info
// about spectrum frequency scale

struct CFreqScale
{
public:
        int nNumberOfFreq; // number of Frequencies
        double fStepFreq;  // frequency step [cm**(-1)]
        double fStartFreq; // start frequency
};


// Description:
// structure, which contains the info
// about sample

struct CSampleProxy
{
public:
        const char* strSampleName; // sample name

        CFreqScale frScale; // descriptor of spectrum - the same for all spectra

        int nNumberOfSpectra; // number of spectra in the sample
        const char* *strSpectrumName; // array of spectra names 
        const double* *fSpectrum; // array of spectra

        int nNumberOfComponents; // number of components in the sample
        const char* *strCompName; // array of component names
        const double* *fCompConc; // array of components concentration
};

#endif // _STRUCTSENUMS_H_