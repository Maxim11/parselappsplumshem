//---------------------------------------------------------------------------
#ifndef _MODELHQO_H_
#define _MODELHQO_H_
#include "SpLumChemStructs.h"



//---------------------------------------------------------------
// ��������� ������
struct ParError
 {
  int kod;//���
  int prFat;//������� ��������� ������
  char text[120];//������������
  ParError(){kod=0;prFat=0;}
 };

//--------------------------------- ����� ����������� ����������
class CModelHQO : public CModel
{
private:
	int numComp;//����� ���������(�������� � ������� ���������)
	int numSp;//����� ��������(����� � ������� ��������)
//	int numFact;//����� �������� (���������� ������)
	//ParError error;//��������� ������
	int nError;
	int  numFreqExcluded;//����� ����������� ������

public:
	//������������ �������
	CModelHQO();
	//������� � 1 ����������� ����������
	CModelHQO(const Matrix &mSpectra, const Matrix &mComponents, const int *UseFreq, const MODEL_MMP *pModelPar);

	//������� � 2 ���������� ��� ������
	int GetError() const;

	//������� � 3 ���������� ����� ����������(��������) ������
	int GetNumVariables() const;

	//������� � 4 ���������� ��� ������
	//���������� �������� ����� ������������� ������� PpCalibrLinear[numComp][numFreq]
	int GetCalibrLinear(Matrix &mClbLin);

	//������� � 5 ���������� ��� ������
	//���������� ���������� ����� ������������� ������� PpCalibrNonLinear[numComp][numFreq]
	int GetCalibrNonLinear(Matrix &mClbNonLin);

	//������� � 6 �������� ����� ������������ ������������� ������.
	// ���������� - ��������� �� ��������� ���������� ������.
	//(��������� ������ � ������������� ������� ����������� �� �����
	// � ���������� � HSO.dll ��� �������� ����������� �������, ������������
	// ����� ������. ������ � dll)
	void Load(const Matrix &mClbLin, const Matrix &mClbNonLin,
			const int *pUseFreq, const MODEL_MMP *pModelPar);


	//������� � 7  ������������ � ������� ������������ ������
	void Predict(const Matrix &mSpectra, Matrix &mComp);

	int GetModelType() const { return parModel.Type; }


	void CalculateMahalanobisDistance(const Matrix& mSp, ColumnVector& vMahDist) {};
	void GetMahalanobisDistance(ColumnVector& vMahDist) const {};
	void GetMatrixScores(Matrix &mS) const {};
	void FillModel_Parcel(CModel_Parcel& mModel) const {};
	void LoadFromQnt_Parcel(const CModel_Parcel& mModel) {};
	void GetModelPar(MODEL_MMP& ModelPar) const {};
	void GetUseFreq(int *pUseFreq) const {};

	Matrix mCalibrLinear;//�������� ����� ������������� ������� [numComp][numFreq]
	Matrix mCalibrNonLinear;//���������� ����� ������������� ������� [numComp][numFreq]
	Matrix mGarm;//������� �������������� �������������� [numFreq][numFact]

	MODEL_MMP parModel;//���������, ���������� ��������� ������
	int *useFreq;//������ ��������� ������������ ������[numFreq]
	int numFreq;//����� ������ (�������� � ������� ��������)

	//������� � 8 ����������
	~CModelHQO();


	//��� �����������
	int SetFrequencies(int nFreq, const int *pUseFreq);
	int SetHarmonics(int numHarmonics);
	int SetCnl(double fCnl) { parModel.knl = fCnl; return nError; }
	int SetShc(double fShc) { parModel.sizeGcub = fShc; return nError; }
	int CreateModel(const Matrix& mX, const Matrix& mY);
};
#endif // _MODEL_HQ_
