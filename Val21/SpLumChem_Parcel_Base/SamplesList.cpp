#include "SamplesList.h"
#include "Dfx.h"
#include "SpLumChemStructs.h"
#include <stdio.h>

// ������ ������ �� �������� �� lst-�����
CSamplesList::CSamplesList(const char* pLstName, bool flagExt)
{
	nError = 0;
	numberOfSamples = 0;
	numberOfComponents = 0;
	pSamplesNames = 0;
	pComponentsNames = 0;


	FILE* fgh = fopen(pLstName, "r");

	if (fgh == NULL)
	{
		nError = -1;
	}
	else
	{
		// read components

		char keyComp[128];
		char sectionComp[128];

		strcpy(sectionComp, "ComponentList");
		strcpy(keyComp, "numberOfComponents");

		numberOfComponents = DfxGetProfileInt(pLstName, sectionComp, keyComp, 0);

		pComponentsNames = new string[numberOfComponents];

		int nC = 0;

		for (int i = 1; i <= numberOfComponents; i++)
		{
			sprintf(keyComp, "Component%d", i);

			char *pstrCompName = DfxGetProfileString(pLstName, sectionComp, keyComp, "no");

			if (strcmp(pstrCompName, "no") != 0)
			{
				pComponentsNames[nC] = pstrCompName;
				nC++;
			}
		}

		numberOfComponents = nC;

		if (numberOfComponents == 0)
		{
			_snprintf(strErrorMessage, MAX_ERR_MSG, "Number of components in %s less or equals 0", pLstName);
			nError = -1;
			return;
		}

		// read samples
		char section[128], key[128];

		if (flagExt)
		{
			strcpy(section, "ExtSampleList");
			strcpy(key, "numberOfSamples");
		}
		else
		{
			strcpy(section, "SampleList");
			strcpy(key, "numberOfSamples");
		}

		numberOfSamples = DfxGetProfileInt(pLstName, section, key, 0);

		if (numberOfSamples == 0)
		{
			if (flagExt)
			{
				_snprintf(strErrorMessage, MAX_ERR_MSG, "Number of external samples in %s equals 0", pLstName);
			}
			else
			{
				_snprintf(strErrorMessage, MAX_ERR_MSG, "Number of samples in %s equals 0", pLstName);
			}


			nError = -1;
			return;
		}

		pSamplesNames = new string[numberOfSamples];

		int nS = 0;

		for (i = 1; i <= numberOfSamples; i++)
		{
			sprintf(key, "Sample%u", i);

			char *pstrSampleName = DfxGetProfileString(pLstName, section, key, "no");

			if (strcmp(pstrSampleName, "no") != 0)
			{
				pSamplesNames[nS] = DfxGetProfileString(pLstName, section, key, "no");
				nS++;
			}
		}

		numberOfSamples = nS;

		if (numberOfSamples == 0)
		{
			if (flagExt)
			{
				_snprintf(strErrorMessage, MAX_ERR_MSG, "Number of external samples in %s equals 0", pLstName);
			}
			else
			{
				_snprintf(strErrorMessage, MAX_ERR_MSG, "Number of samples in %s equals 0", pLstName);
			}


			nError = -1;
			return;
		}
	}
}

CSamplesList::~CSamplesList()
{
	delete[] pSamplesNames;
	delete[] pComponentsNames;
}