#ifndef _HSOMATRIXUTILS_H_
#define _HSOMATRIXUTILS_H_

#include <limits>
#include "newmat.h"

static double Epsilon()         // smallest number such that 1+Eps!=Eps
      { return numeric_limits<double>::epsilon(); }

int lemke1 (double  *c, double  *d, double  *xd, int m, double  *x, bool STOP);

int HCQP(const Matrix &mX, const ColumnVector &vY, double D, double Knl, ColumnVector &vPGarm);

#endif // _HSOMATRIXUTILS_H_
