// QltCalibration.h: interface for the CQltCalibration class.
//
// ��������:
// ����� CQltCalibration - ����� ������������ �����������
// ������������ �������� ������������ �����������,
// ������ ���. ����������� � qlt-����, ������
// ���. ����������� �� qlt-�����, � ������������ ��������
// (���������� ���������� ������������).

#ifndef _QLTCALIBRATION_H_
#define _QLTCALIBRATION_H_

#include <string>
#include "newmat.h"
#include "ModelPCA.h"
#include "CSamples.h"
#include "TransData.h"

using namespace std;

#include "SpLumChemStructs.h"


class CQltCalibration
{
public:
	CQltCalibration(vector<CSampleProxy> *pSamples, // array of samples
					const CFreqScale *FreqScale, // ��������� ����� ��������
					const CSpecPreproc *prePar, // preprocessing parameters
					int numFactors,		// number of factors
					CQnt_Parcel **pQntOut); // created model

/*	void AddSamples(int numSamples,					// number of samples
					const CSampleProxy* pSamples, // array of samples
					const CSpecPreproc &prePar,		// preprocessing parameters
					int numFactors = 0);			// number of factors*/

//	CQltCalibration(const char* fileName);
	void Transfer(CTransData& theTransData);

	~CQltCalibration();

	int GetNumSamples() const { return pClbSamples->GetNumSamples(); }
	int GetNumSampleSpectra(int nSample) const;


	void GetSampleDistances(int nSample,  VDbl *vNormMahDistances) const;
	double GetSampleDistance(int nSample);

	//void GetSampleNormDistances(int nSample, double* dist);
	void GetSampleNormDistances(int nSample, VDbl *vNormMahDistances); // array of normalized distances to be returned

	double GetSampleNormDistance(int nSample);
	void GetSampleSpecDistance(int nSample, int nSpec, double* dist) const;

	int GetNumVariables() const { return pModel->GetNumFactors(); }

	int GetError() const { return nError; }

	void FillQnt(CQnt_Parcel *pQnt);


//	int Save(const char* fileName);
//	int Load(const char* fileName);


	double CalculateMahalanobisDistance(const CFreqScale *dsc, // spectrum descriptor
						const VDbl *vSp);

	double CalculateNormMahalanobisDistance(const CFreqScale *dsc, // spectrum descriptor
						const VDbl *vSp);

	void GetMatrixScores(Matrix &mS) const { pModel->GetMatrixScores(mS); }


//	const char* GetSampleName(int nSample);
//	const char* GetSampleSpecName(int nSample, int nSpec);

	Matrix mX;							// ������� ����������������
										// �������� ��������������� ������
	CSpecPreprocData specPreprocData;	// ������ �������������

	CQnt_Parcel *pQnt; // ����������� ����������

protected:
	int nError;				// ��� ������
	int numberOfFactors;	// ����� ��������
	double fMeanMahDist;	// ������� ���������� ������������
							// ��������������� ������

	CSamples* pClbSamples;	// �������������� �������

	Matrix mCalibr;			// ������������� �������

	ColumnVector vMahalanobisDistances; // ������ ���������� ������������ 
										// �������������� ������

	CFreqScale freqScale;	// ��������� ����� ��������

	CModelPCA *pModel;		// ������������� ������

	CTransData* pTransData; // ������ �� �������� �����������

	int Calibrate();
	int CalculateMahDist();

private:
	void Init();
};


#endif // _QLTCALIBRATION_H_
