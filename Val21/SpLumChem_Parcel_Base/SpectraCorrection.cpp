#include "SpectraCorrection.h"
#include "MatrixUtilities.h"
#include <string.h>

void CSpectraCorrection::Init()
{
	nError = 0;
	pSamplesClb = 0;
	pSamplesMaster = 0;
	pSamplesSlave = 0;
}

CSpectraCorrection::CSpectraCorrection(vector<CSampleProxy> *pSmplsClbMaster, // array of calibration Master samples
					vector<CSampleProxy> *pSmplsMaster, // array of Master samples
					vector<CSampleProxy> *pSmplsSlave, // array of Slave samples
					const CFreqScale *freqScale) // частотная шкала спектров)
{
	Init();

	mFreqScale = *freqScale;

	pSamplesClb = new CSamples(pSmplsClbMaster, freqScale, 0);

	nError = pSamplesClb->GetError();

	if (nError != ER_OK) return;

	pSamplesMaster = new CSamples(pSmplsMaster, freqScale, 0);

	nError = pSamplesMaster->GetError();

	if (nError != ER_OK) return;

	pSamplesSlave = new CSamples(pSmplsSlave, freqScale, 0);

	nError = pSamplesSlave->GetError();

	if (nError != ER_OK) return;


	specPreprocData.fMinFreq = freqScale->fStartFreq;
	specPreprocData.fMaxFreq = freqScale->fStartFreq + freqScale->fStepFreq * (freqScale->nNumberOfFreq - 1);
	specPreprocData.numberOfChannels = freqScale->nNumberOfFreq;

	specPreprocData.nSpType = STYPE_ABS;

	vSlope.ReSize(specPreprocData.numberOfChannels);
	vBias.ReSize(specPreprocData.numberOfChannels);


	return;// 0;
}

CSpectraCorrection::~CSpectraCorrection(void)
{
	delete pSamplesClb;
	delete pSamplesMaster;
	delete pSamplesSlave;
}

int CSpectraCorrection::Correct(string strPreProc)
{
	Matrix mXClb, mXmaster, mXslave;

	int numFreq = specPreprocData.numberOfChannels;

//	RowVector vBias(numFreq), vSlope(numFreq);

	int nCol, numSp;

	double xs, xm, xm2, xms;

	specPreprocData.strPreprocPar = strPreProc;

	nError = pSamplesClb->PreprocessSpectra(specPreprocData, mXClb);

/*	if (strPreProc == "D")
	{
	PrintMatrix(specPreprocData.meanStdSpec, "meanStdSpec.dat");
	}*/
//	PrintMatrix(mXClb, "mXClb.dat");

	if (nError != ER_OK)
	{
//		AppendToDebugReport("Error of Clb spectra preproc");
		return nError;
	}

//	mXClb.CleanUp();

	nError = pSamplesMaster->PreprocessSpectra4Prediction(specPreprocData, mXmaster);

//	PrintMatrix(mXmaster, "mXmaster.dat");

//		AppendToDebugReport("Master spectra preproc");

	if (nError != ER_OK) return nError;

	nError = pSamplesSlave->PreprocessSpectra4Prediction(specPreprocData, mXslave);


//	PrintMatrix(mXslave, "mXslave.dat");

	if (nError != ER_OK) return nError;

//	AppendToDebugReport("Slave spectra preproc");


	nCol = mXmaster.Ncols();

//		if (nCol != mXslave.Ncols()) return -1;

	numSp = mXmaster.Nrows();

//		if (numSp != mXslave.Nrows()) return -1;
	//ColumVector XmXs(numSp);

	// регрессия
	double xms1;

	for (int i = 1; i <= nCol; i++)
	{
		xs = mXslave.Column(i).Sum();
		xm = mXmaster.Column(i).Sum();
		xm2 = mXmaster.Column(i).SumSquare();
		xms = SP(mXmaster.Column(i), mXslave.Column(i)).Sum();

		/*xms = 0;

		for (int k = 1; k <= numSp; k++)
		{
			xms += mXmaster(k, i) * mXslave(k, i);
		}

		if (fabs(xms1 - xms) > 1e-10)
		{
			AppendToDebugReport("Error!");
		}*/

	//xmxs = XmXs.Sum();
		
		vSlope(i) = (numSp*xms - xm * xs) / (numSp * xm2 - xm*xm);
		vBias(i) = (xm2*xs - xms * xm) / (numSp * xm2 - xm*xm);
//		vBias(i) = (xs - vSlope(i) * xm) / numSp;
	}

//	PrintMatrix(vSlope, "vSlope.dat");
//	PrintMatrix(vBias, "vBias.dat");


//	AppendToDebugReport("Regression was done");

	//вычисление декартова расстояния

	//ColumnVector vDecartDist(numSp);

	// для предобработки 'A' надо сначала получить новую mXClb
	// без усредненных спектров

	if (specPreprocData.strPreprocPar.find_first_of('A') != string::npos)
	{
		mXClb.CleanUp();
		nError = pSamplesClb->PreprocessSpectra4Prediction(specPreprocData, mXClb);
	}

	mXClbCor = CorrectMatrix(mXClb, vSlope, vBias);

	Matrix mXmasterCor;

	mXmasterCor = CorrectMatrix(mXmaster, vSlope, vBias);

//	AppendToDebugReport("Correction was done");


	//
	int numSamples = pSamplesMaster->GetNumSamples();

	// average samples
	Matrix mXmasterCorAv(numSamples, nCol);
	Matrix mXslaveAv(numSamples, nCol);
	RowVector vTmp(nCol);

	int iSp;
	int nSp;
	for (int k = 0; k < numSamples; k++)
	{
		iSp = pSamplesMaster->GetSampleSpectraIndexes(k);
		nSp = pSamplesMaster->GetSampleSpectraNumbers(k);
		vTmp = 0;
		for(int i = iSp; i < iSp + nSp; i++)
		{
			vTmp += mXmasterCor.Row(i+1);
		}
		vTmp /= nSp;
		
		mXmasterCorAv.Row(k+1) = vTmp;

		vTmp = 0;
		for(int i = iSp; i < iSp + nSp; i++)
		{
			vTmp += mXslave.Row(i+1);
		}
		vTmp /= nSp;

		mXslaveAv.Row(k+1) = vTmp;
	}

//	AppendToDebugReport("Averaging was done");

	//PrintMatrix(mXslaveAv, "mXslaveAv.dat");
	//PrintMatrix(mXmasterCorAv, "mXmasterCorAv.dat");



	double fCubeDiagonalSlave;

	double fSum = 0, fDif;

	/*for (i = 1; i <= nCol; i++)
	{
		fDif = (mXslaveAv.Column(i).Maximum() - mXslaveAv.Column(i).Minimum());
		fSum += fDif * fDif;
	}*/

	
	for (int i = 1; i <= nCol; i++)
	{
		fDif = (mXslave.Column(i).Maximum() - mXslave.Column(i).Minimum());
		fSum += fDif * fDif;
	}

	fCubeDiagonalSlave = sqrt(fSum);

	vDecartDist.ReSize(numSamples);

	for (int k = 1; k <= numSamples; k++)
	{
		vDecartDist(k)= sqrt((mXslaveAv.Row(k) - mXmasterCorAv.Row(k)).SumSquare());
	}

//	PrintMatrix(vDecartDist, "vDecartDist_notNorm.dat");

	vDecartDist /= fCubeDiagonalSlave;


/*	char strBuffer[256];
	sprintf(strBuffer, "fCubeDiagonalSlave = %.3e\n", fCubeDiagonalSlave);
	AppendToDebugReport(strBuffer);

	sprintf(strBuffer, "fDistMin = %.3e\n", vDecartDist.Minimum());
	AppendToDebugReport(strBuffer);

	sprintf(strBuffer, "fDistMax = %.3e\n", vDecartDist.Maximum());
	AppendToDebugReport(strBuffer);

	sprintf(strBuffer, "fDistMean = %.3e\n", vDecartDist.Sum()/numSamples);
	AppendToDebugReport(strBuffer);*/

//	mXmaster.CleanUp();
//	mXslave.CleanUp();
	return nError;
}

void CSpectraCorrection::GetDistances(vector<double> *vDistances, double *fDistMin, double *fDistMax, double *fDistMean)
{
	if (nError == ER_OK)
	{
		int numSamples = vDecartDist.Nrows();

		for (int i = 0 ; i < numSamples; i++)
		{
			(*vDistances).at(i)= vDecartDist(i+1);
		}

		*fDistMin = vDecartDist.Minimum();

		*fDistMax = vDecartDist.Maximum();

		*fDistMean = vDecartDist.Sum() / numSamples;
	}
	else
	{
		*fDistMin = -1.0;
		*fDistMax = -1.0;
		*fDistMean = -1.0;

		//char strBuffer[128];
		//sprintf(strBuffer, "vDistances size = %d\n", (*vDistances).size());
		//AppendToDebugReport(strBuffer);

		for (int i = 0 ; i < (*vDistances).size(); i++)
		{
			(*vDistances)[i] = -1.0;
		}
	}
}

void CSpectraCorrection::GetCorrectedSpectra(vector<CSampleProxy> *pSmplsClbMaster)
{
//	AppendToDebugReport("Correcting spectra");

//	nError = pSamplesClb->PreprocessSpectra(specPreprocData, mXClb);

//	char strBuffer[256];
//
//	sprintf(strBuffer, "mXClbCor numRows = %d, numCols = %d", mXClbCor.Nrows(), mXClbCor.Ncols());
	
//	AppendToDebugReport(strBuffer);

	int numSamples = pSmplsClbMaster->size();

//	sprintf(strBuffer, "Samples: numSamples = %d, numFreq = %d", numSamples, (*pSmplsClbMaster)[0].mSpectra[0].size());
	
//	AppendToDebugReport(strBuffer);

//	PrintMatrix(mXClbCor, "mXClbCor.dat");


	double res;

	int numFreq = mXClbCor.Ncols();

	int iSp = 0;

	for (int i = 0; i < numSamples; i++)
	{
		//CorrectSampleProxy((*pSmplsClbMaster)[i]);
		int numSpec = (*pSmplsClbMaster)[i].mSpectra.size();

		for (int k = 0; k < numSpec; k++ )
		{
			for(int iF = 0; iF < numFreq; iF++)
			{
//				s = vSlope(iF+1);
//				b = vBias(iF+1);
				res = mXClbCor(iSp+1, iF+1);
				(*pSmplsClbMaster)[i].mSpectra[k][iF] = res;
			}
			iSp++;
		}
	}
}

void CSpectraCorrection::GetPreprocData(CSpecPreprocData_Parcel *pPreprocData)
{
//	pPreprocData->nSpType = specPreprocData.nSpType;
//	pPreprocData->strPreprocPar = specPreprocData.strPreprocPar;
//	pPreprocData->fMinFreq = mFreqScale.fStartFreq;
//	pPreprocData->numberOfChannels = mFreqScale.nNumberOfFreq;
//	pPreprocData->fMaxFreq = mFreqScale.fStartFreq + (mFreqScale.nNumberOfFreq - 1) * mFreqScale.fStepFreq;


	int numPreproc = specPreprocData.strPreprocPar.size();

	int numFreq, iF;

	for (int iP = 0; iP < numPreproc; iP++)
	{
		switch(specPreprocData.strPreprocPar[iP])
		{
			case 'm':
			case 'M': // средневзвешенное:

				numFreq = specPreprocData.meanSpectrum.Ncols();
				//pPreprocData->meanSpectrum.resize(numFreq);

				for (iF = 0; iF < numFreq; iF++)
				{
					pPreprocData->meanSpectrum[iF] = specPreprocData.meanSpectrum(iF+1);
				}

			break;
			case 'd':
			case 'D':

				numFreq = specPreprocData.meanStdSpec.Ncols();
				//pPreprocData->meanStdSpec.resize(numFreq);

				for (iF = 0; iF < numFreq; iF++)
				{
					pPreprocData->meanStdSpec[iF] = specPreprocData.meanStdSpec(iF+1);
				}

				break;
			case 'c':
			case 'C':

				numFreq = specPreprocData.meanSpectrumMSC.Ncols();
				//pPreprocData->meanSpectrumMSC.resize(numFreq);

				for (iF = 0; iF < numFreq; iF++)
				{
					pPreprocData->meanSpectrumMSC[iF] = specPreprocData.meanSpectrumMSC(iF+1);
				}

				break;
		}
	}
}



/*int SpectraCorrection(vector<CSampleProxy> *pSmplsClbMaster, // array of calibration Master samples
					vector<CSampleProxy> *pSmplsMaster, // array of Master samples (common)
					vector<CSampleProxy> *pSmplsSlave, // array of Slave samples (common)
					const CFreqScale *freqScale, // частотная шкала спектров
					const CSpecPreproc *prePar, // preprocessing parameters
					double *fDistMin, // минимальное декратово расстояние
					double *fDistMax, // максимальное декартово расстояние
					double *fDistMean) // среднее декартово расстояние
{
	int nError = ER_OK;

	CSpecPreprocData specPreprocData;

	specPreprocData.fMinFreq = freqScale->fStartFreq;
	specPreprocData.fMaxFreq = freqScale->fStartFreq + freqScale->fStepFreq * (freqScale->nNumberOfFreq - 1);
	specPreprocData.numberOfChannels = freqScale->nNumberOfFreq;

	specPreprocData.nSpType = prePar->nSpType;
	specPreprocData.strPreprocPar = prePar->strPreprocPar;

	CSamples *pSamplesClb, *pSamplesMaster, *pSamplesSlave;

	pSamplesClb = new CSamples(pSmplsClbMaster, freqScale);

	Matrix mXClb, mXmaster, mXslave;

	nError = pSamplesClb->PreprocessSpectra(specPreprocData, mXClb);

	if (nError != ER_OK) return nError;

	delete pSamplesClb;

	mXClb.CleanUp();

	nError = pSamplesMaster->PreprocessSpectra4Prediction(specPreprocData, mXmaster);

	if (nError != ER_OK) return nError;

	delete pSamplesMaster;

	pSamplesSlave = new CSamples(pSmplsSlave, freqScale);

	nError = pSamplesSlave->PreprocessSpectra4Prediction(specPreprocData, mXslave);

	if (nError != ER_OK) return nError;

	delete pSamplesSlave;

	RowVector vBias, vSlope;

	int nCol = mXmaster.Ncols();

	if (nCol != mXslave.Ncols()) return -1;

	int numSp = mXmaster.Nrows();

	if (numSp != mXslave.Nrows()) return -1;


	double xs, xm, xm2, xms;

	//ColumVector XmXs(numSp);

	// регрессия
	for (int i = 1; i <= nCol; i++)
	{
		xs = mXslave.Column(i).Sum();
		xm = mXmaster.Column(i).Sum();
		xm2 = mXmaster.Column(i).SumSquare();
		xms = SP(mXmaster.Column(i), mXslave.Column(i)).Sum();
	//xmxs = XmXs.Sum();
		
		vSlope(i) = (numSp*xms - xm * xs) / (numSp * xm2 - xm*xm);
		vBias(i) = (xm2*xs - xms * xm) / (numSp * xm2 - xm*xm);
//		vBias(i) = (xs - vSlope(i) * xm) / numSp;
	}


	//вычисление декартова расстояния

	double fCubeDiagonalSlave;

	double fSum = 0, fDif;

	for (i = 1; i <= nCol; i++)
	{
		fDif = (mXslave.Column(i).Maximum() - mXslave.Column(i).Minimum());
		fSum += fDif * fDif;
	}

	fCubeDiagonalSlave = sqrt(fSum);

	ColumnVector vDecartDist(numSp);

	Matrix mXmasterCor;

	mXmasterCor = Correct(mXmaster, vSlope, vBias);

	for (int k = 1; k <= numSp; k++)
	{
		vDecartDist(k)= (mXslave.Row(k) - mXmasterCor.Row(k)).SumSquare();
	}

	vDecartDist /= fCubeDiagonalSlave;

	*fDistMin = vDecartDist.Minimum();

	*fDistMax = vDecartDist.Maximum();

	*fDistMean = vDecartDist.Sum() / numSp;

	return nError;
}*/


/*int SpectraCorrection(vector<CSampleProxy> *pSmplsClbMaster, // array of calibration Master samples
					vector<CSampleProxy> *pSmplsMaster, // array of Master samples (common)
					vector<CSampleProxy> *pSmplsSlave, // array of Slave samples (common)
					const CFreqScale *freqScale, // частотная шкала спектров
					int nSpType, // тип спектра
					vector<string> *prePar, // array of preprocessing strings
					vector<double> *fDistMin, // array минимальное декартово расстояние
					vector<double> *fDistMax, // array максимальное декартово расстояние
					vector<double> *fDistMean) // array среднее декартово расстояние
{
	//FILE *fgh = fopen("debug.txt", "w");
//	char strBuffer[512];

//	sprintf(strBuffer, "size of prePar = %d\n", prePar->size());
//	AppendToDebugReport(strBuffer);

//	sprintf(strBuffer, "size of fDistMin = %d\n", fDistMin->size());
//	AppendToDebugReport(strBuffer);

//	sprintf(strBuffer, "size of fDistMax = %d\n", fDistMax->size());
//	AppendToDebugReport(strBuffer);

//	sprintf(strBuffer, "size of fDistMean = %d\n", fDistMin->size());
//	AppendToDebugReport(strBuffer);


	int nError = ER_OK;

//	*fDistMin = 0.0;
//	*fDistMax = 0.0;
//	*fDistMean = 0.0;

	CSpecPreprocData specPreprocData;

	specPreprocData.fMinFreq = freqScale->fStartFreq;
	specPreprocData.fMaxFreq = freqScale->fStartFreq + freqScale->fStepFreq * (freqScale->nNumberOfFreq - 1);
	specPreprocData.numberOfChannels = freqScale->nNumberOfFreq;

	specPreprocData.nSpType = nSpType;

	CSamples *pSamplesClb, *pSamplesMaster, *pSamplesSlave;

	pSamplesClb = new CSamples(pSmplsClbMaster, freqScale, 0);

	nError = pSamplesClb->GetError();

//	sprintf(strBuffer, "SamplesClb nError = %d\n", nError);
//	AppendToDebugReport(strBuffer);


	if (nError != ER_OK) return nError;

	pSamplesMaster = new CSamples(pSmplsMaster, freqScale, 0);

	nError = pSamplesMaster->GetError();

//	sprintf(strBuffer, "SamplesMaster nError = %d\n", nError);
//	AppendToDebugReport(strBuffer);

	if (nError != ER_OK) return nError;

	pSamplesSlave = new CSamples(pSmplsSlave, freqScale, 0);

//	sprintf(strBuffer, "SamplesSlave nError = %d\n", nError);
//	AppendToDebugReport(strBuffer);

	nError = pSamplesSlave->GetError();

	if (nError != ER_OK) return nError;


	Matrix mXClb, mXmaster, mXslave;

	int numFreq = specPreprocData.numberOfChannels;

	RowVector vBias(numFreq), vSlope(numFreq);

	int numPrep = prePar->size();

	int nCol, numSp;

	double xs, xm, xm2, xms;

	for (int iPrep = 0; iPrep < numPrep; iPrep++)
	{
		specPreprocData.strPreprocPar = (*prePar)[iPrep];

		AppendToDebugReport(prePar->at(iPrep).c_str());


		nError = pSamplesClb->PreprocessSpectra(specPreprocData, mXClb);

//		AppendToDebugReport("Clb spectra preproc");


		if (nError != ER_OK)
		{
			AppendToDebugReport("Error of Clb spectra preproc");
			return nError;
		}

		mXClb.CleanUp();

		nError = pSamplesMaster->PreprocessSpectra4Prediction(specPreprocData, mXmaster);

//		AppendToDebugReport("Master spectra preproc");

		if (nError != ER_OK) return nError;

		nError = pSamplesSlave->PreprocessSpectra4Prediction(specPreprocData, mXslave);

//		AppendToDebugReport("Slave spectra preproc");

		if (nError != ER_OK) return nError;

		nCol = mXmaster.Ncols();

//		if (nCol != mXslave.Ncols()) return -1;

		numSp = mXmaster.Nrows();

//		if (numSp != mXslave.Nrows()) return -1;
		//ColumVector XmXs(numSp);

		// регрессия
		for (int i = 1; i <= nCol; i++)
		{
			xs = mXslave.Column(i).Sum();
			xm = mXmaster.Column(i).Sum();
			xm2 = mXmaster.Column(i).SumSquare();
			xms = SP(mXmaster.Column(i), mXslave.Column(i)).Sum();
		//xmxs = XmXs.Sum();
			
			vSlope(i) = (numSp*xms - xm * xs) / (numSp * xm2 - xm*xm);
			vBias(i) = (xm2*xs - xms * xm) / (numSp * xm2 - xm*xm);
	//		vBias(i) = (xs - vSlope(i) * xm) / numSp;
		}


		//вычисление декартова расстояния

		double fCubeDiagonalSlave;

		double fSum = 0, fDif;

		for (i = 1; i <= nCol; i++)
		{
			fDif = (mXslave.Column(i).Maximum() - mXslave.Column(i).Minimum());
			fSum += fDif * fDif;
		}

		fCubeDiagonalSlave = sqrt(fSum);

		ColumnVector vDecartDist(numSp);

		Matrix mXmasterCor;

		mXmasterCor = CorrectMatrix(mXmaster, vSlope, vBias);

		for (int k = 1; k <= numSp; k++)
		{
			vDecartDist(k)= (mXslave.Row(k) - mXmasterCor.Row(k)).SumSquare();
		}

		vDecartDist /= fCubeDiagonalSlave;

		(*fDistMin).at(iPrep) = vDecartDist.Minimum();

		(*fDistMax).at(iPrep) = vDecartDist.Maximum();

		(*fDistMean).at(iPrep) = vDecartDist.Sum() / numSp;

//		sprintf(strBuffer, "fDistMin = %.3e\n", (*fDistMin).at(iPrep));
//		AppendToDebugReport(strBuffer);

//		sprintf(strBuffer, "fDistMax = %.3e\n", (*fDistMax).at(iPrep));
//		AppendToDebugReport(strBuffer);

//		sprintf(strBuffer, "fDistMean = %.3e\n", (*fDistMean).at(iPrep));
//		AppendToDebugReport(strBuffer);



		mXmaster.CleanUp();
		mXslave.CleanUp();
	}

	return nError;
}*/

