// SpLumChemStructs.h: interface for the following structures:
//
// CSpecPreproc - ���������, ���������� �������������
//					��� ���������� �����������
// 
// CFreqScale - ���������, ���������� ������ � ��������� ����� ��������
// 
// CSampleProxy - ���������, ���������� ���������� �� ��������,
//					������� ���������� �� ��������� SpLumPro.exe � Dll ���
//					�������� �����������
//
// CSpecPreprocData - ���������, ���������� ������ ��� ����������� ��������������





#ifndef _STRUCTSENUMS_H_
#define _STRUCTSENUMS_H_

#include "newmat.h"

#include <string>

using namespace std;

// ���� ��������
const int STYPE_TRA = 0; // �����������
const int STYPE_ABS = 1; // ����������

// �������������� ������
const int MODEL_PLS = 0;
const int MODEL_PCR = 1;
const int MODEL_MLR = 2; // �� ������������


// ������������ ����� ��������� �� ������
const int MAX_ERR_MSG = 1024;

// ������ ��������� �� ������
extern char strErrorMessage[];


struct CSpecPreproc
{
public:
	int nSpType;			// ��� ������� - ����������/�����������
	double fMinFreq;		// ������ ������� ���������� ���������
	double fMaxFreq;		// ������� ������� ���������� ���������
	char strPreprocPar[16];	// ������ ���������� �������������
};


struct CFreqScale
{
public:
		double GetEndFreq() const { return fStartFreq + fStepFreq * (nNumberOfFreq - 1); } // end frequency
		void Save(FILE* fgh);
		void Load(FILE* fgh);
		void Print(const char* fileName);

        int nNumberOfFreq;	// ����� ������ � �������
        double fStepFreq;	// �������� ��� �� ������� ������� [��**(-1)]
        double fStartFreq;	// ��������� ������� �������
};



struct CSampleProxy
{
public:
        const char* strSampleName;	// ��� �������

        CFreqScale frScale;			// ��������� ����� ��������

        int nNumberOfSpectra;		// ����� �������� � �������
        const char* *strSpectrumName; // ������ ���� �������� ������� 
        const double* *fSpectrum;	// ������ ��������

        int nNumberOfComponents;	// ����� ��������� � �������
        const char* *strCompName;	// ������ ���� ���������
        double* fCompConc;			// ������ ������������ ���������
        double* fCompConcCorr;		// ������ �������� � ������������ ���������
};


#ifndef _IMPORTING_

enum nErrors {	ER_OK = 0,
				ER_PREDICT,
				ER_PCA,
				ER_PCR,
				ER_FREQ_RANGE,
				ER_MODEL_TYPE,
				ER_FILE_CREATION,
				ER_FILE_OPEN,
				ER_OUT_OF_RANGE,
				ER_NO_COMP,
				ER_BAD_QLT_FILE,
				ER_BAD_QLT_VER,
				ER_BAD_QNT_FILE,
				ER_BAD_QNT_VER,
				ER_POST_PROC,
				ER_DERIV1,
				ER_DERIV2,
				ER_ADD,
				ER_TRANS,
				ER_APJ_NAME,
				ER_APJ_SPEC,
				ER_PLS,
				ER_SEC,
				ER_PREPROC_SP,
				ER_PREPROC_COMP };

struct CSpecPreprocData
{
public:
	int nSpType;		// ��� ������� - ����������/�����������
	double fMinFreq;	// ������ ������� ���������� ���������
	double fMaxFreq;	// ������� ������� ���������� ���������
	string strPreprocPar;	// ������ ���������� �������������
	int numberOfChannels;	// ����� ������ (�������) � ���������

	RowVector meanSpectrum;	// ������� ������ ����. ������
	RowVector meanSpectrumMSC;	// ������� ������ ��� ����������������� ��������
	RowVector meanComponents;	// ������� ������������ ��������� ����. ������
	RowVector meanStdSpec;	// c�������. ���������� ��� �������� ����. ������
	RowVector meanStdComp;	// c�������. ���������� ��� 
							// ������������ ��������� ����. ������
	RowVector vComponentCorrections; // �������� � ������������ ���������

	bool bCompCorr;			// ���� ������� ��� ��������� ��������
							// � ������������� ���������

	void Save(FILE* fgh, int clbVersion);
	void Load(FILE* fgh, int clbVersion);
	void Copy(const CSpecPreprocData &theData);
	void Print(const char* fileName);
	void Clear();
};


#endif // _IMPORTING_



#endif // _STRUCTSENUMS_H_