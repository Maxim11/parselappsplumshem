//#include "stdafx.h"
#include <math.h>
#include <memory.h>

#include "spline.h"



void pre_spline(double *x, double *y, int n, double yp1, double ypn, double *y2)
{
	int i,k;
	double p,qn,sig,un,*u;
	u = new double[n];
	
	if(yp1 > 1e30)
	{
		y2[0] = u[0] = 0;
	}
	else
	{
		y2[0] = -0.5;
		u[0] = (3/(x[1]-x[0]))*((y[1]-y[0])/(x[1]-x[0])-yp1);
	}

	for(i=1; i<n-1; i++)
	{
		sig = (x[i]-x[i-1])/(x[i+1]-x[i-1]);
		p = sig*y2[i-1] + 2;
		y2[i] = (sig - 1)/p;
		u[i] = (y[i+1]-y[i])/(x[i+1]-x[i]) - (y[i]-y[i-1])/(x[i]-x[i-1]);
		u[i] = (6*u[i]/(x[i+1]-x[i-1]) - sig*u[i-1])/p;
	}

	if(ypn > 1e30)
	{
		qn = un = 0;
	}
	else
	{
		qn = 0.5;
		un = (3/(x[n-1]-x[n-2]))*(ypn - (y[n-1]-y[n-2])/(x[n-1]-x[n-2]));
	}

	y2[n-1] = (un-qn*u[n-2])/(qn*y2[n-2]+1);

	for(k=n-2; k>=0; k--)
	{
		y2[k] = y2[k]*y2[k+1] + u[k];
	}

	delete[] u;
}

double spline(double x, double *xa, double *ya, double *y2a, int n)
{

	if(x < xa[0]) return 0;
	if(x > xa[n-1]) return 0;
	
	int k1=0;
	int k2=n-1;
	while(k2-k1 > 1)
	{
		int k = (k2+k1)/2;
		if(xa[k] > x)
			k2 = k;
		else
			k1 = k;
	}

	double h = xa[k2] - xa[k1];
	double a = (xa[k2]-x)/h;
	double b = (x-xa[k1])/h;
	return a*ya[k1] + b*ya[k2] +((a*a*a-a)*y2a[k1] + (b*b*b-b)*y2a[k2])*(h*h)/6;
}


CSpline::CSpline(double *x, double *y, int n, int nallocate, double yp1, double ypn)
{
	nxy = n;
	nalloc = nallocate;

	if(nalloc)
	{
		xa = new double[nxy];
		memcpy(xa, x, nxy*sizeof(double));
		ya = new double[nxy];
		memcpy(ya, y, nxy*sizeof(double));
	}
	else
	{
		xa = x;
		ya = y;
	}


	yp2 = new double[nxy];
	pre_spline(xa, ya, nxy, yp1, ypn, yp2);
}

CSpline::~CSpline()
{
	if(nalloc)
	{
		if(xa) delete [] xa;
		if(ya) delete [] ya;
	}
	if(yp2) delete [] yp2;
}

int CSpline::FindIdx(double x)
{
	if(x < xa[0]) return -1;
	if(x > xa[nxy-1]) return -1;
	
	int k1=0;
	int k2=nxy-1;
	while(k2-k1 > 1)
	{
		int k = (k2+k1)/2;
		if(xa[k] > x)
			k2 = k;
		else
			k1 = k;
	}
	return k1;
}

double CSpline::CalcY(int k, double x)
{
	int k1 = k;
	int k2 = k1+1;

	double h = xa[k2] - xa[k1];
	double a = (xa[k2]-x)/h;
	double b = (x-xa[k1])/h;
	return a*ya[k1] + b*ya[k2] +((a*a*a-a)*yp2[k1] + (b*b*b-b)*yp2[k2])*(h*h)/6;
}

void CSpline::CalcQuot(int k, double *p)
{
	double x1 = xa[k];
	double x2 = xa[k+1];
	double y1 = ya[k];
	double y2 = ya[k+1];
	double dx = xa[k+1] - xa[k];
	double dy2 = yp2[k+1] - yp2[k];

	p[3] = dy2/dx/6.;
	p[2] = (yp2[k] - 6*p[3]*x1)/2;

	double q1 = y1 - p[3]*x1*x1*x1 - p[2]*x1*x1;
	double q2 = y2 - p[3]*x2*x2*x2 - p[2]*x2*x2;
	p[1] = (q2 - q1)/dx;
	p[0] = q1 - p[1]*x1;
}

double CSpline::GetY(double x)
{
	int k = FindIdx(x);
	if(k == -1) return 0;
	return CalcY(k,x);

//	return spline(x, xa, ya, yp2, nxy);
}

double CSpline::GetY1(double x)
{
	int k1 = FindIdx(x);
	if(k1 == -1) return 0;
	int k2 = k1+1;

	double h = xa[k2] - xa[k1];
	double a = (xa[k2]-x)/h;
	double b = (x-xa[k1])/h;
	return (ya[k2]-ya[k1])/h - ((3*a*a-1)*yp2[k1] - (3*b*2-1)*yp2[k2])*h/6;
}

double CSpline::GetY2(double x)
{
	int k1 = FindIdx(x);
	if(k1 == -1) return 0;
	int k2 = k1+1;

	double h = xa[k2] - xa[k1];
	double a = (xa[k2]-x)/h;
	double b = (x-xa[k1])/h;
	return a*yp2[k1] + b*yp2[k2];
}


double CSpline::Integrate(double x1, double x2)
{
	int i1 = FindIdx(x1);
	if(i1 < 0) return 0;
	int i2 = FindIdx(x2);
	if(i2 < 0) return 0;

	double a = 0;
	for(int i=i1; i<=i2; i++)
	{
		double p[4];
		CalcQuot(i,p);
		double xi1 = (i == i1) ? x1 : xa[i]; 
		double xi2 = (i == i2) ? x2 : xa[i+1]; 
		a += p[0]*(xi2 - xi1);
		a += p[1]/2*(xi2*xi2 - xi1*xi1);
		a += p[2]/3*(xi2*xi2*xi2 - xi1*xi1*xi1);
		a += p[3]/4*(xi2*xi2*xi2*xi2 - xi1*xi1*xi1*xi1);
	}
	return a;
}



int CSpline::GetLocalMinMax(int k, double *xmin, double *ymin, double *xmax, double *ymax)
{
	int iMin = 0;
	int iMax = 0;

	if(k < 0) return 0;
	if(k >= nxy-1) return 0;
	
	int k1=k;
	int k2=k+1;

	double h = xa[k2] - xa[k1];

	double a = (yp2[k2] - yp2[k1])/6/h;
	if(a == 0) return 0;

	double b = (xa[k2]*yp2[k1] - xa[k1]*yp2[k2])/2/h;
	double c = ((ya[k2] - ya[k1]) 
				- b*(xa[k2]*xa[k2]-xa[k1]*xa[k1])
				- a*(xa[k2]*xa[k2]*xa[k2]-xa[k1]*xa[k1]*xa[k1]))/h;

	a = a*3;
	b = b*2;
	double det = b*b - 4*a*c;
	if(det < 0) return 0;
	if(det == 0)
	{
		double e1 = -b/2/a;
		if((e1 >= xa[k1]) && (e1 < xa[k2])) 
		{
			double p2 = (xa[k2] - e1)*yp2[k1] + (e1 - xa[k1])*yp2[k2];
			if(p2>0)
			{
				*xmin = e1;
				*ymin = CalcY(k,e1);
				return 1;
			}
			else if(p2 < 0)
			{
				*xmax = e1;
				*ymax = CalcY(k,e1);
				return 2;
			}
			else return 0;
		}
		else 
			return 0;
	}
	else
	{
		double e1 = (-b + sqrt(det))/2/a;
		double e2 = (-b - sqrt(det))/2/a;
		int ne = 0;
		if((e1 >= xa[k1]) && (e1 < xa[k2]))
		{
			double p2 = (xa[k2] - e1)*yp2[k1] + (e1 - xa[k1])*yp2[k2];
			if(p2>0)
			{
				*xmin = e1;
				*ymin = CalcY(k,e1);
				iMin = 1;
			}
			else if(p2 < 0)
			{
				*xmax = e1;
				*ymax = CalcY(k,e1);
				iMax = 1;
			}
		}

		if((e2 >= xa[k1]) && (e2 < xa[k2]))
		{
			double p2 = (xa[k2] - e2)*yp2[k1] + (e2 - xa[k1])*yp2[k2];
			if(p2>0)
			{
				*xmin = e2;
				*ymin = CalcY(k,e2);
				iMin = 1;
			}
			else if(p2 < 0)
			{
				*xmax = e2;
				*ymax = CalcY(k,e2);
				iMax = 1;
			}
		}

		return iMin + 2*iMax;
	}
}


/*
int CSpline::GetLocalExtremes(int k, double *xe1, int *m1, double *xe2, int *m2)
{

	if(k < 0) return 0;
	if(k >= nxy-1) return 0;
	
	int k1=k;
	int k2=k+1;

	double h = xa[k2] - xa[k1];

	double a = (yp2[k2] - yp2[k1])/6/h;
	if(a == 0) return 0;

	double b = (xa[k2]*yp2[k1] - xa[k1]*yp2[k2])/2/h;
	double c = ((ya[k2] - ya[k1]) 
				- b*(xa[k2]*xa[k2]-xa[k1]*xa[k1])
				- a*(xa[k2]*xa[k2]*xa[k2]-xa[k1]*xa[k1]*xa[k1]))/h;

	a = a*3;
	b = b*2;
	double det = b*b - 4*a*c;
	if(det < 0) return 0;
	if(det == 0)
	{
		double e1 = -b/2/a;
		if((e1 >= xa[k1]) && (e1 < xa[k2])) 
		{
			double p2 = (xa[k2] - e1)*yp2[k1] + (e1 - xa[k1])*yp2[k2];
			*m1 = p2 > 0 ? 1 : -1;
			*xe1 = e1;
			return 1;
		}
		else 
			return 0;
	}
	else
	{
		double e1 = (-b + sqrt(det))/2/a;
		double e2 = (-b - sqrt(det))/2/a;
		int ne = 0;
		if((e1 >= xa[k1]) && (e1 < xa[k2]))
		{
			double p2 = (xa[k2] - e1)*yp2[k1] + (e1 - xa[k1])*yp2[k2];
			*m1 = p2 > 0 ? 1 : -1;
			*xe1 = e1;
			ne = 1;
		}

		if((e2 >= xa[k1]) && (e2 < xa[k2]))
		{
			double p2 = (xa[k2] - e2)*yp2[k1] + (e2 - xa[k1])*yp2[k2];
			if(ne == 1) 
			{
				*m2 = p2 > 0 ? 1 : -1;
				*xe2 = e2;
				ne = 2;
			}
			else
			{
				*m1 = p2 > 0 ? 1 : -1;
				*xe1 = e2;
				ne = 1;
			}
		}
		return ne;
	}
}

*/