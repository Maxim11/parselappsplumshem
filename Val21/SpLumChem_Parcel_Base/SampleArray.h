// SampleArray.h: interface for the CSampleArray class.
//
// ��������:
// ����� CSampleArray ������������ ��� ������
// � �������� �������� �� apj-�����
//
// ������������ �������� CSamplesList � CSamplesListExt

#ifndef _SAMPLEARRAY_H_
#define _SAMPLEARRAY_H_

#include <string>

using namespace std;

const int NSAMPLEARRAY_SPEC_MAX = 32;


class CSampleArray
{
public:
	CSampleArray();
	void Init();

	Load(const char *fname, int i, bool flagExt = false);

	int GetSpecNum() const { return m_nSpecNum; }
	int GetSpecIncNum() const { return m_nSpecIncNum; }
	int GetSpecInc(int nIdx) const;
	int GetSpecIndex(int nIdx) const;
	int GetError() const { return nError; }
	string GetSampleFileName() const;

enum SampleErrors{ ER_OK = 0,
					ER_NO_SAMPLE, // no sample with given index "i" was found (sample was excluded)
					ER_FILE_OPEN, // can not open sample file
					ER_APJ_NAME, // no title name specifyed in the sample file
					ER_APJ_SPEC }; // number of spectra in the sample = 0

protected:

	string m_strFileTitle;
	int m_nSpecNum; // number of spectra
	int m_nSpectra[NSAMPLEARRAY_SPEC_MAX]; // spectra indeces

	int m_nSpecIncNum; // number of "on" (included) spectra <= m_nSpectra
	int m_nSpectraInc[NSAMPLEARRAY_SPEC_MAX]; // on/off spectra flags

	int nError;
};



#endif // _SAMPLEARRAY_H_
