#include "SampleFromFile.h"
#include "ApjSampleLoader.h"
#include "SampleUtils.h"
#include "SpLumChemStructs.h"

//#include <afxwin.h>

CSampleFromFile::CSampleFromFile(const char* fname)
{
	Load(fname);
}

void CSampleFromFile::Load(const CSampleArray& pSampleArray)
{
	int nSpec = pSampleArray.GetSpecNum();

	vector<bool> vSpStatus(nSpec);
	vector<int> vSpIndexes(nSpec);

	for (int i = 0; i < nSpec; i++)
	{
		vSpIndexes[i] = pSampleArray.GetSpecIndex(i);

		if (pSampleArray.GetSpecInc(i) == 1)
		{
			vSpStatus[i] = true;
		}
		else
		{
			vSpStatus[i] = false;
		}
	}

	string strSampleName = pSampleArray.GetSampleFileName();

	Load(strSampleName.c_str(), &vSpStatus, &vSpIndexes);
}


void CSampleFromFile::Load(const char* fname)
{

	CApjSampleLoader m_ApjSample(fname);

	nError = m_ApjSample.GetError();

	if (nError != ER_OK)
	{
		return;
	}

	numberOfSpectra = m_ApjSample.GetSpecNum();
	strSampleName = m_ApjSample.GetSampleFileName();
	pstrSpectrumName = new string[numberOfSpectra];
	pfSpectrum = new double*[numberOfSpectra];

//	char sError[256];

	for (int i = 0; i < numberOfSpectra; i++)
	{
		pstrSpectrumName[i] = m_ApjSample.GetSpecFileName(i);

		if (LoadSpectrum((pstrSpectrumName[i]).c_str(), &fStartFreq, &fStepFreq, &numberOfFreq, &(pfSpectrum[i])) == -1)
		{
			_snprintf(strErrorMessage, MAX_ERR_MSG, "Error loading spec. file %s", pstrSpectrumName[i].c_str());
//			sprintf(sError, "Error loading spec. file %s", pstrSpectrumName[i].c_str());
//			AfxMessageBox(sError);
			nError = -1;
			return;
		}
	}

	strRefFileName = m_ApjSample.GetRefDataFileName();

	if (LoadComponents(strRefFileName.c_str(), &numberOfComponents, &pstrCompName, &fCompConc) == -1)
	{
			_snprintf(strErrorMessage, MAX_ERR_MSG,  "Error loading ref. file %s", strRefFileName.c_str());
//			sprintf(sError, "Error loading ref. file %s", strRefFileName.c_str());
//			AfxMessageBox(sError);
			nError = -1;
			return;
	}
}


void CSampleFromFile::Load(const char* fname, const vector<bool> *pvSpStatus)
{

	CApjSampleLoader m_ApjSample(fname);

	nError = m_ApjSample.GetError();

	if (nError != ER_OK)
	{
		return;
	}

	int nSp = 0;

	for (int i = 0; i < pvSpStatus->size(); i++)
	{
		if ((*pvSpStatus)[i]) nSp++;
	}

	if (nSp == 0)
	{
		_snprintf(strErrorMessage, MAX_ERR_MSG, "Error loading sample %s: number of included spectra = 0", fname);
//				sprintf(sError, "Error loading spec. file %s", pstrSpectrumName[i].c_str());
//				AfxMessageBox(sError);
		nError = -1;
		return;
	}

	numberOfSpectra = nSp;

	strSampleName = m_ApjSample.GetSampleFileName();
	pstrSpectrumName = new string[numberOfSpectra];
	pfSpectrum = new double*[numberOfSpectra];

//	char sError[256];

	int iInc = 0;

	for (i = 0; i < pvSpStatus->size(); i++)
	{
		if ((*pvSpStatus)[i])
		{
			pstrSpectrumName[iInc] = m_ApjSample.GetSpecFileName(i);

			if (LoadSpectrum((pstrSpectrumName[iInc]).c_str(), &fStartFreq, &fStepFreq, &numberOfFreq, &(pfSpectrum[iInc])) == -1)
			{
				_snprintf(strErrorMessage, MAX_ERR_MSG, "Error loading spec. file %s", pstrSpectrumName[i].c_str());
//				sprintf(sError, "Error loading spec. file %s", pstrSpectrumName[i].c_str());
//				AfxMessageBox(sError);
				nError = -1;
				return;
			}

			iInc++;
		}
	}

	strRefFileName = m_ApjSample.GetRefDataFileName();

	if (LoadComponents(strRefFileName.c_str(), &numberOfComponents, &pstrCompName, &fCompConc) == -1)
	{
			_snprintf(strErrorMessage, MAX_ERR_MSG,  "Error loading ref. file %s", strRefFileName.c_str());
//			sprintf(sError, "Error loading ref. file %s", pstrSpectrumName[i].c_str());
//			AfxMessageBox(sError);
			nError = -1;
			return;
	}
}

void CSampleFromFile::Load(const char* fname, const vector<bool> *pvSpStatus, const vector<int> *pvSpIndex)
{

	CApjSampleLoader m_ApjSample(fname);

	nError = m_ApjSample.GetError();

	if (nError != ER_OK)
	{
		return;
	}

	int nSp = 0;

	for (int i = 0; i < pvSpStatus->size(); i++)
	{
		if ((*pvSpStatus)[i]) nSp++;
	}

	if (nSp == 0)
	{
		_snprintf(strErrorMessage, MAX_ERR_MSG, "Error loading sample %s: number of included spectra = 0", fname);
//				sprintf(sError, "Error loading spec. file %s", pstrSpectrumName[i].c_str());
//				AfxMessageBox(sError);
		nError = -1;
		return;
	}

	numberOfSpectra = nSp;

	strSampleName = m_ApjSample.GetSampleFileName();
	pstrSpectrumName = new string[numberOfSpectra];
	pfSpectrum = new double*[numberOfSpectra];

//	char sError[256];

	int iInc = 0;
	char buffer[128];

	for (i = 0; i < pvSpStatus->size(); i++)
	{
		if ((*pvSpStatus)[i])
		{
			sprintf(buffer, "%s-%02d.spa", (m_ApjSample.GetFileTitle()).c_str(), (*pvSpIndex)[i]);

			pstrSpectrumName[iInc] = buffer;

			if (LoadSpectrum((pstrSpectrumName[iInc]).c_str(), &fStartFreq, &fStepFreq, &numberOfFreq, &(pfSpectrum[iInc])) == -1)
			{
				_snprintf(strErrorMessage, MAX_ERR_MSG, "Error loading spec. file %s", pstrSpectrumName[i].c_str());
//				sprintf(sError, "Error loading spec. file %s", pstrSpectrumName[i].c_str());
//				AfxMessageBox(sError);
				nError = -1;
				return;
			}

			iInc++;
		}
	}

	strRefFileName = m_ApjSample.GetRefDataFileName();

	if (LoadComponents(strRefFileName.c_str(), &numberOfComponents, &pstrCompName, &fCompConc) == -1)
	{
			_snprintf(strErrorMessage, MAX_ERR_MSG,  "Error loading ref. file %s", strRefFileName.c_str());
//			sprintf(sError, "Error loading ref. file %s", pstrSpectrumName[i].c_str());
//			AfxMessageBox(sError);
			nError = -1;
			return;
	}
}



CSampleFromFile::CSampleFromFile()
{
	strSampleName = ""; // sample name
	nError = ER_OK;

	numberOfSpectra = 0; // number of spectra in the sample
	pstrSpectrumName = NULL; // array of spectra names 
	pfSpectrum = NULL; // array of spectra

	numberOfComponents = 0; // number of components in the sample
	pstrCompName = NULL; // array of component names
	fCompConc = NULL; // array of components concentration
}

CSampleFromFile::~CSampleFromFile()
{
	if (numberOfSpectra > 0)
	{
		for (int i = 0; i < numberOfSpectra; i++)
		{
			delete[] pfSpectrum[i];
		}

		delete[] pfSpectrum;
		delete[] pstrSpectrumName;
	}

	if (numberOfComponents > 0)
	{
		delete[] fCompConc;
		delete[] pstrCompName;
	}
}

bool CSampleFromFile::FindComponent(const string& theCompName) const
{
	bool res = false;

	for (int i = 0; i < numberOfComponents; i++)
	{
		if (theCompName == pstrCompName[i])
		{
			if (fCompConc[i] >= 0) res = true;
			break;
		}
	}

	return res;
}

double* CSampleFromFile::GetSpectrum(int iS, double freqMin)
{
	int shift;

	shift = (fStartFreq - freqMin) / fStepFreq;

	return pfSpectrum[iS] + shift;
}

double* CSampleFromFile::GetSpectrum(int iS)
{
	return pfSpectrum[iS];
}

double CSampleFromFile::GetComponent(const string& theCompName)
{
	double res = 0;

	for (int i = 0; i < numberOfComponents; i++)
	{
		if (theCompName == pstrCompName[i])
		{
			res = fCompConc[i];
			break;
		}
	}

	return res;
}