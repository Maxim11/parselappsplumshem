// ModelHSO.h: interface for the CModelHSO class.
//
// ��������:
// ����� CModelHSO - ����� ��� ������������� ������ 
// Hyper Space Optimization (HSO),  - ��������������������� �����������.
// 
// ������������ �������� CQntCalibration � CQltCalibration
//

#ifndef _MODELHSO_H_
#define _MODELHSO_H_

#include "Model.h"
#include "newmat.h"
#include "SpLumChemStructs.h"
#include "ModelPCA.h"
#include "ModelHQO.h"


class CModelHSO : public CModel
{
public:
	CModelHSO();
	//CModelHSO(const Matrix& mX, const Matrix& mY, const int pIncFreq[], const MODEL_MMP *pModelPar, int numPCAfactors);
	CModelHSO(const Matrix& mX, const Matrix& mY, const int pIncFreq[], const MODEL_MMP *pModelPar);
	~CModelHSO();

	void Predict(const Matrix& mX, Matrix& mYpredicted);
	int GetError() const { return nError; }
	int GetNumVariables() const { return pModelHQO->GetNumVariables(); }
	int GetModelType() const { return pModelHQO->GetModelType(); }
	void GetMatrixScores(Matrix &mS) const {}
	void CalculateMahalanobisDistance(const Matrix& mSp, ColumnVector& vMahDist) { }
	void GetMahalanobisDistance(ColumnVector& vMahDist) const { }

//	void Save(FILE* fgh, int clbVersion) const;
//	void Load(FILE* fgh, int clbVersion);

	void GetModelPar(MODEL_MMP& ModelPar) const { ModelPar = pModelHQO->parModel; }
	void GetUseFreq(int *pIncFreq) const;
//	void FillModel_Parcel(CModel_Parcel& mModel) const { }
	void FillModel_Parcel(CModel_Parcel& mModel) const;
	void LoadFromQnt_Parcel(const CModel_Parcel& mModel) { }

	int SetFrequencies(int nFreq, const int *pUseFreq) {return 0;};
	int SetHarmonics(int numHarmonics) {return 0;};
	int SetCnl(double fCnl) {return 0;};
	int SetShc(double fShc) {return 0;};
	int CreateModel(const Matrix& mX, const Matrix& mY) {return 0;};

protected:
	//MODEL_MMP theModelPar; // ��������� ������ =(*pModelPar);

	CModelHQO *pModelHQO; // ��������� �� ���������� ������, �����������
	              // ����� ������. ������ � HSO.dll

//	CModelPCA *pModelPCA;

	int nError;		// ��� ������
	int nModelType;	// ��� ������
//	int numVar; // ����� ���������� ������

//	Matrix mCalibrLinear; // ������������� ������� (�������� �����)
//	Matrix mCalibrNonLinear; // ������������� ������� (���������� �����)

	//int numFreq;
//	int *pUseFreq; // ������ ������ - 0, 1 ( �� ���./ ���)
};


#endif // _MODELHSO_H_