#ifndef _SAMPLEUTILS_H_
#define _SAMPLEUTILS_H_

#include <string>

using namespace std;


int LoadSpectrum(const char* fname,
				 double* pStartFreq,
				 double* pStepFreq,
				 int* pNumberOfFreq,
				 double* *pfSpectrum);


int	LoadComponents(const char* fname,
				   int* pNumberOfComponents,
				   string* *pstrCompName,
				   double* *pfCompConc);

#endif // _SAMPLEUTILS_H_
