// ApjSampleLoader.cpp: implementation of the CApjSampleLoader class.
//
//////////////////////////////////////////////////////////////////////
#include "Dfx.h"
#include "ApjSampleLoader.h"
#include "SpLumChemStructs.h"


//#ifdef _DEBUG
//#undef THIS_FILE
//static char THIS_FILE[]=__FILE__;
//#define new DEBUG_NEW
//#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

/*CApjSampleLoader::CApjSampleLoader()
{
	m_strTextID = "NoID";
	m_strFileTitle = "NoTitle";
	m_nSpecNum = 0;
	m_nSpecNext = 1;
}*/

/*CApjSampleLoader::CApjSampleLoader(CApjSampleLoader *pSample)
{
	CopySample(*pSample);
}*/

/*CApjSampleLoader::CApjSampleLoader(CApjSampleLoader &Sample)
{
	CopySample(Sample);
}*/

/*CApjSampleLoader::CopySample(CApjSampleLoader &Sample)
{
	m_strTextID = Sample.m_strTextID;
	m_strFileTitle = Sample.m_strFileTitle;
	m_nSpecNum = Sample.m_nSpecNum;
	m_nSpecNext = Sample.m_nSpecNext;
	memcpy(m_nSpectra, Sample.m_nSpectra, m_nSpecNum*sizeof(int));
}*/

CApjSampleLoader::CApjSampleLoader(const char* fname)
{
	if (!DfxFileExist(fname))
	{
		_snprintf(strErrorMessage, MAX_ERR_MSG, "Can not open file %s", fname);

		nError = ER_FILE_OPEN;
	}
	else
	{
		nError = ER_OK;
		GetDfxProfile(fname, "Sample");
	}
}


//CApjSampleLoader::~CApjSampleLoader()
//{
//
//}


/*int CApjSampleLoader::PutDfxProfile(const char *fname, const char *section)
{
	DfxPutProfileString(fname, section,"strTextID", m_strTextID);
	DfxPutProfileString(fname, section,"strFileTitle", m_strFileTitle);
	DfxPutProfileInt(fname, section,"nSpecNum", m_nSpecNum);
	DfxPutProfileInt(fname, section,"nSpecNext", m_nSpecNext);
	DfxPutProfileIntArray(fname, section,"SpecArray", m_nSpecNum, m_nSpectra);
	return 1;
}*/

int CApjSampleLoader::GetDfxProfile(const char *fname, const char *section)
{
	m_strTextID = DfxGetProfileString(fname, section,"strTextID", "NoID");
	m_strFileTitle = DfxGetProfileString(fname, section,"strFileTitle", "NoTitle");
	m_nSpecNum = DfxGetProfileInt(fname, section,"nSpecNum", 0);
	m_nSpecNext = DfxGetProfileInt(fname, section,"nSpecNext", -1);

	if(m_nSpecNum) DfxGetProfileIntArray(fname, section,"SpecArray", m_nSpecNum, m_nSpectra);

	int iOK = 1;
	if(m_nSpecNext == -1)
	{
		m_nSpecNext = m_nSpecNum+1;
		iOK = 0;
	}

	if (m_strFileTitle == "NoTitle")
	{
		_snprintf(strErrorMessage, MAX_ERR_MSG, "strFileTitl was not found in sample %s", fname);

		nError = ER_APJ_NAME;
	}

	if (m_nSpecNum == 0)
	{
		_snprintf(strErrorMessage, MAX_ERR_MSG, "Number of spectra in sample %s equals 0", fname);

		nError = ER_APJ_SPEC;
	}

	return iOK;
}

string CApjSampleLoader::GetSampleFileName()
{
	string str = m_strFileTitle; 
	str += ".saa"; 
	return str;
}

string CApjSampleLoader::GetRefDataFileName()
{
	string str = m_strFileTitle; 
	str += ".ref"; 
	return str;
}

string CApjSampleLoader::GetSpecFileName(int nIdx)
{
	if(nIdx < 0) return "";
	if(nIdx >= m_nSpecNum) return "";
	string str;
	char buffer[128];

	sprintf(buffer, "%s-%02d.spa", m_strFileTitle.c_str(), m_nSpectra[nIdx]);

	str = buffer;
	
	return str;
}

string CApjSampleLoader::GetFileTitle()
{
	return m_strFileTitle;
}





