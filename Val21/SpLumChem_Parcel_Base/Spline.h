// Spline.h: interface for the CSpline class.
//
// �����: �. ���������
//
// ��������:
// ����� CSpline ������������
// ��� ������ � ��-�������� spa-������.
//
// ������������ �������� CIrSpecRaw � CIrSpectrum
//


#ifndef _SPLINE_H_
#define _SPLINE_H_


class CSpline
{
public:
	CSpline(double *x, double *y, int n, int nallocmem=1, double yp1=0, double ypn=0);
	~CSpline();

	int FindIdx(double x);
	double CalcY(int k, double x);
	void CalcQuot(int k, double *p);

	int GetN() {return nxy;}
	double GetY(double x);
	double GetY1(double x);
	double GetY2(double x);
	int GetLocalMinMax(int k, double *xmin, double *ymin, double *xmax, double *ymax);
	double Integrate(double x1, double x2);

	int nxy;
	int nalloc;
	double *xa;
	double *ya;
	double *yp2;
};


#endif //_SPLINE_H_
