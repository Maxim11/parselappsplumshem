#include <windows.h>

#include "SpLumChem.h"

#include "QntCalibration.h"

#include "QltCalibration.h"

#include "Secv.h"

#include "SpectraCorrection.h"

#include "MatrixUtilities.h"

extern void AppendToDebugReport(const char* str);

void log_me(const char* msg, bool bAppend=true)
{
	static char strLogName[256];

	SYSTEMTIME t;
	GetLocalTime(&t);

	if(!bAppend)
	{
		sprintf(strLogName, "SpLumChem_log.txt");
	}

	FILE *f = fopen(strLogName, bAppend ? "at" : "wt");
	if(!f) return;
	fprintf(f, "%04d-%02d-%02d [%02d:%02d:%02d.%03d]  ", t.wYear, t.wMonth, t.wDay, t.wHour, t.wMinute, t.wSecond, t.wMilliseconds);
	fprintf(f, "\t%s\n", msg);
	fclose(f);
}


//#define _DEBUG_


_DLL_API_ void*  IRCalibrate_Create(vector<CSampleProxy> *pSamples, // array of samples
					const CFreqScale *FreqScale, // ��������� ����� ��������
					const CSpecPreproc *prePar, // preprocessing parameters
					int nModelType, // calibartion model type
					int numFactors,  // number of factors
					CQnt_Parcel **pQnt) // model
{

#ifdef _DEBUG_
	char strBuf[256];
	sprintf(strBuf, "IRCalibrate_Create number of Components names = %d", (prePar->strCompName).size());
	AppendToDebugReport(strBuf);
#endif

	return 	(void *) new CQntCalibration(pSamples, FreqScale,
		prePar, nModelType, numFactors, pQnt);

}

_DLL_API_ void*  IRCalibrate_Create(vector<CSampleProxy> *pSamples, // array of samples
					const CFreqScale *FreqScale, // ��������� ����� ��������
					const CSpecPreproc *prePar, // preprocessing parameters
                    const MODEL_MMP *pModelPar,
					 CQnt_Parcel **pQnt) // created model
{

	return 	(void *) new CQntCalibration(pSamples, FreqScale,
		prePar, pModelPar, pQnt);

}

 _DLL_API_ void*  IRCalibrate_Create(vector<CSampleProxy> *pSamples, // array of samples
					const CFreqScale *FreqScale, // ��������� ����� ��������
					const CQnt_Parcel *pQnt) // created model
 {
		return 	(void *) new CQntCalibration(pSamples, FreqScale, pQnt);
 }


// Description:
// Destroy the object of IRCalibrate class

void IRCalibrate_Destroy(void* pCalib)
{
    if (pCalib) delete ((CQntCalibration*)pCalib);
}

// Description:
// Return the number of variables in calibration

int IRCalibrate_GetNumVariables(void* pCalib)
{
	return ((CQntCalibration*)pCalib)->GetNumVariables();
}

// Description:
// Return the number of components in calibration

int IRCalibrate_GetNumComponents(void* pCalib)
{
	return ((CQntCalibration*)pCalib)->GetNumComponents();
}

// Description:
// Return the number of freedom degrees

int IRCalibrate_GetNumFreedomDegrees(void* pCalib)
{
	return ((CQntCalibration*)pCalib)->GetNumFreedomDegrees();
}

// Description:
// Return the error of the quantitative calibration

int IRCalibrate_GetError(void* pCalib)
{
	return ((CQntCalibration*)pCalib)->GetError();
}

// Description:
// Return the SEC (standard error of calibartion) for a given component

int IRCalibrate_GetSEC(void* pCalib,
					   string compName, // component name
					   double* sec) // SEC for this component
{
	return ((CQntCalibration*)pCalib)->GetSEC(compName, sec);
}

// Description:
// Return the R^2 statistic for a given component

int IRCalibrate_GetR2stat(void* pCalib,
								   string compName, // component name
								   double* R2stat) // R^2 statistic for this component
{
	return ((CQntCalibration*)pCalib)->GetR2Stat(compName, R2stat);
}

// Description:
// Save the quantitative calibration in file

/*int IRCalibrate_Save(void* pCalib, const char* fileName)
{
	return ((CQntCalibration*)pCalib)->Save(fileName);
}*/

/*const char* IRCalibrate_GetSampleName(void* pCalib,
									  int nSample) // sample order number
{
	return ((CQntCalibration*)pCalib)->GetSampleName(nSample);
}*/
		

/*const char* IRCalibrate_GetSampleSpecName(void* pCalib,
												  int nSample, // sample order number
												  int nSpec)   // spectrum order number
{
	return ((CQntCalibration*)pCalib)->GetSampleSpecName(nSample, nSpec);
}*/

// added 29.11.2003 for compliment of calibration

/*void*  IRCalibrate_Create(const char* cal_file)
{
	return (void *) new CQntCalibration(cal_file);
}*/

/*void IRCalibrate_AddSamples(void* pCalib,
					int numSamples, // number of samples
					const CSampleProxy* pSamples, // array of samples
					const CSpecPreproc &prePar, // preprocessing parameters
					int nModelType, // calibartion model type
					int numFactors) // number of factors
{
	((CQntCalibration*)pCalib)->AddSamples(numSamples, pSamples,
		prePar, nModelType, numFactors);
}*/

// Description:
// Return the Mahalanobis distances for all spectra in the sample

void IRCalibrate_GetSampleDistances(void* pCalib,
								int nSample, // sample order number
								double* dist) // array of distances to be returned
{
	((CQntCalibration*)pCalib)->GetSampleDistances(nSample, dist);
}

void IRCalibrate_GetSampleNormDistances(void* pCalib,
								int nSample, // sample order number
								double* dist) // array of distances to be returned
{
	((CQntCalibration*)pCalib)->GetSampleNormDistances(nSample, dist);
}

// Description:
// Return the Mahalanobis distances for mean spectrum on the sample

double IRCalibrate_GetSampleDistanceAv(void* pCalib,
								int nSample) // sample order number
{
	return ((CQntCalibration*)pCalib)->GetSampleDistance(nSample);
}

double IRCalibrate_GetSampleNormDistanceAv(void* pCalib,
								int nSample) // sample order number
{
	return ((CQntCalibration*)pCalib)->GetSampleNormDistance(nSample);
}


// Description:
// Return the Mahalanobis distances for the given spectrum in the sample

void IRCalibrate_GetSampleSpecDistance(void* pCalib,
								  int nSample, // sample order number
								  int nSpec,   // spectrum order number
							      double* dist) // the distance to be returned
{
	((CQntCalibration*)pCalib)->GetSampleSpecDistance(nSample, nSpec, dist);
}

// Description:
// Return the TransType

int  IRCalibrate_GetTransType(void* pCalib)
{
	return ((CQntCalibration*)pCalib)->GetTransType();
}


// Description:
// Return the maximum possible frequency range
// in fMinFreq and fMaxFreq, and Error as result

int  IRCalibrate_GetMaxClbFreqRange(void* pCalib, double *fMinFreq, double *fMaxFreq)
{
	return ((CQntCalibration*)pCalib)->GetMaxClbFreqRange(fMinFreq, fMaxFreq);
}



//*****************************************************
//
//   IRPredict
//
//*****************************************************

/*void *IRPredict_Create(const char* cal_file)
{
//	AppendToDebugReport("IRPredict model start");

	return 	(void *) new CQntCalibration(cal_file);
}*/

// Description:
// Destroy the object of IRPredict class

/*void IRPredict_Destroy(void* pPredict)
{
    if (pPredict) delete ((CQntCalibration*)pPredict);
}*/

// Description:
// Predict a spectrum

/*int IRCalibrate_PredictSpectrum(void* pCalib,
								const CFreqScale *dsc, // spectrum descriptor
								const VDbl *vSpectrum, // spectrum data
								double* conc) // the predicted concentration of this conponent
{
	return ((CQntCalibration*)pCalib)->PredictSpectrum(dsc, vSpectrum, conc);
}*/

int IRCalibrate_PredictSpectrum(void* pCalib,
								const CFreqScale *dsc, // spectrum descriptor
								const VDbl *vSpectrum, // spectrum data
								VDbl *pvCompConc) // the predicted concentration of this conponent
{
	return ((CQntCalibration*)pCalib)->PredictSpectrum(dsc, vSpectrum, pvCompConc);
}

_DLL_API_ int IRCalibrate_PredictSamples(void* pCalib,
					vector<CSampleProxy> *pProxySamples, // array of samples
					const CFreqScale *FreqScale, // ��������� ����� ��������
					MDbl *pmCompConc, // ������� ������������� ������������
					VDbl *pvSEV, // ������ SEV [numComp]
					VDbl *pvE,  // ������ ������� ������ (����������) [numComp]
					VDbl *pvSDV,// ������ SDV [numComp]
					VDbl *pvR2) // ������ R2 [numComp]
{
		return ((CQntCalibration*)pCalib)->PredictSamples(pProxySamples, FreqScale, pmCompConc, pvSEV, pvE, pvSDV, pvR2);
}


// Description:
// Return the error of prediction

/*int IRCalibrate_GetError(void* pCalib)
{
	return ((CQntCalibration*)pCalib)->GetError();
}*/



// Description:
// Return the predicted concentration of the component of interest for given spectrum

int IRCalibrate_GetConcentration(void* pCalib,
									string compName, // the component name of interest
									double* conc, // the predicted concentration of this conponent
									double* concErr) // the estimated error of predicted concentration
{
	return ((CQntCalibration*)pCalib)->GetConcentration(compName, // the component name of interest
									conc, // the predicted concentration of this conponent
									concErr);
}

/*void IRCalibrate_GetRefConc(void* pCalib,
									int nSample, // sample order number
									double* compConc) // array of reference concentrations
{
	((CQntCalibration*)pCalib)->GetRefConc(nSample, // sample order number
									compConc);
}*/

void IRCalibrate_GetRefConc(void* pCalib,
									int nSample, // sample order number
									VDbl* pvCompConc) // array of reference concentrations
{
	((CQntCalibration*)pCalib)->GetRefConc(nSample, // sample order number
									pvCompConc);
}

/*void IRCalibrate_GetPredConc(void* pCalib,
									int nSample, // sample order number
									int nSpec, // spec order number
									double* compConc)
{
	((CQntCalibration*)pCalib)->PredictSpectrum(nSample, // sample order number
									nSpec, // spec order number
									compConc);
}*/

void IRCalibrate_GetPredConc(void* pCalib,
									int nSample, // sample order number
									int nSpec, // spec order number
									VDbl* pvCompConc)
{
	((CQntCalibration*)pCalib)->PredictSpectrum(nSample, // sample order number
									nSpec, // spec order number
									pvCompConc);
}


void IRCalibrate_GetPredConcAv(void* pCalib,
									int nSample, // sample order number
									VDbl *pvCompConc)
{
	((CQntCalibration*)pCalib)->PredictSample(nSample, // sample order number
									pvCompConc);
}

double IRCalibrate_GetExtSpecDistance(void* pCalib,
								const CFreqScale *dsc, // spectrum descriptor
								const VDbl *vSpectrum) // spectrum data
{
	return ((CQntCalibration*)pCalib)->CalculateMahalanobisDistance(dsc, vSpectrum);
}

double IRCalibrate_GetExtSpecNormDistance(void* pCalib,
								const CFreqScale *dsc, // spectrum descriptor
								const VDbl *vSpectrum) // spectrum data
{
	return ((CQntCalibration*)pCalib)->CalculateNormMahalanobisDistance(dsc, vSpectrum);
}


// ������� ����� ��������
int IRCalibrate_SetHarmonics(void* pCalib, 
					int numHarmonics)  // number of harmonics
{
	return ((CQntCalibration*)pCalib)->SetHarmonics(numHarmonics);
}

// ������� ������������ ������������
int IRCalibrate_SetCnl(void* pCalib, 
					double fCnl)  // coefficient of non-linearity
{
	return ((CQntCalibration*)pCalib)->SetCnl(fCnl);
}



// ������� ������� ���������
int IRCalibrate_SetShc(void* pCalib, 
					double fShc)  // size of hypercube
{
	return ((CQntCalibration*)pCalib)->SetShc(fShc);
}





//*****************************************************
//
//   Qualitative Calibration
//
//*****************************************************


// Description:
// Construct a qualitative calibration
void Calibrate_Create()
{
	OutputDebugStringA("Create coll");
}

void* QltAnaliz_Create(vector<CSampleProxy> *pSamples, // array of samples
					const CFreqScale *FreqScale, // ��������� ����� ��������
					const CSpecPreproc *prePar, // preprocessing parameters
					int numFactors, // number of factors
					CQnt_Parcel **pQnt) // created model
{
	return 	(void *) new CQltCalibration(pSamples, FreqScale,
		prePar, numFactors, pQnt);
}

// Description:
// Construct a qualitative calibration from file

/*void* QltAnaliz_Load(const char* fileName)
{
	return 	(void *) new CQltCalibration(fileName);
}*/


// Description:
// Destroy a qualitative calibration

void QltAnaliz_Destroy(void* pQlt)
{
	if (pQlt) delete ((CQltCalibration*)pQlt);
//	AppendToDebugReport("Qlt model was destroyed");
}


// Description:
// Save the qualitative calibration in file

/*void QltAnaliz_Save(void* pQlt, const char* fileName)
{
//	AppendToDebugReport("Qlt model to be saved");

	((CQltCalibration*)pQlt)->Save(fileName);

//	AppendToDebugReport("Qlt model was saved");
}*/


// Description:
// Return the error of the qualitative calibration

int  QltAnaliz_GetError(void* pQlt)
{
	return ((CQltCalibration*)pQlt)->GetError();
}

// Description:
// Return the number of variables in the qualitative model

int  QltAnaliz_GetNumVariables(void* pQlt)
{
	return ((CQltCalibration*)pQlt)->GetNumVariables();
}

// Description:
// Return the Mahalanobis distances for mean spectrum of the sample

//double QltAnaliz_GetSampleDistanceAv(void* pQlt,
//								int nSample) // sample order number
//{
//	return ((CQltCalibration*)pQlt)->GetSampleDistance(nSample);
//}

double QltAnaliz_GetSampleNormDistanceAv(void* pQlt,
								int nSample) // sample order number
{
	return ((CQltCalibration*)pQlt)->GetSampleNormDistance(nSample);
}

// Description:
// Return the Mahalanobis distances for all spectra in the sample

//void QltAnaliz_GetSampleDistances(void* pQlt,
//								int nSample, // sample order number
//								double* dist) // array of distances to be returned
//{
//	((CQltCalibration*)pQlt)->GetSampleDistances(nSample, dist);
//}

void QltAnaliz_GetSampleNormDistances(void* pQlt,
								int nSample, // sample order number
						      VDbl *vNormMahDistances) // array of normalized distances to be returned
{
	((CQltCalibration*)pQlt)->GetSampleNormDistances(nSample, vNormMahDistances);
}


// Description:
// Return the Mahalanobis distance for a given spectrum

//former _DLL_API_ double QltAnaliz_PCA_GetDistance(...
//double QltAnaliz_GetExtSpecDistance(void* pQlt,
//										   const CFreqScale *dsc, // spectrum descriptor
//										   const double* data) // spectrum data
//{
//	return ((CQltCalibration*)pQlt)->CalculateMahalanobisDistance(dsc, data);
//}


double QltAnaliz_GetExtSpecNormDistance(void* pQlt,
										   const CFreqScale *dsc, // spectrum descriptor
										   const VDbl *vSpectrum) // spectrum data
{
	//dsc->Print("freqScale_GetExtSpecNormDistance.dat");
	return ((CQltCalibration*)pQlt)->CalculateNormMahalanobisDistance(dsc, vSpectrum);
}







// Description:
// Return the Mahalanobis distances for the given spectrum in the sample

void QltAnaliz_GetSampleSpecDistance(void* pQlt,
								  int nSample, // sample order number
								  int nSpec,   // spectrum order number
							      double* dist) // the distance to be returned
{
	((CQltCalibration*)pQlt)->GetSampleSpecDistance(nSample, nSpec, dist);
}

// Description:
// Return the number of samples in the qualitative model

int  QltAnaliz_GetNumSamples(void* pQlt)
{
	return ((CQltCalibration*)pQlt)->GetNumSamples();
}


// Return the number of spectra for the specific sample 
int  QltAnaliz_GetNumSampleSpecs(void* pQlt, int nSample) // sample order number
{
	return ((CQltCalibration*)pQlt)->GetNumSampleSpectra(nSample);
}



/*const char* QltAnaliz_GetSampleName(void* pQlt,
									  int nSample) // sample order number
{
	return ((CQltCalibration*)pQlt)->GetSampleName(nSample);
}*/
		

/*const char* QltAnaliz_GetSampleSpecName(void* pQlt,
												  int nSample, // sample order number
												  int nSpec)   // spectrum order number
{
	return ((CQltCalibration*)pQlt)->GetSampleSpecName(nSample, nSpec);
}*/

// added 29.11.2003 for compliment of calibration

/*void QltAnaliz_AddSamples(void *pQlt,
					int numSamples, // number of samples
					const CSampleProxy* pSamples, // array of samples
					const CSpecPreproc &prePar, // preprocessing parameters
					int numFactors) // number of factors
{
	 ((CQltCalibration*)pQlt)->AddSamples(numSamples, pSamples,
		prePar, numFactors);
}*/
					


//*****************************************************
//
//   IRSecv
//
//*****************************************************

// Description:
// Construct an object of IRSecv class


void* IRSecv_Create(void* pQnt)
{ 
	return (void *) new CSecv((CQntCalibration*)pQnt);
}

//void* IRSecv_Create(int numSamples, // number of samples
//					const CSampleProxy* samples, // array of samples
//					const CSpecPreproc& prePar, // preprocessing parameters
//					int nModelType, // calibartion model type (default - PLS)
///					int numFactors) // number of factors
//{
//	return 	(void *) new CSecv(numSamples, samples,
//		prePar, nModelType, numFactors);
//}

// Description:
// Destroy the object of IRSecv class

void IRSecv_Destroy(void* pSECV)
{
    if (pSECV) delete ((CSecv*)pSECV);
}

// Description:
// Return the error of cross-validation

int IRSecv_GetError(void* pSECV)
{
	return ((CSecv*)pSECV)->GetError();
}

// Description:
// Return the number of samples

int IRSecv_GetNumSamples(void* pSECV)
{
	return ((CSecv*)pSECV)->GetNumSamples();
}

int IRSecv_GetNumComponents(void* pSECV)
{
	return ((CSecv*)pSECV)->GetNumComponents();
}


// Description:
// Return the predicted concentrations for the spectra of given sample

void IRSecv_GetPredConc(void* pSECV,
						int nSample, // sample order number
						int nSpec, // spectrum order number
						VDbl* vCompConc) // arrays of predicted components concentration
{
	((CSecv*)pSECV)->GetPredConc(nSample, nSpec, vCompConc);
}


void IRSecv_GetPredConcAv(void* pSECV,
						int nSample, // sample order number
						VDbl* vCompConc) // arrays of predicted components concentration
{
	((CSecv*)pSECV)->GetPredConc(nSample, vCompConc);
}

// Description:
// Return the SECV(standard error of cross validation) for a given component

int IRSecv_GetSECV(void* pSECV,
							string compName, // the  component name of interest
							double* secv) // SECV for this component
{
	return ((CSecv*)pSECV)->GetSECV(compName, secv);
}

int IRSecv_GetSECV(void* pSECV,
							const char* compName, // the  component name of interest
							double* secv) // SECV for this component
{
	return ((CSecv*)pSECV)->GetSECV(string(compName), secv);
}


// Description:
// Return the R^2 statistic for a given component

int IRSecv_GetR2stat(void* pSECV,
					string compName, // component name
					double* R2stat) // R^2 statistic for this component
{
	return ((CSecv*)pSECV)->GetR2Stat(compName, R2stat);
}

// Description:
// Return the F statistic for a given component

int IRSecv_GetFstat(void* pSECV,
					string compName, // component name
					double* Fstat) // F statistic for this component
{
	return ((CSecv*)pSECV)->GetFStat(compName, Fstat);
}

int IRSecv_GetSEC(void* pSECV,
				  int nSample, // excluded sample order number
				VDbl* vSEC) // array[numberOfComponents] of sec for this sample
{
	return ((CSecv*)pSECV)->GetSEC(nSample, vSEC);
}

void IRSecv_GetRefConc(void* pSECV,
						int nSample, // sample order number
						VDbl *vCompConc) // array of reference data for this sample
{
	((CSecv*)pSECV)->GetRefConc(nSample, vCompConc);
}

// spectra correction for transfer

/*int IRTransfer_SpectraCorrection(
					vector<CSampleProxy> *pSmplsClbMaster, // array of calibration Master samples
					vector<CSampleProxy> *pSmplsMaster, // array of Master samples (common)
					vector<CSampleProxy> *pSmplsSlave, // array of Slave samples (common)
					const CFreqScale *freqScale, // ��������� ����� ��������
					int nSpType, // ��� �������
					vector<string> *prePar, // array of preprocessing strings
					vector<double> *fDistMin, // ����������� ��������� ����������
					vector<double> *fDistMax, // ������������ ��������� ����������
					vector<double> *fDistMean) // ������� ��������� ����������
{
	return SpectraCorrection(pSmplsClbMaster, // array of calibration Master samples
					pSmplsMaster, // array of Master samples
					pSmplsSlave, // array of Slave samples
					freqScale, // ��������� ����� ��������
					nSpType, // ��� �������
					prePar, // preprocessing parameters
					fDistMin, // ����������� ��������� ����������
					fDistMax, // ������������ ��������� ����������
					fDistMean);
}*/

void* SpectraCorrection_Create(vector<CSampleProxy> *pSmplsClbMaster, // array of calibration Master samples
					vector<CSampleProxy> *pSmplsMaster, // array of Master samples (common)
					vector<CSampleProxy> *pSmplsSlave, // array of Slave samples (common)
					const CFreqScale *freqScale) // ��������� ����� ��������
{
	return new CSpectraCorrection(pSmplsClbMaster, // array of calibration Master samples
					pSmplsMaster, // array of Master samples (common)
					pSmplsSlave, // array of Slave samples (common)
					freqScale);
}

int SpectraCorrection_Correct(void* pSpCor, string strPreproc) // preprocessing string
{
	return ((CSpectraCorrection*)pSpCor)->Correct(strPreproc);
}

void SpectraCorrection_GetDistances(void* pSpCor,
								   vector<double> *fDistances, // ������ ���������� ���������� (����� = 
									double *fDistMin, // // ����������� ��������� ����������
									double *fDistMax, // ������������ ��������� ����������
									double *fDistMean) // ������� ��������� ����������
{
	return ((CSpectraCorrection*)pSpCor)->GetDistances(fDistances, fDistMin, fDistMax, fDistMean);
}

void SpectraCorrection_GetCorrectedSpectra(void* pSpCor, vector<CSampleProxy> *pSmplsClbMaster) // array of calibration Master samples to be corrected
{
	((CSpectraCorrection*)pSpCor)->GetCorrectedSpectra(pSmplsClbMaster);
}

void SpectraCorrection_GetPreprocData(void* pSpCor, CSpecPreprocData_Parcel *pPreprocData)
{
	((CSpectraCorrection*)pSpCor)->GetPreprocData(pPreprocData);
}


void SpectraCorrection_Destroy(void* pSpCor)
{
	delete (CSpectraCorrection*)pSpCor;
}

void IRCalibrate_PreCreate(vector<CSampleProxy> *pSamples, // array of samples
					const CFreqScale *FreqScale, // ��������� ����� ��������
					string strPreproc, // ������ ������������� ��� ��������
					const CSpecPreprocData_Parcel *pPreprocData) // ������ ������������� ��� ��������
{

	CSamples *pClbSamples = new CSamples(pSamples, FreqScale, 0);

	CSpecPreprocData m_specPreprocData;

	m_specPreprocData.Load(FreqScale, strPreproc, pPreprocData);

//  debug:
//	Matrix mSp;
//	m_specPreprocData.Print("specPreprocData_PreCreate.dat");
//	pClbSamples->GetMSpectra(mSp);
//	PrintMatrix(mSp, "mSpectra_Precreate.dat");

	Matrix mSpPrep;

	pClbSamples->PreprocessSpectra4Prediction(m_specPreprocData, mSpPrep);

//	PrintMatrix(mSpPrep, "mSpectraPreproc_Precreate.dat");

	int numSamples = pClbSamples->GetNumSamples();

	int numCol = mSpPrep.Ncols();

	for (int iS = 0; iS < numSamples; iS++)
	{
		int numSp = pClbSamples->GetSampleSpectraNumbers(iS);
		int ind = pClbSamples->GetSampleSpectraIndexes(iS);

		for (int k = 0; k < numSp; k++)
		{
			for (int i = 0; i < numCol; i++)
			{
				(*pSamples)[iS].mSpectra[k][i] = mSpPrep(ind+k+1,i+1);
			}
		}
	}

/*	char strBuf[256];

	int nSample = pSamples->size();
	int nSp = (*pSamples)[nSample-1].mSpectra.size();
	int nF = (*pSamples)[nSample-1].mSpectra[nSp-1].size();
	sprintf(strBuf, "sample %d numSpectra = %d numFreq = %d\n", nSample, nSp, nF);
	AppendToDebugReport(strBuf);
	sprintf(strBuf, "last spectrum, last freq. - 1 = %e", (*pSamples)[nSample-1].mSpectra[nSp-1][nF-2]);
	AppendToDebugReport(strBuf);
	sprintf(strBuf, "last spectrum, last freq. = %e", (*pSamples)[nSample-1].mSpectra[nSp-1][nF-1]);
	AppendToDebugReport(strBuf);*/
	//PrintMatrix(mSpPrep, "preCreate_mSpPrep.dat");

	delete pClbSamples;
}

void*  IRCalibrate_Create(vector<CSampleProxy> *pSamples, // array of samples
					const CFreqScale *FreqScale, // ��������� ����� ��������
					const CSpecPreproc *prePar, // preprocessing parameters
					const CQntOutput *pQntOutput) // �������� (��� ����������)
{
#ifdef _DEBUG_
	char strBuf[256];
	sprintf(strBuf, "IRCalibrate_Create number of Components names = %d", (prePar->strCompName).size());
	AppendToDebugReport(strBuf);
#endif
	return 	(void *) new CQntCalibration(pSamples, FreqScale,
		prePar, pQntOutput);
}


// �������� �������� ��� ���������
int IRCalibrate_LoadSamples4SEV(void* pCalib, 
				  vector<CSampleProxy> *pSamples) // array of samples
{
	return ((CQntCalibration*)pCalib)->LoadSamples4SEV(pSamples);
}

// ������� ���������� ������� �� �������� (������������� 'A')
int IRCalibrate_SetAverage(void* pCalib, 
					bool bAverage) // ���� ����������
{
	return ((CQntCalibration*)pCalib)->SetAverage(bAverage);
}

// ������� ���� ������
int IRCalibrate_SetModelType(void* pCalib, 
					int nModelType) // calibartion model type
{
	return ((CQntCalibration*)pCalib)->SetModelType(nModelType);
}


// ������� ���� �������
int IRCalibrate_SetSpType(void* pCalib, 
					int nSpType) // ��� ������� (STYPE_TRA ��� STYPE_ABS - transmittance or absorbance)
{
	return ((CQntCalibration*)pCalib)->SetSpType(nSpType);
}

// ������� ���������� ���������
 int IRCalibrate_SetFreqRange(void* pCalib, 
					double fMin, double fMax) // ��������� ��������
{
	return ((CQntCalibration*)pCalib)->SetFreqRange(fMin, fMax);
}



// ������� �������������
int IRCalibrate_SetPreproc(void* pCalib, 
					string strPreprocPar) // ������ ���������� �������������
{
	return ((CQntCalibration*)pCalib)->SetPreproc(strPreprocPar);
}

// ������� ����� ��������
int IRCalibrate_SetFactors(void* pCalib, 
					int numFactors)  // number of factors
{
	return ((CQntCalibration*)pCalib)->SetFactors(numFactors);
}


// ��������� ����������� �������
int IRCalibrate_GetResults(void* pCalib, 
					CQntOutput *pQntOutput)  // number of factors
{
	return ((CQntCalibration*)pCalib)->GetResults(pQntOutput);
}


// ABS-TRA
// 
void ABS2TRA(const VDbl *pvSpectrumIN, 
					VDbl *pvSpectrumOUT)
{
	const double minx = 1.0e-14;

	int nFreq = pvSpectrumIN->size();

	for (int i = 0; i < nFreq; i++)
	{
		if ((*pvSpectrumIN)[i] > minx)
		{
			(*pvSpectrumOUT)[i] = pow(10.0, -(*pvSpectrumIN)[i]);
		}
		else
		{
			(*pvSpectrumOUT)[i] = 0;
		}
	}
}

void TRA2ABS(const VDbl *pvSpectrumIN, 
					VDbl *pvSpectrumOUT)
{
	const double minx = 1.0e-14;

	int nFreq = pvSpectrumIN->size();

	for (int i = 0; i < nFreq; i++)
	{
		if ((*pvSpectrumIN)[i] > minx)
		{
			(*pvSpectrumOUT)[i] = -log10((*pvSpectrumIN)[i]);
		}
		else
		{
			(*pvSpectrumOUT)[i] = 0;
		}
	}
}





