// ModelPLS.h: interface for the CModelPLS class.
//
// ��������:
// ����� CModelPLS - ����� ��� ������������� ������ ��������
// �� ��������� ��������� (PLS-������)
// 
// ������������ �������� CQntCalibration � CQltCalibration
//

#ifndef _MODELPLS_H_
#define _MODELPLS_H_

#include "Model.h"
#include "newmat.h"


class CModelPLS : public CModel
{
public:
	CModelPLS() { nError = ER_OK; nModelType = MODEL_PLS; bF2inv = false; }
	CModelPLS(const Matrix& mX, const Matrix& mY, int nFactors);
	void Predict(const Matrix& mX, Matrix& mYpredicted);

	int GetError() const { return nError; }
	int GetNumVariables() const { return numFactors; }
	int GetModelType() const { return nModelType; }
	void GetMatrixScores(Matrix &mS) const { mS = mScores; }

	void CalculateMahalanobisDistance(const Matrix& mSp, ColumnVector& vMahDist);
	void GetMahalanobisDistance(ColumnVector& vMahDist) const;

	void Save(FILE* fgh, int clbVersion) const;
	void Load(FILE* fgh, int clbVersion);

	void GetModelPar(MODEL_MMP& ModelPar) const {};
	void GetUseFreq(int *pUseFreq) const {};

	void FillModel_Parcel(CModel_Parcel& mModel) const;
	//void FillModel_Parcel(CModelHSO_Parcel& mModel) const { }


	void LoadFromQnt_Parcel(const CModel_Parcel& mModel);

	int SetFrequencies(int nFreq, const int *pUseFreq) {return 0;};
	int SetHarmonics(int numHarmonics) {return 0;};
	int SetCnl(double fCnl) {return 0;};
	int SetShc(double fShc) {return 0;};
	int CreateModel(const Matrix& mX, const Matrix& mY) {return 0;};

protected:
	void CalculateScores(const Matrix& mSp, Matrix& mSpScores);
	void CreateOne(const Matrix& mSpectra, const Matrix& mComponents, int nFactors);
	void CreateMany(const Matrix& mSpectra, const Matrix& mComponents, int nFactors);


	int nError;		// ��� ������
	int numFactors;	// ����� ��������
	int nModelType;	// ��� ������ (= MODEL_PLS)
	bool bF2inv; // ��������� ������� mF2inv ��� ���

	Matrix mCalibr;	// ������������� �������
	Matrix mFactors;// ������� �������� (numFactors x numFreq)
	Matrix mScores; // ������� ������ (scores) (numSp x numFactors)
	Matrix mYloadings;	// ������� ���������� �������� (loadings) (numComp x numFactors)
	Matrix mF2inv;	// �������, �������� � �������
					// mFactors x mFacors (numFactors x numFactors)

	RowVector vS2; // vS2(i) = (mScores.Column(i)).SumSquare()
};


#endif // _MODELPLS_H_