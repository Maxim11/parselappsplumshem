//#include <math.h>
#include "SECV.h"
#include "SpLumChemStructs.h"
#include "Model.h"
#include "ModelPLS.h"
#include "ModelPCR.h"
#include "ModelHSO.h"
#include "PreprocUtils.h"
#include "MatrixUtilities.h"

#include "LLTIMER.h"
extern void log_me(const char* msg, bool bAppend=true);


class CStepInfo
{
public:
	CStepInfo(const char *str) { name = str; reset(); }
	string name;
	double tt;
	int n;
	void reset() { n = 0; tt = 0; }
	double add(double t) { tt += t;  n++; return tt / n; }
	double av() { return n > 0 ? tt / n : 0.; }
/*
	void spr(char* str)
	{
		sprintf(str, "%s : %7f", name.c_str(), tt);
	}
*/
	void spr(char* str)
	{
		sprintf(str, "%-20s : %.5f s (%4d x %.3f ms)", name.c_str(), tt, n, av()*1000);
	}

};


CSecv::CSecv(const CQntCalibration* pQntCalibration)
{
char strlog[256];
InitLLTimer();
StartLLTimer();
double tll1 = GetLLTimerSec();
double tll0 = tll1;

	Init();

CStepInfo siInit("SECV: Init");
tll1 = GetLLTimerSec()-tll1;
siInit.add(tll1);

siInit.spr(strlog);
log_me(strlog, false);

//tll1 = GetLLTimerSec();

	nError = pQntCalibration->GetError();

	if (nError != ER_OK)
	{
		return;
	}

	//AppendToDebugReport("qnt was loaded in SECV");


	pClbSamples = new CSamples();

	pQntCalibration->GetClbSamples(pClbSamples);

	pQntCalibration->GetSpecPreprocData(specPreprocData);

	int nModelType;

	nModelType = pQntCalibration->GetModelType();

	MODEL_MMP theModelPar;

	//AppendToDebugReport("start of get Model Par in SECV");

	int *pUseFreq = 0;

	if (nModelType == MODEL_HSO)
	{
		pQntCalibration->GetModelPar(theModelPar);
		pUseFreq = new int[specPreprocData.numberOfChannels];
		pQntCalibration->GetUseFreq(pUseFreq);
	}

	//AppendToDebugReport("end of get Model Par in SECV");


	int numFactors;

	numFactors = pQntCalibration->GetNumVariables();
    
	freqScale.fStartFreq = pClbSamples->GetStartFreq();
	freqScale.fStepFreq = pClbSamples->GetStepFreq();
	freqScale.nNumberOfFreq = pClbSamples->GetNumFreq();
	
	CTransData theTransData;

	pQntCalibration->GetTransData(theTransData);

//	delete pQntCalibration;

	int numberOfComponents = pClbSamples->GetNumComponents();
	int numberOfSpectra = pClbSamples->GetNumSpectra();
	int numberOfSamples = pClbSamples->GetNumSamples();

	mConcPredicted.ReSize(numberOfSpectra, numberOfComponents);
	mSampleConcPredicted.ReSize(numberOfSamples, numberOfComponents);

	mSEC.ReSize(numberOfSamples, numberOfComponents);

	int nTransFlag = pClbSamples->GetTransFlag();

	CSpecPreprocData thePreprocDataConst;

	Matrix mX0;
	if(SECV_PREP_ONCE)
	{
		mX0.CleanUp();
		pClbSamples->PreprocessSpectra(specPreprocData, mX0, -1);
	}

CStepInfo siPreCycle("SECV: pre-cycle");
tll1 = GetLLTimerSec() - tll1;
siPreCycle.add(tll1);
siPreCycle.spr(strlog);
log_me(strlog);



CStepInfo siPreProcSpec("SECV: preproc spec");
CStepInfo siPreProcComp("SECV: preproc comp");
CStepInfo siModel      ("SECV: model calc  ");
CStepInfo siPreProcPred("SECV: preproc pred");
CStepInfo siPred       ("SECV: predict     ");
CStepInfo siCycle      ("SECV: cycle total ");

//double tll10 = GetLLTimerSec();

for (int iS = 0; iS < numberOfSamples; iS++)
{
	tll1 = GetLLTimerSec();
	double tll10 = tll1;

	Matrix mX, mY;
	mX.CleanUp();
	mY.CleanUp();

	if(SECV_PREP_ONCE)
	{
		mX = mX0;
		pClbSamples->ExcludeSample(specPreprocData, mX, iS);
	}

	else
	{
		pClbSamples->PreprocessSpectra(specPreprocData, mX, iS);
	}

	siPreProcSpec.add(GetLLTimerSec() - tll1);
	tll1 = GetLLTimerSec();

		//AppendToDebugReport("end of preproc in SECV");

	if (nTransFlag != 2)
	{
		theTransData.CorrectSpectra(mX);
	}

	nError = pClbSamples->GetError();

	if (nError != ER_OK)
	{
			//AppendToDebugReport("error of sp. preproc in SECV");

		return;
	}

	pClbSamples->PreprocessComponents(specPreprocData, mY, iS);
	nError = pClbSamples->GetError();

	siPreProcComp.add(GetLLTimerSec() - tll1);
	tll1 = GetLLTimerSec();
	//AppendToDebugReport("end of preproc comp. in SECV");

	if (nError != ER_OK)
	{
		return;
	}

	//AppendToDebugReport("start of model in SECV");

	//		char sTmp[16];
	//		sprintf(sTmp, "model type = %d", nModelType);
		//AppendToDebugReport(sTmp);

	switch(nModelType)
	{
		case MODEL_PLS:
			pModel = new CModelPLS(mX, mY, numFactors);
			nError = pModel->GetError();
			break;

		case MODEL_PCR:
			pModel = new CModelPCR(mX, mY, numFactors);
			nError = pModel->GetError();
			break;

		case MODEL_HSO:
			//AppendToDebugReport("start of HSO in SECV");
			pModel = new CModelHSO(mX, mY, pUseFreq, &theModelPar);//, 0);
			//AppendToDebugReport("end of HSO in SECVt");

			nError = pModel->GetError();
			break;

		default:
			nError = ER_MODEL_TYPE;
	}

	siModel.add(GetLLTimerSec() - tll1);
	tll1 = GetLLTimerSec();

	if (nError == ER_OK)
	{
		Matrix mSampleSpectra;
		Matrix mSSpPrep;
		RowVector vMeanSampleSpectrum;
		RowVector vMSSpPrep;

		mSampleSpectra.CleanUp();
		mSSpPrep.CleanUp();

		vMeanSampleSpectrum.CleanUp();
		vMSSpPrep.CleanUp();

		pClbSamples->GetSampleSpectra(mSampleSpectra, iS);
		pClbSamples->GetMeanSpectrum(iS, vMeanSampleSpectrum);

		if (nTransFlag != 2)
		{// without frequency adjustment

			PrepSpForPrediction(specPreprocData, mSampleSpectra, mSSpPrep, freqScale.fStepFreq);
			PrepSpForPrediction(specPreprocData, vMeanSampleSpectrum, vMSSpPrep, freqScale.fStepFreq);

			theTransData.CorrectSpectra(mSSpPrep);
			theTransData.CorrectSpectra(vMSSpPrep);
		}

		else
		{
				// specific case: transfered and added calibration
			if (pClbSamples->IsSampleAdded(iS))
			{
				PrepSpForPrediction(&freqScale, specPreprocData, mSampleSpectra, mSSpPrep, false);
				PrepSpForPrediction(&freqScale, specPreprocData, vMeanSampleSpectrum, vMSSpPrep, false);
			}

			else
			{
					// at first perform the constant preproc
				CSpecPreprocData thePreprocDataConst;
				pClbSamples->GetPreprocDataConst(thePreprocDataConst);
				Matrix mSSpPrepConst;
				RowVector vMSSpPrepConst;
				PrepSpForPrediction(&freqScale, thePreprocDataConst, mSampleSpectra, mSSpPrepConst, false);
				PrepSpForPrediction(&freqScale, thePreprocDataConst, vMeanSampleSpectrum, vMSSpPrepConst, false);

					// then correct the spectra
				theTransData.CorrectSpectra(mSSpPrepConst);
				theTransData.CorrectSpectra(vMSSpPrepConst);

					// then perform the remain preprocessing
				CSpecPreprocData thePreprocDataRem;
				thePreprocDataRem.Copy(specPreprocData);

				string strPreprocConst = thePreprocDataConst.strPreprocPar;
				string strPreprocFull, strPreprocCut;
				strPreprocFull = specPreprocData.strPreprocPar;
				int lp = strPreprocFull.length();

				for (int il = 0; il < lp; il++)
				{
					char ch = strPreprocFull[il];
					if (strPreprocConst.find(ch) == string::npos)
					{
						strPreprocCut += ch;
					}
				}

				thePreprocDataRem.strPreprocPar = strPreprocCut;
				thePreprocDataRem.nSpType = STYPE_TRA;

				PrepSpForPrediction(thePreprocDataRem, mSSpPrepConst, mSSpPrep, freqScale.fStepFreq);
				PrepSpForPrediction(thePreprocDataRem, vMSSpPrepConst, vMSSpPrep, freqScale.fStepFreq);
			}
		}

		siPreProcPred.add(GetLLTimerSec() - tll1);
		tll1 = GetLLTimerSec();
		Matrix mYpredicted;
		int indS, numSSp;

		indS = pClbSamples->GetSampleSpectraIndexes(iS);
		numSSp = pClbSamples->GetSampleSpectraNumbers(iS);
		pModel->Predict(mSSpPrep, mYpredicted);

			// �� ��������� ���������
		PostProcessComponents(mYpredicted, specPreprocData, false);
			/*if (theTransData.nTransType == 4)
			{
				// new transfer
				PostProcessComponents(mYpredicted, pClbSamples->thePreprocDataConst);
			}*/


			//theTransData.CorrectComponents(mYpredicted);

		mConcPredicted.Rows(indS + 1, indS + numSSp) = mYpredicted;
		mYpredicted.CleanUp();

			// now predict MeanSpectrum
		pModel->Predict(vMSSpPrep, mYpredicted);
		PostProcessComponents(mYpredicted, specPreprocData, false);
		mSampleConcPredicted.Row(iS + 1) = mYpredicted;
		mYpredicted.CleanUp();
		pModel->Predict(mX, mYpredicted);

		RowVector vSEC;
		vSEC.ReSize(numberOfComponents);

		PostProcessComponents(mY, specPreprocData, false);
		PostProcessComponents(mYpredicted, specPreprocData, false);

		Matrix	mErrorsSEC;
			
		mErrorsSEC.CleanUp();
		mErrorsSEC = mY - mYpredicted;

		int numberOfFreedomDegrees = mX.Nrows() - pModel->GetNumVariables();

		if (specPreprocData.meanComponentsTrans.Ncols() != 0)
		{
			numberOfFreedomDegrees--;
		}

		if (specPreprocData.meanSpectrum.Ncols() != 0)
		{
			numberOfFreedomDegrees--;
		}

		if (numberOfFreedomDegrees <= 0)
		{
			nError = ER_SEC;
			return;
		}

		for (int iC = 1; iC <= numberOfComponents; iC++)
		{
			vSEC(iC) = (mErrorsSEC.Column(iC)).SumSquare();
			vSEC(iC) /= numberOfFreedomDegrees;
			vSEC(iC) = sqrt(vSEC(iC));
		}

		mSEC.Row(iS + 1) = vSEC;
		delete pModel;
		pModel = 0;
	}

	else
	{
		delete pModel;
		pModel = 0;
		return;
	}

	siPred.add(GetLLTimerSec() - tll1);
	tll1 = GetLLTimerSec();
	siCycle.add(GetLLTimerSec() - tll10);
}

tll1 = GetLLTimerSec();
siPreProcSpec.spr(strlog);
log_me(strlog);
siPreProcComp.spr(strlog);
log_me(strlog);
siModel.spr(strlog);
log_me(strlog);
siPreProcPred.spr(strlog);
log_me(strlog);
siPred.spr(strlog);
log_me(strlog);
siCycle.spr(strlog);
log_me(strlog);

	delete[] pUseFreq;

	Matrix mConc;

	pClbSamples->GetMComponents(mConc);

	Matrix	mErrors = mConc - mConcPredicted;

	RowVector vStd2Comp;

	pClbSamples->GetStd2Components(vStd2Comp);

	vR2Stat.ReSize(numberOfComponents);

	vSECV.ReSize(numberOfComponents);

	for (int iC = 1; iC <= numberOfComponents; iC++)
	{
		vSECV(iC) = (mErrors.Column(iC)).SumSquare();

		vR2Stat(iC) = vSECV(iC) / vStd2Comp(iC);

		vSECV(iC) = sqrt(vSECV(iC) / numberOfSpectra);
	}

	vR2Stat *= -(numberOfSpectra - 1.0) / (numberOfSpectra - numFactors - 1.0);

	vR2Stat += 1.0;

	vFStat.ReSize(numberOfComponents);

	for (int iC = 1; iC <= numberOfComponents; iC++)
	{
		vFStat(iC) = vR2Stat(iC) / (1.0 - vR2Stat(iC));
	}

	vFStat *= (numberOfSpectra - numFactors - 1.0) / numFactors;

//	AppendToDebugReport("end of SECV");

	CStepInfo siPostCycle("SECV: PostCycle");
	tll1 = GetLLTimerSec() - tll1;
	siPostCycle.add(tll1);
	siPostCycle.spr(strlog);
	log_me(strlog);

	CStepInfo siTotal("SECV: total");
	tll1 = GetLLTimerSec() - tll0;
	siTotal.add(tll1);
	siTotal.spr(strlog);
	log_me(strlog);

}

CSecv::~CSecv()
{
	if (!flagBatch)
	{
		delete pClbSamples;
	}

	delete pModel;
}

int CSecv::GetR2Stat(string sCompName, double* R2Stat)
{
	int ind = pClbSamples->FindComponentIndex(sCompName);

	nError = pClbSamples->GetError();

	if (nError == ER_OK)
	{
		*R2Stat = vR2Stat(ind + 1);
	}

	return nError;
}

int CSecv::GetSECV(string sCompName, double* SECV)
{
	int ind = pClbSamples->FindComponentIndex(sCompName);

	nError = pClbSamples->GetError();

	if (nError == ER_OK)
	{
		*SECV = vSECV(ind + 1);
	}

	return nError;
}

int CSecv::GetFStat(string sCompName, double* Fstat)
{
	int ind = pClbSamples->FindComponentIndex(sCompName);

	nError = pClbSamples->GetError();

	if (nError == ER_OK)
	{
		*Fstat = vFStat(ind + 1);
	}

	return nError;
}

void CSecv::GetPredConc(int nSample, VDbl *vCompConc)
{
	int numComp;

	numComp = pClbSamples->GetNumComponents();

	for (int iC = 0; iC < numComp; iC++)
	{
		(*vCompConc)[iC] = mSampleConcPredicted(nSample + 1, iC + 1);
	}
}

void CSecv::GetPredConc(int nSample, int nSpec, VDbl *vCompConc)
{
	int indS, numComp;

	indS = pClbSamples->GetSampleSpectraIndexes(nSample);
	numComp = pClbSamples->GetNumComponents();

	for (int iC = 0; iC < numComp; iC++)
	{
		(*vCompConc)[iC] = mConcPredicted(indS + 1 + nSpec, iC + 1);
	}
}

void CSecv::GetRefConc(int nSample, VDbl *vCompConc)
{
	pClbSamples->GetRefConc(nSample, vCompConc);
}

int CSecv::GetSEC(int nSample, VDbl *vSEC)
{
	int numComp;

	numComp = pClbSamples->GetNumComponents();

	for (int iC = 0; iC < numComp; iC++)
	{
		(*vSEC)[iC] = mSEC(nSample + 1, iC + 1);
	}

	return nError;
}

void CSecv::Init()
{
	nError = ER_OK;
	pClbSamples = 0;
	pModel = 0;
	flagBatch = false;
}

int CSecv::GetNumSamples() const
{
	int nSamples;

	if ((nError == ER_OK) && (pClbSamples))
	{
        nSamples = pClbSamples->GetNumSamples();
	}
	else
	{
		nSamples = 0;
	}

	return nSamples;
}

int CSecv::GetNumComponents() const
{
	int nComp;

	if ((nError == ER_OK) && (pClbSamples))
	{
        nComp = pClbSamples->GetNumComponents();
	}
	else
	{
		nComp = 0;
	}

	return nComp;
}