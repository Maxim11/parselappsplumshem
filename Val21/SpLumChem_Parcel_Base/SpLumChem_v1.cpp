#include "SpLumChem.h"

#include "QntCalibration.h"

#include "QltCalibration.h"

#include "Secv.h"

extern void AppendToDebugReport(const char* str);


_DLL_API_ void*  IRCalibrate_Create(vector<CSampleProxy> *pSamples, // array of samples
					const CFreqScale *FreqScale, // ��������� ����� ��������
					const CSpecPreproc *prePar, // preprocessing parameters
					int nModelType, // calibartion model type
					int numFactors,  // number of factors
					CQnt_Parcel **pQnt) // model
{

	return 	(void *) new CQntCalibration(pSamples, FreqScale,
		prePar, nModelType, numFactors, pQnt);

}

_DLL_API_ void*  IRCalibrate_Create(vector<CSampleProxy> *pSamples, // array of samples
					const CFreqScale *FreqScale, // ��������� ����� ��������
					const CSpecPreproc *prePar, // preprocessing parameters
                    const MODEL_MMP *pModelPar,
	         		 int numPCAfactors)
{

	return 	(void *) new CQntCalibration(pSamples, FreqScale,
		prePar, pModelPar, numPCAfactors);

}

 _DLL_API_ void*  IRCalibrate_Create(vector<CSampleProxy> *pSamples, // array of samples
					const CFreqScale *FreqScale, // ��������� ����� ��������
					const CQnt_Parcel *pQnt) // created model
 {
		return 	(void *) new CQntCalibration(pSamples, FreqScale, pQnt);
 }


// Description:
// Destroy the object of IRCalibrate class

void IRCalibrate_Destroy(void* pCalib)
{
    if (pCalib) delete ((CQntCalibration*)pCalib);
}

// Description:
// Return the number of variables in calibration

int IRCalibrate_GetNumVariables(void* pCalib)
{
	return ((CQntCalibration*)pCalib)->GetNumVariables();
}

// Description:
// Return the number of components in calibration

int IRCalibrate_GetNumComponents(void* pCalib)
{
	return ((CQntCalibration*)pCalib)->GetNumComponents();
}

// Description:
// Return the number of freedom degrees

int IRCalibrate_GetNumFreedomDegrees(void* pCalib)
{
	return ((CQntCalibration*)pCalib)->GetNumFreedomDegrees();
}

// Description:
// Return the error of the quantitative calibration

int IRCalibrate_GetError(void* pCalib)
{
	return ((CQntCalibration*)pCalib)->GetError();
}

// Description:
// Return the SEC (standard error of calibartion) for a given component

int IRCalibrate_GetSEC(void* pCalib,
					   const char* compName, // component name
					   double* sec) // SEC for this component
{
	return ((CQntCalibration*)pCalib)->GetSEC(compName, sec);
}

// Description:
// Return the R^2 statistic for a given component

int IRCalibrate_GetR2stat(void* pCalib,
								   const char* compName, // component name
								   double* R2stat) // R^2 statistic for this component
{
	return ((CQntCalibration*)pCalib)->GetR2Stat(compName, R2stat);
}

// Description:
// Save the quantitative calibration in file

/*int IRCalibrate_Save(void* pCalib, const char* fileName)
{
	return ((CQntCalibration*)pCalib)->Save(fileName);
}*/

/*const char* IRCalibrate_GetSampleName(void* pCalib,
									  int nSample) // sample order number
{
	return ((CQntCalibration*)pCalib)->GetSampleName(nSample);
}*/
		

/*const char* IRCalibrate_GetSampleSpecName(void* pCalib,
												  int nSample, // sample order number
												  int nSpec)   // spectrum order number
{
	return ((CQntCalibration*)pCalib)->GetSampleSpecName(nSample, nSpec);
}*/

// added 29.11.2003 for compliment of calibration

/*void*  IRCalibrate_Create(const char* cal_file)
{
	return (void *) new CQntCalibration(cal_file);
}*/

/*void IRCalibrate_AddSamples(void* pCalib,
					int numSamples, // number of samples
					const CSampleProxy* pSamples, // array of samples
					const CSpecPreproc &prePar, // preprocessing parameters
					int nModelType, // calibartion model type
					int numFactors) // number of factors
{
	((CQntCalibration*)pCalib)->AddSamples(numSamples, pSamples,
		prePar, nModelType, numFactors);
}*/

// Description:
// Return the Mahalanobis distances for all spectra in the sample

void IRCalibrate_GetSampleDistances(void* pCalib,
								int nSample, // sample order number
								double* dist) // array of distances to be returned
{
	((CQntCalibration*)pCalib)->GetSampleDistances(nSample, dist);
}

void IRCalibrate_GetSampleNormDistances(void* pCalib,
								int nSample, // sample order number
								double* dist) // array of distances to be returned
{
	((CQntCalibration*)pCalib)->GetSampleNormDistances(nSample, dist);
}

// Description:
// Return the Mahalanobis distances for mean spectrum on the sample

double IRCalibrate_GetSampleDistanceAv(void* pCalib,
								int nSample) // sample order number
{
	return ((CQntCalibration*)pCalib)->GetSampleDistance(nSample);
}

double IRCalibrate_GetSampleNormDistanceAv(void* pCalib,
								int nSample) // sample order number
{
	return ((CQntCalibration*)pCalib)->GetSampleNormDistance(nSample);
}


// Description:
// Return the Mahalanobis distances for the given spectrum in the sample

void IRCalibrate_GetSampleSpecDistance(void* pCalib,
								  int nSample, // sample order number
								  int nSpec,   // spectrum order number
							      double* dist) // the distance to be returned
{
	((CQntCalibration*)pCalib)->GetSampleSpecDistance(nSample, nSpec, dist);
}

// Description:
// Return the TransType

int  IRCalibrate_GetTransType(void* pCalib)
{
	return ((CQntCalibration*)pCalib)->GetTransType();
}


// Description:
// Return the maximum possible frequency range
// in fMinFreq and fMaxFreq, and Error as result

int  IRCalibrate_GetMaxClbFreqRange(void* pCalib, double *fMinFreq, double *fMaxFreq)
{
	return ((CQntCalibration*)pCalib)->GetMaxClbFreqRange(fMinFreq, fMaxFreq);
}



//*****************************************************
//
//   IRPredict
//
//*****************************************************

/*void *IRPredict_Create(const char* cal_file)
{
//	AppendToDebugReport("IRPredict model start");

	return 	(void *) new CQntCalibration(cal_file);
}*/

// Description:
// Destroy the object of IRPredict class

void IRPredict_Destroy(void* pPredict)
{
    if (pPredict) delete ((CQntCalibration*)pPredict);
}

// Description:
// Predict a spectrum

int IRPredict_PredictSpectrum(void* pPredict,
								const CFreqScale& dsc, // spectrum descriptor
								const double* data)
{
	return ((CQntCalibration*)pPredict)->PredictSpectrum(dsc, data);
}

// Description:
// Return the error of prediction

int IRPredict_GetError(void* pPredict)
{
	return ((CQntCalibration*)pPredict)->GetError();
}


// Description:
// Return the number of components to can be predected

int IRPredict_GetNumComponents(void* pPredict)
{
	return ((CQntCalibration*)pPredict)->GetNumComponents();
}


// Description:
// Return the predicted concentration of the component of interest for given spectrum

int IRPredict_GetConcentration(void* pPredict,
									const char* compName, // the component name of interest
									double* conc, // the predicted concentration of this conponent
									double* concErr) // the estimated error of predicted concentration
{
	return ((CQntCalibration*)pPredict)->GetConcentration(compName, // the component name of interest
									conc, // the predicted concentration of this conponent
									concErr);
}

void IRPredict_GetRefConc(void* pPredict,
									int nSample, // sample order number
									double* compConc) // array of reference concentrations
{
	((CQntCalibration*)pPredict)->GetRefConc(nSample, // sample order number
									compConc);
}


void IRPredict_GetPredConc(void* pPredict,
									int nSample, // sample order number
									int nSpec, // spec order number
									double* compConc)
{
	((CQntCalibration*)pPredict)->PredictSpectrum(nSample, // sample order number
									nSpec, // spec order number
									compConc);
}


void IRPredict_GetPredConcAv(void* pPredict,
									int nSample, // sample order number
									double* compConc)
{
	((CQntCalibration*)pPredict)->PredictSample(nSample, // sample order number
									compConc);
}

double IRPredict_GetExtSpecDistance(void* pPredict,
								const CFreqScale& dsc, // spectrum descriptor
								 const double* data) // spectrum data
{
	return ((CQntCalibration*)pPredict)->CalculateMahalanobisDistance(dsc, data);
}

double IRPredict_GetExtSpecNormDistance(void* pPredict,
								const CFreqScale& dsc, // spectrum descriptor
								 const double* data) // spectrum data
{
	return ((CQntCalibration*)pPredict)->CalculateNormMahalanobisDistance(dsc, data);
}



//*****************************************************
//
//   Qualitative Calibration
//
//*****************************************************


// Description:
// Construct a qualitative calibration

void* QltAnaliz_Create(vector<CSampleProxy> *pSamples, // array of samples
					const CFreqScale *FreqScale, // ��������� ����� ��������
					const CSpecPreproc *prePar, // preprocessing parameters
					int numFactors) // number of factors
{
	return 	(void *) new CQltCalibration(pSamples, FreqScale,
		prePar, numFactors);
}

// Description:
// Construct a qualitative calibration from file

/*void* QltAnaliz_Load(const char* fileName)
{
	return 	(void *) new CQltCalibration(fileName);
}*/


// Description:
// Destroy a qualitative calibration

void QltAnaliz_Destroy(void* pQlt)
{
	if (pQlt) delete ((CQltCalibration*)pQlt);
//	AppendToDebugReport("Qlt model was destroyed");
}


// Description:
// Save the qualitative calibration in file

/*void QltAnaliz_Save(void* pQlt, const char* fileName)
{
//	AppendToDebugReport("Qlt model to be saved");

	((CQltCalibration*)pQlt)->Save(fileName);

//	AppendToDebugReport("Qlt model was saved");
}*/


// Description:
// Return the error of the qualitative calibration

int  QltAnaliz_GetError(void* pQlt)
{
	return ((CQltCalibration*)pQlt)->GetError();
}


// Description:
// Return the Mahalanobis distance for a given spectrum

//former _DLL_API_ double QltAnaliz_PCA_GetDistance(...
double QltAnaliz_GetExtSpecDistance(void* pQlt,
										   const CFreqScale& dsc, // spectrum descriptor
										   const double* data) // spectrum data
{
	return ((CQltCalibration*)pQlt)->CalculateMahalanobisDistance(dsc, data);
}


double QltAnaliz_GetExtSpecNormDistance(void* pQlt,
										   const CFreqScale& dsc, // spectrum descriptor
										   const double* data) // spectrum data
{
	return ((CQltCalibration*)pQlt)->CalculateNormMahalanobisDistance(dsc, data);
}


// Description:
// Return the Mahalanobis distances for all spectra in the sample

void QltAnaliz_GetSampleDistances(void* pQlt,
								int nSample, // sample order number
								double* dist) // array of distances to be returned
{
	((CQltCalibration*)pQlt)->GetSampleDistances(nSample, dist);
}

void QltAnaliz_GetSampleNormDistances(void* pQlt,
								int nSample, // sample order number
								double* dist) // array of distances to be returned
{
	((CQltCalibration*)pQlt)->GetSampleNormDistances(nSample, dist);
}

// Description:
// Return the Mahalanobis distances for mean spectrum of the sample

double QltAnaliz_GetSampleDistanceAv(void* pQlt,
								int nSample) // sample order number
{
	return ((CQltCalibration*)pQlt)->GetSampleDistance(nSample);
}

double QltAnaliz_GetSampleNormDistanceAv(void* pQlt,
								int nSample) // sample order number
{
	return ((CQltCalibration*)pQlt)->GetSampleNormDistance(nSample);
}


// Description:
// Return the Mahalanobis distances for the given spectrum in the sample

void QltAnaliz_GetSampleSpecDistance(void* pQlt,
								  int nSample, // sample order number
								  int nSpec,   // spectrum order number
							      double* dist) // the distance to be returned
{
	((CQltCalibration*)pQlt)->GetSampleSpecDistance(nSample, nSpec, dist);
}

// Description:
// Return the number of samples in the qualitative model

int  QltAnaliz_GetNumSamples(void* pQlt)
{
	return ((CQltCalibration*)pQlt)->GetNumSamples();
}


// Return the number of spectra for the specific sample 
int  QltAnaliz_GetNumSampleSpecs(void* pQlt, int nSample) // sample order number
{
	return ((CQltCalibration*)pQlt)->GetNumSampleSpectra(nSample);
}

// Description:
// Return the number of variables in the qualitative model

int  QltAnaliz_GetNumVariables(void* pQlt)
{
	return ((CQltCalibration*)pQlt)->GetNumVariables();
}

/*const char* QltAnaliz_GetSampleName(void* pQlt,
									  int nSample) // sample order number
{
	return ((CQltCalibration*)pQlt)->GetSampleName(nSample);
}*/
		

/*const char* QltAnaliz_GetSampleSpecName(void* pQlt,
												  int nSample, // sample order number
												  int nSpec)   // spectrum order number
{
	return ((CQltCalibration*)pQlt)->GetSampleSpecName(nSample, nSpec);
}*/

// added 29.11.2003 for compliment of calibration

/*void QltAnaliz_AddSamples(void *pQlt,
					int numSamples, // number of samples
					const CSampleProxy* pSamples, // array of samples
					const CSpecPreproc &prePar, // preprocessing parameters
					int numFactors) // number of factors
{
	 ((CQltCalibration*)pQlt)->AddSamples(numSamples, pSamples,
		prePar, numFactors);
}*/
					


//*****************************************************
//
//   IRSecv
//
//*****************************************************

// Description:
// Construct an object of IRSecv class


/*void* IRSecv_Create(const char *fname)
{ 
//d
//	AppendToDebugReport("IRSecv_Create start");
//	void *p = (void *) new CSecv(fname);

//d
//	AppendToDebugReport("IRSecv_Create end");

	return (void *) new CSecv(fname);
}

//void* IRSecv_Create(int numSamples, // number of samples
//					const CSampleProxy* samples, // array of samples
//					const CSpecPreproc& prePar, // preprocessing parameters
//					int nModelType, // calibartion model type (default - PLS)
///					int numFactors) // number of factors
//{
//	return 	(void *) new CSecv(numSamples, samples,
//		prePar, nModelType, numFactors);
//}

// Description:
// Destroy the object of IRSecv class

void IRSecv_Destroy(void* pSECV)
{
    if (pSECV) delete ((CSecv*)pSECV);
}

// Description:
// Return the error of cross-validation

int IRSecv_GetError(void* pSECV)
{
	return ((CSecv*)pSECV)->GetError();
}

// Description:
// Return the number of samples

int IRSecv_GetNumSamples(void* pSECV)
{
	return ((CSecv*)pSECV)->GetNumSamples();
}


// Description:
// Return the predicted concentrations for the spectra of given sample

void IRSecv_GetPredConc(void* pSECV,
						int nSample, // sample order number
						int nSpec, // spectrum order number
						double* compConc) // arrays of predicted components concentration
{
	((CSecv*)pSECV)->GetPredConc(nSample, nSpec, compConc);
}


void IRSecv_GetPredConcAv(void* pSECV,
						int nSample, // sample order number
						double* compConc) // arrays of predicted components concentration
{
	((CSecv*)pSECV)->GetPredConc(nSample, compConc);
}

// Description:
// Return the SECV(standard error of cross validation) for a given component

int IRSecv_GetSECV(void* pSECV,
							const char* compName, // the  component name of interest
							double* secv) // SECV for this component
{
	return ((CSecv*)pSECV)->GetSECV(compName, secv);
}


// Description:
// Return the R^2 statistic for a given component

int IRSecv_GetR2stat(void* pSECV,
					const char* compName, // component name
					double* R2stat) // R^2 statistic for this component
{
	return ((CSecv*)pSECV)->GetR2Stat(compName, R2stat);
}

// Description:
// Return the F statistic for a given component

int IRSecv_GetFstat(void* pSECV,
								   const char* compName, // component name
								   double* Fstat) // F statistic for this component
{
	return ((CSecv*)pSECV)->GetFStat(compName, Fstat);
}

int IRSecv_GetSEC(void* pSECV,
				  int nSample, // excluded sample order number
			double* sec) // array[numberOfComponents] of sec for this sample
{
	return ((CSecv*)pSECV)->GetSEC(nSample, sec);
}

void IRSecv_GetRefConc(void* pSECV,
									int nSample, // sample order number
									double* compConc) // array of reference data for this sample
{
	((CSecv*)pSECV)->GetRefConc(nSample, compConc);
}*/