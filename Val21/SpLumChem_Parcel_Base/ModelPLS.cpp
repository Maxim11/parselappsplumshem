#include "ModelPLS.h"
#include "SpLumChemStructs.h"
#include "MatrixUtilities.h"

#include "newmatap.h"
#include <math.h>
#include "PLSutils.h"
//#include <time.h>

//extern double durationPLSinit, durationPLScycle, durationPLSend, durationPLSpredict;


CModelPLS::CModelPLS(const Matrix& mSpectra, const Matrix& mComponents, int nFactors )
{
	nModelType = MODEL_PLS;
	nError = ER_OK;
	bF2inv = false;

//	PrintMatrix(mSpectra, "mSpectraPLS.dat");
//	PrintMatrix(mComponents, "mComponentsPLS.dat");

	if (mComponents.Ncols() == 1)
	{
		CreateOne(mSpectra, mComponents, nFactors);
	}
	else
	{
		CreateMany(mSpectra, mComponents, nFactors);
	}
}

void CModelPLS::CreateMany(const Matrix& mSpectra, const Matrix& mComponents, int nFactors)
{
	const double eps = 1e-12;


	Matrix mX;        // the matrix of spectral residuals
	Matrix mY;        // the matrix of concentration residuals

	mX = mSpectra;
	mY = mComponents;


	int nRowsX = mX.Nrows(); // m = numSpectra
	int nColumnsX = mX.Ncols(); // n = numFreq
	int nColumnsY = mY.Ncols(); // l = numComp

	int nRank = __min(nRowsX, nColumnsX);
	numFactors = nFactors;

	if ((numFactors > 0)  &&  (numFactors <= nRank))
	{
		nRank = numFactors;
	}
	else
	{
		nError = ER_PLS;
		return;
	}

	Matrix mXY(nColumnsX, nColumnsY), mQT(nColumnsY, nColumnsY), mTB(1, 1);

	mXY = 0.0;
	mQT = 0.0;
	mTB = 0.0;

	DiagonalMatrix mS(nColumnsY);

	mS = 0.0;

	Matrix mP;        // the matrix of spectral factors
	Matrix mQ;        // the matrix of concentration factors
	Matrix mW;        // the matrix of weights
	Matrix mT;        // the matrix of spectral scores
	Matrix mU;        // the matrix of concentration scores
	RowVector vB;     // the vector containing the inner relationships


	ColumnVector vTC(nRowsX), vQC(nColumnsY);

	mP.ReSize(nRank, nColumnsX);
	mP = 0.0;
	mQ.ReSize(nRank, nColumnsY); // numFactors x numComp
	mQ = 0.0;
	mW.ReSize(nColumnsX, nRank);
	mW = 0.0;
	vB.ReSize(nRank);
	vB = 0.0;
	mT.ReSize(nRowsX, nRank);
	mT = 0.0;
	mU.ReSize(nRowsX, nRank);
	mU = 0.0;


	for (int h = 1; h <= nRank; h++)
	{

		mXY = mX.t() * mY;

		SymmetricMatrix mXYXY;

		mXYXY << mXY.t() * mXY;

		EigenValues(mXYXY, mS, mQT);

		if (!CheckDescending(mS))
		{
			// sort s and qt in descending order
			SortDescending(mS, mQT);
		}

		vQC = mQT.Column(1);

		mQ.Row(h) = vQC.t();

/*		if (h==4)
		{
			PrintMatrix(mS, "mS_4.dat");
			PrintMatrix(mXYXY, "mXYXY_4.dat");
		}*/

		// � ���������� �������� ���������� xy.t()*xy ����� ��������� ����,
		// ��� �������� � ������������� �������. �������:
		if (mS(1) < eps)
		{

			nError = ER_PLS;
            _snprintf(strErrorMessage, MAX_ERR_MSG, "Error PLS. Factor %d. Eigenvalue = %.2e", h, mS(1));
//			AppendToDebugReport(strErrorMessage);

			return;
		}

		mW.Column(h) = (mXY * vQC) / sqrt(mS(1));

		vTC = mX * mW.Column(h);

		mT.Column(h) = vTC;

		mU.Column(h) = mY * vQC;

		double tsqr = SumSquare(vTC);

		mP.Row(h) = (vTC.t() * mX) / tsqr;

		mTB = (vTC.t() * mU.Column(h));

		vB(h) = mTB(1,1) / tsqr;

		mX -= vTC * mP.Row(h);

		mY -= vB(h) * (vTC * mQ.Row(h));
	}

	mFactors = mP;
	mScores = mT;
	mYloadings = mQ.t();


	//mF2inv = (mFactors * mFactors.t()).i();

	vS2.ReSize(mScores.Ncols());

	for (int k = 1; k <= mScores.Ncols(); k++)
	{
		vS2(k) = sqrt((mScores.Column(k)).SumSquare());
	}

	int h = mFactors.Ncols();
	int mrank = mFactors.Nrows();

	if (numFactors != 0) mrank = (mrank < numFactors) ? mrank : numFactors;

	DiagonalMatrix mI(h);
	Matrix mTnew(h, h);
	//Matrix mCalT(h, mrank);
	//mCalibr.ReSize(h, mrank);

	mI = 1.0;
	mTnew = mI;

	mCalibr = vB(1) * (mW.Column(1) * mQ.Row(1));

	for (int f = 2; f <= mrank; f++)
	{
		mTnew -= mTnew *  mW.Column(f - 1) *  mP.Row(f - 1);
		mCalibr += vB(f) * (mTnew * mW.Column(f) * mQ.Row(f));
	}

	//mCalibr = mCalT.t();

	//PrintMatrix(mCalibr, "mCalibrPLS.dat");
}

void CModelPLS::CreateOne(const Matrix& mSpectra, const Matrix& mComponents, int nFactors)
{
//	PrintMatrix(mSpectra, "mSpectraPLS.dat");
//	PrintMatrix(mComponents, "mComponentsPLS.dat");

//	clock_t start, finish;

//	start = clock();

	const double eps = 1e-12;

	Matrix mX;        // the matrix of spectral residuals
	Matrix mY;        // the matrix of concentration residuals

	mX = mSpectra; // numSp x numFreq
	mY = mComponents; // numSp x 1


	int nRowsX = mX.Nrows(); // m
	int nColumnsX = mX.Ncols(); // n
	int nColumnsY = mY.Ncols(); // l

	int nRank = __min(nRowsX, nColumnsX);
	numFactors = nFactors;

	if ((numFactors > 0)  &&  (numFactors <= nRank))
	{
		nRank = numFactors;
	}
	else
	{
		nError = ER_PLS;
		return;
	}

	//Matrix mXY(nColumnsX, nColumnsY);
	Matrix mQT(nColumnsY, nColumnsY);//, mTB(1, 1);

//	mXY = 0.0;
	mQT = 0.0;
//	mTB = 0.0;

	//DiagonalMatrix mS(nColumnsY);


//	Matrix mU;        // the matrix of concentration scores


	ColumnVector vTC(nRowsX);//, vQC(nColumnsY);
	ColumnVector vXY(nColumnsX);
	vXY = 0;

	Matrix mP(nRank, nColumnsX);// the matrix of spectral factors
//	mP.ReSize(nRank, nColumnsX);
	mP = 0;
	//mQ.ReSize(nRank, nColumnsY);
	//mQ = 0.0;

//	Matrix mQ;        // the matrix of concentration factors	Matrix mQ;        // the matrix of concentration factors
	ColumnVector vQ(nRank);        // the matrix of concentration factors	Matrix mQ;        // the matrix of concentration factors

//	vQ.ReSize(nRank);
	vQ = 0;

	Matrix mW(nColumnsX, nRank);// the matrix of weights
//	mW.ReSize(nColumnsX, nRank);
	mW = 0;

	RowVector vB(nRank);// the vector containing the inner relationships
//	vB.ReSize(nRank);
	vB = 0.0;

	Matrix mT(nRowsX, nRank);// the matrix of spectral scores
//	mT.ReSize(nRowsX, nRank);
	mT = 0.0;
//	mU.ReSize(nRowsX, nRank);
//	mU = 0.0;

//	finish = clock();
//	durationPLSinit += (double)(finish - start);// / CLOCKS_PER_SEC;

//	start = clock();

	double fS, tsqr;

	for (int h = 1; h <= nRank; h++)
	{

		vXY = mX.t()*mY; // numFreq x 1

		//SymmetricMatrix mXYXY;

		//mXYXY << mXY.t() * mXY;

		//EigenValues(mXYXY, mS, mQT);

		/*if (!CheckDescending(mS))
		{
			// sort s and qt in descending order
			SortDescending(mS, mQT);
		}*/

		//vQC = mQT.Column(1);

		//mQ.Row(h) = vQC.t();

		fS = vXY.SumSquare();
		vQ(h) = sqrt(fS);//1.0;

/*		if (h==4)
		{
			PrintMatrix(mS, "mS_4.dat");
			PrintMatrix(mXYXY, "mXYXY_4.dat");
		}*/

		// � ���������� �������� ���������� xy.t()*xy ����� ��������� ����,
		// ��� �������� � ������������� �������. �������:
		if (fS < eps)
		{

			nError = ER_PLS;
            _snprintf(strErrorMessage, MAX_ERR_MSG, "Error PLS. Factor %d. Eigenvalue = %.2e", h, fS);
//			AppendToDebugReport(strErrorMessage);

			return;
		}

//		mW.Column(h) = (mXY * vQC) / sqrt(mS(1));
		mW.Column(h) = vXY/sqrt(fS);

		vTC = mX * mW.Column(h);

		mT.Column(h) = vTC;

//		mU.Column(h) = mY * vQC;
//		mU.Column(h) = mY;

		tsqr = vTC.SumSquare();

		mP.Row(h) = (vTC.t()/tsqr)*mX;

		//mTB = vTC.t() * mY;//mU.Column(h));

		//vB(h) = mTB(1,1) / tsqr;
		vB(h) = (vTC.t()*mY).AsScalar() / tsqr;

		mX -= vTC * mP.Row(h);

		mY -= vB(h) * vTC;// * vQ(h));
	}

	mFactors = mP;
	mScores = mT;
	mYloadings = vQ.t();

	vS2.ReSize(mScores.Ncols());

	for (int k = 1; k <= mScores.Ncols(); k++)
	{
		vS2(k) = sqrt((mScores.Column(k)).SumSquare());
	}

//	finish = clock();
//	durationPLScycle += (double)(finish - start);// / CLOCKS_PER_SEC;

	//mF2inv = (mFactors * mFactors.t()).i();

	int h = mFactors.Ncols();
	int mrank = mFactors.Nrows();

	if (numFactors != 0) mrank = (mrank < numFactors) ? mrank : numFactors;

	//DiagonalMatrix mI(h);
	//Matrix mCalT(h, mrank);
	//mCalibr.ReSize(h, mrank);

	//mI = 1.0;
	//mTnew = mI;

//	start = clock();

	//AppendToDebugReport("mCalibr.ReSize start");
	//mCalibr.ReSize(h, 1);
	//AppendToDebugReport("mCalibr.ReSize end");

	mCalibr = vB(1)*mW.Column(1);
	//AppendToDebugReport("mCalibr=");

/*	if (mrank == 2)
	{
		Matrix mTW(h, 1);
		mTW = mW.Column(2) - mW.Column(1)*(mP.Row(1)*mW.Column(2));
		mCalT += vB(2)*mTW;
	}
	else
		if (mrank == 3)
		{
			Matrix mTW(h, 1);
			mTW = mW.Column(2) - mW.Column(1)*(mP.Row(1)*mW.Column(2));
			mCalT += vB(2)*mTW;

			mTW *= (mP.Row(2)*mW.Column(3)).AsScalar();
			mCalT += vB(3)*(mW.Column(3)-mW.Column(1)*(mP.Row(1)*mW.Column(3))-mTW);
		}
		else
		{
			Matrix mTnew(h, h);
			mTnew =  mW.Column(1) *  mP.Row(1);
			mCalT += vB(2)*(mTnew * mW.Column(2));

			for (int f = 3; f <= mrank; f++)
			{
				mTnew -= mTnew *  mW.Column(f - 1) *  mP.Row(f - 1);
				mCalT += vB(f)*(mTnew * mW.Column(f));
			}
		}*/

	if ((mrank >= 2) && (mrank <= 10))
	{
		ColumnVector vTW(nColumnsX);
		ColumnVector vTWhelp(nColumnsX);
		
		vTW = mW.Column(1);

		double fS;
		// i = f-1; i+1=f
		for (int f = 2; f <= mrank; f++)
		{
			GetTW(f-2, f, mW, mP, vTWhelp);
			fS = (mP.Row(f-1)*mW.Column(f)).AsScalar();
			vTW *= -fS;
			vTW += vTWhelp;
			mCalibr += vB(f) * vTW;
		}
	}
	else
		if (mrank > 10)
		{
			DiagonalMatrix mI(h);
			mI = 1.0;
			Matrix mTnew(h, h);
			mTnew = mI;
			mTnew -=  mW.Column(1) *  mP.Row(1);
			mCalibr += vB(2)*(mTnew * mW.Column(2));

			for (int f = 3; f <= mrank; f++)
			{
				mTnew -= mTnew *  mW.Column(f - 1) *  mP.Row(f - 1);
				mCalibr += vB(f)*(mTnew * mW.Column(f));
			}
		}

	//mCalibr = mCalT.t();

//	finish = clock();
//	durationPLSend += (double)(finish - start);// / CLOCKS_PER_SEC;

	//AppendToDebugReport("mCalibr end");
	//PrintMatrix(mCalibr, "mCalibrPLS.dat");
}

// � ������� ����������� PCR-������ ��������� ������������
// ������������ ��������� ��� ������� �������� (����������������)
void CModelPLS::Predict(const Matrix& mX, Matrix& mYpredicted)
{
	//AppendToDebugReport("Predict start");
	//clock_t start, finish;

	//start = clock();

	if (mX.Ncols() == mCalibr.Nrows())
	{
		//mYpredicted = mX * mCalibr.t();
		mYpredicted = mX * mCalibr;
	}
	else nError = ER_PREDICT;

	//finish = clock();
	//durationPLSpredict += (double)(finish - start);// / CLOCKS_PER_SEC;

}

// �� ����������� PLS-������ ��������� ���������� ������������
// ��� ������ ������� �������� (������� ��������������)
void CModelPLS::CalculateMahalanobisDistance(const Matrix& mSp, ColumnVector& vMahDist)
{
	if (mSp.Ncols() != mFactors.Ncols())
	{
		nError = ER_OUT_OF_RANGE;
	}
	else
	{
		Matrix mSpScores;
		CalculateScores(mSp, mSpScores);

		// ��������� ������� mScores;
		for (int k = 1; k <= mSpScores.Ncols(); k++)
		{
			mSpScores.Column(k) /= vS2(k);
		}

		int nSp = mSpScores.Nrows();

		vMahDist.ReSize(nSp);

		for (int i = 1; i <= nSp; i++)
		{
			vMahDist(i) = sqrt((mSpScores.Row(i)).SumSquare());
		}
	}
}

// ��������� ���������� ������������ ��� ��������������
// �������� �� ������� ��������� PCA-������
void CModelPLS::GetMahalanobisDistance(ColumnVector& vMahDist) const
{
	int nSp = mScores.Nrows();

	vMahDist.ReSize(nSp);

	// normalize mScores;

	Matrix mSpScores = mScores;

	for (int k = 1; k <= mSpScores.Ncols(); k++)
	{
		mSpScores.Column(k) /= vS2(k);
	}

	for (int i = 1; i <= nSp; i++)
	{
		vMahDist(i) = sqrt((mSpScores.Row(i)).SumSquare());
	}
}

// �� ����������� PLS-������ ��������� ������� ������
// ��� ������ ������� �������� (������� ��������������)
void CModelPLS::CalculateScores(const Matrix& mSp, Matrix& mSpScores)
{
	// ��������� mF2inv ���� ����������
	if (!bF2inv)
	{
		mF2inv = (mFactors * mFactors.t()).i();

		bF2inv = true;
	}

	if (mSp.Ncols() != mFactors.Ncols())
	{
		nError = ER_OUT_OF_RANGE;
	}
	else
	{
		mSpScores.ReSize(mSp.Nrows(), mF2inv.Ncols());
		mSpScores = (mSp * mFactors.t()) * mF2inv;
	}
}

void CModelPLS::Save(FILE* fgh, int clbVersion) const
{
	fwrite(&nModelType, sizeof(int), 1, fgh);
	SaveMatrix(mFactors, fgh);
	SaveMatrix(mScores, fgh);
	SaveMatrix(mCalibr, fgh);
}

void CModelPLS::Load(FILE* fgh, int clbVersion)
{
	LoadMatrix(mFactors, fgh);
	LoadMatrix(mScores, fgh);
	LoadMatrix(mCalibr, fgh);
	numFactors = mFactors.Nrows();
	mF2inv = (mFactors * mFactors.t()).i();
	vS2.ReSize(mScores.Ncols());

	for (int i = 1; i <= mScores.Ncols(); i++)
	{
		vS2(i) = sqrt((mScores.Column(i)).SumSquare());
	}
}

void CModelPLS::FillModel_Parcel(CModel_Parcel& mModel) const
{
	mModel.nModelType = nModelType;
//	mModel.mCalibr = Matrix2MDbl(mCalibr);
//	mModel.mFactors = Matrix2MDbl(mFactors);
//	mModel.mScores = Matrix2MDbl(mScores);

	Matrix2MDblResize(mCalibr, mModel.mCalibr);
	Matrix2MDblResize(mFactors, mModel.mFactors);
	Matrix2MDblResize(mScores, mModel.mScores);
	Matrix2MDblResize(mYloadings, mModel.mYloadings);
}

void CModelPLS::LoadFromQnt_Parcel(const CModel_Parcel& mModel)
{
	nModelType = mModel.nModelType;
	mCalibr = MDbl2Matrix(mModel.mCalibr);
	mFactors = MDbl2Matrix(mModel.mFactors);
	mScores = MDbl2Matrix(mModel.mScores);
}


