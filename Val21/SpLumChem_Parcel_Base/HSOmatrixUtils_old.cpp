//---------------------------------------------------------------------------
//#pragma hdrstop
#include "HSOMatrixUtils.h"
//#include "MatrixUtils.h"
//#include <time.h>
#include "newmat.h"
//#include "newmatap.h"
#include "fftw3.h"
#include <complex>

using namespace std;
typedef std::complex<double> Complex;

//#include <math.h>
//#define _USE_MATH_DEFINES 
#define M_PI 3.14159265358979323846

extern void AppendToDebugReport(const char* str);

//double durationTrans = 0;
//double durationHCQP = 0;
//double durationRev = 0;


// global variables:
int numFreqGlobal = 0;
double *pInGlobal = 0, *pOutGlobal = 0;
fftw_plan mFFTplanGlobal;

int numFactGlobal = 0;
Matrix mGarmGlobal;



//--------------------------------------------------------------------------
// ���������� ��� ������ �� ������� lemke1()
int HCQP(const Matrix &mX, const ColumnVector &vY, double D, double Knl, ColumnVector &vPGarm)
{
//	i = HCQP(xGarm, vY, SizeGcub, 0, vPGarm);//������� ������ HCQP
// mX [numFact x numSp]
// vY [numSp]
// vPGarm [numFact]
// n = numSp
// k = numFact

	int numFact = mX.Nrows();
	int numSp = mX.Ncols();

	Matrix mC(numFact, numFact);
	ColumnVector vD(numFact);

//	AppendToDebugReport("Start rpcf");

	rpcf(mX, vY, mC, vD);

//	AppendToDebugReport("End rpcf");


	//double *dop;
	//dop = new double[k];
	ColumnVector vDop(numFact);

	vDop = D;

	if (Knl)
	{
		for (int i = numFact/2; i < numFact; i++)
		{
            vDop(i) *= Knl;
		}
	}

//	i = lemke1(mC, vD, vDdop, numFact, vP, false);

	double *c = new double[numFact*numFact];
	double *d = new double[numFact];
	double *xd = new double[numFact];
	double *x = new double[numFact];

	for (int k = 0; k < numFact; k++)
	{
		d[k] = vD(k+1);
		xd[k] = vDop(k+1);

        for (int j = 0; j < numFact; j++)
		{
			c[k*numFact + j] = mC(k+1, j+1);
		}
	}

	int i = lemke1(c, d, xd, numFact, x, false);

	for (k = 0; k < numFact; k++)
	{
		vPGarm(k+1) = x[k];
	}

	return i;
}
//----------------------------------------------------------------------------
void rpcf(const Matrix &mX, const ColumnVector &vY, Matrix &mC, Matrix &mD)
{
// mX [numFact x numSp]
// vY [numSp]
// c [numFact x numFact] = mX * mX.t()
// d [numFact] = mX * vY

//MatrixMultiplication(k,1,n,0,0,X,Y,d);
//MatrixMultiplication(k,k,n,0,1,X,X,c);

	mC = mX * mX.t();
	mC *= 2.0;

	mD = mX * vY;
	mD *= -2.0;

	return;
}





//���������(�������������/��������) �������������� ������� x
//	Transformation(numGarm, mX, numFact, IndexUseFreq, mXGarm, mGarm);

/*void Transformation(int numGarm, const Matrix &mX, int numFact,
					const int *pIndexUseFreq, Matrix &mXGarm, Matrix &mGarm)
{
	// mX [numFreq x numSpec]
	// mXGram [numFact x numSpec]
	// mGarm [numFact x numSpec]

//	for (int i = 0; i < numFreq; i++)
//	{
//		if (IndexUseFreq[i] == 0) mSp.Row(i+1) = 0.0;
//	}

	if (numGarm)
	{
    //������������ ������� �������������� �������������� MGarm[numFreq*numFact]
			GarmTransformation(mGarm);
			GarmTransformation(numFact, mX, mXGarm);
//			AppendToDebugReport("GarmTrans OK");
	}
	else
	{
    //������������ ������� ��������� �������������� MGarm[numFreq*numFact]
		PointTransformation(pIndexUseFreq, mGarm);
	}

//	AppendToDebugReport("xGarm OK");

	if (bPrint)	
	{
		PrintMatrix("xGarm_my.dat", xGarm, numFact, numFreq);
		bPrint = false;
	}

return;
}*/

/*void Transformation(int numGarm, const Matrix &mX, int numFact,
					const int *pIndexUseFreq, Matrix &mXGarm)
{
	// mX [numFreq x numSpec]
	// mXGram [numFact x numSpec]
	// mGarm [numFact x numSpec]

//	for (int i = 0; i < numFreq; i++)
//	{
//		if (IndexUseFreq[i] == 0) mSp.Row(i+1) = 0.0;
//	}

	if (numGarm)
	{
    //������������ ������� �������������� �������������� MGarm[numFreq*numFact]
	//		GarmTransformation(mGarm);
			GarmTransformation(numFact, mX, mXGarm);
//			AppendToDebugReport("GarmTrans OK");
	}
	else
	{
    //������������ ������� ��������� �������������� MGarm[numFreq*numFact]
		PointTransformation(pIndexUseFreq, mGarm);
		mXGarm = mGarm.T()*mX;
	}

	return;
}*/

/*void PointTransformation(const int *pIndexUseFreq, Matrix &mGarm)
{
	// mGarm[numFreq x numFact]
	int numFreq = mGarm.Nrows();
	int numFact = mGarm.Ncols();

	mGarm = 0.0;

	//������������ MGarm � ������ IndexUseFreq[numFreq]
	int nFact=0;

	for(int nFreq = 0; nFreq < numFreq; nFreq++)
	{
		if(pIndexUseFreq[nFreq])
		{
			mGarm(nFreq+1, nFact+1) = 1.0;
			nFact++;
		}
	}

	return;
}*/



/*void GarmTransformation(const int *pIndexUseFreq, Matrix *mGarm)
{
	// mGarm [numFreq x numFact]
	//������������ mGarm � ������ IndexUseFreq[numFreq]

	int numFreq = mGarm.Nrows();
	int numFact = mGarm.Ncols();

	Complex zW, zS, zS0;
	zS0 = Complex(1.0, 0);

	for(int m = 0; m <= numFact/2; m++)
	{
		zW = Complex(polar(1.0, 2*M_PI*m/(numFreq-1.0)));
		zS = zS0;

		for(int nFreq = 0; nFreq < numFreq; nFreq++)
		{
			if(m == 0)
			{
				MGarm(nFreq+1, 1) = pIndexUseFreq[nFreq]*0.5;
			}
			else
			{
				MGarm(nFreq+1, 2*m) = pIndexUseFreq[nFreq] * zS.real();
				MGarm(nFreq+1, 2*m+1) = pIndexUseFreq[nFreq] * zS.imag();
			}
			zS *= zW;
		}
		zS0 *= -1.0;
	}

	// test
//	if (bFlagPrint)
//	{
//		PrintMatrix("mGarm.dat", MGarm, numFreq, numFact); 
//		PrintMatrix("mGarm_my.dat", MGarm_my, numFreq, numFact); 
//		bFlagPrint = false;
//	}

	return;
}*/

/*void GarmTransformation(int numFact, const Matrix &mX, Matrix &mXGarm)
{
	// mX [numFreq x numSp]
	// mXgarm [numFact x numSp]
	int numSp = mX.Ncols();
	int numFreq = mX.Nrows();

	ColumnVector vSpCut(numFreq-1);

	ColumnVector vRe, vIm;

	double fXlast;

	for (int i = 1; i <= numSp; i++)
	{
		vSpCut = mX.SubMatrix(1, numFreq-1, i, i);
		fXlast = mX(numFreq, i);
		RealFFT(vSpCut, vRe, vIm);

		mXGarm(1, i) = 0.5*(vRe(1) + fXlast);
		int nSign = -1;

		for (int m = 1; m <= numFact/2; m++)
		{
			mXGarm(2*m, i) = nSign*(vRe(m+1)+fXlast);
			mXGarm(2*m+1, i) = -nSign*vIm(m+1);
			nSign *= -1;
		}
	}

//	AppendToDebugReport("GarmTrans end");
}*/

void GarmTransformationFFTW(int numFact, const Matrix &mX, Matrix &mXGarm)
{
	// mX [numFreq x numSp]
	// mXgarm [numFact x numSp]
	int numSp = mX.Ncols();
	int numFreq = mX.Nrows();

//	ColumnVector vSpCut(numFreq-1);

//	ColumnVector vRe, vIm;

	double fXlast;

	InitFFTW(numFreq, numFact);

	for (int i = 1; i <= numSp; i++)
	{
		for (int k = 0; k < numFreq-1; k++)
		{
			pInGlobal[k] = mX(k+1, i); 
		}
		//vSpCut = mX.SubMatrix(1, numFreq-1, i, i);
		fXlast = mX(numFreq, i);

		fftw_execute(mFFTplanGlobal);

		//RealFFT(vSpCut, vRe, vIm);

		mXGarm(1, i) = 0.5*(pOutGlobal[0] + fXlast);
		int nSign = -1;

		for (int m = 1; m <= numFact/2; m++)
		{
			if (nSign > 0)
			{
				mXGarm(2*m, i) = pOutGlobal[m] + fXlast;
				mXGarm(2*m+1, i) = -pOutGlobal[numFreq-1-m];
			}
			else
			{
				mXGarm(2*m, i) = -pOutGlobal[m] + fXlast;
				mXGarm(2*m+1, i) = pOutGlobal[numFreq-1-m];
			}

			nSign *= -1;
		}
	}

//	AppendToDebugReport("GarmTrans end");
}


void GarmTransformation(Matrix &mGarm)
{
	// mGarm[numFreq, numFact]
	int numFact = mGarm.Ncols();
	int numFreq = mGarm.Nrows(); 

	Complex zW, zS, zS0, zW0;
	zS0 = Complex(-1.0, 0);
	zW0 = Complex(polar(1.0, 2*M_PI /(numFreq-1.0)));
	zW = Complex(1.0, 0);

	mGarm.Column(1) = 0.5;

	for(int m = 1; m <= numFact/2; m++)
	{
		zS = zS0;
		zW *= zW0;

		for(int nFreq = 0; nFreq < numFreq; nFreq++)
		{
			mGarm(nFreq + 1, 2*m) = zS.real();
			mGarm(nFreq + 1, 2*m+1) = zS.imag();
			zS *= zW;
		}
		zS0 *= -1.0;
	}

	// test
/*	if (bFlagPrint)
	{
		PrintMatrix("mGarm.dat", MGarm, numFreq, numFact); 
		PrintMatrix("mGarm_my.dat", MGarm_my, numFreq, numFact); 
		bFlagPrint = false;
	}*/

	return;
}

//---------------------------------------------------------------------------
//#pragma package(smart_init)
//-------------------------------------------------------------------------
// ���������� ��� ������
int lemke1 ( double  *c,double  *d, double  *xd, int m, double  *x,bool STOP )
 {
   int currentIter;
   double eps;
   eps=Epsilon();
   double r,r1;
   double *q,*tab,*cpcr,*cpcl;
   int pcl,pcr,baz1,*baz;
   int i, j, n;
   n = 2*m;
   q = new double[n];
   for(i = 0;i < m; i ++)
      {
      q[i] = d[i];
      for(j = 0;j < m; j ++)
         q[i] -= c[i*m + j] * xd[j];
      q[i + m] = 2*xd[i];
      }
   r=0.0;
   for (i=0;i<n;i++)
     if (r>q[i])
       {
       r=q[i];
       pcr=i;
       }
   if ( r == 0.0 )
     {
     for (i=0;i<m;i++)
        x[i]= -xd[i];
     delete []q;
     return 0;
     }
   tab = new double[n*(2*n+2)];
   cpcr = new double[2*n+2];
   cpcl = new double[n];
   baz = new int[n];
   pcl = 2 * n;
   for ( i = 0; i < n; i++ )
       {
       baz[i] = i;
       tab[i * (2*n + 2) + i] = 1;
       tab[i * (2*n + 2) + 2*n] = (-1);
       tab[i * (2*n + 2) + 2*n+1] = q[i];
       for ( j = 0; j < n; j++ )
           {
           if(i != j)
             tab[i * (2*n + 2) + j] = 0;
           tab[i * (2*n + 2) + n + j] = ((i<m)&&(j<m)) ?  -c[i*m + j] : 0.0;
           }
       if(i<m)
         tab[i * (2*n + 2) + i + 3*m] = -1;
       else
         tab[i * (2*n + 2) + i + m] = 1;
       }
   currentIter=0;
   while(1)
        {
        if(STOP)
           {
           delete []q;
           delete []tab;
           delete []cpcr;
           delete []cpcl;
           delete []baz;
           return -1;
           }
        if ( ( fabs ( tab[pcr * (2*n + 2) + pcl] ) ) < eps )
           {
           delete []q;
           delete []tab;
           delete []cpcr;
           delete []cpcl;
           delete []baz;
           return 2;
           }
        for ( i = 0; i < ( 2 * n + 2 ); i++ )
            cpcr[i] = tab[pcr * (2*n + 2) + i];
        for ( i = 0; i < n; i++ )
            cpcl[i] = tab[i * (2*n + 2) + pcl];
        for ( j = 0; j < ( 2 * n + 2 ); j++ )
            {
            tab[pcr * (2*n + 2) + j] = tab[pcr * (2*n + 2) + j] / cpcr[pcl];
            for ( i = 0; i < n; i++ )
                if ( i != pcr )
                   tab[i * (2*n + 2) + j] -= cpcr[j] * cpcl[i]/cpcr[pcl];
            }
    currentIter++;
        baz1 = baz[pcr];
        baz[pcr] = pcl;
        for ( i = 0; i < n; i++ )
           if ( ( baz[i] == 2*n ) && ( tab[i * (2*n + 2) + 2*n+1] > eps ) )
              break;
        if ( i == n )
           {
           for ( i = 0; i < m; i++ )
              x[i] = -xd[i];
           for ( i = 0; i < n; i++ )
              if ( (baz[i] >= n) && (baz[i] < 2*n) && ((baz[i]-n)< m) )
                 x[baz[i] - n] += tab[i * (2*n + 2) + 2*n+1];
           delete []q;
           delete []tab;
           delete []cpcr;
           delete []cpcl;
           delete []baz;
           return 0;
           }
        if ( baz1 >= n )
           pcl = baz1 - n;
        else
           pcl = baz1 + n;
        j = 0;
        for ( i = 0; i < n; i++ )
            {
            if ( tab[i * (2*n + 2) + pcl] > 0 )
               {
               r1 = tab[i * (2 * n + 2) + 2 * n + 1] / tab[i * ( 2 * n + 2) + pcl];
               j++;
               if ( ( j == 1 ) || ( r > r1 ) )
                  {
                  pcr = i;
                  r = r1;
                  }
               }
            }
        if ( j == 0 )
           {
           for (i=0;i<m;i++)
           x[i]= 0;
           delete []q;
           delete []tab;
           delete []cpcr;
           delete []cpcl;
           delete []baz;
           return 3;
           }
        }
 }

 void InitFFTW(int numFreq, int numFact)
 {
	 char strBuf[1024];
	 if (numFreq != numFreqGlobal)
	 {
//		AppendToDebugReport("InitFFTW start");

		if (pInGlobal) fftw_free(pInGlobal);
		if (pOutGlobal) fftw_free(pOutGlobal);

//		AppendToDebugReport("End memory free");

		//fftw_destroy_plan(mFFTplanGlobal);

		//AppendToDebugReport("End destroy plan");

		pInGlobal = (double*) fftw_malloc(sizeof(double) * (numFreq-1));
		pOutGlobal = (double*) fftw_malloc(sizeof(double) * (numFreq-1));

//		AppendToDebugReport("End malloc");

		mFFTplanGlobal = fftw_plan_r2r_1d(numFreq-1, pInGlobal, pOutGlobal, FFTW_R2HC, FFTW_ESTIMATE);

//		AppendToDebugReport("End create plan");

		numFreqGlobal = numFreq;
//		AppendToDebugReport("InitFFTW end");
	}

	 if (numFact != numFactGlobal)
	 {
//		AppendToDebugReport("Init mGarmGlobal start");
		mGarmGlobal.CleanUp();
		mGarmGlobal.ReSize(numFreq, numFact);
		GarmTransformation(mGarmGlobal);
		numFactGlobal = numFact;
//		AppendToDebugReport("Init mGarmGlobal end");
	}
 }

 void FreeFFTW()
 {
//	AppendToDebugReport("FreeFFTW start");
	if (pInGlobal) fftw_free(pInGlobal);
	if (pOutGlobal) fftw_free(pOutGlobal);
	fftw_destroy_plan(mFFTplanGlobal);
	numFreqGlobal = 0;
	pInGlobal=0;
	pOutGlobal=0;
	mGarmGlobal.CleanUp();
	numFactGlobal=0;
//	AppendToDebugReport("FreeFFTW end");
 }