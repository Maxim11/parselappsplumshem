#include "Sample.h"

Sample::Sample(const string& sname,
		const SpecDesc& desc,
		int numSp,
		const dVec pSp[],
		const string pSpName[],
		int numComp,
		const string pCompName[],
		const dVec& Comp)
{
	fSampleName = sname;
	fDesc = desc;
	fNumberOfSpectra = numSp;
	fSpectrumName = new string[fNumberOfSpectra];

	fSpectrum = new dVec[fNumberOfSpectra];

	for (int i = 0; i < fNumberOfSpectra; i++)
	{
		fSpectrumName[i] = pSpName[i];
		fSpectrum[i].resize(pSp[i].size());
		fSpectrum[i] = pSp[i];
	}

	fNumberOfComponents = numComp;
	fComponentName = new string[fNumberOfComponents];
	
	for (int k = 0; k < fNumberOfComponents; k++)
	{
		fComponentName[k] = pCompName[k];
	}

	fComponents.resize(Comp.size());
	fComponents = Comp;
}

Sample::~Sample()
{
	delete[] fSpectrumName;
	delete[] fComponentName;
}
