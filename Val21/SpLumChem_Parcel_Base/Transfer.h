// Transfer.h: interface for the IRCalibrTransfer class.
//
// ��������:
// ����� IRCalibrTransfer ������������
// ��� �������� ����������. 
//
// ������������ ���������� TRANS4Samples.exe.
// �� b����������� ���������� SpLumPro.exe!!!


#ifndef _IRCALIBRTRANSFER_H_
#define _IRCALIBRTRANSFER_H_

#include "newmat.h"
#include "SpLumChemStructs.h"
#include "QntCalibration.h"
#include "QltCalibration.h"
#include "TransData.h"

#include <vector>

using namespace std;

class IRCalibrTransfer
{
public:
	int Predict(const char **speclist, const char *s_dir,  const char *m_dir, int numSp, double SEV[]);

	double *GetDistS() const { return pdistS; }
	double *GetDistM() const { return pdistM; }
	double *GetDistS_tr() const { return pdistS_tr; }
	double *GetDistM_tr() const { return pdistM_tr; }

	double GetDistS(int numSample, int numSp);
	double GetDistM(int numSample, int numSp);
	double GetDistS_tr(int numSample, int numSp);
	double GetDistM_tr(int numSample, int numSp);


	int GetNumSampleSpectra(int numSample);

	double *GetSampR() const { return psampR; }
	double GetSECbeforeTrans(int nc);
	double GetSECafterTrans(int nc);
	double GetR2afterTrans(int nc);


	void SaveModel(const char *fName);
	const char *GetCompName(int nc);
	int GetNumComp() const { return pQntCalibration->GetNumComponents(); }

	double *GetSEV_M_Before() const { return pSEV_M_Before; }
	double *GetSEV_S_Before() const { return pSEV_S_Before; }

	double *GetSEV_M_After() const { return pSEV_M_After; }
	double *GetSEV_S_After() const { return pSEV_S_After; }

	IRCalibrTransfer();
	~IRCalibrTransfer();

	int Transfer(char* *pstrSampleName,
				const char* strMasterDir,
				const char* strSlaveDir,
				int numberOfSamples,
				const char* strQntName,
				int nTrType,
				vector<vector<bool>*> *pvSampleSpectraStatus);

	int Error() const { return nError; }


protected:
	CTransData theTransData;

	double *pSEV_M_Before;
	double *pSEV_S_Before;

	double *pSEV_M_After;
	double *pSEV_S_After;

	double *pSEC_Before;
	double *pSEC_After;

	double* pR2_After;


	double *pdistS;
	double *pdistS_tr;
	double *pdistM;
	double *pdistM_tr;

	double *psampR;

	int numberOfComponents;
	int nError;
	int numberOfCommonSamples;


	CQntCalibration *pQntCalibration;
	CQltCalibration *pQltCalibration;

	RowVector vSECbeforeTrans;
	RowVector vSECafterTrans;

	int* pSampleSpectraIndexes; // array [numberOfSamples] of first spectrum indexes (zero based)
								// of given sample
	int* pSampleSpectraNumbers; // array [numberOfSamples] of spectra number in
								// given sample

private:
	int CheckSamples(char* *pstrSampleName,
				const char* strMasterDir,
				const char* strSlaveDir,
				int numberOfSamples,
				vector<vector<bool>*> *pvSampleSpectraStatus);
	int LoadModel(const char* strClbName);
	int LoadSamples(char* *pstrSampleName,
				const char* strMasterDir,
				const char* strSlaveDir,
				int numberOfSamples,
				vector<vector<bool>*> *pvSampleSpectraStatus);
	string* pstrComponentName; // name of 

	Matrix mMasterSp, mSlaveSp;

	Matrix mComponents;

	CFreqScale theFreqScale;

	int nTotalSpectra;
};

#endif //_IRCALIBRTRANSFER_H_