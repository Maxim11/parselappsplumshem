// TransData.h: interface for the CTransData class.
//
// ��������:
// ����� CTransData ������������ ��� �������� ������
// � �������� (������!!!) �����������
//

#ifndef _TRANSDATA_H_
#define _TRANSDATA_H_

#include "newmat.h"
#include <stdio.h>

class CTransData
{
public:
	CTransData();
	CTransData(const CTransData *pTransData);

	void Save(FILE* fgh, int clbVersion);
	void Load(FILE* fgh, int clbVersion);
	int CorrectSpectra(Matrix& mSpectra);
	int CorrectSpectra(Matrix& mSpectra, int numSpectra);

	int CorrectComponents(Matrix& mComponents);
	void SetTrType(int nType) { nTransType = nType; }
	void SetSlopeRef(const RowVector& vSlope) { vSlopeRef = vSlope; }
	void SetBiasRef(const RowVector& vBias) { vBiasRef = vBias; }
	void SetSlopeSp(const RowVector& vSlope) { vSlopeSp = vSlope; }
	void SetBiasSp(const RowVector& vBias) { vBiasSp = vBias; }

	int nTransType;

	// nTransType = 1: only ref. data are corrected
	RowVector vSlopeRef;
	RowVector vBiasRef;

	// nTransType = 2: only spectra are corrected
	RowVector vSlopeSp;
	RowVector vBiasSp;
};


#endif // _TRANSDATA_H_
