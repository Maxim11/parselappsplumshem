#include <math.h>

#include "CSamples.h"
#include "PreprocUtils.h"
#include "MatrixUtilities.h"
#include "SpLumChemStructs.h"

CSamples::CSamples(vector<CSampleProxy> *pSamples, const CFreqScale *frScale, const vector<string> *strCompName) // array of samples
{
	Init();

	numberOfSamples = pSamples->size();

	numberOfComponents = (*pSamples)[0].vCompConc.size();

#ifdef _DEBUG_
	char strBuf[256];
	sprintf(strBuf, "numberOfSamples = %d\n", numberOfSamples);
	AppendToDebugReport(strBuf);
	sprintf(strBuf, "numberOfComponents = %d\n", numberOfComponents);
	AppendToDebugReport(strBuf);
	
	int nNames = (*strCompName).size();
	sprintf(strBuf, "number of component names = %d\n", nNames);
	AppendToDebugReport(strBuf);

	for(int i = 0; i < nNames; i++)
	{
		sprintf(strBuf, "name = %s\n", (*strCompName)[i].c_str());
		AppendToDebugReport(strBuf);
	}
#endif

	freqScale = *frScale;

	pSampleSpectraIndexes = new int[numberOfSamples];
	pSampleSpectraNumbers = new int[numberOfSamples];

	numberOfSpectra = 0;

	for (int i = 0; i < numberOfSamples; i++)
	{
		pSampleSpectraIndexes[i] = numberOfSpectra;
		pSampleSpectraNumbers[i] = (*pSamples)[i].mSpectra.size();
		numberOfSpectra += pSampleSpectraNumbers[i];
	}

#ifdef _DEBUG_
	sprintf(strBuf, "numberOfSpectra = %d\n", numberOfSpectra);
	AppendToDebugReport(strBuf);
	sprintf(strBuf, "numberOfFreq = %d\n", freqScale.nNumberOfFreq);
	AppendToDebugReport(strBuf);
#endif

	mSpectra.ReSize(numberOfSpectra, freqScale.nNumberOfFreq);

#ifdef _DEBUG_
	AppendToDebugReport("start loading spectra");
#endif

	for (int i = 0; i < numberOfSamples; i++)
	{
		for (int j = 0; j < pSampleSpectraNumbers[i]; j++)
		{
			int iSp = pSampleSpectraIndexes[i] + j;

			for (int iF = 0; iF < freqScale.nNumberOfFreq; iF++)
			{
				mSpectra(iSp + 1, iF + 1) = (*pSamples)[i].mSpectra[j][iF];
			}
		}
	}

#ifdef _DEBUG_
	AppendToDebugReport("start loading components");
#endif

	if (numberOfComponents > 0)
	{
		// now we fill the matrix of components,
		// fill arrays of component names

		if (strCompName)
		{
			pstrComponentName = new string[numberOfComponents];

			for (int i = 0; i < numberOfComponents; i++)
			{
				pstrComponentName[i] = (*strCompName)[i];
			}
		}

		mComponents.ReSize(numberOfSpectra, numberOfComponents);

		for (int i = 0; i < numberOfSamples; i++)
		{
			for (int j = 0; j < pSampleSpectraNumbers[i]; j++)
			{
				int iSp = pSampleSpectraIndexes[i] + j;

				for (int iC = 0; iC < numberOfComponents; iC++)
				{
					mComponents(iSp + 1, iC + 1) = (*pSamples)[i].vCompConc[iC];
				}
			}
		}
#ifdef _DEBUG_
	AppendToDebugReport("end loading components");
#endif 

	}
}




CSamples::CSamples()
{
	Init();
}


CSamples::~CSamples()
{
	delete[] pSampleSpectraIndexes;
	delete[] pSampleSpectraNumbers;
	delete[] pstrComponentName;
}


int CSamples::PreprocessSpectra(CSpecPreprocData& specPreprocData, Matrix& mX) const
{

	int iStart, iEnd;

	iStart = 0;
	iEnd = specPreprocData.numberOfChannels;

	if (specPreprocData.strPreprocPar.find_first_of('A') != string::npos)
	{
		mX.ReSize(numberOfSamples, iEnd - iStart);
		mX = 0.0;

		for (int i = 1; i <= numberOfSamples; i++)
		{
			int ind = pSampleSpectraIndexes[i - 1];
			int nSp = pSampleSpectraNumbers[i - 1];

			for (int iSp = 1; iSp <= nSp; iSp++)
			{
				mX.Row(i) += mSpectra.SubMatrix(ind + iSp, ind + iSp, iStart + 1, iEnd);
			}
			mX.Row(i) /= nSp;
		}
	}
	else
	{
		mX = mSpectra.Columns(iStart + 1, iEnd);
	}

	//AppendToDebugReport("end of basic preproc.");

	// ���������� ����� ��� PLS � PCR
	if ((specPreprocData.nModelType != MODEL_HSO) && (specPreprocData.numberOfExcludedFreq > 0))
	{
		specPreprocData.numberOfChannels -= specPreprocData.numberOfExcludedFreq;

		ExcludeColumns(mX, specPreprocData.numberOfExcludedFreq, specPreprocData.vUseFreq);
	}

	//AppendToDebugReport("end of freq. excl.");

	if (mX.Ncols() == 0)
	{
		return ER_FREQ_RANGE;
	}

	if (specPreprocData.nSpType == STYPE_ABS)
	{
		ABS(mX);
//		AppendToDebugReport("ABS");
	}

	for (int iP = 0; iP < specPreprocData.strPreprocPar.length(); iP++)
	{
		ColumnVector vMeanXf, vDevXf;

		const int nWidth = 10; // window size for derivation
		const int nPoly = 4; // polynome power for derivation

		if (nError != ER_OK)
		{
			break;
		}

		int nErrPreproc;

		switch(specPreprocData.strPreprocPar[iP])
		{
			case 'm':
			case 'M': // ����������������:
				MeanCentering(mX, specPreprocData.meanSpectrum);

				break;
			case 'd':
			case 'D':
				nErrPreproc = DeviationScaling(mX, specPreprocData.meanStdSpec);

				if (nErrPreproc != 0)
				{
					_snprintf(strErrorMessage, MAX_ERR_MSG, "Error doing deviation scaling");
					return ER_PREPROC_SP;
				}

				break;
			case 'n':
			case 'N':
				nErrPreproc = SNVtransformation(mX, vMeanXf, vDevXf);

				if (nErrPreproc != 0)
				{
					_snprintf(strErrorMessage, MAX_ERR_MSG, "Error doing standard normal variate transformation (SNV) and detrending");
					return ER_PREPROC_SP;
				}

				break;
			case 'c':
			case 'C':
				nErrPreproc = MultCorrection(mX, specPreprocData.meanSpectrumMSC);
				if (nErrPreproc != 0)
				{
					_snprintf(strErrorMessage, MAX_ERR_MSG, "Error doing multiplicative scatter correction");
					return ER_PREPROC_SP;
				}
				break;
			case 'b':
			case 'B':
				nErrPreproc = BaseLineCor(mX);
				if (nErrPreproc != 0)
				{
					_snprintf(strErrorMessage, MAX_ERR_MSG, "Error performing base line correction");
					return nErrPreproc;
				}
				break;
			case '1':
				nErrPreproc = Derivate(mX, freqScale.fStepFreq, nWidth, nPoly, 1);
				if (nErrPreproc != 0)
				{
					_snprintf(strErrorMessage, MAX_ERR_MSG, "Error derivative: too few points in spectra");
					return nErrPreproc;
				}
				break;
			case '2':
				nErrPreproc = Derivate(mX, freqScale.fStepFreq, nWidth, nPoly, 2);
				if (nErrPreproc != 0)
				{
					_snprintf(strErrorMessage, MAX_ERR_MSG, "Error derivative: too few points in spectra");
					return nErrPreproc;
				}
				break;
			case 'h':
			case 'H': // ������������ �� ��������:
				NormMax(mX, specPreprocData.fMax);
				break;
			default:
				break;
		}
	}

	return ER_OK;
}

int CSamples::PreprocessSpectra4Prediction(const CSpecPreprocData &specPreprocData, Matrix &mSpPrep)
{

	int iStart, iEnd;

#ifdef _DEBUG_
	AppendToDebugReport("CSamples::PreprocessSpectra4Prediction Start");
#endif

	iStart = FindIndex(specPreprocData.fMinFreq, freqScale, specPreprocData, true);
	iEnd = FindIndex(specPreprocData.fMaxFreq, freqScale, specPreprocData, false);


	if ((iStart < 0) || (iEnd < 0) || (iStart > iEnd))
	{
		nError = ER_FREQ_RANGE;
		//AppendToDebugReport("Error freq range");
		return nError;
	}


    if (iEnd - iStart + 1 != specPreprocData.numberOfChannels)
	{
		nError = ER_FREQ_RANGE;
		return nError;
	}

#ifdef _DEBUG_
	AppendToDebugReport("mSpectra.Columns Start");
	char strTmp[1024];
	sprintf(strTmp, "iStart = %d iEnd = %d mSpectra.Ncols = %d\n", iStart, iEnd, mSpectra.Ncols());
	AppendToDebugReport(strTmp);
#endif

	mSpPrep = mSpectra.Columns(iStart + 1, iEnd + 1);

#ifdef _DEBUG_
	AppendToDebugReport("mSpectra.Columns End");
#endif

	if (mSpPrep.Ncols() == 0)
	{
		return ER_FREQ_RANGE;
	}

	if (specPreprocData.nSpType == STYPE_ABS)
	{
		ABS(mSpPrep);
		//AppendToDebugReport("ABS");
	}

	for (int iP = 0; iP < specPreprocData.strPreprocPar.length(); iP++)
	{
		const int nWidth = 10; // window size for derivation
		const int nPoly = 4; // polynome power for derivation

		if (nError != ER_OK)
		{
			break;
		}

		int iSp, j;
		ColumnVector vMeanXf, vDevXf;

		switch(specPreprocData.strPreprocPar[iP])
		{
			case 'm':
			case 'M': // ����������������:
				//AppendToDebugReport("Start M");
				//specPreprocData.Print("specPreprocData");

				for (iSp = 1; iSp <= mSpPrep.Nrows(); iSp++)
				{
					mSpPrep.Row(iSp) -= specPreprocData.meanSpectrum;
				}
				//AppendToDebugReport("End M");


			break;
			case 'd':
			case 'D':

				for (j = 1; j <= mSpPrep.Ncols(); j++)
				{
					mSpPrep.Column(j) /= specPreprocData.meanStdSpec(j);
				}

				break;
			case 'n':
			case 'N':

				SNVtransformation(mSpPrep, vMeanXf, vDevXf);
				break;
			case 'c':
			case 'C':
				nError = MultCorrectionForPrediction(mSpPrep, specPreprocData.meanSpectrumMSC);
				break;
			case 'b':
			case 'B':
				BaseLineCor(mSpPrep);
				break;
			case '1':
				//AppendToDebugReport("Start 1");
				Derivate(mSpPrep, freqScale.fStepFreq, nWidth, nPoly, 1);
				//AppendToDebugReport("End 1");

				break;
			case '2':
				Derivate(mSpPrep, freqScale.fStepFreq, nWidth, nPoly, 2);
				break;
			case 'h':
			case 'H': // ������������ �� ��������:
				mSpPrep /= specPreprocData.fMax;
				break;
		}
	}

	return nError;
}

int CSamples::PreprocessSpectra(CSpecPreprocData& specPreprocData, CTransData *pTransData, Matrix& mX) const
{
	int iStart, iEnd;

	iStart = 0;
	iEnd = specPreprocData.numberOfChannels;


	if (specPreprocData.strPreprocPar.find_first_of('A') != string::npos)
	{
		mX.ReSize(numberOfSamples, iEnd - iStart);
		mX = 0.0;

		for (int i = 1; i <= numberOfSamples; i++)
		{
			int ind = pSampleSpectraIndexes[i - 1];
			int nSp = pSampleSpectraNumbers[i - 1];

			for (int iSp = 1; iSp <= nSp; iSp++)
			{
				mX.Row(i) += mSpectra.SubMatrix(ind + iSp, ind + iSp, iStart + 1, iEnd);
			}
			mX.Row(i) /= nSp;
		}
	}
	else
	{
		mX = mSpectra.Columns(iStart + 1, iEnd);
	}

	if (specPreprocData.nSpType == STYPE_ABS)
	{
		ABS(mX);
	}

	if (pTransData->nTransType == 21)
	{
		int nErrTrans = pTransData->CorrectSpectra(mX);

		if (nErrTrans != ER_OK)
		{
			return nErrTrans;
		}
	}

	for (int iP = 0; iP < specPreprocData.strPreprocPar.length(); iP++)
	{
		ColumnVector vMeanXf, vDevXf;

		const int nWidth = 10; // window size for derivation
		const int nPoly = 4; // polynome power for derivation

		if (nError != ER_OK)
		{
			break;
		}

		int nErrPreproc;

		switch(specPreprocData.strPreprocPar[iP])
		{
			case 'm':
			case 'M': // ����������������:
//				PrintMatrix(mX, "mX.dat");

				if (pTransData->nTransType == 22)
				{
					int nErrTrans = pTransData->CorrectSpectra(mX);

					if (nErrTrans != ER_OK)
					{
						return nErrTrans;
					}
				}


				MeanCentering(mX, specPreprocData.meanSpectrum);
				break;
			case 'd':
			case 'D':
				nErrPreproc = DeviationScaling(mX, specPreprocData.meanStdSpec);

				if (nErrPreproc != 0)
				{
					_snprintf(strErrorMessage, MAX_ERR_MSG, "Error doing deviation scaling");
					return ER_PREPROC_SP;
				}

				break;
			case 'n':
			case 'N':
				nErrPreproc = SNVtransformation(mX, vMeanXf, vDevXf);

				if (nErrPreproc != 0)
				{
					_snprintf(strErrorMessage, MAX_ERR_MSG, "Error doing standard normal variate transformation (SNV) and detrending");
					return ER_PREPROC_SP;
				}

				break;
			case 'c':
			case 'C':
				nErrPreproc = MultCorrection(mX, specPreprocData.meanSpectrumMSC);
				if (nErrPreproc != 0)
				{
					_snprintf(strErrorMessage, MAX_ERR_MSG, "Error doing multiplicative scatter correction");
					return ER_PREPROC_SP;
				}
				break;
			case 'b':
			case 'B':
				if (mX.Ncols() <= 3)
				{
					return ER_PREPROC_SP;
				}
				else
				{
					BaseLineCor(mX);
				}				break;
			case '1':
				if (specPreprocData.numberOfChannels < 2 * nWidth + 1)
				{
					_snprintf(strErrorMessage, MAX_ERR_MSG, "Error derivative: too few points in spectra");
					return ER_DERIV1;
				}
				else
				{
					Derivate(mX, freqScale.fStepFreq, nWidth, nPoly, 1);
				}
				break;
			case '2':
				if (specPreprocData.numberOfChannels < 2 * nWidth + 1)
				{
					_snprintf(strErrorMessage, MAX_ERR_MSG, "Error derivative: too few points in spectra");
					return ER_DERIV1;
				}
				else
				{
					Derivate(mX, freqScale.fStepFreq, nWidth, nPoly, 2);
				}
				break;
			case 'h':
			case 'H': // ������������ �� ��������:
				NormMax(mX, specPreprocData.fMax);
				break;
			default:
				break;
		}
	}

	if (pTransData->nTransType == 2)
	{
		int nErrTrans = pTransData->CorrectSpectra(mX);

		if (nErrTrans != ER_OK)
		{
			return nErrTrans;
		}
	}

	return ER_OK;
}

int CSamples::ExcludeSample(const CSpecPreprocData& specPreprocData, Matrix& mX, int iS) const
{
//	exclude rows for sample iS
	if (specPreprocData.strPreprocPar.find_first_of('A') != string::npos) 
	// with 'A'
	{
		Matrix mXtmp = mX;

		if (iS == 0)
		// exclude 1st spec
		{
			mX = mXtmp.Rows(2, mXtmp.Nrows());
		}
		else if (iS == numberOfSamples - 1)
		// exclude last spec
		{
			mX = mXtmp.Rows(1, mXtmp.Nrows() - 1);
		}
		else
		// exclude spec iS
		{
			mX = mXtmp.Rows(1, iS) & mXtmp.Rows(iS + 2, mXtmp.Nrows());
		}
	}
	else 
	// without 'A'
	{
		Matrix mXtmp = mX;

		int indS, indSnext;

		indS = pSampleSpectraIndexes[iS];

		if (iS < numberOfSamples - 1)
		{
			indSnext = pSampleSpectraIndexes[iS + 1];
		}
		else indSnext = numberOfSpectra;

		if (indS != 0)
		{
			if (indSnext < numberOfSpectra)
			{
				mX = mXtmp.Rows(1, indS) & mXtmp.Rows(indSnext + 1, numberOfSpectra);
			}
			else
			{
				mX = mXtmp.Rows(1, indS);
			}
		}
		else
		{
			mX = mXtmp.Rows(indSnext + 1, numberOfSpectra);
		}
	}
	return ER_OK;
}



int CSamples::PreprocessSpectra(CSpecPreprocData& specPreprocData, Matrix& mX, int iS) const
{
	//_sleep(100);

//	int iStart, iEnd;

	const int iStart = 0;
	const int iEnd = freqScale.nNumberOfFreq;

	specPreprocData.numberOfChannels = iEnd - iStart;

//	exclude rows for sample iS
	if (specPreprocData.strPreprocPar.find_first_of('A') != string::npos) 
	// with 'A'
	{
		Matrix mSp;

		// get sample-averaged spectra
		GetMSpectra(mSp, true);

		Matrix mXtmp = mSp.Columns(iStart + 1, iEnd);

		if (iS == -1)
		// exclude nothing
		{
			mX = mXtmp.Rows(1, mSp.Nrows());
		}
		else if (iS == 0)
		// exclude 1st spec
		{
			mX = mXtmp.Rows(2, mSp.Nrows());
		}
		else if (iS == numberOfSamples - 1)
		// exclude last spec
		{
			mX = mXtmp.Rows(1, mSp.Nrows() - 1);
		}
		else
		// exclude spec iS
		{
			mX = mXtmp.Rows(1, iS) & mXtmp.Rows(iS + 2, mSp.Nrows());
		}
	}
	else 
	// without 'A'
	{
		if(iS == -1)
		// no exclusion
		{
			mX = mSpectra.Columns(iStart + 1, iEnd);
		}
		else
		{
			Matrix mXtmp = mSpectra.Columns(iStart + 1, iEnd);
			int indS, indSnext;

			indS = pSampleSpectraIndexes[iS];

			if (iS < numberOfSamples - 1)
			{
				indSnext = pSampleSpectraIndexes[iS + 1];
			}
			else indSnext = numberOfSpectra;

			if (indS == 0)
			{
				mX = mXtmp.Rows(indSnext + 1, numberOfSpectra);
			}
			else 
			{
				if (indSnext < numberOfSpectra)
				{
					mX = mXtmp.Rows(1, indS) & mXtmp.Rows(indSnext + 1, numberOfSpectra);
				}
				else
				{
					mX = mXtmp.Rows(1, indS);
				}
			}
		}
	}

// ���������� ������ ��� PLS � PCR �������
	
	if ((specPreprocData.nModelType != MODEL_HSO) && (specPreprocData.numberOfExcludedFreq > 0))
	{
		ExcludeColumns(mX, specPreprocData.numberOfExcludedFreq, specPreprocData.vUseFreq);
	}

	if (specPreprocData.nSpType == STYPE_ABS)
	{
		ABS(mX);
	}

	int nErrPreproc = 0;

	for (int iP = 0; iP < specPreprocData.strPreprocPar.length(); iP++)
	{
		ColumnVector vMeanXf, vDevXf;

		const int nWidth = 10; // window size for derivation
		const int nPoly = 4; // polynome power for derivation


		if (nError != ER_OK)
		{
			break;
		}

		switch(specPreprocData.strPreprocPar[iP])
		{
			case 'm':
			case 'M': // ����������������:

				MeanCentering(mX, specPreprocData.meanSpectrum);

			break;
			case 'd':
			case 'D':
				nErrPreproc = DeviationScaling(mX, specPreprocData.meanStdSpec);

				if (nErrPreproc != 0)
				{
					_snprintf(strErrorMessage, MAX_ERR_MSG, "Error doing deviation scaling");
					return ER_PREPROC_SP;
				}

				break;
			case 'n':
			case 'N':
				nErrPreproc = SNVtransformation(mX, vMeanXf, vDevXf);

				if (nErrPreproc != 0)
				{
					_snprintf(strErrorMessage, MAX_ERR_MSG, "Error doing standard normal variate transformation (SNV) and detrending");
					return ER_PREPROC_SP;
				}

				break;
			case 'c':
			case 'C':
				nErrPreproc = MultCorrection(mX, specPreprocData.meanSpectrumMSC);
				if (nErrPreproc != 0)
				{
					_snprintf(strErrorMessage, MAX_ERR_MSG, "Error performing multiplicative scatter correction");
					return ER_PREPROC_SP;
				}
				break;
			case 'b':
			case 'B':
				nErrPreproc = BaseLineCor(mX);
				if (nErrPreproc != 0)
				{
					_snprintf(strErrorMessage, MAX_ERR_MSG, "Error performing base line correction");
					return nErrPreproc;
				}
				break;
			case '1':
				nErrPreproc = Derivate(mX, freqScale.fStepFreq, nWidth, nPoly, 1);
				if (nErrPreproc != 0)
				{
					_snprintf(strErrorMessage, MAX_ERR_MSG, "Error derivative: too few points in spectra");
					return nErrPreproc;
				}
				break;
			case '2':
				nErrPreproc = Derivate(mX, freqScale.fStepFreq, nWidth, nPoly, 2);
				if (nErrPreproc != 0)
				{
					_snprintf(strErrorMessage, MAX_ERR_MSG, "Error derivative: too few points in spectra");
					return nErrPreproc;
				}
				break;
			case 'h':
			case 'H': // ������������ �� ��������:
				NormMax(mX, specPreprocData.fMax);
				//NormMax(mX);
				break;
			default:
				break;
		}
	}

	return ER_OK;
}

int CSamples::PreprocessComponents(CSpecPreprocData& specPreprocData, Matrix& mY)
{
	if (specPreprocData.strPreprocPar.find_first_of('A') != string::npos)
	{
		mY.ReSize(numberOfSamples, numberOfComponents);
		mY = 0.0;

		for (int i = 1; i <= numberOfSamples; i++)
		{
			int ind = pSampleSpectraIndexes[i - 1];
			int nSp = pSampleSpectraNumbers[i - 1];

			for (int iSp = 1; iSp <= nSp; iSp++)
			{
				mY.Row(i) += mComponents.Row(ind + iSp);
			}
			mY.Row(i) /= nSp;
		}
	}
	else
	{
		mY = mComponents;
	}
	

	int nErrPreproc = 0;

	// const preprocessing

	for (int iP = 0; iP < specPreprocData.strPreprocParTrans.length(); iP++)
	{
		if (nError != ER_OK)
		{
			break;
		}

		switch(specPreprocData.strPreprocParTrans[iP])
		{
			case 'm':
			case 'M': // ����������������:
				MeanCentering(mY, specPreprocData.meanComponentsTrans);
			break;
/*			case 'd':
			case 'D':
				nErrPreproc = DeviationScaling(mY, thePreprocDataConst.meanStdComp);

				if (nErrPreproc != 0)
				{
					return ER_PREPROC_COMP;
				}

				break;*/
			default:
				break;
		}
	}


	// remain preproc
	for (int iP = 0; iP < specPreprocData.strPreprocPar.length(); iP++)
	{
		if (nError != ER_OK)
		{
			break;
		}

		switch(specPreprocData.strPreprocPar[iP])
		{
			case 'm':
			case 'M': // ����������������:
				MeanCentering(mY, specPreprocData.meanComponents);
			break;
			case 'd':
			case 'D':
				nErrPreproc = DeviationScaling(mY, specPreprocData.meanStdComp);

				if (nErrPreproc != 0)
				{
					return ER_PREPROC_COMP;
				}

				break;
			default:
				break;
		}
	}

	return ER_OK;
}

int CSamples::PreprocessComponents(CSpecPreprocData& specPreprocData, Matrix& mY, int iS)
{
	if (specPreprocData.strPreprocPar.find_first_of('A') != string::npos)
	{
		Matrix mComp;

		GetMComponents(mComp, true);

		if (iS == 0)
		{
			mY = mComp.Rows(2, mComp.Nrows());
		}
		else
		{
			if (iS == numberOfSamples - 1)
			{
				mY = mComp.Rows(1, mComp.Nrows() - 1);
			}
			else
			{
				mY = mComp.Rows(1, iS) & mComp.Rows(iS + 2, mComp.Nrows());
			}
		}
	}
	else
	{

		int indS, indSnext;

		indS = pSampleSpectraIndexes[iS];

		if (iS < numberOfSamples - 1)
		{
			indSnext = pSampleSpectraIndexes[iS + 1];
		}
		else indSnext = numberOfSpectra;

		if (indS != 0)
		{
			if (indSnext < numberOfSpectra)
			{
				mY = mComponents.Rows(1, indS) & mComponents.Rows(indSnext + 1, numberOfSpectra);
			}
			else
			{
				mY = mComponents.Rows(1, indS);
			}
		}
		else
		{
			mY = mComponents.Rows(indSnext + 1, numberOfSpectra);
		}
	}

	int nErrPreproc = 0;

	// const preprocessing

	for (int iP = 0; iP < specPreprocData.strPreprocParTrans.length(); iP++)
	{
		if (nError != ER_OK)
		{
			break;
		}

		switch(specPreprocData.strPreprocParTrans[iP])
		{
			case 'm':
			case 'M': // ����������������:
				MeanCentering(mY, specPreprocData.meanComponentsTrans);
			break;
/*			case 'd':
			case 'D':
				nErrPreproc = DeviationScaling(mY, thePreprocDataConst.meanStdComp);

				if (nErrPreproc != 0)
				{
					return ER_PREPROC_COMP;
				}

				break;*/
			default:
				break;
		}
	}

	// remain preproc

	for (int iP = 0; iP < specPreprocData.strPreprocPar.length(); iP++)
	{
		if (nError != ER_OK)
		{
			break;
		}

		switch(specPreprocData.strPreprocPar[iP])
		{
			case 'm':
			case 'M': // ����������������:
				MeanCentering(mY, specPreprocData.meanComponents);
			break;
			case 'd':
			case 'D':
				nErrPreproc = DeviationScaling(mY, specPreprocData.meanStdComp);

				if (nErrPreproc != 0)
				{
					return ER_PREPROC_COMP;
				}

				break;
			default:
				break;
		}
	}

	return ER_OK;
}


int CSamples::FindComponentIndex(const string& compName)
{
	int ind = -1;
	nError = ER_NO_COMP;

	for (int i = 0; i < numberOfComponents; i++)
	{

		if (pstrComponentName[i] == compName)
		{
			nError = ER_OK;
			ind = i;
			break;
		}
	}
	return ind;
}

/*void CSamples::Save(FILE* fgh, int clbVersion)
{
		fwrite(&numberOfSamples, sizeof(int), 1, fgh);

		for (int iSample = 0; iSample < numberOfSamples; iSample++)
		{
			SaveString(pstrSampleName[iSample], fgh);
		}

		fwrite(pSampleSpectraNumbers, sizeof(int), numberOfSamples, fgh);

		fwrite(pSampleSpectraIndexes, sizeof(int), numberOfSamples, fgh);

		// write spectra

		freqScale.Save(fgh);

		fwrite(&numberOfSpectra, sizeof(int), 1, fgh);

		for (int iSp = 0; iSp < numberOfSpectra; iSp++)
		{
			SaveString(pstrSpectrumName[iSp], fgh);
		}

		SaveMatrix(mSpectra, fgh);

		// write components

		fwrite(&numberOfComponents, sizeof(int), 1, fgh);

		if (numberOfComponents > 0)
		{
			for (int iC = 0; iC < numberOfComponents; iC++)
			{
				SaveString(pstrComponentName[iC], fgh);
			}

			SaveMatrix(mComponents, fgh);
		}

	if (clbVersion > 18)
	{
		fwrite(&nFlagTrans, sizeof(int), 1, fgh);

		if (nFlagTrans == 2)
		{
			// transfered and added calibration (old)
			fwrite(&numberOfAddedSamples, sizeof(int), 1, fgh);
			fwrite(&numberOfAddedSpectra, sizeof(int), 1, fgh);
			thePreprocDataConst.Save(fgh, clbVersion);
		}

		if ((nFlagTrans == 3) || (nFlagTrans == 4))
		{
			// new transfer
			// ������� ��� ������ �������� ����� ������, ���
			// nFlagTrans = 3, �� ����� ���������������
			// ���� �������� � CTransData � ��������� �� 4
			thePreprocDataConst.Save(fgh, clbVersion);
		}
	}
}*/


/*void CSamples::Load(FILE* fgh, int clbVersion)
{
	fread(&numberOfSamples, sizeof(int), 1, fgh);

	pstrSampleName = new string[numberOfSamples];

	for (int iSample = 0; iSample < numberOfSamples; iSample++)
	{
		LoadStringMy(pstrSampleName[iSample], fgh);
	}

	
	pSampleSpectraNumbers = new int[numberOfSamples];
	pSampleSpectraIndexes = new int[numberOfSamples];

	fread(pSampleSpectraNumbers, sizeof(int), numberOfSamples, fgh);

	fread(pSampleSpectraIndexes, sizeof(int), numberOfSamples, fgh);

	// read spectra

	freqScale.Load(fgh);

	fread(&numberOfSpectra, sizeof(int), 1, fgh);

	pstrSpectrumName = new string[numberOfSpectra];

	for (int iSp = 0; iSp < numberOfSpectra; iSp++)
	{
		LoadStringMy(pstrSpectrumName[iSp], fgh);
	}

	LoadMatrix(mSpectra, fgh);

	// read components

	fread(&numberOfComponents, sizeof(int), 1, fgh);

	if (numberOfComponents > 0)
	{
		pstrComponentName = new string[numberOfComponents];

		for (int iC = 0; iC < numberOfComponents; iC++)
		{
			LoadStringMy(pstrComponentName[iC], fgh);
		}

		LoadMatrix(mComponents, fgh);
	}

	if (clbVersion > 18)
	{
		fread(&nFlagTrans, sizeof(int), 1, fgh);

		if (nFlagTrans == 2)
		{
			// transfered and added calibration
			fread(&numberOfAddedSamples, sizeof(int), 1, fgh);
			fread(&numberOfAddedSpectra, sizeof(int), 1, fgh);
			thePreprocDataConst.Load(fgh, clbVersion);
		}

		if ((nFlagTrans == 3) || (nFlagTrans == 4))
		{
			// new transfer
			thePreprocDataConst.Load(fgh, clbVersion);
		}
	}
}*/

/*const char* CSamples::GetSampleName(int nSample)
{
	if ((nSample < 0) || (nSample >= numberOfSamples))
	{
		nError = ER_OUT_OF_RANGE;
		return 0;
	}

	return (pstrSampleName[nSample]).c_str();
}*/

const char* CSamples::GetComponentName(int nComponent)
{
	if ((nComponent < 0) || (nComponent >= numberOfComponents))
	{
		nError = ER_OUT_OF_RANGE;
		return 0;
	}

	return (pstrComponentName[nComponent]).c_str();
}


/*const char* CSamples::GetSampleSpecName(int nSample, int nSpec)
{
	if ((nSample < 0) || (nSample >= numberOfSamples))
	{
		nError = ER_OUT_OF_RANGE;
		return 0;
	}

	if ((nSpec < 0) || (nSpec >= pSampleSpectraNumbers[nSample]))
	{
		nError = ER_OUT_OF_RANGE;
		return 0;
	}

	return (pstrSpectrumName[pSampleSpectraIndexes[nSample] + nSpec]).c_str();

}*/

void CSamples::GetSampleSpectra(Matrix& mSampleSpectra, int iS)
{
	int indS, numSSp;

	indS = pSampleSpectraIndexes[iS];
	numSSp = pSampleSpectraNumbers[iS];

	mSampleSpectra = mSpectra.Rows(indS + 1, indS + numSSp);
}

void CSamples::GetStd2Components(RowVector& vStd2) const
{
	CalculateStd2(mComponents, vStd2);
}

void CSamples::GetMComponents(Matrix& mComp, bool bAverage) const
{
	if (bAverage)
	{
		mComp.ReSize(numberOfSamples, numberOfComponents);
		mComp = 0.0;

		for (int i = 1; i <= numberOfSamples; i++)
		{
			int ind = pSampleSpectraIndexes[i - 1];
			int nSp = pSampleSpectraNumbers[i - 1];

			for (int iSp = 1; iSp <= nSp; iSp++)
			{
				mComp.Row(i) += mComponents.Row(ind + iSp);
			}
			mComp.Row(i) /= nSp;
		}
	}
	else
	{
		mComp = mComponents;
	}
}

void CSamples::GetMSpectra(Matrix& mSp, bool bAverage) const
{
	if (bAverage)
	{
		mSp.ReSize(numberOfSamples, mSpectra.Ncols());
		mSp = 0.0;

		for (int i = 1; i <= numberOfSamples; i++)
		{
			int ind = pSampleSpectraIndexes[i - 1];
			int nSp = pSampleSpectraNumbers[i - 1];

			for (int iSp = 1; iSp <= nSp; iSp++)
			{
				mSp.Row(i) += mSpectra.Row(ind + iSp);
			}
			mSp.Row(i) /= nSp;
		}
	}
	else
	{
		mSp = mSpectra;
	}
}

int CSamples::GetSpectrum(int nSample, int nSpec, RowVector& vSpectrum) const
{
	if (nSample >= numberOfSamples)
	{
		return ER_OUT_OF_RANGE;
	}

	if (nSpec >= pSampleSpectraNumbers[nSample])
	{
		return ER_OUT_OF_RANGE;
	}

	int iS;

	iS = pSampleSpectraIndexes[nSample] + nSpec;

	vSpectrum = mSpectra.Row(iS + 1);

	return ER_OK;
}

int CSamples::GetMeanSpectrum(int nSample, RowVector& vSpectrum) const
{
	if (nSample >= numberOfSamples)
	{
		return ER_OUT_OF_RANGE;
	}

	int numSpectra = pSampleSpectraNumbers[nSample];

	int iSfirst;

	iSfirst = pSampleSpectraIndexes[nSample];

	vSpectrum = mSpectra.Row(iSfirst + 1);

	for (int iS = iSfirst + 1; iS < iSfirst + numSpectra; iS++)
	{
		vSpectrum += mSpectra.Row(iS + 1);
	}

	vSpectrum /= numSpectra;

	return ER_OK;
}


/*void CSamples::AddSamples(CSamples &theAddedSamples)
{
	if (numberOfComponents != theAddedSamples.GetNumComponents())
	{
		nError = ER_ADD;

		return;
	}


	if ((nFlagTrans == 3) || (nFlagTrans == 4))
	{
		// new transfer

		nError = theAddedSamples.PreprocessSpectra4Transfer(thePreprocDataConst);
	}

	if (nError != ER_OK)
	{
		nError = ER_ADD;

		return;
	}


	if (freqScale.fStartFreq < theAddedSamples.GetStartFreq())
	{
		nError = ER_ADD;

		return;
	}

	if (freqScale.fStepFreq != theAddedSamples.GetStepFreq())
	{
		nError = ER_FREQ_RANGE;

		return;
	}

	// first add the Components and Spectra Matrix

	int iStart, iEnd;

	double tmp;

	tmp = (freqScale.fStartFreq - theAddedSamples.GetStartFreq()) / freqScale.fStepFreq;
	iStart = (int)floor(tmp);
	if (tmp - iStart > 0.5) iStart++;

	tmp = (freqScale.GetEndFreq() - theAddedSamples.GetStartFreq()) / freqScale.fStepFreq;
	iEnd = (int)floor(tmp);
	if (tmp - iEnd > 0.5) iEnd++;

	Matrix mAddedSpectra, mAddedComponents;

	theAddedSamples.GetMSpectra(mAddedSpectra);
	theAddedSamples.GetMComponents(mAddedComponents);

	if ((iEnd > mAddedSpectra.Ncols()) || (iEnd - iStart + 1 != mSpectra.Ncols()))
	{
		nError = ER_FREQ_RANGE;

		return;
	}
	mSpectra = mSpectra & mAddedSpectra.Columns(iStart + 1, iEnd + 1);

	mComponents = mComponents & mAddedComponents;

	// now add the remain info

	int numAddedSamples;

	numAddedSamples = theAddedSamples.GetNumSamples();


	int* pSampleSpectraIndexesCopy;
	int* pSampleSpectraNumbersCopy;
	string* strSampleNameCopy;

	pSampleSpectraIndexesCopy = new int[numberOfSamples];
	pSampleSpectraNumbersCopy = new int[numberOfSamples];
	strSampleNameCopy = new string[numberOfSamples];

	for (int i = 0; i < numberOfSamples; i++)
	{
		pSampleSpectraIndexesCopy[i] = pSampleSpectraIndexes[i];
		pSampleSpectraNumbersCopy[i] = pSampleSpectraNumbers[i];
		strSampleNameCopy[i] = pstrSampleName[i];
	}

	delete[] pSampleSpectraIndexes;
	delete[] pSampleSpectraNumbers;
	delete[] pstrSampleName;

	string* strSpectrumNameCopy;

	strSpectrumNameCopy = new string[numberOfSpectra];

	for (i = 0; i < numberOfSpectra; i++)
	{
		strSpectrumNameCopy[i] = pstrSpectrumName[i];
	}

	delete[] pstrSpectrumName;

	pSampleSpectraIndexes = new int[numberOfSamples	+ numAddedSamples];
	pSampleSpectraNumbers = new int[numberOfSamples + numAddedSamples];
	pstrSampleName = new string[numberOfSamples +  numAddedSamples];

	// copy the old samples

	for (i = 0; i < numberOfSamples; i++)
	{
		pSampleSpectraIndexes[i] = pSampleSpectraIndexesCopy[i];
		pSampleSpectraNumbers[i] = pSampleSpectraNumbersCopy[i];
		pstrSampleName[i] = strSampleNameCopy[i];
	}

	delete[] pSampleSpectraIndexesCopy;
	delete[] pSampleSpectraNumbersCopy;
	delete[] strSampleNameCopy;

	int numAddedSpectra;

	numAddedSpectra = theAddedSamples.GetNumSpectra();


	pstrSpectrumName = new string[numberOfSpectra +  numAddedSpectra];

	// copy the old spectra names

	for (i = 0; i < numberOfSpectra; i++)
	{
		pstrSpectrumName[i] = strSpectrumNameCopy[i];
	}

	delete[] strSpectrumNameCopy;

	// add the new samples (and the new spectra names)

	int indStart;

	indStart = pSampleSpectraIndexes[numberOfSamples - 1] + pSampleSpectraNumbers[numberOfSamples - 1];

	for (i = 0; i < numAddedSamples; i++)
	{
		pSampleSpectraIndexes[numberOfSamples + i] = indStart + theAddedSamples.GetSampleSpectraIndexes(i);
		pSampleSpectraNumbers[numberOfSamples + i] = theAddedSamples.GetSampleSpectraNumbers(i);
		pstrSampleName[numberOfSamples + i] = string(theAddedSamples.GetSampleName(i));

		for (int k = 0; k < theAddedSamples.GetSampleSpectraNumbers(i); k++)
		{
			pstrSpectrumName[numberOfSpectra] = string(theAddedSamples.GetSampleSpecName(i, k));
			numberOfSpectra++;
		}
	}

	numberOfSamples += numAddedSamples;
}*/


void CSamples::CopySamples(CSamples& theSamples)
{
	freqScale.fStartFreq = theSamples.GetStartFreq();
	freqScale.fStepFreq = theSamples.GetStepFreq();
	freqScale.nNumberOfFreq = theSamples.GetNumFreq();

	theSamples.GetMComponents(mComponents);
	theSamples.GetMSpectra(mSpectra);
	nError = theSamples.GetError();
	numberOfComponents = theSamples.GetNumComponents();
	numberOfSamples = theSamples.GetNumSamples();
	numberOfSpectra = theSamples.GetNumSpectra();

	pSampleSpectraIndexes = new int[numberOfSamples];
	pSampleSpectraNumbers = new int[numberOfSamples];
	pstrComponentName = new string[numberOfComponents];
//	pstrSampleName = new string[numberOfSamples];
//	pstrSpectrumName = new string[numberOfSpectra];

	for (int i = 0; i < numberOfSamples; i++)
	{
		pSampleSpectraIndexes[i] = theSamples.GetSampleSpectraIndexes(i);
		pSampleSpectraNumbers[i] = theSamples.GetSampleSpectraNumbers(i);
//		pstrSampleName[i] = string(theSamples.GetSampleName(i));
//		for (int k = 0; k < pSampleSpectraNumbers[i]; k++)
//		{
//			pstrSpectrumName[pSampleSpectraIndexes[i] + k] = string(theSamples.GetSampleSpecName(i, k));
//		}
	}

	for (int i = 0; i < numberOfComponents; i++)
	{
		pstrComponentName[i] = string(theSamples.GetComponentName(i));
	}

	// copy thePreprocDataConst added 17.04.2005
	nFlagTrans = theSamples.GetTransFlag();
	bFreqExcluded = theSamples.IsFreqExcluded();
	thePreprocDataConst.Copy(theSamples.thePreprocDataConst);
}

void CSamples::GetRefConc(int nSample, // sample order number
					double* compConc) // array of ref. data of this sample
{
	int iS = pSampleSpectraIndexes[nSample];

	for (int i = 0; i < numberOfComponents; i++)
	{
		compConc[i] = mComponents(iS + 1, i + 1);
	}
}

void CSamples::GetRefConc(int nSample, // sample order number
					VDbl *pvCompConc) // array of ref. data of this sample
{
	int iS = pSampleSpectraIndexes[nSample];

	for (int i = 0; i < numberOfComponents; i++)
	{
		(*pvCompConc)[i] = mComponents(iS + 1, i + 1);
	}
}


void CSamples::Init()
{
	pSampleSpectraIndexes = 0; // array [numberOfSamples] of first spctrum indexes of
								// given sample
	pSampleSpectraNumbers = 0; // array [numberOfSamples] of spetra number in
								// given sample

//	pstrSampleName = 0; // samples names
//	pstrSpectrumName = 0; // spectra names
	pstrComponentName = 0; // component name

	numberOfSpectra = 0;
	numberOfSamples = 0;
	numberOfComponents = 0;

	numberOfAddedSamples = 0;
	numberOfAddedSpectra = 0;

	nFlagTrans = 0; // not tranfered, not added

	bFreqExcluded = false; // freq. not excluded

	nError = ER_OK;
}

/*int CSamples::PreprocessSpectra4Transfer(void *p, bool bExtSpectra)
{
	CPbt *pPbt;

	pPbt = (CPbt*)p;

	thePreprocDataConst.bCompCorr = false;
	thePreprocDataConst.fMinFreq = pPbt->GetStartFreq();
	thePreprocDataConst.fMaxFreq = pPbt->GetStartFreq() + (pPbt->GetStepFreq() * (pPbt->GetNumFreq() - 1));
	thePreprocDataConst.nSpType = pPbt->GetSpType();
	thePreprocDataConst.strPreprocPar = string(pPbt->GetPreprocStr());

	if (pPbt->vMeanSpectrum.Ncols())
	{
		thePreprocDataConst.meanSpectrum = pPbt->vMeanSpectrum;
	}

	if (pPbt->vMeanSpectrumMSC.Ncols())
	{
		thePreprocDataConst.meanSpectrumMSC = pPbt->vMeanSpectrumMSC;
	}

	if (pPbt->vMeanStdSpec.Ncols())
	{
		thePreprocDataConst.meanStdSpec = pPbt->vMeanStdSpec;
	}


	Matrix mSpPrep;

	nError = PrepSpForPrediction(freqScale, thePreprocDataConst, mSpectra, mSpPrep);

	if (nError != ER_OK)
	{
		return nError;
	}

	if (!bExtSpectra)
	{
		nError = pPbt->CorrectSpectra(mSpPrep);
	}

	if (nError != ER_OK)
	{
		return nError;
	}

	freqScale.fStartFreq = pPbt->GetStartFreq();
	freqScale.fStepFreq = pPbt->GetStepFreq();
	freqScale.nNumberOfFreq = pPbt->GetNumFreq();

	mSpectra.Release();
	mSpectra = mSpPrep;

	Matrix mCompPrep;

	PreprocessComponents(thePreprocDataConst, mCompPrep);

	// added 17.04.2005
	nFlagTrans = 4; // New transfer

	return ER_OK;
}*/


/*int CSamples::PreprocessSpectra4Transfer(const CSpecPreprocData &thePreprocData)
{
	SetPreprocDataConst(thePreprocData);

	Matrix mSpPrep;

	nError = PrepSpForPrediction(freqScale, thePreprocDataConst, mSpectra, mSpPrep);

	if (nError != ER_OK)
	{
		return nError;
	}

	if (nError != ER_OK)
	{
		return nError;
	}

	freqScale.fStartFreq = thePreprocData.fMinFreq;
	freqScale.nNumberOfFreq = mSpPrep.Ncols();

	mSpectra.Release();
	mSpectra = mSpPrep;

	// don't forget to preprocess component!!!
	// if 'M' or 'D' preproc. is used
	Matrix mCompPrep;

	PreprocessComponents(thePreprocDataConst, mCompPrep);

	// added 17.04.2005
	nFlagTrans = 4; // New transfer

	return ER_OK;
}*/

void CSamples::SetPreprocDataConst(const CSpecPreprocData &theData)
{
	nFlagTrans = 4;

	thePreprocDataConst.fMaxFreq = theData.fMaxFreq;
	thePreprocDataConst.fMinFreq = theData.fMinFreq;

	if ((theData.meanComponents).Ncols())
	{
		thePreprocDataConst.meanComponents = theData.meanComponents;
	}

	if ((theData.meanSpectrum).Ncols())
	{
		thePreprocDataConst.meanSpectrum = theData.meanSpectrum;
	}

	if ((theData.meanSpectrumMSC).Ncols())
	{
		thePreprocDataConst.meanSpectrumMSC = theData.meanSpectrumMSC;
	}

	if ((theData.meanStdComp).Ncols())
	{
		thePreprocDataConst.meanStdComp = theData.meanStdComp;
	}

	if ((theData.meanStdSpec).Ncols())
	{
		thePreprocDataConst.meanStdSpec = theData.meanStdSpec;
	}


	thePreprocDataConst.nSpType = theData.nSpType;

	thePreprocDataConst.numberOfChannels = theData.numberOfChannels;
	thePreprocDataConst.strPreprocPar = theData.strPreprocPar;

//	if ((theData.vComponentCorrectionsAdd).Ncols())
//	{
//		thePreprocDataConst.vComponentCorrectionsAdd = theData.vComponentCorrectionsAdd;
//	}

//	if ((theData.vComponentCorrectionsMul).Ncols())
//	{
//		thePreprocDataConst.vComponentCorrectionsMul = theData.vComponentCorrectionsMul;
//	}
}

bool CSamples::IsSampleAdded(int iS)
{
	// iS - zero based index of the sample

	bool bRes = false;

	if (iS >= (numberOfSamples - numberOfAddedSamples)) bRes = true;

	return bRes;
}

void CSamples::GetPreprocDataConst(CSpecPreprocData& m_PreprocDataConst) const
{
	m_PreprocDataConst.Copy(thePreprocDataConst);
}

int CSamples::ExcludeFrequencies(const CSpecPreprocData & thePreprocData)
{
	if ((bFreqExcluded == false) && (nError == ER_OK) && (thePreprocData.numberOfExcludedFreq > 0) && (thePreprocData.nModelType != MODEL_HSO))
	{
		ExcludeColumns(mSpectra, thePreprocData.numberOfExcludedFreq, thePreprocData.vUseFreq);
		bFreqExcluded = true;
	}

	return nError;
}


