
#include "spectra.h"


int ReadSpec(const char *fname, double *pFreq_first, double *pFreq_step, int *pNf, double **ppData)
{
	CIrSpectrum spc;
	if(!spc.Read(fname)) return 0;

	int kMin = spc.GetIndex(spc.fmin);
	int kMax = spc.GetIndex(spc.fmax)+1;
	if(kMin < 0) return 0;
	if(kMax > spc.points) return 0;
	int np = kMax-kMin+1;

	*pNf = np;		
	*pFreq_first = spc.GetFreq(kMin);		
	*pFreq_step = spc.res;

	double *data = new double[np];
	for(int k=0; k<np; k++)
	{
		data[k] = spc.s[k+kMin];
	}
	*ppData = data;

	return 1;
}
