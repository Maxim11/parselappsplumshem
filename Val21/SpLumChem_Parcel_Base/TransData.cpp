#include "TransData.h"
#include "MatrixUtilities.h"
#include "SpLumChemStructs.h"

CTransData::CTransData()
{
	nTransType = 0; // non transfered calibration
}


CTransData::CTransData(const CTransData *pTransData)
{
	nTransType = pTransData->nTransType;

	switch(nTransType)
	{
	case 0:
		break;
	case 1:
		vSlopeRef = pTransData->vSlopeRef;
		vBiasRef = pTransData->vBiasRef;
		break;
	case 2:
		vSlopeSp = pTransData->vSlopeSp;
		vBiasSp = pTransData->vBiasSp;
		break;
	case 3:
		vSlopeRef = pTransData->vSlopeRef;
		vBiasRef = pTransData->vBiasRef;

		vSlopeSp = pTransData->vSlopeSp;
		vBiasSp = pTransData->vBiasSp;
		break;
	case 4:
		vSlopeSp = pTransData->vSlopeSp;
		vBiasSp = pTransData->vBiasSp;
		break;
	}
}

void CTransData::Save(FILE* fgh, int clbVersion)
{
	if (clbVersion <= 14)
	{
		nTransType = 0;
		return;
	}

	fwrite(&nTransType, sizeof(int), 1, fgh);

	switch(nTransType)
	{
	case 0:
		break;
	case 1:
		SaveRowVector(vSlopeRef, fgh);
		SaveRowVector(vBiasRef, fgh);
		break;
	case 2:
		SaveRowVector(vSlopeSp, fgh);
		SaveRowVector(vBiasSp, fgh);
		break;
	case 3:
		SaveRowVector(vSlopeRef, fgh);
		SaveRowVector(vBiasRef, fgh);

		SaveRowVector(vSlopeSp, fgh);
		SaveRowVector(vBiasSp, fgh);
		break;
	case 4:
		SaveRowVector(vSlopeSp, fgh);
		SaveRowVector(vBiasSp, fgh);
		break;
	}
}


void CTransData::Load(FILE* fgh, int clbVersion)
{
//	FILE *deb = fopen("debug.txt", "w");
//	fprintf(deb, "clbVersion = %d\n", clbVersion);

//	fclose(fgh);

	if (clbVersion <= 14)
	{
		nTransType = 0;

		return;
	}

	fread(&nTransType, sizeof(int), 1, fgh);

//	fprintf(deb, "TransType = %d\n", nTransType);

//	fclose(deb);


	switch(nTransType)
	{
	case 0:
		break;
	case 1:
		LoadRowVector(vSlopeRef, fgh);
		LoadRowVector(vBiasRef, fgh);
		break;
	case 2:
		LoadRowVector(vSlopeSp, fgh);
		LoadRowVector(vBiasSp, fgh);
		break;
	case 3:
		LoadRowVector(vSlopeRef, fgh);
		LoadRowVector(vBiasRef, fgh);

		LoadRowVector(vSlopeSp, fgh);
		LoadRowVector(vBiasSp, fgh);
		break;
	case 4:
		LoadRowVector(vSlopeSp, fgh);
		LoadRowVector(vBiasSp, fgh);
		break;
	}
	// read components
}

int CTransData::CorrectSpectra(Matrix& mSpectra)
{
/*	FILE *fgh = fopen("debug.txt", "a");
	fprintf(fgh, "TransType = %d\n");
	PrintMatrix(vBiasSp, fgh);
	PrintMatrix(vSlopeSp, fgh);
	fclose(fgh);*/

	switch(nTransType)
	{
	case 2:
	case 3:
//	case 4:
		int numFreq = vSlopeSp.Ncols();

		if (numFreq != mSpectra.Ncols())
		{
			return ER_TRANS;
		}
		
		for (int iC = 1; iC <= numFreq; iC++)
		{
			mSpectra.Column(iC) *= vSlopeSp(iC);
			mSpectra.Column(iC) += vBiasSp(iC);
		}
		break;
	}

	return ER_OK;
}

int CTransData::CorrectSpectra(Matrix& mSpectra, int numSpectra)
{
/*	FILE *fgh = fopen("debug.txt", "a");
	fprintf(fgh, "TransType = %d\n");
	PrintMatrix(vBiasSp, fgh);
	PrintMatrix(vSlopeSp, fgh);
	fclose(fgh);*/

	double fSlope, fBias;

	switch(nTransType)
	{
	case 2:
	case 3:
//	case 4:
		int numFreq = vSlopeSp.Ncols();

		if (numFreq != mSpectra.Ncols())
		{
			return ER_TRANS;
		}


		for (int iC = 1; iC <= numFreq; iC++)
		{
			fBias = vBiasSp(iC);
			fSlope = vSlopeSp(iC);

			for (int iS = 1; iS <= numSpectra; iS++)
			{
				mSpectra(iS, iC) *= fSlope;
				mSpectra(iS, iC) += fBias;
			}
		}
		break;
	}

	return ER_OK;
}

int CTransData::CorrectComponents(Matrix& mComponents)
{
	switch(nTransType)
	{
	case 0:
		break;
	case 2:
		break;
	case 1:
	case 3:
		int numFreq = vSlopeRef.Ncols();

		if (numFreq != mComponents.Ncols())
		{
			return ER_TRANS;
		}
		
		for (int iC = 1; iC <= numFreq; iC++)
		{
			mComponents.Column(iC) *= vSlopeRef(iC);
			mComponents.Column(iC) += vBiasRef(iC);
		}
		break;
	}

	return ER_OK;
}