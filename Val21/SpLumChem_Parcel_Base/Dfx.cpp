//#include "stdafx.h"

#include "dfx.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <ctype.h>


int DfxFileExist(const char *fname)
{
	if(!fname) return 0;
	FILE *f = fopen(fname, "r");
	if(!f) return 0;
	fclose(f);
	return 1;
}

int DfxCountProfileSections(const char *fname, const char* substr)
{
	FILE *f = fopen(fname, "rt");
	if(!f) return 0;

	int nsec=0;
	static char str[1024];

	// searching for sections
	int nLen = strlen(substr);

	while(fgets(str,1020,f))
	{
		char *p=str;
		while(isspace(*p)) p++;
		if(*p == '[') 
		{
			char *p1 = strchr(p,']');
			if((p1 - p - 1) == nLen)
			{
				nsec++;
				if(substr)
				{
					if(strncmp(p+1,substr,strlen(substr))) nsec--;
				}
			}
		}
	}
	fclose(f);
	return nsec;
}

const char* DfxQueryProfileSection(const char *fname, int idx, const char* substr)
{
	static char section[1024];

	FILE *f = fopen(fname, "rt");
	if(!f) return 0;

	int nsec=0;
	int iok = 0;
	static char str[1024];

	// searching for sections
	while(!iok && fgets(str,1020,f))
	{
		char *p=str;
		while(isspace(*p)) p++;
		if(*p == '[') 
		{
			char *p1 = strchr(p,']');
			if(p1)
			{
				*p1 = 0;

				// checking for substr if exists
				if(substr)
				{
					if(strncmp(p+1,substr,strlen(substr))) p1=0;
				}
				
				// if OK checking for idx
				if(p1)
				{
					if(nsec == idx)
					{
						strcpy(section,p+1);
						iok = 1;
					}
					else
						nsec++;
				}
			}
		}
	}
	fclose(f);
	return iok ? section : 0;
}

int DfxQueryProfileSection(const char *fname, const char *section);


int DfxCheckProfileSection(const char *chkstr, const char *section, int soft=0)
{
	char str[1024];
	strcpy(str, chkstr);
	char *p=str;
	while(isspace(*p)) p++;
	if(*p == '[') 
	{
		p = strtok(++p,"]");
		int ok = soft ? !strncmp(p,section,strlen(section)) : !strcmp(p, section);
		if(ok)
		{
			return 1;
		}
		else
			return 0;
	}
	return 0;
}

int DfxCheckProfileKey(const char *chkstr, const char *key)
{
	char str[1024];
	strcpy(str, chkstr);

	char *p=str;
	while(isspace(*p)) p++;
	if(*p) 
	{
		if(*p == '[') return -1;
		p = strtok(p,"=");
		if(!strcmp(p, key))
		{
			return 1;
		}
		
	}
	return 0;
}


char* DfxGetProfileItem(const char *fname, const char *section, const char *key)
{
	if(!strlen(fname)) return 0;
	FILE *f = fopen(fname, "rt");
	if(!f) return 0;

	int secfound=0;
	int keyfound=0;
	static char str[1024];

	// searching for section
	while(fgets(str,1020,f))
	{
		secfound = DfxCheckProfileSection(str, section);
		if(secfound) break;
	}

	if(secfound)
	{
	// searching for key
		while(fgets(str,1020,f))
		{
			keyfound = DfxCheckProfileKey(str, key);
			if(keyfound) break;
		}
	}
	fclose(f);

	if(keyfound <= 0) return 0;

	char *p1 = strtok(strchr(str,'=')+1, "\n");
	while(isspace(*p1)) p1++;

	char *p = p1 + strlen(p1) - 1;
	while(isspace(*p))
	{
		*p-- = 0;
	}
	return p1;
}

int DfxQueryProfileItem(const char *fname, const char *section, const char *key)
{
	return (DfxGetProfileItem(fname, section, key) != 0);
}

char* DfxGetProfileString(const char *fname, const char *section, const char *key, char* sval)
{
	char *p = DfxGetProfileItem(fname, section, key);
	if(!p) 
	{
		return sval;
	}
	else
	{
		char *p1 = p;
		while(*p1 == '<') p1++;

		char *p2 = p1 + strlen(p1) - 1;
		while(*p2 == '>')
		{
			*p2-- = 0;
		}
		return p1;
	}
}

const char* DfxGetProfileText(const char *fname, const char *section, const char *key, char* sval, int insert_cr)
{
	static char result[1024];
	result[0] = 0;

	char keyn[256];
	int i=0;
	while(1)
	{
		sprintf(keyn, "%s_#_%d", key, i);
		char *p = DfxGetProfileItem(fname, section, keyn);
		if(!p) 
		{
			break;
		}
		else
		{
			char *p1 = p;
			while(*p1 == '<') p1++;

			char *p2 = p1 + strlen(p1) - 1;
			while(*p2 == '>')
			{
				*p2-- = 0;
			}
			strcat(result,p1);
			if(insert_cr) strcat(result,"\r");
			strcat(result,"\n");
		}
		i++;
	}
	if(!i) 
		return sval;
	else
		return result;
}

unsigned DfxGetProfileHex(const char *fname, const char *section, const char *key, unsigned uval)
{
	char *p = DfxGetProfileItem(fname, section, key);
	if(!p) 
	{
		return uval;
	}
	else
	{
		unsigned u;
		sscanf(p,"%X",&u);
		return u;
	}
}


int DfxGetProfileInt(const char *fname, const char *section, const char *key, int ival)
{
	char *p = DfxGetProfileItem(fname, section, key);
	if(!p) 
	{
		return ival;
	}
	else
	{
		return atoi(p);
	}
}

double DfxGetProfileDouble(const char *fname, const char *section, const char *key, double dval)
{
	char *p = DfxGetProfileItem(fname, section, key);
	if(!p) 
	{
		return dval;
	}
	else
	{
		return atof(p);
	}
}


int DfxGetProfileIntArray(const char *fname, const char *section, const char *key, int imaxsize, int *pdata)
{
	char *p = DfxGetProfileItem(fname, section, key);
	if(!p) 
	{
		return 0;
	}
	int isize = atoi(strtok(p," \t\n\r"));
	if(isize > imaxsize) isize = imaxsize;
	for(int i=0; i<isize; i++)
	{
		pdata[i] = atoi(strtok(0," \t\n\r"));
	}
	return isize;
}



void DfxRemoveProfileSection(const char *fname, const char *section)
{
	char str[1024];

	FILE *ftmp = fopen("dfxtmp.tmp","wt");
	FILE *f = fopen(fname,"rt");
	if(f)
	{
		// searching for section
		while(fgets(str,1020,f))
		{
			if(DfxCheckProfileSection(str, section)) break;
			fprintf(ftmp,"%s",str);
		}

		while(fgets(str,1020,f))
		{
			char *p=str;
			while(isspace(*p)) p++;
			if(*p == '[') 
			{
				fprintf(ftmp,"%s",str);
				break;
			}
		}

		while(fgets(str,1020,f))
		{
			fprintf(ftmp,"%s",str);
		}
		fclose(f);
	}
	
	fclose(ftmp);
	remove(fname);
	rename("dfxtmp.tmp", fname);
	remove("dfxtmp.tmp");
}


void DfxPutProfileItem(const char *fname, const char *section, const char *key, const char *item)
{
	char str[1024];

	FILE *ftmp = fopen("dfxtmp.tmp","wt");
	FILE *f = fopen(fname,"rt");
	if(f)
	{
		int secfound=0;
		int keyfound=0;

		// searching for section
		while(fgets(str,1020,f))
		{
			secfound = DfxCheckProfileSection(str, section);
			fprintf(ftmp,"%s",str);
			if(secfound) break;
		}

		if(!secfound)
		{
			fprintf(ftmp,"[%s]\n",section);
			fprintf(ftmp,"%s=%s\n\n", key, item);
		}
		else
		{
		// searching for key
			while(fgets(str,1020,f))
			{
				keyfound = DfxCheckProfileKey(str, key);
				if(keyfound == 1) 
				{
					fprintf(ftmp,"%s=%s\n", key, item);
					break;
				}
				if(keyfound == -1) 
				{
					fprintf(ftmp,"%s=%s\n\n", key, item);
					fprintf(ftmp,"%s",str);
					break;
				}
				if(keyfound == 0) 
				{
					if(strchr(str,'=')) fprintf(ftmp,"%s",str);
				}
			}
			if(!keyfound)
			{
				fprintf(ftmp,"%s=%s\n\n", key, item);
			}
		}

		while(fgets(str,1020,f))
		{
			fprintf(ftmp,"%s",str);
		}
		fclose(f);
	}
	else
	{
		fprintf(ftmp,"[%s]\n",section);
		fprintf(ftmp,"%s=%s\n\n", key, item);
	}
	
	fclose(ftmp);
	remove(fname);
	rename("dfxtmp.tmp", fname);
}

void DfxPutProfileString(const char *fname, const char *section, const char *key, const char* sval)
{
	char p[1024];
	strcpy(p,"<");
	if(sval)
	{
		strncat(p,sval,1000);
	}
	strcat(p,">");
	DfxPutProfileItem(fname, section, key, p);
}

void DfxPutProfileText(const char *fname, const char *section, const char *key, const char* sval)
{
	if(!sval) return;
	int len = strlen(sval);
	if(!len) return;

	char keyn[256];
	char *str = new char[len+1];
	strcpy(str, sval);
	char *p = str; 
	int n=0;
	for(int i=0; i<len; i++)
	{
		char c = str[i];
		if(c == '\n' || c == '\r'|| c == '\0')
		{
			str[i] = 0;
			sprintf(keyn, "%s_#_%d", key, n);
			DfxPutProfileString(fname, section, keyn, p);
			n++;
			if(c)
			{
				i++;
				p = str + i;
				while(*p == '\n' || *p == '\r')
				{
					*p++ = 0;
					i++;
				}
			}
		}
	}
	delete [] str;
}

void DfxPutProfileHex(const char *fname, const char *section, const char *key, unsigned uval)
{
	char str[256];
	sprintf(str,"%08X",uval);
	DfxPutProfileItem(fname, section, key, str);
}

void DfxPutProfileInt(const char *fname, const char *section, const char *key, int ival)
{
	char str[256];
	sprintf(str,"%d",ival);
	DfxPutProfileItem(fname, section, key, str);
}

void DfxPutProfileDouble(const char *fname, const char *section, const char *key, double dval)
{
	char str[256];
	sprintf(str,"%25.18e",dval);
	DfxPutProfileItem(fname, section, key, str);
}

void DfxPutProfileIntArray(const char *fname, const char *section, const char *key, int isize, int *pdata)
{
	char str[1024];
	char *p = str;

	p += sprintf(p,"%d",isize);

	for(int i=0; i<isize; i++)
	{
		p += sprintf(p," %d",pdata[i]);
	}

	DfxPutProfileItem(fname, section, key, str);
}
