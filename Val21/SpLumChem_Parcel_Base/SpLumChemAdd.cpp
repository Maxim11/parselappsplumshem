#include "SpLumChemStructs.h"

#include "SpLumChemAdd.h"
#include "CSamples.h"
#include "QntCalibration.h"
#include "Secv.h"
#include "SampleFromFile.h"
#include "Transfer.h"
#include "PbtFile.h"


void *IRCalibrTransfer_Create()
{
	return (void *)new IRCalibrTransfer();
}

void IRCalibrTransfer_Destroy(void *pIRCalT)
{
	delete ((IRCalibrTransfer *)pIRCalT);
}

int IRCalibrTransfer_Transfer(void *pIRCalT,
				char* *pstrSampleName,
				const char* strMasterDir,
				const char* strSlaveDir,
				int numberOfSamples,
				const char* strQntName,
				int nTrType,
				vector<vector<bool>*> *pvSampleSpectraStatus)
{
	return ((IRCalibrTransfer*)pIRCalT)->Transfer(pstrSampleName,
				strMasterDir,
				strSlaveDir,
				numberOfSamples,
				strQntName,
				nTrType,
				pvSampleSpectraStatus);
}

int IRCalibrTransfer_GetNumComp(void *pIRCalT)
{
	return ((IRCalibrTransfer*)pIRCalT)->GetNumComp();
}

double *IRCalibrTransfer_GetSEV_M_Before(void *pIRCalT)
{
	return ((IRCalibrTransfer*)pIRCalT)->GetSEV_M_Before();
}

double *IRCalibrTransfer_GetSEV_S_Before(void *pIRCalT)
{
	return ((IRCalibrTransfer*)pIRCalT)->GetSEV_S_Before();
}

double *IRCalibrTransfer_GetSEV_M_After(void *pIRCalT)
{
	return ((IRCalibrTransfer*)pIRCalT)->GetSEV_M_After();
}

double *IRCalibrTransfer_GetSEV_S_After(void *pIRCalT)
{
	return ((IRCalibrTransfer*)pIRCalT)->GetSEV_S_After();
}

//void IRCalibrTransfer_SetNumCS(void *pIRCalT, int numCS)
//{
//	((IRCalibrTransfer*)pIRCalT)->SetNumCS(numCS);
//}

const char *IRCalibrTransfer_GetCompName(void *pIRCalT, int nc)
{
	return ((IRCalibrTransfer*)pIRCalT)->GetCompName(nc);
}

void IRCalibrTransfer_SaveModel(void *pIRCalT, const char *FileName)
{
	((IRCalibrTransfer*)pIRCalT)->SaveModel(FileName);
}

double IRCalibrTransfer_GetSEC_Before(void *pIRCalT, int comp)
{
	return ((IRCalibrTransfer*)pIRCalT)->GetSECbeforeTrans(comp);
}

double IRCalibrTransfer_GetSEC_After(void *pIRCalT, int comp)
{
	return ((IRCalibrTransfer*)pIRCalT)->GetSECafterTrans(comp);
}

double IRCalibrTransfer_GetR2_After(void *pIRCalT, int comp)
{
	return ((IRCalibrTransfer*)pIRCalT)->GetR2afterTrans(comp);
}

double *IRCalibrTransfer_GetDistS(void *pIRCalT)
{
	return ((IRCalibrTransfer*)pIRCalT)->GetDistS();
}

double *IRCalibrTransfer_GetDistS_tr(void *pIRCalT)
{
	return ((IRCalibrTransfer*)pIRCalT)->GetDistS_tr();
}

double *IRCalibrTransfer_GetDistM(void *pIRCalT)
{
	return ((IRCalibrTransfer*)pIRCalT)->GetDistM();
}

double *IRCalibrTransfer_GetDistM_tr(void *pIRCalT)
{
	return ((IRCalibrTransfer*)pIRCalT)->GetDistM_tr();
}

double *IRCalibrTransfer_GetSampR(void *pIRCalT)
{
	return ((IRCalibrTransfer*)pIRCalT)->GetSampR();
}

//int IRCalibrTransfer_Predict(void *pIRCalT,
//							 const char **spec_list,
//							 const char *s_dir,
//							 const char *m_dir,
//							 int numSp,
//							 double SEV[])
//{
//	return ((IRCalibrTransfer*)pIRCalT)->Predict(spec_list,
//						s_dir, m_dir, numSp, SEV);
//}
int IRCalibrTransfer_GetNumSampleSpectra(void *pIRCalT, int numSample)
{
	return ((IRCalibrTransfer*)pIRCalT)->GetNumSampleSpectra(numSample);
}
double IRCalibrTransfer_GetDistS(void *pIRCalT, int numSample, int numSp)
{
	return ((IRCalibrTransfer*)pIRCalT)->GetDistS(numSample, numSp);
}

double IRCalibrTransfer_GetDistM(void *pIRCalT, int numSample, int numSp)
{
	return ((IRCalibrTransfer*)pIRCalT)->GetDistM(numSample, numSp);
}

double IRCalibrTransfer_GetDistS_tr(void *pIRCalT, int numSample, int numSp)
{
	return ((IRCalibrTransfer*)pIRCalT)->GetDistS_tr(numSample, numSp);
}

double IRCalibrTransfer_GetDistM_tr(void *pIRCalT, int numSample, int numSp)
{
	return ((IRCalibrTransfer*)pIRCalT)->GetDistM_tr(numSample, numSp);
}

// functions for SE4Samples

// Add to IRCalibrate
void*  IRCalibrate_Create(void* pSamples, //samples
					const CSpecPreproc &prePar, // preprocessing parameters
					int nModelType, // calibartion model type
					int numFactors)
{
	return 	(void *) new CQntCalibration(pSamples,
		prePar, nModelType, numFactors);
}

const char* IRCalibrate_GetComponentName(void* pIRCal, int nComponent)
{
	return ((CQntCalibration *)pIRCal)->GetComponentName(nComponent);
}

int IRCalibrate_CalculateSEV(void* pIRCal, void* pSamples, double* pSEV)
{
	return ((CQntCalibration *)pIRCal)->CalculateSEV(pSamples, pSEV);
}

int IRCalibrate_OutputScores(void* pIRCal, const char *fname)
{
	return ((CQntCalibration *)pIRCal)->OutputScores(fname);
}


// Add to IRSecv
void*  IRSecv_Create(void* pSamples, //samples
					const CSpecPreproc &prePar, // preprocessing parameters
					int nModelType, // calibartion model type
					int numFactors,
					const char* fname)
{
	return 	(void *) new CSecv(pSamples, prePar, nModelType, numFactors, fname);
}


// Add CSamples
void* CSamples_Create(const char* strLstFile)
{
	return (void *)new CSamples(strLstFile);
}


void* CSamples_CreateExt(const char* strLstFile)
{
	return (void *)new CSamples(strLstFile, true);
}


int CSamples_GetNumComponents(void* pSamples)
{
	return ((CSamples *)pSamples)->GetNumComponents();
}

int CSamples_GetError(void* pSamples)
{
	return ((CSamples *)pSamples)->GetError();
}

const char* CSamples_GetComponentName(void* pSamples, int nComponent)
{
	return ((CSamples *)pSamples)->GetComponentName(nComponent);
}


void CSamples_Destroy(void *pSamples)
{
	delete ((CSamples *)pSamples);
}

int CSamples_PreprocessSpectra4Transfer(void *pSamples, void *p, bool bExtSpectra)
{
	return ((CSamples *)pSamples)->PreprocessSpectra4Transfer(p, bExtSpectra);
}


// SampleFromFile

void* CSampleFromFile_Create(const char* fname)
{
	return (void *)new CSampleFromFile(fname);
}

void CSampleFromFile_Destroy(void *pSampleFromFile)
{
	delete (CSampleFromFile *)pSampleFromFile;
}

int CSampleFromFile_GetNumSpectra(void *pSampleFromFile)
{
	return ((CSampleFromFile *)pSampleFromFile)->GetNumSpectra();
}

const char* CSampleFromFile_GetSpectrumName(void *pSampleFromFile, int iS)
{
	return (((CSampleFromFile *)pSampleFromFile)->GetSpectrumName(iS)).c_str();
}

int CSampleFromFile_GetError(void *pSampleFromFile)
{
	return ((CSampleFromFile *)pSampleFromFile)->GetError();
}

// Pbt (preprocessing before transfer)
void* CPbt_Create(const char* fname)
{
	return (void *)new CPbt(fname);
}

int CPbt_GetError(void *pPbt)
{
	return ((CPbt *)pPbt)->GetError();
}

void CPbt_Destroy(void *pPbt)
{
	delete (CPbt *)pPbt;
}

const char* CPbt_GetPreprocStr(void *pPbt)
{
	return ((CPbt *)pPbt)->GetPreprocStr();
}

const char* CPbt_GetPrefix(void *pPbt)
{
	return ((CPbt *)pPbt)->GetPrefix();
}

int CPbt_GetSpType(void *pPbt)
{
	return ((CPbt *)pPbt)->GetSpType();
}

double CPbt_GetStartFreq(void *pPbt)
{
	return ((CPbt *)pPbt)->GetStartFreq();
}

double CPbt_GetStopFreq(void *pPbt)
{
	return ((CPbt *)pPbt)->GetStopFreq();
}



const char *GetDllVersion() { return "01.04.12"; }

const char *GetErrorMessage() { return strErrorMessage; }
