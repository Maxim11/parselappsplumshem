#include "HSOMatrixUtils.h"
#include "newmat.h"


//--------------------------------------------------------------------------
// ���������� ��� ������ �� ������� lemke1()
int HCQP(const Matrix &mX, const ColumnVector &vY, double D, double Knl, ColumnVector &vPGarm)
{
// mX [numSp x numFact]
// vY [numSp]
// vPGarm [numFact]
// n = numSp
// k = numFact
// D - ������ ��������� (��������� - ����������)
// Knl - ����������� ������������

	int numSp = mX.Nrows();
	int numFact = mX.Ncols();

	Matrix mC(numFact, numFact);
	ColumnVector vD(numFact);

//	AppendToDebugReport("Start rpcf");

//	rpcf(mX, vY, mC, vD);

	mC =  mX.t() * mX;
	mC *= 2.0;

	vD = mX.t() * vY;
	vD *= -2.0;

	ColumnVector vDop(numFact);

	vDop = D;

	if (Knl)
	{
		for (int i = numFact/2; i < numFact; i++)
		{
            vDop(i+1) *= Knl;
		}
	}

//	i = lemke1(mC, vD, vDdop, numFact, vP, false);

	double *c = new double[numFact*numFact];
	double *d = new double[numFact];
	double *xd = new double[numFact];
	double *x = new double[numFact];

	for (int k = 0; k < numFact; k++)
	{
		d[k] = vD(k+1);
		xd[k] = vDop(k+1);

        for (int j = 0; j < numFact; j++)
		{
			c[k*numFact + j] = mC(k+1, j+1);
		}
	}

	int i = lemke1(c, d, xd, numFact, x, false);

	for (int  k = 0; k < numFact; k++)
	{
		vPGarm(k+1) = x[k];
	}

	return i;
}

// ���������� ��� ������
int lemke1 (double  *c, double  *d, double  *xd, int m, double  *x, bool STOP)
 {
   int currentIter;
   double eps;
   eps=Epsilon();
   double r,r1;
   double *q,*tab,*cpcr,*cpcl;
   int pcl,pcr,baz1,*baz;
   int i, j, n;
   n = 2*m;
   q = new double[n];
   for(i = 0;i < m; i ++)
      {
      q[i] = d[i];
      for(j = 0;j < m; j ++)
         q[i] -= c[i*m + j] * xd[j];
      q[i + m] = 2*xd[i];
      }
   r=0.0;
   for (i=0;i<n;i++)
     if (r>q[i])
       {
       r=q[i];
       pcr=i;
       }
   if ( r == 0.0 )
     {
     for (i=0;i<m;i++)
        x[i]= -xd[i];
     delete []q;
     return 0;
     }
   tab = new double[n*(2*n+2)];
   cpcr = new double[2*n+2];
   cpcl = new double[n];
   baz = new int[n];
   pcl = 2 * n;
   for ( i = 0; i < n; i++ )
       {
       baz[i] = i;
       tab[i * (2*n + 2) + i] = 1;
       tab[i * (2*n + 2) + 2*n] = (-1);
       tab[i * (2*n + 2) + 2*n+1] = q[i];
       for ( j = 0; j < n; j++ )
           {
           if(i != j)
             tab[i * (2*n + 2) + j] = 0;
           tab[i * (2*n + 2) + n + j] = ((i<m)&&(j<m)) ?  -c[i*m + j] : 0.0;
           }
       if(i<m)
         tab[i * (2*n + 2) + i + 3*m] = -1;
       else
         tab[i * (2*n + 2) + i + m] = 1;
       }
   currentIter=0;
   while(1)
        {
        if(STOP)
           {
           delete []q;
           delete []tab;
           delete []cpcr;
           delete []cpcl;
           delete []baz;
           return -1;
           }
        if ( ( fabs ( tab[pcr * (2*n + 2) + pcl] ) ) < eps )
           {
           delete []q;
           delete []tab;
           delete []cpcr;
           delete []cpcl;
           delete []baz;
           return 2;
           }
        for ( i = 0; i < ( 2 * n + 2 ); i++ )
            cpcr[i] = tab[pcr * (2*n + 2) + i];
        for ( i = 0; i < n; i++ )
            cpcl[i] = tab[i * (2*n + 2) + pcl];
        for ( j = 0; j < ( 2 * n + 2 ); j++ )
            {
            tab[pcr * (2*n + 2) + j] = tab[pcr * (2*n + 2) + j] / cpcr[pcl];
            for ( i = 0; i < n; i++ )
                if ( i != pcr )
                   tab[i * (2*n + 2) + j] -= cpcr[j] * cpcl[i]/cpcr[pcl];
            }
    currentIter++;
        baz1 = baz[pcr];
        baz[pcr] = pcl;
        for ( i = 0; i < n; i++ )
           if ( ( baz[i] == 2*n ) && ( tab[i * (2*n + 2) + 2*n+1] > eps ) )
              break;
        if ( i == n )
           {
           for ( i = 0; i < m; i++ )
              x[i] = -xd[i];
           for ( i = 0; i < n; i++ )
              if ( (baz[i] >= n) && (baz[i] < 2*n) && ((baz[i]-n)< m) )
                 x[baz[i] - n] += tab[i * (2*n + 2) + 2*n+1];
           delete []q;
           delete []tab;
           delete []cpcr;
           delete []cpcl;
           delete []baz;
           return 0;
           }
        if ( baz1 >= n )
           pcl = baz1 - n;
        else
           pcl = baz1 + n;
        j = 0;
        for ( i = 0; i < n; i++ )
            {
            if ( tab[i * (2*n + 2) + pcl] > 0 )
               {
               r1 = tab[i * (2 * n + 2) + 2 * n + 1] / tab[i * ( 2 * n + 2) + pcl];
               j++;
               if ( ( j == 1 ) || ( r > r1 ) )
                  {
                  pcr = i;
                  r = r1;
                  }
               }
            }
        if ( j == 0 )
           {
           for (i=0;i<m;i++)
           x[i]= 0;
           delete []q;
           delete []tab;
           delete []cpcr;
           delete []cpcl;
           delete []baz;
           return 3;
           }
        }
 }