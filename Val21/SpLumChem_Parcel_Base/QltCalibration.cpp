#include <math.h>
#include "QltCalibration.h"
#include "PreprocUtils.h"
#include "MatrixUtilities.h"
#include "CSamples.h"
#include "debug.h"


CQltCalibration::~CQltCalibration()
{
#ifdef _DEBUG_
	AppendToDebugReport("Qlt destructor start");
#endif	//AppendToDebugReport("FillQnt end");

	delete pClbSamples;
	delete pModel;
	delete pTransData;
	delete pQnt;

#ifdef _DEBUG_
	AppendToDebugReport("Qlt destructor start");
#endif	//AppendToDebugReport("FillQnt end");
}

CQltCalibration::CQltCalibration(vector<CSampleProxy> *pSamples, // array of samples
					const CFreqScale *frScale, // ��������� ����� ��������
					const CSpecPreproc *prePar, // preprocessing parameters
					int numFactors, // number of factors
					CQnt_Parcel **pQntOut) // created model
{
#ifdef _DEBUG_
	AppendToDebugReport("Start Qlt");
#endif	

	Init();

	*pQntOut = 0;

	pClbSamples = new CSamples(pSamples, frScale, &(prePar->strCompName));

	nError = pClbSamples->GetError();

	if (nError != ER_OK)
	{
		return;
	}
	pTransData = new CTransData();

	freqScale = *frScale;

	//frScale->Print("freqScaleCreate.txt");

	specPreprocData.nModelType = MODEL_PCA;

	specPreprocData.fMinFreq = freqScale.fStartFreq;
	specPreprocData.fMaxFreq = freqScale.fStartFreq + freqScale.fStepFreq * (freqScale.nNumberOfFreq - 1);
	specPreprocData.numberOfChannels = freqScale.nNumberOfFreq;

	specPreprocData.nSpType = prePar->nSpType;
	specPreprocData.strPreprocPar = prePar->strPreprocPar;

	specPreprocData.numberOfExcludedFreq = prePar->vFreqExcluded.size();
	specPreprocData.vUseFreq.resize(frScale->nNumberOfFreq, 1);

	if (specPreprocData.numberOfExcludedFreq)
	{
		for(int i = 0; i < prePar->vFreqExcluded.size(); i++)
		{
			int ind = prePar->vFreqExcluded[i];
			specPreprocData.vUseFreq[ind] = 0;
		}
	}

	nError = pClbSamples->PreprocessSpectra(specPreprocData, mX);

	if (nError != ER_OK)
	{
		return;
	}

	if (numFactors > 0 )
	{
		pModel = new CModelPCA(mX, numFactors);
	}
	else
	{
		pModel = new CModelPCA(mX, 10);
	}

	nError = pModel->GetError();

	if (nError != ER_OK)
	{
		return;
	}

#ifdef _DEBUG_
	AppendToDebugReport("Model was created");
#endif	


	//AppendToDebugReport("CQltCalibration::CQltCalibration Model end");

	if (nError == ER_OK)
	{
		CalculateMahDist();
	}

#ifdef _DEBUG_
	AppendToDebugReport("Mah Dist were calculated");
#endif	


	if (nError == ER_OK)
	{
		pQnt = new CQnt_Parcel();
		FillQnt(pQnt);
		*pQntOut = pQnt;
	}

#ifdef _DEBUG_
	AppendToDebugReport("Qnt was filled");
#endif	//AppendToDebugReport("FillQnt end");

}

// ��������� ������� � ������������ ����������
/*void CQltCalibration::AddSamples(int numSamples, // number of samples
					const CSampleProxy* pSamples, // array of samples
					const CSpecPreproc &prePar, // preprocessing parameters
					int numFactors) // number of factors
{
	CSamples theAddedSamples(numSamples, pSamples);

	pClbSamples->AddSamples(theAddedSamples);

	freqScale = pSamples[0].frScale;

	specPreprocData.fMinFreq = prePar.fMinFreq;
	specPreprocData.fMaxFreq = prePar.fMaxFreq;
	specPreprocData.nSpType = prePar.nSpType;
	specPreprocData.strPreprocPar = string(prePar.strPreprocPar);

	nError = ER_OK;

	pClbSamples->PreprocessSpectra(specPreprocData, mX);

	nError = pClbSamples->GetError();

	if (nError != ER_OK)
	{
		return;
	}

	delete pModel;

	pModel = 0;

	if (numFactors > 0 )
	{
		pModel = new CModelPCA(mX, numFactors);
		nError = pModel->GetError();
	}
	else
	{
		pModel = new CModelPCA(mX, 10);
		nError = pModel->GetError();
	}

	if (nError == ER_OK)
	{
		CalculateMahDist();
	}
}*/

/*CQltCalibration::CQltCalibration(const char* fileName)
{
	Init();

	Load(fileName);
}*/

// ��������� ���������� ������������
// ��� �������� ��������������� ������
int CQltCalibration::CalculateMahDist()
{
	if (specPreprocData.strPreprocPar.find_first_of('A') != string::npos)
	{
		// averaging was performed before Model building
		// now we need to predict all spectra, not averaged
		Matrix mSp;
		pClbSamples->GetMSpectra(mSp);

		PrepSpForPrediction(&freqScale, specPreprocData, mSp, mX, false);
		pModel->CalculateMahalanobisDistance(mX, vMahalanobisDistances);
	}
	else
	{
		pModel->GetMahalanobisDistance(vMahalanobisDistances);
	}

	int numClbSpectra = vMahalanobisDistances.Nrows();

	if (numClbSpectra > 1)
	{
		fMeanMahDist = sqrt(vMahalanobisDistances.SumSquare() / (numClbSpectra - 1));
	}
	else
	{
		fMeanMahDist = sqrt(vMahalanobisDistances.SumSquare());
	}

	return nError;
}

// ���������� ����� �������� ��� ������� �������
// ��������������� ������
int CQltCalibration::GetNumSampleSpectra(int nSample) const
{
	return pClbSamples->GetSampleSpectraNumbers(nSample);
}


// ���������� ������ ���������� ������������
// ��� �������� ������� ������� ��������������� ������
void CQltCalibration::GetSampleDistances(int nSample, VDbl *vNormMahDistances) const
{
	int nSpectra = GetNumSampleSpectra(nSample);

	for (int i = 0; i < nSpectra; i++)
	{
		int nSpIndex;

		nSpIndex = pClbSamples->GetSampleSpectraIndexes(nSample);

		(*vNormMahDistances)[i] = vMahalanobisDistances(nSpIndex + i + 1);
	}
}

// ���������� ������ ������������� ���������� ������������
// ��� �������� ������� ������� ��������������� ������
void CQltCalibration::GetSampleNormDistances(int nSample, VDbl *vNormMahDistances)
{
	GetSampleDistances(nSample, vNormMahDistances);

	int nSpectra = GetNumSampleSpectra(nSample);

	for (int i = 0; i < nSpectra; i++)
	{
		(*vNormMahDistances)[i] /= fMeanMahDist;
	}
}


// ���������� ���������� ������������ ��� �������
// ������� ������� ������� ��������������� ������
void CQltCalibration::GetSampleSpecDistance(int nSample, int nSpec, double* dist) const
{
	int nSpIndex;

	nSpIndex = pClbSamples->GetSampleSpectraIndexes(nSample);

	*dist = vMahalanobisDistances(nSpIndex + nSpec + 1);
}


/*int CQltCalibration::Save(const char* fileName)
{
	const int version = 19; // 25.03.2004

	FILE *fgh = fopen(fileName, "wb");

	if (!fgh)
	{
		nError = ER_FILE_CREATION;
	}
	else
	{
		string strClbName = "Qualitative Calibration";

		SaveString(strClbName, fgh);

		fwrite(&version, sizeof(int), 1, fgh);

		// write samples

		pClbSamples->Save(fgh, version);

		// write model

		specPreprocData.Save(fgh, version);

		pModel->Save(fgh, version);

		SaveColumnVector(vMahalanobisDistances, fgh);

		// write TransData
		pTransData->Save(fgh, version);

		fclose(fgh);
	}

	return nError;
}*/


/*int CQltCalibration::Load(const char* fileName)
{
	const int nVersion = 19; // 25.03.2004


	FILE *fgh = fopen(fileName, "rb");

	if (!fgh)
	{
		nError = ER_FILE_OPEN;
	}
	else
	{
		nError = ER_OK;

		string strClbName;
		LoadStringMy(strClbName, fgh);


		if (strClbName != "Qualitative Calibration")
		{
			nError = ER_BAD_QLT_FILE;
			fclose(fgh);

			return nError;
		}

		int version;

		fread(&version, sizeof(int), 1, fgh);

		if (version < nVersion)
		{
			nError = ER_BAD_QLT_VER;
			fclose(fgh);

			return nError;
		}

		// read samples

		pClbSamples = new CSamples();

		pClbSamples->Load(fgh, version);

		// read model

		specPreprocData.Load(fgh, version);

		pModel = new CModelPCA;

		pModel->Load(fgh, version);

		LoadColumnVector(vMahalanobisDistances, fgh);

		int numClbSpectra = vMahalanobisDistances.Nrows();

		if (numClbSpectra > 1)
		{
			fMeanMahDist = sqrt(vMahalanobisDistances.SumSquare() / (numClbSpectra - 1));
		}
		else
		{
			fMeanMahDist = sqrt(vMahalanobisDistances.SumSquare());
		}

		fclose(fgh);

		// load freqScale from pClbSamples
		freqScale.fStartFreq = pClbSamples->GetStartFreq();
		freqScale.fStepFreq = pClbSamples->GetStepFreq();
		freqScale.nNumberOfFreq = pClbSamples->GetNumFreq();

		// read TransData
		pTransData = new CTransData();
		pTransData->Load(fgh, version);
	}

	return nError;
}*/

/*const char* CQltCalibration::GetSampleName(int nSample)
{
	return pClbSamples->GetSampleName(nSample);
}*/

/*const char* CQltCalibration::GetSampleSpecName(int nSample, int nSpec)
{
	return pClbSamples->GetSampleSpecName(nSample, nSpec);
}*/

// ��������� ���������� ������������ ��� ������������ �������
// �� ����������� �������������� ������
double CQltCalibration::CalculateMahalanobisDistance(const CFreqScale *dsc, // spectrum descriptor
						const VDbl *vSp)
{
	RowVector vSpectrum = VDbl2Row(*vSp);

	RowVector vX;

	nError = PrepSpForPrediction(dsc, specPreprocData, vSpectrum, vX, false);

	//char str[64];
	//sprintf(str, "nError = %d", nError);
    //AppendToDebugReport(str);

	if (nError != ER_OK)
	{
		return -1;
	}

	ColumnVector vMah;

	pModel->CalculateMahalanobisDistance(vX, vMah);

	nError = pModel->GetError();

	if (nError != ER_OK)
	{
		return -1;
	}

	return vMah(1);
}


double CQltCalibration::CalculateNormMahalanobisDistance(const CFreqScale *dsc, // spectrum descriptor
						const VDbl *vSpectrum)
{
	//dsc->Print("freqScale.txt");
	double dist = CalculateMahalanobisDistance(dsc, vSpectrum);

	// normalize
	dist /= fMeanMahDist;

	return dist;

}

void CQltCalibration::Transfer(CTransData& theTransData)
{
	(*pTransData).nTransType = theTransData.nTransType;

	switch(theTransData.nTransType)
	{
	case 1:
		(*pTransData).vBiasRef = theTransData.vBiasRef;
		(*pTransData).vSlopeRef = theTransData.vSlopeRef;
		break;
	case 2:
		(*pTransData).vBiasSp = theTransData.vBiasSp;
		(*pTransData).vSlopeSp = theTransData.vSlopeSp;
		break;
	}

	pClbSamples->PreprocessSpectra(specPreprocData, mX);

	pTransData->CorrectSpectra(mX);

	int numFactors = pModel->GetNumFactors();

	if (numFactors > 0 )
	{
		pModel = new CModelPCA(mX, numFactors);
	}
	else
	{
		pModel = new CModelPCA(mX, 10);
	}

	if (nError == ER_OK)
	{
		CalculateMahDist();
	}
}

void CQltCalibration::Init()
{
	nError = ER_OK;
	pClbSamples = 0;
	pModel = 0;
	pTransData = 0;
	pQnt = 0;
}

// ��������� ���������� ������������ ��� �������� ������� 
// ������� ������� �������������� ������
double CQltCalibration::GetSampleDistance(int nSample)
{
//	char str[256];
//	sprintf(str, "numClbSpectra = %d\n", numClbSpectra);
	//AppendToDebugReport("CQltCalibration::GetSampleDistance start");

	RowVector vSpectrum;

	pClbSamples->GetMeanSpectrum(nSample, vSpectrum);

	RowVector vX;

	nError = PrepSpForPrediction(&freqScale, specPreprocData, vSpectrum, vX, false);

	if (nError != ER_OK)
	{
		return nError;
	}

	ColumnVector vMah;

	pModel->CalculateMahalanobisDistance(vX, vMah);

	nError = pModel->GetError();

	if (nError != ER_OK)
	{
		return -1;
	}

	//AppendToDebugReport("CQltCalibration::GetSampleDistance end");

	return vMah(1);
}

// ��������� ������������� ���������� ������������ ��� �������� ������� 
// ������� ������� �������������� ������
double CQltCalibration::GetSampleNormDistance(int nSample)
{
	//AppendToDebugReport("CQltCalibration::GetSampleNormDistance start");

	double dist = GetSampleDistance(nSample);

	//AppendToDebugReport("CQltCalibration::GetSampleNormDistance end");

	return dist / fMeanMahDist;
}

void CQltCalibration::FillQnt(CQnt_Parcel *pQnt)
{
	if (specPreprocData.meanComponents.Ncols())
	{
		//pQnt->m_specPreprocData.meanComponents = Row2VDbl(specPreprocData.meanComponents);
		Row2VDbl(specPreprocData.meanComponents, pQnt->m_specPreprocData.meanComponents);
	}
	else
	{
		pQnt->m_specPreprocData.meanComponents.clear();
	}

	if (specPreprocData.meanSpectrum.Ncols())
	{
		//pQnt->m_specPreprocData.meanSpectrum = Row2VDbl(specPreprocData.meanSpectrum);
		Row2VDbl(specPreprocData.meanSpectrum, pQnt->m_specPreprocData.meanSpectrum);
	}
	else
	{
		pQnt->m_specPreprocData.meanSpectrum.clear();
	}

	if (specPreprocData.meanSpectrumMSC.Ncols())
	{
		//pQnt->m_specPreprocData.meanSpectrumMSC = Row2VDbl(specPreprocData.meanSpectrumMSC);
		Row2VDbl(specPreprocData.meanSpectrumMSC, pQnt->m_specPreprocData.meanSpectrumMSC);
	}
	else
	{
		pQnt->m_specPreprocData.meanSpectrumMSC.clear();
	}

	if (specPreprocData.meanStdComp.Ncols())
	{
		//pQnt->m_specPreprocData.meanStdComp = Row2VDbl(specPreprocData.meanStdComp);
		Row2VDbl(specPreprocData.meanStdComp, pQnt->m_specPreprocData.meanStdComp);
	}
	else
	{
		pQnt->m_specPreprocData.meanStdComp.clear();
	}

	if (specPreprocData.meanStdSpec.Ncols())
	{
		//Qnt->m_specPreprocData.meanStdSpec = Row2VDbl(specPreprocData.meanStdSpec);
		Row2VDbl(specPreprocData.meanStdSpec, pQnt->m_specPreprocData.meanStdSpec);
	}
	else
	{
		pQnt->m_specPreprocData.meanStdSpec.clear();
	}

	pQnt->m_specPreprocData.fMax = specPreprocData.fMax;

	//pQnt->m_SpecPreproc
	pQnt->m_SpecPreproc.nSpType = specPreprocData.nSpType;
	pQnt->m_SpecPreproc.strPreprocPar = specPreprocData.strPreprocPar;
	pQnt->m_SpecPreproc.strPreprocParTrans = specPreprocData.strPreprocParTrans;

//	if (specPreprocData.vComponentCorrections.Ncols())
//	{
		//pQnt->m_specPreprocData.vComponentCorrections = Row2VDbl(specPreprocData.vComponentCorrections);
//		Row2VDbl(specPreprocData.vComponentCorrections, pQnt->m_specPreprocData.vComponentCorrections);
//	}
//	else
//	{
//		pQnt->m_specPreprocData.vComponentCorrections.clear();
//	}

//	if (!specPreprocData.vFreqExcluded.empty())
//	{
//		pQnt->m_specPreprocData.vFreqExcluded = specPreprocData.vFreqExcluded;
//	}
//	else
//	{
//		pQnt->m_specPreprocData.vFreqExcluded.clear();
//	}

//	AppendToDebugReport("End filling m_specPreprocData");

	//pQnt->m_specPreprocDataTrans
	if (specPreprocData.meanComponentsTrans.Ncols())
	{
		Row2VDbl(specPreprocData.meanComponentsTrans, pQnt->m_specPreprocDataTrans.meanComponents);
	}
	else
	{
		pQnt->m_specPreprocDataTrans.meanComponents.clear();
	}

	pQnt->strPreprocParTrans = specPreprocData.strPreprocParTrans;

	// fill Model
	pModel->FillModel_Parcel(pQnt->m_Model);

//	AppendToDebugReport("End filling m_Model");

	// results
//	pQnt->vSEC = Column2VDbl(vSEC);
//	Column2VDbl(vSEC, pQnt->vSEC);

//	AppendToDebugReport("End filling vSEC");

//	pQnt->vR2Stat = Column2VDbl(vR2Stat);
//	Column2VDbl(vR2Stat, pQnt->vR2Stat);

//	AppendToDebugReport("End filling vR2Stat");

//	pQnt->vMahalanobisDistances = Column2VDbl(vMahalanobisDistances);
	Column2VDbl(vMahalanobisDistances, pQnt->vMahalanobisDistances);

	pQnt->fMeanMahDist = fMeanMahDist;

//	AppendToDebugReport("End filling vMahalanobis");


//	AppendToDebugReport("End filling pQnt");
}
