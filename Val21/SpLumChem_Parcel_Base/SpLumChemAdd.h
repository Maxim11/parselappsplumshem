#ifndef _SPLUMCHEMADD_H_
#define _SPLUMCHEMADD_H_


// this header includes functions, which needed
// in all programms except that for SpLumPro
// which included in file SpLumChem.h

#ifdef _IMPORTING_
#define _DLL_API_ __declspec(dllimport)
#else
#define _DLL_API_ __declspec(dllexport)
#endif

//#include "StructsEnums.h"
#include "SpLumChemStructs.h"


#include <vector>

using namespace std;



//----------------------------- IRCalTransfer -------------------------------//

_DLL_API_ void *IRCalibrTransfer_Create();

_DLL_API_ void IRCalibrTransfer_Destroy(void *pIRCalT);

_DLL_API_ int IRCalibrTransfer_GetNumComp(void *pIRCalT);

_DLL_API_ const char *IRCalibrTransfer_GetCompName(void *pIRCalT, int nc);

_DLL_API_ double *IRCalibrTransfer_GetSEV_M_Before(void *pIRCalT);

_DLL_API_ double *IRCalibrTransfer_GetSEV_S_Before(void *pIRCalT);

_DLL_API_ double *IRCalibrTransfer_GetSEV_M_After(void *pIRCalT);

_DLL_API_ double *IRCalibrTransfer_GetSEV_S_After(void *pIRCalT);

_DLL_API_ double *IRCalibrTransfer_GetDistS(void *pIRCalT);

_DLL_API_ double *IRCalibrTransfer_GetDistS_tr(void *pIRCalT);

_DLL_API_ double *IRCalibrTransfer_GetDistM(void *pIRCalT);

_DLL_API_ double *IRCalibrTransfer_GetDistM_tr(void *pIRCalT);

_DLL_API_ double *IRCalibrTransfer_GetSampR(void *pIRCalT);

_DLL_API_ void IRCalibrTransfer_SetNumCS(void *pIRCalT, int numCS);

_DLL_API_ int IRCalibrTransfer_Transfer(void *pIRCalT,
				char* *pstrSampleName,
				const char* strMasterDir,
				const char* strSlaveDir,
				int numberOfSamples,
				const char* strQntName,
				int nTrType,
				vector<vector<bool>*> *pvSampleSpectraStatus);

_DLL_API_ void IRCalibrTransfer_SaveModel(void *pIRCalT, const char *FileName);

_DLL_API_ double IRCalibrTransfer_GetSEC_Before(void *pSECV, int comp);

_DLL_API_ double IRCalibrTransfer_GetSEC_After(void *pSECV, int comp);

_DLL_API_ double IRCalibrTransfer_GetR2_After(void *pSECV, int comp);

_DLL_API_  int IRCalibrTransfer_GetNumSampleSpectra(void *pIRCalT, int numSample);

_DLL_API_  double IRCalibrTransfer_GetDistS(void *pIRCalT, int numSample, int numSp);

_DLL_API_  double IRCalibrTransfer_GetDistM(void *pIRCalT, int numSample, int numSp);

_DLL_API_  double IRCalibrTransfer_GetDistS_tr(void *pIRCalT, int numSample, int numSp);

_DLL_API_  double IRCalibrTransfer_GetDistM_tr(void *pIRCalT, int numSample, int numSp);


//_DLL_API_ int IRCalibrTransfer_Predict(void *pIRCalT,
//									   const char **spec_list,
//									   const char *s_dir,
//									   const char *m_dir,
//									   int numSp,
//									   double SEV[]);


// functions for SE4Samples


// Add to IRCalibrate
_DLL_API_ void* IRCalibrate_Create(void* pSamples, //samples
					const CSpecPreproc &prePar, // preprocessing parameters
					int nModelType, // calibartion model type
					int numFactors = 0); // number of factors

_DLL_API_ const char* IRCalibrate_GetComponentName(void* pIRCal, int nComponent);


_DLL_API_ int IRCalibrate_CalculateSEV(void* pIRCal, void* pSamples, double* pSEV);


_DLL_API_ int IRCalibrate_OutputScores(void* pIRCal, const char *fname);


// Add to IRSecv
_DLL_API_ void* IRSecv_Create(void* pSamples, // pointer on samples
					const CSpecPreproc &prePar, // preprocessing parameters
					int nModelType, // calibartion model type
					int numFactors, // number of factors
					const char* fname = 0); // secv.txt file



// Add CSamples
_DLL_API_ void* CSamples_Create(const char* strLstFile);

_DLL_API_ void* CSamples_CreateExt(const char* strLstFile);

_DLL_API_ void CSamples_Destroy(void *pSamples);

_DLL_API_ int CSamples_GetError(void *pSamples);

_DLL_API_ int CSamples_GetNumComponents(void* pSamples);

_DLL_API_ const char* CSamples_GetComponentName(void* pSamples, int nComponent);

_DLL_API_ int CSamples_PreprocessSpectra4Transfer(void* pSamples, void *p, bool bExtSpectra);


// SampleFromFile
_DLL_API_ void* CSampleFromFile_Create(const char* fname);

_DLL_API_ void CSampleFromFile_Destroy(void *pSampleFromFile);

_DLL_API_ int CSampleFromFile_GetNumSpectra(void *pSampleFromFile);

_DLL_API_ const char* CSampleFromFile_GetSpectrumName(void *pSampleFromFile, int iS);

_DLL_API_ int CSampleFromFile_GetError(void *pSampleFromFile);


// Pbt (preprocessing before transfer)
_DLL_API_ void* CPbt_Create(const char* fname);

_DLL_API_ int CPbt_GetError(void *pPbt);

_DLL_API_ void CPbt_Destroy(void *pPbt);

_DLL_API_ int CPbt_GetSpType(void *pPbt);

_DLL_API_ const char* CPbt_GetPreprocStr(void *pPbt);

_DLL_API_ const char* CPbt_GetPrefix(void *pPbt);

_DLL_API_ double CPbt_GetStartFreq(void *pPbt);

_DLL_API_ double CPbt_GetStopFreq(void *pPbt);

// version
_DLL_API_ const char *GetDllVersion();

_DLL_API_ const char *GetErrorMessage();

#endif // _SPLUMCHEMADD_H_
