#ifndef _PREPROCUTILS_H_
#define _PREPROCUTILS_H_

#include "newmat.h"

#include "SpLumChemStructs.h"

#define SECV_PREP_ONCE 0

void MeanCentering(const Matrix& mX, Matrix& mXpreproc, RowVector& vXmean);

int DeviationScaling(const Matrix& mX, Matrix& mXpreproc, RowVector& vXdev);

int SNVtransformation(const Matrix& mX, Matrix& mXpreproc, ColumnVector& vXmean, ColumnVector& vXdev);

void MeanCentering(Matrix& mX, RowVector& vXmean);

int DeviationScaling(Matrix& mX, RowVector& vXdev);

int SNVtransformation(Matrix& mX, ColumnVector& vXmean, ColumnVector& vXdev);

int BaseLineCor(Matrix& mX);

int PrepSpForPrediction(const CFreqScale *freqScale,
						const CSpecPreprocData& specPreprocData,
						const Matrix& mSp,
						Matrix& mSpPrep, bool bOptim);

int PrepSpForPrediction(const CSpecPreprocData& specPreprocData,
						const Matrix& mSp,
						Matrix& mSpPrep,
						double fStepFreq);


//void PrepCompForPrediction(const CSpecPreprocData& specPreprocData,
//						const Matrix& mComp,
//						Matrix& mCompPrep);

int PostProcessComponents(Matrix& mY, const CSpecPreprocData& specPreprocData, bool bCor);

int Derivate(Matrix& mX, double fStepX, int nWinSize, int nPolOrder, int nDerivOrder);

int MultCorrection(Matrix& mX, RowVector& vXmean);

int MultCorrectionForPrediction(Matrix& mX, const RowVector& vXmean);

void ABS(Matrix& mX);

void ExcludeColumns(Matrix &mXp, int numberOfExcludedFreq, const VInt &vFreqExcluded);

//int PreprocessSpectra(CSpecPreprocData& specPreprocData,
//					  double fStepFreq,
//				const Matrix& mSp,
//				Matrix& mSpPrep);

int PreprocessSpectra(CSpecPreprocData& specPreprocData,
					  double fStepFreq,
				Matrix& mSpPrep);

int FindIndex(double f, const CFreqScale & freqScale, const CSpecPreprocData & specPreprocData, bool bMin);


#endif // _PREPROCUTILS_H_
