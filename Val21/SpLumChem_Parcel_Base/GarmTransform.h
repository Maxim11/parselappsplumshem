#ifndef _GARMTRANSFORM_H_
#define _GARMTRANSFORM_H_

#include "newmat.h"
#include "SpLumChemStructs.h"
//void GarmTransformOld(int numFact, const Matrix &mX, const int *pUseFreq, Matrix &mXGarm);

void GarmTransform(int numFact, int numFreq, const int *pUseFreq, Matrix &mGarm);

void GarmTransform(int numFact, int numFreq, const VInt &vFreqExcluded, Matrix &mGarm);

//void GarmTransformNewGl(int numFact, const Matrix &mX, const int *pUseFreq, Matrix &mXGarm);

#endif // _GARMTRANSFORM_H_
