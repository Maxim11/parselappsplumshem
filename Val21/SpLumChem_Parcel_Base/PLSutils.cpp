#include "PLSutils.h"
#include <stdlib.h>


bool CheckDescending(const DiagonalMatrix &D)
{
	// check sorting diagonal matrix in descending order
	// return true if sorted, false otherwise

	int n = D.Nrows();
	if (n == 0) return true;

	bool res = true;
	double y, x = D(1);


	for (int i = 2; i <= n; i++)
	{
		y = D(i);
		if (y  > x)
		{
			res = false;
			break;
		}
		x = y;
	}

	return res;
}

double *V;

int compare(const void *x1, const void *x2)
{
	if (*(V + *(int *)x1) > *(V + * (int *)x2)) return -1;
	else return 1;
}

void sort_d2(int p[], int n)
{
	// sort in descending order!!!
	qsort((void*) p, (size_t)n, sizeof(int), compare);
}

int SortDescending(DiagonalMatrix &D, Matrix &E)
{
	// D - eivenvalues
	// E - eigenvectors

	int n = D.Nrows();
	if ((n == 0) || (n != E.Ncols())) return -1;

	V = D.Store();

	int *index = new int[n];
	for (int i = 0; i < n; i++) index[i]=i;
	sort_d2(index, n);

	DiagonalMatrix Dsort(n);
	Matrix Esort(n, E.Nrows());

	for (int i = 1; i <= n; i++)
	{
		int k = index[i-1] + 1;

		Dsort(i) = D(k);
		Esort.Column(i) = E.Column(k);
	}

	D = Dsort;
	E = Esort;

	delete[] index;

	return 0;
}