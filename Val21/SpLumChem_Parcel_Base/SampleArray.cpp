// ApjSampleLoader.cpp: implementation of the CApjSampleLoader class.
//
//////////////////////////////////////////////////////////////////////

//#include "stdafx.h"
#include "Dfx.h"
#include "SampleArray.h"
#include "SpLumChemStructs.h"


CSampleArray::CSampleArray()
{
	Init();
}

void CSampleArray::Init()
{
	m_strFileTitle = "";
	m_nSpecNum = 0;
	m_nSpecIncNum = 0;
	nError = ER_OK;
}

CSampleArray::Load(const char *fname, int i, bool flagExt)
{
	// fname - name of new list file (extension "lst")
	// sample number (starting with 1)
	char section[32];

	Init();
	
	if (flagExt)
	{
		sprintf(section, "SampleArrayExt_%d", i);
	}
	else
	{
		sprintf(section, "SampleArray_%d", i);
	}

	int nFlagSample = DfxCountProfileSections(fname, section);

	if (nFlagSample == 0)
	{
		// the sample was not found (was commented)
		nError = ER_NO_SAMPLE;
		return -1;
	}

	m_strFileTitle = DfxGetProfileString(fname, section,"strFileTitle", "NoTitle");
	m_nSpecNum = DfxGetProfileInt(fname, section,"nSpecNum", 0);

	m_nSpecIncNum = 0;

	if(m_nSpecNum)
	{
		DfxGetProfileIntArray(fname, section,"SpecArray", m_nSpecNum, m_nSpectra);
		DfxGetProfileIntArray(fname, section,"SpecArInc", m_nSpecNum, m_nSpectraInc);

		for (int i = 0; i < m_nSpecNum; i++)
		{
			if (m_nSpectraInc[i] == 1)	m_nSpecIncNum++;
		}
	}

	int iOK = 1;

	if (m_strFileTitle == "NoTitle")
	{
		_snprintf(strErrorMessage, MAX_ERR_MSG, "strFileTitle was not found in sample %s", fname);

		nError = ER_APJ_NAME;
	}

	if (m_nSpecNum == 0)
	{
		_snprintf(strErrorMessage, MAX_ERR_MSG, "Number of spectra in sample %s equals 0", fname);

		nError = ER_APJ_SPEC;
	}

	return 0;
}


string CSampleArray::GetSampleFileName() const
{
	string str = m_strFileTitle; 
	str += ".saa"; 
	return str;
}


int CSampleArray::GetSpecInc(int nIdx) const
{
	int res;

	if ((nIdx < 0) || (nIdx >= m_nSpecNum))
	{
		res = -1;
	}
	else
	{
		res = m_nSpectraInc[nIdx];
	}

	return res;
}

int CSampleArray::GetSpecIndex(int nIdx) const
{
	int res;

	if ((nIdx < 0) || (nIdx >= m_nSpecNum))
	{
		res = -1;
	}
	else
	{
		res = m_nSpectra[nIdx];
	}

	return res;
}




