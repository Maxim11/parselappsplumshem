// ApjSampleLoader.h: interface for the CApjSampleLoaderLoader class.
//
// ��������:
// ����� CApjSampleLoader ������������ ��� ������ ��������
// �� ����� ������� ������� (apj-�����).
//
// ������������ ������� CSampleFromFile

#ifndef _APJSAMPLELOADER_H_
#define _APJSAMPLELOADER_H_

#include <string>

using namespace std;

const int NSAMPLE_SPEC_MAX = 32;


class CApjSampleLoader
{
public:
	CApjSampleLoader(const char* strFileName);
	int GetDfxProfile(const char *fname, const char *section);
	int GetSpecNum() const { return m_nSpecNum; }
	int GetError() const { return nError; }

	string GetSampleFileName();
	string GetRefDataFileName();
	string GetSpecFileName(int nIdx);
	string GetFileTitle();

protected:
	string m_strTextID;
	string m_strFileTitle;

	int m_nSpecNum;
	int m_nSpecNext;
	int m_nSpectra[NSAMPLE_SPEC_MAX];

	int nError;




enum SampleErrors{ ER_OK,
					ER_FILE_OPEN, // can not open sample file
					ER_APJ_NAME, // no title name specifyed in the sample file
					ER_APJ_SPEC }; // number of spectra in the sample = 0
};



#endif // _APJSAMPLELOADER_H_
