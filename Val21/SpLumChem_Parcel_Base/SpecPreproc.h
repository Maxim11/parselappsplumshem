#ifndef _SPECPREPROC_H_
#define _SPECPREPROC_H_

#include <string>

using namespace std;

enum SpecType { ABS, TRANS };

enum ModelType { PLS = 0, PCR, PCR1, MLR };


struct SpecPreproc
{
	SpecType kSpType; // spectrum type - transmittance or absorbance
	double fMinFreq;  // minimum frequency for calibration
	double fMaxFreq;  // maximum frequency for calibration
	string fParStr;   // preprocessing parameter string
	SpecPreproc(SpecType sType, double minFreq, double maxFreq, const string& parStr);
	SpecPreproc();
};

#endif // _SPECPREPROC_H_