//  SpLumChemStructs.h
#ifndef _STRUCTSENUMS_H_
#define _STRUCTSENUMS_H_

//#define _IMPORTING_

#ifndef _IMPORTING_
#include "newmat.h"
#endif

#include <string>

#include <vector>

//#include <valarray>

using namespace std;

typedef vector<double> VDbl; // ��� ��������
typedef vector<int> VInt; // ��� ��������
typedef vector<VDbl> MDbl; // ��� ������

// ���� ��������
const int STYPE_TRA = 0; // transmittance
const int STYPE_ABS = 1; // absorbance

// ���� ������������� ������� (���������)
const int MODEL_PLS = 0;
const int MODEL_PCR = 1;
const int MODEL_HSO = 2;
const int MODEL_PCA = 3;
const int MODEL_MLR = 4;


// ������������ ����� ��������� �� ������
const int MAX_ERR_MSG = 1024;

// ������ ��������� �� ������
extern char strErrorMessage[];


struct MODEL_MMP  // ����� ������
 {
  public:
   int Type;
   int garm;        // ����� �������� (��� �������� ������ garm = 0)
   double sizeGcub;  // ������ ���������
   double knl;     // ����������� ������������ (��� �������� ������ k_nl = 0)
   MODEL_MMP() {Type = 3;};
 };

struct CSpecPreproc
 {
  public:
	int nSpType; // ��� ������� (STYPE_TRA ��� STYPE_ABS - transmittance or absorbance)
//	double fMinFreq;  // ������ ������� ������������� ��������� ��� ���������� ����������
//	double fMaxFreq;  // ������� ������� ������������� ��������� ��� ���������� ����������
	string strPreprocPar; // ������ ���������� �������������
	string strPreprocParTrans; // ������ ���������� �������������, ��������������� ��� ��������� ��������
//    int numFreqExcluded; // ����� ����������� ������
    VInt vFreqExcluded; // ������ ��������� ����������� ����� ������� numFreqExcluded
                         //���� ������� ����������� ����� (������ � 0)
//	VDbl vCompConcCorrAdd; // ������ ���������� �������� � ������������� ��������� 
//	VDbl vCompConcCorrMul; // ������ ����������������� �������� � ������������� ��������� 
	vector<string> strCompName; // ������ �������� ��������� (�������) �������
 };

struct CFreqScale
 {
  public:
		double GetEndFreq() const { return fStartFreq + fStepFreq * (nNumberOfFreq - 1); } // end frequency
		void Save(FILE* fgh);
		void Load(FILE* fgh);
		void Print(const char* fileName) const;
        int nNumberOfFreq; // ����� ������ � �������
        double fStepFreq;  // ��� �� ������� [cm**(-1)]
        double fStartFreq; // ��������� ������� �������
 };


struct CSampleProxy
 {
  public:
        MDbl mSpectra; // ��������� ������ �������� (������ nNumberOfSpectra x frScale.nNumberOfFreq)
//        vector<string> strCompName; // ������ �������� ��������� (�������) �������
		VDbl vCompConc; // ������ ������������ ���������  
 };

struct CSpecPreprocData_Parcel
{
	VDbl meanSpectrum;// ������� ������ ����. ������
	VDbl meanSpectrumMSC; // ������� ������ ��� ����������������� ���������
	VDbl meanComponents;	// ������� ������������ ��������� ����. ������
	VDbl meanStdSpec;	// c�������. ���������� ��� �������� ����. ������
	VDbl meanStdComp;	// c�������. ���������� ��� 
	//VDbl meanComponentsTrans; // c�������. ���������� ��� ��������
	double fMax; // ������. �������� ���. ������ ��� ������������� H
};

struct CModel_Parcel
{
	int nModelType;
	MDbl mFactors;
	MDbl mScores;
	MDbl mCalibr;
	MDbl mYloadings; // Ncomp x Npc


	// HSO
	int numVar; // ����� ���������� ������
	MDbl mCalibrLinear; // ������������� ������� (�������� �����)
	MDbl mCalibrNonLinear; // ������������� ������� (���������� �����)
};


/*struct CModelHSO_Parcel
{
	int nModelType;	// ��� ������
	int numVar; // ����� ���������� ������

	MDbl mCalibrLinear; // ������������� ������� (�������� �����)
	MDbl mCalibrNonLinear; // ������������� ������� (���������� �����)

	MDbl mScores;	// ������� ������ (scores) PCA-������ (numSp x numFactors)
	MDbl mFactors; // ������� �������� PCA-������ (numFactors x numFreq)
};*/


struct CQnt_Parcel
{
	// ��������� �������������
	CSpecPreproc m_SpecPreproc;

	// ������ �������������
	CSpecPreprocData_Parcel m_specPreprocData;

	// ������ ������������� ��� ��������
	string strPreprocParTrans;

	// ������ ������������� ��� ��������
	CSpecPreprocData_Parcel m_specPreprocDataTrans;

	// ������ ������ (PCR, PLS, HSO, PCA)
	CModel_Parcel m_Model;

	// ������ ������ (HSO)
	//CModelHSO_Parcel m_ModelHSO;

	// ���������� �������
	VDbl vSEC;
	VDbl vR2Stat;
	VDbl vMahalanobisDistances;

	// ������� ���������� ������������
	double fMeanMahDist;
};

struct CQntOutput
{
	// ���������� �������
	// ��� ������� ����� [numComp]

	// SEC
	bool bSEC;
	VDbl vSEC;// ������ SEV
	VDbl vR2StatSEC;// ������ ��������� R2

	// SECV
	bool bSECV;
	VDbl vSECV; // ������ SEV
	VDbl vR2StatSECV; // ������ ��������� R2
	VDbl vFStatSECV; // ������ ��������� F

	// SEV
	bool bSEV;
	VDbl vSEV; // ������ SEV
	VDbl vErr;  // ������ ������� ������ (����������)
	VDbl vSDV;// ������ SDV
	VDbl vR2StatSEV; // ������ ��������� R2
};






#ifndef _IMPORTING_

#define nClbNewMethod 31 // virst verion of dll with new version

extern void AppendToDebugReport(const char* str);


enum nErrors {	ER_OK = 0,
				ER_PREDICT,
				ER_PCA,
				ER_PCR,
				ER_FREQ_RANGE,
				ER_MODEL_TYPE,
				ER_FILE_CREATION,
				ER_FILE_OPEN,
				ER_OUT_OF_RANGE,
				ER_NO_COMP,
				ER_BAD_QLT_FILE,
				ER_BAD_QLT_VER,
				ER_BAD_QNT_FILE,
				ER_BAD_QNT_VER,
				ER_POST_PROC,
				ER_DERIV1,
				ER_DERIV2,
				ER_ADD,
				ER_TRANS,
				ER_APJ_NAME,
				ER_APJ_SPEC,
				ER_PLS,
				ER_SEC,
				ER_PREPROC_SP,
				ER_PREPROC_COMP,
				ER_HQO};

struct CSpecPreprocData
{
public:
//	~CSpecPreprocData() { delete[] pFreqExcluded;}
	CSpecPreprocData() { fMax = 1; numberOfExcludedFreq = 0; }
	int nSpType;		// ��� ������� - ����������/�����������
	double fMinFreq;	// ������ ������� ���������� ���������
	double fMaxFreq;	// ������� ������� ���������� ���������
	string strPreprocPar;	// ������ ���������� �������������
	string strPreprocParTrans;	// ������ ���������� �������������, ���������������� ��� ��������
	int numberOfChannels;	// ����� ������ (�������) � ���������

	RowVector meanSpectrum;	// ������� ������ ����. ������
	RowVector meanSpectrumMSC;	// ������� ������ ��� ����������������� ��������
	RowVector meanComponents;	// ������� ������������ ��������� ����. ������
	RowVector meanComponentsTrans;	// ������� ������������ ��������� ����. ������ ��� ������������� ��������
	RowVector meanStdSpec;	// c�������. ���������� ��� �������� ����. ������
	RowVector meanStdComp;	// c�������. ���������� ��� 
							// ������������ ��������� ����. ������
//	RowVector vComponentCorrectionsAdd; // �������� � ������������ ���������
//	RowVector vComponentCorrectionsMul; // �������� � ������������ ���������



//    VInt vFreqExcluded; // ������ ��������� ����������� ����� ������� numFreqExcluded
//                         //0-����� �� ������������;1-������������
    VInt vUseFreq; // ������ ��������� ������������ ������ ������� numberOfChannels
					// 1 - ������������, 0 - �� ������������ (���������) 

	int numberOfExcludedFreq; // ����� ����������� ������

	int nModelType;


//	bool bCompCorr;			// ���� ������� ��� ��������� ��������
							// � ������������� ���������

//	int numberOfFreqExcluded;	// ����� ����������� ������
//	int *pFreqExcluded; // ������ �������� ����������� ������

	double fMax; // ������. �������� ���. ������ ��� ������������� H

	// ��������� ������ ��������� � ����������� ����������
	int LoadFromQnt_Parcel(const CFreqScale *pFreqScale, const CQnt_Parcel *pQnt);
//	int LoadTransFromQnt_Parcel(const CFreqScale *pFreqScale, const CQnt_Parcel *pQnt);
	int Load(const CFreqScale *pFreqScale, string strPar, const CSpecPreprocData_Parcel *pPreprocData);



//	void Save(FILE* fgh, int clbVersion);
//	void Load(FILE* fgh, int clbVersion);
	void Copy(const CSpecPreprocData &theData);
	void Print(const char* fileName) const;
	void Clear();
};

struct COptimizationFlags
{
	bool bSEC;
	bool bSEV;
	bool bSECV;
	COptimizationFlags() { bSEC=bSEV=bSECV=false; }
};







#endif // _IMPORTING_


#endif // _STRUCTSENUMS_H_
