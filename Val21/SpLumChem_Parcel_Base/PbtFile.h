// PbtFile.h: interface for the CPbt class.
//
// ��������:
// ����� CPbt ������������ ��� ������ � ������� ��������
// ���������� (pbt-�����).
//
// ������������ ������� CSamples


#ifndef _PBTFILE_H_
#define _PBTFILE_H_

#include <string>
#include "CorrectData.h"
#include "SpLumChemStructs.h"
#include "newmat.h"

using namespace std;

class CPbt
{
public:
	CPbt();
	CPbt(const char *fname);
	~CPbt();
	int Load(const char *fname);
	int GetError() const { return nError; }
	int GetSpType() const { return nSpType; }
	const char* GetPreprocStr() const { return strPreprocPar.c_str(); }
	const char* GetPrefix() const { return strPrefix.c_str(); }
	int CorrectSpectra(Matrix &mSp) { return m_Correct.CorrectSpectra(mSp); }
	double GetStartFreq() { return m_FreqScale.fStartFreq; }
	double GetStepFreq() { return m_FreqScale.fStepFreq; }
	double GetStopFreq() { return m_FreqScale.fStartFreq + m_FreqScale.fStepFreq * (m_FreqScale.nNumberOfFreq - 1); }
	int GetNumFreq() { return m_FreqScale.nNumberOfFreq; }

	RowVector vMeanSpectrumMSC; // ������� ������ ��� ����������������� ��������
	RowVector vMeanStdSpec;		// c�������. ���������� ��� �������� ������ ��� ��������
	RowVector vMeanSpectrum;	// ������� ������ ������ ��� ��������
	CCorrectData m_Correct;		// ���������, ���������� ������ ��� ��������� ��������

private:
	void Init();

	int nError;				// ��� ������
	CFreqScale m_FreqScale;	// ��������� ����� ��������
	int numberOfComponents; // ����� ���������
	string* pstrComponentName;	// ������ ���� ���������
	int nSpType;				// ��� ������� - ����������/�����������
	string strPreprocPar;		// ������ �������������
	string strPrefix;			// ������� � �������� ������ �������� 
								// ��������������� ������
};

#endif // _PBTFILE_H_