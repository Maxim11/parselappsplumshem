#ifndef _PCA_H_
#define _PCA_H_

#include "newmat.h"
void Housholder(const SymmetricMatrix& A, DiagonalMatrix& D,
   DiagonalMatrix& E, Matrix& Q);

int FindEigenValues(const DiagonalMatrix& mD, const DiagonalMatrix& mOffD,
					 int k, DiagonalMatrix &mEigen);

int FindEigenVectors(const DiagonalMatrix& mD, const DiagonalMatrix& mOffD,
					 const DiagonalMatrix &mEigen, Matrix & mEigenVectors);

//int PCA_New(const Matrix& mX, int numFact);

//int PCA_Simple(const Matrix& mX, int numFact);

//int PCA_VCM(const Matrix& mX, int numberOfFactors);

#endif //  _PCA_H_