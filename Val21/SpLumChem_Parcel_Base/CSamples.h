// CSamples.h: interface for the CSamples class.
//
// ��������:
// ����� CSamples ������������ ��� ������
// � ������� ��������
//
// ������������ �������� CQntCalibration, CQltCalibration � CSecv

#ifndef _CSAMPLES_H_
#define _CSAMPLES_H_

#include <string>
#include "newmat.h"
#include <stdio.h>

using namespace std;

#include "SpLumChemStructs.h"
#include "TransData.h"


extern void AppendToDebugReport(const char* str);

class CSamples
{
public:
	int PreprocessSpectra4Transfer(void *pPbt, bool bExtSpectra);
	int PreprocessSpectra4Transfer(const CSpecPreprocData &thePreprocData);

//	CSamples(int numSamples, // number of samples
//					const CSampleProxy* pSamples); // array of samples

	CSamples(vector<CSampleProxy> *pSamples, const CFreqScale *frScale,  const vector<string> *strCompName); // array of samples


//	void LoadOldLst(const char* strLstFile, bool flagExt = false);

//	void LoadNewLst(const char* strLstFile, bool flagExt = false);

//	CSamples(const char* strLstFile, bool flagExt = false);

	void AddSamples(CSamples &theAddedSamples); // array of samples

	int ExcludeFrequencies(const CSpecPreprocData & thePreprocData);

	CSamples();
	~CSamples();
	int GetNumSamples() const { return numberOfSamples; }
	int GetNumSpectra() const { return numberOfSpectra; } 
	int GetNumComponents() const { return numberOfComponents; } 
	int GetError() const { return nError; }
	void GetSampleSpectra(Matrix& mSampleSpectra, int iS);
	int GetSampleSpectraIndexes(int iS) const { return pSampleSpectraIndexes[iS]; }
	int GetSampleSpectraNumbers(int iS) const { return pSampleSpectraNumbers[iS]; }
	double GetStartFreq() const { return freqScale.fStartFreq; }
	double GetStepFreq() const { return freqScale.fStepFreq; }
	int GetNumFreq() const { return freqScale.nNumberOfFreq; }
	int GetTransFlag() const { return nFlagTrans; }
	bool IsFreqExcluded() const { return bFreqExcluded; }

//	const char* GetSampleName(int nSample);
//	const char* GetSampleSpecName(int nSample, int nSpec);
	const char* GetComponentName(int nComponent);

	int PreprocessSpectra(CSpecPreprocData& specPreprocData, Matrix& mX) const;
	int PreprocessSpectra(CSpecPreprocData& specPreprocData, CTransData *pTransData, Matrix& mX) const;

	int PreprocessSpectra4Prediction(const CSpecPreprocData &specPreprocData, Matrix &mSpPrep);

	int ExcludeSample(const CSpecPreprocData& specPreprocData, Matrix& mX, int iS) const;
	int PreprocessSpectra(CSpecPreprocData& specPreprocData, Matrix& mX, int iS) const;
	int PreprocessComponents(CSpecPreprocData& specPreprocData, Matrix& mY);
	int PreprocessComponents(CSpecPreprocData& specPreprocData, Matrix& mY, int iS);

	int FindComponentIndex(const string& compName);
	void GetMComponents(Matrix& mComp, bool bAverage = false) const;

	void GetMSpectra(Matrix& mSp, bool bAverage = false) const;
	int GetSpectrum(int nSample, int nSpec, RowVector& vSpectrum) const;
	int GetMeanSpectrum(int nSample, RowVector& vSpectrum) const;



	void GetStd2Components(RowVector& vStd2) const;
	void Save(FILE *fgh, int clbVersion);
	void Load(FILE *fgh, int clbVersion);
	void CopySamples(CSamples& theSamples);
	void GetRefConc(int nSample, // sample order number
					double* compConc); // array of ref. data of this sample

	void GetRefConc(int nSample, // sample order number
					VDbl *pvCompConc); // array of ref. data of this sample


	void SetPreprocDataConst(const CSpecPreprocData &theData);

	bool IsSampleAdded(int iS);

	void GetPreprocDataConst(CSpecPreprocData& m_PreprocDataConst) const;

	CSpecPreprocData thePreprocDataConst; // ������ ���������� �������������,
										// ������������ � �������� ��� ��������

	CFreqScale freqScale; // ��������� ����� �������� ��������
							// ���������� ��� ���� ��������!


protected:
	int nError;				// ��� ������
	int numberOfComponents;	// ����� ���������
	int numberOfSamples;	// ����� ��������
	int numberOfSpectra;	// ������ ����� ��������

	int* pSampleSpectraIndexes; // array [numberOfSamples] of first spectrum indexes (zero based)
								// of given sample
	int* pSampleSpectraNumbers; // array [numberOfSamples] of spectra number in
								// given sample

	Matrix mSpectra;			// ������� ��������
	Matrix mComponents;			// ������� ������������ ���������

//	string* pstrSampleName; // samples names
//	string* pstrSpectrumName; // spectra names
	string* pstrComponentName; // component names


	// added 27.09.2004
	// for adding of transfered calibration
	int numberOfAddedSamples;	// ����� ����������� ��������
	int numberOfAddedSpectra;	// ����� ����������� ��������
	int nFlagTrans;				// ���� ��������
	bool bFreqExcluded; // ���� ���������� ������


private:
	void Init();
};

#endif // _CSAMPLES_H_