// QltCalibration.h: interface for the CQltCalibration class.
//
// ��������:
// ����� CQntCalibration - ����� �������������� �����������
// ������������ �������� �������������� �����������,
// ������ ���. ����������� � qnt-����, ������
// ���. ����������� �� qnt-�����, � ������������ ��������
// (��� ������������ ��� � ���������� ������������).


#ifndef _QNTCALIBRATION_H_
#define _QNTCALIBRATION_H_


#include <string>
#include "newmat.h"
#include "model.h"
#include "CSamples.h"
#include "TransData.h"
#include "ModelHQO.h"

using namespace std;

#include "SpLumChemStructs.h"


class CQntCalibration
{
public:
	CQntCalibration(vector<CSampleProxy> *pSamples, // array of samples
					const CFreqScale *FreqScale, // ��������� ����� ��������
					const CSpecPreproc *prePar, // preprocessing parameters
					int nModelType,
					int numFactors,
					CQnt_Parcel **pQnt);

/*	CQntCalibration(void* pSamples,
					const CSpecPreproc &prePar,
					int nModelType,
					int numFactors);*/

/*	CQntCalibration(int numSamples,
					const CSampleProxy* pSamples,
					const CSpecPreproc &prePar,
					int nModelType,
					int numFactors,
					const char *strPbtName);*/

	CQntCalibration(vector<CSampleProxy> *pSamples, // array of samples
					const CFreqScale *FreqScale, // ��������� ����� ��������
					const CSpecPreproc *prePar, // preprocessing parameters
					const MODEL_MMP *pModelPar,
					//int numPCAfactors,
					CQnt_Parcel **pQntOut); // created model


	CQntCalibration(vector<CSampleProxy> *pSamples, // array of samples
					const CFreqScale *FreqScale, // ��������� ����� ��������
					const CQnt_Parcel *pQnt); //  ��������� ����� ������

	// ��� �����������
	CQntCalibration(vector<CSampleProxy> *pSamples, // array of samples
		const CFreqScale *FreqScale, // ��������� ����� ��������
		const CSpecPreproc *prePar, // preprocessing parameters
		const CQntOutput *pQntOutput); // �������� (��� ����������)

	// ��� �����������
	int SECV(int numFactors);
	int SECV(); // ��� HQO ������

	int LoadSamples4SEV(vector<CSampleProxy> *pSamples); // array of samples 

	int SetModelType(int nModelType); // calibartion model type 

	int SetAverage(bool bAver); // Averaging spectra over samples 

	int SetSpType(int nSpType); // ��� ������� (STYPE_TRA ��� STYPE_ABS - transmittance or absorbance) 

	int SetFreqRange(double fMin, double fMax); // ��������� �������� 

	int SetPreproc(string strPreprocPar); // ������ ���������� �������������

	int SetFactors(int numFactors);  // number of factors 

	int SetHarmonics(int numHarmonics);  // number of harmonics 

	int SetCnl(double fCnl);  // coefficient of non linearity 

	int SetShc(double fShc);  // size of hypercube 

	int GetResults(CQntOutput *pQntOutput);  // number of factors


	int PredictSamples(vector<CSampleProxy> *pSamples, // array of samples
					const CFreqScale *FreqScale, // ��������� ����� ��������
					MDbl *pmCompConc, // ������� ������������� ������������
					VDbl *pvSEV, // ������ SEV [numComp]
					VDbl *pvE,  // ������ ������� ������ (����������) [numComp]
					VDbl *pvSDV,// ������ SDV [numComp]
					VDbl *pvR2); // ������ R2 [numComp]

//	int PredictSEVSamples(VDbl *pvSEV, // ������ SEV [numComp]
//					VDbl *pvE,  // ������ ������� ������ (����������) [numComp]
//					VDbl *pvSDV,// ������ SDV [numComp]
//					VDbl *pvR2); // ������ R2 [numComp]

	int PredictSEVSamples();


//	CQntCalibration(const char* fileName);
	~CQntCalibration();

	int GetNumSamples() const { return pClbSamples->GetNumSamples(); }
	int GetNumVariables() const { return pModel->GetNumVariables(); }
	int GetNumComponents() const { return pClbSamples->GetNumComponents(); } 
	int GetNumFreedomDegrees() const { return numberOfFreedomDegrees; }
	int GetModelType() const { return pModel->GetModelType(); }

	int GetError() const { return nError; }
	void GetClbSamples(CSamples* pSamples) const;
	void GetSpecPreprocData(CSpecPreprocData& theSpecPreprocData) const;
	void GetModelPar(MODEL_MMP& theModelPar) const;
	void GetRefConc(int nSample,  VDbl *pvCompConc);

//	int Load(const char* fileName);
//	int Save(const char* fileName);

	int GetSEC(string compName, double* sec);
	int GetR2Stat(string sCompName, double* R2Stat);

	int PredictSpectrum(const CFreqScale *dsc,
						const VDbl *vSp,
						VDbl *pvCompConc);

	int PredictPreprocSpectra(const Matrix& mPreprocSpectra,
						Matrix& mComp);

//	int PredictSpectrum(int nSample,
//						int nSpec,
//						double* compConc);

	int PredictSpectrum(int nSample,
						int nSpec,
						VDbl *pvCompConc);


	int PredictSample(int nSample,
						VDbl *pvCompConc);


	int GetConcentration(string sCompName,
					double* conc,
					double* concErr);

//	const char* GetSampleName(int nSample);
	const char* GetComponentName(int nComponent);
//	const char* GetSampleSpecName(int nSample, int nSpec);

	int CalculateSEC();
	int CalculateMahDist();
	int CalculateSEV(void* pSamples, double* pSEV);


/*	void AddSamples(int numSamples,
					const CSampleProxy* pSamples,
					const CSpecPreproc &prePar,
					int nModelType,
					int numFactors = 0);*/

	void Transfer(CTransData& theTransData);

	double GetSampleDistance(int nSample);

	double GetSampleNormDistance(int nSample);

	void GetSampleDistances(int nSample, double* dist);

	void GetSampleNormDistances(int nSample, double* dist);

	void GetSampleSpecDistance(int nSample, int nSpec, double* dist);

	int GetNumSampleSpectra(int nSample) const;

	double GetMahalanobisDistance(int i);

	double CalculateMahalanobisDistance(const CFreqScale *dsc,
						const VDbl *vSp);

	int CalculateMahalanobisDistances(const Matrix& mX,
						ColumnVector& vMah);

	double CalculateNormMahalanobisDistance(const CFreqScale *dsc,
						const VDbl *vSp);

	int CalculateNormMahalanobisDistances(const Matrix& mX,
						ColumnVector& vMah);

	int OutputScores(const char *fname);

	void GetTransData(CTransData& theTransData) const;

	int GetTransType() const { return pTransData->nTransType; }

	int GetMaxClbFreqRange(double *fMinFreq, double *fMaxFreq);

	void GetUseFreq(int *pUseFreq) const;

	void FillQnt(CQnt_Parcel *pQnt);


	Matrix mX;		// ������� ����������������
					// �������� ��������������� ������
	Matrix mY;		// ������� ����������������
					// ��������� ��������������� ������

	CSpecPreprocData specPreprocData;	// ������ �������������
	CSpecPreprocData specPreprocDataTrans;	// ������ ������������� ��������
	CFreqScale freqScale;				// ��������� ����� ��������
	CTransData* pTransData;				// ������ �� �������� �����������
	CQnt_Parcel *pQnt; // ����������� ����������
//	CQnt_ParcelHSO *pQntHSO; // ����������� ���������� � HSO �������


	// for optimisation
	Matrix mSpectraABSTRA; // 
	Matrix mSpectraABSTRAcut; //
	Matrix mGarm; //


protected:
	int nError;					// ��� ������
	int numberOfFreedomDegrees;	// ����� �������� �������

	ColumnVector vSEC;			// ����������� ������ ���������� ��� ���������
	ColumnVector vR2Stat;		// �������� ���������� R^2 ��� ���������

	ColumnVector vSECV;			// ����������� ������ ���������� ��� ���������
	ColumnVector vR2StatSECV;		// �������� ���������� R^2 ��� ���������
	ColumnVector vFStat;		// �������� ���������� R^2 ��� ���������

	ColumnVector vSEV; // ������ SEV [numComp]
	ColumnVector vErr;  // ������ ������� ������ (����������) [numComp]
	ColumnVector vSDV;// ������ SDV [numComp]
	ColumnVector vR2StatSEV; // ������ R2 [numComp]


	CSamples* pClbSamples;		// �������������� �������
	CSamples* pSEVSamples;		// ������� ��� ��������� (��� �����������)

	Matrix mCalibr;				// ������������� �������
	Matrix mComponentsPredicted;	// ������� ������������� ���������

	CModel *pModel; // ������������� ������
//	CModelHQO *pModelHQO; // ������������� ������ HQO
	int *pUseFreq;// ������ ������������ ������ ��� HQO ��� �����������

	ColumnVector vMahalanobisDistances; // ���������� ������������
						// ��� �������� ��������������� ������

	double fMeanMahDist; // ������� ���������� ������������
						// ��� �������� ��������������� ������

	bool flagBatch;		// ���� ��������� ������� ����������
						// ����������� � ��������� Envelope4Samples

	int Calibrate(const CSpecPreproc *prePar,
					int nModelType,
					int numFactors);

	int Calibrate(const CSpecPreproc *prePar,
					const MODEL_MMP *pModelPar);

//	int Calibrate(const CSpecPreproc *prePar,
//					const MODEL_MMP *pModelPar,
//					int numPCAfactors);

	// for SECV in optimization
	int PreprocessSpectraSECV(CSpecPreprocData& specPreprocDataSECV, Matrix& mX, int iS);
	int ExcludeSampleSECV(CSpecPreprocData& specPreprocDataSECV, Matrix& mX, int iS);

	void GetSampleSpectra(Matrix& mSampleSpectra, int iS);

	COptimizationFlags mOptFlags;

//	CModelPCA *pModelPCA;		// PCA ������ ��� HSO

private:
	void Init();
};






#endif // _QNTCALIBRATION_H_