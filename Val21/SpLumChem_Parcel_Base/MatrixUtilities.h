// Различные матричные утилиты


#ifndef _MATRIXUTILITIES_H_
#define _MATRIXUTILITIES_H_

#include "newmat.h"
#include <stdio.h>
//#include <string>
#include "SpLumChemStructs.h"

using namespace std;


void SaveColumnVector(const ColumnVector& vColumn, FILE* fgh);

void SaveRowVector(const RowVector& vRow, FILE* fgh);

void SaveMatrix(const Matrix& mX, FILE* fgh);

int LoadColumnVector(ColumnVector& vColumn, FILE* fgh);

int LoadRowVector(RowVector& vRow, FILE* fgh);

int LoadRowVector(RowVector& vRow, int nSize, FILE* fgh);

void LoadMatrix(Matrix& mX, FILE* fgh);

void ReadMatrix(Matrix& mX, FILE* fgh);

void PrintDiagonalMatrix(const DiagonalMatrix& mX, const char* fname);


//void SaveString(const string& str, FILE* fgh);

//void LoadStringMy(string& str, FILE* fgh);

//void LoadStringPbt(string& str, FILE* fgh);

void PrintMatrix(const Matrix& mX, const char* fname);

//void PrintMatrix(const MDbl& mX, const char* fname);

void PrintMatrix(const Matrix& mX, FILE* fgh);

void CalculateMean(const Matrix& mX, RowVector& vMean);

void CalculateStd2(const Matrix& mX, RowVector& vStd2);

int Ordinary_LS (const Matrix &Ym, const Matrix &Ys, RowVector &Slope, RowVector &Bias);

Matrix CorrectMatrix(const Matrix &Ys, const RowVector &Slope, const RowVector &Bias);

void NormMax(Matrix &mX, double &fMax);

VDbl Row2VDbl(const RowVector &vRow);

VDbl Column2VDbl(const ColumnVector &vRow);

MDbl Matrix2MDbl(const Matrix &mMatrix);

// another version of these functions
void Row2VDbl(const RowVector &vRow, VDbl &vRes);

void Column2VDbl(const ColumnVector &vRow, VDbl &vRes);

int Column2VDbl(const ColumnVector &vColumn, VDbl *vRes);

void Matrix2MDblResize(const Matrix &mMatrix, MDbl &mRes);

int Matrix2MDbl(const Matrix &mMatrix, MDbl *mRes);

RowVector VDbl2Row(const VDbl &vRow);

ColumnVector VDbl2Column(const VDbl &vRow);


Matrix MDbl2Matrix(const MDbl &mMatrix);

void Print(const char *strfname, const CQnt_Parcel *pQnt);

void GetTW(int k, int i, const Matrix &mW, const Matrix &mP, ColumnVector &vTW);





#endif // _MATRIXUTILITIES_H_
