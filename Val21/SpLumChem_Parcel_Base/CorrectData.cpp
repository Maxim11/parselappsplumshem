#include "CorrectData.h"
#include "MatrixUtilities.h"
//#include "SpLumChemStructs.h"

CCorrectData::CCorrectData()
{
	nCorrectType = 0; // non corrected
}

void CCorrectData::Save(FILE* fgh, int clbVersion)
{
//	if (clbVersion <= 14)
//	{
//		nCorrectType = 0;
//		return;
//	}

	fwrite(&nCorrectType, sizeof(int), 1, fgh);

	switch(nCorrectType)
	{
	case 0:
		break;
	case 1:
		SaveRowVector(vBiasSp, fgh);
		break;
	case 2:
		SaveRowVector(vBiasSp, fgh);
		SaveRowVector(vSlopeSp, fgh);
		break;
	case 3:
		SaveRowVector(vBiasSp, fgh);
		SaveRowVector(vSlopeSp, fgh);
		SaveRowVector(vQuadrSp, fgh);
		break;
	}
}


void CCorrectData::Load(FILE* fgh, int nSize)
{
//	if (clbVersion <= 14)
//	{
//		nCorrectType = 0;
//
//		return;
//	}

	fread(&nCorrectType, sizeof(int), 1, fgh);

	switch(nCorrectType)
	{
	case 0:
		break;
	case 1:
		LoadRowVector(vBiasSp, nSize, fgh);
		break;
	case 2:
		LoadRowVector(vBiasSp, nSize, fgh);
		LoadRowVector(vSlopeSp, nSize, fgh);
		break;
	case 3:
		LoadRowVector(vBiasSp, nSize, fgh);
		LoadRowVector(vSlopeSp, nSize, fgh);
		LoadRowVector(vQuadrSp, nSize, fgh);
		break;
	}
	// read components
}

int CCorrectData::CorrectSpectra(Matrix& mSpectra)
{
	if (nCorrectType == 0) 	return 0;

	int numFreq = vBiasSp.Ncols();

	if (numFreq != mSpectra.Ncols())
	{
		return -1;
	}

	int iC;

	switch(nCorrectType)
	{
	case 1:
		for (iC = 1; iC <= numFreq; iC++)
		{
			mSpectra.Column(iC) += vBiasSp(iC);
		}
		break;
	case 2:
		for (iC = 1; iC <= numFreq; iC++)
		{
			mSpectra.Column(iC) *= vSlopeSp(iC);
			mSpectra.Column(iC) += vBiasSp(iC);
		}
		break;
	case 3:
		int numSp = mSpectra.Nrows();

		for (iC = 1; iC <= numFreq; iC++)
		{
			double a, b, c;
			a = vBiasSp(iC);
			b = vSlopeSp(iC);
			c = vQuadrSp(iC);

			for (int iSp = 1; iSp <= numSp; iSp++)
			{
				double x = mSpectra(iSp, iC);
				mSpectra(iSp, iC) = a + (b + c * x) * x;
			}
		}
		break;
	}

	return 0;
}
