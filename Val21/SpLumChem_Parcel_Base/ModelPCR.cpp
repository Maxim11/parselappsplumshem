#include "ModelPCR.h"
#include "SpLumChemStructs.h"
#include "ModelPCA.h"
#include <math.h>
#include "MatrixUtilities.h"
//#include "Debug.h"



CModelPCR::CModelPCR(const Matrix& mX, const Matrix& mY, int nFactors)
{
#ifdef _DEBUG_
		AppendToDebugReport("Start Model PCR");
#endif

	nModelType = MODEL_PCR;
	nError = ER_OK;

	if (nFactors <= 0)
	{
		numFactors = __min(mX.Nrows(), mX.Ncols());
	}
	else
	{
		numFactors = nFactors;
	}

	CModelPCA* pModelPCA = 0;

	pModelPCA = new CModelPCA(mX, numFactors);

	if ((nError = pModelPCA->GetError()) != ER_OK)
	{
#ifdef _DEBUG_
		AppendToDebugReport("Error in Model PCA");
#endif
		return;
	}

#ifdef _DEBUG_
		AppendToDebugReport("PCA was created");
#endif

	// �������� ����� �������� �������:
	numFactors = pModelPCA->GetNumFactors();

	pModelPCA->GetMatrixScores(mScores);
	pModelPCA->GetMatrixFactors(mFactors);
	pModelPCA->GetMatrixEigen(mEigenInv);

	delete pModelPCA;

	/*mEigenInv.ReSize(numFactors);

	for (int i = 1; i <= numFactors; i++)
	{
		mEigenInv(i) = 1.0 / (mFactors.Row(i)).SumSquare();
	}*/

	// ��� ������������� ���� PCA-���������
	// ������� mScores �����������, �.�.
	// mScores.Column(i).SumSqure() = 1

	mCalibr = mScores.t() * mY; // numFactors x numComponents
	mYloadings = mCalibr.t(); //  numComponents x numFactors

#ifdef _DEBUG_
	PrintMatrix(mCalibr.t(), "mCalibrT.dat");
	PrintMatrix(mFactors.t(), "mFactorsT.dat");
#endif

}

CModelPCR::CModelPCR(const Matrix& mX, const Matrix& mY, int nFactors, const Matrix& mScoresGuess)
{
	nModelType = MODEL_PCR;
	nError = ER_OK;

	if (nFactors <= 0)
	{
		numFactors = __min(mX.Nrows(), mX.Ncols());
	}
	else
	{
		numFactors = nFactors;
	}

	CModelPCA* pModelPCA = 0;

	pModelPCA = new CModelPCA(mX, numFactors);//, mScoresGuess);

	if ((nError = pModelPCA->GetError()) != ER_OK) return;

	// �������� ����� �������� �������:
	numFactors = pModelPCA->GetNumFactors();

	pModelPCA->GetMatrixScores(mScores);
	pModelPCA->GetMatrixFactors(mFactors);
	pModelPCA->GetMatrixEigen(mEigenInv);

	delete pModelPCA;

///mEigenInv.ReSize(numFactors);

//	for (int i = 1; i <= numFactors; i++)
//	{
//		mEigenInv(i) = 1.0 / (mFactors.Row(i)).SumSquare();
//	}

	// ��� ������������� ���� PCA-���������
	// ������� mScores �����������, �.�.
	// mScores.Column(i).SumSqure() = 1

	mCalibr = mScores.t() * mY; // numFactors x numComponents

}

// � ������� ����������� PCR-������ ��������� ������������
// ������������ ��������� ��� ������� �������� (����������������)
void CModelPCR::Predict(const Matrix& mX, Matrix& mYpredicted)
{
	if (mX.Ncols() != mFactors.Ncols())
	{
		nError = ER_PCR;
		return;
	}

	// ������� ��������� ����� (scores) ������� mX, �.�.
	// ���������� mX �� mFactors

	Matrix mXScores;

	mXScores = mX * (mFactors.t() * mEigenInv); // mX.Ncols x numFactors

	mYpredicted = mXScores * mCalibr; // mX.Ncols x numComponents
}

// �� ����������� PCR-������ ��������� ���������� ������������
// ��� ������ ������� �������� (������� ��������������)
void CModelPCR::CalculateMahalanobisDistance(const Matrix& mSp,
											 ColumnVector& vMahDist)
{
	if (mSp.Ncols() != mFactors.Ncols())
	{
		nError = ER_PCR;
		return;
	}

	// ������� ��������� ����� (scores) ������� mSp, �.�.
	// ���������� mX �� mFactors

	Matrix mSpScores;

	mSpScores = mSp * (mFactors.t() * mEigenInv); // numFactors

	int nSp = mSp.Nrows();

	vMahDist.ReSize(nSp);

	for (int i = 1; i <= nSp; i++)
	{
		vMahDist(i) = sqrt((mSpScores.Row(i)).SumSquare());
	}
}


// ��������� ���������� ������������ ��� ��������������
// �������� �� ������� ��������� PCA-������

void CModelPCR::GetMahalanobisDistance(ColumnVector& vMahDist) const
{
	int nSp = mScores.Nrows();

	vMahDist.ReSize(nSp);

	for (int i = 1; i <= nSp; i++)
	{
		vMahDist(i) = sqrt((mScores.Row(i)).SumSquare());
	}
}


void CModelPCR::Save(FILE* fgh, int clbVersion) const
{
	fwrite(&nModelType, sizeof(int), 1, fgh);
	SaveMatrix(mFactors, fgh);
	SaveMatrix(mScores, fgh);
	SaveMatrix(mCalibr, fgh);
}

void CModelPCR::Load(FILE* fgh, int clbVersion)
{
	LoadMatrix(mFactors, fgh);
	LoadMatrix(mScores, fgh);
	LoadMatrix(mCalibr, fgh);

	numFactors = mFactors.Nrows();

	mEigenInv.ReSize(numFactors);

	for (int i = 1; i <= numFactors; i++)
	{
		mEigenInv(i) = 1.0 / (mFactors.Row(i)).SumSquare();
	}
}

void CModelPCR::FillModel_Parcel(CModel_Parcel& mModel) const
{
	mModel.nModelType = nModelType;
//	mModel.mCalibr = Matrix2MDbl(mCalibr);
//	mModel.mFactors = Matrix2MDbl(mFactors);
//	mModel.mScores = Matrix2MDbl(mScores);
	Matrix2MDblResize(mCalibr, mModel.mCalibr);
	Matrix2MDblResize(mFactors, mModel.mFactors);
	Matrix2MDblResize(mScores, mModel.mScores);
	Matrix2MDblResize(mYloadings, mModel.mYloadings);
}

void CModelPCR::LoadFromQnt_Parcel(const CModel_Parcel& mModel)
{
	nModelType = mModel.nModelType;
	mCalibr = MDbl2Matrix(mModel.mCalibr);
	mFactors = MDbl2Matrix(mModel.mFactors);
	mScores = MDbl2Matrix(mModel.mScores);
}


