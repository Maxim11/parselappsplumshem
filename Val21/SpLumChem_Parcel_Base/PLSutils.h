// ������� ��� PLS-������

#ifndef _PLSUTILS_H_
#define _PLSUTILS_H_

#include "newmat.h"

bool CheckDescending(const DiagonalMatrix& D);

int SortDescending(DiagonalMatrix& D, Matrix& E);

#endif // _PLSUTILS_H_