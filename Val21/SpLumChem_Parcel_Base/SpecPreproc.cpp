#include "SpecPreproc.h"

SpecPreproc::SpecPreproc(SpecType sType,
						 double minFreq,
						 double maxFreq,
						 const string& parStr)
{
	kSpType = sType;
	fMinFreq = minFreq;
	fMaxFreq = maxFreq;
	fParStr = parStr;
}

SpecPreproc::SpecPreproc()
{
	SpecPreproc(TRANS, 0.0, 0.0, "");
}