// ModelPCA.h: interface for the CModelPCA class.
//
// ��������:
// ����� CModelPCA - ����� ��� ������ �������������� ��������� (PCA-������)
// 
//
// ������������ �������� CQntCalibration � CQltCalibration
//


#ifndef _MODELPCA_H_
#define _MODELPCA_H_

#include "newmat.h"
#include <stdio.h>
#include "SpLumChemStructs.h"

class CModelPCA
{
public:
	CModelPCA(const Matrix& mX, int numFactors = 10);
	//CModelPCA(const Matrix& mX, int numFactors, const Matrix &mScoresGuess);


	CModelPCA();
	~CModelPCA() { }

	int GetError() const { return nError; }
	int GetNumFactors() const { return numberOfFactors; }
	void GetMatrixFactors(Matrix& mF) const { mF = mFactors; }
	void GetMatrixScores(Matrix& mS) const { mS = mScores; }
	void GetMatrixEigen(DiagonalMatrix& mE) const { mE = mEigenInv; }

	void CalculateMahalanobisDistance(const Matrix& mSp, ColumnVector& vMahDist);
	void GetMahalanobisDistance(ColumnVector& vMahDist) const;
	void FillModel_Parcel(CModel_Parcel& mModel) const;

	void Save(FILE* fgh, int clbVersion);
	void Load(FILE* fgh, int clbVersion);

	Matrix mScores;		// ������� ������ (scores) (numSp x numFactors)
	Matrix mFactors;	// ������� �������� (numFactors x numFreq)


protected:
	void PCAfactors(const Matrix& mX);
//	void PCAfactors(const Matrix& mX, const Matrix &mScoresGuess);
	int PCAeigen_G_VCM(const Matrix& mX);
	int PCA_VCM(const Matrix &mX, int numberOfFactors);
	int PCA_Simple(const Matrix &mX, int numberOfFactors);
	//int PCA_New(const Matrix& mX, int numberOfFactors);

	int nError;				// ��� ������
	int numberOfFactors;	// ����� ��������
	int nModelType;	// ��� ������ (= MODEL_PCA)


	DiagonalMatrix mEigenInv; // �������, �������� � �������
								// ����������� �������� (numFactors)
};

#endif // _MODELPCA_H_
