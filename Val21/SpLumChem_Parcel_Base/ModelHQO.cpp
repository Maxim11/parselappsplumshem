//---------------------------------------------------------------------------
//#pragma hdrstop
#include <stdio.h >
#include <math.h>
#include "Model.h"
#include "newmat.h"
#include "ModelHQO.h"
#include "HSOMatrixUtils.h"
#include "MatrixUtilities.h"
#include "GarmTransform.h"
//#include <time.h>

//bool bPrint = true;

#define _DEBUG_

//---------------------------------------------------------------------------
//#pragma package(smart_init)
//------------------------------------------------------------------------------

CModelHQO::CModelHQO()
{
	nError = ER_OK;
	numComp = 0;
	numFreq = 0;
	numFreqExcluded = 0;
	numSp = 0;
	useFreq = 0;

	parModel.garm = 0;
	parModel.knl = 0;
	parModel.sizeGcub = 0;
}


//������� � 1 ����������� ����������
CModelHQO::CModelHQO(const Matrix& mX, const Matrix& mY, const int *pUseFreq, const MODEL_MMP *pModelPar)
{
	// mX [numSp x numFreq]
	// mY [numSp x numComp]
	nError = ER_OK;
	useFreq = 0;

	SetFrequencies(mX.Ncols(), pUseFreq);
	SetHarmonics(pModelPar->garm);

	//������������� ���������� ������
	parModel.Type = pModelPar->Type;
//	parModel.garm = pModelPar->garm;
	parModel.sizeGcub = pModelPar->sizeGcub;
	parModel.knl = pModelPar->knl;

	CreateModel(mX, mY);

	return;
}

int CModelHQO::CreateModel(const Matrix& mX, const Matrix& mY)
{
	// mX [numSp x numFreq]
	// mY [numSp x numComp]

#ifdef _DEBUG_
	AppendToDebugReport("Start HQO::CreateModel");
#endif	

	numSp = mX.Nrows();
	//numFreq = mX.Ncols();
	numComp = mY.Ncols();

	//������ ��� ������������� ������
	mCalibrLinear.CleanUp();
	mCalibrNonLinear.CleanUp();
	mCalibrLinear.ReSize(numFreq, numComp);
	mCalibrNonLinear.ReSize(numFreq, numComp);

	mCalibrLinear = 0.0;
	mCalibrNonLinear = 0.0;

	//����������� ����� �������� (numFact)
	int numFact = GetNumVariables();

	if((numSp - numFact) < 2)//����� �������� ������� �� ������ ���� ������ 2-�
	{
		nError = ER_HQO; //������
		return nError;
	}

	if (parModel.knl != 0)
	{
		numFact /= 2;
	}

	int ErrorLemke;

	ColumnVector vConc(numSp);

	//Matrix mGarm(numFreq, numFact); // [numFreq x numFact]
	Matrix mXGarm; // [numSp x numFact]

	//���������(�������������/��������) �������������� ������� ��������
	if (parModel.garm)
	{
		//GarmTransform(numFact, numFreq, pUseFreq,  mGarm);

		mXGarm = mX * mGarm;

		if (parModel.knl != 0)
		{
			Matrix mX2(numSp, numFreq);
			mX2 = SP(mX, mX);
			Matrix mX2Garm;
			mX2Garm = mX2 * mGarm;
			mXGarm |= mX2Garm;
		}
	}
	else
	{
		// ������ ���������� ����������� �������
		mXGarm.ReSize(numSp, numFact);

		if (numFreqExcluded > 0)
		{
			int iF = 0;

			for (int i = 0; i < numFreq; i++)
			{
				if (useFreq[i] != 0)
				{
					mXGarm.Column(iF+1) = mX.Column(i+1);
					iF++;
				}
			}
		}
		else
		{
					mXGarm = mX;
		}

		if (parModel.knl != 0)
		{
			Matrix mX2Garm(numSp, numFact);
			mX2Garm = SP(mXGarm, mXGarm);

			mXGarm |= mX2Garm;
		}
	}

#ifdef _DEBUG_
	AppendToDebugReport("HQO::End of GarmTrans");
#endif	

	for(int i = 0; i < numComp; i++)
	{
		vConc = mY.Column(i+1);

		if(parModel.knl == 0)//�������� ������
		{
			ColumnVector vCalibGarm(numFact);

			ErrorLemke = HCQP(mXGarm, vConc, parModel.sizeGcub, 0, vCalibGarm);

			if (parModel.garm)
			{
				mCalibrLinear.Column(i+1) = mGarm*vCalibGarm;
			}
			else
			{
				if (numFreqExcluded > 0)
				{
					int iF = 0;

					for (int k = 0; k < numFreq; k++)
					{
						if (useFreq[k] != 0)
						{
							mCalibrLinear(k+1, i+1) = vCalibGarm(iF+1);
							iF++;
						}
						else
						{
							mCalibrLinear(k+1, i+1) = 0;
						}
					}
				}
				else
				{
					mCalibrLinear.Column(i+1) = vCalibGarm;
				}
			}
		}
		else
		{//���������� ������
			ColumnVector vCalibGarm(2*numFact);

			ErrorLemke = HCQP(mXGarm, vConc, parModel.sizeGcub, parModel.knl, vCalibGarm);

			if (parModel.garm)
			{
				mCalibrLinear.Column(i+1) = mGarm * vCalibGarm.Rows(1, numFact);
				mCalibrNonLinear.Column(i+1) = mGarm * vCalibGarm.Rows(numFact+1, 2*numFact);
			}
			else
			{
				if (numFreqExcluded > 0)
				{
					int iF = 0;

					for (int k = 0; k < numFreq; k++)
					{
						if (useFreq[k] != 0)
						{
							mCalibrLinear(k+1, i+1) = vCalibGarm(iF+1);
							mCalibrNonLinear(k+1, i+1) = vCalibGarm(numFact+iF+1);
							iF++;
						}
						else
						{
							mCalibrLinear(k+1, i+1) = 0;
							mCalibrNonLinear(k+1, i+1) = 0;
						}
					}
				}
				else
				{
					mCalibrLinear.Column(i+1) = vCalibGarm.Rows(1, numFact);
					mCalibrNonLinear.Column(i+1) = vCalibGarm.Rows(numFact+1, 2*numFact);
				}
			}
		}

		if(ErrorLemke)
		{
			nError = ER_HQO;
			break;
		}
	}

#ifdef _DEBUG_
	AppendToDebugReport("HQO::End");
#endif	

	return nError;
}


//------------------------------------------------------------------------------
//������� � 2 ���������� ��� ������
int CModelHQO::GetError() const
 {
 /*switch(error.kod)
       {
       case 0:
             {
             sprintf(error.text,"%s","�����");
             break;
             }
       case 1:
             {
             sprintf(error.text,"%s","��������� ��� ������������ �� ������������� �������������");
             break;
             }
       case 2:
             {
             sprintf(error.text,"%s","����� �������� �������  ������ 2-�");
             break;
             }
       case 3:
             {
             sprintf(error.text,"%s","������ � ������� ������� ������ ��");
             break;
             }
       default:
             break;
       }*/
 return nError;
 }
//------------------------------------------------------------------------------
//������� � 3 ���������� ����� ���������� ������
int CModelHQO::GetNumVariables() const
 {
	int NumFact;

	if (parModel.garm)
	{
		NumFact = 2*parModel.garm + 1;
	}
	else
	{
		NumFact = numFreq - numFreqExcluded;

		//char str[64];
		//sprintf(str, "numFreq = %d numFreqExcluded = %d\n", numFreq, numFreqExcluded);
		//AppendToDebugReport(str);
	}

	if (parModel.knl != 0)
	{
		NumFact *= 2;
	}

	return NumFact;
}
//------------------------------------------------------------------------------
//������� � 4 ���������� ��� ������
//������������ �������� ����� ������������� �������
/*int CModelHQO::GetCalibrLinear(double **PpCalibrLinear)
{
	for(int i=0;i<numComp;i++)
	for(int j=0;j<numFreq;j++)
		PpCalibrLinear[i][j]= mCalibrLinear(j+1, i+1);
	return error.kod;
}*/

int CModelHQO::GetCalibrLinear(Matrix &mClbLin)
{
	mClbLin = mCalibrLinear;
	return nError;
}
//------------------------------------------------------------------------------
//������� � 5 ���������� ��� ������
//������������ ���������� ����� ������������� ������� ppCalibrNonLinear[numComp][numFreq]
/*int CModelHQO::GetCalibrNonLinear(double **PpCalibrNonLinear)
{
	for(int i=0;i<numComp;i++)
	for(int j=0;j<numFreq;j++)
		PpCalibrNonLinear[i][j] = mCalibrNonLinear(j+1, i+1);
	return error.kod;
}*/

int CModelHQO::GetCalibrNonLinear(Matrix &mClbNonLin)
{
	mClbNonLin = mCalibrNonLinear;
	return nError;
}

//------------------------------------------------------------------------------
//������� � 6 ����������� ����������: �������� ����� ������������ ������������� ������
//���������� - ��������� �� ��������� ���������� ������, �����������
//(��������� ������ � ������������� ������� ����������� �� �����
//� ���������� � HSO.dll ��� �������� ����������� �������, ������������
//����� ������. ������ � dll)
/*CModelHQO::CModelHQO(int NumComp,int NumFreq,double **PpCalibrLinear,double **PpCalibrNonLinear,
           int *UseFreq,const MODEL_MMP *PModelPar)
 {
 int i,j;
 numFreq=NumFreq;
 numComp=NumComp;
 parModel.Type = PModelPar->Type;
 parModel.garm = PModelPar->garm;
 parModel.sizeGcub = PModelPar->sizeGcub;
 parModel.knl = PModelPar->knl;

 mCalibrLinear.ReSize(numFreq, numComp);
 mCalibrNonLinear.ReSize(numFreq, numComp);

 for(i=0;i<numComp;i++)
 for(j=0;j<numFreq;j++)
   {
    mCalibrLinear(j+1, i+1) = PpCalibrLinear[i][j];
    mCalibrNonLinear(j+1, i+1) = PpCalibrNonLinear[i][j];
    }
 //������ � ������������� ������� ��������� ������������ ������(������)
 useFreq=new int[numFreq];
 for(i=0;i<numFreq;i++)
    useFreq[i]=UseFreq[i];
 //����������� ����� ����������� ������(������)
 numFreqExcluded=0;
 for(i=0;i<numFreq;i++)
    if(!useFreq[i])
      numFreqExcluded++;
 //����������� ����� �������� (numFact)
 numFact=GetNumVariables();
 return;
 }*/

void CModelHQO::Load(const Matrix &mClbLin, const Matrix &mClbNonLin,
			const int *pUseFreq, const MODEL_MMP *pModelPar)
{ 
	// mClbLin [numFreq x numComp]

	numFreq = mClbLin.Nrows();
	numComp = mClbLin.Ncols();

	parModel.Type = pModelPar->Type;
	parModel.garm = pModelPar->garm;
	parModel.sizeGcub = pModelPar->sizeGcub;
	parModel.knl = pModelPar->knl;

	mCalibrLinear = mClbLin;
	mCalibrNonLinear = mClbNonLin;

	//������ � ������������� ������� ��������� ������������ ������(������)
	useFreq = new int[numFreq];

	//����������� ����� ����������� ������(������)
	numFreqExcluded = 0;

	for(int i = 0; i < numFreq; i++)
	{
		useFreq[i] = pUseFreq[i];
		if(!useFreq[i]) numFreqExcluded++;
	}

//����������� ����� �������� (numFact)
//	numFact=GetNumVariables();
	return;
}



//------------------------------------------------------------------------------
//������� � 7  ������������ � ������� ������������ ������
// �� ������� "��������" ppSp[numSpectra][numFreq]
// ���������� ������� ������������� "������������" ppComp[numSpectra]*[numComp]
/*void CModelHQO::Predict(int NumSpectra,int NumFreq,int NumComp,
                        double **PpSp,double **PpComp)
 {
 if(NumFreq != numFreq)
   {
   error.kod=1;//������
   return;
   }
 if(NumComp != numComp)
   {
   error.kod=1;//������
   return;
   }

   Matrix mSpectra(NumSpectra, numFreq);
   Matrix mComp(NumSpectra, numComp);

   for(int i = 0; i < NumSpectra; i++)
   {
	   mSpectra.Row(i+1) << PpSp[i];
   }

   Predict(mSpectra, mComp);

   for(int i = 0; i < NumSpectra; i++)
   {
		for (int k = 0; k < numComp; k++)
		{
			PpComp[i][k] = mComp(i+1, k+1);
		}
   }

return;
}*/

void CModelHQO::Predict(const Matrix &mSpectra, Matrix &mComp)
{
	// mSpectra [numSp x NumFreq]
	// mComp [numSp x numComp]

	//PrintMatrix(mSpectra, "mSpectra4Predict.dat");
	//PrintMatrix(mCalibrLinear, "mCalibrLinear.dat");

	if(numFreq != mSpectra.Ncols())
	{
		nError = ER_FREQ_RANGE;//������
		return;
	}

//	if(numComp != mComp.Ncols())
//	{
//		error.kod=1;//������
//		return;
//	}

	//������ �������� ����� mComp

	mComp = mSpectra*mCalibrLinear;

	//������ ���������� ����� mComp
	if(parModel.knl)
	{
		int numSp = mSpectra.Nrows();
		for(int i = 1; i <= numSp; i++)
		{
			mComp.Row(i) += SP(mSpectra.Row(i), mSpectra.Row(i))*mCalibrNonLinear;
		}
	}

	return;
}


//------------------------------------------------------------------------------
//������� � 8 ����������
/*CModelHQO::~CModelHQO()
 {
 for(int i=0;i<numComp;i++)
    {
    delete []ppCalibrLinear[i];
    delete []ppCalibrNonLinear[i];
    }
 delete []ppCalibrLinear;
 delete []ppCalibrNonLinear;
 delete []useFreq;
 return;
 }*/

CModelHQO::~CModelHQO()
{
	delete[] useFreq;
	return;
}



//��� �����������
int CModelHQO::SetFrequencies(int nFreq, const int *pUseFreq)
{
	numFreq = nFreq;

	delete[] useFreq;
	useFreq=new int[numFreq];

	numFreqExcluded = 0;

	for(int i = 0; i < numFreq; i++)
	{
		useFreq[i] = pUseFreq[i];
		if (!useFreq[i])
		{
			numFreqExcluded++;
		}
	}

	if (numFreqExcluded == numFreq)
	{
		nError = ER_FREQ_RANGE;
	}

	return nError;
}

int CModelHQO::SetHarmonics(int numHarmonics)
{
	//AppendToDebugReport("Start CModelHQO::SetHarmonics");
	nError = ER_OK;

	parModel.garm = numHarmonics;

	if (numHarmonics)
	{
		int numFact = 2*numHarmonics+1;
		mGarm.CleanUp();
		GarmTransform(numFact, numFreq, useFreq, mGarm);
	}

	//AppendToDebugReport("End CModelHQO::SetHarmonics");

	return nError;
}