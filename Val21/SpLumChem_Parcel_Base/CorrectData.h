// CorrectData.h: interface for the CCorrectData class.
//
// ��������:
// ����� CCorrectData ������������ ��� �������� ����������
// � �������� ���������� (��������� ��������) � ����������
// ��������������� ���������
//
// ������������ ������� CPbt


#ifndef _CORRECTDATA_H_
#define _CORRECTDATA_H_

#include "newmat.h"
#include <stdio.h>

class CCorrectData
{
public:
	CCorrectData();
	void Save(FILE* fgh, int clbVersion);
	void Load(FILE* fgh, int nSize);
	int CorrectSpectra(Matrix& mSpectra);

	int nCorrectType;

	// nCorrectType = 1: shift
	RowVector vBiasSp;

	// nCorrectType = 2: linear correction
	RowVector vSlopeSp;

	// nCorrectType = 3: quadratic correction
	RowVector vQuadrSp;
};


#endif // _CORRECTDATA_H_
