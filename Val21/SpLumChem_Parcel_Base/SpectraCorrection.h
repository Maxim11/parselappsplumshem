#ifndef _SpectraCorrection_H_
#define _SpectraCorrection_H_

//#include <string>
#include "CSamples.h"

using namespace std;

#include "SpLumChemStructs.h"



class CSpectraCorrection
{
public:
//	CSpectraCorrection(void);
	~CSpectraCorrection(void);
	void Init();
	CSpectraCorrection(vector<CSampleProxy> *pSmplsClbMaster, // array of calibration Master samples
					vector<CSampleProxy> *pSamplesMaster, // array of Master samples
					vector<CSampleProxy> *pSamplesSlave, // array of Slave samples
					const CFreqScale *FreqScale); // частотная шкала спектров
	int Correct(string strPreProc);
	void GetDistances(vector<double> *fDistances, double *fDistMin, double *fDistMax, double *fDistMean);
//	void GetCorrectedClbSpectra(*fDistMin, *fDistMax, *fDistMean,  vector<double> *fDistances);
//	void GetPreprocData(CSpecPreprocData_Parcel *pPreprocData);
	void GetCorrectedSpectra(vector<CSampleProxy> *pSmplsClbMaster);
	void GetPreprocData(CSpecPreprocData_Parcel *pPreprocData);

protected:
	CSamples *pSamplesClb, *pSamplesMaster, *pSamplesSlave;
	CSpecPreprocData specPreprocData;
	int nError;
	ColumnVector vDecartDist;
	RowVector vBias, vSlope;
	CFreqScale mFreqScale;
	Matrix mXClbCor;
};

/*int SpectraCorrection(vector<CSampleProxy> *pSmplsClbMaster, // array of calibration Master samples
					vector<CSampleProxy> *pSmplsMaster, // array of Master samples (common)
					vector<CSampleProxy> *pSmplsSlave, // array of Slave samples (common)
					const CFreqScale *freqScale, // частотная шкала спектров
					const CSpecPreproc *prePar, // preprocessing parameters
					double *fDistMin, // минимальное декартово расстояние
					double *fDistMax, // максимальное декартово расстояние
					double *fDistMean); // среднее декартово расстояние*/

/*int SpectraCorrection(vector<CSampleProxy> *pSmplsClbMaster, // array of calibration Master samples
					vector<CSampleProxy> *pSmplsMaster, // array of Master samples (common)
					vector<CSampleProxy> *pSmplsSlave, // array of Slave samples (common)
					const CFreqScale *freqScale, // частотная шкала спектров
					int nSpType, // тип спектра
					vector<string> *prePar, // array of preprocessing strings
					vector<double> *fDistMin, // минимальное декартово расстояние
					vector<double> *fDistMax, // максимальное декартово расстояние
					vector<double> *fDistMean); // среднее декартово расстояние
*/

#endif _SpectraCorrection_H_