#include <math.h>
#include "ModelPCA.h"
#include "SpLumChemStructs.h"
#include "MatrixUtilities.h"
#include "newmatap.h"
//#include "Debug.h"
//#include "PCA.h"

//#define PCA_OLD
#define PCA_SIMPLE
//#include <time.h>

//extern double durationPCA;//PLSinit, durationPLScycle, durationPLSend, durationPLSpredict;


CModelPCA::CModelPCA(const Matrix& mX, int numFactors/*=10*/)
{
	const double eps = 1e-12;

//	clock_t start, finish;

//	start = clock();

#ifdef _DEBUG_
		AppendToDebugReport("Start ModelPCA");
#endif

	nModelType = MODEL_PCA;

	nError = ER_OK;

	numberOfFactors = numFactors;

	int numRows = mX.Nrows();
	int numColumns = mX.Ncols();

	if ((numFactors > numRows) || (numFactors > numColumns))
	{
		nError = ER_PCA;
		return;
	}

#ifdef _DEBUG_
	char strBuf[1024];
	sprintf(strBuf, "numRows = %d\n numCols = %d\n numFactors = %d\n", numRows, numColumns, numFactors);
	AppendToDebugReport(strBuf);
#endif

	mScores.ReSize(numRows, numberOfFactors);

	mFactors.ReSize(numberOfFactors, numColumns);

	PCAfactors(mX);

	if (nError != ER_OK)
	{
#ifdef _DEBUG_
	AppendToDebugReport("Error in PCAfactors");
#endif	
		return;
	}
	
	mEigenInv.ReSize(mFactors.Nrows());

	double fEigenValue;

	for (int i = 1; i <= mFactors.Nrows(); i++)
	{
		fEigenValue = mFactors.Row(i).SumSquare();

		if (fEigenValue > eps)
		{
			mEigenInv(i) = 1.0 / fEigenValue;
		}
		else
		{
			nError = ER_PCA;
			break;
		}
	}

//	PrintMatrix(mScores, "mScores.dat");
//	PrintMatrix(mFactors, "mFactors.dat");
//	PrintDiagonalMatrix(mEigenInv, "mEigenInv.dat");

	//double fErrMax = (mX - mScores

//	finish = clock();
//	durationPCA += (double)(finish - start);// / CLOCKS_PER_SEC;
}

/*CModelPCA::CModelPCA(const Matrix& mX, int numFactors, const Matrix &mScoresGuess)
{
//	clock_t start, finish;

//	start = clock();
	nModelType = MODEL_PCA;

	nError = ER_OK;

	numberOfFactors = numFactors;

	int numRows = mX.Nrows();
	int numColumns = mX.Ncols();

	mScores.ReSize(numRows, numberOfFactors);

	mFactors.ReSize(numberOfFactors, numColumns);

	PCAfactors(mX);//, mScoresGuess);

//	PCAfactors(mX, mScoresGuess);

	if (nError == ER_OK)
	{
		mEigenInv.ReSize(numberOfFactors);

		for (int i = 1; i <= numberOfFactors; i++)
		{
			mEigenInv(i) = 1.0 / (mFactors.Row(i)).SumSquare();
		}
	}
//	finish = clock();
//	durationPCA += (double)(finish - start);// / CLOCKS_PER_SEC;
}*/


CModelPCA::CModelPCA()
{
	nModelType = MODEL_PCA;

	nError = ER_OK;

	numberOfFactors = 0;
}

/*void CModelPCA::PCAfactors(const Matrix& mX)
{
#ifdef OLD_PCA
	int numRows = mX.Nrows();
	int numColumns = mX.Ncols();

	PrintMatrix(mX, "mX.dat");

	int oldFactor = numberOfFactors;

	int cnt = numberOfFactors;

	for (int n = 0; n < cnt; n++)
	{
		mScores.ReSize(numRows, numberOfFactors);
		mFactors.ReSize(numberOfFactors, numColumns);

		mScores = 0.1; // �������������� ������������ ���������
		mFactors = 0.0;

		if ((numberOfFactors = PCAeigen_G_VCM(mX)) == oldFactor)
		{
			break;
		}

		if (numberOfFactors <= 0)
		{
			nError = ER_PCA;
			return;
		}

		oldFactor = numberOfFactors;
	}

	PrintMatrix(mScores, "mScores.dat");
	PrintMatrix(mFactors, "mFactors.dat");

	if (nError != ER_OK) return;
#else


	// new PCA
	int n = mX.Nrows();

	SymmetricMatrix mZ; // nxn = numSp x numSp

	mZ << mX * mX.t();

	DiagonalMatrix mD, mOffD; // n
	Matrix mQ; // nxn

	Housholder(mZ, mD, mOffD, mQ);
	AppendToDebugReport("Housholder done");
	PrintMatrix(mD, "mD.dat");
	PrintMatrix(mOffD, "mOffD.dat");
	PrintMatrix(mQ, "mQ.dat");


	DiagonalMatrix mEigen; // numberOfFactors

	if (FindEigenValues(mD, mOffD, numberOfFactors, mEigen) != numberOfFactors)
	{
		nError = ER_PCA;
		return;
	}
	AppendToDebugReport("EigenValues done");


	// mScores numRows * numFactors
	FindEigenVectors(mD, mOffD, mEigen, mScores);
	AppendToDebugReport("EigenVectors done");


	mScores = mQ * mScores;
	mFactors = mScores.t() * mX;
#endif
}*/

/*void CModelPCA::PCAfactors(const Matrix& mX, const Matrix &mScoresGuess)
{
	int numColumns = mX.Ncols();
	mScores = mScoresGuess;
	mFactors.ReSize(numberOfFactors, numColumns);

	if (PCAeigen_G_VCM(mX) != numberOfFactors)
	{
		nError = ER_PCA;
	}	
}*/

void CModelPCA::PCAfactors(const Matrix& mX)
{

	int nFact;

	if (numberOfFactors == 1)
	{
		// Old algorithm
		nFact = PCA_VCM(mX, numberOfFactors);
	}
	else
	{
#ifdef PCA_SIMPLE
		//Simple algorithm - QR
		nFact = PCA_Simple(mX, numberOfFactors);
#else
#ifdef PCA_OLD
		nFact = PCA_VCM(mX, numberOfFactors);
#else
		nFact = PCA_New(mX, numberOfFactors);
#endif
#endif
	}

	if (nFact != numberOfFactors)
	{
		nError = ER_PCA;
	}
	else
	{
		mFactors = mScores.t() * mX;
	}
}

// ���������� ������� �������� ��� ������ ��������
// �������� "decomposition of the Variance-Covariance Matrix" �� Galactic

int CModelPCA::PCAeigen_G_VCM(const Matrix& mX)
{
	int numRows = mX.Nrows();

	Matrix mZ = mX * mX.t();

	// ��� ������ Power Method

	for (int i = 1; i <= numberOfFactors; i++)
	{
		ColumnVector vOldScores(numRows);

		vOldScores = 0.0;

		long double lambda = 1.0;

		for (int j = 0; j < 100; j++)
		{
			mScores.Column(i) = mZ * mScores.Column(i);

			long double sumsq = SumSquare(mScores.Column(i));

			lambda = sqrt(sumsq);  // ������.��������

			if (lambda < 1e-30)
			{
				if (i == 1)
				{// ��� �������������� ���������?
					nError = ER_PCA;
				}

				return i - 1;
			}

			mScores.Column(i) /= lambda;

			if (j != 0)
			{
				double res = fabs(mScores.Column(i).Sum() - vOldScores.Sum());
				if (res < fabs(mScores.Column(i).Sum()) * 1e-20) break;
			}

			vOldScores = mScores.Column(i);
		}

		mZ -= (mScores.Column(i) * mScores.Column(i).t()) * lambda;
	}

	mFactors = mScores.t() * mX;


	return numberOfFactors;
}


void CModelPCA::Save(FILE* fgh, int clbVersion)
{
	fwrite(&numberOfFactors, sizeof(int), 1, fgh);
	SaveMatrix(mFactors, fgh);
	SaveMatrix(mScores, fgh);
}

void CModelPCA::Load(FILE* fgh, int clbVersion)
{
	nError = ER_OK;
	fread(&numberOfFactors, sizeof(int), 1, fgh);
	LoadMatrix(mFactors, fgh);
	LoadMatrix(mScores, fgh);

	mEigenInv.ReSize(mFactors.Nrows());

	for (int i = 1; i <= mFactors.Nrows(); i++)
	{
		mEigenInv(i) = 1.0 / (mFactors.Row(i)).SumSquare();
	}
}

// �� ����������� PCA-������ ��������� ���������� ������������
// ��� ������ ������� �������� (������� ��������������)
void CModelPCA::CalculateMahalanobisDistance(const Matrix& mSp,
											 ColumnVector& vMahDist)
{
	if (mSp.Ncols() != mFactors.Ncols())
	{
		nError = ER_PCA;
		//AppendToDebugReport("Error PCA");
		return;
	}

	// ������� ��������� ����� (scores) ������� mX, �.�.
	// ���������� mX �� mFactors

	Matrix mSpScores;

	mSpScores = mSp * (mFactors.t() * mEigenInv);

	int nSp = mSp.Nrows();

	vMahDist.ReSize(nSp);

	for (int i = 1; i <= nSp; i++)
	{
		vMahDist(i) = sqrt((mSpScores.Row(i)).SumSquare());
	}

	//PrintMatrix(vMahDist, "vMahDist.dat");
}


// ��������� ���������� ������������ ��� ��������������
// �������� �� ������� ��������� PCA-������
void CModelPCA::GetMahalanobisDistance(ColumnVector& vMahDist) const
{
	int nSp = mScores.Nrows();

	vMahDist.ReSize(nSp);

	for (int i = 1; i <= nSp; i++)
	{
		vMahDist(i) = sqrt((mScores.Row(i)).SumSquare());
	}
}

void CModelPCA::FillModel_Parcel(CModel_Parcel& mModel) const
{
	mModel.nModelType = nModelType;
//	mModel.mCalibr = Matrix2MDbl(mCalibr);
//	mModel.mFactors = Matrix2MDbl(mFactors);
//	mModel.mScores = Matrix2MDbl(mScores);
//	Matrix2MDblResize(mCalibr, mModel.mCalibr);
	Matrix2MDblResize(mFactors, mModel.mFactors);
	Matrix2MDblResize(mScores, mModel.mScores);
}

int CModelPCA::PCA_Simple(const Matrix& mX, int numFact)
{
	int n = mX.Nrows();

    SymmetricMatrix mZ; // nxn = numSp x numSp

	mZ << mX * mX.t();

	DiagonalMatrix mEigenValues;
	Matrix mEigenVectors;

	double fMax = mZ.MaximumAbsoluteValue();
	double fEps;

	if (fMax < 1.0)
	{
		fEps = fMax * DBL_EPSILON;
	}
	else fEps = DBL_EPSILON;

	EigenValues(mZ, mEigenValues, mEigenVectors);

	int nGoodEigen = 0;

	//PrintDiagonalMatrix(mEigenValues, "mEigenValues.dat");

	for (int i = 1; i <= numFact; i++)
	{
		if (fabs(mEigenValues(n-i+1)) <= fEps)
		{
			break;
		}
		else
		{
			nGoodEigen++;
			mScores.Column(i) = mEigenVectors.Column(n-i+1);
		}
	}
	
	return nGoodEigen;
}

int CModelPCA::PCA_VCM(const Matrix& mX, int numberOfFactors)
{
	int numRows = mX.Nrows();

	Matrix mZ = mX * mX.t();

	//Matrix mScores(numRows, numberOfFactors);

	// ��� ������ Power Method

	ColumnVector vOldScores(numRows);

	mScores = 0.1; // �������������� ������������ ���������

	for (int i = 1; i <= numberOfFactors; i++)
	{
		vOldScores = 0.0;

		long double lambda = 1.0;

		for (int j = 0; j < 100; j++)
		{
			mScores.Column(i) = mZ * mScores.Column(i);

			long double sumsq = SumSquare(mScores.Column(i));

			lambda = sqrt(sumsq);  // ������.��������

			//char strTmp[64];

			//sprintf(strTmp, "lambda = %.e", lambda);

			//AppendToDebugReport(strTmp);

			if (lambda < 1e-30)
			{
				return i-1;//-1;
			}

			mScores.Column(i) /= lambda;

			if (j != 0)
			{
				double res = fabs(mScores.Column(i).Sum() - vOldScores.Sum());
				if (res < fabs(mScores.Column(i).Sum()) * 1e-20) break;
			}

			vOldScores = mScores.Column(i);
		}

		mZ -= (mScores.Column(i) * mScores.Column(i).t()) * lambda;
	}

	//double fMax = mZ.MaximumAbsoluteValue();
	//char strTmp[64];

	//sprintf(strTmp, "Z.Max = %.e", fMax);

	//AppendToDebugReport(strTmp);

	return numberOfFactors;
}

/*int CModelPCA::PCA_New(const Matrix& mX, int numberOfFactors)
{
	int n = mX.Nrows();

	//clock_t start, finish;

	SymmetricMatrix mZ; // nxn = numSp x numSp

	mZ << mX * mX.t();

	DiagonalMatrix mD, mOffD; // n
	Matrix mQ; // nxn

	//start = clock();
	Housholder(mZ, mD, mOffD, mQ);
	//finish = clock();
	//durationHH += (finish - start);

	//AppendToDebugReport("Housholder done");
	//PrintMatrix(mD, "mD.dat");
	//PrintMatrix(mOffD, "mOffD.dat");
	//PrintMatrix(mQ, "mQ.dat");


	//start = clock();

	DiagonalMatrix mEigen; // numFact

	int numEigenVal = FindEigenValues(mD, mOffD, numberOfFactors, mEigen);

	//PrintDiagonalMatrix(mEigen, "mEigen.dat");

	if (numEigenVal != numberOfFactors)
	{
		nError = ER_PCA;
		return numEigenVal;
	}
	//AppendToDebugReport("EigenValues done");
	//finish = clock();
	//durationEVal += (double)(finish - start);


	// mScores numRows * numFactors
	//start = clock();

	Matrix mEigenVectors;
	FindEigenVectors(mD, mOffD, mEigen, mEigenVectors);
	//AppendToDebugReport("EigenVectors done");
	//finish = clock();
	//durationEVect += (double)(finish - start);

	//start = clock();
	mScores = mQ * mEigenVectors;

	//PrintMatrix(mScores, "mScores.dat");

	//finish = clock();
	//durationScores += (double)(finish - start);


	// test EigenVectors
//	ColumnVector vErr(n);

	for (int i = 1; i <= numFact; i++)
	{
		double Lambda = mEigen(i);
		double fErr;
		int k;
		k = 1;
		fErr = (mD(k)-Lambda)*mEigenVectors(k, i) + mOffD(k+1)*mEigenVectors(k+1, i);
		vErr(k) = fErr;
		for (k = 2; k < n; k++)
		{
			fErr = (mD(k)-Lambda)*mEigenVectors(k, i) + mOffD(k)*mEigenVectors(k-1, i) + mOffD(k+1)*mEigenVectors(k+1, i);
			vErr(k) = fErr;
		}
		k = n;
		fErr = (mD(k)-Lambda)*mEigenVectors(k, i) + mOffD(k)*mEigenVectors(k-1, i);
		vErr(k) = fErr;
	}

	//PrintDiagonalMatrix(mEigen, "mEigenValues.dat");
	//PrintMatrix(mEigenVectors, "mEigenVectors.dat");
	//PrintMatrix(mScores, "mScores_new.dat");
//	mFactors = mScores.t() * mX;
	return numEigenVal;
}*/

