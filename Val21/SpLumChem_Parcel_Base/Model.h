// Model.h: interface for the CModel class.
//
// ��������:
// ����� CModel - ������� ����������� ����� ��� ������������� ������
// 
//
// ������������ �������� CModelPCR, CModelPLS, CQntCalibration
//

#ifndef _MODEL_H_
#define _MODEL_H_


#include <stdio.h>
#include "newmat.h"
#include "SpLumChemStructs.h"

class CModel
{
public:
	virtual int GetError() const = 0;
	virtual int GetNumVariables() const = 0;
	virtual int GetModelType() const = 0;
	virtual void CalculateMahalanobisDistance(const Matrix& mSp, ColumnVector& vMahDist) = 0; // preprocessed spectrum
	virtual void GetMahalanobisDistance(ColumnVector& vMahDist) const = 0; // preprocessed spectrum
	virtual void Predict(const Matrix& mX, Matrix& mYpredicted) = 0;
	virtual void GetMatrixScores(Matrix &mS) const = 0;
	//virtual void Save(FILE* fgh, int clbVersion) const = 0;
	//virtual void Load(FILE* fgh, int clbVersion) = 0;
	virtual void GetModelPar(MODEL_MMP& ModelPar) const = 0;
	virtual void GetUseFreq(int *pUseFreq) const = 0;
	virtual void FillModel_Parcel(CModel_Parcel& mModel) const = 0;
	//virtual void FillModel_Parcel(CModelHSO_Parcel& mModel) const = 0;
	virtual void LoadFromQnt_Parcel(const CModel_Parcel& mModel) = 0;
	virtual ~CModel() {}

	//��� HQO
	virtual int SetFrequencies(int nFreq, const int *pUseFreq) = 0;
	virtual int SetHarmonics(int numHarmonics) = 0;
	virtual int SetCnl(double fCnl) = 0;
	virtual int SetShc(double fShc) = 0;
	virtual int CreateModel(const Matrix& mX, const Matrix& mY) = 0;

};

#endif // _MODEL_H_
