#ifndef _READ_SPEC_H_
#define _READ_SPEC_H_

int ReadSpec(const char *fname, double *pFreq_first, double *pFreq_step, int *pNf, double **ppData);

#endif//_READ_SPEC_H_	