// CSampleFromFile.h: interface for the CSampleFromFile class.
//
// ��������:
// ����� CSampleFromFile ������������ ��� ������
// � ������� �������� (saa-�����).
//
// ������������ �������� CSample � IRCalibrTransfer



#ifndef _SAMPLE_FROM_FILE_H_
#define _SAMPLE_FROM_FILE_H_

#include "SampleArray.h"

#include <string>
#include <vector>

using namespace std;


class CSampleFromFile
{
public:
	CSampleFromFile();
	CSampleFromFile(const char* fname);

	void Load(const char* fname);
	void Load(const char* fname,  const vector<bool> *pvSpStatus);
	void Load(const char* fname, const vector<bool> *pvSpStatus, const vector<int> *pvSpIndex);
	void Load(const CSampleArray& pSampleArray);

	~CSampleFromFile();
	int GetError() const { return nError; }
	int GetNumComponents() const { return numberOfComponents; }
	int GetNumSpectra() const { return numberOfSpectra; }
	int GetNumFreq() const { return numberOfFreq; }
	double GetStartFreq() const { return fStartFreq; }
	double GetStepFreq() const { return fStepFreq; }

	bool FindComponent(const string& theCompName) const;
	double* GetSpectrum(int iS);
	double* GetSpectrum(int iS, double freqMin);
	double GetComponent(const string& theCompName);
	string GetSpectrumName(int iS) const { return pstrSpectrumName[iS] ; }



protected:
	int nError;			// ��� ������

	string strSampleName;// ��� �������

	double fStartFreq;	// ��������� ������� �������� �������
	double fStepFreq;	// ��� �� ������� �������� �������
	int numberOfFreq;	//����� ������ �������� �������

	int numberOfSpectra;		// ����� �������� � �������
	string* pstrSpectrumName;	// ������ ���� �������� ������� 
	double* *pfSpectrum;		// ������ �������� �������


	// components data
	string strRefFileName;	// ������ ���� ������ ����������� ������ 
	int numberOfComponents;	// ����� ��������� � �������
	string* pstrCompName;	// ������ ���� ���������
	double* fCompConc;		// ������ ������������ ���������
	enum SampleErrors{ ER_OK };
};

#endif // _SAMPLE_FROM_FILE_H_