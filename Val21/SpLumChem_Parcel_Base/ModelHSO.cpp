#include <math.h>
#include "ModelHSO.h"
#include "SpLumChemStructs.h"
#include "sclm_cut.h"
#include "MatrixUtilities.h"
#include "ModelHQO.h"



//CModelHSO::CModelHSO(const Matrix& mX, const Matrix& mY, const int pIncFreq[], const MODEL_MMP *pModelPar, int numPCAfactors)
CModelHSO::CModelHSO(const Matrix& mX, const Matrix& mY, const int pIncFreq[], const MODEL_MMP *pModelPar)
{
	nError = ER_OK;

	//HQO model
	//AppendToDebugReport("HQO start");

	pModelHQO = new CModelHQO(mX, mY, pIncFreq, pModelPar);

	//AppendToDebugReport("HQO end");

	nError = pModelHQO->GetError();


/*	if (nError == ER_OK)
	{
		if (numPCAfactors > 0)
		{
			pModelPCA = new CModelPCA(mX, numPCAfactors);
		}
		else
		{
			pModelPCA = 0;
		}
	}*/
}

/*void CModelHSO::Save(FILE* fgh, int clbVersion) const
{
//	FILE *asd = fopen("ModelPar3.dat", "w");
//	fprintf(asd, "type = %d\n garm = %d\n sizeGcub = %.1f\n knl = %.1f\n", theModelPar.Type, theModelPar.garm, theModelPar.sizeGcub, theModelPar.knl);
//	fclose(asd);

	fwrite(&theModelPar.Type, sizeof(int), 1, fgh);
	fwrite(&theModelPar.garm, sizeof(int), 1, fgh);
	fwrite(&theModelPar.sizeGcub, sizeof(double), 1, fgh);
	fwrite(&theModelPar.knl, sizeof(double), 1, fgh);

	SaveMatrix(mCalibrLinear, fgh);
	SaveMatrix(mCalibrNonLinear, fgh);

	// ������ ������ - 0, 1 (���. �� ���.)
	//fread
	int numFreq = mCalibrLinear.Ncols();
	fwrite(pUseFreq, sizeof(int), numFreq, fgh);

	pModelPCA->Save(fgh, clbVersion);
}*/

/*void CModelHSO::Load(FILE* fgh, int clbVersion)
{
	nError = ER_OK;
	numVar = 0;

//	fread(&theModelPar.Type, sizeof(int), 1, fgh);
	fread(&theModelPar.garm, sizeof(int), 1, fgh);
	fread(&theModelPar.sizeGcub, sizeof(double), 1, fgh);
	fread(&theModelPar.knl, sizeof(double), 1, fgh);

	LoadMatrix(mCalibrLinear, fgh);
	LoadMatrix(mCalibrNonLinear, fgh);

	int numComp = mCalibrLinear.Nrows();
	int numFreq = mCalibrLinear.Ncols();

	// ������ ������ - 0, 1 (���. �� ���.)
	//fread
	pUseFreq = new int[numFreq];
	fread(pUseFreq, sizeof(int), numFreq, fgh);

	pModelPCA=new CModelPCA();
	pModelPCA->Load(fgh, clbVersion);


	double **ppCalibrLinear = new double*[numComp];
	double **ppCalibrNonLinear = new double*[numComp];

	for (int i = 0; i < numComp; i++)
	{
		ppCalibrLinear[i] = new double[numFreq];
		ppCalibrNonLinear[i] = new double[numFreq];
	}

	for (i = 0; i < numComp; i++)
	{
		for (int j = 0; j < numFreq; j++)
		{
			ppCalibrLinear[i][j] = mCalibrLinear(i + 1, j + 1);
			ppCalibrNonLinear[i][j] = mCalibrNonLinear(i + 1, j + 1);
		}
	}



	pModel = ModelHSO_CreateMade(numComp, numFreq, ppCalibrLinear, ppCalibrNonLinear, pUseFreq, &theModelPar);

	nError = ModelHSO_GetError(pModel);

	if (nError == ER_OK)
	{
		numVar = ModelHSO_GetNumVariables(pModel);
	}


	for (int j = 0; j < numComp; j++)
	{
		delete[] ppCalibrLinear[j];
		delete[] ppCalibrNonLinear[j];
	}

	delete[] ppCalibrLinear;
	delete[] ppCalibrNonLinear;
}*/

CModelHSO::CModelHSO()
{
	nError = ER_OK;
//	theModelPar.Type = MODEL_HSO;
//	theModelPar.garm = 0;
//	theModelPar.knl =0.0;
//	theModelPar.sizeGcub = 0.0;

	pModelHQO = 0;
//	pModelPCA = 0;
//	pUseFreq = 0;
}

// � ������� ����������� HSO-������ ��������� ������������
// ������������ ��������� ��� ������� �������� (����������������)
void CModelHSO::Predict(const Matrix& mX, Matrix& mYpredicted)
{
	//AppendToDebugReport("HQO predict start");
	pModelHQO->Predict(mX, mYpredicted);
	//AppendToDebugReport("HQO predict end");
}

CModelHSO::~CModelHSO()
{
	delete pModelHQO;
//	ModelHSO_Destroy(pModel);
//	delete pModelPCA;
//	delete[] pUseFreq;
}

void CModelHSO::GetUseFreq(int *pIncFreq) const
{
	int numFreq = pModelHQO->numFreq;

	for (int i = 0; i < numFreq; i++)
	{
		pIncFreq[i] = pModelHQO->useFreq[i];
	}
}

void CModelHSO::FillModel_Parcel(CModel_Parcel& mModel) const
{
	mModel.nModelType = pModelHQO->GetModelType();
	mModel.numVar = pModelHQO->GetNumVariables();
//	mModel.mCalibr = Matrix2MDbl(mCalibr);
//	mModel.mFactors = Matrix2MDbl(mFactors);
//	mModel.mScores = Matrix2MDbl(mScores);

	Matrix2MDblResize(pModelHQO->mCalibrLinear, mModel.mCalibrLinear);
	Matrix2MDblResize(pModelHQO->mCalibrNonLinear, mModel.mCalibrNonLinear);

	// PCA-model
	//Matrix mFactors, mScores;
	//pModelPCA->GetMatrixFactors(mFactors);
	//pModelPCA->GetMatrixScores(mScores);
//	Matrix2MDblResize(pModelPCA->mFactors, mModel.mFactors);
//	Matrix2MDblResize(pModelPCA->mScores, mModel.mScores);
}