#include "SampleUtils.h"
#include "Dfx.h"
//#include "SpLumChemStructs.h"
#include "Spectra.h"

int	LoadComponents(const char* fname,
				   int* pNumberOfComponents,
				   string* *pstrCompName,
				   double* *pfCompConc)
{
	if (!DfxFileExist(fname)) return -1;

	int numComp = DfxGetProfileInt(fname, "ComponentList", "ncomp", 0);

	if (numComp > 0)
	{
		char strNum[64];
		string strSection;
		string strSectionBase("Component_");
		string strKeyName("strName");
		string strKeyConc("fConc");

		*pfCompConc = new double[numComp];
		*pstrCompName = new string[numComp];

		for (int i = 0; i < numComp; i++)
		{
			sprintf(strNum, "%d", i + 1);
			strSection = strSectionBase + strNum;
			(*pstrCompName)[i] = DfxGetProfileString(fname, strSection.c_str(), strKeyName.c_str(), "\0");
			(*pfCompConc)[i] = DfxGetProfileDouble(fname, strSection.c_str(), strKeyConc.c_str(), -1);
		}
	}

	*pNumberOfComponents = numComp;

	return numComp;
}

int LoadSpectrum(const char* fname,
				 double* pStartFreq,
				 double* pStepFreq,
				 int* pNumberOfFreq,
				 double* *pfSpectrum)
{
	CIrSpectrum spc;

	if (!spc.Read(fname)) return -1;

	int kMin = spc.GetIndex(spc.fmin);
	int kMax = spc.GetIndex(spc.fmax) + 1;

	if (kMin < 0) return 0;

	if (kMax > spc.points) return 0;
	int np = kMax - kMin + 1;

	*pNumberOfFreq = np;		
	*pStartFreq = spc.GetFreq(kMin);		
	*pStepFreq = spc.res;

	*pfSpectrum = new double[np];

	for(int k = 0; k < np; k++)
	{
		(*pfSpectrum)[k] = spc.s[k + kMin];
	}

	return np;
}