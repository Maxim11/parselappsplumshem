// CSampleList.h: interface for the CSampleList class.
//
// ��������:
// ����� CSampleList ������������ ��� ������
// � lst-������ (������������ ���������� Envelope4Samples.exe).
//
// ������������ ������� CSamples



#ifndef _SAMPLESLIST_H_
#define _SAMPLESLIST_H_

#include <string>

using namespace std;

class CSamplesList
{
public:
	CSamplesList(const char* ptrLstName, bool flagExt = false);
	int GetError() const { return nError; }
	int GetNumOfSamples() const { return numberOfSamples; }
	int GetNumOfComponents() const { return numberOfComponents; }
	string GetSampleName(int i) const { return pSamplesNames[i]; }
	string GetComponetName(int i) const { return pComponentsNames[i]; }
	~CSamplesList();
private:
	int nError;				// ��� ������
	int numberOfSamples;	// ����� ��������
	int numberOfComponents;	// ����� ���������
	string* pSamplesNames;	// ������ ���� ��������
	string* pComponentsNames;// ������ ���� ���������
};


#endif// _SAMPLESLIST_H_

