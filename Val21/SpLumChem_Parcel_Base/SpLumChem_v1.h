#ifndef _SPLUMCHEM_H_
#define _SPLUMCHEM_H_

#ifdef _IMPORTING_
#define _DLL_API_ __declspec(dllimport)
#else
#define _DLL_API_ __declspec(dllexport)
#endif


#include "SpLumChemStructs.h"

//  --------------------    IRCalibrate   -------------------------------- //
//  
//	interface to IRCalibrate class
//  class to create quantitative calibration
//
// --------------------------------------------------------------------------

// Description:
// Construct a quantitative calibration

//_DLL_API_ void*  IRCalibrate_Create(vector<CSampleProxy> *pSamples, // array of samples
//					const CFreqScale *FreqScale, // ��������� ����� ��������
//					const CSpecPreproc *prePar, // preprocessing parameters
//					int nModelType, // calibartion model type
//					int numFactors,  // number of factors
//					CQnt_Parcel *pQnt); // created model

_DLL_API_ void*  IRCalibrate_Create(vector<CSampleProxy> *pSamples, // array of samples
					const CFreqScale *FreqScale, // ��������� ����� ��������
					const CSpecPreproc *prePar, // preprocessing parameters
					int nModelType, // calibartion model type
					int numFactors,  // number of factors
					CQnt_Parcel **pQnt); // created model


_DLL_API_ void*  IRCalibrate_Create(vector<CSampleProxy> *pSamples, // array of samples
					const CFreqScale *FreqScale, // ��������� ����� ��������
					const CSpecPreproc *prePar, // preprocessing parameters
                    const MODEL_MMP *pModelPar,
	         		 int numPCAfactors);

_DLL_API_ void*  IRCalibrate_Create(vector<CSampleProxy> *pSamples, // array of samples
					const CFreqScale *FreqScale, // ��������� ����� ��������
					const CQnt_Parcel *pQnt); // created model


// Description:
// Return the TransType

_DLL_API_ int  IRCalibrate_GetTransType(void* pCalib);


// Description:
// Destroy the object of IRCalibrate class

_DLL_API_ void IRCalibrate_Destroy(void* pCalib);

// Description:
// Return the number of variables in calibration

_DLL_API_ int IRCalibrate_GetNumVariables(void* pCalib);

// Description:
// Return the number of components in calibration

_DLL_API_ int IRCalibrate_GetNumComponents(void* pCalib); 

// Description:
// Return the number of freedom degrees

_DLL_API_ int IRCalibrate_GetNumFreedomDegrees(void* pCalib);

// Description:
// Return the error of the quantitative calibration

_DLL_API_ int IRCalibrate_GetError(void* pCalib);

// Description:
// Return the SEC (standard error of calibartion) for a given component

_DLL_API_ int IRCalibrate_GetSEC(void* pCalib,
								   const char* compName, // component name
								   double* sec); // SEC for this component

// Description:
// Return the R^2 statistic for a given component

_DLL_API_ int IRCalibrate_GetR2stat(void* pCalib,
								   const char* compName, // component name
								   double* R2stat); // R^2 statistic for this component


// Description:
// Save the quantitative calibration in file

_DLL_API_ int IRCalibrate_Save(void* pCalib, const char* fileName);

_DLL_API_ const char* IRCalibrate_GetSampleName(void* pCalib,
									  int nSample); // sample order number
		

_DLL_API_ const char* IRCalibrate_GetSampleSpecName(void* pCalib,
												  int nSample, // sample order number
												  int nSpec);   // spectrum order number

_DLL_API_ void* IRCalibrate_Create(const char* cal_file);



// Description:
// Return the Mahalanobis distances for all spectra in the sample

_DLL_API_ void IRCalibrate_GetSampleDistances(void* pCalib,
												  int nSample, // sample order number
											      double* dist); // array of distances to be returned

_DLL_API_ void IRCalibrate_GetSampleNormDistances(void* pCalib,
												  int nSample, // sample order number
											      double* dist); // array of distances to be returned



// Description:
// Return the Mahalanobis distances for mean spectrum of the sample

_DLL_API_ double IRCalibrate_GetSampleDistanceAv(void* pCalib,
												  int nSample); // sample order number

_DLL_API_ double IRCalibrate_GetSampleNormDistanceAv(void* pCalib,
												  int nSample); // sample order number


// Description:
// Return the Mahalanobis distances for the given spectrum in the sample

_DLL_API_ void IRCalibrate_GetSampleSpecDistance(void* pCalib,
												  int nSample, // sample order number
												  int nSpec,   // spectrum order number
											      double* dist); // the distance to be returned

// Description:
// Return the maximum possible frequency range
// in fMinFreq and fMaxFreq, and Error as result

_DLL_API_ int  IRCalibrate_GetMaxClbFreqRange(void* pCalib, double *fMinFreq, double *fMaxFreq);



//  --------------------    IRSecv   -------------------------------- //
//  
//	interface to IRSecv class
//  (class to get standard ertror of cross-validation of the model)
//
// --------------------------------------------------------------------------

// Description:
// Construct an object of IRSecv class

_DLL_API_ void* IRSecv_Create(const char *fname);

//_DLL_API_ void* IRSecv_Create(int numSamples, // number of samples
//					const CSampleProxy* samples, // array of samples
//					const CSpecPreproc& prePar, // preprocessing parameters
//					int nModelType, // calibartion model type (default - PLS)
//					int numFactors = 0); // number of factors

// Description:
// Destroy the object of IRSecv class

_DLL_API_ void IRSecv_Destroy(void* pSECV);

// Description:
// Return the error of cross-validation

_DLL_API_ int IRSecv_GetError(void* pSECV);

// Description:
// Return the number of samples

_DLL_API_ int IRSecv_GetNumSamples(void* pSECV);


// Description:
// Return the predicted concentrations for the spectrum of given sample

_DLL_API_ void IRSecv_GetPredConc(void* pSECV,
									int nSample, // sample order number
									int nSpec, // spectrum order number
									double* compConc); // array of predicted component concentrations

// Description:
// Return the predicted concentrations for the mean spectrum of given sample

_DLL_API_ void IRSecv_GetPredConcAv(void* pSECV,
									int nSample, // sample order number
									double* compConc); // array of predicted component concentrations


_DLL_API_ void IRSecv_GetRefConc(void* pSECV,
									int nSample, // sample order number
									double* compConc); // array of reference data for this sample


// Description:
// Return the SECV(standard error of cross validation) for a given component

_DLL_API_ int IRSecv_GetSECV(void* pSECV,
							const char* compName, // the  component name of interest
							double* secv); // SECV for this component

// Description:
// Return the R^2 statistic for a given component

_DLL_API_ int IRSecv_GetR2stat(void* pSECV,
								   const char* compName, // component name
								   double* R2stat); // R^2 statistic for this component

// Description:
// Return the F statistic for a given component

_DLL_API_ int IRSecv_GetFstat(void* pSECV,
								   const char* compName, // component name
								   double* Fstat); // F statistic for this component

// Description:
// Give the SEC array for model with excluded given sample
// Return the error code

_DLL_API_ int IRSecv_GetSEC(void* pSECV,
							int nSample, // excluded sample order number
							double* sec); // array[numberOfComponents] of sec for this sample



//  --------------------    IRPredict   -------------------------------- //
//  
//	interface to IRPredict class 
//  (class to predict spectrum with given quantitative model)
//
// --------------------------------------------------------------------------

// Description:
// Construct an object of IRPredict class from file with quantitative calibration

_DLL_API_ void *IRPredict_Create(const char* cal_file);

// Description:
// Destroy the object of IRPredict class

_DLL_API_ void IRPredict_Destroy(void* pPredict);

// Description:
// Predict a spectrum

_DLL_API_ int IRPredict_PredictSpectrum(void* pPredict,
										const CFreqScale& dsc, // spectrum descriptor
										const double* data); // spectrum data
// Description:
// Return the error of prediction

_DLL_API_ int IRPredict_GetError(void* pPredict);

// Description:
// Return the number of components to can be predected

_DLL_API_ int IRPredict_GetNumComponents(void* pPredict);

// Description:
// Return the predicted concentration of the component of interest for given spectrum

_DLL_API_ int IRPredict_GetConcentration(void* pPredict,
									const char* compName, // the component name of interest
									double* conc, // the predicted concentration of this conponent
									double* concErr); // the estimated error of predicted concentration

// Description:
// Get reference data
_DLL_API_ void IRPredict_GetRefConc(void* pPredict,
									int nSample, // sample order number
									double* compConc); // array of reference concentrations

// Description:
// Get self-prediction data
_DLL_API_ void IRPredict_GetPredConc(void* pPredict,
									int nSample, // sample order number
									int nSpec, // spec order number
									double* compConc); // array of predicted components concentrations

// Description:
// Get self-prediction data for mean spectrum of the sample
_DLL_API_ void IRPredict_GetPredConcAv(void* pPredict,
									int nSample, // sample order number
									double* compConc); // array of predicted components concentrations


// Description:
// Return the Mahalanobis distance for a given spectrum


_DLL_API_ double IRPredict_GetExtSpecDistance(void* pPredict,
										   const CFreqScale& dsc, // spectrum descriptor
										   const double* data); // spectrum data

_DLL_API_ double IRPredict_GetExtSpecNormDistance(void* pPredict,
										   const CFreqScale& dsc, // spectrum descriptor
										   const double* data); // spectrum data


//  --------------------    QltAnaliz_PCA   -------------------------------- //
//  
//	interface to QltAnaliz_PCA class
//  class to create qualitative calibration
//
// --------------------------------------------------------------------------

// Description:
// Construct a qualitative calibration

_DLL_API_ void* QltAnaliz_Create(vector<CSampleProxy> *pSamples, // array of samples
					const CFreqScale *FreqScale, // ��������� ����� ��������
					const CSpecPreproc *prePar, // preprocessing parameters
					int numFactors = 0); // number of factors

// Description:
// Construct a qualitative calibration from file

_DLL_API_ void* QltAnaliz_Load(const char* fileName);


// Description:
// Destroy a qualitative calibration

_DLL_API_ void QltAnaliz_Destroy(void* pQlt);

// Description:
// Save the qualitative calibration in file

_DLL_API_ void QltAnaliz_Save(void* pQlt, const char* fileName);

// Description:
// Return the error of the qualitative calibration

_DLL_API_ int  QltAnaliz_GetError(void* pQlt);

// Description:
// Return the Mahalanobis distance for a given spectrum

//former _DLL_API_ double QltAnaliz_PCA_GetDistance(...
_DLL_API_ double QltAnaliz_GetExtSpecDistance(void* pQlt,
										   const CFreqScale& dsc, // spectrum descriptor
										   const double* data); // spectrum data


_DLL_API_ double QltAnaliz_GetExtSpecNormDistance(void* pQlt,
										   const CFreqScale& dsc, // spectrum descriptor
										   const double* data); // spectrum data

// Description:
// Return the Mahalanobis distances for all spectra in the sample

_DLL_API_ void QltAnaliz_GetSampleDistances(void* pQlt,
												  int nSample, // sample order number
											      double* dist); // array of distances to be returned

_DLL_API_ void QltAnaliz_GetSampleNormDistances(void* pQlt,
												  int nSample, // sample order number
											      double* dist); // array of distances to be returned

// Description:
// Return the Mahalanobis distance for mean spectrum of the sample

_DLL_API_ double QltAnaliz_GetSampleDistanceAv(void* pQlt,
												  int nSample); // sample order number


_DLL_API_ double QltAnaliz_GetSampleNormDistanceAv(void* pQlt,
												  int nSample); // sample order number





// Description:
// Return the Mahalanobis distances for the given spectrum in the sample

_DLL_API_ void QltAnaliz_GetSampleSpecDistance(void* pQlt,
												  int nSample, // sample order number
												  int nSpec,   // spectrum order number
											      double* dist); // the distance to be returned

// Description:
// Return the number of samples in the qualitative model

_DLL_API_ int  QltAnaliz_GetNumSamples(void* pQlt);

// Return the number of spectra for the specific sample 
_DLL_API_ int  QltAnaliz_GetNumSampleSpecs(void* pQlt,
											   int nSample); // sample order number

// Description:
// Return the number of variables in the qualitative model

_DLL_API_ int  QltAnaliz_GetNumVariables(void* pQlt);

_DLL_API_ const char* QltAnaliz_GetSampleName(void* pQlt,
									  int nSample); // sample order number
		

_DLL_API_ const char* QltAnaliz_GetSampleSpecName(void* pQlt,
												  int nSample, // sample order number
												  int nSpec);   // spectrum order number

_DLL_API_ void QltAnaliz_AddSamples(void *pQlt,
					int numSamples, // number of samples
					const CSampleProxy* pSamples, // array of samples
					const CSpecPreproc &prePar, // preprocessing parameters
					int numFactors = 0); // number of factors

#endif // _SPLUMCHEM_H_
