#include "QntCalibration.h"
#include "ModelPLS.h"
#include "ModelPCR.h"
#include "ModelHSO.h"
#include <math.h>
#include "PreprocUtils.h"
#include "MatrixUtilities.h"
#include "PbtFile.h"
#include "HSOMatrixUtils.h"

#include "LLTIMER.h"
extern void log_me(const char* msg, bool bAppend=true);

//#include "Debug.h"


//#include <time.h>

//double durationPLSinit, durationPLScycle, durationPLSend, durationPLSpredict;
//double durationQntSECV, durationQntSEC, durationQntCreateModel;

//double durationPCA;//QntSECV, durationQntSEC, durationQntCreateModel;

//clock_t startGlobal;

extern void AppendToDebugReport(const char* str);

CQntCalibration::~CQntCalibration()
{
	/*FILE *fgh = fopen("Speed_test.txt", "a");
	fprintf(fgh, "durationPLSinit=%.3f s\tdurationPLScycle=%.3f s\tdurationPLSend=%.3f s\n", durationPLSinit/CLOCKS_PER_SEC, durationPLScycle/CLOCKS_PER_SEC, durationPLSend/CLOCKS_PER_SEC);
	fprintf(fgh, "durationPLSpredict=%.3f s\n", durationPLSpredict/CLOCKS_PER_SEC);
	fprintf(fgh, "durationQntSEC=%.3f s\n", durationQntSEC/CLOCKS_PER_SEC);
	fprintf(fgh, "durationQntSECV=%.3f s\n", durationQntSECV/CLOCKS_PER_SEC);
	fprintf(fgh, "durationQntCreateModel=%.3f s\n", durationQntCreateModel/CLOCKS_PER_SEC);

	clock_t finish = clock();
	fprintf(fgh, "TotalTime=%.3f s\n", (double)(finish - startGlobal)/CLOCKS_PER_SEC);
	fclose(fgh);*/

//	FILE *fgh = fopen("Speed_test.txt", "a");
//	fprintf(fgh, "durationPCA=%.3f\n", durationPCA/CLOCKS_PER_SEC);
//	fclose(fgh);

#ifdef _DEBUG_
		AppendToDebugReport("Start destructor");
#endif 

	if (!flagBatch)
	{
		delete pClbSamples;
	}

	delete pSEVSamples;

	delete pModel;
	delete pTransData;
	delete pQnt;

//	FreeFFTW();
//	delete pQntHSO;
//	delete pModelHQO;
	delete[] pUseFreq;

#ifdef _DEBUG_
		AppendToDebugReport("End destructor");
#endif 
}

// ���������� �������� SEC ��� ������� ����������
int CQntCalibration::GetSEC(string sCompName, double* sec)
{
//	string sCompName(compName);

	int ind = pClbSamples->FindComponentIndex(sCompName);

	nError = pClbSamples->GetError();

	if (nError == ER_OK)
	{
		*sec = vSEC(ind + 1);
	}

	return nError;
}

// ���������� �������� ���������� R^2 ��� ������� ����������
int CQntCalibration::GetR2Stat(string sCompName, double* R2Stat)
{
//	string sCompName(compName);

	int ind = pClbSamples->FindComponentIndex(sCompName);

	if (nError == ER_OK)
	{
		*R2Stat = vR2Stat(ind + 1);
	}

	return nError;
}

// �������� �����������, ������������ � ��������� SpLumPro.exe
CQntCalibration::CQntCalibration(vector<CSampleProxy> *pSamples, // array of samples
					const CFreqScale *FreqScale, // ��������� ����� ��������
					const CSpecPreproc *prePar, // preprocessing parameters
					int nModelType, // calibartion model type
					int numFactors, // number of factors
					CQnt_Parcel **pQntOut) // created model
{
#ifdef _DEBUG_
    FILE *fgh = fopen("debug.txt", "a");

	fprintf(fgh, "freqScale.fStartFreq = %.3f\n", FreqScale->fStartFreq);
	fprintf(fgh, "freqScale.fEnd = %.3f\n", FreqScale->fStartFreq + FreqScale->fStepFreq * FreqScale->nNumberOfFreq);

	int nFExcl = prePar->vFreqExcluded.size();
	fprintf(fgh, "size of vFreqExcluded=%d\n", nFExcl);

	for (int m=0 ; m < nFExcl; m++)
	{
		fprintf(fgh, "%d\n", prePar->vFreqExcluded[m]);
	}

	fclose(fgh);

	char strBuf[256];

	int nSample = 1;
	int nSp = (*pSamples)[nSample-1].mSpectra.size();
	int nF = (*pSamples)[nSample-1].mSpectra[0].size();
	sprintf(strBuf, "sample %d numSpectra = %d numFreq = %d\n\n", nSample, nSp, nF);
	AppendToDebugReport(strBuf);

	sprintf(strBuf, "numFreq= %d\n", FreqScale->nNumberOfFreq);
	AppendToDebugReport(strBuf);



	nSample = pSamples->size();
	nSp = (*pSamples)[nSample-1].mSpectra.size();
	nF = (*pSamples)[nSample-1].mSpectra[nSp-1].size();
	sprintf(strBuf, "sample %d numSpectra = %d numFreq = %d\n\n", nSample, nSp, nF);
	AppendToDebugReport(strBuf);
	sprintf(strBuf, "last spectrum, last freq. - 1 = %e", (*pSamples)[nSample-1].mSpectra[nSp-1][nF-2]);
	AppendToDebugReport(strBuf);
	sprintf(strBuf, "last spectrum, last freq. = %e", (*pSamples)[nSample-1].mSpectra[nSp-1][nF-1]);
	AppendToDebugReport(strBuf);

	sprintf(strBuf, "number of Components names = %d", prePar->strCompName.size());
	AppendToDebugReport(strBuf);

#endif

char strlog[256];
InitLLTimer();
StartLLTimer();
double tll1 = GetLLTimerSec();

	Init();

tll1 = GetLLTimerSec()-tll1;
sprintf(strlog, "Init(): %.4f", tll1);
log_me(strlog, false);
tll1 = GetLLTimerSec();

	*pQntOut = 0;


#ifdef _DEBUG_
	AppendToDebugReport("end of init");
#endif

	pClbSamples = new CSamples(pSamples, FreqScale, &(prePar->strCompName));

#ifdef _DEBUG_
	AppendToDebugReport("samples were created");
#endif

	int numComponents = (*pSamples)[0].vCompConc.size();

	vSEC.ReSize(numComponents);
	vR2Stat.ReSize(numComponents);

	specPreprocData.nModelType = nModelType;

	specPreprocData.numberOfExcludedFreq = prePar->vFreqExcluded.size();
	specPreprocData.vUseFreq.resize(FreqScale->nNumberOfFreq, 1);

	if (specPreprocData.numberOfExcludedFreq)
	{
		for(int i = 0; i < prePar->vFreqExcluded.size(); i++)
		{
			int ind = prePar->vFreqExcluded[i];
			specPreprocData.vUseFreq[ind] = 0;
		}
	}

tll1 = GetLLTimerSec()-tll1;
sprintf(strlog, "PreCalibrate: %.4f", tll1);
log_me(strlog);
tll1 = GetLLTimerSec();

	Calibrate(prePar, nModelType, numFactors);

tll1 = GetLLTimerSec()-tll1;
sprintf(strlog, "Calibrate(): %.4f", tll1);
log_me(strlog);
tll1 = GetLLTimerSec();

	if (nError == ER_OK)
	{
		pQnt = new CQnt_Parcel();
		FillQnt(pQnt);
		*pQntOut = pQnt;
#ifdef _DEBUG_
		AppendToDebugReport("ER_OK end Qnt");
#endif 
	}
tll1 = GetLLTimerSec()-tll1;
sprintf(strlog, "Done: %.4f", tll1);
log_me(strlog);
tll1 = GetLLTimerSec();
}

// ����������� ��� ������ ������, ������������ � ��������� SpLumPro.exe
CQntCalibration::CQntCalibration(vector<CSampleProxy> *pSamples, // array of samples
					const CFreqScale *FreqScale, // ��������� ����� ��������
					const CSpecPreproc *prePar, // preprocessing parameters
					const MODEL_MMP *pModelPar, // ��������� � ����������� ��� ���������� ������
					//int numPCAfactors, // ����� �����. ��������� ��� PCA ������
					CQnt_Parcel **pQntOut) // created model

{

 /*   FILE *fgh = fopen("debug.txt", "w");

	fprintf(fgh, "prePar.fMinFreq = %.3f\n", prePar->fMinFreq);
	fprintf(fgh, "prePar.fMaxFreq = %.3f\n", prePar->fMaxFreq);

	fprintf(fgh, "freqScale.fStartFreq = %.3f\n", FreqScale->fStartFreq);
	fprintf(fgh, "freqScale.fEnd = %.3f\n", FreqScale->fStartFreq + FreqScale->fStepFreq * FreqScale->nNumberOfFreq);

	int nFExcl = prePar->vFreqExcluded.size();
	fprintf(fgh, "size of vFreqExcluded=%d\n", nFExcl);

	for (int m=0 ; m < nFExcl; m++)
	{
		fprintf(fgh, "%d\n", prePar->vFreqExcluded[m]);
	}

	fclose(fgh);*/
#ifdef _DEBUG_
	AppendToDebugReport("Start Qnt. HQO");
#endif

	AppendToDebugReport("Calling Init()");
	Init();
	AppendToDebugReport("Init() OK");
	

	*pQntOut = 0;

	pClbSamples = new CSamples(pSamples, FreqScale, &(prePar->strCompName));

	// debug
	Matrix mSp;
	pClbSamples->GetMSpectra(mSp);
	//PrintMatrix(mSp, "mSp_Create.dat");


	int numComponents = (*pSamples)[0].vCompConc.size();
//	specPreprocData.vComponentCorrectionsAdd.ReSize(numComponents);
//	specPreprocData.vComponentCorrectionsMul.ReSize(numComponents);

	vSEC.ReSize(numComponents);
	vR2Stat.ReSize(numComponents);

//	if (!prePar->vCompConcCorrAdd.empty())
//	{
//		specPreprocData.vComponentCorrectionsAdd = VDbl2Row(prePar->vCompConcCorrAdd);
//	}
//	else
//	{
//		specPreprocData.vComponentCorrectionsAdd = 0.0;
//	}

//	if (!prePar->vCompConcCorrMul.empty())
//	{
//		specPreprocData.vComponentCorrectionsMul = VDbl2Row(prePar->vCompConcCorrMul);
//	}
//	else
//	{
//		specPreprocData.vComponentCorrectionsMul = 1.0;
//	}

	specPreprocData.nModelType = MODEL_HSO;

	specPreprocData.numberOfExcludedFreq = prePar->vFreqExcluded.size();
	specPreprocData.vUseFreq.resize(FreqScale->nNumberOfFreq, 1);



	if (specPreprocData.numberOfExcludedFreq)
	{
		for(int i = 0; i < prePar->vFreqExcluded.size(); i++)
		{
			int ind = prePar->vFreqExcluded[i];
			specPreprocData.vUseFreq[ind] = 0;
		}
	}

//	Calibrate(prePar, pModelPar, numPCAfactors);
	Calibrate(prePar, pModelPar);

	if (nError == ER_OK)
	{
#ifdef _DEBUG_
	AppendToDebugReport("Qnt HQO model end. ER_OK");
#endif
		pQnt = new CQnt_Parcel();
		FillQnt(pQnt);
		*pQntOut = pQnt;
	}
}


	// ��� �����������
CQntCalibration::CQntCalibration(vector<CSampleProxy> *pSamples, // array of samples
		const CFreqScale *FreqScale, // ��������� ����� ��������
		const CSpecPreproc *prePar, // preprocessing parameters
		const CQntOutput *pQntOutput) // �������� (��� ����������)
{
	Init();
/*	durationPLSinit = 0;
	durationPLScycle = 0;
	durationPLSend = 0;
	durationPLSpredict = 0;
	durationQntSECV = 0;
	durationQntSEC = 0;
	durationQntCreateModel = 0;

	startGlobal = clock();*/

	//durationQNT = 0;

//	prePar->vCompConcCorrAdd = 0;
//	prePar->vCompConcCorrMul = 1.0;
	
#ifdef _DEBUG_
	AppendToDebugReport("CQntCalibration::CQntCalibration Start");
#endif

	int numComponents = (*pSamples)[0].vCompConc.size();

	mOptFlags.bSEC = pQntOutput->bSEC;
	mOptFlags.bSEV = pQntOutput->bSEV;
	mOptFlags.bSECV = pQntOutput->bSECV;

/*	vSEC.CleanUp();
	vR2Stat.CleanUp();

	vSECV.CleanUp();
	vR2StatSECV.CleanUp();
	vFStat.CleanUp();*/

	// for SEC
	vSEC.ReSize(numComponents);
	vR2Stat.ReSize(numComponents);

	// for SECV
	vSECV.ReSize(numComponents);
	vR2StatSECV.ReSize(numComponents);
	vFStat.ReSize(numComponents);

	// for SEV
	vSEV.ReSize(numComponents);
	vR2StatSEV.ReSize(numComponents);
	vErr.ReSize(numComponents);
	vSDV.ReSize(numComponents);





	freqScale.fStartFreq = FreqScale->fStartFreq;
	freqScale.fStepFreq = FreqScale->fStepFreq;
	freqScale.nNumberOfFreq = FreqScale->nNumberOfFreq;


	specPreprocData.numberOfExcludedFreq = prePar->vFreqExcluded.size();
	specPreprocData.vUseFreq.resize(freqScale.nNumberOfFreq, 1);
/*
AppendToDebugReport("CQntCalibration::CQntCalibration 2");
char buff[256];
sprintf(buff, "prePar->nSpType=%d", prePar->nSpType);
AppendToDebugReport(buff);
sprintf(buff, "prePar->strPreprocPar.size()=%d", prePar->strPreprocPar.size());
AppendToDebugReport(buff);
sprintf(buff, "prePar->strPreprocParTrans.size()=%d", prePar->strPreprocParTrans.size());
AppendToDebugReport(buff);
sprintf(buff, "prePar->strPreprocPar=%s", prePar->strPreprocPar.c_str());
AppendToDebugReport(buff);
sprintf(buff, "prePar->strPreprocParTrans=%s", prePar->strPreprocParTrans.c_str());
AppendToDebugReport(buff);
*/
	specPreprocData.strPreprocParTrans = prePar->strPreprocParTrans;
//AppendToDebugReport("CQntCalibration::CQntCalibration 3");


	if (specPreprocData.numberOfExcludedFreq)
	{
		for(int i = 0; i < prePar->vFreqExcluded.size(); i++)
		{
			int ind = prePar->vFreqExcluded[i];
			specPreprocData.vUseFreq[ind] = 0;
		}
	}


	pClbSamples = new CSamples(pSamples, FreqScale, &(prePar->strCompName));

	nError = pClbSamples->GetError();
}


// �������� ��� ������������ � ����������� ������
CQntCalibration::CQntCalibration(vector<CSampleProxy> *pSamples, // array of samples
					const CFreqScale *FreqScale, // ��������� ����� ��������
					const CQnt_Parcel *pQnt) //  ��������� ����� ������
{
	Init();

//	AppendToDebugReport("end of init");

	pClbSamples = new CSamples(pSamples, FreqScale, 0);

	// load model
	switch(pQnt->m_Model.nModelType)
		{
		case MODEL_PCR:

			pModel = new CModelPCR;
			break;
		case MODEL_PLS:

			pModel = new CModelPLS;
			break;

		case MODEL_HSO:

			pModel = new CModelHSO;
			//AppendToDebugReport("end empty HSO creation.");

			break;

		default:
			nError = ER_BAD_QNT_VER;

			return;
		}

	pModel->LoadFromQnt_Parcel(pQnt->m_Model);
//	AppendToDebugReport("samples were created");

	specPreprocData.LoadFromQnt_Parcel(FreqScale, pQnt);

/*	if (!pQnt->strPreprocParTrans.empty())
	{
		specPreprocDataTrans.LoadTransFromQnt_Parcel(FreqScale, pQnt);
	}*/

	fMeanMahDist = pQnt->fMeanMahDist;

//	pTransData = new CTransData();

	// load freqScale from pClbSamples
	freqScale.fStartFreq = FreqScale->fStartFreq;
	freqScale.fStepFreq = FreqScale->fStepFreq;
	freqScale.nNumberOfFreq = FreqScale->nNumberOfFreq;
}

// �����������, ������������ ��� ���������� ������������ �����������
/*CQntCalibration::CQntCalibration(int numSamples, // number of samples
					const CSampleProxy* pSamples, // array of samples
					const CSpecPreproc &prePar, // preprocessing parameters
					int nModelType, // calibartion model type
					int numFactors, // number of factors
					const char *strPbtName) // name of pbt-file
{
	Init();

	CPbt *pPbt = new CPbt(strPbtName);

	if ((nError = pPbt->GetError()) != ER_OK)
	{
		return;
	}

	// new transfer - added 17.04.2005
	pTransData->nTransType = 4;

	pTransData->SetBiasSp(pPbt->m_Correct.vBiasSp);
	pTransData->SetSlopeSp(pPbt->m_Correct.vSlopeSp);


	pClbSamples = new CSamples(numSamples, pSamples);

	if ((nError = pClbSamples->GetError()) != ER_OK)
	{
		return;
	}

	if (pSamples[0].fCompConcCorr)
	{
		specPreprocData.bCompCorr = true;
		specPreprocData.vComponentCorrections.ReSize(pSamples[0].nNumberOfComponents);
		specPreprocData.vComponentCorrections << pSamples[0].fCompConcCorr;
	}
	else
	{
		specPreprocData.bCompCorr = false;
	}

	pClbSamples->PreprocessSpectra4Transfer(pPbt, false);

	if ((nError = pClbSamples->GetError()) != ER_OK)
	{
		return;
	}

	char strPreprocPbt[32];

	strcpy(strPreprocPbt, pPbt->GetPreprocStr());

	CSpecPreproc m_preParCut = prePar;

	if (strlen(strPreprocPbt))
	{
		// exclude from prePar.strPreprocPar preprocessing
		// which ure using in Pbt.strpreprocPar

		int nLen = strlen(prePar.strPreprocPar);

		int n = 0;
		for (int i = 0; i < nLen; i++)
		{
			char ch = prePar.strPreprocPar[i];

			if (strchr(strPreprocPbt, ch) == NULL)
			{
				m_preParCut.strPreprocPar[n] = ch;
				n++;
			}
		}
		m_preParCut.strPreprocPar[n] = '\0';
	}

	if (pPbt->GetSpType() == STYPE_ABS)
	{
		m_preParCut.nSpType = STYPE_TRA;
	}

	Calibrate(m_preParCut, nModelType, numFactors);

	delete pPbt;
}*/

// ������������� ������ � ���������� ������������� ������
int CQntCalibration::Calibrate(const CSpecPreproc *prePar,
					int nModelType,
					int numFactors)
{
char strlog[256];
double tll1 = GetLLTimerSec();
tll1 = GetLLTimerSec()-tll1;
sprintf(strlog, "Calibrate0: %.4f", tll1);
log_me(strlog);
tll1 = GetLLTimerSec();

	freqScale.fStartFreq = pClbSamples->GetStartFreq();
	freqScale.fStepFreq = pClbSamples->GetStepFreq();
	freqScale.nNumberOfFreq = pClbSamples->GetNumFreq();

	specPreprocData.fMinFreq = freqScale.fStartFreq;
	specPreprocData.fMaxFreq = freqScale.fStartFreq + freqScale.fStepFreq * (freqScale.nNumberOfFreq - 1);
	specPreprocData.numberOfChannels = freqScale.nNumberOfFreq;

	specPreprocData.nSpType = prePar->nSpType;
	specPreprocData.strPreprocPar = prePar->strPreprocPar;
	specPreprocData.strPreprocParTrans = prePar->strPreprocParTrans;

#ifdef _DEBUG_
		AppendToDebugReport("Start Calibrate");
#endif 

	nError = pClbSamples->PreprocessSpectra(specPreprocData, mX);

tll1 = GetLLTimerSec()-tll1;
sprintf(strlog, "Calibrate1: %.4f", tll1);
log_me(strlog);
tll1 = GetLLTimerSec();

	if (nError != ER_OK)
	{
#ifdef _DEBUG_
		AppendToDebugReport("Error preprocessing spectra");
#endif
		return nError;
	}

#ifdef _DEBUG_
		AppendToDebugReport("Spectra were preprocessed");
#endif

#ifdef _DEBUG_
	PrintMatrix(mX, "mX");
#endif

	nError = pClbSamples->PreprocessComponents(specPreprocData, mY);

tll1 = GetLLTimerSec()-tll1;
sprintf(strlog, "Calibrate2: %.4f", tll1);
log_me(strlog);
tll1 = GetLLTimerSec();

	if (nError != ER_OK)
	{
#ifdef _DEBUG_
		AppendToDebugReport("Error preprocessing components");
#endif
		return nError;
	}

#ifdef _DEBUG_
		AppendToDebugReport("Components were preprocessed");
#endif

	switch(nModelType)
	{
	case MODEL_PLS:
		pModel = new CModelPLS(mX, mY, numFactors);
		nError = pModel->GetError();

		break;
	case MODEL_PCR:
		pModel = new CModelPCR(mX, mY, numFactors);
		nError = pModel->GetError();

		break;
	default:
		nError = ER_MODEL_TYPE;
	}

tll1 = GetLLTimerSec()-tll1;
sprintf(strlog, "Calibrate3: %.4f", tll1);
log_me(strlog);
tll1 = GetLLTimerSec();

	if (nError == ER_OK)
	{
#ifdef _DEBUG_
		AppendToDebugReport("Model was created");
#endif
		CalculateSEC();
#ifdef _DEBUG_
		AppendToDebugReport("SEC was calculated");
#endif

		CalculateMahDist();

tll1 = GetLLTimerSec()-tll1;
sprintf(strlog, "Calibrate4: %.4f", tll1);
log_me(strlog);
tll1 = GetLLTimerSec();

#ifdef _DEBUG_
		AppendToDebugReport("MahDist were calculated");
#endif

//		AppendToDebugReport("End of Mah. dist");
	}
#ifdef _DEBUG_
	else
	{
		AppendToDebugReport("Error in model creation");
	}
#endif

	return nError;
}

// ������������� ������ � ���������� ������������� ������
//int CQntCalibration::Calibrate(const CSpecPreproc *prePar,
//					const MODEL_MMP *pModelPar,
//					int numPCAfactors)
int CQntCalibration::Calibrate(const CSpecPreproc *prePar,
					const MODEL_MMP *pModelPar)
{
	//AppendToDebugReport("start of calibr.");
#ifdef _DEBUG_
	AppendToDebugReport("Start Calibrate HSO");
#endif

	freqScale.fStartFreq = pClbSamples->GetStartFreq();
	freqScale.fStepFreq = pClbSamples->GetStepFreq();
	freqScale.nNumberOfFreq = pClbSamples->GetNumFreq();

	specPreprocData.fMinFreq = freqScale.fStartFreq;
	specPreprocData.fMaxFreq = freqScale.fStartFreq + freqScale.fStepFreq * (freqScale.nNumberOfFreq - 1);
	specPreprocData.numberOfChannels = freqScale.nNumberOfFreq;

	specPreprocData.nSpType = prePar->nSpType;
	specPreprocData.strPreprocPar = prePar->strPreprocPar;
	specPreprocData.strPreprocParTrans = prePar->strPreprocParTrans;


/*	if (prePar->vFreqExcluded.size())
	{
		specPreprocData.vFreqExcluded = prePar->vFreqExcluded;
	}*/

	// ��������� ������������� - ����������� �� �������� (������� 'H')
	specPreprocData.strPreprocPar += "H";


	//specPreprocData.Print("specPreprocData.dat");
//		FILE *fgh = fopen("tmp", "w");
	    // ������ �� ���������!
	//	specPreprocData.numberOfFreqExcluded = prePar.numFreqExcluded;

		
//		fclose(fgh);

	//AppendToDebugReport("start of preproc.");

	nError = pClbSamples->PreprocessSpectra(specPreprocData, mX);

	if (nError != ER_OK)
	{
#ifdef _DEBUG_
	AppendToDebugReport("Error Preprocessing Spectra");
#endif
		return nError;
	}
#ifdef _DEBUG_
	AppendToDebugReport("ER_OK. Spectra were preprocessed");
#endif
	// ��������� ������ 0 � 1
		// 1 - ������� ��������
		// 0 - ���

	//	specPreprocData.pFreqExcluded = new int[specPreprocData.numberOfChannels];

	int *pUseFreq = new int[specPreprocData.numberOfChannels];
	for (int k = 0; k < specPreprocData.numberOfChannels; k++)
	{
		pUseFreq[k] = 1;
	}
	if (prePar->vFreqExcluded.size() > 0)
	{
		for(int i = 0; i < prePar->vFreqExcluded.size(); i++)
		{
			int ind = prePar->vFreqExcluded[i];
			pUseFreq[ind] = 0;
		}
	}

	//AppendToDebugReport("end of preproc.");


	if (nError != ER_OK)
	{
		return nError;
	}

	nError = pClbSamples->PreprocessComponents(specPreprocData, mY);

	if (nError != ER_OK)
	{
		return nError;
	}

	//PrintMatrix(mX, "mX.dat");
	//PrintMatrix(mY, "mY.dat");

	//specPreprocData.Print("specPreprocData_preproc.dat");

#ifdef _DEBUG_
	AppendToDebugReport("Start ModelHSO");
#endif

	pModel = new CModelHSO(mX, mY, pUseFreq, pModelPar);//, numPCAfactors);
	nError = pModel->GetError();
	delete[] pUseFreq;

//	FILE *fgh = fopen("debug.txt", "w"); 
//	fprintf(fgh, "Error = %d\n", nError);
//	fclose(fgh);
	//AppendToDebugReport("end of HSO model");


	if (nError == ER_OK)
	{
#ifdef _DEBUG_
	AppendToDebugReport("ER_OK. End ModelHSO");
#endif
		CalculateSEC();
#ifdef _DEBUG_
	AppendToDebugReport("SEC was calculated");
#endif
		//CalculateMahDist();
	}

	return nError;
}


// �����������, ������������ � ��������� Envelope4Samples
// ��� ������ ����������� ���������� ��� ������� ������� ��������
/*CQntCalibration::CQntCalibration(void* pSamples, // pointer on samples
					const CSpecPreproc &prePar, // preprocessing parameters
					int nModelType, // calibartion model type
					int numFactors) // number of factors
{
	Init();

	pClbSamples = (CSamples*)pSamples;

	// new transfer - added 17.04.2005
	if (pClbSamples->GetTransFlag() == 4)
	{
		pTransData->nTransType = 4;
	}

	flagBatch = true;

	specPreprocData.bCompCorr = false;

	Calibrate(prePar, nModelType, numFactors);
}*/

// ���������� �������� � ������������ ������
/*void CQntCalibration::AddSamples(int numSamples, // number of samples
					const CSampleProxy* pSamples, // array of samples
					const CSpecPreproc &prePar, // preprocessing parameters
					int nModelType, // calibartion model type
					int numFactors) // number of factors
{
	CSamples theAddedSamples(numSamples, pSamples);

	pClbSamples->AddSamples(theAddedSamples);

	nError = pClbSamples->GetError();

	if (nError != ER_OK)
	{
		return;
	}


	freqScale = pSamples[0].frScale;

	// at first clear old preproc. data

	specPreprocData.Clear();

	specPreprocData.fMinFreq = prePar.fMinFreq;
	specPreprocData.fMaxFreq = prePar.fMaxFreq;

	if (pTransData->nTransType == 4)
	{
		if (theAddedSamples.thePreprocDataConst.nSpType == STYPE_ABS)
		{
			specPreprocData.nSpType = STYPE_TRA;
		}
		else
		{
			specPreprocData.nSpType = prePar.nSpType;
		}

		char strPreprocParConst[256];
		strncpy(strPreprocParConst, pClbSamples->thePreprocDataConst.strPreprocPar.c_str(), 256);

		if (strlen(strPreprocParConst))
		{
			// exclude from prePar.strPreprocPar preprocessing
			// which ure using in strpreprocParConst

			int nLen = strlen(prePar.strPreprocPar);

			for (int i = 0; i < nLen; i++)
			{
				char ch = prePar.strPreprocPar[i];

				if (strchr(strPreprocParConst, ch) == NULL)
				{
					specPreprocData.strPreprocPar += ch;
				}
			}
		}
		else
		{
			specPreprocData.strPreprocPar = string(prePar.strPreprocPar);
		}
	}
	else
	{
		specPreprocData.nSpType = prePar.nSpType;
		specPreprocData.strPreprocPar = string(prePar.strPreprocPar);
	}


	if (pSamples[0].fCompConcCorr)
	{
		specPreprocData.bCompCorr = true;
		specPreprocData.vComponentCorrections.ReSize(pSamples[0].nNumberOfComponents);
		specPreprocData.vComponentCorrections << pSamples[0].fCompConcCorr;
	}
	else
	{
		specPreprocData.bCompCorr = false;
	}


	nError = ER_OK;

	delete pModel;
	pModel = 0;

	pClbSamples->PreprocessSpectra(specPreprocData, mX);

	nError = pClbSamples->GetError();

	if (nError != ER_OK)
	{
		return;
	}

	pClbSamples->PreprocessComponents(specPreprocData, mY);

	nError = pClbSamples->GetError();

	if (nError != ER_OK)
	{
		return;
	}

	switch(nModelType)
	{
	case MODEL_PLS:
		pModel = new CModelPLS(mX, mY, numFactors);
		break;
	case MODEL_PCR:
		pModel = new CModelPCR(mX, mY, numFactors);
		break;
	default:
		nError = ER_MODEL_TYPE;
	}

	if (nError == ER_OK)
	{
		vSEC.CleanUp();
		vR2Stat.CleanUp();
		vMahalanobisDistances.CleanUp();

		CalculateSEC();

		CalculateMahDist();
	}
}*/

/*CQntCalibration::CQntCalibration(const char* fileName)
{
	Init();

	Load(fileName);
}*/



/*int CQntCalibration::Save(const char* fileName)
{
//	const int version = 19; // 27.09.2004 for adding of transfered calibration
	const int version = nClbNewMethod; // = 30 (28.02.2006 new method)

	FILE *fgh = fopen(fileName, "wb");

	if (!fgh)
	{
		nError = ER_FILE_CREATION;
	}
	else
	{
		string strClbName = "Quantitative Calibration";

		SaveString(strClbName, fgh);

		fwrite(&version, sizeof(int), 1, fgh);

		// write samples
		pClbSamples->Save(fgh, version);

		// write model
		specPreprocData.Save(fgh, version);

		pModel->Save(fgh, version);

		SaveColumnVector(vSEC, fgh);

		SaveColumnVector(vR2Stat, fgh);

		SaveColumnVector(vMahalanobisDistances, fgh);

		// write TransData
		pTransData->Save(fgh, version);

		fclose(fgh);
	}

	return nError;
}*/



/*int CQntCalibration::Load(const char* fileName)
{
	const int nVersion = 18; // 8.03.2004

	FILE *fgh = fopen(fileName, "rb");

	if (!fgh)
	{
		nError = ER_FILE_OPEN;
	}
	else
	{
		nError = ER_OK;

		string strClbName;
		LoadStringMy(strClbName, fgh);


		if (strClbName != "Quantitative Calibration")
		{
			nError = ER_BAD_QNT_FILE;
			fclose(fgh);

			return nError;
		}

		int version;

		fread(&version, sizeof(int), 1, fgh);

		if (version < nVersion)
		{
			nError = ER_BAD_QNT_VER;
			fclose(fgh);

			return nError;
		}

		pClbSamples = new CSamples();

		pClbSamples->Load(fgh, version);

		// read model

		specPreprocData.Load(fgh, version);

		int nModelType;
		fread(&nModelType, sizeof(int), 1, fgh);

		switch(nModelType)
		{
		case MODEL_PCR:

			pModel = new CModelPCR;
			break;
		case MODEL_PLS:

			pModel = new CModelPLS;
			break;

		case MODEL_HSO:

			pModel = new CModelHSO;
			//AppendToDebugReport("end empty HSO creation.");

			break;

		default:
			nError = ER_BAD_QNT_VER;
			fclose(fgh);

			return nError;
		}

		pModel->Load(fgh, version);

		//AppendToDebugReport("end load HSO");


		// read clb results
		LoadColumnVector(vSEC, fgh);

		LoadColumnVector(vR2Stat, fgh);

		LoadColumnVector(vMahalanobisDistances, fgh);

		int numClbSpectra = vMahalanobisDistances.Nrows();

		if (numClbSpectra > 1)
		{
			fMeanMahDist = sqrt(vMahalanobisDistances.SumSquare() / (numClbSpectra - 1));
		}
		else
		{
			fMeanMahDist = sqrt(vMahalanobisDistances.SumSquare());
		}

		// read TransData
		pTransData = new CTransData();
		pTransData->Load(fgh, version);
		fclose(fgh);

		// load freqScale from pClbSamples
		freqScale.fStartFreq = pClbSamples->GetStartFreq();
		freqScale.fStepFreq = pClbSamples->GetStepFreq();
		freqScale.nNumberOfFreq = pClbSamples->GetNumFreq();
	}

	
	return nError;
}*/

// ������������� ����������� ������:
// ��������� �������� ������������ ��������� �� ����������� �������������� ������
// ��� ������� �������.

int CQntCalibration::PredictSpectrum(const CFreqScale *dsc,
						const VDbl *vSp, VDbl *pvCompConc)
{
	RowVector vSpectrum(dsc->nNumberOfFreq);

	for (int i = 0; i < dsc->nNumberOfFreq; i++)
	{
		vSpectrum(i+1) = (*vSp)[i];
	}

	RowVector vX;

	if (pTransData->nTransType == 4)
	{
		RowVector vX1; // preprocessed spectrum with DataConst

		nError = PrepSpForPrediction(dsc, pClbSamples->thePreprocDataConst, vSpectrum, vX1, false);

		if (nError == ER_OK)
		{
			nError = PrepSpForPrediction(&pClbSamples->freqScale, specPreprocData, vX1, vX, false);
		}
	}
	else
	{
		nError = PrepSpForPrediction(dsc, specPreprocData, vSpectrum, vX, false);
	}

	if (nError != ER_OK)
	{
		return nError;
	}

	pModel->Predict(vX, mComponentsPredicted);

	nError = pModel->GetError();

	if (nError != ER_OK)
	{
		return nError;
	}

	nError = PostProcessComponents(mComponentsPredicted, specPreprocData, true);

	if (nError != ER_OK)
	{
		return nError;
	}

/*	if (pTransData->nTransType == 4)
	{
		// new transfer
		nError = PostProcessComponents(mComponentsPredicted, pClbSamples->thePreprocDataConst);
	}*/

	if (nError != ER_OK)
	{
		return nError;
	}

//	nError = pTransData->CorrectComponents(mComponentsPredicted);

	int numComp = mComponentsPredicted.Ncols();

	for (int iC = 0; iC < numComp; iC++)
	{
		(*pvCompConc)[iC] = mComponentsPredicted(1, iC + 1);
//		concErr[iC] = vSEC(iC + 1);
	}


	return nError;
}

int CQntCalibration::PredictSamples(vector<CSampleProxy> *pProxySamples, // array of samples
					const CFreqScale *FreqScale, // ��������� ����� ��������
					MDbl *pmCompConc, // ������� ������������� ������������
					VDbl *pvSEV, // ������ SEV [numComp]
					VDbl *pvE,  // ������ ������� ������ (����������) [numComp]
					VDbl *pvSDV,// ������ SDV [numComp]
					VDbl *pvR2) // ������ R2 [numComp]
{
	CSamples *pSamples = new CSamples(pProxySamples, FreqScale, 0);

	//specPreprocData.Print("PredictSamples_specPreprocData");

	Matrix mSpPrep;

	pSamples->ExcludeFrequencies(specPreprocData);
	pSamples->PreprocessSpectra4Prediction(specPreprocData, mSpPrep);

//	PrintMatrix(mSpPrep, "PredictSamples_mSpPrep.dat");
	
	Matrix mComponentsPredicted;

	pModel->Predict(mSpPrep, mComponentsPredicted);

#ifdef _DEBUG_
	Matrix mSp;
	pSamples->GetMSpectra(mSp);
	PrintMatrix(mSp.t(), "mSpT.dat");
	PrintMatrix(mSpPrep.t(), "mtSpPrepT.dat");
#endif

	//PrintMatrix(mComponentsPredicted, "PredictSamples_mComponentsPredicted.dat");


	nError = pModel->GetError();

	if (nError != ER_OK)
	{
		return nError;
	}

	nError = PostProcessComponents(mComponentsPredicted, specPreprocData, true);

	if (nError != ER_OK)
	{
		return nError;
	}

//	nError = pTransData->CorrectComponents(mComponentsPredicted);

//	if (nError != ER_OK)
//	{
//		return nError;
//	}

	// ���������� ������������� ������������

	Matrix2MDbl(mComponentsPredicted, pmCompConc);

	// ��������� SEV

	Matrix mComp;

	pSamples->GetMComponents(mComp, false);

	Matrix mErrors;

	mErrors = mComponentsPredicted - mComp;

	int numComp = mComp.Ncols();
	int numSp = mComp.Nrows();

	ColumnVector vSEV(numComp);
	RowVector vStd2Comp(numComp);
	ColumnVector vR2(numComp);

	pSamples->GetStd2Components(vStd2Comp);

	for (int iC = 1; iC <= numComp; iC++)
	{
		double fErrSum2;
		fErrSum2 = mErrors.Column(iC).SumSquare();
		vSEV(iC) = sqrt(fErrSum2 / numSp);
		(*pvE)[iC - 1] = mErrors.Column(iC).Sum() / numSp;
		mErrors.Column(iC) -= (*pvE)[iC-1];
		(*pvSDV)[iC - 1] =  sqrt((mErrors.Column(iC)).SumSquare());
		if (numSp > 1) (*pvSDV)[iC - 1] /= sqrt(numSp - 1.0);
		vR2Stat(iC) = fErrSum2 / vStd2Comp(iC);
	}

	int numFact = pModel->GetNumVariables();

	if ((numSp - numFact - 1.0) > 0)
	{
		vR2Stat *= -(numSp - 1.0) / (numSp - numFact - 1.0);
		vR2Stat += 1.0;
	}
	else
	{
		vR2Stat = 0.0;
	}

	Column2VDbl(vSEV, pvSEV);

	Column2VDbl(vR2Stat, pvR2);

	delete pSamples;

	return nError;
}

//int CQntCalibration::PredictSEVSamples(VDbl *pvSEV, // ������ SEV [numComp]
//					VDbl *pvE,  // ������ ������� ������ (����������) [numComp]
//					VDbl *pvSDV,// ������ SDV [numComp]
//					VDbl *pvR2) // ������ R2 [numComp]
//for optimization
int CQntCalibration::PredictSEVSamples()
{
	//AppendToDebugReport("Start PredictSEVSamples");

	//AppendToDebugReport("Start PredictSEVSamples");

	//specPreprocData.Print("PredictSEVSamples_specPreprocData");

	if ((specPreprocData.numberOfExcludedFreq > 0) && (specPreprocData.nModelType != MODEL_HSO))
	{
		pSEVSamples->ExcludeFrequencies(specPreprocData);
	}

	if ((pSEVSamples) && (nError == ER_OK))
	{
		Matrix mSpPrep;

		//AppendToDebugReport("Start PreprocessSpectra4Prediction");
		//AppendToDebugReport(specPreprocData.strPreprocPar.c_str());

#ifdef _DEBUG_
	AppendToDebugReport("Start PreprocessSpectra4Prediction");
#endif

		nError = pSEVSamples->PreprocessSpectra4Prediction(specPreprocData, mSpPrep);

#ifdef _DEBUG_
	AppendToDebugReport("Samples have been preprocessed");
#endif
		//AppendToDebugReport("Samples Preprocessed");
		//PrintMatrix(mSpPrep, "PredictSEVSamples_mSpPrep.dat");
	
		if (nError != ER_OK)
		{
			return nError;
		}

		//char strBuf[256];
		//sprintf(strBuf, "mSpPrep.Ncols = %d mSpectraABSTRAcut.Ncols = %d\n", mSpPrep.Ncols(), mSpectraABSTRAcut.Ncols());
		//AppendToDebugReport(strBuf);

		Matrix mComponentsPredicted;

		pModel->Predict(mSpPrep, mComponentsPredicted);

#ifdef _DEBUG_
	AppendToDebugReport("Samples have been predicted");
#endif
		//PrintMatrix(mComponentsPredicted, "PredictSEVSamples_mComponentsPredicted.dat");

		nError = pModel->GetError();

		//AppendToDebugReport("Samples predicted");

		if (nError != ER_OK)
		{
			return nError;
		}

		//AppendToDebugReport("Start Postprocessed");

#ifdef _DEBUG_
	AppendToDebugReport("Start Postprocessing");
#endif

		nError = PostProcessComponents(mComponentsPredicted, specPreprocData, false);

#ifdef _DEBUG_
	AppendToDebugReport("End Postprocessing");
#endif

		//AppendToDebugReport("Conc. Postprocessed");

		if (nError != ER_OK)
		{
			return nError;
		}

	//	nError = pTransData->CorrectComponents(mComponentsPredicted);

		// ���������� ������������� ������������

		//Matrix2MDbl(mComponentsPredicted, pmCompConc);

		// ��������� SEV

		Matrix mComp;

		pSEVSamples->GetMComponents(mComp, false);

		Matrix mErrors;

		mErrors = mComponentsPredicted - mComp;

#ifdef _DEBUG_
	AppendToDebugReport("mErrors calculated");
#endif
		//AppendToDebugReport("mErrors calculated");


		int numComp = mComp.Ncols();
		int numSp = mComp.Nrows();

		RowVector vStd2Comp;//(numComp);

		pSEVSamples->GetStd2Components(vStd2Comp);

		for (int iC = 1; iC <= numComp; iC++)
		{
			double fErrSum2;
			fErrSum2 = mErrors.Column(iC).SumSquare();
			vSEV(iC) = sqrt(fErrSum2 / numSp);
			vErr(iC) = mErrors.Column(iC).Sum() / numSp;
			mErrors.Column(iC) -= vErr(iC);
			vSDV(iC) =  sqrt((mErrors.Column(iC)).SumSquare());
			if (numSp > 1) vSDV(iC) /= sqrt(numSp - 1.0);
			vR2StatSEV(iC) = fErrSum2 / vStd2Comp(iC);
		}

		int numFact;

		numFact = pModel->GetNumVariables();

		if ((numSp - numFact - 1.0) > 0)
		{
			vR2StatSEV *= -(numSp - 1.0) / (numSp - numFact - 1.0);
			vR2StatSEV += 1.0;
		}
		else
		{
			vR2StatSEV = 0.0;
		}
	}
	else nError = -1;

#ifdef _DEBUG_
	if (nError != ER_OK)
	{
		AppendToDebugReport("Error in PredictSEVSamples");
	}
#endif


	return nError;
}


// ��������� �������� ������������� ������������ ���������
// ��� ���� �������� ������� ������� ��������������� ������
int CQntCalibration::PredictSample(int nSample, // sample order number
									VDbl *pvCompConc)
{
	//AppendToDebugReport("Start of Predict Sample");
	RowVector vSpectrum, vX;

	pClbSamples->GetMeanSpectrum(nSample, vSpectrum);

	nError = pClbSamples->GetError();

	if (nError != ER_OK)
	{
		return nError;
	}

	if (pClbSamples->GetTransFlag() != 2)
	{
		nError = PrepSpForPrediction(&freqScale, specPreprocData, vSpectrum, vX, false);
		
		if (nError != ER_OK)
		{
			return nError;
		}

		pTransData->CorrectSpectra(vSpectrum);
	}
	else
	{
		// calibration was transfered and added
		if (pClbSamples->IsSampleAdded(nSample))
		{
			nError = PrepSpForPrediction(&freqScale, specPreprocData, vSpectrum, vX, false);
			if (nError != ER_OK)
			{
		//		//AppendToDebugReport("Error1");
				return nError;
			}
		}
		else
		{
			CSpecPreprocData m_PreprocDataConst;

			pClbSamples->GetPreprocDataConst(m_PreprocDataConst);

			// at first perform the const preprocessing

			RowVector vXconst;

			nError = PrepSpForPrediction(&freqScale, m_PreprocDataConst, vSpectrum, vXconst, false);

			if (nError != ER_OK)
			{
				return nError;
			}

			// then, correct spectrum
			pTransData->CorrectSpectra(vXconst);

			string strPreprocConst = m_PreprocDataConst.strPreprocPar;
			string strPreprocFull, strPreprocCut;
			strPreprocFull = specPreprocData.strPreprocPar;

			for (int il = 0; il < strPreprocFull.length(); il++)
			{
				char ch = strPreprocFull[il];
				if (strPreprocConst.find(ch) == string::npos)
				{
					strPreprocCut += ch;
				}
			}

			// at last perform the remaining preprocessing
			CSpecPreprocData thePreprocDataRem;

			thePreprocDataRem.Copy(specPreprocData);

			thePreprocDataRem.strPreprocPar = strPreprocCut;
			thePreprocDataRem.nSpType = STYPE_TRA;

			nError = PrepSpForPrediction(thePreprocDataRem, vXconst, vX, freqScale.fStepFreq);

			if (nError != ER_OK)
			{
				return nError;
			}

			specPreprocData.strPreprocPar = strPreprocFull;
		}
	}

	if (nError != ER_OK)
	{
		return nError;
	}

	pModel->Predict(vX, mComponentsPredicted);

	//if (nSample == 1) PrintMatrix(mComponentsPredicted, "mCompPred.dat");

	nError = pModel->GetError();

	if (nError != ER_OK)
	{
		return nError;
	}

	nError = PostProcessComponents(mComponentsPredicted, specPreprocData, false);

	if (nError != ER_OK)
	{
		return nError;
	}

	nError = pTransData->CorrectComponents(mComponentsPredicted);

	if (nError != ER_OK)
	{
		return nError;
	}


	int numComp = pClbSamples->GetNumComponents();

	for (int i = 0; i < numComp; i++)
	{
		(*pvCompConc)[i] = mComponentsPredicted(1, i + 1);
	}

	return nError;
}

// ��������� ���������� ������������ ��� ������������ �������
// �� ����������� �������������� ������
double CQntCalibration::CalculateMahalanobisDistance(const CFreqScale *dsc, // spectrum descriptor
						const VDbl *vSp)
{
	RowVector vSpectrum(vSp->size());

	for (int i = 0; i < vSp->size(); i++)
	{
		vSpectrum(i+1) = (*vSp)[i];
	}

	RowVector vX;

	if (pTransData->nTransType == 4)
	{
		// new transfer
		RowVector vX1; // preprocessed spectrum with DataConst

		nError = PrepSpForPrediction(dsc, pClbSamples->thePreprocDataConst, vSpectrum, vX1, false);

		if (nError == ER_OK)
		{
			nError = PrepSpForPrediction(&pClbSamples->freqScale, specPreprocData, vX1, vX, false);
		}
	}
	else
	{
		nError = PrepSpForPrediction(dsc, specPreprocData, vSpectrum, vX, false);
	}

	if (nError != ER_OK)
	{
		return nError;
	}

	ColumnVector vMah;

	pModel->CalculateMahalanobisDistance(vX, vMah);

	nError = pModel->GetError();

	if (nError != ER_OK)
	{
		return -1;
	}

	return vMah(1);
}


// ��������� ���������� ������������ ��� ����������� �������� (����������������)
// �� ����������� �������������� ������
int CQntCalibration::CalculateMahalanobisDistances(const Matrix& mX,
						ColumnVector& vMah)
{
	pModel->CalculateMahalanobisDistance(mX, vMah);

	nError = pModel->GetError();

	return nError;
}


// ��������� ������������� ���������� ������������ ��� ������������ �������
// �� ����������� �������������� ������
double CQntCalibration::CalculateNormMahalanobisDistance(const CFreqScale *dsc, // spectrum descriptor
						const VDbl *vSpectrum)
{
	double dist = CalculateMahalanobisDistance(dsc, vSpectrum);

	// normalize
	dist /= fMeanMahDist;

	return dist;

}

// ��������� ������������� ���������� ������������ ��� ����������� �������� (����������������)
// �� ����������� �������������� ������
int CQntCalibration::CalculateNormMahalanobisDistances(const Matrix& mX, // spectrum descriptor
						ColumnVector& vMah)
{
	CalculateMahalanobisDistances(mX, vMah);

	vMah /= fMeanMahDist;

	return nError;
}

// ��������� ������������� ������������ ��������� ��� ������� �������
// ������� ������� ��������������� ������.
int CQntCalibration::PredictSpectrum(int nSample, // sample order number
									int nSpec, // spec order number
									VDbl *pvCompConc)
{
	//AppendToDebugReport("Start of Predict Spectrum");

	RowVector vSpectrum, vX;

	pClbSamples->GetSpectrum(nSample, nSpec, vSpectrum);

	//char strBuf[128];
	//sprintf(strBuf, "vSpectrum.Ncols()=%d", vSpectrum.Ncols());
	//AppendToDebugReport(strBuf);

	nError = pClbSamples->GetError();

	if (nError != ER_OK)
	{
		return nError;
	}

	nError = PrepSpForPrediction(&freqScale, specPreprocData, vSpectrum, vX, false);
//	sprintf(strBuf, "vX.Ncols()=%d", vX.Ncols());
//	AppendToDebugReport(strBuf);

	//AppendToDebugReport("End PrepSpForPrediction");

	if (nError != ER_OK)
	{
		//char strBuf[64];

		//sprintf(strBuf, "nError = %d\n", nError);
		//AppendToDebugReport(strBuf);

		/*if (nSample == 1)
		{
			sprintf(strBuf, "numChannels = %d\n", specPreprocData.numberOfChannels);
			AppendToDebugReport(strBuf);
			sprintf(strBuf, "numFreqExcluded = %d\n", specPreprocData.numberOfExcludedFreq);
			AppendToDebugReport(strBuf);
			sprintf(strBuf, "numFreq = %d\n", freqScale.nNumberOfFreq);
			AppendToDebugReport(strBuf);
		}*/


		return nError;
	}

	//AppendToDebugReport("Start Prediction");


	pModel->Predict(vX, mComponentsPredicted);

	//if (nSample == 1) PrintMatrix(mComponentsPredicted, "mCompPred.dat");

	nError = pModel->GetError();


	if (nError != ER_OK)
	{
		//AppendToDebugReport("Error in predicting");
		return nError;
	}

	nError = PostProcessComponents(mComponentsPredicted, specPreprocData, false);

	if (nError != ER_OK)
	{
		return nError;
	}


	int numComp = pClbSamples->GetNumComponents();

	for (int i = 0; i < numComp; i++)
	{
		(*pvCompConc)[i] = mComponentsPredicted(1, i + 1);
	}

	return nError;
}

// ��������� ������������� ������������ ��������� ��� 
// ����������� �������� (����������������)
int CQntCalibration::PredictPreprocSpectra(const Matrix& mPreprocSpectra, // sample order number
									Matrix& mComp)
{

	pModel->Predict(mPreprocSpectra, mComp);

	nError = PostProcessComponents(mComp, specPreprocData, true);

	if (nError != ER_OK)
	{
		return nError;
	}

	nError = PostProcessComponents(mComp, pClbSamples->thePreprocDataConst, false);

	if (nError != ER_OK)
	{
		return nError;
	}

	nError = pTransData->CorrectComponents(mComp);

	if (nError != ER_OK)
	{
		return nError;
	}

	return nError;
}


// ���������� ������������� �������� ������������ (� ��� ������)
// ��� ������� ����������, ���������� �������� PredictSpectrum
int CQntCalibration::GetConcentration(string sCompName, // the component name of interest
									double* conc, // the predicted concentration of this conponent
									double* concErr) // the estimated error of predicted concentration
{
	//string sCompName(compName);

	int ind = pClbSamples->FindComponentIndex(sCompName);

	if (nError == ER_OK)
	{
		*conc = mComponentsPredicted(1, ind + 1);
		*concErr = vSEC(ind + 1);
	}


	return nError;
}

/*const char* CQntCalibration::GetSampleName(int nSample)
{
	return pClbSamples->GetSampleName(nSample);
}*/

const char* CQntCalibration::GetComponentName(int nComponent)
{
	return pClbSamples->GetComponentName(nComponent);
}

/*const char* CQntCalibration::GetSampleSpecName(int nSample, int nSpec)
{
	return pClbSamples->GetSampleSpecName(nSample, nSpec);
}*/

// ��������� SEC �������������� ������
int CQntCalibration::CalculateSEC()
{

	Matrix mYpredicted;
	Matrix mErrors;


	pModel->Predict(mX, mYpredicted);

	//AppendToDebugReport("end of predict.");


	// �� ��������� ���������
	PostProcessComponents(mYpredicted, specPreprocData, false);

/*	if (pTransData->nTransType == 4)
	{
		PostProcessComponents(mYpredicted, pClbSamples->thePreprocDataConst);
	}*/

//	pTransData->CorrectComponents(mYpredicted);

	Matrix mComp;

	if (specPreprocData.strPreprocPar.find_first_of('A') != string::npos)
	{
		pClbSamples->GetMComponents(mComp, true);
		//AppendToDebugReport("Get MComp");
	}
	else
	{
		pClbSamples->GetMComponents(mComp, false);
	}

	int numComp = mY.Ncols();
	int numSp = mY.Nrows();

	mErrors = mComp - mYpredicted;

	int numFact;

	numFact = pModel->GetNumVariables();

	numberOfFreedomDegrees = mX.Nrows() - numFact;

	if (specPreprocData.meanSpectrum.Ncols() != 0)
	{
		numberOfFreedomDegrees--;
	}

	// for transfer
	if (specPreprocData.meanComponentsTrans.Ncols() != 0)
	{
		numberOfFreedomDegrees--;
	}

	if (numberOfFreedomDegrees <= 0)
	{
		nError = ER_SEC;
		return nError;
	}

	if (numSp - numFact - 1 <= 0)
	{
		nError = ER_SEC;
		return nError;
	}


	RowVector vStd2Comp;

	pClbSamples->GetStd2Components(vStd2Comp);

//	vSEC.ReSize(numComp);

//	vR2Stat.ReSize(numComp);


	for (int iC = 1; iC <= numComp; iC++)
	{
		vSEC(iC) = (mErrors.Column(iC)).SumSquare();

		vR2Stat(iC) = vSEC(iC) / vStd2Comp(iC);

		vSEC(iC) = sqrt(vSEC(iC) / numberOfFreedomDegrees);
	}

	vR2Stat *= -(numSp - 1.0) / (numSp - numFact - 1.0);

	vR2Stat += 1.0;

	//AppendToDebugReport("end of SEC");

	return nError;
}

// ��������� SEV �������������� ������ �� ��������������
// ������ �������� ��� ���������
// ������� ������������ � ��������� Envelope4Samples.
int CQntCalibration::CalculateSEV(void* pSamples, double* pSEV)
{
	CSamples* pExtSamples = (CSamples*) pSamples;
	
	if (pExtSamples->GetError() != ER_OK)
	{
		return nError;
	}

	Matrix mExtSp;

	pExtSamples->GetMSpectra(mExtSp, false);

	Matrix mExtSpPreproc;

	if (pTransData->nTransType == 4)
	{
		nError = PrepSpForPrediction(&(pExtSamples->freqScale), pClbSamples->thePreprocDataConst, mExtSp, mExtSpPreproc, false);

		if (nError == ER_OK)
		{
			nError = PrepSpForPrediction(&(pClbSamples->freqScale), specPreprocData, mExtSpPreproc, mExtSp, false);
			mExtSpPreproc = mExtSp;
		}
	}
	else
	{
		nError = PrepSpForPrediction(&(pExtSamples->freqScale), specPreprocData, mExtSp, mExtSpPreproc, false);
	}

	if (nError != ER_OK)
	{
		return nError;
	}



	Matrix mComponentsPredicted;

	pModel->Predict(mExtSpPreproc, mComponentsPredicted);

	// ��������� ���������
	nError = PostProcessComponents(mComponentsPredicted, specPreprocData, true);

	if (nError != ER_OK)
	{
		return nError;
	}

/*	if (pTransData->nTransType == 4)
	{
		// new transfer
		nError = PostProcessComponents(mComponentsPredicted, pClbSamples->thePreprocDataConst);
	}*/

	if (nError != ER_OK)
	{
		return nError;
	}

//	nError = pTransData->CorrectComponents(mComponentsPredicted);

	if (nError != ER_OK)
	{
		return nError;
	}

	Matrix mComp;

	pExtSamples->GetMComponents(mComp, false);

	Matrix mErrors;

	mErrors = mComp - mComponentsPredicted;

	int numComp = mComp.Ncols();
	int numExtSp = mComp.Nrows();

	for (int iC = 1; iC <= numComp; iC++)
	{
		pSEV[iC - 1] = sqrt((mErrors.Column(iC)).SumSquare() / numExtSp);
	}

	return nError;
}

void CQntCalibration::GetSpecPreprocData(CSpecPreprocData& theSpecPreprocData) const
{

	theSpecPreprocData.Copy(specPreprocData);
}

// ���������� �������������� �������
void CQntCalibration::GetClbSamples(CSamples* pSamples) const
{
	pSamples->CopySamples(*pClbSamples);
}

// ���������� ������ ����������� ������������ ��� �������
// ������� ��������������� ������
void CQntCalibration::GetRefConc(int nSample, VDbl *pvCompConc)
{
	pClbSamples->GetRefConc(nSample, pvCompConc);
}


// ��������� ������� (������!) �����������
void CQntCalibration::Transfer(CTransData& theTransData)
{
	(*pTransData).nTransType = theTransData.nTransType;

	switch(theTransData.nTransType)
	{
	case 1:
		(*pTransData).vBiasRef = theTransData.vBiasRef;
		(*pTransData).vSlopeRef = theTransData.vSlopeRef;
		break;
	case 2:
	case 21:
	case 22:

		(*pTransData).vBiasSp = theTransData.vBiasSp;
		(*pTransData).vSlopeSp = theTransData.vSlopeSp;
		break;
	}

	nError = pClbSamples->PreprocessSpectra(specPreprocData, pTransData, mX);

	if (nError != ER_OK)
	{
		return;
	}

	pClbSamples->PreprocessComponents(specPreprocData, mY);

	nError = pClbSamples->GetError();

	if (nError != ER_OK)
	{
		return;
	}

	int nModelType = pModel->GetModelType();
	int numFactors = pModel->GetNumVariables();

	delete pModel;

	switch(nModelType)
	{
	case MODEL_PLS:
		pModel = new CModelPLS(mX, mY, numFactors);
		break;
	case MODEL_PCR:
		pModel = new CModelPCR(mX, mY, numFactors);
		break;
	default:
		nError = ER_MODEL_TYPE;
	}

	if (nError == ER_OK)
	{
		CalculateSEC();
		CalculateMahDist();
	}
}

// ��������� ���������� ������������ ��� �������� �������
// ������� ������� ��������������� ������
double CQntCalibration::GetSampleDistance(int nSample)
{
	RowVector vSpectrum;

	pClbSamples->GetMeanSpectrum(nSample, vSpectrum);

	RowVector vX;

	nError = PrepSpForPrediction(&freqScale, specPreprocData, vSpectrum, vX, false);

	if (nError != ER_OK)
	{
		return nError;
	}

	ColumnVector vMah;

	pModel->CalculateMahalanobisDistance(vX, vMah);

	nError = pModel->GetError();

	if (nError != ER_OK)
	{
		return -1;
	}

	return vMah(1);
}

// ��������� ���������� ������������ ��� ���� ��������
// ������� ������� ��������������� ������
void CQntCalibration::GetSampleDistances(int nSample, double* dist)
{
	int nSpectra = GetNumSampleSpectra(nSample);
	int nSpIndex = pClbSamples->GetSampleSpectraIndexes(nSample);

	for (int i = 0; i < nSpectra; i++)
	{
		dist[i] = GetMahalanobisDistance(nSpIndex + i + 1);
	}
}

// ��������� ������������� ���������� ������������ ��� �������� �������
// ������� ������� ��������������� ������
double CQntCalibration::GetSampleNormDistance(int nSample)
{
	double dist = GetSampleDistance(nSample);

	return dist / fMeanMahDist;
}

// ��������� ������������� ���������� ������������ ��� ���� ��������
// ������� ������� ��������������� ������
void CQntCalibration::GetSampleNormDistances(int nSample, double* dist)
{
	GetSampleDistances(nSample, dist);

	int nSpectra = GetNumSampleSpectra(nSample);

	for (int i = 0; i < nSpectra; i++)
	{
		dist[i] /= fMeanMahDist;
	}

}

// ��������� ���������� ������������ ��� ������� �������
// ������� ������� ��������������� ������

void CQntCalibration::GetSampleSpecDistance(int nSample, int nSpec, double* dist)
{
	int nSpIndex;

	nSpIndex = pClbSamples->GetSampleSpectraIndexes(nSample);

	*dist = GetMahalanobisDistance(nSpIndex + nSpec + 1);
}

// ���������� ����� �������� � ������ �������������� �������
int CQntCalibration::GetNumSampleSpectra(int nSample) const
{
	return pClbSamples->GetSampleSpectraNumbers(nSample);
}

// ���������� ���������� ������������ ��� �������
// ��������������� ������
double CQntCalibration::GetMahalanobisDistance(int i)
{
	double res;

	if ((i < 1) || (i > vMahalanobisDistances.Nrows()))
	{
		nError = ER_OUT_OF_RANGE;
		res = -1;
	}
	else
	{
		res = vMahalanobisDistances(i);
	}

	return res;
}

// ��������� ���������� ������������ ��� ��������
// ��������������� ������
int CQntCalibration::CalculateMahDist()
{
	if (specPreprocData.strPreprocPar.find_first_of('A') != string::npos)
	{
		// averaging was performed before Model building
		// now we need to predict all spectra, not averaged
		Matrix mSp;
		pClbSamples->GetMSpectra(mSp);

		PrepSpForPrediction(&freqScale, specPreprocData, mSp, mX, false);

		pModel->CalculateMahalanobisDistance(mX, vMahalanobisDistances);
	}
	else
	{
		// get Mahalanobis Distances from the built model
		pModel->GetMahalanobisDistance(vMahalanobisDistances);
	}

	int numClbSpectra = vMahalanobisDistances.Nrows();

//	char str[256];
//	sprintf(str, "numClbSpectra = %d\n", numClbSpectra);
//	AppendToDebugReport(str);

	if (numClbSpectra > 1)
	{
		fMeanMahDist = sqrt(vMahalanobisDistances.SumSquare() / (numClbSpectra - 1));
	}
	else
	{
		fMeanMahDist = sqrt(vMahalanobisDistances.SumSquare());
	}


	return nError;
}





void CQntCalibration::Init()
{
	nError = ER_OK;
	pClbSamples = 0;
	pSEVSamples = 0;
	pModel = 0;
//	pTransData = 0;
	pTransData = new CTransData();

	flagBatch = false;
	specPreprocData.strPreprocPar = "";

	pQnt = 0;
	pUseFreq = 0;
}


/*int CQntCalibration::OutputScores(const char *fname)
{
	FILE *fgh = fopen(fname, "w");

	if (fgh == 0)
	{
		return -1;
	}

	Matrix mScores;

	pModel->GetMatrixScores(mScores);

	int numSamples = GetNumSamples();



	if (specPreprocData.strPreprocPar.find_first_of('A') != string::npos)
	{
		for (int iS = 0; iS < numSamples; iS++)
		{
			fprintf(fgh, "%s\t", pClbSamples->GetSampleName(iS));

			int numCol = mScores.Ncols();

			for (int k = 0; k < numCol; k++)
			{
				fprintf(fgh, "\t%10.2e", mScores(iS + 1, k + 1));
			}
			fprintf(fgh, "\n");
		}
	}
	else
	{
		int numSpectra;
		int nSp = 0;

		for (int iS = 0; iS < numSamples; iS++)
		{
			numSpectra = GetNumSampleSpectra(iS);

			for (int iSpec = 0; iSpec < numSpectra; iSpec++)
			{
				fprintf(fgh, "%s\t", pClbSamples->GetSampleSpecName(iS, iSpec));

				int numCol = mScores.Ncols();

				for (int k = 0; k < numCol; k++)
				{
					fprintf(fgh, "\t%10.2e", mScores(nSp + 1, k + 1));
				}
				fprintf(fgh, "\n");
				nSp++;
			}
		}
	}



	fclose(fgh);

	return 0;
}*/

void CQntCalibration::GetTransData(CTransData& theTransData) const
{
	theTransData.nTransType = (*pTransData).nTransType;

	switch(theTransData.nTransType)
	{
	case 1:
		theTransData.vBiasRef = (*pTransData).vBiasRef;
		theTransData.vSlopeRef = (*pTransData).vSlopeRef;
		break;
	case 2:
		theTransData.vBiasSp = (*pTransData).vBiasSp;
		theTransData.vSlopeSp = (*pTransData).vSlopeSp;
		break;
	}
}

// ���������� ������� ������������� ���������,
// ������������� � �����������
int CQntCalibration::GetMaxClbFreqRange(double *fMinFreq, double *fMaxFreq)
{
	if (nError == ER_OK)
	{
		*fMinFreq = pClbSamples->freqScale.fStartFreq;
		*fMaxFreq = pClbSamples->freqScale.GetEndFreq();
	}
	else
	{
		*fMinFreq = 0;
		*fMaxFreq = 0;
	}

	return nError;
}

void CQntCalibration::GetModelPar(MODEL_MMP& theModelPar) const
{
	pModel->GetModelPar(theModelPar);
}

void CQntCalibration::GetUseFreq(int *pUseFreq) const
{
	if (GetModelType() == MODEL_HSO)
	{
		pModel->GetUseFreq(pUseFreq);
	}
	else
	{
		pUseFreq = 0;
	}
}

void CQntCalibration::FillQnt(CQnt_Parcel *pQnt)
{
//	AppendToDebugReport("Start filling Qnt");
//	pQnt = new CQnt_Parcel;

	// fill preproc Data
//	pQnt->m_specPreprocData.fMax = specPreprocData.fMax;
//	pQnt->m_specPreprocData.fMaxFreq = specPreprocData.fMaxFreq;
//	pQnt->m_specPreprocData.fMinFreq = specPreprocData.fMinFreq;

	if (specPreprocData.meanComponents.Ncols())
	{
		//pQnt->m_specPreprocData.meanComponents = Row2VDbl(specPreprocData.meanComponents);
		Row2VDbl(specPreprocData.meanComponents, pQnt->m_specPreprocData.meanComponents);
	}
	else
	{
		pQnt->m_specPreprocData.meanComponents.clear();
	}

	if (specPreprocData.meanSpectrum.Ncols())
	{
		//pQnt->m_specPreprocData.meanSpectrum = Row2VDbl(specPreprocData.meanSpectrum);
		Row2VDbl(specPreprocData.meanSpectrum, pQnt->m_specPreprocData.meanSpectrum);
	}
	else
	{
		pQnt->m_specPreprocData.meanSpectrum.clear();
	}

	if (specPreprocData.meanSpectrumMSC.Ncols())
	{
		//pQnt->m_specPreprocData.meanSpectrumMSC = Row2VDbl(specPreprocData.meanSpectrumMSC);
		Row2VDbl(specPreprocData.meanSpectrumMSC, pQnt->m_specPreprocData.meanSpectrumMSC);
	}
	else
	{
		pQnt->m_specPreprocData.meanSpectrumMSC.clear();
	}

	if (specPreprocData.meanStdComp.Ncols())
	{
		//pQnt->m_specPreprocData.meanStdComp = Row2VDbl(specPreprocData.meanStdComp);
		Row2VDbl(specPreprocData.meanStdComp, pQnt->m_specPreprocData.meanStdComp);
	}
	else
	{
		pQnt->m_specPreprocData.meanStdComp.clear();
	}

	if (specPreprocData.meanStdSpec.Ncols())
	{
		//Qnt->m_specPreprocData.meanStdSpec = Row2VDbl(specPreprocData.meanStdSpec);
		Row2VDbl(specPreprocData.meanStdSpec, pQnt->m_specPreprocData.meanStdSpec);
	}
	else
	{
		pQnt->m_specPreprocData.meanStdSpec.clear();
	}

	pQnt->m_specPreprocData.fMax = specPreprocData.fMax;

	//pQnt->m_SpecPreproc
	pQnt->m_SpecPreproc.nSpType = specPreprocData.nSpType;
	pQnt->m_SpecPreproc.strPreprocPar = specPreprocData.strPreprocPar;
	pQnt->m_SpecPreproc.strPreprocParTrans = specPreprocData.strPreprocParTrans;
//	if (specPreprocData.vComponentCorrectionsAdd.Ncols())
//	{
//		Row2VDbl(specPreprocData.vComponentCorrectionsAdd, pQnt->m_SpecPreproc.vCompConcCorrAdd);
//	}
//	else
//	{
//		pQnt->m_SpecPreproc.vCompConcCorrAdd.clear();
//	}

//	if (specPreprocData.vComponentCorrectionsMul.Ncols())
//	{
//		Row2VDbl(specPreprocData.vComponentCorrectionsMul, pQnt->m_SpecPreproc.vCompConcCorrMul);
//	}
//	else
//	{
//		pQnt->m_SpecPreproc.vCompConcCorrMul.clear();
//	}

	//pQnt->m_specPreprocDataTrans
	if (specPreprocData.meanComponentsTrans.Ncols())
	{
		Row2VDbl(specPreprocData.meanComponentsTrans, pQnt->m_specPreprocDataTrans.meanComponents);
	}
	else
	{
		pQnt->m_specPreprocDataTrans.meanComponents.clear();
	}

	pQnt->strPreprocParTrans = specPreprocData.strPreprocParTrans;



//	if (specPreprocData.vComponentCorrections.Ncols())
//	{
		//pQnt->m_specPreprocData.vComponentCorrections = Row2VDbl(specPreprocData.vComponentCorrections);
//		Row2VDbl(specPreprocData.vComponentCorrections, pQnt->m_specPreprocData.vComponentCorrections);
//	}
//	else
//	{
//		pQnt->m_specPreprocData.vComponentCorrections.clear();
//	}

//	if (!specPreprocData.vFreqExcluded.empty())
//	{
//		pQnt->m_specPreprocData.vFreqExcluded = specPreprocData.vFreqExcluded;
//	}
//	else
//	{
//		pQnt->m_specPreprocData.vFreqExcluded.clear();
//	}

//	AppendToDebugReport("End filling m_specPreprocData");


	// fill Model
	pModel->FillModel_Parcel(pQnt->m_Model);

//	AppendToDebugReport("End filling m_Model");


	// results
//	pQnt->vSEC = Column2VDbl(vSEC);
	Column2VDbl(vSEC, pQnt->vSEC);

//	AppendToDebugReport("End filling vSEC");

//	pQnt->vR2Stat = Column2VDbl(vR2Stat);
	Column2VDbl(vR2Stat, pQnt->vR2Stat);

//	AppendToDebugReport("End filling vR2Stat");

//	pQnt->vMahalanobisDistances = Column2VDbl(vMahalanobisDistances);
	Column2VDbl(vMahalanobisDistances, pQnt->vMahalanobisDistances);

	pQnt->fMeanMahDist = fMeanMahDist;

//	AppendToDebugReport("End filling vMahalanobis");


//	AppendToDebugReport("End filling pQnt");
}

// ��� �����������
int CQntCalibration::LoadSamples4SEV(vector<CSampleProxy> *pSamples)
{
	// numComp = 1 ������ ��� �����������!
	vector <string> vCompNames(1);
	vCompNames[0] = pClbSamples->GetComponentName(0);
	pSEVSamples = new CSamples(pSamples, &freqScale, &vCompNames);



//	char strBuf[256];

//	sprintf(strBuf, "freqScale.fStartFreq =%e\n", freqScale.fStartFreq);
//	AppendToDebugReport(strBuf);

	return 0;
}


int CQntCalibration::SetModelType(int nModelType) // calibartion model type 
{
	//AppendToDebugReport("Start SetModelType");

	nError = ER_OK;

	specPreprocData.nModelType = nModelType;

	//char str[64];
	//sprintf(str, "Model type = %d", nModelType);
	//AppendToDebugReport(str);


	if (nModelType == MODEL_HSO)
	{
#ifdef _DEBUG_
		AppendToDebugReport("Start MODEL_HSO");
#endif

		if (pModel) delete pModel;
		pModel = new CModelHQO();
#ifdef _DEBUG_
		AppendToDebugReport("End MODEL_HSO");
#endif
	}

	if (nModelType == MODEL_MLR)
	{
		nError = ER_MODEL_TYPE;
	}



	return nError;
}

int CQntCalibration::SetAverage(bool bAverage) // ���������� �������� �������
{

#ifdef _DEBUG_
	AppendToDebugReport("CQntCalibration::SetAverage Start");
#endif

	nError = ER_OK;

	if (bAverage)
	{
		specPreprocData.strPreprocPar = "A";
	}
	else
	{
		specPreprocData.strPreprocPar.clear();
	}

	return nError;
}

int CQntCalibration::SetSpType(int nSpType) // ��� ������� (STYPE_TRA ��� STYPE_ABS - transmittance or absorbance) 
{
	//AppendToDebugReport("Start SetSpType");

	nError = ER_OK;

//	mSpectraABSTRA.CleanUp();

//	pClbSamples->GetMSpectra(mSpectraABSTRA);

	//if (nError != ER_OK)
	//{
	//	AppendToDebugReport("Error in SetSpType at start");
	//}

	mSpectraABSTRA.CleanUp();


	if (nError == ER_OK)
	{
		pClbSamples->ExcludeFrequencies(specPreprocData);
	}

	if (specPreprocData.strPreprocPar.find_first_of('A') != string::npos)
	{
		pClbSamples->GetMSpectra(mSpectraABSTRA, true);
	}
	else
	{
		pClbSamples->GetMSpectra(mSpectraABSTRA, false);
	}

//	if (specPreprocData.numberOfExcludedFreq > 0)
//	{
		//specPreprocData.numberOfChannels -= specPreprocData.numberOfExcludedFreq;
//		ExcludeColumns(mSpectraABSTRA, specPreprocData.numberOfExcludedFreq, specPreprocData.vFreqExcluded);
//	}

	if (nSpType == STYPE_ABS)
	{
		ABS(mSpectraABSTRA);
	}

	specPreprocData.nSpType = nSpType;

#ifdef _DEBUG_
	char strBuf[256];
	sprintf(strBuf, "CQntCalibration::SetSpType mSpectraABSTRA.Ncols = %d\n", mSpectraABSTRA.Ncols());
	AppendToDebugReport(strBuf);
#endif

	//AppendToDebugReport("End SetSpType");

	return nError;
}

int CQntCalibration::SetFreqRange(double fMinFreq, double fMaxFreq) // ��������� �������� 
{
	//AppendToDebugReport("Start SetFreqRange");

	int iStart, iEnd;

	nError = ER_OK;

	mSpectraABSTRAcut.CleanUp();

	//if (nError != ER_OK)
	//{
	//	AppendToDebugReport("Error in SetFreqRange at start");
	//}

	// ���������� ������

	iStart = FindIndex(fMinFreq, freqScale, specPreprocData, true);
	iEnd = FindIndex(fMaxFreq, freqScale, specPreprocData, false);

	specPreprocData.fMinFreq = fMinFreq;
	specPreprocData.fMaxFreq = fMaxFreq;
	specPreprocData.numberOfChannels = iEnd - iStart + 1;


	if ((iStart < 0) || (iEnd < 0) || (iStart > iEnd))
	{
		nError = ER_FREQ_RANGE;
		//AppendToDebugReport("Error freq range");
	}
	else
	{
		mSpectraABSTRAcut = mSpectraABSTRA.Columns(iStart + 1, iEnd + 1);

		if (specPreprocData.nModelType == MODEL_HSO)
		{
			if (pUseFreq) delete[] pUseFreq;

			pUseFreq = new int[specPreprocData.numberOfChannels];

			if (specPreprocData.numberOfExcludedFreq)
			{
				for (int i = iStart; i <= iEnd; i++)
				{
					pUseFreq[i-iStart] = specPreprocData.vUseFreq[i];
				}
			}
			else
			{
				for (int i = 0; i < specPreprocData.numberOfChannels; i++)
				{
					pUseFreq[i] = 1;
				}
			}

			nError = pModel->SetFrequencies(specPreprocData.numberOfChannels, pUseFreq);
		}
	}



//	if (nError != ER_OK)
//	{
		//AppendToDebugReport("Error freq range");
//	}

	//AppendToDebugReport("End SetFreqRange");

	return nError;
}

int CQntCalibration::SetPreproc(string strPreprocPar) // ������ ���������� �������������
{
	//AppendToDebugReport("Start SetPreproc");

	nError = ER_OK;

	if (nError == ER_OK)
	{
		if (specPreprocData.strPreprocPar.size())
		{
			if (specPreprocData.strPreprocPar[0]=='A')
			{
				specPreprocData.strPreprocPar.clear();
				specPreprocData.strPreprocPar = "A" + strPreprocPar;
			}
			else
			{
				specPreprocData.strPreprocPar.clear();
				specPreprocData.strPreprocPar = strPreprocPar;
			}
		}
		else
		{
				specPreprocData.strPreprocPar.clear();
				specPreprocData.strPreprocPar = strPreprocPar;
		}


	// ��������� ������������� - ����������� �� �������� (������� 'H')
	specPreprocData.strPreprocPar += "H";


		mX.CleanUp();
		mY.CleanUp();

		// clean PreprocData vectors
		specPreprocData.meanSpectrum.CleanUp();
		specPreprocData.meanComponents.CleanUp();
		specPreprocData.meanSpectrumMSC.CleanUp();
		specPreprocData.meanStdComp.CleanUp();
		specPreprocData.meanStdSpec.CleanUp();

		mX = mSpectraABSTRAcut;
		nError = PreprocessSpectra(specPreprocData, freqScale.fStepFreq, mX);
		if (nError == ER_OK)
		{
			nError = pClbSamples->PreprocessComponents(specPreprocData, mY);
		}
	}


#ifdef _DEBUG_
	AppendToDebugReport("End SetPreproc");
	char strTmp[64];
	sprintf(strTmp, "CQntCalibration::SetPreproc nError=%d", nError);
	AppendToDebugReport(strTmp);
#endif


	return nError;
}

int CQntCalibration::SetFactors(int numFactors)  // number of factors 
{
//	AppendToDebugReport("Start SetFactors");
//	clock_t start, finish;

//	start = clock();


	nError = ER_OK;


//	PrintMatrix(mX, "mX.dat");
//	PrintMatrix(mY, "mY.dat");
//	specPreprocData.Print("specPreprocData.dat");


	if (nError != ER_OK)
	{
		return nError;
	}

	delete pModel;

	pModel = 0;

	switch(specPreprocData.nModelType)
	{
	case MODEL_PLS:
		pModel = new CModelPLS(mX, mY, numFactors);
		nError = pModel->GetError();

		break;
	case MODEL_PCR:
		pModel = new CModelPCR(mX, mY, numFactors);
		nError = pModel->GetError();

		break;
	default:
		nError = ER_MODEL_TYPE;
	}

///	finish = clock();

//	durationQntCreateModel += (double)(finish - start);// / CLOCKS_PER_SEC;


	if (nError == ER_OK)
	{
//		start = clock();

		CalculateSEC();

//		finish = clock();
//		durationQntSEC += (double)(finish - start);// / CLOCKS_PER_SEC;


//		CalculateMahDist();

//		AppendToDebugReport("End of Mah. dist");

		if (nError == ER_OK)
		{
			if (mOptFlags.bSEV)
			{
				PredictSEVSamples();
			}
		}

		if (nError == ER_OK)
		{
			if (mOptFlags.bSECV)
			{
	//			start = clock();
				SECV(numFactors);
	//			finish = clock();
	//			durationQntSECV += (double)(finish - start);// / CLOCKS_PER_SEC;
			}
		}
	}

//	AppendToDebugReport("End SetFactors");


	return nError;
}

int CQntCalibration::SetCnl(double fCnl)
{
	nError = ER_OK;
	pModel->SetCnl(fCnl);

	//char strTmp[64];
	//sprintf(strTmp, "CQntCalibration::SetCnl nError=%d", nError);
	//AppendToDebugReport(strTmp);

	return nError;
}

int CQntCalibration::SetShc(double fShc)  // size of hypercube 
{
	nError = ER_OK;

	pModel->SetShc(fShc);

#ifdef _DEBUG_
	char strBuf[256];
	sprintf(strBuf, "CQntCalibration::SetShc mX.Ncols = %d\n", mX.Ncols());
	AppendToDebugReport(strBuf);
#endif

	nError = pModel->CreateModel(mX, mY);



	//char strTmp[64];
	//sprintf(strTmp, "CQntCalibration::SetShc nError=%d", nError);
	//AppendToDebugReport(strTmp);

	//PrintMatrix(mX, "mX_opt.dat");
	//PrintMatrix(mY, "mY_opt.dat");

	if (nError == ER_OK)
	{
#ifdef _DEBUG_
	AppendToDebugReport("SetShc. ER_OK");
#endif
		CalculateSEC();

#ifdef _DEBUG_
	AppendToDebugReport("SetShc. SEC OK");
#endif


		if (mOptFlags.bSEV)
		{
#ifdef _DEBUG_
	AppendToDebugReport("SetShc. Start SEV");
#endif
			PredictSEVSamples();
#ifdef _DEBUG_
	AppendToDebugReport("SetShc. SEV OK");
#endif
		}

		if (mOptFlags.bSECV)
		{
			SECV();
		}
	}

	return nError;
}

int CQntCalibration::SetHarmonics(int numHarmonics)  // number of harmonics 
{


	nError = ER_OK;

	if (pModel)
	{
		pModel->SetHarmonics(numHarmonics);
	}

	nError = pModel->GetError();

#ifdef _DEBUG_
	if (nError != ER_OK)
	{
		AppendToDebugReport("Error in SetHarmonics");
	}
#endif


	//char strTmp[64];
	//sprintf(strTmp, "End CQntCalibration::SetHarmonicsnError=%d numHarmonics=%d", nError, numHarmonics);
	//AppendToDebugReport(strTmp);

	return nError;
}

int CQntCalibration::GetResults(CQntOutput *pQntOutput)  // number of factors
{
	int n = pQntOutput->vErr.size();
	for (int i = 0; i < n; i++) pQntOutput->vErr[i] = 0.0;


	if (pQntOutput->bSEC)
	{
		
		n = pQntOutput->vSEC.size();
		for (int i = 0; i < n; i++) pQntOutput->vSEC[i] = vSEC(i+1);

		n = pQntOutput->vR2StatSEC.size();
		for (int i = 0; i < n; i++) pQntOutput->vR2StatSEC[i] = vR2Stat(i+1);
	}

	if (pQntOutput->bSEV)
	{	
		n = pQntOutput->vSEV.size();
		for (int i = 0; i < n; i++) pQntOutput->vSEV[i] = vSEV(i+1);

		n = pQntOutput->vErr.size();
		for (int i = 0; i < n; i++) pQntOutput->vErr[i] = vErr(i+1);

		n = pQntOutput->vSDV.size();
		for (int i = 0; i < n; i++) pQntOutput->vSDV[i] = vSDV(i+1);

		n = pQntOutput->vR2StatSEV.size();
		for (int i = 0; i < n; i++) pQntOutput->vR2StatSEV[i] = vR2StatSEV(i+1);
	}

	if (pQntOutput->bSECV)
	{	
		n = pQntOutput->vSECV.size();
		for (int i = 0; i < n; i++) pQntOutput->vSECV[i] = vSECV(i+1);

		n = pQntOutput->vR2StatSECV.size();
		for (int i = 0; i < n; i++)	pQntOutput->vR2StatSECV[i] = vR2StatSECV(i+1);

		n = pQntOutput->vFStatSECV.size();
		for (int i = 0; i < n; i++) pQntOutput->vFStatSECV[i] = vFStat(i+1);
	}

	return nError;
}

// ��� ���������� SECV  � ������ �����������
int CQntCalibration::SECV(int numFactors)
{
	CModel *pModelSECV = 0;

	CSpecPreprocData specPreprocDataSECV;

	GetSpecPreprocData(specPreprocDataSECV);

	int numberOfComponents = pClbSamples->GetNumComponents();
	int numberOfSpectra = pClbSamples->GetNumSpectra();
	int numberOfSamples = pClbSamples->GetNumSamples();

	Matrix mConcPredicted;//, mSEC;

	mConcPredicted.ReSize(numberOfSpectra, numberOfComponents);


	Matrix mXSECV0;
	if(SECV_PREP_ONCE)
	{
		mXSECV0.CleanUp();
		PreprocessSpectraSECV(specPreprocDataSECV, mXSECV0, -1);
	}

	Matrix mXSECV, mYSECV;

	Matrix mSampleSpectra;
	Matrix mSSpPrep;
	
	Matrix mScores;

	for (int iS = 0; iS < numberOfSamples; iS++)
	{
		mXSECV.CleanUp();
		mYSECV.CleanUp();

		if(SECV_PREP_ONCE)
		{
			mXSECV = mXSECV0;
			ExcludeSampleSECV(specPreprocDataSECV, mXSECV, iS);
		}
		else
		{
			PreprocessSpectraSECV(specPreprocDataSECV, mXSECV, iS);
		}



		pClbSamples->PreprocessComponents(specPreprocDataSECV, mYSECV, iS);
		nError = pClbSamples->GetError();


		if (nError != ER_OK)
		{
			return nError;
		}



		//AppendToDebugReport("start of model in SECV");

//		char sTmp[16];
//		sprintf(sTmp, "model type = %d", nModelType);
		//AppendToDebugReport(sTmp);

		switch(specPreprocDataSECV.nModelType)
		{
		case MODEL_PLS:
			pModelSECV = new CModelPLS(mXSECV, mYSECV, numFactors);
			nError = pModelSECV->GetError();
			break;

		case MODEL_PCR:
			if (iS == 0)
			{
				pModelSECV = new CModelPCR(mXSECV, mYSECV, numFactors);
				pModelSECV->GetMatrixScores(mScores);
			}
			else
			{
				pModelSECV = new CModelPCR(mXSECV, mYSECV, numFactors, mScores);
			}
			nError = pModelSECV->GetError();
			break;

//		case MODEL_HSO:
			//AppendToDebugReport("start of HSO in SECV");
//			pModel = new CModelHSO(mX, mY, pUseFreq, &theModelPar, 0);
			//AppendToDebugReport("end of HSO in SECVt");

//			nError = pModel->GetError();
//			break;

		default:
			nError = ER_MODEL_TYPE;
		}

		//ppendToDebugReport("end model in SECV");

		if (nError == ER_OK)
		{

			//RowVector vMeanSampleSpectrum;
			//RowVector vMSSpPrep;

			mSampleSpectra.CleanUp();
			mSSpPrep.CleanUp();

			//vMeanSampleSpectrum.CleanUp();
			//vMSSpPrep.CleanUp();


			pClbSamples->GetSampleSpectra(mSampleSpectra, iS);
			//GetSampleSpectra(mSampleSpectra, iS);

			//pClbSamples->GetMeanSpectrum(iS, vMeanSampleSpectrum);

//			if (nTransFlag != 2)
//			{// without frequency adjustment

			//AppendToDebugReport("start PrepSpForPrediction in SECV");

			nError = PrepSpForPrediction(&freqScale, specPreprocDataSECV, mSampleSpectra, mSSpPrep, true);

			if (nError != ER_OK)
			{
				//AppendToDebugReport("Error in PrepSpForPrediction in SECV");
				return nError;
			}
			//AppendToDebugReport("end PrepSpForPrediction in SECV");

//				PrepSpForPrediction(specPreprocData, vMeanSampleSpectrum, vMSSpPrep, freqScale.fStepFreq);

//				theTransData.CorrectSpectra(mSSpPrep);
//				theTransData.CorrectSpectra(vMSSpPrep);
//			}
/*			else
			{
				// specific case: transfered and added calibration

				if (pClbSamples->IsSampleAdded(iS))
				{
					PrepSpForPrediction(&freqScale, specPreprocData, mSampleSpectra, mSSpPrep);
					PrepSpForPrediction(&freqScale, specPreprocData, vMeanSampleSpectrum, vMSSpPrep);
				}
				else
				{
					// at first perform the constant preproc
					CSpecPreprocData thePreprocDataConst;

					pClbSamples->GetPreprocDataConst(thePreprocDataConst);

					Matrix mSSpPrepConst;
					RowVector vMSSpPrepConst;


					PrepSpForPrediction(&freqScale, thePreprocDataConst, mSampleSpectra, mSSpPrepConst);
					PrepSpForPrediction(&freqScale, thePreprocDataConst, vMeanSampleSpectrum, vMSSpPrepConst);

					// then correct the spectra
					theTransData.CorrectSpectra(mSSpPrepConst);
					theTransData.CorrectSpectra(vMSSpPrepConst);

					// then perform the remain preprocessing
					CSpecPreprocData thePreprocDataRem;

					thePreprocDataRem.Copy(specPreprocData);

					string strPreprocConst = thePreprocDataConst.strPreprocPar;

					string strPreprocFull, strPreprocCut;
					strPreprocFull = specPreprocData.strPreprocPar;


					for (int il = 0; il < strPreprocFull.length(); il++)
					{
						char ch = strPreprocFull[il];
						if (strPreprocConst.find(ch) == string::npos)
						{
							strPreprocCut += ch;
						}
					}

					thePreprocDataRem.strPreprocPar = strPreprocCut;
					thePreprocDataRem.nSpType = STYPE_TRA;


					PrepSpForPrediction(thePreprocDataRem, mSSpPrepConst, mSSpPrep, freqScale.fStepFreq);
					PrepSpForPrediction(thePreprocDataRem, vMSSpPrepConst, vMSSpPrep, freqScale.fStepFreq);
				}
			}*/

			Matrix mYpredicted;

			int indS, numSSp;

			indS = pClbSamples->GetSampleSpectraIndexes(iS);
			numSSp = pClbSamples->GetSampleSpectraNumbers(iS);

			//AppendToDebugReport("start of Predict in SECV");

			//char strBuf[256];
			//sprintf(strBuf, "mSSpPrep.Ncols = %d mXSECV.Ncols=%d\n", mSSpPrep.Ncols(), mXSECV.Ncols());
			//AppendToDebugReport(strBuf);

			pModelSECV->Predict(mSSpPrep, mYpredicted);
			nError = pModelSECV->GetError();
			if (nError != ER_OK)
			{
				//AppendToDebugReport("Error in pModelSECV->Predict in SECV");
				return nError;
			}
			//AppendToDebugReport("end of Predict in SECV");

			// �� ��������� ���������
			PostProcessComponents(mYpredicted, specPreprocDataSECV, false);
			//AppendToDebugReport("end PostProcess in SECV");

			/*if (theTransData.nTransType == 4)
			{
				// new transfer
				PostProcessComponents(mYpredicted, pClbSamples->thePreprocDataConst);
			}*/


			//theTransData.CorrectComponents(mYpredicted);

			mConcPredicted.Rows(indS + 1, indS + numSSp) = mYpredicted;
			//AppendToDebugReport("mConcPredicted in SECV");

			mYpredicted.CleanUp();
			//AppendToDebugReport("end mYpredicted.CleanUp in SECV");

			// now predict MeanSpectrum

			//pModelSECV->Predict(vMSSpPrep, mYpredicted);

			//PostProcessComponents(mYpredicted, specPreprocData, false);

			//theTransData.CorrectComponents(mYpredicted);

			//mSampleConcPredicted.Row(iS + 1) = mYpredicted;

			//mYpredicted.CleanUp();


			//pModelSECV->Predict(mX, mYpredicted);

			//RowVector vSEC;
			//vSEC.ReSize(numberOfComponents);

			//PostProcessComponents(mY, specPreprocDataSECV, false);

			/*if (theTransData.nTransType == 4)
			{
				// new transfer
				PostProcessComponents(mY, pClbSamples->thePreprocDataConst);
			}*/

			//PostProcessComponents(mYpredicted, specPreprocDataSECV, false);

			/*if (theTransData.nTransType == 4)
			{
				// new transfer
				PostProcessComponents(mYpredicted, pClbSamples->thePreprocDataConst);
			}*/

			//Matrix	mErrorsSEC;
			
			//mErrorsSEC.CleanUp();
			//mErrorsSEC = mY - mYpredicted;

			//int numberOfFreedomDegrees = mX.Nrows() - pModelSECV->GetNumVariables();

//			fprintf(fgh, "numVar = %d dof = %d\n", pModel->GetNumVariables(), numberOfFreedomDegrees);

//			if (specPreprocData.meanComponentsTrans.Ncols() != 0)
//			{
//				numberOfFreedomDegrees--;
//			}

			//if (specPreprocDataSECV.meanSpectrum.Ncols() != 0)
			//{
			//	numberOfFreedomDegrees--;
			//}

			//if (numberOfFreedomDegrees <= 0)
			//{
			//	nError = ER_SEC;
			//	return;
			//}

/*			for (int iC = 1; iC <= numberOfComponents; iC++)
			{
				vSEC(iC) = (mErrorsSEC.Column(iC)).SumSquare();
				vSEC(iC) /= numberOfFreedomDegrees;
				vSEC(iC) = sqrt(vSEC(iC));
			}
			mSEC.Row(iS + 1) = vSEC;*/
			//AppendToDebugReport("start delete model in SECV");
			delete pModelSECV;
			//AppendToDebugReport("end delete model in SECV");
			pModelSECV = 0;
		}
		else
		{
			delete pModelSECV;
			pModelSECV = 0;
			return nError;
		}
	}
//	fclose(fgh);

//	delete[] pUseFreq;

	Matrix mConc;

	pClbSamples->GetMComponents(mConc);

	Matrix	mErrors = mConc - mConcPredicted;

	RowVector vStd2Comp;

	pClbSamples->GetStd2Components(vStd2Comp);

//	vR2Stat.ReSize(numberOfComponents);

//	vSECV.ReSize(numberOfComponents);

	for (int iC = 1; iC <= numberOfComponents; iC++)
	{
		vSECV(iC) = (mErrors.Column(iC)).SumSquare();

		vR2StatSECV(iC) = vSECV(iC) / vStd2Comp(iC);

		vSECV(iC) = sqrt(vSECV(iC) / numberOfSpectra);
	}


	if (numberOfSpectra - numFactors - 1.0 <= 0)
	{
		nError = ER_SEC;
		return nError;
	}

	vR2StatSECV *= -(numberOfSpectra - 1.0) / (numberOfSpectra - numFactors - 1.0);

	vR2StatSECV += 1.0;

//	vFStat.ReSize(numberOfComponents);

	for (int iC = 1; iC <= numberOfComponents; iC++)
	{
		vFStat(iC) = vR2StatSECV(iC) / (1.0 - vR2StatSECV(iC));
	}

	vFStat *= (numberOfSpectra - numFactors - 1.0) / numFactors;

	//AppendToDebugReport("end of SECV");
	return nError;
}

// ��� HQO ������ � ������ �����������
int CQntCalibration::SECV()
{
	CSpecPreprocData specPreprocDataSECV;

	GetSpecPreprocData(specPreprocDataSECV);

	int numberOfComponents = pClbSamples->GetNumComponents();
	int numberOfSpectra = pClbSamples->GetNumSpectra();
	int numberOfSamples = pClbSamples->GetNumSamples();

	Matrix mConcPredicted;//, mSEC;

	mConcPredicted.ReSize(numberOfSpectra, numberOfComponents);

	Matrix mXSECV0;
	if(SECV_PREP_ONCE)
	{
		mXSECV0.CleanUp();
		PreprocessSpectraSECV(specPreprocDataSECV, mXSECV0, -1);
	}

	Matrix mXSECV, mYSECV;

	Matrix mSampleSpectra;
	Matrix mSSpPrep;
	
	Matrix mScores;

	for (int iS = 0; iS < numberOfSamples; iS++)
	{
		//sprintf(strBuffer,"%d\n", iS);
		//AppendToDebugReport(strBuffer);


		mXSECV.CleanUp();
		mYSECV.CleanUp();

		if(SECV_PREP_ONCE)
		{
			mXSECV = mXSECV0;
			ExcludeSampleSECV(specPreprocDataSECV, mXSECV, iS);
		}
		else
		{
			PreprocessSpectraSECV(specPreprocDataSECV, mXSECV, iS);
		}

		//AppendToDebugReport("end of preproc in SECV");

//		if (nTransFlag != 2)
//		{
//			theTransData.CorrectSpectra(mX);
//		}

		pClbSamples->PreprocessComponents(specPreprocDataSECV, mYSECV, iS);
		nError = pClbSamples->GetError();

		//AppendToDebugReport("end of preproc comp. in SECV");

		if (nError != ER_OK)
		{
			return nError;
		}

		//AppendToDebugReport("start of model in SECV");

//		char sTmp[16];
//		sprintf(sTmp, "model type = %d", nModelType);
		//AppendToDebugReport(sTmp);

		switch(specPreprocDataSECV.nModelType)
		{
		case MODEL_HSO:
			nError = pModel->CreateModel(mXSECV, mYSECV);
			break;

		default:
			nError = ER_MODEL_TYPE;
		}

		if (nError == ER_OK)
		{

			mSampleSpectra.CleanUp();
			mSSpPrep.CleanUp();

			pClbSamples->GetSampleSpectra(mSampleSpectra, iS);

			nError = PrepSpForPrediction(&freqScale, specPreprocDataSECV, mSampleSpectra, mSSpPrep, true);

			if (nError != ER_OK)
			{
				return nError;
			}
			Matrix mYpredicted;

			int indS, numSSp;

			indS = pClbSamples->GetSampleSpectraIndexes(iS);
			numSSp = pClbSamples->GetSampleSpectraNumbers(iS);

			pModel->Predict(mSSpPrep, mYpredicted);
			nError = pModel->GetError();

			if (nError != ER_OK)
			{
				return nError;
			}

			// �� ��������� ���������
			PostProcessComponents(mYpredicted, specPreprocDataSECV, false);

			mConcPredicted.Rows(indS + 1, indS + numSSp) = mYpredicted;

			mYpredicted.CleanUp();
		}
		else
		{
			return nError;
		}
	}

	Matrix mConc;

	pClbSamples->GetMComponents(mConc);

	Matrix	mErrors = mConc - mConcPredicted;

	RowVector vStd2Comp;

	pClbSamples->GetStd2Components(vStd2Comp);

//	vR2Stat.ReSize(numberOfComponents);

//	vSECV.ReSize(numberOfComponents);

	for (int iC = 1; iC <= numberOfComponents; iC++)
	{
		vSECV(iC) = (mErrors.Column(iC)).SumSquare();

		vR2StatSECV(iC) = vSECV(iC) / vStd2Comp(iC);

		vSECV(iC) = sqrt(vSECV(iC) / numberOfSpectra);
	}

	int numFactors = pModel->GetNumVariables();
	vR2StatSECV *= -(numberOfSpectra - 1.0) / (numberOfSpectra - numFactors - 1.0);

	vR2StatSECV += 1.0;

	//char str[64];
	//sprintf(str, "numFactors = %d", numFactors);

	//AppendToDebugReport(str);


//	vFStat.ReSize(numberOfComponents);

	for (int iC = 1; iC <= numberOfComponents; iC++)
	{
		vFStat(iC) = vR2StatSECV(iC) / (1.0 - vR2StatSECV(iC));
	}

	vFStat *= (numberOfSpectra - numFactors - 1.0) / numFactors;

//	AppendToDebugReport("end of SECV");
	return nError;
}



// for SECV
int CQntCalibration::PreprocessSpectraSECV(CSpecPreprocData& specPreprocDataSECV, Matrix& mX, int iS)
{
	if (specPreprocDataSECV.strPreprocPar.find_first_of('A') != string::npos)
	{
		int numSamples = mSpectraABSTRAcut.Nrows();
		if (iS == -1)
		{
			mX = mSpectraABSTRAcut.Rows(1, numSamples);
		}
		else if (iS == 0)
		{
			mX = mSpectraABSTRAcut.Rows(2, numSamples);
		}
		else
		{
			if (iS == numSamples)
			{
				mX = mSpectraABSTRAcut.Rows(1, numSamples - 1);
			}
			else
			{
				mX = mSpectraABSTRAcut.Rows(1, iS) & mSpectraABSTRAcut.Rows(iS + 2, numSamples);
			}
		}
	}
	else
	{

		int numberOfSamples = pClbSamples->GetNumSamples();
		int numberOfSpectra = pClbSamples->GetNumSpectra();

		if (iS == -1)
		{
			mX = mSpectraABSTRAcut.Rows(1, numberOfSpectra);
		}
		else
		{
			int indS, indSnext;

			indS = pClbSamples->GetSampleSpectraIndexes(iS);

			if (iS < numberOfSamples - 1)
			{
				indSnext = pClbSamples->GetSampleSpectraIndexes(iS + 1);
			}
			else
			{
				indSnext = numberOfSpectra;
			}

			if (indS != 0)
			{
				if (indSnext < numberOfSpectra)
				{
					mX = mSpectraABSTRAcut.Rows(1, indS) & mSpectraABSTRAcut.Rows(indSnext + 1, numberOfSpectra);
				}
				else
				{
					mX = mSpectraABSTRAcut.Rows(1, indS);
				}
			}
			else
			{
				mX = mSpectraABSTRAcut.Rows(indSnext + 1, numberOfSpectra);
			}
		}
	}

	PreprocessSpectra(specPreprocDataSECV, freqScale.fStepFreq, mX);

	return nError;
}

int CQntCalibration::ExcludeSampleSECV(CSpecPreprocData& specPreprocDataSECV, Matrix& mX, int iS)
{
	Matrix mXtmp = mX;
	if (specPreprocDataSECV.strPreprocPar.find_first_of('A') != string::npos)
	{
		int numSamples = mSpectraABSTRAcut.Nrows();
		if (iS == 0)
		{
			mX = mXtmp.Rows(2, numSamples);
		}
		else
		{
			if (iS == numSamples)
			{
				mX = mXtmp.Rows(1, numSamples - 1);
			}
			else
			{
				mX = mXtmp.Rows(1, iS) & mXtmp.Rows(iS + 2, numSamples);
			}
		}
	}
	else
	{

		int numberOfSamples = pClbSamples->GetNumSamples();
		int numberOfSpectra = pClbSamples->GetNumSpectra();

		int indS, indSnext;

		indS = pClbSamples->GetSampleSpectraIndexes(iS);

		if (iS < numberOfSamples - 1)
		{
			indSnext = pClbSamples->GetSampleSpectraIndexes(iS + 1);
		}
		else
		{
			indSnext = numberOfSpectra;
		}

		if (indS != 0)
		{
			if (indSnext < numberOfSpectra)
			{
				mX = mXtmp.Rows(1, indS) & mXtmp.Rows(indSnext + 1, numberOfSpectra);
			}
			else
			{
				mX = mXtmp.Rows(1, indS);
			}
		}
		else
		{
			mX = mXtmp.Rows(indSnext + 1, numberOfSpectra);
		}
	}

	return nError;
}

void CQntCalibration::GetSampleSpectra(Matrix& mSampleSpectra, int iS)
{
	int indS, numSSp;

	indS = pClbSamples->GetSampleSpectraIndexes(iS);
	numSSp = pClbSamples->GetSampleSpectraNumbers(iS);

	mSampleSpectra = mSpectraABSTRAcut.Rows(indS + 1, indS + numSSp);
}



