
#include "GarmTransform.h"
#include "const.h"
#include "MatrixUtilities.h"
#include <complex>


/*Matrix mGarmGl;
bool bGl = false;

void InitGarmGl(int numFreq, int numFact, const int *pUseFreq)
{
	mGarmGl.ReSize(numFreq, numFact);

	for(int nFreq=0; nFreq < numFreq; nFreq++)
	{
		for(int nFact=0; nFact < numFact; nFact++)
		{
			if (nFact == 0)
			{
				mGarmGl(nFreq+1, nFact+1) = pUseFreq[nFreq]*0.5;
				continue;
			}

			if (nFact%2)//��������
			{
				mGarmGl(nFreq+1, nFact+1) = pUseFreq[nFreq]*cos((nFact+1)*M_PI*(-0.5+nFreq/(numFreq-1.0)));
			}
			else
			{
				mGarmGl(nFreq+1, nFact+1) = pUseFreq[nFreq]*sin(nFact*M_PI*(-0.5+nFreq/(numFreq-1.0)));
			}
		}
   }

	bGl = true;
}

void GarmTransformOld(int numFact, const Matrix &mX, const int *pUseFreq, Matrix &mXGarm)
{
	//mX[numSp x numFreq]
	//mGarm[numFreq x numFact]
	//mX=mGarm*mX [numSp x numFact]
	
	// ������ ��� ������ ����� sin � cos
	int numFreq = mX.Ncols();

	Matrix mGarm(numFreq, numFact);

	//������������ mGarm � ������ pUseFreq[numFreq]
	for(int nFreq=0; nFreq < numFreq; nFreq++)
	{
		for(int nFact=0; nFact < numFact; nFact++)
		{
			if (nFact == 0)
			{
				mGarm(nFreq+1, nFact+1) = pUseFreq[nFreq]*0.5;
				continue;
			}

			if (nFact%2)//��������
			{
				mGarm(nFreq+1, nFact+1) = pUseFreq[nFreq]*cos((nFact+1)*M_PI*(-0.5+nFreq/(numFreq-1.0)));
			}
			else
			{
				mGarm(nFreq+1, nFact+1) = pUseFreq[nFreq]*sin(nFact*M_PI*(-0.5+nFreq/(numFreq-1.0)));
			}
		}
   }

	mXGarm = mX * mGarm;
//	mXGarm = mGarm;

	//PrintMatrix(mGarm.Column(4), "mGarmCol4Old.dat");

	return;
}

void GarmTransformNewGl(int numFact, const Matrix &mX, const int *pUseFreq, Matrix &mXGarm)
{
	//mX[numSp x numFreq]
	//mGarm[numFreq x numFact]
	//mX=mGarm*mX [numSp x numFact]
	
	int numFreq = mX.Ncols();

	if (!bGl)
	{
		InitGarmGl(numFreq, numFact, pUseFreq);
	}
	else
	{
		mXGarm = mX * mGarmGl;
	}

	return;
}*/

/*void GarmTransformOld2(int numFact, int numFreq, const int *pUseFreq, Matrix &mGarm)
{
	//mGarm[numFreq x numFact]
	//AppendToDebugReport("Start GarmTransform");

	// wrong optimized code!!!


	mGarm.CleanUp();
	mGarm.ReSize(numFreq, numFact);

	mGarm.Column(1)=0.5;

	// numFreq - odd
	double w = 2*M_PI/(numFreq-1.0);

	int N = numFreq/2;
	int nGarm = numFact/2;

	double fCos, fSin;

	double fSign = 1;

	for (int i = 0; i < N; i++)
	{
		fCos = cos(i*w);
		fSin = sin(i*w);

		mGarm(i+1, 2) = -fCos;
		mGarm(i+1, 3) = -fSin;

		mGarm(N+i+1, 2) = fCos;
		mGarm(N+i+1, 3) = fSin;

		fSign = 1;

		for (int m = 2; m <= nGarm; m++)
		{
			mGarm(i+1, 2*m) = -mGarm(i+1, 2*(m-1))*fCos + mGarm(i+1, 2*m-1)*fSin;
			mGarm(i+1, 2*m+1) = -mGarm(i+1, 2*(m-1))*fSin - mGarm(i+1, 2*m-1)*fCos;
			mGarm(N+i+1, 2*m) = fSign*mGarm(i+1, 2*m);
			mGarm(N+i+1, 2*m+1) = fSign*mGarm(i+1, 2*m+1);
			fSign *= -1;
		}
	}

	fSign = -1.0;
	for (int m = 1; m <= nGarm; m++)
	{
		mGarm(numFreq, 2*m) = fSign;
		mGarm(numFreq, 2*m+1) = 0;
		fSign *= -1;
	}

	for (int i = 0; i < numFreq; i++)
	{
		if (pUseFreq[i] == 0)
		{
			mGarm.Row(i+1) = 0;
		}
	}

	//Matrix mGarmT = mGarm.t();
	//PrintMatrix(mGarmT, "mGarm.dat");
	//AppendToDebugReport("End GarmTransform");

	return;
}*/

void GarmTransform(int numFact, int numFreq, const int *pUseFreq, Matrix &mGarm)
{
	//mX[numSp x numFreq]
	//mGarm[numFreq x numFact]
	//mX=mGarm*mX [numSp x numFact]

	mGarm.CleanUp();
	mGarm.ReSize(numFreq, numFact);

	complex <double> w(polar(1.0, 2*M_PI/(numFreq-1.0)));
	complex <double> z (1.0, 0);

	//������������ mGarm � ������ pUseFreq[numFreq]

	mGarm.Column(1) = 0.5;

	int numGarm = numFact / 2;

	int m = 1;

	for(int k = 0; k < numFreq; k++)
	{
		mGarm(k+1, 2*m) = -real(z);
		mGarm(k+1, 2*m+1) = -imag(z);
		z *= w;
	}


	if (numGarm >= 2)
	{
		for(int k = 0; k < numFreq; k++)
		{
			w = complex <double>(mGarm(k+1, 2), mGarm(k+1, 3));
			z = w;//complex <double> (1.0, 0.0);

			for(m = 2; m <= numGarm; m++)
			{
				z *= w;
				mGarm(k+1, 2*m) = real(z);
				mGarm(k+1, 2*m+1) = imag(z);
			}
		}
	}

	for (int i = 0; i < numFreq; i++)
	{
		if (pUseFreq[i] == 0)
		{
			mGarm.Row(i+1) = 0;
		}
	}


	return;
}

/*void GarmTransform(int numFact, int numFreq, const VInt &vFreqExcluded, Matrix &mGarm)
{
	//mGarm[numFreq x numFact]

	mGarm.Column(1)=0.5;

	// numFreq - odd
	double w = 2*M_PI/(numFreq-1.0);

	int N = numFreq/2;
	int nGarm = numFact/2;

	double fCos, fSin;

	double fSign = 1;

	for (int i = 0; i < N; i++)
	{
		fCos = cos(i*w);
		fSin = sin(i*w);

		mGarm(i+1, 2) = -fCos;
		mGarm(i+1, 3) = -fSin;

		mGarm(N+i+1, 2) = fCos;
		mGarm(N+i+1, 3) = fSin;

		fSign = 1;

		for (int m = 2; m <= nGarm; m++)
		{
			mGarm(i+1, 2*m) = -mGarm(i+1, 2*(m-1))*fCos + mGarm(i+1, 2*m-1)*fSin;
			mGarm(i+1, 2*m+1) = -mGarm(i+1, 2*(m-1))*fSin - mGarm(i+1, 2*m-1)*fCos;
			mGarm(N+i+1, 2*m) = fSign*mGarm(i+1, 2*m);
			mGarm(N+i+1, 2*m+1) = fSign*mGarm(i+1, 2*m+1);
			fSign *= -1;
		}
	}

	fSign = -1.0;
	for (int m = 1; m <= nGarm; m++)
	{
		mGarm(numFreq, 2*m) = fSign;
		mGarm(numFreq, 2*m+1) = 0;
		fSign *= -1;
	}

	for (int i = 0; i < numFreq; i++)
	{
		if (vFreqExcluded[i] == 0)
		{
			mGarm.Row(i+1) = 0;
		}
	}

	return;
}*/



